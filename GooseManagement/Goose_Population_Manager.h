/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Goose_Population_Manager.h This is the header file for the goose population manager class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 21st February 2013 \n
*/
//---------------------------------------------------------------------------

#ifndef Goose_Population_ManagerH
#define Goose_Population_ManagerH

//---------------------------------------------------------------------------

class Goose;
class Goose_Population_Manager;
class AOR_Probe_Goose;

//------------------------------------------------------------------------------
/** \brief a list of goose roosts as points */
typedef std::vector<APoint> roostlist;

/**
\brief
Used for creation of a new Goose object
*/
class struct_Goose
{
 public:
  /**
  \brief the sex
  */
  bool m_sex;
  /**
  \brief is part of a family
  */
  bool m_family;
  /**
  \brief Size of family unit if any
  */
  int m_grpsize;
  /**
  \brief x-coord
  */
  int m_x;
  /**
  \brief y-coord
  */
  int m_y;
  /**
  \brief species ID
  */
  int m_species;
  /**
  \brief
  The weight
  */
  double m_weight;
  /**
  \brief Landscape pointer
  */
  Landscape* m_L;
  /** \brief
  Goose_Population_Manager pointer
  */
  Goose_Population_Manager * m_GPM;
  /** \brief
  Roost location
  */
  APoint m_roost;
};

/**
\brief
A class to hold an active goose foraging location and the number of birds of different types there
*/
class GooseActiveForageLocation
{
protected:
	/**
	\brief
	A landscape element ( LE ) reference number used by the Landscape class to identify this location.
	*/
	int m_polygonref;
	/**
	\brief
	The area of feeding location.
	*/
	double m_area;
	/**
	\brief
	The grain density in kJ/m2
	*/
	double m_graindensity;
	/**
	\brief
	The maize density in kJ/m2
	*/
	double m_maizedensity;
	/**
	\brief
	The grazing intake rate in kJ/min
	*/
	double m_grazingrate[gs_foobar];
	/** 
	\brief 
	Contains the total amount of grazing eaten today 
	*/
	double m_grazedbiomass;
	/**
	\brief
	The total grain kJ
	*/
	double m_grainKJ_total;
	/**
	\brief
	The total maize kJ
	*/
	double m_maizeKJ_total;
	/**
	\brief
	Whether it is a cereal crop, grass etc
	*/
	TTypeOfMaxIntakeSource m_HabitatType;
	/**
	\brief
	An array holding the number of geese of different type i.e. Pinkfoot families, juvs, Barnacle families, juvs, Greylag familes, juvs
	*/
	int m_BirdsPresent[gst_foobar];
	/**
	\brief
	An array holding the maximum number of geese of different types and total i.e. Pinkfoot families, juvs, Barnacle families, juvs, Greylag familes, juvs present on the field at one time 
	for each day.
	*/
	int m_MaxBirdsPresent[gst_foobar]; 
	/** \brief
	An array holding the distance to the closest roost.
	*/
	double m_dist_to_closest_roost[gs_foobar];
	/** \brief
	This is a time saving pointer to the correct population manager object
	*/
	Goose_Population_Manager*  m_OurPopulationManager;
public:
	/**
	\brief
	Constructor for GooseActiveForageLocation
	*/
	GooseActiveForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref, int a_area, double a_graindensity, double a_maizedensity, double * a_grazing, Goose_Population_Manager* p_NPM);
	/**
	\brief
	Set the area in m
	*/
	void SetArea( int a_area) { m_area = a_area; }
	/**
	\brief
	Get the area in m
	*/
	double GetArea() { return m_area; }
	/**
	\brief
	Get the landscape element ( LE ) reference number used by the Landscape class to identify this location.
	*/
	int GetPolygonref() { return m_polygonref; }
	/**
	\brief
	Sum of the maximum of all geese present (this may not be the same as the real maximum)
	*/
	int GetMaxBirdsPresent()
	{ 		
		int sum=0; 
		for (int i = (int) 0; i<gst_foobar;i++) 
			sum+=m_MaxBirdsPresent[i]; 
		return sum; 
	}
	/**
	\brief
	Sum of the maximum of each goose species present (this may not be the same as the real maximum)
	*/
	int GetMaxSpBirdsPresent(GooseSpecies a_goose) {
		int sum = 0;
			switch (a_goose) {
				case gs_Pinkfoot:
					sum += (m_MaxBirdsPresent[ gst_PinkfootFamilyGroup ] + m_MaxBirdsPresent[ gst_PinkfootNonBreeder ]);
					break;
				case gs_Barnacle:
					sum += (m_MaxBirdsPresent[ gst_BarnacleFamilyGroup ] + m_MaxBirdsPresent[ gst_BarnacleNonBreeder ]);
					break;
				case gs_Greylag:
					sum += (m_MaxBirdsPresent[ gst_GreylagFamilyGroup ] + m_MaxBirdsPresent[ gst_GreylagNonBreeder ]);
					break;
				default:
					return -1;
			}
			return sum;
		}
		
	/**	\brief	Set the landscape element ( LE ) reference number used by the Landscape class to identify this location.*/
	void SetPolygonref(int a_polyref) { m_polygonref = a_polyref; }
	/**	\brief	Returns the number of huntable birds at the location */
	int GetHuntables(void);
	/**
	\brief
	Returns the total number of geese at the location.
	*/
	int GetGooseNumbers( void ) {
		int sum = 0;
		for (int i = 0; i < gst_foobar; i++) {
			sum += m_BirdsPresent[ i ];
		}
		return sum;
	}
	/**
	\brief
	Returns the density of geese at the location.
	*/
	double GetGooseDensity( void ) {
		//return GetGooseNumbers() / m_area;
		return GetGooseNumbers();  /** @todo Testing a new implementation which uses flock size and not density.*/
	}
	/**
	\brief
	Get how many birds of a type
	*/
	int GetBirds(GooseSpeciesType a_type) { return m_BirdsPresent[(int)a_type]; }
	/**
	\brief
	Get how many birds of a species
	*/
	int GetBirds( GooseSpecies a_goose ) {
				int sum = 0;
		switch (a_goose) {
			case gs_Pinkfoot:
				sum += (GetBirds(gst_PinkfootFamilyGroup) + GetBirds(gst_PinkfootNonBreeder));
				break;
			case gs_Barnacle:
				sum += (GetBirds(gst_BarnacleFamilyGroup) + GetBirds(gst_BarnacleNonBreeder));
				break;
			case gs_Greylag:
				sum += (GetBirds(gst_GreylagFamilyGroup) + GetBirds(gst_GreylagNonBreeder));
				break;
			default:
				return -1;
		}
		return sum;
	}
	
	/**\brief
	Adds geese to the location.
	*/
	void AddGeese(GooseSpeciesType a_type, int a_number) 
	{
		m_BirdsPresent[(int)a_type] += a_number; 
		if (m_BirdsPresent[(int)a_type] > m_MaxBirdsPresent[(int)a_type])
		{
			m_MaxBirdsPresent[(int)a_type] = m_BirdsPresent[(int)a_type];
		}
	}
	/**
	\brief
	Removes geese to the location.
	*/
	void RemoveGeese(GooseSpeciesType a_type, int a_number) { 
		m_BirdsPresent[a_type] -= a_number; 
#ifdef __DEBUG_GOOSE
		if (m_BirdsPresent[a_type]<0) 
		{
			g_msg->Warn("GooseActiveForageLocation::RemoveGeese - Negative birds at forage location", (double)m_BirdsPresent[a_type]);
			exit(0);
		}
#endif
	}
	/**
	\brief
	Returns the current forage density (grain/m2)
	*/
	double GetGrainDensity(void) { return m_graindensity; }
	/**
	\brief
	Sets forage density (grain/m2)
	*/
	void SetGrainDensity(double a_density) { m_graindensity = a_density; }
	/**
	\brief
	Returns the current maize forage density (kJ/m2)
	*/
	double GetMaizeDensity(void) {
		return m_maizedensity;
	}
	/**
	\brief
	Returns whether the current crop is a cereal
	*/
	bool GetHabitatType(void) {
		return m_HabitatType;
	}
	/**
	\brief
	Sets maize forage density (kJ/m2)
	*/
	void SetMaizeDensity( double a_density ) {
		m_maizedensity = a_density;
	}
	/**
	\brief
	Returns the current forage rate kJ/min assuming no other geese affect this
	*/
	double GetGrazing(int gs) { return m_grazingrate[gs]; }
	/**
	\brief
	Returns the distance to the closest roost for gs
	*/
	double GetRoostDist(int gs) { return m_dist_to_closest_roost[gs]; }
	/**
	\brief
	Returns the current grazing forage eaten in g
	*/
	double GetGrazedBiomass(void) { return m_grazedbiomass; }
	/**
	\brief
	Sets forage density (kJ/m2)
	*/
	void SetGrazing(int gs, double a_density) { m_grazingrate[gs] = a_density; }
	/** \brief	Sets the distance to the closest roost in meters */
	void SetDistToClosestRoost(int gs, double a_dist) { m_dist_to_closest_roost[gs] = a_dist; }
	/**
	\brief
	Sets grazed biomass to zero
	*/
	void ResetGrazing() { m_grazedbiomass = 0.0; }
	
	/**	\brief Removes grain from the field as kJ.  */
	void RemoveGrainKJ(double a_kJ) { m_grainKJ_total -= a_kJ; }
	
	/**
	\brief
	Removes maize from the field as kJ
	*/
	void RemoveMaizeKJ( double a_kJ ) {
		m_maizeKJ_total -= a_kJ;
	}
	/**
	\brief
	Records forage removed as kJ from the field as grams
	*/
	void Graze(double a_kJ) {
		m_grazedbiomass += a_kJ * 0.2857;  /**3.5 kJ/gram ww; Robbins 1993*/
	}
	/**
	\brief
	Empties the bird recording arrays
	*/
	void ClearBirds();
	
	/**	\brief Updates the grain or maize density based on the current total grain or maize amount */
	void UpdateKJ() {
		m_graindensity = (m_grainKJ_total / m_area) / 17.67 / 0.04;  /** 17.67 kJ/g dw grain & 0.04 g/grain*/
		m_maizedensity = m_maizeKJ_total / m_area;
	}
};

/**
\brief
The class to handle all goose population related matters
*/
class Goose_Population_Manager : public Population_Manager
{
public:
	// Methods
		/** \brief
		Goose_Population_Manager Constructor
		*/
	Goose_Population_Manager(Landscape* L);
	/** \brief
	Goose_Population_Manager Destructor
	*/
	virtual ~Goose_Population_Manager(void);
	/** \brief
	* Get the time of day (in minutes).
	For the goose model, sunrise is defined as m_daytime == 0. Hence this function returns the number of minutes since sunrise.
	*/
	int GetDayTime() { return m_daytime; }
	/** \brief
	* Get the daylight minutes left today
	*/
	int GetDaylightLeft() { return m_daylightleft; }
	/** \brief
	* Is it daylight hours? Daylight starts at m_daytime == 0.
	*/
	int GetIsDaylight() { return m_daylight; }
	/** \brief Are we in the pinkfoot hunting season? */
	bool InPinkfootSeason() { return m_PinkfootSeason; }
	/** \brief Are we in the greylag hunting season? */
	bool InGreylagSeason() { return m_GreylagSeason; }
	/** \brief
	Method for creating a new individual Goose
	*/
	void CreateObjects(int ob_type, TAnimal *pvo, struct_Goose* data, int number);
	/** \brief
	Tests if a forage location is currently in use, if so returns the index to it
	*/
	int ForageLocationInUse(int a_polyref);
	/** \brief
	Adds a goose or geese to the forage location - uses polygon number as reference
	*/
	void AddGeeseToForageLocationP(GooseSpeciesType a_type, int a_polyref, int a_number);
	/** \brief
	Adds a goose or geese to the forage location - requires an index
	*/
	void AddGeeseToForageLocation(GooseSpeciesType a_type, int a_index, int a_number);
	/** \brief
	Removes a goose or geese to the forage location - requires an index
	*/
	void RemoveGeeseFromForageLocation(GooseSpeciesType a_type, int a_index, int a_number);
	/** \brief
	Creates a new forage location and adds a goose or geese to the forage location. Returns an index to the forage location list.
	*/
	int NewForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref);
	/** \brief
	Returns a pointer to the forage location indexed by index.
	*/
	GooseActiveForageLocation* GetForageLocation(unsigned int a_index) {
		if ((m_GooseForageLocations.size() == 0) || (m_GooseForageLocations.size() <= a_index)) return NULL; else return &m_GooseForageLocations[a_index];
	}
	/** \brief
	Get the forage rate based on the grain density
	\param [in] a_graindensity The grain density (grain/m2) on the forage location
	\param [in] a_species The type of goose species
	\return The species specific intake rate (kJ/min) when feeding on grain
	*/
	double GetFeedingRate(double a_graindensity, GooseSpecies a_species)
	{
		/**	Unpublished curve from Nolet.
		* o:/ST_GooseProject/Field data/Fugledata/Functional response pink-feet.xlsx
		* This curve has already taken digestibility, energy content and assimilation
		* into account.
		*/
		if (a_species == gs_Greylag) return m_IntakeRateVSGrainDensity_PF->GetY(a_graindensity) * 1.21;
		else if (a_species == gs_Pinkfoot) return m_IntakeRateVSGrainDensity_PF->GetY(a_graindensity);
		else return m_IntakeRateVSGrainDensity_PF->GetY(a_graindensity) * 0.74;
	}
	/** \brief
	Get the forage rate when feeding on maize
	\param [in] a_maizedensity The maize density in kJ/m2
	\param [in] a_species The type of goose species
	\return The species specific intake rate when feeding on maize
	*/
	double GetMaizeFeedingRate(double a_maizedensity, GooseSpecies a_species) {
		/**	Functional response for geese feeding on maize is currently lacking.
		The curve used here is based on field observations of barnacle geese 
		made with wildlife cameras in Jutland in 2015. Assimilation for energy
		is accounted for in the functional response curce. Value found for Bewick's swans
		from Nolet, B. unpubl. For greylag and pinkfeet the value is scaled based
		on the relationship between */
		if (a_species == gs_Greylag) return m_IntakeRateVSMaizeDensity_BN->GetY(a_maizedensity) * 1.64;
		else if (a_species == gs_Pinkfoot) return m_IntakeRateVSMaizeDensity_BN->GetY(a_maizedensity) * 1.35;
		else return m_IntakeRateVSMaizeDensity_BN->GetY(a_maizedensity);
	}
	/** \brief
	Get the forage intake rate for a forage density
	*/
	double GetForageRateDensity(double a_foragedensity) { return m_ForageRateVSGooseDensity->GetY(a_foragedensity); }
	/** \brief
	Returns the total goose density for a forage location.
	*/
	double GetForageGooseDensity(int a_index) { return m_GooseForageLocations[a_index].GetGooseDensity(); }
	/** \brief
	Returns the forage density for a forage location and goose type. This is species specific
	*/
	double GetForageGrazing(int a_index, int gs) { return m_GooseForageLocations[a_index].GetGrazing(gs); }
	/** \brief
	Returns the forage density for a forage location.
	*/
	double GetGrainDensity(int a_index) { return m_GooseForageLocations[a_index].GetGrainDensity(); }
	/** \brief
	Returns the maize forage density for a forage location.
	*/
	double GetMaizeDensity(int a_index) {
		return m_GooseForageLocations[a_index].GetMaizeDensity();
	}
	/** \brief Removes kJ eaten as grains from a forage area.*/
	void RemoveGrainKJ(double a_kJ, int a_index) { m_GooseForageLocations[a_index].RemoveGrainKJ(a_kJ); }

	/** \brief
	Removes KJ eaten as maize from a forage area.
	*/
	void RemoveMaizeKJ(double a_kJ, int a_index) {
		m_GooseForageLocations[a_index].RemoveMaizeKJ(a_kJ);
	}
	/** \brief
	Removes KJ as grams veg biomass from a forage area.
	*/
	void Graze(double a_kJ, int a_index) { m_GooseForageLocations[a_index].Graze(a_kJ); }
	/** \brief
	Removes the forage eaten from the field
	\param [in] a_forage The forage to be removed in Kj
	\param [in] a_maxintakesource The source of the max intake
	\param [in] m_myForageIndex Temporary storage for a forage location index
	*/
	void RemoveMaxForageKj(double a_forage, TTypeOfMaxIntakeSource a_maxintakesource, int m_myForageIndex);
	/** \brief
	Returns the number of birds at a forage location - given by a poly ref
	*/
	int GetBirdsAtForageLoc(int a_index, GooseSpeciesType a_type) {
		return m_GooseForageLocations[a_index].GetBirds(a_type);
	}
	/** \brief
	Returns the number of birds at a forage location - given by a poly ref
	*/
	int BirdsToShootAtPoly(int a_poly /*, double & a_protectedpct */);
	/** \brief
	Passes the message to shoot a number of birds at a forage location.
	*/
	void BirdsShot(int a_polyref, int a_numbershot, GooseHunter* a_Hunter);
	/** \brief
	Passes a 'Bang' message to birds near to the location specified by the polygon reference.
	*/
	void BangAtPoly(int a_polyref);

	/** \brief Asks for a pointer to a goose that can be followed. */
	Goose_Base* GetLeader(APoint a_homeloc, GooseSpecies a_species);

	/** \brief
	Changes a_x & a_y to the location of the nearest roost of a_type to a_x, a_y
	*/
	void FindClosestRoost(int &a_x, int &a_y, unsigned a_type);
	/** \brief Returns the distance in to the nearest roost of a_type in meters	*/
	double GetDistToClosestRoost(int a_x, int a_y, unsigned a_type);
	/** \brief Get daily thermal costs const */
	double GetThermalCosts(GooseSpecies a_goose) { return m_thermalcosts[a_goose]; }
	/** \brief Is a list of active goose forage locations where we have geese */
	std::vector<GooseActiveForageLocation>m_GooseForageLocations;
	/** \brief Get the number of forage locations*/
	int GetNumberOfForageLocs(void) { return int(m_GooseForageLocations.size()); }
	/** \brief Get a forage location for my species (picked randomly among the active locations)*/
	int GetForageLocIndex(GooseSpecies a_species, int a_x, int a_y);
	/** \brief Function to be able to draw randomly from predefined distributions */
	Variate_gen* m_variate_generator;
	/** \brief 	Record a forage location count */
	void RecordIndForageLoc(double a_count, int a_groupsize, GooseSpecies a_species);
	/** \brief 	Record the weight */
	void RecordWeight(double a_weight, GooseSpecies a_species);
	/** \brief 	Record the habitat use */
	void RecordHabitatUse(int a_habitatype, GooseSpecies a_species, int a_count);
	/** \brief 	Record the habitat use */
	void RecordHabitatUseFieldObs(int a_habitatype, GooseSpecies a_species, int a_count);
	/** \brief 	Record the time spent foraging */
	void RecordForagingTime(int a_time, GooseSpecies a_species);
	/** \brief 	Record the flight distance */
	void RecordFlightDistance(int a_distance, GooseSpecies a_species);
	/** \brief 	Record the daily energy budget */
	void RecordDailyEnergyBudget(int a_deb, GooseSpecies a_species);
	/** \brief 	Record the daily energy balance */
	void RecordDailyEnergyBalance(int a_balance, GooseSpecies a_species);
	/** \brief  Record the state */
	void RecordState();
	/** \brief 	Record the reason for leaving */
	void RecordLeaveReason(TTypeOfLeaveReason a_leavereason, GooseSpeciesType a_speciestype);
protected:
	// Attributes
		/** \brief Holds the time of day. Note that sunrise is at m_daytime == 0.*/
	int m_daytime;
	/** \brief Flag for in daylight hours. Sunrise is always at m_daytime == 0. */
	bool m_daylight;
	/** \brief Number of daylight minutes left */
	int m_daylightleft;
	/** \brief Temporary storage for daily goose energetic thermal costs constant for each species */
	double m_thermalcosts[3];
	/** \brief The observed distribution of young for pink feet*/
	std::vector<int> m_youngdist;
	/** \brief Storage for goose numbers. Used when birds are immigrating */
	int m_migrationnumbers[3][2];
	/** \brief Flag to indicate if we are in the pinkfoot hunting season? */
	bool m_PinkfootSeason;
	/** \brief Flag to indicate if we are in the greylag hunting season*/
	bool m_GreylagSeason;
	/**	\brief	The list of roosts	*/
	std::vector<roostlist> m_roosts;
	/** \brief
	Speed optimisation to hold all potential forage rates in the range 0-X grain density per m2
	*/
	HollingsDiscCurveClass* m_IntakeRateVSGrainDensity_PF;
	/** \brief
	Speed optimisation to hold all potential forage rates in the range 0-X maize density per m2 @todo fix
	*/
	HollingsDiscCurveClass* m_IntakeRateVSMaizeDensity_BN;
	/** \brief
	Speed optimisation to hold all potential competition reductions in the range 0 - 1 goose/m2
	*/
	//GompertzCurveClass* m_ForageRateVSGooseDensity;
	/** \brief
	Speed optimisation to hold all potential feeding time increases as a function of flock size
	*/
	PettiforFeedingTimeCurveClass* m_ForageRateVSGooseDensity;
	/** \brief
	Pointer to an output file for goose population data
	*/
	ofstream* m_GoosePopDataFile;
	/** \brief
	Pointer to an output file for goose energetics data
	*/
	ofstream* m_GooseEnergeticsDataFile;
	/** \brief
	Pointer to an output file for goose habitat use data
	*/
	ofstream* m_GooseHabitatUseFile;
	/** \brief
	Pointer to an output file for goose habitat use data, field observation mimic version
	*/
	ofstream* m_GooseHabitatUseFieldObsFile;
	/** \brief
	Pointer to an output file for goose individual forage location count data
	*/
	ofstream* m_GooseIndLocCountFile;
	/** \brief
	Pointer to an output file for goose weight stats data
	*/
	ofstream* m_GooseWeightStatsFile;
	/** \brief
	Pointer to an output file for goose leave reason stats data
	*/
	ofstream* m_GooseLeaveReasonStatsFile;
	/**	\brief
	Pointer to an output file for goose field forage data
	*/
	ofstream* m_GooseFieldForageDataFile;
	/** \brief Pointer to an output file for almass version */
	ofstream* m_GooseGitVersionFile;
	/**	\brief
	Data for the habitat use
	*/
		int m_HabitatUseStats[gs_foobar * tomis_foobar];
	/**	\brief
	Data for the habitat use, field observation mimic version
	*/
	int m_HabitatUseFieldObsStats[gs_foobar * tomis_foobar];
	/**	\brief
	Statistics for the number of forage locations visited per goose of the population
	*/
	SimpleStatistics m_IndividualForageLocationData[gs_foobar];
	/**	\brief
	Statistics for the weights of the population
	*/
	SimpleStatistics m_WeightStats[gs_foobar];
	/**	\brief
	Statistics for the time spent foraging for the population
	*/
	SimpleStatistics m_ForagingTimeStats[gs_foobar];
	/**	\brief
	Statistics for the flight distances in the population
	*/
	SimpleStatistics m_FlightDistanceStats[gs_foobar];
	/**	\brief
	Statistics for the daily energy budget in the population
	*/
	SimpleStatistics m_DailyEnergyBudgetStats[gs_foobar];
	/**	\brief
	Statistics for the daily energy balance in the population
	*/
	SimpleStatistics m_DailyEnergyBalanceStats[gs_foobar];
	/** \brief	Pointer to an output file for state stats data */
	ofstream* m_StateStatsFile;
	/**	\brief Debugging code. Statistics for the number of times a state method is called */
	SimpleStatistics m_StateStats;
	/**	\brief
	Statistics for reasons for leaving the simulation
	*/
	SimpleStatistics m_LeaveReasonStats[gst_foobar][tolr_foobar];
	/**	\brief
	AOR Probe for pinkfeet
	*/
	AOR_Probe_Goose * m_AOR_Pinkfeet;
	/**	\brief
	AOR Probe for barnacles
	*/
	AOR_Probe_Goose * m_AOR_Barnacles;
	/**	\brief
	AOR Probe for greylags
	*/
	AOR_Probe_Goose * m_AOR_Greylags;
	/** \brief
	Pointer to an output file for goose x y data
	*/
	ofstream* m_GooseXYDumpFile;

	// Methods
		/** \brief Called upon initialization of the simulation. Sets up output files. */
	virtual void Init(void);
	/** \brief  Things to do before anything else at the start of a timestep
	*/
	virtual void DoFirst(void);
	/** \brief Things to do before the Step
	*/
	virtual void DoBefore() { ; }
	/** \brief
	Things to do before the EndStep
	*/
	virtual void DoAfter() { ; }
	/** \brief
	Things to do after the EndStep
	*/
	virtual void DoLast();
	/** \brief
	Controls immigration to the Danish simulation area
	*/
	virtual void DoImmigration();
	/** \brief
	Controls emigration from the Danish simulation area
	*/
	virtual void DoEmigration();
	/** \brief
	Produces output to a standard file describing the state of the goose populations.
	*/
	void GoosePopulationDescriptionOutput();
	/** \brief
	Produces output to a standard file describing the energetic state of all individuals of the goose populations.
	*/
	void GooseFieldForageInfoOutput();
	/** \brief
	Produces output to a standard file describing the number of birds foraging at each field and the field conditions
	*/
	void GooseEnergyRecordOutput();
	/** \brief
	Outputs simple stats for the weights in the population
	*/
	void GooseWeightStatOutput();
	/** \brief
	Outputs simple stats for the number of forage locations visited per goose
	*/
	void GooseIndLocCountOutput();
	/** \brief
	Outputs simple stats for the goose habitat use
	*/
	void GooseHabitatUseOutput();
	/** \brief
	Outputs simple stats for the goose habitat use but using rules to mimic field observations
	*/
	void GooseHabitatUseFieldObsOutput();
	/** \brief
	Clear simple stats for forage location counts
	*/
	void ClearIndLocCountStats();
	/** \brief
	Clear simple stats for the weights in the population
	*/
	void ClearGooseWeightStats();
	/** \brief
	Clear simple stats for habitat use
	*/
	void ClearGooseHabitatUseStats();
	/** \brief
	Clear simple stats for field obs habitat use
	*/
	void ClearGooseHabitatUseFieldObsStats();
	/** \brief
	Clear simple stats for foraging times in the population
	*/
	void ClearGooseForagingTimeStats();
	/** \brief
	Clear simple stats for flight distances in the population
	*/
	void ClearGooseFlightDistanceStats();
	/** \brief
	Clear simple stats for daily energy budget in the population
	*/
	void ClearGooseDailyEnergyBudgetStats();
	/** \brief
	Clear simple stats for daily energy balance in the population
	*/
	void ClearGooseDailyEnergyBalanceStats();
	/** \brief  Clear simple stats for the states */
	void ClearStateStats();
	/** \brief  Write simple stats for the states */
	void StateStatOutput();
	/** \brief
	Outputs simple stats for the reasons for leaving the simulation
	*/
	void GooseLeaveReasonStatOutput();
	/** \brief
	Clear simple stats for the reasons for leaving the simulation
	*/
	void ClearGooseLeaveReasonStats();
	/** \brief
	Writes a file with the openness scores on places where geese have been observed in the field
	*/
	void ObservedOpennessQuery();
	/** \brief
	Writes a file the values of the config variables used
	*/
	void WriteConfig();
	/** \brief
	Get the numbers to immigrate.
	*/
	void GetImmigrationNumbers(GooseSpecies a_goose, bool a_season);
	/** \brief 	Are we in the hunting season? */
	bool InHuntingSeason(int a_day, GooseSpecies a_species);
	/** \brief 	Handy function for writing headers */
	void WriteHeaders(ofstream *a_file, std::vector<std::string> a_headers);
	/** \brief 	The modified goose version of the standard output for creating AOR statistics */
	virtual void TheAOROutputProbe();
	/** \brief 	Does nothing, but we need it here because it automatically called by the Population_Manager if the NWordOutput is used */
	virtual void TheRipleysOutputProbe(FILE* /*a_prb*/){ ; };
	/** \brief 	Does nothing, but we need it here because it automatically called by the Population_Manager if the NWordOutput is used */
	virtual void CloseTheRipleysOutputProbe() { ; }
	/** \brief 	Does nothing, but we need it here because it automatically called by the Population_Manager if the NWordOutput is used */
	virtual void CloseTheReallyBigOutputProbe() { ; }
	/** \brief
	Outputs x y data 
	*/
	void XYDump();
	/** \brief 	Translates gst enum to string  */
	std::string GooseTypeToString(GooseSpeciesType a_gst);
	/** \brief 	Translates gs enum to string  */
	std::string GooseToString(GooseSpecies a_gs);
	/** \brief 	Translates tomis enum to string  */
	std::string IntakeSourceToString(TTypeOfMaxIntakeSource a_intake_source);
	/** \brief 	Translates tolr enum to string  */
	std::string LeaveReasonToString(TTypeOfLeaveReason a_leave_reason);
};

#endif
