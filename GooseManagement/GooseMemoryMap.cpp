//
/**
\file GooseMemoryMap.cpp C++ \brief code file for the Goose Memory Map and associated classes.
*/

#include <cmath>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <string>
#include "../Landscape/ls.h"
#include "GooseMemoryMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "Goose_Base.h"

using namespace std;

/** /brief The rate at which memory of a threat decays */
static CfgFloat cfg_goose_mem_threatdecayrate("GOOSE_MEM_THREATDECAYRATE", CFG_CUSTOM, 0.1);
/** /brief Threshold value for when to delete objects stored in the goose's memory, here after 30 days */
static CfgInt cfg_goose_mem_minmemoryvalue("GOOSE_MEM_MINMEMVALUE", CFG_CUSTOM, 30);
/** /brief The expected foraging time when evaluating cost benefit of flying to a location */
static CfgInt cfg_goose_mem_expectedforagingtime("GOOSE_MEM_EXPECTEDFORAGINGTIME", CFG_CUSTOM, 120);
int GooseMemoryLocation::m_infinitememorystop = 0;  // Initialize static variable

/** \brief This variable provides access the to the internal ALMaSS math functions */
extern ALMaSS_MathFuncs g_AlmassMathFuncs;

//*******************************************************************************************************
/** /brief Function to figure out if a particular memory should be deleted from the goose's memory */
bool IsMarkedToDelete(GooseMemoryLocation & o)
{
	return o.ShouldDelete();
}

//*******************************************************************************************************

GooseMemoryMap::GooseMemoryMap(Goose_Base* a_owner)
{
	m_myOwner = a_owner;
	m_threatdecayrate = cfg_goose_mem_threatdecayrate.value();
	m_expectedforagingtime = cfg_goose_mem_expectedforagingtime.value();
	GooseMemoryLocation aloc;
	aloc.m_infinitememorystop = cfg_goose_mem_minmemoryvalue.value(); // Set the static variable
	//m_memorylocations.reserve(200); // there are usually 200-300 memories, reserving this in advance does not hurt, and speeds the process when new elements are added (a bit).
}


void GooseMemoryMap::MemAdd(GooseMemoryLocation a_gml)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list, just stop when we find one bigger and insert
	* before it.\n
	* However, perhaps we have been here before, so just in case make a test and quitely update the memory if we find this polygon id in the list.\n
	* If no polygon ids are bigger or equal to ours then we need to add ours to the end of the list.
	*/
	if (m_memorylocations.size() == 0)
	{
		m_memorylocations.push_back(a_gml);
		return;
	}
	int polyid = a_gml.m_polygonid;
	std::vector<GooseMemoryLocation>::iterator ci;
	for (ci = m_memorylocations.begin(); ci != m_memorylocations.end(); ++ci)
	{
		if (ci->m_polygonid>polyid)
		{
			m_memorylocations.insert(ci, a_gml);
			return;
		}
		if (ci->m_polygonid == polyid)
		{
			// We have been here before so update memory
			ci->m_grain = a_gml.m_grain;
			ci->m_maize = a_gml.m_maize;
			ci->m_grazing = a_gml.m_grazing;
			ci->m_foodresource = a_gml.m_foodresource;
			return;
		}
		// the only possibility left is that polyid is > than any on our list, so push it to the end
	}
	m_memorylocations.push_back(a_gml);
}
//-------------------------------------------------------------------------------------------------------

bool GooseMemoryMap::MemDel(int a_polyid)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list, just stop when we find the right one and
	* remove it
	*/
	for (std::vector<GooseMemoryLocation>::iterator ci = m_memorylocations.begin(); ci != m_memorylocations.end(); ++ci)
	{
		if (ci->m_polygonid == a_polyid)
		{
			m_memorylocations.erase(ci);
			return true;
		}
	}
	return false;
}
//-------------------------------------------------------------------------------------------------------

bool GooseMemoryMap::ChangeAddFoodRes(int a_polyid, double a_foodres)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list, just stop when we find one bigger and add the food resource to be added
	*/
	std::vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); ++it)
	{
		if (it->m_polygonid == a_polyid)
		{
			it->m_foodresource += a_foodres;
			return true; // all good
		}
	}
	return false; // entry not found
}
//-------------------------------------------------------------------------------------------------------

bool GooseMemoryMap::ChangeAddThreat(int a_polyid, double a_threat)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list, just stop when we find one bigger and add the threat to be added
	*/
	std::vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); ++it)
	{
		if (it->m_polygonid == a_polyid)
		{
			it->m_threat += a_threat;
			return true; // all good
		}
	}
	return false; // entry not found
}
//-------------------------------------------------------------------------------------------------------

bool GooseMemoryMap::ChangeMultFoodRes(int a_polyid, double a_foodres)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list, just stop when we find one bigger and multiply by the value passed
	*/
	std::vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); ++it)
	{
		if (it->m_polygonid == a_polyid)
		{
			it->m_foodresource *= a_foodres;
			return true; // all good
		}
	}
	return false; // entry not found
}
//-------------------------------------------------------------------------------------------------------

bool GooseMemoryMap::ChangeSetFoodRes(int a_polyid, double a_grain, double a_maize, double a_grazing)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list,
	* just stop when we find our polygon.
	*/
	/** @todo Speed-up Identified as the program bottleneck. 
	* The problem is that in most cases this is called we run all the way to the end of the vector and don't find our polygon. */
	std::vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); ++it)
	{
		if (it->m_polygonid == a_polyid)
		{
			it->m_grain = a_grain;
			it->m_maize = a_maize;
			it->m_grazing = a_grazing;
			it->m_foodresource = m_myOwner->GetMaxIntakeRate(a_grain, a_maize, a_grazing);
			APoint roost = m_myOwner->GetRoost();
			int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( roost.m_x, roost.m_y, it->m_x, it->m_y );
			it->m_score = CalcScore(dist, it->m_foodresource, it->m_threat);
			return true; // all good
		}
	}
	return false; // entry not found
}
//-------------------------------------------------------------------------------------------------------


bool GooseMemoryMap::ChangeMultThreat(int a_polyid, double a_threat)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list, just stop when we find one bigger and multiply by the value passed
	*/
	std::vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); ++it)
	{
		if (it->m_polygonid == a_polyid)
		{
			it->m_threat *= a_threat;
			return true; // all good
		}
	}
	return false; // entry not found
}
//-------------------------------------------------------------------------------------------------------


double GooseMemoryMap::GetFoodRes(int a_polyid)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list.
	*/
	std::vector<GooseMemoryLocation>::const_iterator ci;
	for (ci = m_memorylocations.begin(); ci != m_memorylocations.end(); ++ci)
	{
		if (ci->m_polygonid == a_polyid) return ci->m_foodresource;
	}
	GooseMemoryError("Polygonid not found", a_polyid);
	return 0; // compiler happiness
}
//-------------------------------------------------------------------------------------------------------

double GooseMemoryMap::GetThreat(int a_polyid)
{
	/**
	* Here we make use of the fact that there should only be one of each entry, so no need to read the whole list.
	*/
	std::vector<GooseMemoryLocation>::const_iterator ci;
	for (ci = m_memorylocations.begin(); ci != m_memorylocations.end(); ++ci)
	{
		if (ci->m_polygonid == a_polyid) return ci->m_threat;
	}
	GooseMemoryError("Polygonid not found", a_polyid);
	return 0; // compiler happiness
}
//-------------------------------------------------------------------------------------------------------

GooseMemoryLocation GooseMemoryMap::GetBestFeedingScore()
{
	/**
	* 0 - If there are no memories return an impossible score to trigger explore
	* 1 - Loop through all locations.
	* 2 - Keep track of the best score.
	* 3 - We need a way out if nothing is at all suitable, so if score is too small return a dummy location
	*	  with a big negative score.
	* 4 - Return the best scoring location if we have one.
	*/
	double score = -9999.0;
	if (m_memorylocations.size()<1)
	{
		GooseMemoryLocation gml;
		gml.m_score = score;
		return gml;
	}

	std::vector<GooseMemoryLocation>::iterator ci, ci_best;
	for (ci = m_memorylocations.begin(); ci != m_memorylocations.end(); ++ci)
	{
		if (ci->m_score>score)
		{
			ci_best = ci;
			score = ci->m_score;
		}
	}
	if (score < 1)
	{
		GooseMemoryLocation gml;
		gml.m_score = -9999;
		return gml;
	}
	return *(ci_best);
}
//-------------------------------------------------------------------------------------------------------

double GooseMemoryMap::CalcScore(int a_dist, double a_foodresource, double a_threat)
{
		double cost = a_dist * m_myOwner->GetFlightCost();
		double score = (a_foodresource * m_expectedforagingtime) - cost;
		score = score * (1 - a_threat);
		return score;
}
//-------------------------------------------------------------------------------------------------------

bool GooseMemoryMap::IsKnownArea(int a_polyid)
{
	/** Loop through all locations and see if we have this polygon */
	std::vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); ++it)
	{
		/** If we have it, return true, otherwise return false. */
		if (it->m_polygonid == a_polyid) return true;
	}
	return false;
}
//-------------------------------------------------------------------------------------------------------

void GooseMemoryMap::DecayAllMemory(void)
{
	/**
	* 1 - Loop through all locations
	* 2 - Decay the memory
	* 3 - Update the memory score
	* 4 - Remove any memories that are old
	*/

	vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); it++)
	{
		// 2 - Decay and age the memory 
		it->m_threat *= m_threatdecayrate;
		it->m_age++;
		// 3. Update the memory score
		int rx = m_myOwner->GetRoost().m_x;
		int ry = m_myOwner->GetRoost().m_y;
		int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( rx, ry, it->m_x, it->m_y );
		double flightcost = m_myOwner->GetFlightCost();
		it->CalcScore(dist, flightcost, m_expectedforagingtime);
	}
	// 4. Remove memories too old
	m_memorylocations.erase(remove_if(m_memorylocations.begin(), m_memorylocations.end(), IsMarkedToDelete), m_memorylocations.end());
}

//-------------------------------------------------------------------------------------------------------

double GooseMemoryMap::GetTotalThreats(void)
{
	double theThreat = 0.0;
	/** 1 - Loop through all locations. */
	std::vector<GooseMemoryLocation>::iterator it;
	for (it = m_memorylocations.begin(); it != m_memorylocations.end(); ++it)
	{
		/** 2 - Sum the threats and return this. */
		theThreat += it->m_threat;
	}
	return theThreat;
}
void GooseMemoryMap::ClearAllMemory(void)
{
	m_memorylocations.erase(m_memorylocations.begin(), m_memorylocations.end());
}
//-------------------------------------------------------------------------------------------------------

void GooseMemoryMap::GooseMemoryError(std::string a_str, int a_value)
{
	/** Opens a file called GooseMemoryMapError.txt and prints an error message before dumping the contents of the current memory. The program then closes. */
	std::ofstream ofile("GooseMemoryMapError.txt", std::ios::app | std::ios::out);
	ofile << a_str << ':' << a_value << std::endl;
	// Dumping goose memory
	std::vector<GooseMemoryLocation>::iterator ci;
	ofile << "polygonid" << '\t' << "m_x" << '\t' << "m_y" << '\t' << "foodresource" << '\t' << "threat" << std::endl;
	for (ci = m_memorylocations.begin(); ci != m_memorylocations.end(); ++ci)
	{
		ofile << ci->m_polygonid << '\t' << ci->m_x << '\t' << ci->m_y << '\t' << ci->m_foodresource << '\t' << ci->m_threat << std::endl;
	}
	ofile.close();
	exit(9);
}
//-------------------------------------------------------------------------------------------------------
