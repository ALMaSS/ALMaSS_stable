/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Goose_Population_Manager.cpp
\brief <B>The source code for the goose population manager and associated classes</B>
*/
/**  \file Goose_Population_Manager.cpp
Version of  8th February 2013 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../Hunters/Hunters_all.h"
#include "../GooseManagement/Goose_Population_Manager.h"
#include "GoosePinkFooted_All.h"
#include "GooseGreylag_All.h"
#include "GooseBarnacle_All.h"

#include "../BatchALMaSS/BoostRandomGenerators.h"
#include <boost/io/ios_state.hpp>

/** \brief A random number generator (0-1) */
extern boost::variate_generator<base_generator_type &, boost::uniform_real<>> g_rand_uni;
/** \brief A random number generator (0-9999) */
extern boost::variate_generator<base_generator_type &, boost::uniform_int<>> g_rand_uni2;
/** \brief This pointer provides access the to landscape module */
extern Landscape* g_land;
/** \brief This variable provides access the to the internal ALMaSS math functions */
extern ALMaSS_MathFuncs g_AlmassMathFuncs;
/** \brief This variable controls how long the simulation will run for */
extern int g_torun;
/** \brief This config variable controls whether we should make AOR output */
extern CfgBool cfg_AOROutput_used;


/** \brief Scales the growth of vegetation - max value */
extern CfgFloat cfg_PermanentVegGrowthMaxScaler;
/** \brief Scales the growth of vegetation - min value */
extern CfgFloat cfg_PermanentVegGrowthMinScaler;
/** \brief Config to control if the Ripley probe is used or not */
extern CfgBool cfg_ReallyBigOutput_used;
/** \brief Should git version info be printed to file and console?*/
extern CfgBool l_map_print_git_version_info;

//---------------------------------------------------------------------------------------

/** \brief The likelyhood that a goose will follow another goose when going foraging - barnacle goose */
static CfgInt cfg_goose_bn_followinglikelyhood( "GOOSE_FOLLOWINGLIKELYHOOD_BN", CFG_CUSTOM, 0, 0, 10000);
/** \brief The likelyhood that a goose will follow another goose when going foraging - pinkfooted goose */
static CfgInt cfg_goose_pf_followinglikelyhood("GOOSE_FOLLOWINGLIKELYHOOD_PF", CFG_CUSTOM, 0, 0, 10000);
/** \brief The likelyhood that a goose will follow another goose when going foraging - graylag goose */
static CfgInt cfg_goose_gl_followinglikelyhood("GOOSE_FOLLOWINGLIKELYHOOD_GL", CFG_CUSTOM, 0, 0, 10000);
/** \brief Lean weight of pinkfoot geese */
/** Based on male weights from Norway (Trøndelag) Gundersen et al Submitted 2016.
    assuming a lean of 90% of the measured mean weight on day 265. */
CfgFloat cfg_goose_pf_baseweight("GOOSE_PINKFOOTWEIGHT", CFG_CUSTOM, 2307, 2000, 3000);  /**@todo Currently male weight. Should Male, Female, Juvs not have different weights? */
/** \brief Lean weight of barnacle geese */
CfgFloat cfg_goose_bn_baseweight("GOOSE_BARNACLEWEIGHT", CFG_CUSTOM, 1708, 1500, 2000);  /**@todo Male*/
/** \brief Lean weight of greylag geese */
/** @todo From Leif Nilsson (male weight) */
CfgFloat cfg_goose_gl_baseweight( "GOOSE_GREYLAGWEIGHT", CFG_CUSTOM, 2795, 2500, 3000);
/** \brief the cost of flight in kJ per m per gram */
/** Calculated as 12 * BMR to get cost of flight. We devide this by 24 to per hour. 
* this is then devided by an average flight speed of 57.6 km/h (Green & Alerstam 2000 J Avian Biol 31:215-225)
* This gives a cost in kJ/km which is divided by 1000 to meters and mass to get to per gram. */
static CfgFloat cfg_goose_flightcost( "GOOSE_FLIGHTCOST", CFG_CUSTOM, 0.000002079, 0, 1);
/** \brief The maximum forage range in m for pink-footed geese */
CfgFloat cfg_goose_pf_ForageDist("GOOSE_FORAGEDIST_PF", CFG_CUSTOM, 35000, 1000, 35000);
/** \brief The maximum forage range in m for barnacle geese */
CfgFloat cfg_goose_bn_ForageDist("GOOSE_FORAGEDIST_BN", CFG_CUSTOM, 35000, 1000, 35000);
/** \brief The maximum forage range when already at a forage location for pink-footed geese (m) */
CfgFloat cfg_goose_pf_FieldForageDist("GOOSE_FIELDFORAGEDIST_PF", CFG_CUSTOM, 5000, 1000, 35000);
/** \brief The maximum forage range when already at a forage location for barnacle geese (m) */
CfgFloat cfg_goose_bn_FieldForageDist("GOOSE_FIELDFORAGEDIST_BN", CFG_CUSTOM, 5000, 1000, 35000);
/** \brief The maximum forage range when already at a forage location for greylag geese (m) */
CfgFloat cfg_goose_gl_FieldForageDist("GOOSE_FIELDFORAGEDIST_GL", CFG_CUSTOM, 5000, 1000, 35000);
/** \brief The maximum forage range in m for greylag geese */
CfgFloat cfg_goose_gl_ForageDist("GOOSE_FORAGEDIST_GL", CFG_CUSTOM, 35000, 1000, 35000);
/** \brief The cost of BMR per kg goose - constant 1*/
CfgFloat cfg_goose_BMRconstant1("GOOSE_BMRCONSTANTA", CFG_CUSTOM, 4.19*78.30, 1, 500);
/** \brief The cost of BMR per kg goose - constant 2 */
CfgFloat cfg_goose_BMRconstant2("GOOSE_BMRCONSTANTB", CFG_CUSTOM, 0.72, 0, 1);
/** \brief Pinkfooted goose lower critical temperature for calculating thermal regulation cost. Constant 1 */
/** Calculated from Kendeigh 1977 based on body weight 2524g */
CfgFloat cfg_goose_pf_Thermalconstant("GOOSE_THERMALCONSTANTA_PF", CFG_CUSTOM, 11.4, 1, 25);
/** \brief Barnacle goose lower critical temperature for calculating thermal regulation cost. Constant 1 */
/** Calculated from Kendeigh 1977 based on body weight 1880g */
CfgFloat cfg_goose_bn_Thermalconstant( "GOOSE_THERMALCONSTANTA_BN", CFG_CUSTOM, 12.1, 1, 25);
/** \brief Greylag goose lower critical temperature for calculating thermal regulation cost. Constant 1 */
/** Calculated from Kendeigh 1977 based on body weight 3454g */
CfgFloat cfg_goose_gl_Thermalconstant( "GOOSE_THERMALCONSTANTA_GL", CFG_CUSTOM, 10.8, 1, 25);
/** \brief Thermal regulation cost Constant b */
/** The 1.272 is from Lefebvre & Raveling 1967. Journal of Wildlife Management 31(3): 538-546*/
CfgFloat cfg_goose_Thermalconstantb("GOOSE_THERMALCONSTANTB", CFG_CUSTOM, 1.272 * 24, 1, 100); // kJ per day
/** \brief The maximum a goose is allowed to eat in a day as a scaling of BMR */
/** Calculated from maximum daily food intake and adjusted for energy assimilation and content in winter wheat.
Therkildsen & Madsen 2000, Wildlife Biology 6: 167-172.*/
CfgFloat cfg_goose_MaxAppetiteScaler("GOOSE_MAXAPPETITESCALER",CFG_CUSTOM, 4.82, 1, 10);
/** \brief The maximum proportion of weight that can be stored as energy reserves */
/** Calculated based on observed API scores weights and lean weight*/
CfgFloat cfg_goose_MaxEnergyReserveProportion("GOOSE_MAXENERGYRESERVEPROPORTION",CFG_CUSTOM, 0.27, 0.1, 1);
/** \brief The initial proportion of weight that can be stored as energy reserves */
CfgFloat cfg_goose_InitialEnergyReserveProportion("GOOSE_INITIALENERGYRESERVEPROPORTION", CFG_CUSTOM, 0.1, 0.0, 1);
/** \brief The energy content of fat*/
CfgFloat cfg_goose_EnergyContentOfFat("GOOSE_ENERGYCONTENTOFFAT",CFG_CUSTOM, 39.8, 1, 100);
/** \brief The metabolic costs of converting tissue to energy or vice versa  */
CfgFloat cfg_goose_MetabolicConversionCosts("GOOSE_METABOLICCONVCOSTS",CFG_CUSTOM, 11.4, 1, 100); 
/** \brief The minimum openness score that a goose will tolerate for foraging */
/** Distance to closests landscape feature avoided by geese measured from the centre of the polygon. */
CfgFloat cfg_goose_MinForageOpenness( "GOOSE_MINFORAGEOPENNESS", CFG_CUSTOM, 100.0, 10, 10000);
/** \brief The maximum distance in m that a goose can hear a scary bang */
/** From Gitte Jensen unpubl. */
CfgInt cfg_goose_MaxScareDistance("GOOSE_MAXSCAREDIST",CFG_CUSTOM, 500, 1, 5000);
/** \brief The 5-day mean of body condition which triggers the bird to leave */
/** Bodycondition = total weight/lean weight */
static CfgFloat cfg_goose_LeavingThreshold("GOOSE_LEAVINGTHRESHOLD", CFG_CUSTOM, 1.0, 1.0, 5.0);
/** \brief The number of minutes that geese will be foraging after sunset */
CfgInt cfg_goose_AfterDarkTime( "GOOSE_AFTERDARKTIME", CFG_CUSTOM, 30, 0, 180 );
/** \brief To calculate daily energy budget. This is the multiple of BMR spent during daytime
* Default is based on Madsen 1985 ("Relations between change in spring habitat selection...")
*/
CfgFloat cfg_goose_daytime_BMR_multiplier("GOOSE_DAYTIMEBMRMULTIPLIER", CFG_CUSTOM, 1.65, 1.0, 10.0);
/** \brief To calculate daily energy budget. This is the multiple of BMR spent during nighttime */
CfgFloat cfg_goose_nighttime_BMR_multiplier("GOOSE_NIGHTTIMEBMRMULTIPLIER", CFG_CUSTOM, 1.3, 1.0, 10.0);
/** \brief Chance of changing roost */
CfgFloat cfg_goose_roostchangechance("GOOSE_ROOSTCHANGECHANCE", CFG_CUSTOM, 0.0, 0.0, 1.0); // Default is no change of roost



/** \brief Input variable to control exit of the goose model
The geese leave the simulation in spring, so this variable is used to hard exit the
model once they have all left. */
CfgInt cfg_goose_ModelExitDay( "GOOSE_MODELEXITDAY", CFG_CUSTOM, 9999, 1, 100000 );
/** \brief The day where all grain across the landscape is reset. */
CfgInt cfg_goose_grain_and_maize_reset_day("GOOSE_GRAINANDMAIZERESETDAY", CFG_CUSTOM, 59, 0, 364);

/** \brief The time in minutes (multiple of ten) from sunrise when the habitat use is recorded for geese */
static CfgInt cfg_gooseHabitatUsetime("GOOSE_HABUSERECORDTIME", CFG_CUSTOM, 120, 0, 12 * 60);
/** \brief The time in minutes (multiple of ten) from sunrise when the AOR probe is recorded for geese */
static CfgInt cfg_gooseAORtime("GOOSE_AORRECORDTIME", CFG_CUSTOM, 120, 0, 12 * 60);
/** \brief The cfg variable determining whether to print goose population stats */
static CfgBool cfg_goose_PopulationDescriptionON( "GOOSE_POPULATIONDESCRIPTIONON", CFG_CUSTOM, true );
/** \brief Should we record goose energetics? */
static CfgBool cfg_goose_EnergyRecord("GOOSE_ENERGYRECORD", CFG_CUSTOM, false);
/** \brief Should we record gooseindividual forage location count statistics? */
static CfgBool cfg_goose_IndLocCounts("GOOSE_INDLOCCOUNTSTATS", CFG_CUSTOM, false);
/** \brief Should we record goose weight statistics? */
static CfgBool cfg_goose_WeightStats("GOOSE_WEIGHTSTATS", CFG_CUSTOM, false);
/** \brief  Should we record state statistics? */
static CfgBool cfg_goose_StateStats("GOOSE_STATESTATS", CFG_CUSTOM, false);
/** \brief Should we record goose leaving reason statistics? */
static CfgBool cfg_goose_LeaveReasonStats("GOOSE_LEAVEREASONSTATS", CFG_CUSTOM, false);
/** \brief Should we record goose field forage information? */
static CfgBool cfg_goose_FieldForageInfo( "GOOSE_FIELDFORAGEINFO", CFG_CUSTOM, false );
/** \brief Should we query the map for openness scores on locations where geese have been observed in the field? */
static CfgBool cfg_goose_ObservedOpennessQuery( "GOOSE_OBSERVEDOPENNESS", CFG_CUSTOM, false );
/** \brief Should we write the values of the configs to a file? */
static CfgBool cfg_goose_WriteConfig( "GOOSE_WRITECONFIG", CFG_CUSTOM, true );
/** \brief Should we use stripped down output to optimize run time? */
static CfgBool cfg_goose_runtime_reporting("GOOSE_RUNTIMEREPORTING", CFG_CUSTOM, true);

/** \brief Input: Pink-footed goose start numbers */
CfgInt cfg_goose_pf_startnos("GOOSE_PF_STARTNOS", CFG_CUSTOM, 13440);
/** \brief Input: Pink-footed goose proportion of young at start */
CfgFloat cfg_goose_pf_young_proportion("GOOSE_PF_YOUNG_PROPORTION", CFG_CUSTOM, 0.21, 0.0, 1.0);
// Barnacle geese
/** \brief Input: Barnacle goose start numbers */
CfgInt cfg_goose_bn_startnos("GOOSE_BN_STARTNOS", CFG_CUSTOM, 5600);
/** \brief Input: Barnacle goose proportion of young at start */
CfgFloat cfg_goose_bn_young_proportion("GOOSE_BN_YOUNG_PROPORTION", CFG_CUSTOM, 0.21, 0.0, 1.0);
// Greylag geese
/** \brief Input: Greylag geese start numbers */
CfgInt cfg_goose_gl_startnos("GOOSE_GL_STARTNOS", CFG_CUSTOM, 8960);
/** \brief Input: Greylag goose proportion of young at start */
CfgFloat cfg_goose_gl_young_proportion("GOOSE_GL_YOUNG_PROPORTION", CFG_CUSTOM, 0.21, 0.0, 1.0);

/** \brief Input: Should we simulate a spring migration? */
CfgBool cfg_goose_pf_springmigrate( "GOOSE_PF_SPRING_MIGRATE", CFG_CUSTOM, true );
/** \brief Input: Date for onset of spring migration */
CfgInt cfg_goose_pf_springmigdatestart( "GOOSE_PF_SPRING_MIG_START", CFG_CUSTOM, 7, 1, 365 );
/** \brief Input: Date for end of spring migration */
CfgInt cfg_goose_pf_springmigdateend( "GOOSE_PF_SPRING_MIG_END", CFG_CUSTOM, 59, 1, 365 );
/** \brief Input: Number of pinkfeet to immigrate in spring */
CfgInt cfg_goose_pf_springmignos("GOOSE_PF_SPRING_MIG_NOS", CFG_CUSTOM, 5600);
/** \brief Input: Should we simulate a pinkfoot fall migration? */
CfgBool cfg_goose_pf_fallmigrate( "GOOSE_PF_FALL_MIGRATE", CFG_CUSTOM, true );
/** \brief Input: Date for onset of pinkfoot fall migration */
CfgInt cfg_goose_pf_fallmigdatestart( "GOOSE_PF_FALL_MIGRATION_START", CFG_CUSTOM, 274, 1, 365);
/** \brief Input: Date for end of pinkfoot fall migration */
CfgInt cfg_goose_pf_fallmigdateend( "GOOSE_PF_FALL_MIGRATION_END", CFG_CUSTOM, 304, 1, 365);
/** \brief Input: The pinkfoot fall migration probability */
CfgFloat cfg_goose_pf_fallmig_probability("GOOSE_PF_FALLMIG_PROBABILITY", CFG_CUSTOM, 0.03, 0.0, 1.0);

/** \brief Input: Should we simulate greylag a spring migration? */
CfgBool cfg_goose_gl_springmigrate( "GOOSE_GL_SPRING_MIGRATE", CFG_CUSTOM, true );
/** \brief Input: Date for onset of greylag spring migration */
CfgInt cfg_goose_gl_springmigdatestart( "GOOSE_GL_SPRING_MIG_START", CFG_CUSTOM, 15, 1, 365 );
/** \brief Input: Date for end of greylag spring migration */
CfgInt cfg_goose_gl_springmigdateend( "GOOSE_GL_SPRING_MIG_END", CFG_CUSTOM, 75, 1, 365 );
/** \brief Input: Number of greylag to immigrate in spring */
CfgInt cfg_goose_gl_springmignos("GOOSE_GL_SPRING_MIG_NOS", CFG_CUSTOM, 2240);
/** \brief Input: Should we simulate a greylag fall migration? */
CfgBool cfg_goose_gl_fallmigrate( "GOOSE_GL_FALL_MIGRATE", CFG_CUSTOM, true );
/** \brief Input: Date for onset of greylag fall migration */
CfgInt cfg_goose_gl_fallmigdatestart( "GOOSE_GL_FALL_MIGRATION_START", CFG_CUSTOM, 232, 1, 365 );
/** \brief Input: Date for end of greylag fall migration */
CfgInt cfg_goose_gl_fallmigdateend( "GOOSE_GL_FALL_MIGRATION_END", CFG_CUSTOM, 334, 1, 365 );
/** \brief Input: The greylag fall migration probability */
CfgFloat cfg_goose_gl_fallmig_probability("GOOSE_GL_FALLMIG_PROBABILITY", CFG_CUSTOM, 0.0083, 0.0, 1.0);

/** \brief Input: Should we simulate barnacle a spring migration? */
CfgBool cfg_goose_bn_springmigrate( "GOOSE_BN_SPRING_MIGRATE", CFG_CUSTOM, true );
/** \brief Input: Date for onset of barnacle spring migration */
CfgInt cfg_goose_bn_springmigdatestart( "GOOSE_BN_SPRING_MIG_START", CFG_CUSTOM, 15, 1, 365 );
/** \brief Input: Date for end of barnacle spring migration */
CfgInt cfg_goose_bn_springmigdateend( "GOOSE_BN_SPRING_MIG_END", CFG_CUSTOM, 75, 1, 365 );
/** \brief Input: Number of barnacle to immigrate in spring */
CfgInt cfg_goose_bn_springmignos("GOOSE_BN_SPRING_MIG_NOS", CFG_CUSTOM, 8960);
/** \brief Input: Should we simulate a barnacle fall migration? */
CfgBool cfg_goose_bn_fallmigrate( "GOOSE_BN_FALL_MIGRATE", CFG_CUSTOM, true );
/** \brief Input: Date for onset of barnacle fall migration */
CfgInt cfg_goose_bn_fallmigdatestart( "GOOSE_BN_FALL_MIGRATION_START", CFG_CUSTOM, 318, 1, 365 );
/** \brief Input: Date for end of barnacle fall migration */
CfgInt cfg_goose_bn_fallmigdateend( "GOOSE_BN_FALL_MIGRATION_END", CFG_CUSTOM, 348, 1, 365 );
/** \brief Input: The barnacle fall migration probability */
CfgFloat cfg_goose_bn_fallmig_probability("GOOSE_BN_FALLMIG_PROBABILITY", CFG_CUSTOM, 0.0083, 0.0, 1.0);

/** \brief The sex ratio of pinkfoot non-breeders */
CfgFloat cfg_goose_pf_sexratio("GOOSE_PF_SEXRATIO", CFG_CUSTOM, 0.5, 0.0, 1.0);
/** \brief The sex ratio of barnacle non-breeders */
CfgFloat cfg_goose_bn_sexratio("GOOSE_BN_SEXRATIO", CFG_CUSTOM, 0.5, 0.0, 1.0);
/** \brief The sex ratio of greylag non-breeders */
CfgFloat cfg_goose_gl_sexratio("GOOSE_GL_SEXRATIO", CFG_CUSTOM, 0.5, 0.0, 1.0);

/** \brief Input: The initial starting date for arrival for pink-foots to the simulation area */
CfgInt cfg_goose_pf_arrivedatestart("GOOSE_PF_ARRIVEDATESTART", CFG_CUSTOM, 265);  // 23/09
/** \brief Input: The last date for arrival for pink-foots to the simulation area */
CfgInt cfg_goose_pf_arrivedateend("GOOSE_PF_ARRIVEDATEEND", CFG_CUSTOM, 280); // 08/10
/** \brief Input: The initial starting date for arrival for barnacles to the simulation area */
CfgInt cfg_goose_bn_arrivedatestart("GOOSE_BN_ARRIVEDATESTART", CFG_CUSTOM, 277); // 05/10
/** \brief Input: The last date for arrival for barnacles to the simulation area */
CfgInt cfg_goose_bn_arrivedateend("GOOSE_BN_ARRIVEDATEEND", CFG_CUSTOM, 298); // 26/10
/** \brief Input: The initial starting date for arrival for greylags to the simulation area */
CfgInt cfg_goose_gl_arrivedatestart("GOOSE_GL_ARRIVEDATESTART", CFG_CUSTOM, 212); // 01/08
/** \brief Input: The last date for arrival for greylags to the simulation area */
CfgInt cfg_goose_gl_arrivedateend("GOOSE_GL_ARRIVEDATEEND", CFG_CUSTOM, 231);  // 20/08
/** \brief Input: The initial starting date for pink-foots to the leave simulation area */
CfgInt cfg_goose_pf_leavingdatestart("GOOSE_PF_LEAVINGDATESTART", CFG_CUSTOM, 78); // 20/03
/** \brief Input: The last date for pink-foots to leave the simulation area */
CfgInt cfg_goose_pf_leavingdateend("GOOSE_PF_LEAVINGDATEEND", CFG_CUSTOM, 104);  // 15/04
/** \brief Input: The initial starting date for barnacles to leave the simulation area */
CfgInt cfg_goose_bn_leavingdatestart("GOOSE_BN_LEAVINGDATESTART", CFG_CUSTOM, 90); // 01/04
/** \brief Input: The last date for barnacles to leave the simulation area */
CfgInt cfg_goose_bn_leavingdateend("GOOSE_BN_LEAVINGDATEEND", CFG_CUSTOM, 134); // 15/05
/** \brief Input: The initial starting date for greylags to leave the simulation area */
CfgInt cfg_goose_gl_leavingdatestart("GOOSE_GL_LEAVINGDATESTART", CFG_CUSTOM, 58); // 28/02
/** \brief Input: The last date for greylags to leave the simulation area */
CfgInt cfg_goose_gl_leavingdateend("GOOSE_GL_LEAVINGDATEEND", CFG_CUSTOM, 68); // 10/03

/** \brief The decay rate for spilled grain for Harvest to Spring*/
CfgFloat cfg_goose_GrainDecayRateWinter("GOOSE_GRAINDECAYRATEWINTER", CFG_CUSTOM, 0.9985, 0.0, 1.0);// Halflife of 463 days
/** \brief The decay rate for spilled grain for Spring until 1st July */
CfgFloat cfg_goose_GrainDecayRateSpring("GOOSE_GRAINDECAYRATESPRING", CFG_CUSTOM, 0.95, 0.0, 1.0); // Halflife of approx 17 days
/** \brief The scaler to go from energy intake from grass forage to winter cereal
* The default value of 1.0325 is a quick fix to account for higher energy intake on winter cereal - Based on Therkildsen & Madsen 2000 Energetics of feeding...*/
CfgFloat cfg_goose_grass_to_winter_cereal_scaler("GOOSE_GRASS_TO_WINTER_CEREAL_SCALER", CFG_CUSTOM, 1.0325, 0.0, 10.0);
/** \brief The decay rate for the minimum forage rate*/
CfgFloat cfg_goose_MinForageRateDecayRate("GOOSE_MINFORAGEDECAYRATE", CFG_CUSTOM, 0.72, 0.0, 1.0);
/** \brief The mean for the normal distribution which determines roost leave time */
CfgInt cfg_goose_RoostLeaveDistMean( "GOOSE_ROOSTLEAVEDISTMEAN", CFG_CUSTOM, 30 );
/** \brief The standard deviation for the normal distribution which determines roost leave time */
CfgInt cfg_goose_RoostLeaveDistSD( "GOOSE_ROOSTLEAVEDISTSD", CFG_CUSTOM, 10 );

/** \brief The time when the geese are counted on fields.
We divide the daylight hours by this cfg and add that to the sun up time to get the timing for counting. E.g. a value of 2 will give noon. */
CfgInt cfg_goose_TimedCounts("GOOSE_TIMEDCOUNTS", CFG_CUSTOM, 2, 1, 12);

/** \brief The start of the pinkfoot hunting season */
 CfgInt cfg_goose_pinkfootopenseasonstart("GOOSE_PF_OPENSEASONSTART", CFG_CUSTOM, 243);
/** \brief The end of the pinkfoot hunting season */
 CfgInt cfg_goose_pinkfootopenseasonend("GOOSE_PF_OPENSEASONEND", CFG_CUSTOM, 364);
/** \brief The start of the greylag hunting season */
 CfgInt cfg_goose_greylagopenseasonstart("GOOSE_GL_OPENSEASONSTART", CFG_CUSTOM, 243);
/** \brief The end of the greylag hunting season */
 CfgInt cfg_goose_greylagopenseasonend("GOOSE_GL_OPENSEASONEND", CFG_CUSTOM, 31);
 /** \brief The exponent for the distance weighting curve */
 CfgFloat cfg_goose_dist_weight_power("GOOSE_DIST_WEIGHTING_POWER", CFG_CUSTOM, 1.0, -10.0, 10.0);
 /** \brief The decrease in intake rate resulting from snow */
 CfgFloat cfg_goose_snow_scaler("GOOSE_SNOW_SCALER", CFG_CUSTOM, 0.1, 0.0, 1.0);

 /**
The P1 curve replicates a curve fitted by Therkildsen, O.R. & Madsen, J. 2000: Assessment of food intake rates in pinkfooted geese Anser brachyrhynchus 
based on examination of oesophagus contents. - Wildl. Biol. 6: 167-172.\n
Assimilation of energy is based on Madsen 1985 Ornis Scand. p 222 - 228: Relations between spring habitat...
Conversion from g/dw to kJ is using 19.8 (Madsen 1985 Vol 16 p. 222 - 228 Ornis Scandinavica)
The P2 & P3 curves are fitted based on Durant et al 2003 J Anim Ecol 72, 220-231.\n
Y-values are kJ per minute, x-values are grass height. The formula is a simple 2nd order polynomial which is the then scaled to get to kJ.\n
The H1 curve is based on Amano et al. 2004 Alleviating grazing damage... Journal of Applied Ecology 41(4): 675-688.\n
The H2 curve is based on Clausen et al. (2018) Agriculture, Ecosystems & Environment 259: 72-76.
The Petti1 curve is based on Pettifor et al (2000). Journal of Applied Ecology 37: 103-135.
*/
 /** \brief Coefficient A in a second order polynomial function */
 CfgFloat cfg_P1A("POLYNOMIALTWO_ONE_A", CFG_CUSTOM, -0.1884);
 /** \brief Coefficient B in a second order polynomial function */
 CfgFloat cfg_P1B("POLYNOMIALTWO_ONE_B", CFG_CUSTOM, 3.1349);
 /** \brief The constant C in a second order polynomial function */
 CfgFloat cfg_P1C("POLYNOMIALTWO_ONE_C", CFG_CUSTOM, 0.0);
 /** \brief Scaler for assimilation of energy from grass for pinkfeet.
 * Madsen 1985 Vol 16 p. 222 - 228 Ornis Scandinavica */
 CfgFloat cfg_P1D("POLYNOMIALTWO_ONE_D", CFG_CUSTOM, 0.404);
 /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
 CfgBool cfg_P1E("POLYNOMIALTWO_ONE_E", CFG_CUSTOM, false);
 /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
 CfgFloat cfg_P1F("POLYNOMIALTWO_ONE_F", CFG_CUSTOM, 16.63);
 /** \brief Min x-value */
 CfgFloat cfg_P1G("POLYNOMIALTWO_ONE_G", CFG_CUSTOM, 0.00);
 /** \brief File name for grass functional response for pinkfeet */
 CfgStr cfg_P1H("POLYNOMIALTWO_ONE_H", CFG_CUSTOM, "KJIntakeAtVaryingGrassHeights_PF");

 /** \brief Coefficient A in a second order polynomial function */
 CfgFloat cfg_G6A("POLYNOMIALTWO_TWO_A", CFG_CUSTOM, -0.1094);
 /** \brief Coefficient B in a second order polynomial function */
 CfgFloat cfg_G6B("POLYNOMIALTWO_TWO_B", CFG_CUSTOM, 2.6695);
 /** \brief The constant C in a second order polynomial function */
 CfgFloat cfg_G6C("POLYNOMIALTWO_TWO_C", CFG_CUSTOM, 0.0);
 /** \brief Scaler for assimilation of energy from grass for pinkfeet.
 * Madsen 1985 Vol 16 p. 222 - 228 Ornis Scandinavica */
 CfgFloat cfg_G6D("POLYNOMIALTWO_TWO_D", CFG_CUSTOM, 0.404);
 /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
 CfgBool cfg_G6E("POLYNOMIALTWO_TWO_E", CFG_CUSTOM, false);
 /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
 CfgFloat cfg_G6F("POLYNOMIALTWO_TWO_F", CFG_CUSTOM, 24.4013);
 /** \brief Min x-value */
 CfgFloat cfg_G6G("POLYNOMIALTWO_TWO_G", CFG_CUSTOM, 0.00);
 /** \brief File name for grass functional response for greylags */
 CfgStr cfg_G6H("POLYNOMIALTWO_TWO_H", CFG_CUSTOM, "KJIntakeAtVaryingGrassHeights_GL");

 /** \brief Coefficient A in a second order polynomial function */
 CfgFloat cfg_B6A("POLYNOMIALTWO_THREE_A", CFG_CUSTOM, -0.066);
 /** \brief Coefficient B in a second order polynomial function */
 CfgFloat cfg_B6B("POLYNOMIALTWO_THREE_B", CFG_CUSTOM, 0.88);
 /** \brief The constant C in a second order polynomial function */
 CfgFloat cfg_B6C("POLYNOMIALTWO_THREE_C", CFG_CUSTOM, 5.0);
 /** \brief Scaler for assimilation of energy from grass for pinkfeet.
 * Madsen 1985 Vol 16 p. 222 - 228 Ornis Scandinavica */
 CfgFloat cfg_B6D("POLYNOMIALTWO_THREE_D", CFG_CUSTOM, 0.404);
 /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
 CfgBool cfg_B6E("POLYNOMIALTWO_THREE_E", CFG_CUSTOM, false);
 /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
 CfgFloat cfg_B6F("POLYNOMIALTWO_THREE_F", CFG_CUSTOM, 13.4761);
 /** \brief Min x-value */
 CfgFloat cfg_B6G("POLYNOMIALTWO_THREE_B", CFG_CUSTOM, 0.00);
 /** \brief File name for grass functional response for barnacles */
 CfgStr cfg_B6H("POLYNOMIALTWO_THREE_H", CFG_CUSTOM, "KJIntakeAtVaryingGrassHeights_BG");

 // ---- Grain functional response calculated by Kevin on 13/10/2017 ---- //
 /** \brief Attack rate in a type II functional response curve (Hollings disc equation) */
 CfgFloat cfg_H1A("HOLLINGS_ONE_A", CFG_CUSTOM, 0.04217666);
 /** \brief Handling time in a type II functional response curve (Hollings disc equation) */
 CfgFloat cfg_H1B("HOLLINGS_ONE_B", CFG_CUSTOM, 0.0840075);
 /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
 CfgBool cfg_H1C("HOLLINGS_ONE_C", CFG_CUSTOM, false);
 /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
 CfgFloat cfg_H1D("HOLLINGS_ONE_D", CFG_CUSTOM, 2500);
 /** \brief Min x-value */
 CfgFloat cfg_H1E("HOLLINGS_ONE_E", CFG_CUSTOM, 0);
 /** \brief File name for functional response on grain for pinkfeet */
 CfgStr cfg_H1F("HOLLINGS_ONE_G", CFG_CUSTOM, "KJIntakeAtDiffGrainDensities_PF");

 // ---- Maize functional response Calculated by Kevin on 29/06/2018 ---- //
 /** \brief Attack rate in a type II functional response curve (Hollings disc equation) */
 CfgFloat cfg_H2A("HOLLINGS_TWO_A", CFG_CUSTOM, 0.04294186);
 /** \brief Handling time in a type II functional response curve (Hollings disc equation) */
 CfgFloat cfg_H2B("HOLLINGS_TWO_B", CFG_CUSTOM, 0.05844966);
 /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
 CfgBool cfg_H2C("HOLLINGS_TWO_C", CFG_CUSTOM, false);
 /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
 CfgFloat cfg_H2D("HOLLINGS_TWO_D", CFG_CUSTOM, 2100);
 /** \brief Min x-value */
 CfgFloat cfg_H2E("HOLLINGS_TWO_E", CFG_CUSTOM, 0);
 /** \brief File name for functional response maize for barnacle */
 CfgStr cfg_H2F("HOLLINGS_TWO_G", CFG_CUSTOM, "KJIntakeAtDiffMaizeDensities_BN");

 // ---- Pettifor feeding time curve ---- //
 /** \brief  Maximum feeding time */
 CfgFloat cfg_Petti1A("PETTIFOR_ONE_A", CFG_CUSTOM, 0.88);
 /** \brief  Minimum feeding time */
 CfgFloat cfg_Petti1B("PETTIFOR_ONE_B", CFG_CUSTOM, 0.57);
 /** \brief  Threshold flock size above which feeding time no longer increases */
 CfgFloat cfg_Petti1C("PETTIFOR_ONE_C", CFG_CUSTOM, 280);
 /** \brief Logical config to control if the curve should be reversed (i.e. 1 - value) */
 CfgBool cfg_Petti1D("PETTIFOR_ONE_D", CFG_CUSTOM, false);
 /** \brief Max x-value - at this point the curve tends to 0, must stop here to avoid negative values */
 CfgFloat cfg_Petti1E("PETTIFOR_ONE_E", CFG_CUSTOM, 1000);
 /** \brief Min x-value */
 CfgFloat cfg_Petti1F("PETTIFOR_ONE_F", CFG_CUSTOM, 0);
 /** \brief File name for density dependent functional response for pinkfeet */
 CfgStr cfg_Petti1G("PETTIFOR_ONE_G", CFG_CUSTOM, "FeedingTimePettifor_PF");

 /** \brief Controlling whether the functional response curves used are written to disk */
 CfgBool cfg_WriteCurve("CURVE_WRITE", CFG_CUSTOM, false);

 //---------------------------------------------------------------------------

 GooseActiveForageLocation::GooseActiveForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref,
													  int a_area, double a_graindensity, double a_maizedensity, double *a_grazing,
													  Goose_Population_Manager *p_NPM)
 {
	 m_HabitatType = tomis_foobar;
	 SetPolygonref(a_polyref);
	 SetArea(a_area);
	 SetGrainDensity(a_graindensity); // grain/m2
	 SetMaizeDensity(a_maizedensity); // kJ/m2
	 /**
	* The grain density is measured in number of grains. To go from grains to grams 
	* we use the weight of a grain being 0.04
	*/
	 m_grainKJ_total = a_area * a_graindensity * 17.67 * 0.04; /** 17.67 kJ/g dw grain & 0.04 grain/g */
	 m_maizeKJ_total = a_area * a_maizedensity;				   // Already in kJ/m2
	 for (int i = 0; i < gs_foobar; i++)
	 {
		 SetGrazing(i, a_grazing[i]);
	 }
	 for (int i = 0; i < gst_foobar; i++)
	 {
		 m_BirdsPresent[i] = 0;
		 m_MaxBirdsPresent[i] = 0;
	 }
	 UpdateKJ(); // Scales m_xxxxKJ_total to area. Used also when recalculating after consumption by geese.
	 AddGeese(a_type, a_number);
	 m_OurPopulationManager = p_NPM;
}
//---------------------------------------------------------------------------

void GooseActiveForageLocation::ClearBirds()
{
	for (int i = 0; i < gst_foobar; i++)
	{
		if (m_BirdsPresent[i] != 0)
		{
			g_land->Warn("GooseActiveForageLocation::ClearBirds() - still birds on field", "");
			std::exit(1);
			break;
		}
		m_MaxBirdsPresent[i] = 0;
	}
}
//---------------------------------------------------------------------------

int GooseActiveForageLocation::GetHuntables()
{
	int huntables = 0;
	if (m_OurPopulationManager->InPinkfootSeason()) {
		huntables += m_BirdsPresent[gst_PinkfootFamilyGroup] + m_BirdsPresent[gst_PinkfootNonBreeder];
	}
	if (m_OurPopulationManager->InGreylagSeason()) {
		huntables += m_BirdsPresent[gst_GreylagFamilyGroup] + m_BirdsPresent[gst_GreylagNonBreeder];
	}
	return huntables;
}
//---------------------------------------------------------------------------

double Goose_Population_Manager::GetDistToClosestRoost(int a_x, int a_y, unsigned a_type)
{
	int rx = m_roosts[a_type][0].m_x;
	int ry = m_roosts[a_type][0].m_y;
	int dist = g_AlmassMathFuncs.CalcDistPythagoras(rx, ry, a_x, a_y);
	for (unsigned i = 1; i<m_roosts[a_type].size(); i++)
	{
		rx = m_roosts[a_type][i].m_x;
		ry = m_roosts[a_type][i].m_y;
		int di = g_AlmassMathFuncs.CalcDistPythagoras(rx, ry, a_x, a_y);
		if (di < dist) {
			dist = di;
		}
	}
	return dist;
}


Goose_Population_Manager::~Goose_Population_Manager(void)
{
	/** Close files and delete pointers to them */
	m_GoosePopDataFile->close();
	delete m_GoosePopDataFile;
	m_GooseFieldForageDataFile->close();
	delete m_GooseFieldForageDataFile;
	m_GooseEnergeticsDataFile->close();
	delete m_GooseEnergeticsDataFile;
	m_GooseIndLocCountFile->close();
	delete m_GooseIndLocCountFile;
	m_GooseHabitatUseFile->close();
	delete m_GooseHabitatUseFile;
	m_GooseHabitatUseFieldObsFile->close();
	delete m_GooseHabitatUseFieldObsFile;
	m_GooseWeightStatsFile->close();
	delete m_GooseWeightStatsFile;
	/** Delete any memory constructs created by new */
	delete m_IntakeRateVSGrainDensity_PF;
	delete m_IntakeRateVSMaizeDensity_BN;
	delete m_ForageRateVSGooseDensity;
	delete m_variate_generator;
	if (cfg_AOROutput_used.value()) {
		delete m_AOR_Pinkfeet;
		delete m_AOR_Greylags;
		delete m_AOR_Barnacles;
	}
	m_GooseXYDumpFile->close();
	delete m_GooseXYDumpFile;
	if (l_map_print_git_version_info.value()) {
		m_GooseGitVersionFile ->close();
		delete m_GooseGitVersionFile;
	}
}
//---------------------------------------------------------------------------

Goose_Population_Manager::Goose_Population_Manager(Landscape* L) : Population_Manager(L)
{
	g_torun = cfg_goose_ModelExitDay.value();
	// Initialise member variables
	m_thermalcosts[0] = 0.0;
	m_thermalcosts[1] = 0.0;
	m_thermalcosts[2] = 0.0;
	// Set start of day - which is at sunrise. 10 is added to m_daytime in DoFirst. Therfore we start out with -10.
    m_daytime = -10;
	// Load List of Animal Classes
	m_ListNames[0] = "Pinkfoot Family";
	m_ListNames[1] = "Pinkfoot Nonbreeder";
	m_ListNames[2] = "Barnacle Family";
	m_ListNames[3] = "Barnacle Nonbreeder";
	m_ListNames[4] = "Greylag Family";
	m_ListNames[5] = "Greylag Nonbreeder";
	m_ListNameLength = 6;
	m_population_type = TOP_Goose;
	// We need one vector for each life stage
	for (unsigned i=0; i<(10-m_ListNameLength); i++)
	{
		TheArray.pop_back();
	}
    strcpy( m_SimulationName, "Goose Simulation" );
	/** Set up before step action sorts
	This determines how we handle the arrays between steps
	*/
	BeforeStepActions[0]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[1]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[2]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[3]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	BeforeStepActions[4]=5; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3=SortXIndex, 4 = do nothing, 5 = shuffle 1 in 500 times
	/**
	* There is a mandatory file for goose populations that describe the roost locations for each goose type in the landscape - GooseRoosts.txt
	* For each type of goose the x,y locations for roosts are listed
	*/
	ifstream ifile("GooseRoosts.txt");
	if (!ifile.is_open())
	{
		m_TheLandscape->Warn("Goose_Population_Manager::Goose_Population_Manager(Landscape* L) ","GooseRoosts.txt missing");
		exit(1);
	}
	int no_entries;
	ifile >> no_entries;
	roostlist r;
	for (unsigned i = 0; i < gs_foobar; i++) m_roosts.push_back(r);
	for (int i=0; i< no_entries; i++)
	{
		int type;
		APoint ap;
		ifile >> type >> ap.m_x >> ap.m_y;
		m_roosts[type].push_back(ap);
	}

	/**
	Read in a file with the distribution of the brood sizes as observed from the field.
	We only have data from Pinkfeet, so this distribution is recycled for the other species
	*/
	ifstream jfile( "PfYoungDist.txt" );
	if (!jfile.is_open()) {
		m_TheLandscape->Warn( "Goose_Population_Manager::Goose_Population_Manager(Landscape* L) ", "PfYoungDist.txt missing" );
		exit( 1 );
	}
	no_entries;
	jfile >> no_entries;
	for (int i = 0; i< no_entries; i++) {
		int young;
		jfile >> young;
		m_youngdist.push_back( young );
	}
	// We dont start in the hunting season
	m_PinkfootSeason = false;
	m_GreylagSeason = false;
	// We make one goose base just to access its static members
	APoint pt;
	pt.m_x = 0;
	pt.m_y = 0;
	Goose_Base gb(NULL,NULL,1,true, pt);
	gb.SetFlightCost(cfg_goose_flightcost.value());
	gb.Set_mingooseforagerate(0.0, gs_Pinkfoot);
	gb.Set_mingooseforagerate(0.0, gs_Barnacle);
	gb.Set_mingooseforagerate(0.0, gs_Greylag);
	gb.Set_Indivmingooseforagerate( 0.0 );
	gb.Set_GooseMaxAppetiteScaler(cfg_goose_MaxAppetiteScaler.value());
	gb.Set_GooseMaxEnergyReserveProportion( cfg_goose_MaxEnergyReserveProportion.value() );
	gb.Set_GooseKJtoFatConversion( 1.0/(cfg_goose_EnergyContentOfFat.value() + cfg_goose_MetabolicConversionCosts.value()));
	gb.Set_GooseFattoKJConversion(1.0 / (cfg_goose_EnergyContentOfFat.value() - cfg_goose_MetabolicConversionCosts.value()));
	gb.Set_GooseMinForageOpenness( cfg_goose_MinForageOpenness.value() );
	gb.Set_GooseLeavingThreshold( cfg_goose_LeavingThreshold.value() * 5.0); // 5 because we use a 5-day running average
	gb.Set_GooseForageDist(cfg_goose_pf_ForageDist.value(), cfg_goose_bn_ForageDist.value(), cfg_goose_gl_ForageDist.value());
	gb.Set_GooseFieldForageDist(cfg_goose_pf_FieldForageDist.value(), cfg_goose_bn_FieldForageDist.value(), cfg_goose_gl_FieldForageDist.value());

	gb.Set_GooseFollowingLikelyhood(cfg_goose_bn_followinglikelyhood.value(), gst_BarnacleFamilyGroup);
	gb.Set_GooseFollowingLikelyhood(cfg_goose_pf_followinglikelyhood.value(), gst_PinkfootFamilyGroup);
	gb.Set_GooseFollowingLikelyhood(cfg_goose_gl_followinglikelyhood.value(), gst_GreylagFamilyGroup);
	// We use the value for families for NB also. Currently not possible to quantify a difference. 
	// At a later stage we might want to explore this more, so we keep the cfg for later.
	gb.Set_GooseFollowingLikelyhood(cfg_goose_bn_followinglikelyhood.value(), gst_BarnacleNonBreeder);
	gb.Set_GooseFollowingLikelyhood(cfg_goose_pf_followinglikelyhood.value(), gst_PinkfootNonBreeder);
	gb.Set_GooseFollowingLikelyhood(cfg_goose_gl_followinglikelyhood.value(), gst_GreylagNonBreeder);

	gb.Set_GooseLeavingRoost( true );
	/** Create curves for food intake rates and forage rates */
	m_ForageRateVSGooseDensity = new PettiforFeedingTimeCurveClass(cfg_Petti1A.value(), cfg_Petti1B.value(), cfg_Petti1C.value(), cfg_Petti1D.value(), cfg_Petti1E.value(), cfg_Petti1F.value(), cfg_Petti1G.value());
	m_IntakeRateVSGrainDensity_PF = new HollingsDiscCurveClass(cfg_H1A.value(), cfg_H1B.value(), cfg_H1C.value(), cfg_H1D.value(), cfg_H1E.value(), cfg_H1F.value());
	m_IntakeRateVSMaizeDensity_BN = new HollingsDiscCurveClass( cfg_H2A.value(), cfg_H2B.value(), cfg_H2C.value(), cfg_H2D.value(), cfg_H2E.value(), cfg_H2F.value() );
	if (cfg_WriteCurve.value()) {
		m_ForageRateVSGooseDensity->WriteDataFile( 10 );
		m_IntakeRateVSGrainDensity_PF->WriteDataFile( 10 );
		m_IntakeRateVSMaizeDensity_BN->WriteDataFile( 10 );
	}
	// Set up a normal distribution which we can then draw from at random
	Mersenne_Twister twister;
	NormalDistDouble LeaveNormal( cfg_goose_RoostLeaveDistMean.value(), cfg_goose_RoostLeaveDistSD.value() );  // mean and std 
	m_variate_generator = new Variate_gen( twister, LeaveNormal );
	Init();
}

//---------------------------------------------------------------------------

void Goose_Population_Manager::Init()
{
	/**
	*Finally opens any output files needed
	*/
	if (cfg_ReallyBigOutput_used.value()) {
		OpenTheReallyBigProbe();
	}
	else ReallyBigOutputPrb = 0;
	if (cfg_AOROutput_used.value()) {
		m_AOR_Pinkfeet = new AOR_Probe_Goose(this, m_TheLandscape, "AOR_pinkfoot.txt");
		m_AOR_Barnacles = new AOR_Probe_Goose(this, m_TheLandscape, "AOR_barnacle.txt");
		m_AOR_Greylags = new AOR_Probe_Goose(this, m_TheLandscape, "AOR_greylag.txt");
	}
	m_GooseXYDumpFile = new ofstream("GooseXYDump.txt", ios::out);
	std::vector<std::string> xy_headers;
	xy_headers = { "X", "Y", "poly_ref", "fl_x", "fl_y", "fl_poly" };
	WriteHeaders(m_GooseXYDumpFile, xy_headers);

	m_GoosePopDataFile = new ofstream("GoosePopulationData.txt", ios::out);
	std::vector<std::string> pd_headers;
	pd_headers = { "season", "day", "pf_families", "pf_non_breeders", "bn_families",
					"bn_non_breeders", "gl_families", "gl_non_breeders", "snow_depth" };
	WriteHeaders(m_GoosePopDataFile, pd_headers);

	m_GooseWeightStatsFile = new ofstream("GooseWeightStats.txt", ios::out);
	std::vector<std::string> ws_headers = { "season", "day", "day_in_year", "species", "mean_weight", "mean_weight_se", "n" };
	WriteHeaders(m_GooseWeightStatsFile, ws_headers);

	m_GooseIndLocCountFile = new ofstream("GooseIndLocCountStats.txt", ios::out);
	std::vector<std::string> ilc_headers = { "season", "day", "day_in_year", "species",
											"n_forage_locs", "n_forage_locs_se", "n" };
	WriteHeaders(m_GooseIndLocCountFile, ilc_headers);

	m_GooseHabitatUseFile = new ofstream("GooseHabitatUseStats.txt", ios::out);
	std::vector<std::string> ghu_headers = { "season", "day", "day_in_year", "species" };
	for (int h = 0; h < tomis_foobar; h++)
	{
		string str1 = IntakeSourceToString((TTypeOfMaxIntakeSource)h);
		ghu_headers.push_back(str1);
	}
	ghu_headers.push_back("count");
	WriteHeaders(m_GooseHabitatUseFile, ghu_headers);
	ClearGooseHabitatUseStats();

	m_GooseHabitatUseFieldObsFile = new ofstream("GooseHabitatUseFieldObsStats.txt", ios::out);
	std::vector<std::string> ghufo_headers = { "season", "day", "day_in_year", "species" };
	for (int h = 0; h < tomis_foobar; h++)
	{
		string str1 = IntakeSourceToString((TTypeOfMaxIntakeSource)h);
		ghufo_headers.push_back(str1);
	}
	ghufo_headers.push_back("count");
	WriteHeaders(m_GooseHabitatUseFieldObsFile, ghufo_headers);
	ClearGooseHabitatUseFieldObsStats();

	m_GooseLeaveReasonStatsFile = new ofstream("GooseLeaveReasonStats.txt", ios::out);
	std::vector<std::string> lr_headers = { "season", "day", "day_in_year", "species_type", "leave_reason", "n" };
	WriteHeaders(m_GooseLeaveReasonStatsFile, lr_headers);

	m_StateStatsFile = new ofstream("GooseStateStats.txt", ios::out);
	std::vector<std::string> ss_headers = { "season", "day", "n" };
	WriteHeaders(m_StateStatsFile, ss_headers);

	m_GooseEnergeticsDataFile = new ofstream("GooseEnergeticsData.txt", ios::out);
	std::vector<std::string> ed_headers;
	ed_headers = { "season", "day", "species", "foraging_time", "foraging_time_se",
				  "flight_distance", "flight_distance_se", "daily_energy_budget",
				  "daily_energy_budget_se", "daily_energy_balance",
				  "daily_energy_balance_se", "day_length" };
	WriteHeaders(m_GooseEnergeticsDataFile, ed_headers);
	m_GooseFieldForageDataFile = new ofstream("GooseFieldForageData.txt", ios::out);
	// We use two different versions dependent on whether we are testing of just running
	if (cfg_goose_runtime_reporting.value())
	{
		std::vector<std::string> fd_headers;
		fd_headers = {
			"season",
			"day",
			"geese",
			"pinkfoot",
			"pinkfoot_timed",
			"roost_dist_pinkfoot",
			"barnacle",
			"barnacle_timed",
			"roost_dist_barnacle",
			"greylag",
			"greylag_timed",
			"roost_dist_greylag",
			"polyref"
		};
		WriteHeaders(m_GooseFieldForageDataFile, fd_headers);
	}
	if (!cfg_goose_runtime_reporting.value())
	{
		std::vector<std::string> fd_headers;
		fd_headers = {"season", "day", "polyref",  "utm_x", "utm_y", "geese",
					  "geese_timed", "openness", "pinkfoot", "pinkfoot_timed",
					  "roost_dist_pinkfoot", "grass_pinkfoot", "barnacle",
					  "barnacle_timed", "roost_dist_barnacle", "grass_barnacle",
					  "greylag", "greylag_timed", "roost_dist_greylag", "grass_greylag",
					  "grain", "maize", "veg_type_chr", "veg_height", "digestability",
					  "veg_phase", "previous_crop", "last_sown_veg" };
		WriteHeaders(m_GooseFieldForageDataFile, fd_headers);
	}
	if (cfg_goose_ObservedOpennessQuery.value())
	{
		ObservedOpennessQuery();
	}
	if (cfg_goose_WriteConfig.value()) {
		WriteConfig();
	}
#ifdef __GITVERSION
	if (l_map_print_git_version_info.value())
	{
		m_GooseGitVersionFile = new ofstream("almass-version.txt", ios::out);
		(*m_GooseGitVersionFile) << "hash=" << GIT_HASH << std::endl;
		(*m_GooseGitVersionFile) << "time=" << COMPILE_TIME << std::endl;
		(*m_GooseGitVersionFile) << "branch=" << GIT_BRANCH << std::endl;
	}
#endif
}
/** 
Here locations of geese observed in the field are read in from a file and the
openness score for those locations are written to a file.
*/
void Goose_Population_Manager::ObservedOpennessQuery()  
{
	ofstream ofile("GooseObservedOpenness.txt", ios::out);
	ofile << "Polyref" << '\t' << "Openness" << '\t' << "ElementType" << '\t'<< "CentroidX" << '\t' << "CentroidY" << endl;

	ifstream ifile( "GooseObservations.txt" );
	if (!ifile.is_open())
	{
		m_TheLandscape->Warn( "Goose_Population_Manager::Goose_Population_Manager(Landscape* L) ", "GooseObservations.txt missing" );
		exit( 1 );
	}

	int no_entries;
	ifile >> no_entries;  // Read in the first line (number of lines)

	for (int i = 0; i < no_entries; i++)
	{
		int x;
		int y;
	    string thetype;
		int centroidx;
		int centroidy;
		int thepolyref;
		int theopenness;

		ifile >> x >> y;  // Read in the first and second column as x and y
		thetype = m_TheLandscape->PolytypeToString(m_TheLandscape->SupplyElementType(x, y));  /**@todo The string is padded by blank spaces - can this be fixed? */
		centroidx = m_TheLandscape->SupplyCentroidX(x, y);
		centroidy = m_TheLandscape->SupplyCentroidY(x, y);
		thepolyref = m_TheLandscape->SupplyPolyRef( x, y );
		theopenness = m_TheLandscape->SupplyOpenness( x, y );

		//Write out the stuff to a file.
		ofile << thepolyref << '\t' << theopenness << '\t' << thetype << '\t' << centroidx << '\t' << centroidy << endl;  
	}
	ifile.close();
	ofile.close();
}

/**
Write out the values of the configs used
Handy when compiling the result documents
*/
void Goose_Population_Manager::WriteConfig() {
	ofstream ofile( "GooseConfig.txt", ios::out);
	boost::io::ios_flags_saver  ifs( ofile );  // Only use fixed within the scope of WriteConfig
	ofile << fixed;
	ofile << "Variable" << '\t' << "Value" << endl;
	ofile << "GOOSE_PINKFOOTWEIGHT" << '\t' << cfg_goose_pf_baseweight.value() << endl;
	ofile << "GOOSE_BARNACLEWEIGHT" << '\t' << cfg_goose_bn_baseweight.value() << endl;
	ofile << "GOOSE_GREYLAGWEIGHT" << '\t' << cfg_goose_gl_baseweight.value() << endl;
	ofile << "GOOSE_FLIGHTCOST" << '\t' << cfg_goose_flightcost.value() << endl;
	ofile << "GOOSE_FORAGEDIST_PF" << '\t' << cfg_goose_pf_ForageDist.value() << endl;
	ofile << "GOOSE_FORAGEDIST_BN" << '\t' << cfg_goose_bn_ForageDist.value() << endl;
	ofile << "GOOSE_FORAGEDIST_GL" << '\t' << cfg_goose_gl_ForageDist.value() << endl;
	ofile << "GOOSE_BMRCONSTANTA" << '\t' << cfg_goose_BMRconstant1.value() << endl;
	ofile << "GOOSE_BMRCONSTANTB" << '\t' << cfg_goose_BMRconstant2.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTA_PF" << '\t' << cfg_goose_pf_Thermalconstant.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTA_BN" << '\t' << cfg_goose_bn_Thermalconstant.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTA_GL" << '\t' << cfg_goose_gl_Thermalconstant.value() << endl;
	ofile << "GOOSE_THERMALCONSTANTB" << '\t' << cfg_goose_Thermalconstantb.value() << endl;
	ofile << "GOOSE_MAXAPPETITESCALER" << '\t' << cfg_goose_MaxAppetiteScaler.value() << endl;
	ofile << "GOOSE_MAXENERGYRESERVEPROPORTION" << '\t' << cfg_goose_MaxEnergyReserveProportion.value() << endl;
	ofile << "GOOSE_ENERGYCONTENTOFFAT" << '\t' << cfg_goose_EnergyContentOfFat.value() << endl;
	ofile << "GOOSE_METABOLICCONVCOSTS" << '\t' << cfg_goose_MetabolicConversionCosts.value() << endl;
	ofile << "GOOSE_MINFORAGEOPENNESS" << '\t' << cfg_goose_MinForageOpenness.value() << endl;
	ofile << "GOOSE_PF_SEXRATIO" << '\t' << cfg_goose_pf_sexratio.value() << endl;
	ofile << "GOOSE_BN_SEXRATIO" << '\t' << cfg_goose_bn_sexratio.value() << endl;
	ofile << "GOOSE_GL_SEXRATIO" << '\t' << cfg_goose_gl_sexratio.value() << endl;
	ofile << "GOOSE_PF_ARRIVEDATESTART" << '\t' << cfg_goose_pf_arrivedatestart.value() << endl;
	ofile << "GOOSE_PF_ARRIVEDATEEND" << '\t' << cfg_goose_pf_arrivedateend.value() << endl;
	ofile << "GOOSE_BN_ARRIVEDATESTART" << '\t' << cfg_goose_bn_arrivedatestart.value() << endl;
	ofile << "GOOSE_BN_ARRIVEDATEEND" << '\t' << cfg_goose_bn_arrivedateend.value() << endl;
	ofile << "GOOSE_GL_ARRIVEDATESTART" << '\t' << cfg_goose_gl_arrivedatestart.value() << endl;
	ofile << "GOOSE_GL_ARRIVEDATEEND" << '\t' << cfg_goose_gl_arrivedateend.value() << endl;
	ofile << "GOOSE_PF_LEAVINGDATESTART" << '\t' << cfg_goose_pf_leavingdatestart.value() << endl;
	ofile << "GOOSE_PF_LEAVINGDATEEND" << '\t' << cfg_goose_pf_leavingdateend.value() << endl;
	ofile << "GOOSE_BN_LEAVINGDATESTART" << '\t' << cfg_goose_bn_leavingdatestart.value() << endl;
	ofile << "GOOSE_BN_LEAVINGDATEEND" << '\t' << cfg_goose_bn_leavingdateend.value() << endl;
	ofile << "GOOSE_GL_LEAVINGDATESTART" << '\t' << cfg_goose_gl_leavingdatestart.value() << endl;
	ofile << "GOOSE_GL_LEAVINGDATEEND" << '\t' << cfg_goose_gl_leavingdateend.value() << endl;
	ofile << "GOOSE_GRAINDECAYRATEWINTER" << '\t' << cfg_goose_GrainDecayRateWinter.value() << endl;
	ofile << "GOOSE_GRAINDECAYRATESPRING" << '\t' << cfg_goose_GrainDecayRateSpring.value() << endl;
	ofile << "GOOSE_MAXSCAREDIST" << '\t' << cfg_goose_MaxScareDistance.value() << endl;
	ofile << "GOOSE_FOLLOWINGLIKELYHOOD_BN" << '\t' << cfg_goose_bn_followinglikelyhood.value() << endl;
	ofile << "GOOSE_FOLLOWINGLIKELYHOOD_PF" << '\t' << cfg_goose_pf_followinglikelyhood.value() << endl;
	ofile << "GOOSE_FOLLOWINGLIKELYHOOD_GL" << '\t' << cfg_goose_gl_followinglikelyhood.value() << endl;
	ofile << "GOOSE_ROOSTLEAVEDISTSD" << '\t' << cfg_goose_RoostLeaveDistSD.value() << endl;
	ofile << "GOOSE_ROOSTLEAVEDISTMEAN" << '\t' << cfg_goose_RoostLeaveDistMean.value() << endl;
	ofile << "GOOSE_INITIALENERGYRESERVEPROPORTION" << '\t' << cfg_goose_InitialEnergyReserveProportion.value() << endl;
	ofile << "GOOSE_ROOSTCHANGECHANCE" << '\t' << cfg_goose_roostchangechance.value() << endl;
	ofile << "GOOSE_LEAVEREASONSTATS" << '\t' << cfg_goose_LeaveReasonStats.value() << endl;
	ofile << "GOOSE_DIST_WEIGHTING_POWER" << '\t' << cfg_goose_dist_weight_power.value() << endl;
	ofile << "GOOSE_GRASS_TO_WINTER_CEREAL_SCALER" << '\t' << cfg_goose_grass_to_winter_cereal_scaler.value() << endl;
	ofile << "GOOSE_DAYTIMEBMRMULTIPLIER" << '\t' << cfg_goose_daytime_BMR_multiplier.value() << endl;
	ofile << "GOOSE_NIGHTTIMEBMRMULTIPLIER" << '\t' << cfg_goose_nighttime_BMR_multiplier.value() << endl;
	ofile << "GOOSE_SNOW_SCALER" << '\t' << cfg_goose_snow_scaler.value() << endl;
	ofile << "VEG_GROWTHSCALERMAX" << '\t' << cfg_PermanentVegGrowthMaxScaler.value() << endl;
	ofile << "VEG_GROWTHSCALERMIN" << '\t' << cfg_PermanentVegGrowthMinScaler.value() << endl;
	ofile.close();
}

void Goose_Population_Manager::CreateObjects(int a_ob_type, TAnimal *, struct_Goose * a_data, int a_number)
{
	for (int i = 0; i<a_number; i++)
	{
		switch (a_ob_type)
		{
		case gst_PinkfootFamilyGroup:
			Goose_Pinkfoot_FamilyGroup* new_PFFamilyGoose;
			if (unsigned(TheArray[a_ob_type].size())>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_PFFamilyGoose = dynamic_cast<Goose_Pinkfoot_FamilyGroup*>(TheArray[a_ob_type][GetLiveArraySize(a_ob_type)]);
				new_PFFamilyGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_PFFamilyGoose = new Goose_Pinkfoot_FamilyGroup(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				TheArray[a_ob_type].push_back(new_PFFamilyGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_PinkfootNonBreeder:
			Goose_Pinkfoot_NonBreeder* new_PFNonbreederGoose;
			if (unsigned(TheArray[a_ob_type].size())>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_PFNonbreederGoose = dynamic_cast<Goose_Pinkfoot_NonBreeder*>(TheArray[a_ob_type][GetLiveArraySize(a_ob_type)]);
				new_PFNonbreederGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_PFNonbreederGoose = new Goose_Pinkfoot_NonBreeder(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				TheArray[a_ob_type].push_back(new_PFNonbreederGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_BarnacleFamilyGroup:
			Goose_Barnacle_FamilyGroup* new_BFamilyGoose;
			if (unsigned(TheArray[a_ob_type].size())>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_BFamilyGoose = dynamic_cast<Goose_Barnacle_FamilyGroup*>(TheArray[a_ob_type][GetLiveArraySize(a_ob_type)]);
				new_BFamilyGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_BFamilyGoose = new Goose_Barnacle_FamilyGroup(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				TheArray[a_ob_type].push_back(new_BFamilyGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_BarnacleNonBreeder:
			Goose_Barnacle_NonBreeder* new_BNonbreederGoose;
			if (unsigned(TheArray[a_ob_type].size())>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_BNonbreederGoose = dynamic_cast<Goose_Barnacle_NonBreeder*>(TheArray[a_ob_type][GetLiveArraySize(a_ob_type)]);
				new_BNonbreederGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_BNonbreederGoose = new Goose_Barnacle_NonBreeder(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				TheArray[a_ob_type].push_back(new_BNonbreederGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_GreylagNonBreeder:
			Goose_Greylag_NonBreeder* new_GNonbreederGoose;
			if (unsigned(TheArray[a_ob_type].size())>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_GNonbreederGoose = dynamic_cast<Goose_Greylag_NonBreeder*>(TheArray[a_ob_type][GetLiveArraySize(a_ob_type)]);
				new_GNonbreederGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
			}
			else {
				new_GNonbreederGoose = new Goose_Greylag_NonBreeder(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_roost);
				TheArray[a_ob_type].push_back(new_GNonbreederGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		case gst_GreylagFamilyGroup:
			Goose_Greylag_FamilyGroup* new_GFamilyGoose;
			if (unsigned(TheArray[a_ob_type].size())>GetLiveArraySize(a_ob_type)) {
				// We need to reuse an object
				new_GFamilyGoose = dynamic_cast<Goose_Greylag_FamilyGroup*>(TheArray[a_ob_type][GetLiveArraySize(a_ob_type)]);
				new_GFamilyGoose->ReInit(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				IncLiveArraySize(a_ob_type);
				}
			else {
				new_GFamilyGoose = new Goose_Greylag_FamilyGroup(a_data->m_L, a_data->m_GPM, a_data->m_weight, a_data->m_sex, a_data->m_grpsize, a_data->m_roost);
				TheArray[a_ob_type].push_back(new_GFamilyGoose);
				IncLiveArraySize(a_ob_type);
			}
			break;
		default:
			char ob[255];
			sprintf(ob, "%d", (int)a_ob_type);
			m_TheLandscape->Warn("Goose_Population_Manager::CreateObjects() unknown object type - ", ob);
			exit(1);
			break;
		}
	}
}

//---------------------------------------------------------------------------

void Goose_Population_Manager::FindClosestRoost(int &a_x, int &a_y, unsigned a_type)
{
	int answer = 0;
	int rx = m_roosts[a_type][0].m_x;
	int ry = m_roosts[a_type][0].m_y;
	int dist = g_AlmassMathFuncs.CalcDistPythagoras(rx, ry, a_x, a_y);
	for (unsigned i=1; i<m_roosts[a_type].size(); i++)
	{
		rx = m_roosts[a_type][i].m_x;
		ry = m_roosts[a_type][i].m_y;
		int di = g_AlmassMathFuncs.CalcDistPythagoras( rx, ry, a_x, a_y );
		if (di<dist) answer = i;
	}
	a_x = m_roosts[a_type][answer].m_x;
	a_y = m_roosts[a_type][answer].m_y;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoFirst()
{
	/**
	* The Goose_Population_Manager DoFirst has three main functions. The first is to control the variables related to the goose timestep of 10 minutes.
	* Here we assume the daylight hours start at time 0 and go on until the end of the daylength at any given date. \n
	*
	* Then, if we are at start of day, then max goose numbers are relayed to the field elements. \n
	* Next checks the date and determines if we need to either migrate out of the area or into the area. If so calls the appropriate immigration or emigration code.
	* Otherwise if we are in normal Danish simulation mode then at the start of each day the list of active forage locations is emptied ready to be refilled
	* as the geese start foraging and some global energetic constants are calculated (thermal regulation costs).\n
	*/
	int dlt = m_TheLandscape->SupplyDaylength();
	m_daytime = ( m_daytime + 10 ) % 1440; // We have 10 minutes timesteps. This resets m_daytime to 0 when it hits 1440.
	if (m_daytime < dlt ) m_daylight = true; else m_daylight = false;
	m_daylightleft = dlt - m_daytime;
	/*
	* Next checks the date and determines if we need to either migrate out of the area or into the area. If so calls the appropriate immigration or emigration code.
	* Otherwise if we are in normal Danish simulation mode then at the start of each day the list of active forage locations is emptied ready to be refilled
	* as the geese start foraging.
	*/
	if (m_daytime == 0)
	{
		DoImmigration();
		DoEmigration();
		m_GooseForageLocations.clear();
		// Calculate energetic constants for each species
		double temp = m_TheLandscape->SupplyTemp();
		if (temp < cfg_goose_pf_Thermalconstant.value())
		{
			double dt = cfg_goose_pf_Thermalconstant.value() - temp;
			m_thermalcosts[gs_Pinkfoot] = cfg_goose_Thermalconstantb.value() * dt;
		}
		if (temp < cfg_goose_bn_Thermalconstant.value())
		{
			double dt = cfg_goose_bn_Thermalconstant.value() - temp;
			m_thermalcosts[ gs_Barnacle ] = cfg_goose_Thermalconstantb.value() * dt;
		}
		if (temp < cfg_goose_gl_Thermalconstant.value())
		{
			double dt = cfg_goose_gl_Thermalconstant.value() - temp;
			m_thermalcosts[ gs_Greylag ] = cfg_goose_Thermalconstantb.value() * dt;
		}
	}
	/*
	* Then we check if we are in the hunting season of any of the legal quarry species
	*/
	int today = m_TheLandscape->SupplyDayInYear();
	if (InHuntingSeason(today, gs_Pinkfoot))
	{
		m_PinkfootSeason = true;
	}
	else m_PinkfootSeason = false;
	if (InHuntingSeason(today, gs_Greylag))
	{
		m_GreylagSeason = true;
	}
	else m_GreylagSeason = false;
	/*
	* By March first we reset all grain and maize resources to make sure we don't get 
	* problems in the following season
	*/
	if (today == cfg_goose_grain_and_maize_reset_day.value()) {
		m_TheLandscape->ResetGrainAndMaize();
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoLast() {
	/**
	* The first job of DoLast is to update grain status in the forage locations.\n
	* If it is end of day, then max goose numbers are relayed to the field elements, then to inform the landscape
	* of changes in grain and maize densities as a result of goose feeding. This is done here rather than have the geese interact directly with the landscape elements for efficiency, since landscape elements will only
	* be updated once a day, and after the geese are finished. This also means fewer calculations and indirections.\n
	* If there is no grain or maize to deplete this does nothing. \n @todo Where is the grazing resource depleted?
	* \n
	* Once that is done then we need to produce output - we produce three output files here - the first with the goose
	* information at individual level. The second is field information about where the geese forage. In both cases the
	* data is written to these files when between start and end year, and start and end day (inclusive).\n
	* The third output is the a description of the population sizes of each type of goose we
	* simulate (these are numbers extant in the area being simulated).
	*/
	for (unsigned i = 0; i < m_GooseForageLocations.size(); i++) {
		m_GooseForageLocations[ i ].UpdateKJ();
	}
	int daylength = m_TheLandscape->SupplyDaylength();
	// Since the daylight starts at m_daytime == 0 and the daylength varies, we are recording geese at a timepoint by dividing the day by a specified value. E.g. to get noon, we divide by 2
	int rectime = int(round( daylength / cfg_goose_TimedCounts.value() ));
	rectime -= rectime % 10;  // To get a timestep point
	if (rectime == m_daytime) {
		int sz = (int)m_GooseForageLocations.size();
		for (int i = 0; i < sz; i++) {
			int pref = m_GooseForageLocations[i].GetPolygonref();
			int goose_numbers = m_GooseForageLocations[i].GetGooseNumbers();
			m_TheLandscape->RecordGooseNumbersTimed(pref, goose_numbers);
			for (unsigned j = 0; j < gs_foobar; j++) {
				int goose_numbers_timed = m_GooseForageLocations[i].GetBirds((GooseSpecies)j);
				m_TheLandscape->RecordGooseSpNumbersTimed(pref, goose_numbers_timed, (GooseSpecies)j);
				int roost_dist = int( m_GooseForageLocations[i].GetRoostDist((GooseSpecies)j));
				m_TheLandscape->RecordGooseRoostDist(pref, roost_dist, (GooseSpecies)j);
			}
		}
	}
	int day = m_TheLandscape->SupplyDayInYear();
	if (day == 300)
	{
		rectime = int(round(daylength / cfg_goose_TimedCounts.value()));
		rectime -= rectime % 10;  // To get a timestep point
		if (rectime == m_daytime) 
		{
			XYDump();
		}
	}
	if (m_daytime == 1430) // Last chance before sunrise (start of next day)
	{
		day = m_TheLandscape->SupplyDayInYear(); // **CJT** This looks to be superfluous
		if (day == 183) m_SeasonNumber++;

		int sz = (int)m_GooseForageLocations.size();
		for (int i = 0; i < sz; i++) {
			m_TheLandscape->RecordGooseNumbers( m_GooseForageLocations[ i ].GetPolygonref(), m_GooseForageLocations[ i ].GetMaxBirdsPresent() );
			for (unsigned j = 0; j < gs_foobar; j++) {
				m_TheLandscape->RecordGooseSpNumbers( m_GooseForageLocations[ i ].GetPolygonref(), m_GooseForageLocations[ i ].GetMaxSpBirdsPresent( (GooseSpecies)j ), (GooseSpecies)j );
			}
			// Remove bird records from the location and do a debug check on any birds remaining.
			m_GooseForageLocations[ i ].ClearBirds();
		}
		// If there are any geese and the output is required we record output:
		int anygeese = 0;
		for (int i = 0; i < gst_foobar; ++i) {
			anygeese = (int) GetLiveArraySize( i );
			if (anygeese > 0) {
				break;
			}
		}
		if (anygeese > 0) {
			if (cfg_goose_FieldForageInfo.value()) {
				GooseFieldForageInfoOutput();
			}
			if (cfg_goose_EnergyRecord.value()) {
				GooseEnergyRecordOutput();
				ClearGooseForagingTimeStats();
				ClearGooseFlightDistanceStats();
				ClearGooseDailyEnergyBudgetStats();
				ClearGooseDailyEnergyBalanceStats();
			}
			if (cfg_goose_WeightStats.value()) {
				if ((day + 1) % 7 == 0)  // Only do weekly mean of the weights. +1 to avoid day 0 problems
				{
					GooseWeightStatOutput();  // Here we record the weight stats for the day
					ClearGooseWeightStats();  // and here we reset them.
				}
			}
			if (cfg_goose_IndLocCounts.value()) {
				GooseIndLocCountOutput();  // Here we record the individual forage location counts stats for the day
				ClearIndLocCountStats();  // and here we reset them.
			}
			if (cfg_goose_StateStats.value())
			{
				StateStatOutput();
				ClearStateStats();
			}
			if (cfg_goose_PopulationDescriptionON.value()) {
				GoosePopulationDescriptionOutput();
			}
			if (cfg_goose_LeaveReasonStats.value()) {
				GooseLeaveReasonStatOutput();
				ClearGooseLeaveReasonStats();
			}
		}
		// Update forage:
		for (int i = 0; i < sz; i++) {
			int TheForageLocationPolyref = m_GooseForageLocations[ i ].GetPolygonref();
			double poly_grain = m_TheLandscape->SupplyBirdSeedForage( TheForageLocationPolyref );
			if (poly_grain > 0) {
				m_TheLandscape->SetBirdSeedForage( TheForageLocationPolyref, m_GooseForageLocations[ i ].GetGrainDensity() );
			}
			double poly_maize = m_TheLandscape->SupplyBirdMaizeForage( TheForageLocationPolyref );
			if (poly_maize > 0) {
				m_TheLandscape->SetBirdMaizeForage( TheForageLocationPolyref, m_GooseForageLocations[ i ].GetMaizeDensity() );
			}
			if (m_GooseForageLocations[ i ].GetGrazedBiomass() > 0) {
				m_TheLandscape->GrazeVegetationTotal( TheForageLocationPolyref, m_GooseForageLocations[ i ].GetGrazedBiomass() );
				m_GooseForageLocations[ i ].ResetGrazing();
			}
		}
	}
	// Record habitat use
	if (GetDayTime() == cfg_gooseHabitatUsetime.value())
	{
		// Need to cycle through all the geese and ask where they foraged last
		for (int i = 0; i < gst_foobar; i++)
		{
			Goose_Base* gb;
			int sz = int(GetLiveArraySize(i));
			for (int j = 0; j < sz; j++)
			{
				// This records what the geese fed on - provides the habitat use output
				gb = (dynamic_cast<Goose_Base*>(TheArray[i][j]));
				TMaxIntakeSource src = gb->GetMaxIntakeSource();
				if (src.m_maxintakesource != tomis_foobar)
				{
					RecordHabitatUse(src.m_maxintakesource, gb->GetSpecies(), gb->GetGroupsize());
					// Below the same kind of output, but now mimicing the field observations
					TTypeOfMaxIntakeSource res = tomis_foobar;
					// If it is maize it is maize if in stubble
					if (src.m_maxintakesource == tomis_sowncrop) res = tomis_sowncrop;
					else if ((((src.m_prevsowncrop == tov_Maize) || (src.m_prevsowncrop == tov_MaizeSilage))) && (src.m_instubble)) res = tomis_maize;
					else if (src.m_instubble) {
						if (src.m_iscereal) res = tomis_grain;
						else if (m_TheLandscape->SupplyIsGrass2(src.m_veg)) res = tomis_grass; // post silage cut
					}
					else res = tomis_grass;
					RecordHabitatUseFieldObs(res, gb->GetSpecies(), gb->GetGroupsize());
				}
			}
		}
		// Write the record
		if (g_date->GetDayInMonth() == 1)
		{
			GooseHabitatUseOutput();
			GooseHabitatUseFieldObsOutput();
		}
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseHabitatUseOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		double pct[tomis_foobar];
		int count = 0;
		for (int h = 0; h < tomis_foobar; h++) {
			count += m_HabitatUseStats[i* tomis_foobar + h];
			pct[h] = 0;
		}
		for (int h = 0; h < tomis_foobar; h++) {
			if (count > 0) pct[h] = m_HabitatUseStats[i* tomis_foobar + h] / double(count); else pct[h] = 0;
		}

		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseHabitatUseFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t';
		for (int h = 0; h < tomis_foobar; h++) {
			(*m_GooseHabitatUseFile) << pct[h] << '\t';
		}
		(*m_GooseHabitatUseFile) << count << endl;
	}
	ClearGooseHabitatUseStats();
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseHabitatUseFieldObsOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		double pct[tomis_foobar];
		int count = 0;
		for (int h = 0; h < tomis_foobar; h++) {
			count += m_HabitatUseFieldObsStats[i* tomis_foobar + h];
			pct[h] = 0;
		}
		for (int h = 0; h < tomis_foobar; h++) {
			if (count > 0) pct[h] = m_HabitatUseFieldObsStats[i* tomis_foobar + h] / double(count); else pct[h] = 0;
		}

		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseHabitatUseFieldObsFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t';
		for (int h = 0; h < tomis_foobar; h++) {
			(*m_GooseHabitatUseFieldObsFile) << pct[h] << '\t';
		}
		(*m_GooseHabitatUseFieldObsFile) << count << endl;
	}
	ClearGooseHabitatUseFieldObsStats();
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseFieldForageInfoOutput()
{
	/** 
	* The information needed here is in the landscape. 
	* We need to trawl through all the fields and record those that have suitable openness scores and food for geese 
	*/
	int Day = m_TheLandscape->SupplyGlobalDate();

	GooseFieldList* afieldlist = m_TheLandscape->GetGooseFields(cfg_goose_MinForageOpenness.value());
	unsigned sz = (unsigned) afieldlist->size();
	if (cfg_goose_runtime_reporting.value())
	{
		for (unsigned f = 0; f < sz; f++)
		{
			// Only write if there were any geese
			if ((*afieldlist)[f].geese > 0) {
				(*m_GooseFieldForageDataFile)
					<< m_SeasonNumber << '\t'
					<< Day << '\t'
					<< (*afieldlist)[f].geese << '\t';
					for (int i = 0; i < gs_foobar; ++i)
					{
						(*m_GooseFieldForageDataFile) 
							<< (*afieldlist)[f].geesesp[i] << '\t'
							<< (*afieldlist)[f].geesespTimed[i] << '\t'
							<< (*afieldlist)[f].roostdists[i] << '\t';
					}
					(*m_GooseFieldForageDataFile) << (*afieldlist)[f].polyref << endl;
			}
		}
	}
	if (!cfg_goose_runtime_reporting.value()) {
		for (unsigned f = 0; f < sz; f++)
		{
			int pref = (*afieldlist)[f].polyref;
			int utm_x = 484378 + m_TheLandscape->SupplyCentroidX(pref);
			int utm_y = 6335161 - m_TheLandscape-> SupplyCentroidY(pref);
			(*m_GooseFieldForageDataFile)
				<< m_SeasonNumber << '\t'
				<< Day << '\t'
				<< pref << '\t'
				<< utm_x << '\t'
				<< utm_y << '\t'
				<< (*afieldlist)[f].geese << '\t'
				<< (*afieldlist)[f].geeseTimed << '\t'
				<< (*afieldlist)[f].openness << '\t';
			for (int i = 0; i < gs_foobar; ++i)
			{
				(*m_GooseFieldForageDataFile)
					<< (*afieldlist)[f].geesesp[i] << '\t'
					<< (*afieldlist)[f].geesespTimed[i] << '\t'
					<< (*afieldlist)[f].roostdists[i] << '\t'
					<< (*afieldlist)[f].grass[i] << '\t';
			}
			(*m_GooseFieldForageDataFile)
				<< (*afieldlist)[f].grain << '\t'
				<< (*afieldlist)[f].maize << '\t'
				<< (*afieldlist)[f].vegtypechr << '\t'
				<< (*afieldlist)[f].vegheight << '\t'
				<< (*afieldlist)[f].digestability << '\t'
				<< (*afieldlist)[f].vegphase << '\t'
				<< (*afieldlist)[f].previouscrop << '\t'
				<< (*afieldlist)[f].lastsownveg << endl;
		}
	}
	delete afieldlist;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseEnergyRecordOutput()
{
	int day = m_TheLandscape->SupplyGlobalDate();
	int day_length = m_TheLandscape->SupplyDaylength();
		for (int i = 0; i < gs_foobar; ++i)
		{
			std::string gs = GooseToString((GooseSpecies)i);
			(*m_GooseEnergeticsDataFile)
				<< m_SeasonNumber << '\t'
				<< day << '\t'
				<< gs << '\t'
				<< m_ForagingTimeStats[i].get_meanvalue() << '\t'
				<< m_ForagingTimeStats[i].get_SE() << '\t'
				<< m_FlightDistanceStats[i].get_meanvalue() << '\t'
				<< m_FlightDistanceStats[i].get_SE() << '\t'
				<< m_DailyEnergyBudgetStats[i].get_meanvalue() << '\t'
				<< m_DailyEnergyBudgetStats[i].get_SE() << '\t'
				<< m_DailyEnergyBalanceStats[i].get_meanvalue() << '\t'
				<< m_DailyEnergyBalanceStats[i].get_SE() << '\t'
				<< day_length << endl;
		}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseWeightStatOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseWeightStatsFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t'
			<< m_WeightStats[i].get_meanvalue() << '\t'
			<< m_WeightStats[i].get_SE() << '\t'
			<< m_WeightStats[i].get_N() << endl;
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseIndLocCountOutput()
{
	for (int i = 0; i < gs_foobar; ++i)
	{
		std::string gs = GooseToString((GooseSpecies)i);
		(*m_GooseIndLocCountFile)
			<< m_SeasonNumber << '\t'
			<< m_TheLandscape->SupplyGlobalDate() << '\t'
			<< m_TheLandscape->SupplyDayInYear() << '\t'
			<< gs << '\t'
			<< m_IndividualForageLocationData[i].get_meanvalue() << '\t'
			<< m_IndividualForageLocationData[i].get_SE() << '\t'
			<< m_IndividualForageLocationData[i].get_N() << endl;
	}
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::
StateStatOutput() {
	int Day = m_TheLandscape->SupplyGlobalDate();
	(*m_StateStatsFile) << m_SeasonNumber << '\t' << Day << '\t' << m_StateStats.get_N() << endl;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::GooseLeaveReasonStatOutput()
{
	int day = m_TheLandscape->SupplyGlobalDate();
	int day_in_year = m_TheLandscape->SupplyDayInYear();
	for (int i = 0; i < gst_foobar; ++i)
	{
		std::string gst = GooseTypeToString((GooseSpeciesType)i);
		for (int j = 0; j < tolr_foobar; ++j)
		{
			(*m_GooseLeaveReasonStatsFile)
				<< m_SeasonNumber << '\t'
				<< day << '\t'
				<< day_in_year << '\t'
				<< gst << '\t'
				<< LeaveReasonToString((TTypeOfLeaveReason)j) << '\t'
				<< m_LeaveReasonStats[i][j].get_N() << endl;
		}
	}
}

//---------------------------------------------------------------------------
void Goose_Population_Manager::GoosePopulationDescriptionOutput()
{
	/**
	* Here the first job is to correct the standard output and produce a more detailed
	* description of the population, ie not counting families as one.
	*/
	int sz = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
	int pff = 0;
	for (int j = 0; j<sz; j++)
	{
		pff += dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootFamilyGroup][j])->GetGroupsize();
	}
	sz = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
	int bf = 0;
	for (int j = 0; j<sz; j++)
	{
		bf += dynamic_cast<Goose_Base*>(TheArray[gst_BarnacleFamilyGroup][j])->GetGroupsize();
	}
	sz = int(GetLiveArraySize(gst_GreylagFamilyGroup));
	int gf = 0;
	for (int j = 0; j<sz; j++)
	{
		gf += dynamic_cast<Goose_Base*>(TheArray[gst_GreylagFamilyGroup][j])->GetGroupsize();
	}
	(*m_GoosePopDataFile)
	  << m_SeasonNumber << '\t'
	  << m_TheLandscape->SupplyGlobalDate() << '\t'
	  << pff << '\t'
	  << int(GetLiveArraySize(gst_PinkfootNonBreeder)) << '\t'
	  << bf << '\t'
	  << int(GetLiveArraySize(gst_BarnacleNonBreeder)) << '\t'
	  << gf << '\t'
	  << int(GetLiveArraySize(gst_GreylagNonBreeder)) << '\t' 
	  << m_TheLandscape->SupplySnowDepth() << endl;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoImmigration()
{
	/**
	* Here we need to determine the inputs in terms of birds to the Danish simulation area and create all the necessary objects and object relationships.
	* This is a tiny bit tricky since it is important that all birds start the simulation with a sensible and complete characteristics.
	*
	* The first step is to determine whether there are birds of each type to immigrate today, and if so how many. We have the numbers to immigrate and the 
	* range of days over which this happens. For sake of simplicity and because we don't have other information we will assume an even distribution of birds 
	* over this period.
	*/
	
	for (int i = 0; i < gs_foobar; ++i)
	{
		m_migrationnumbers[i][0] = 0;
		m_migrationnumbers[i][1] = 0;
	}

	// Get the date as day in year
	int today = m_TheLandscape->SupplyDayInYear();
	// Pink foot immigration?
	if ((today >= cfg_goose_pf_arrivedatestart.value()) && (today <= cfg_goose_pf_arrivedateend.value()))
	{
		// there are some birds to come in today
		GetImmigrationNumbers(gs_Pinkfoot, true);  // true is fall immigration
	}
	/** Pinkfoot spring migration*/
	if (cfg_goose_pf_springmigrate.value()) {
		if (cfg_goose_pf_leavingdateend.value() > today) {
			if ((today >= cfg_goose_pf_springmigdatestart.value()) && (today <= cfg_goose_pf_springmigdateend.value()) && m_SeasonNumber > 0) {
				// there are some birds to come in today
				GetImmigrationNumbers(gs_Pinkfoot, false);  // false is spring immigration
			}
		}
	}
	// Barnacle immigration?
	if ((today >= cfg_goose_bn_arrivedatestart.value()) && (today <= cfg_goose_bn_arrivedateend.value()))
	{
		// there are some birds to come in today
		GetImmigrationNumbers(gs_Barnacle, true);
	}
	/** Barnacle spring migration*/
	if (cfg_goose_bn_springmigrate.value()) {
		if (cfg_goose_bn_leavingdateend.value() > today) {
			if ((today >= cfg_goose_bn_springmigdatestart.value()) && (today <= cfg_goose_bn_springmigdateend.value()) && m_SeasonNumber > 0) {
				// there are some birds to come in today
				GetImmigrationNumbers(gs_Barnacle, false);
			}
		}
	}
	// Greylag immigration?
	if ((today >= cfg_goose_gl_arrivedatestart.value()) && (today <= cfg_goose_gl_arrivedateend.value()))
	{
		// there are some birds to come in today
		GetImmigrationNumbers(gs_Greylag, true);
	}
	/** Greylag spring migration*/
	if (cfg_goose_gl_springmigrate.value()) {
		if (cfg_goose_gl_leavingdateend.value() > today) {
			if ((today >= cfg_goose_gl_springmigdatestart.value()) && (today <= cfg_goose_gl_springmigdateend.value()) && m_SeasonNumber > 0) {
				// there are some birds to come in today
				GetImmigrationNumbers(gs_Greylag, false);
			}
		}
	}

	struct_Goose* gs;
	gs = new struct_Goose;
	gs->m_GPM = this;
	gs->m_L = m_TheLandscape;
	
	int youngno = (int) m_youngdist.size(); // The length of the vector with brood sizes

	// Create the pinkfooted families
	for (int i=0; i<m_migrationnumbers[gs_Pinkfoot][0]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Pinkfoot].size());
		gs->m_roost = m_roosts[gs_Pinkfoot][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		gs->m_sex = true;
		gs->m_weight = cfg_goose_pf_baseweight.value();
		gs->m_family = true;
		gs->m_grpsize = 2 + m_youngdist[ random(youngno) ];
		CreateObjects(gst_PinkfootFamilyGroup,NULL,gs,1);
	}
	// Create the pink footed non-breeders
	for (int i = 0; i<m_migrationnumbers[gs_Pinkfoot][1]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Pinkfoot].size());
		gs->m_roost = m_roosts[gs_Pinkfoot][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		if (g_rand_uni() < cfg_goose_pf_sexratio.value()) gs->m_sex = true; else gs->m_sex = false; // true = male, false = female
		gs->m_weight = cfg_goose_pf_baseweight.value();
		gs->m_family = false;
		CreateObjects(gst_PinkfootNonBreeder,NULL,gs,1);
	}
	// Create the barnacle families
	for (int i = 0; i<m_migrationnumbers[gs_Barnacle][0]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Barnacle].size());
		gs->m_roost = m_roosts[gs_Barnacle][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		gs->m_sex = true;
		gs->m_weight = cfg_goose_bn_baseweight.value();
		gs->m_family = true;
		gs->m_grpsize = 2 + m_youngdist[ random( youngno) ];  // Values for Pinkfeet in lack of better data
		CreateObjects(gst_BarnacleFamilyGroup, NULL, gs, 1);
	}
	// Create the barnacle non-breeders
	for (int i = 0; i<m_migrationnumbers[gs_Barnacle][1]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Barnacle].size());
		gs->m_roost = m_roosts[gs_Barnacle][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		if (g_rand_uni() < cfg_goose_bn_sexratio.value()) gs->m_sex = true; else gs->m_sex = false; // true = male, false = female
		gs->m_weight = cfg_goose_bn_baseweight.value();
		gs->m_family = false;
		CreateObjects(gst_BarnacleNonBreeder,NULL,gs,1);
	}
	// Create the greylag families
	for (int i = 0; i<m_migrationnumbers[gs_Greylag][0]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Greylag].size());
		gs->m_roost = m_roosts[gs_Greylag][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		gs->m_sex = true;
		gs->m_weight = cfg_goose_gl_baseweight.value();
		gs->m_family = true;
		gs->m_grpsize = 2 + m_youngdist[ random( youngno) ];  // Values for Pinkfeet in lack of better data
		CreateObjects(gst_GreylagFamilyGroup, NULL, gs, 1);
	}
	// Create the greylag non-breeders
	for (int i = 0; i<m_migrationnumbers[gs_Greylag][1]; i++)
	{
		// Find the roost location
		int index = random((int) m_roosts[gs_Greylag].size());
		gs->m_roost = m_roosts[gs_Greylag][index];
		gs->m_x = gs->m_roost.m_x;
		gs->m_y = gs->m_roost.m_y;
		if (g_rand_uni() < cfg_goose_gl_sexratio.value()) gs->m_sex = true; else gs->m_sex = false; // true = male, false = female
		gs->m_weight = cfg_goose_gl_baseweight.value();
		gs->m_family = false;
		CreateObjects(gst_GreylagNonBreeder,NULL,gs,1);
	}
	delete gs;
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::DoEmigration()
{
	double emigrationchance;
	// Get the date as day in year
	int today = m_TheLandscape->SupplyDayInYear();
	// Pink foot emigration?
	if ((today >= cfg_goose_pf_leavingdatestart.value()) && (today <= cfg_goose_pf_leavingdateend.value()))
	{
		// Should do some emigration. The chance of this is determined by the date. This is a linearly scaling probability
		// equal to 1/(1+lastdate-today)
		emigrationchance = 1.0 / (1 + cfg_goose_pf_leavingdateend.value() - today);
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootFamilyGroup][j])->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_PinkfootNonBreeder));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootNonBreeder][j])->On_Emigrate();
		}
	}
	// Pinkfoot fall migration?
	if (cfg_goose_pf_fallmigrate.value() && (today >= cfg_goose_pf_fallmigdatestart.value()) && (today <= cfg_goose_pf_fallmigdateend.value())) {
		emigrationchance = cfg_goose_pf_fallmig_probability.value();
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootFamilyGroup][j])->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_PinkfootNonBreeder));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootNonBreeder][j])->On_Emigrate();
		}
	}
	// Barnacle emigration?
	if ((today >= cfg_goose_bn_leavingdatestart.value()) && (today <= cfg_goose_bn_leavingdateend.value()))
	{
		// Should do some emigration. The chance of this is determined by the date. This is a linearly scaling probability
		// equal to 1/(1+lastdate-today)
		emigrationchance = 1.0 / (1 + cfg_goose_bn_leavingdateend.value() - today);
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_BarnacleFamilyGroup][j])->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_BarnacleNonBreeder));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_BarnacleNonBreeder][j])->On_Emigrate();
		}
	}
	// Barnacle fall migration?
	if (cfg_goose_bn_fallmigrate.value() && (today >= cfg_goose_bn_fallmigdatestart.value()) && (today <= cfg_goose_bn_fallmigdateend.value())) {
		emigrationchance = cfg_goose_bn_fallmig_probability.value();
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_BarnacleFamilyGroup][j])->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_BarnacleNonBreeder));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_BarnacleNonBreeder][j])->On_Emigrate();
		}
	}
	// Greylag emigration?
	if ((today >= cfg_goose_gl_leavingdatestart.value()) && (today <= cfg_goose_gl_leavingdateend.value()))
	{
		// Should do some emigration. The chance of this is determined by the date. This is a linearly scaling probability
		// equal to 1/(1+lastdate-today)
		emigrationchance = 1.0 / (1 + cfg_goose_gl_leavingdateend.value() - today);
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_GreylagFamilyGroup));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_GreylagFamilyGroup][j])->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_GreylagNonBreeder));
		for (int j = 0; j<sz; j++)
		{
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_GreylagNonBreeder][j])->On_Emigrate();
		}
	}
	// Greylag fall migration?
	if (cfg_goose_gl_fallmigrate.value() && (today >= cfg_goose_gl_fallmigdatestart.value()) && (today <= cfg_goose_gl_fallmigdateend.value())) {
		emigrationchance = cfg_goose_gl_fallmig_probability.value();
		// Now we loop through all geese and set the behaviour to emigrate
		int sz = int(GetLiveArraySize(gst_GreylagFamilyGroup));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_GreylagFamilyGroup][j])->On_Emigrate();
		}
		sz = int(GetLiveArraySize(gst_GreylagNonBreeder));
		for (int j = 0; j<sz; j++) {
			if (g_rand_uni() <= emigrationchance) dynamic_cast<Goose_Base*>(TheArray[gst_GreylagNonBreeder][j])->On_Emigrate();
		}
	}
}
//---------------------------------------------------------------------------

int Goose_Population_Manager::ForageLocationInUse(int a_polyref)
{
	/**
	* \param [in] a_polyref is the polygon reference number that identifies the field for foraging
	* \return Returns and integer which is the index in the vector if the polyref is found, otherwise returns -1
	* Searches the whole m_GooseForageLocations vector for a match for a_polyref. Stops looking if one is found.
	*/
	int index = -1;
	int sz = (int) m_GooseForageLocations.size();
	for (int i=0; i< sz; i++)
	{
		if (m_GooseForageLocations[i].GetPolygonref() == a_polyref )
		{
			index = i;
			break;
		}
	}
	return index;
}
//---------------------------------------------------------------------------

int Goose_Population_Manager::NewForageLocation(GooseSpeciesType a_type, int a_number, int a_polyref )
{
	/**
	* \param [in] a_type is type of goose species to forage here in this first forage group
	* \param [in] a_number is the number of geese in this first forarge group
	* \param [in] a_polyref is the polygon reference number that identifies the field for foraging
	*
	* Adds the new forage location to the m_GooseForageLocations vector and adds the starting number of 
	* geese of the type specified. Calculates the distance to the closest roost for each species.
	*/
	double grain = m_TheLandscape->SupplyBirdSeedForage( a_polyref );  // grain/m2
	double maize = m_TheLandscape->SupplyBirdMaizeForage( a_polyref );  // kJ/m2
	double grazing[gs_foobar];
	// NB GooseSpeciesType >> 1 becomes a GooseSpecies
	for (int g = 0; g < gs_foobar; g++)
	{
		grazing[g] = m_TheLandscape->GetActualGooseGrazingForage(a_polyref, (GooseSpecies) g);
	}
	GooseActiveForageLocation aFL(a_type, a_number, a_polyref, (int) m_TheLandscape->SupplyPolygonArea(a_polyref), grain, maize, grazing, this);
	aFL.ResetGrazing();
	// Calculate the distance to the closest roost for each species
	int x = m_TheLandscape->SupplyCentroidX(a_polyref);
	int y = m_TheLandscape->SupplyCentroidY(a_polyref);
	for (int i = 0; i < gs_foobar; i++) {
		double dist = GetDistToClosestRoost(x, y, i);
		aFL.SetDistToClosestRoost(i, dist);
	}
	m_GooseForageLocations.push_back(aFL);
	int ind = (int)m_GooseForageLocations.size() - 1;
	return ind;
}
//---------------------------------------------------------------------------

 void Goose_Population_Manager::AddGeeseToForageLocation( GooseSpeciesType a_type, int a_index, int a_number )
 {
	 m_GooseForageLocations[a_index].AddGeese(a_type, a_number);
 }
 //---------------------------------------------------------------------------

 void Goose_Population_Manager::RemoveGeeseFromForageLocation(GooseSpeciesType a_type, int a_index, int a_number)
 {
	 m_GooseForageLocations[a_index].RemoveGeese(a_type, a_number);
 }
 //---------------------------------------------------------------------------

	 
	 void Goose_Population_Manager::AddGeeseToForageLocationP(GooseSpeciesType a_type, int a_polyref, int a_number)
 {
	/**f
	* First step is to get the forage location - then add the number of birds of a_type to it.
	*/
	int index = ForageLocationInUse( a_polyref );
	if (index == -1 )
	{
		NewForageLocation(a_type,a_number, a_polyref);
	}
	else m_GooseForageLocations[index].AddGeese(a_type, a_number);
 }
 //---------------------------------------------------------------------------

	 int Goose_Population_Manager::BirdsToShootAtPoly(int a_polyref /*, double &a_protectedpct */)
	 {
		 /**
		 * Assumes that a_protectedpct is initialised to 0.0 on entry. First step is to get the forage location - the find out how many birds are there.
		 */
		 int index = ForageLocationInUse(a_polyref);
		 if (index == -1) return 0;
		 // There are birds here - so how many and what type?
		 int geese = 0;
		 if (InPinkfootSeason())
		 {
			 geese += m_GooseForageLocations[index].GetBirds(gst_PinkfootFamilyGroup) + m_GooseForageLocations[index].GetBirds(gst_PinkfootNonBreeder);
		 }
		 if (InGreylagSeason())
		 {
			 geese += m_GooseForageLocations[index].GetBirds(gst_GreylagFamilyGroup) + m_GooseForageLocations[index].GetBirds(gst_GreylagNonBreeder);
		 }

		 //int protectedgeese = m_GooseForageLocations[index].GetBirds(gst_BarnacleFamilyGroup) + m_GooseForageLocations[index].GetBirds(gst_BarnacleNonBreeder);
		 //int birds = geese+protectedgeese;
		 //if (protectedgeese> 0) a_protectedpct = protectedgeese / (double)(birds); // else a_protectedpct = 0.0; This is not necessary since a_protectedpct should be 0.0 on entry
		 if (geese < 0) {
			 g_msg->Warn("Goose_Population_Manager::BirdsToShootAtPoly 0 or less then 0 geese to shoot.", geese);
			 exit(0);
		 }
		 return geese;
	 }
//---------------------------------------------------------------------------

void Goose_Population_Manager::BirdsShot(int a_polyref, int a_numbershot, GooseHunter* a_hunter)
 {
	 /**
	 * First step is to get the forage location - the we pick a_numbershot birds to kill.
	 * This is a bit problematic since we know the type of birds but not exactly which ones are here. To speed things up as much as possible we first determine
	 * which birds will be shot by picking randomly from the birds here.
	 * Next we need to make some 'BANG's and scare the birds away from this location and nearby locations.
	 * Note that barnacle geese cannot be shot and are excluded by the loop below.
	 * @todo Add input parameter to docu
	 */
	 int index = ForageLocationInUse(a_polyref);
	 if (index == -1)
	 {
		 g_msg->Warn("Goose_Population_Manager::BirdsShot - No birds at this location to shoot. Tried to kill ", a_numbershot);
		 exit(0);
	 }
	 int birds = (int)m_GooseForageLocations[index].GetHuntables(); // This is the number of geese not number of objects!
	 if (birds < 1) return; // Other hunters have shot all the birds already
	 GooseSpeciesType gst = gst_foobar;
	 int found = 0;
	 int counter = 0;
	 while (a_numbershot > 0)
	 {
		 counter++;
		 if (counter > 25)
		 {
			 int pffg = m_GooseForageLocations[index].GetBirds(gst_PinkfootFamilyGroup);
			 int pfnb = m_GooseForageLocations[index].GetBirds(gst_PinkfootNonBreeder);
			 int glfg = m_GooseForageLocations[index].GetBirds(gst_GreylagFamilyGroup);
			 int glnb = m_GooseForageLocations[index].GetBirds(gst_GreylagNonBreeder);
			 //cout << "Goose_Population_Manager::BirdsShot() Been shooting like crazy all morning - something is not quite right here...";
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - where are those damn birds?. Tried to kill ", a_numbershot);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - there where supposed to this many of them:", birds);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of pinkfoot familygroups:", pffg);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of pinkfoot nonbreeders:", pfnb);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of greylag familygroups:", glfg);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - consisting of greylag nonbreeders:", glnb);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - Today is day in year:", g_land->SupplyDayInYear());
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - in year:", g_land->SupplyYear());
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - Pinkfoot season?", m_PinkfootSeason);
			 g_msg->Warn("Goose_Population_Manager::BirdsShot() - in while() - Greylag season?", m_GreylagSeason);
			 exit(0);
		 }
		 found = 0;
		 int deadbird = (int)(g_rand_uni() * birds);
		 {
			 found = m_GooseForageLocations[index].GetBirds(gst_PinkfootFamilyGroup);
			 if (found > deadbird && InPinkfootSeason()) gst = gst_PinkfootFamilyGroup;
			 else {
				 deadbird -= found;
				 found = m_GooseForageLocations[index].GetBirds(gst_PinkfootNonBreeder);
				 if (found > deadbird && InPinkfootSeason()) gst = gst_PinkfootNonBreeder;
				 else {
					 deadbird -= found;
					 found = m_GooseForageLocations[index].GetBirds(gst_GreylagNonBreeder);
					 if (found > deadbird && InGreylagSeason()) gst = gst_GreylagNonBreeder;
					 else {
						 deadbird -= found;
						 found = m_GooseForageLocations[index].GetBirds(gst_GreylagFamilyGroup);
						 if (found > deadbird && InGreylagSeason()) gst = gst_GreylagFamilyGroup;
					 }
				 }
			 }
		 }
		 // gst has the enum value identifying the type of shot bird.
		 int sz = int(GetLiveArraySize(gst));
		 for (int i = 0; i < sz; i++)
		 {
			 if (TheArray[gst][i]->SupplyPolygonRef() == a_polyref)
			 {
				 if (TheArray[gst][i]->GetCurrentStateNo() != -1)
				 {
					 // Kill the bird
					 TheArray[gst][i]->KillThis();
					 // Tell the hunter
					 a_hunter->OnShotABird(gst, a_polyref);
					 a_numbershot--;
					 break;
				 }
			 }
		 }
	 }
}
//---------------------------------------------------------------------------

void Goose_Population_Manager::BangAtPoly(int a_polyref)
{
	/**
	* Here we have to find all the geese that are near to this polygon - then tell them they are scared of this location.
	* Unfortunately there is no easy way to do this except trawl through all geese and test the distance they are from the bang
	* at the moment this method is called.
	* We assume the bang occurs at the centroid since we do not have a particular location for this within the polygon.
	* Centroid distance to the location of each bird is checked and if under the maximum threat distance then we tell the bird they are scared.
	* An alternative approach would be to link the size of scare to the distance. This is provided as an alternative assuming __LINKGOOSESCARETODISTANCE is defined.
	*/
	int polyX = m_TheLandscape->SupplyCentroidX(a_polyref);
	int polyY = m_TheLandscape->SupplyCentroidY(a_polyref);
	int maxscaredist = cfg_goose_MaxScareDistance.value();
	for (unsigned i=0; i<(m_ListNameLength); i++)
	{
		unsigned sz = (unsigned) TheArray[i].size();
		for (unsigned j=0; j<sz; j++)
		{
			APoint ap = TheArray[i][j]->SupplyPoint();
			int dist = g_AlmassMathFuncs.CalcDistPythagorasApprox( polyX, polyY, ap.m_x, ap.m_y );
			if (dist < maxscaredist)
			{
				Goose_Base* gb = dynamic_cast<Goose_Base*>(TheArray[i][j]);
#ifdef __LINKGOOSESCARETODISTANCE

				gb->On_Bang(a_polyref, 1.0-(double) (dist/maxscaredist));
#else
				gb->On_Bang(a_polyref);
#endif
			}
		}
	}
}
//---------------------------------------------------------------------------
/**
* When leaving the roost in the morning we have the option to either follow another bird or go exploring on our own./n
* If we follow another bird this function will find a bird to follow (a leader). It is a prerequisite that the leader
* is from the same roost as the bird following and that the leader have found a suitable forage location.
* \param [in] a_roost The roost of the follower as a point.
* \param [in] a_species The species of the follower.
* \return A pointer to the leader.
*/
Goose_Base* Goose_Population_Manager::GetLeader(APoint a_roost, GooseSpecies a_species)
{
	// Need to start a random point in our list of birds
	// We know the total number of bird objects of the species, so assume they are one circular long list 
	// 
	Goose_Base* GBp;
	int sz1 = 0;
	int sz2 = 0;
	int base = (int)a_species * 2;  // To get the position of the speciesFamilyGroup in TheArray
	int base1 = 1 + ((int)a_species * 2);  // the speciesNonBreeder is always the next one
	switch (a_species) {
	case gs_Pinkfoot:
		sz1 = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
		sz2 = int(GetLiveArraySize(gst_PinkfootNonBreeder));
		break;
	case gs_Barnacle:
		sz1 = int(GetLiveArraySize(gst_BarnacleFamilyGroup));
		sz2 = int(GetLiveArraySize(gst_BarnacleNonBreeder));
		break;
	case gs_Greylag:
		sz1 = int(GetLiveArraySize(gst_GreylagFamilyGroup));
		sz2 = int(GetLiveArraySize(gst_GreylagNonBreeder));
		break;
	};
	int total = sz1 + sz2;
	int start = random(total);
	for (int g = 0; g < total; g++)  // Loops until a goose which does not return -1 for the forage location index is found
	{
		int index;
		if (start == total) start = 0;
		if (start < sz1) {
			index = start;
			GBp = dynamic_cast<Goose_Base*>(TheArray[base][index]);
			if ((GBp->GetRoost().m_x == a_roost.m_x) && (GBp->GetRoost().m_y == a_roost.m_y) && (GBp->GetForageLocIndex() != -1)) {
				return GBp;
			}
		}
		else {
			index = start - sz1;
			GBp = dynamic_cast<Goose_Base*>(TheArray[base1][index]);
			if ((GBp->GetRoost().m_x == a_roost.m_x) && (GBp->GetRoost().m_y == a_roost.m_y) && (GBp->GetForageLocIndex() != -1)) {
				return GBp;
			}
		}
		start++;
	}
	return NULL;
}

/** @todo add documentation */
int Goose_Population_Manager::GetForageLocIndex( GooseSpecies a_species, int a_x, int a_y ) {
	int forageindex = -1;
	std::vector<int> fields;
	int sz = (int)m_GooseForageLocations.size();
	if (sz > 0) {
		for (int i = 0; i < sz; i++) {
			int birds = m_GooseForageLocations[ i ].GetBirds( a_species );
			if (birds > 0) {
				int index = m_GooseForageLocations[ i ].GetPolygonref();
				fields.push_back( index );
			}
		}
		if (fields.size() != 0) {
			// Find the distances to those forage locations
			int answer = random(int(fields.size()));
			int max_dist = 43266;  // diagonal of Vejlerne
			int fx, fy;
			for (int i = 0; i<fields.size(); i++) {
				fx = m_TheLandscape->SupplyCentroidX( fields[ i ] );
				fy = m_TheLandscape->SupplyCentroidY( fields[ i ] );
				int di = g_AlmassMathFuncs.CalcDistPythagoras( fx, fy, a_x, a_y );
				if (g_rand_uni() < exp(-(di / max_dist) * cfg_goose_dist_weight_power.value()))
				{
					answer = i;
				} 
			}
			int polyref = fields[ answer ];
			forageindex = ForageLocationInUse( polyref );
		}
	}
	return forageindex;
}


void Goose_Population_Manager::RemoveMaxForageKj( double forage, TTypeOfMaxIntakeSource a_maxintakesource, int m_myForageIndex ) {
	switch (a_maxintakesource) {
		case tomis_grain:
			RemoveGrainKJ( forage, m_myForageIndex );
			break;
		case tomis_maize:
			RemoveMaizeKJ( forage, m_myForageIndex );
			break;
		case tomis_grass:
			Graze( forage, m_myForageIndex );
			break;
		case tomis_foobar:
			m_TheLandscape->Warn( "Goose_Population_Manager::RemoveMaxForageKj", " - Somebody visited the foobar. Not allowed!" );
			exit( 0 );
		default:
			;
	}
}

void Goose_Population_Manager::GetImmigrationNumbers(GooseSpecies a_goose, bool fall)
{
	if (a_goose == gs_Pinkfoot && fall)
	{
		int pfnum = cfg_goose_pf_startnos.value();
		int pfyoung = (int)floor(pfnum * cfg_goose_pf_young_proportion.value());
		int	pffam = (int)floor(pfyoung / 4); // On average the families have 4 young
		int	pfnb = pfnum - (pffam + pfyoung);  // The familes are then two adults plus their young

		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (pffam / ((1 + cfg_goose_pf_arrivedateend.value()) - cfg_goose_pf_arrivedatestart.value())));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (pfnb / ((1 + cfg_goose_pf_arrivedateend.value()) - cfg_goose_pf_arrivedatestart.value())));
	}
	if (a_goose == gs_Pinkfoot && !fall)
	{
		int pfnum = cfg_goose_pf_springmignos.value();
		int pfyoung = (int)floor(pfnum * cfg_goose_pf_young_proportion.value());
		int	pffam = (int)floor(pfyoung / 4); // On average the families have 4 young
		int	pfnb = pfnum - (pffam + pfyoung);  // The familes are then two adults plus their young

		int start = cfg_goose_pf_springmigdatestart.value();
		int end = cfg_goose_pf_springmigdateend.value();
		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (pffam / ((1 + end) - start)));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (pfnb / ((1 + end) - start)));
	}
	if (a_goose == gs_Barnacle && fall)
	{
		int bnnum = cfg_goose_bn_startnos.value();
		int bnyoung = (int)floor(bnnum * cfg_goose_bn_young_proportion.value());
		int	bnfam = (int)floor(bnyoung / 4); // On average the families have 4 young
		int	bnnb = bnnum - (bnfam + bnyoung);  // The familes are then two adults plus their young

		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (bnfam / ((1 + cfg_goose_bn_arrivedateend.value()) - cfg_goose_bn_arrivedatestart.value())));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (bnnb / ((1 + cfg_goose_bn_arrivedateend.value()) - cfg_goose_bn_arrivedatestart.value())));
	}
	if (a_goose == gs_Barnacle && !fall)
	{
		int bnnum = cfg_goose_bn_springmignos.value();
		int bnyoung = (int)floor(bnnum * cfg_goose_bn_young_proportion.value());
		int	bnfam = (int)floor(bnyoung / 4); // On average the families have 4 young
		int	bnnb = bnnum - (bnfam + bnyoung);  // The familes are then two adults plus their young

		int start = cfg_goose_bn_springmigdatestart.value();
		int end = cfg_goose_bn_springmigdateend.value();
		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (bnfam / ((1 + end) - start)));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (bnnb / ((1 + end) - start)));
	}
	if (a_goose == gs_Greylag && fall)
	{
		int glnum = cfg_goose_gl_startnos.value();
		int glyoung = (int)floor(glnum * cfg_goose_gl_young_proportion.value());
		int	glfam = (int)floor(glyoung / 4); // On average the families have 4 young
		int	glnb = glnum - (glfam + glyoung);  // The familes are then two adults plus their young

		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (glfam / ((1 + cfg_goose_gl_arrivedateend.value()) - cfg_goose_gl_arrivedatestart.value())));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (glnb / ((1 + cfg_goose_gl_arrivedateend.value()) - cfg_goose_gl_arrivedatestart.value())));
	}
	if (a_goose == gs_Greylag && !fall)
	{
		int glnum = cfg_goose_gl_springmignos.value();
		int glyoung = (int)floor(glnum * cfg_goose_gl_young_proportion.value());
		int	glfam = (int)floor(glyoung / 4); // On average the families have 4 young
		int	glnb = glnum - (glfam + glyoung);  // The familes are then two adults plus their young

		int start = cfg_goose_gl_springmigdatestart.value();
		int end = cfg_goose_gl_springmigdateend.value();
		m_migrationnumbers[a_goose][0] = (int)floor(0.5 + (glfam / ((1 + end) - start)));
		m_migrationnumbers[a_goose][1] = (int)floor(0.5 + (glnb / ((1 + end) - start)));
	}
}


// Weight stats
void Goose_Population_Manager::RecordWeight(double a_weight, GooseSpecies a_species)
{
	m_WeightStats[a_species].add_variable(a_weight);
}

void Goose_Population_Manager::ClearGooseWeightStats() {
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_WeightStats[(GooseSpecies)i].ClearData();
	}
}

// Habitat use stats
void Goose_Population_Manager::RecordHabitatUse(int a_habitatype, GooseSpecies a_species, int a_count)
{
	m_HabitatUseStats[a_species * tomis_foobar + a_habitatype] += a_count;
}

// Habitat use stats
void Goose_Population_Manager::RecordHabitatUseFieldObs(int a_habitatype, GooseSpecies a_species, int a_count)
{
	m_HabitatUseFieldObsStats[a_species * tomis_foobar + a_habitatype] += a_count;
}

void Goose_Population_Manager::ClearGooseHabitatUseStats() {
	for (unsigned i = 0; i < gs_foobar*tomis_foobar; i++)
	{
		m_HabitatUseStats[i] = 0;
	}
}

void Goose_Population_Manager::ClearGooseHabitatUseFieldObsStats() {
	for (unsigned i = 0; i < gs_foobar*tomis_foobar; i++)
	{
		m_HabitatUseFieldObsStats[i] = 0;
	}
}

// Foraging time stats
void Goose_Population_Manager::RecordForagingTime(int a_time, GooseSpecies a_species)
{
	m_ForagingTimeStats[a_species].add_variable(a_time);
}

void Goose_Population_Manager::ClearGooseForagingTimeStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_ForagingTimeStats[(GooseSpecies)i].ClearData();
	}
}

// Flight distance stats
void Goose_Population_Manager::RecordFlightDistance(int a_distance, GooseSpecies a_species)
{
	m_FlightDistanceStats[a_species].add_variable(a_distance);
}

void Goose_Population_Manager::ClearGooseFlightDistanceStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_FlightDistanceStats[(GooseSpecies)i].ClearData();
	}
}

// Daily energy budget stats
void Goose_Population_Manager::RecordDailyEnergyBudget(int a_deb, GooseSpecies a_species)
{
	m_DailyEnergyBudgetStats[a_species].add_variable(a_deb);
}

void Goose_Population_Manager::ClearGooseDailyEnergyBudgetStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_DailyEnergyBudgetStats[(GooseSpecies)i].ClearData();
	}
}

// Daily energy balance stats
void Goose_Population_Manager::RecordDailyEnergyBalance(int a_balance, GooseSpecies a_species)
{
	m_DailyEnergyBalanceStats[a_species].add_variable(a_balance);
}

void Goose_Population_Manager::ClearGooseDailyEnergyBalanceStats() 
{
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_DailyEnergyBalanceStats[(GooseSpecies)i].ClearData();
	}
}

// Forage location stats
void Goose_Population_Manager::RecordIndForageLoc(double a_count, int a_groupsize, GooseSpecies a_species)
{
	for (int i=0; i<a_groupsize; i++) m_IndividualForageLocationData[a_species].add_variable(a_count);
}

void Goose_Population_Manager::ClearIndLocCountStats() {
	for (unsigned i = 0; i < gs_foobar; i++)
	{
		m_IndividualForageLocationData[(GooseSpecies)i].ClearData();
	}
}

// State stats
void Goose_Population_Manager::RecordState() {
	m_StateStats.add_variable(1);
}

void Goose_Population_Manager::ClearStateStats() {
		m_StateStats.ClearData();
}

void Goose_Population_Manager::RecordLeaveReason(TTypeOfLeaveReason a_leavereason, GooseSpeciesType a_speciestype) {
	m_LeaveReasonStats[a_speciestype][a_leavereason].add_variable(1);
}

void Goose_Population_Manager::ClearGooseLeaveReasonStats() {
	for (unsigned i = 0; i < gst_foobar; i++)
	{
		for (unsigned j = 0; j < tolr_foobar; j++)
		{
			m_LeaveReasonStats[(GooseSpeciesType)i][(TTypeOfLeaveReason)j].ClearData();
		}
	}
}

bool Goose_Population_Manager::InHuntingSeason(int a_day, GooseSpecies a_species) {
	/**
	*  The goose hunting season is complicated because it runs over a year boundary (or it can)
	* Particularly the first year of the simulation is tricky, because we start out in January
	* which can be legal hunting season, but the geese don't arrive until the fall.
	*/
	if (GetSeasonNumber() == 0) {

		return false;  // We don't want them to start in January in the first year of the sim.
	}
	if (a_species == gs_Pinkfoot)
	{
		int pfstart = cfg_goose_pinkfootopenseasonstart.value();
		int pfend = cfg_goose_pinkfootopenseasonend.value();
		if (pfstart > pfend) {
			if (a_day < pfstart && a_day > pfend) return false;
			else {
				return true;  // must be in the first year after the start of season
			}
		}
		else {
			// Season all in one year
			if (a_day < pfstart || a_day > pfend) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	else if (a_species == gs_Greylag)
	{
		int glstart = cfg_goose_greylagopenseasonstart.value();
		int glend = cfg_goose_greylagopenseasonend.value();
		if (glstart > glend) {
			if (a_day < glstart && a_day > glend) return false;
			else {
				return true;  // must be in the first year after the start of season
			}
		}
		else {
			// Season all in one year
			if (a_day < glstart || a_day > glend) {
				return false;
			}
			else {
				return true;
			}
		}
	}
	else
	{
		g_msg->Warn(WARN_FILE, "Goose_Population_Manager::InHuntingSeason: ""Species error", "");
		exit(1);
	}
}


void Goose_Population_Manager::WriteHeaders(ofstream *a_file, std::vector<std::string> a_headers)
{
	for (int i = 0; i < a_headers.size(); ++i)
	{
		if (i <= a_headers.size() - 2)
		{
			(*a_file) << a_headers[i];
			(*a_file) << '\t';
		}
		else
		{
			(*a_file) << a_headers[i];
			(*a_file) << endl;
		}
	}
}


void Goose_Population_Manager::TheAOROutputProbe() {
	/** 
	* Here the problem is that we can't use the standard outputs because of the special
	* goose population structures. So we need some special adaptions to the probe.
	* This is done by extending the AOR_Probe class and using this, one for each species
	* We also need to do this only once per day, so since the main calling loops use days
	* as triggers we need to return doing nothing if the time is wrong.
	*/
	if (GetDayTime() == cfg_gooseAORtime.value())
	{
		m_AOR_Pinkfeet->DoProbe(gs_Pinkfoot);
		m_AOR_Barnacles->DoProbe(gs_Barnacle);
		m_AOR_Greylags->DoProbe(gs_Greylag);
	}
}

void Goose_Population_Manager::XYDump() {
	// Families
	int total = int(GetLiveArraySize(gst_PinkfootFamilyGroup));
	int x, y, poly, index, gaflx, gafly, gaflpoly;
	//int w = m_TheLandscape->SupplySimAreaWidth();
	//int h = m_TheLandscape->SupplySimAreaHeight();
	
	for (int j = 0; j<total; j++)      
	{
		int group_size = dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootFamilyGroup][j])->GetGroupsize();
		for (int k = 0; k < group_size; k++)
		{
			x = dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootFamilyGroup][j])->Supply_m_Location_x();
			y = dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootFamilyGroup][j])->Supply_m_Location_y();
			poly = m_TheLandscape->SupplyPolyRef(x, y);
			index = dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootFamilyGroup][j])->GetForageLocIndex();
			if (index != -1) {
				GooseActiveForageLocation* gafl = GetForageLocation(index);
				gaflpoly = gafl->GetPolygonref();
				gaflx = m_TheLandscape->SupplyCentroidX(gaflpoly);
				gafly = m_TheLandscape->SupplyCentroidY(gaflpoly);
			}
			else {
				gaflpoly = -1;
				gaflx = -1;
				gafly = -1;
			}
			(*m_GooseXYDumpFile) << x << '\t' << y << '\t' << poly << '\t' <<
				gaflx << '\t' << gafly << '\t' << gaflpoly <<  endl;
		}
	}
	// Non-breeders
	total = int(GetLiveArraySize(gst_PinkfootNonBreeder));
	
	for (int j = 0; j<total; j++)
	{
		x = dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootNonBreeder][j])->Supply_m_Location_x();
		y = dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootNonBreeder][j])->Supply_m_Location_y();
		poly = m_TheLandscape->SupplyPolyRef(x, y);
		index = dynamic_cast<Goose_Base*>(TheArray[gst_PinkfootNonBreeder][j])->GetForageLocIndex();
		if (index != -1) {
			GooseActiveForageLocation* gafl = GetForageLocation(index);
			gaflpoly = gafl->GetPolygonref();
			gaflx = m_TheLandscape->SupplyCentroidX(gaflpoly);
			gafly = m_TheLandscape->SupplyCentroidY(gaflpoly);
		}
		else {
			gaflpoly = -1;
			gaflx = -1;
			gafly = -1;
		}
		(*m_GooseXYDumpFile) << x << '\t' << y << '\t' << poly << '\t' <<
			gaflx << '\t' << gafly << '\t' << gaflpoly << endl;
	}
}

std::string Goose_Population_Manager::GooseTypeToString(GooseSpeciesType a_gst)
{
	switch (a_gst)
	{
	case gst_PinkfootFamilyGroup:
		return "pinkfoot_family";
	case gst_PinkfootNonBreeder:
		return "pinkfoot_nonbreeder";
	case gst_BarnacleFamilyGroup:
		return "barnacle_family";
	case gst_BarnacleNonBreeder:
		return "barnacle_nonbreeder";
	case gst_GreylagFamilyGroup:
		return "greylag_family";
	case gst_GreylagNonBreeder:
		return "greylag_nonbreeder";
	default:
		return "gst_foobar";
	}
}

std::string Goose_Population_Manager::GooseToString(GooseSpecies a_gs)
{
	switch (a_gs)
	{
	case gs_Pinkfoot:
		return "pinkfoot";
	case gs_Barnacle:
		return "barnacle";
	case gs_Greylag:
		return "greylag";
	default:
		return "gs_foobar";
	}
}

std::string Goose_Population_Manager::IntakeSourceToString(TTypeOfMaxIntakeSource a_intake_source)
{
	switch (a_intake_source)
	{
	case tomis_grass:
		return "grass";
	case tomis_sowncrop:
		return "winter_cereal";
	case tomis_maize:
		return "maize";
	case tomis_grain:
		return "grain";
	default:
		return "tomis_foobar";
	}
}

std::string Goose_Population_Manager::LeaveReasonToString(TTypeOfLeaveReason a_leave_reason)
{
	switch (a_leave_reason)
	{
	case tolr_migration:
		return "migration";
	case tolr_bodycondition:
		return "body_condition";
	case tolr_leanweight:
		return "lean_weight";
	case tomis_grain:
		return "grain";
	default:
		return "tolr_foobar";
	}
}

