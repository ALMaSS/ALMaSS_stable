#ifndef OLIVEMOTH_H
#define OLIVEMOTH_H


class LaceWing_Population_Manager;
class LaceWing;

typedef enum
{
      toLaceWings_InitialState=0,
      toLaceWings_Develop,
      toLaceWings_Progress,
      toLaceWings_Move,
      toLaceWings_Lay,
      toLaceWings_Die
} TTypeOfLaceWingState;


class LaceWing_Population_Manager : public Population_Manager {
 public:
  LaceWing_Population_Manager(Landscape* L);
  virtual ~LaceWing_Population_Manager(void);

  //  void CreateObjects(int ob_type, TAnimal *pvo, struct_LaceWing* data, int number);
  /** \brief Adds a new animal of type T. Must pass an existing
      animal to initialise from */

#define PM_REUSE
  
#ifdef PM_CHUNKING
  template <class T>
    void Add(TAnimal &animal) {
  }
#endif
  
#ifdef PM_SIMPLE
  template <class T>
    void Add(TAnimal &animal) {
    int ob_type=T::myID;
    auto obsize=TheArray[ob_type].size();
    auto obcap=TheArray[ob_type].capacity();
    cout << "Adding: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
    //TAnimal* newAnimal = new T(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
    auto newAnimal = new T(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
    TheArray[ob_type].push_back(newAnimal);
    IncLiveArraySize(ob_type);
  }
#endif
  
#ifdef PM_REUSE
  template <class T>
    void Add(TAnimal &animal) {
    int ob_type=T::myID;
    auto obsize=TheArray[ob_type].size();
    auto obcap=TheArray[ob_type].capacity();
    if (unsigned(TheArray[ob_type].size())>GetLiveArraySize(ob_type)) {
      cout << "Reusing: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
      dynamic_cast<T*>(TheArray[ob_type][GetLiveArraySize(ob_type)])->ReInit(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
      IncLiveArraySize(ob_type);
    }
    else {
      cout << "Adding: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
      TAnimal* newAnimal = new T(animal.Supply_m_Location_x(), animal.Supply_m_Location_y(), m_TheLandscape, this);
      TheArray[ob_type].push_back(newAnimal);
      IncLiveArraySize(ob_type);
    }
  }
#endif
  
 protected:
  /** \brief  Things to do before anything else at the start of a timestep  */
  virtual void DoFirst() {}
  /** \brief Things to do before the Step */
  virtual void DoBefore(){}
  /** \brief Things to do before the EndStep */
  virtual void DoAfter(){}
  /** \brief Things to do after the EndStep */
  virtual void DoLast(){}
};

class LaceWing : public TAnimal {
 public:
  LaceWing(int a_x, int a_y, Landscape* p_L, LaceWing_Population_Manager* p_PM);
  virtual ~LaceWing();
  void ReInit(int a_x, int a_y, Landscape* p_L, LaceWing_Population_Manager* p_PM);
  //  inline void FillData(struct_LaceWing* a_data);
  //  inline void FillData(struct_LaceWing* a_data);
    
  /** \brief Behavioural state development */
  virtual TTypeOfLaceWingState st_Develop( void );// { return toLaceWings_Develop;}
  /** \brief Behavioural state movement */
  virtual TTypeOfLaceWingState st_Movement( void ) { return toLaceWings_Develop;}
  virtual TTypeOfLaceWingState st_Progress(void);
  virtual void nextStage() {}
  /** \brief Behavioural state dying */
  void st_Dying( void );
  /** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
  virtual void BeginStep(void) {} // NB this is not used in the LaceWing code
  /** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
  virtual void Step(void);
  /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
  virtual void EndStep(void) {} // NB this is not used in the LaceWing code
  /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
  void setProgress(float p) {
    dev_progress=p;
  }
  
  //float FINISH=a*pow(T,b)*24;
  //   void setDevFinish() {  dev_finish=devparam_a*pow(20.0,devparam_b);}
  static const int myID = -1;
  
 protected:
  /** \brief Variable to record current behavioural state */
   TTypeOfLaceWingState m_CurrentOMState;
   LaceWing_Population_Manager* m_OurPopulationManager;
   /** \brief Holds the age of the bee in days */
   unsigned m_Age;
   float dev_progress, devparam_a,devparam_b;
};

class LaceWing;
class LaceWingEgg;
class LaceWingLarva;
class LaceWingPupa;
class LaceWingAdult;

class LaceWingEgg : public LaceWing {
 public:
  // Methods
  /** \brief LaceWing_Egg constructor */
  LaceWingEgg(int  a_x , int  a_y , Landscape* p_L, LaceWing_Population_Manager* p_BPM);
  /** \brief LaceWing_Egg destructor */
  virtual ~LaceWingEgg();
  virtual void nextStage() {
    m_OurPopulationManager->Add<LaceWingLarva>(*this);
  }
  //  TTypeOfLaceWingState st_Hatch(void);
  //TTypeOfLaceWingState st_Develop(void);

  static const int myID = -1;
  //virtual void Step(void);
};

class LaceWingLarva : public LaceWing {
 public:
  // Methods
  /** \brief LaceWing_Larva constructor */
  LaceWingLarva(int  a_x , int  a_y , Landscape* p_L, LaceWing_Population_Manager* p_BPM);
  /** \brief LaceWing_Larva destructor */
  virtual ~LaceWingLarva();
  virtual void nextStage() {
    m_OurPopulationManager->Add<LaceWingPupa>(*this);
  }
  //TTypeOfLaceWingState st_Pupate(void);
  //TTypeOfLaceWingState st_Develop(void);

  static const int myID = -1;
  //virtual void Step(void);
};

class LaceWingPupa : public LaceWing {
 public:
  // Methods
  /** \brief LaceWing_Pupa constructor */
  LaceWingPupa(int  a_x , int  a_y , Landscape* p_L, LaceWing_Population_Manager* p_BPM);
  /** \brief ReInit for object pool */
  //  void ReInit(int  a_x, int  a_y, Landscape* p_L, LaceWing_Population_Manager* p_BPM);
  /** \brief LaceWing_Pupa destructor */
  virtual ~LaceWingPupa();

  virtual void nextStage() {
    m_OurPopulationManager->Add<LaceWingAdult>(*this);
  }

  //TTypeOfLaceWingState st_Emerge(void);
  //TTypeOfLaceWingState st_Develop(void);

  static const int myID = -1;
  //virtual void Step(void);
};

class LaceWingAdult : public LaceWing {
 public:
  // Methods
  /** \brief LaceWing_Adult constructor */
  LaceWingAdult(int  a_x , int  a_y , Landscape* p_L, LaceWing_Population_Manager* p_BPM);
  /** \brief ReInit for object pool */
  //void ReInit(int  a_x, int  a_y, Landscape* p_L, LaceWing_Population_Manager* p_BPM);
  /** \brief LaceWing_Adult destructor */
  virtual ~LaceWingAdult();

  virtual TTypeOfLaceWingState st_Movement( void );
  //  TTypeOfLaceWingState st_Dying(void);
  virtual void nextStage() {
    m_CurrentStateNo=-1;
  }


  virtual TTypeOfLaceWingState lay() {
    cout << "Not making generic Egg. I shouldn't happen\n";
    return toLaceWings_Develop;
  }

  TTypeOfLaceWingState st_Develop(void);
  TTypeOfLaceWingState st_Lay(void);
  static const int myID = -1;
  virtual void Step(void);
};

#endif
