/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Dormouse_Population_Manager.h 
\brief <B>The header file for the dormouse population manager classes</B>
*/
/**  \file Dormouse_Population_Manager.h
Version of December 2013 \n
By Lars Dalby & Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef Dormouse_Population_ManagerH
#define Dormouse_Population_ManagerH
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

class Dormouse_Base;
class Dormouse_Population_Manager;
struct struct_Dormouse;

//------------------------------------------------------------------------------


typedef enum
{
	dob_JuvenileMale = 0,
	dob_JuvenileFemale,
	dob_Male,
	dob_Female,
	dob_foobar
}DormouseObject;

/**
\brief
The class to handle all population related matters
*/
class Dormouse_Population_Manager : public Population_Manager
{
protected:
	// Methods
	/** \brief  Things to do before anything else at the start of a timestep  */
	virtual void DoFirst();
	/** \brief Things to do before the Step */
	virtual void DoBefore(){}
	/** \brief Things to do before the EndStep */
	virtual void DoAfter(){}
	/** \brief Things to do after the EndStep */
	virtual void DoLast(){}
	/** \brief @todo LADA: Enter explanation here */
	double AssessHabitat(int p_Polyref);
	/** \brief Assess the quality of habitat at p_Polyref */
	vector<double> DormouseHabitatBaseQualities;
	// Attributes
	int YoungProducedToday, JuvsProducedToday;
	/** \brief @todo LADA: Enter explanation here*/
	bool BarrierSearch(int F_x, int F_y, int M_x, int M_y);
	/** \brief Ensure that dormice only start out in the simulation in suitable locations*/
	bool SuitableStartingLocation(int a_x, int a_y);
public:
// Methods
   /** \brief Dormouse_Population_Manager Constructor */
   Dormouse_Population_Manager(Landscape* L);
   /** \brief Dormouse_Population_Manager Destructor */
   virtual ~Dormouse_Population_Manager (void);
   /** \brief Called upon initialization of the simulation. Sets up output files. */
   virtual void Init(void);
   /** \brief Method for creating a new individual, used when starting the simulation */
   void InitialCreateObjects(DormouseObject ob_type, TAnimal* pvo, struct_Dormouse* a_data, int a_number);
   /** \brief Method for creating a new individual */
   void CreateObjects(DormouseObject ob_type, TAnimal* pvo, struct_Dormouse* a_data, int a_number);
   /** \brief Short method to add the number of juveniles maturing to young to the number of young produced on the current day */
   void AddToYoung(int young) { YoungProducedToday += young; };
   /** \brief Short method to add the number of juveniles to the number of juveniles produced on the current day */
   void AddToJuvs(int juvs) { JuvsProducedToday += juvs; };
   /** \brief Method to get the habitat quality from the predefined vector of habitat quality scores */
    double GetHabitatQuality(int a_index) { return DormouseHabitatBaseQualities[a_index]; };
   /** \brief The locations of dormice currently in the simulation  @todo LADA: Check with Chris that this is right*/
   IDMap<TAnimal*> *DormouseMap;
   //Dormouse_Female* FindClosestFemale(int p_x, int p_y, int p_steps);  //**@todo LADA: FindClosestFemale not implemented yet... */
   Dormouse_Male* FindClosestMale(int p_x, int p_y, int p_steps);
   /** \brief Function to get the number of dormice within a specified range (typically a territory) */
   int SupplyHowManyDormice(unsigned p_x, unsigned p_y, unsigned p_size);
   /** \brief Searches the territory for any older females.*/
   bool SupplyOlderFemales(unsigned p_x, unsigned p_y, unsigned p_Age, unsigned p_range);
   /** \brief Searches the territory for any older males.*/
   bool SupplyOlderMales(unsigned p_x, unsigned p_y, unsigned p_Age, unsigned p_range);
   /** \brief Searches the territory for mature territory holding females. */
   int SupplyCountFemales(unsigned p_x, unsigned p_y, unsigned p_TerrRange);
   /** \brief Finds and mates with any females within p_range. */
   void SendMessageMated(unsigned p_x, unsigned p_y, unsigned p_range);
   /** \brief Output file with proportions of territory holding individuals. */
   ofstream* m_DormouseTerritoryFile;
   void TheTerritoryProbe();
   /** \brief Output file with proportions of pregnant females. */
   ofstream* m_DormousePregnancyFile;
   void ThePregnancyProbe();
   /** \brief Output file with proportions of pregnant females. */
   ofstream* m_DormouseMateFile;
   void TheMateProbe();
   /** \brief Output file with the number of juveniles born each day. */
   ofstream* m_DormouseJuvenileFile;
   void TheJuvenileProbe();   
   /** \brief Output file with the age of every individual each day. */
   ofstream* m_DormouseAgeFile;
   void TheAgeProbe();
   /** \brief Output file with the hibernation status of every individual each day. */
   ofstream* m_DormouseHibernationFile;
   void TheHibernationProbe();
   /** \breif Output probe: Ripley. */
   virtual void TheRipleysOutputProbe(FILE* a_prb);

};

#endif