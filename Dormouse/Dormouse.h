/*
*******************************************************************************************************
Copyright (c) 2013, Lars Dalby & Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Dormouse.h 
\brief <B>The header file for all dormouse lifestage classes</B>
*/
/**  \file Dormouse.h
Version of December 2013 \n
By Lars Dalby & Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef DormouseH
#define DormouseH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Dormouse_Base;
class Dormouse_Population_Manager;

//------------------------------------------------------------------------------

/**
Used for the population manager's list of Dormouse
*/
/**@todo LADA: This one has been commented out for a while - is it needed? */
//typedef vector<Dormouse*> TListOfDormouse;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/** Dormouse behavioural states. */
typedef enum
{
  // Base
    toDormouses_InitialState=0,
    toDormouses_Develop,
    toDormouses_Move,
	toDormouses_GetFat,
	toDormouses_Hibernate,
    toDormouses_Die,
  // Juvenile males:
	toDormouses_BecomeAdult,
  // Females:
	toDormouses_GiveBirth,
	toDormouses_Gestation,
	toDormouses_Mating,
	toDormouses_Lactating,
	toDormouses_FExplore,
	toDormouses_ReproBehaviour,
	// Males
	toDormouses_HangOut,
	toDormouses_Explore,
	toDormouses_Check_n_Explore,
} TTypeOfDormouseState;
//------------------------------------------------------------------------------

/** \brief Used for creation of a new Dormouse object */
struct struct_Dormouse
{
public:
	/** \brief x-coord */
	int x;
	/** \brief y-coord */
	int y;
	/** \brief species ID */
	int species;
	/** Age in days */
	int age;
	/** \brief Does it have a territory */
	bool territory;
	/** \brief Number of days in unsuitable habitat. */
	unsigned int DaysInUnsuitable;
	/** \brief Does it have a territory */
	bool hibernating;
	/** \brief Landscape pointer */
	Landscape* L;
	/** \brief Dormouse_Population_Manager pointer */
	Dormouse_Population_Manager* DPM;
};
//------------------------------------------------------------------------------

/** \brief The base class for all dormice */
class Dormouse_Base : public TAnimal
{
   /**
   Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
   m_OurLandscape is a pointer, the other two are int.
   NB All areas are squares of size length X length  // What is this one doing?
   */

protected:
   /** \brief Variable to record current behavioural state */
   TTypeOfDormouseState CurrentDorState;
   /** \brief A unique ID number for this species */
   unsigned m_SpeciesID;
   /** \brief A typical member variable - this one determines the max dispersal distance */
   unsigned m_DispersalMax;
// Attributes
   /** \brief Local storage for whether it is breeding season */
   static bool m_BreedingSeason;
   /** \brief Flag to indicate if the individual is hibernating */
   bool m_Hibernating;
   /** \brief Their age in days */
   int m_Age;
   /** \brief Flag to indicate if they have reached maturation age. */
   bool m_Mature;
   /** \brief Their sex Male==true Female==false */
   bool m_Sex;
   /** \brief The height of simulation landscape */
   int SimH;
   /** \brief The width of simulation landscape */
   int SimW;
   /** \brief The current dispersal direction */
   int m_DispVector;
   /** \brief Do they have a terriory? */
   bool m_Have_Territory;
   /** \brief Maximum territory size male*/
   static unsigned int m_MaxMaleTerritorySize;
   /** \brief Maximum territory size female*/
   static unsigned int m_MaxFemaleTerritorySize;
   /** \brief Minimum territory size male*/
   static unsigned int m_MinMaleTerritorySize;
   /** \brief Minimum territory size female*/
   static unsigned int m_MinFemaleTerritorySize;
   /** \brief The size of their territory (radius of a square) */
   int m_TerrRange;
   /** \brief The minimum territory range*/
   unsigned int m_MinTerrRange;
   /** \brief Minimum acceptable habitat quality - assumes that minimum hab qual for survival is 2 * min female terr size */
   static double m_MinFDorHabQual;
   /** \brief Minimum acceptable habitat quality - assumes that minimum hab qual for survival is 2 * min male terr size */
   static double m_MinMDorHabQual;
   /** \brief Duration (in days) of fattening time before hibernation */
   int m_FatCount;
   /** \brief Number of days in unsuitable habitat */
   unsigned m_DaysInUnsuitable;
   /** \brief Maximum number of days a dormouse can endure in unsuitable habitat */
   unsigned m_MaxDaysInUnsuitable;
   /** \brief Calulate habitat quality */
   double CalculateQuality(int x, int y, int a_ddep);
   /** \brief The core movement behaviour */
   void MoveTo(int p_Vector, int p_Distance, int iterations);
   /** \brief The actual stepping without danger of stepping off the map */
   void DoWalking(int p_Distance, int &p_Vector, int &vx, int &vy);
   /** \brief The actual stepping where there is danger of stepping off the map */
   void DoWalkingCorrect(int p_Distance, int &p_Vector, int &vx, int &vy);
   /** \brief Scoring of cost of moving through different landscape elements */
   int MoveQuality(int p_x, int p_y);
   

public:
   /** \brief Dormouse_Base constructor */
   Dormouse_Base(struct_Dormouse* a_SD);
   /** \brief Dormouse_Base destructor */
   ~Dormouse_Base();
   /** \brief Behavioural state hibernation */
   virtual TTypeOfDormouseState st_Hibernate() { return toDormouses_InitialState; };
   /** \brief Behavioural state fattening up for hibernation */
   TTypeOfDormouseState st_GetFat();
   /** \brief Behavioural state dying */
   void st_Dying();
   /** \brief Function for doing a mortality test */
   virtual bool MortalityTest();
   /** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void BeginStep(); /** @todo LADA: Remove this later: NB this is not used in the Dormouse code */
   /** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
   virtual void Step() {};  // LADA: added {} for testing and removed void between ()
   /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void EndStep();  // LADA: deleted void between ()
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   unsigned GetSpeciesID() { return m_SpeciesID; }
   /** \brief A typical interface function - this one returns the SpeciesID number as an unsigned integer */
   void SetSpeciesID(unsigned a_spID) { m_SpeciesID = a_spID; };
   /** \brief Set Breeding Season flag */
   void SetBreedingSeason(bool a_flag) { m_BreedingSeason = a_flag; };
   /** \brief Set Hibernation flag */
   void SetHibernation(bool a_flag) { m_Hibernating = a_flag; };
   /** \brief Set Territory flag */
   void SetTerritory(bool a_flag) { m_Have_Territory = a_flag; };
   /** \brief Set the age */
   void Set_Age(int Age) { m_Age = Age; };
   /** \brief Set mature */
   void Set_Mature(bool a_flag ) { m_Mature = a_flag; };
   /** \brief Get our current dormouse state */
   TTypeOfDormouseState WhatDorState() { return CurrentDorState; };
   /** \brief This is a time saving pointer to the correct population manager object */
   Dormouse_Population_Manager* m_OurPopulationManager;
   /** \brief Set the location of a dormouse on the location map */
   virtual void SetLocation();
   /** \brief Frees the location of a dormouse on the location map */
   virtual void FreeLocation();
   virtual bool GetLocation(int px, int py);
   /** \brief Tell our sex */
   bool SupplySex() { return m_Sex; };
   /** \brief Tell if the individual is hibernating */
   bool SupplyHibernation() { return m_Hibernating; };
   /** \brief Tell our age */
   unsigned SupplyAge() { return m_Age; };
   /** \brief Tell whether we have a territory */
   bool SupplyTerritorial() { return m_Have_Territory; };
   /** \brief Tell if mature */
   bool SupplyMature() { return m_Mature; };
   /** Tell our x coordinate */
   unsigned SupplyX() { return m_Location_x; };
   /** Tell our y coordinate */
   unsigned SupplyY() { return m_Location_y; };
};
//------------------------------------------------------------------------------

/** \brief The class for juvenile male dormice */
class Dormouse_JuvenileMale : public Dormouse_Base
{
protected:
	void st_BecomeAdult();
public:
	Dormouse_JuvenileMale(struct_Dormouse * a_DormouseStruct);
	virtual ~Dormouse_JuvenileMale();
	virtual void Step();
	// Methods	
	TTypeOfDormouseState st_Initial_Explore();
	TTypeOfDormouseState st_JMDevelop();
	//TTypeOfDormouseState st_BecomeAdult();
	TTypeOfDormouseState st_Dispersal();
};
//------------------------------------------------------------------------------

/** \brief The class for juvenile female dormice */
class Dormouse_JuvenileFemale : public Dormouse_Base
{
public:
	Dormouse_JuvenileFemale(struct_Dormouse * a_DormouseStruct);
	virtual ~Dormouse_JuvenileFemale();
	virtual void Step();	
	
protected:
	void st_BecomeAdult();
	// Methods	
	TTypeOfDormouseState st_Initial_Explore();
	TTypeOfDormouseState st_JFDevelop();
	TTypeOfDormouseState st_Dispersal();
	//TTypeOfDormouseState st_BecomeAdult();	
};
//------------------------------------------------------------------------------

/** \brief The class for adult male dormice */
class Dormouse_Male : public Dormouse_JuvenileMale
{
public:
	Dormouse_Male(struct_Dormouse * a_DormouseStruct);
	virtual ~Dormouse_Male();
	virtual void Step();
	virtual	TTypeOfDormouseState st_Hibernate();
protected:
// Methods:
	TTypeOfDormouseState st_Dispersal();
	TTypeOfDormouseState st_HangOut();
	TTypeOfDormouseState st_Check_n_Explore();
	TTypeOfDormouseState st_Explore();
	/** \brief Main mating behaviour for males. */
	TTypeOfDormouseState st_Mating();
};
//------------------------------------------------------------------------------

/** \brief The class for adult female dormice */
class Dormouse_Female : public Dormouse_JuvenileFemale
{
public:
	Dormouse_Female(struct_Dormouse * a_DormouseStruct);
	virtual ~Dormouse_Female();
	virtual void Step();
	virtual	TTypeOfDormouseState st_Hibernate();
	/** \brief Tell whether we are pregnant */
	bool SupplyPregnant() { return m_Pregnant; };
	/** \brief Tell whether we have a mate */
	bool SupplyMate() { return m_Mate; };
	/** \brief Set the flag to indicate that the individual has been mated. */
	void SetMated(bool a_flag) { m_Mated = a_flag; };
	/** \brief Tell whether we have been mated */
	bool SupplyMated() { return m_Mated; };
protected:
// Attributes:
	/** \brief A flag indicating whether pregnant or not. */
	bool m_Pregnant;
	/** \brief A flag indicating whether the individual has a mate or not. */
	bool m_Mate;
	/** \brief A flag indicating whether the individual has been mated or not. */
	bool m_Mated;
	/** \brief A counter counting down gestation days. */
	int m_DaysUntilBirth;
	/** \brief The number of young in the current litter (if one). */
	int m_NoOfYoung;
	/** \brief The age of current litter in days. */
	unsigned m_YoungAge;
	/** \brief The total number of young produced. */
	int m_NoOfYoungTotal;
// Methods:
	TTypeOfDormouseState st_GiveBirth();
	TTypeOfDormouseState st_UpdateGestation();
	TTypeOfDormouseState st_Mating();
	TTypeOfDormouseState st_Lactating();
	TTypeOfDormouseState st_ReproBehaviour();
	TTypeOfDormouseState st_Dispersal();
	TTypeOfDormouseState st_Explore();
	
	
};
//------------------------------------------------------------------------------

#endif