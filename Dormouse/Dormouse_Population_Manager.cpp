/*
*******************************************************************************************************
Copyright (c) 2013, Lars Dalby & Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Dormouse_Population_Manager.cpp
\brief <B>The main source code for all dormouse population manager classes</B>
*/
/**  \file Dormouse_Population_Manager.cpp
Version of December 2013 \n
By Lars Dalby & Chris J. Topping \n \n
*/

//-------------------------------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../Dormouse/Dormouse.h"
#include "../Dormouse/Dormouse_Population_Manager.h"

//-------------------------------------------------------------------------------------------------

// List of config variables:

//-------------------------------------------------------------------------------------------------
static CfgInt cfg_DormouseStartNos("DOR_STARTNOS", CFG_CUSTOM, 400);  // 2500 =  1 ind/ha ref: Juskaitis & B�chner 2013 pages 111-112
static CfgInt cfg_BreedingSeasonStart("DOR_BREEDINGSEASONSTART", CFG_CUSTOM, 120);
static CfgInt cfg_BreedingSeasonEnd("DOR_BREEDINGSEASONEND", CFG_CUSTOM, 243);  /**@todo End of breeding season. LADA: (currently September 1st - which needs to be checked!) */
static CfgInt cfg_HibernationStart("DOR_HIBERNATIONSTART", CFG_CUSTOM, 300);
static CfgInt cfg_HibernationEndMale("DOR_HIBERNATIONENDMALE", CFG_CUSTOM, 100);  //  Early April
static CfgInt cfg_HibernationEndFemale("DOR_HIBERNATIONENDFEMALE", CFG_CUSTOM, 114); // females emerge two weeks later (refs in Juskaitis 2008, p. 43)
/** The probability of moving to less favourable habitats when walking */
static CfgFloat cfg_MoveToLessFavourable("DOR_MOVETOLESSFAVOURALBLE", CFG_CUSTOM, 0.005);  /**@todo LADA: Copied from the vole code - needs to be considered if the value is appropriate*/
double g_MoveToLessFavourable = cfg_MoveToLessFavourable.value(); 
static CfgFloat cfg_DailyMortalityRisk("DOR_DAILYMORTALITYRISK", CFG_CUSTOM, 0.003);  /**@todo LADA: Copied from the vole code - needs to be considered if the value is appropriate*/
double g_DailyMortRisk = cfg_DailyMortalityRisk.value();
static CfgInt cfg_SignificantAgeDifference("DOR_SIGNIFICANTAGEDIFF", CFG_CUSTOM, 24); // Age difference that triggers dominance.
unsigned g_SignificantAgeDiff = cfg_SignificantAgeDifference.value();
static CfgBool cfg_DormouseStartinOptimalOnly("DOR_OPTIMALSTARTINGLOCATION", CFG_CUSTOM, true);
// 中中中中 Probes 中中中中中� //
static CfgBool cfg_DormouseTerritoryProbe("DOR_TERRITORYPROBE", CFG_CUSTOM, true);  // Records number of individuals of each sex holding a territory each day.
static CfgBool cfg_DormousePregnancyProbe("DOR_PREGNANCYPROBE", CFG_CUSTOM, true);  // Records number of individuals of females pregnant.
static CfgBool cfg_DormouseMateProbe("DOR_MATEPROBE", CFG_CUSTOM, true);  // Records number of individuals of females with a mate.
static CfgBool cfg_DormouseJuvenileProbe("DOR_JUVENILEPROBE", CFG_CUSTOM, true);  // Records the number of individuals born each day.
static CfgBool cfg_DormouseAgeProbe("DOR_AGEPROBE", CFG_CUSTOM, true);  // Records the age every individual each day.
static CfgBool cfg_DormouseHibernationProbe("DOR_HIBERNATIONPROBE", CFG_CUSTOM, true);  // Records the hibernation state of every individual on the 1st of each month.
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;


//-------------------------------------------------------------------------------------------------

Dormouse_Population_Manager::~Dormouse_Population_Manager (void)
{
	if (cfg_DormouseTerritoryProbe.value())
	{
		(*m_DormouseTerritoryFile).close();
	}
	if (cfg_DormousePregnancyProbe.value())
	{
		(*m_DormousePregnancyFile).close();
	}
	if (cfg_DormouseMateProbe.value())
	{
		(*m_DormouseMateFile).close();
	}
}
//-------------------------------------------------------------------------------------------------

Dormouse_Population_Manager::Dormouse_Population_Manager(Landscape* L) : Population_Manager(L)
{
    // Load List of Animal Classes
	m_ListNames[0] = "Juvenile male";
	m_ListNames[1] = "Juvenile female";
	m_ListNames[2] = "Male";
	m_ListNames[3] = "Female";
	m_ListNameLength = 4;
	// We need one vector for each life stage
	// This loop the removes the 6 extra vectors
	for (unsigned i = 0; i < (10 - m_ListNameLength); i++)
	{
		TheArray.pop_back();
	}
    strcpy( m_SimulationName, "Dormouse Simulation" );
	// Create some animals
	struct_Dormouse* sp;
	sp = new struct_Dormouse;
	sp->DPM = this;
	sp->L = m_TheLandscape;
	DormouseMap = new IDMap<TAnimal*>(L);
	int Nos = cfg_DormouseStartNos.value()/2;  //  Split the starting number into equal halfes of male and female
	for (int j = dob_Male; j < dob_foobar; j++)
		for (int i = 0; i < Nos; i++)
		{
			bool found = false;
			while (!found)
			{
				sp->x = random(SimW);
				sp->y = random(SimH);
				found = SuitableStartingLocation(sp->x, sp->y);
			}
			InitialCreateObjects((DormouseObject) j, NULL, sp, 1);
		}
	
	unsigned int p = m_TheLandscape->SupplyNumberOfPolygons();
	DormouseHabitatBaseQualities.resize(p);
	YoungProducedToday = 0; 
	Init();
}
//-------------------------------------------------------------------------------------------------

void Dormouse_Population_Manager::Init(){
	if (cfg_DormouseTerritoryProbe.value())
	{
		m_DormouseTerritoryFile = new ofstream("DormouseTerritory.txt", ios::out);
	}
	if (cfg_DormousePregnancyProbe.value())
	{
		m_DormousePregnancyFile = new ofstream("DormousePregnancy.txt", ios::out);
	}
	if (cfg_DormouseMateProbe.value())
	{
		m_DormouseMateFile = new ofstream("DormouseMate.txt", ios::out);
	}
	if (cfg_DormouseJuvenileProbe.value())
	{
		m_DormouseJuvenileFile = new ofstream("DormouseJuvenile.txt", ios::out);
	}
	if (cfg_DormouseAgeProbe.value())
	{
		m_DormouseAgeFile = new ofstream("DormouseAge.txt", ios::out);
	}
	if (cfg_DormouseHibernationProbe.value())
	{
		m_DormouseHibernationFile = new ofstream("DormouseHibernation.txt", ios::out);
	}
	if (cfg_RipleysOutput_used.value()) {
		OpenTheRipleysOutputProbe("");
	}
}

void Dormouse_Population_Manager::CreateObjects(DormouseObject a_ob_type, TAnimal *, struct_Dormouse* a_data, int a_number)
{
   
   for (int i=0; i<a_number; i++)
   {
	   if (a_ob_type == dob_Male)
	   {
		   Dormouse_Male* new_Male;
		   new_Male = new Dormouse_Male(a_data);
		   TheArray[a_ob_type].push_back(new_Male);
		   DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_Male);
		   new_Male->Set_Age(a_data->age);
		   new_Male->SetHibernation(a_data->hibernating);
		   new_Male->SetTerritory(a_data->territory);
	   }
	   if (a_ob_type == dob_Female)
	   {
		   Dormouse_Female* new_Female;
		   new_Female = new Dormouse_Female(a_data);
		   TheArray[a_ob_type].push_back(new_Female);
		   DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_Female);
		   new_Female->Set_Age(a_data->age);
		   new_Female->SetHibernation(a_data->hibernating);
		   new_Female->SetTerritory(a_data->territory);
		}
	   if (a_ob_type == dob_JuvenileFemale)
	   {
		   Dormouse_JuvenileFemale* new_JuvenileFemale;
		   new_JuvenileFemale = new Dormouse_JuvenileFemale(a_data);
		   TheArray[a_ob_type].push_back(new_JuvenileFemale);
		   DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_JuvenileFemale);
		   new_JuvenileFemale->Set_Age(a_data->age);
		   new_JuvenileFemale->SetHibernation(a_data->hibernating);
	   }
	   if (a_ob_type == dob_JuvenileMale)
	   {
		   Dormouse_JuvenileMale* new_JuvenileMale;
		   new_JuvenileMale = new Dormouse_JuvenileMale(a_data);
		   TheArray[a_ob_type].push_back(new_JuvenileMale);
		   DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_JuvenileMale);
		   new_JuvenileMale->Set_Age(a_data->age);
		   new_JuvenileMale->SetHibernation(a_data->hibernating);
	   }
   }
}
//-------------------------------------------------------------------------------------------------

void Dormouse_Population_Manager::InitialCreateObjects(DormouseObject a_ob_type, TAnimal *, struct_Dormouse* a_data, int a_number)
{

	for (int i = 0; i<a_number; i++)
	{
		if (a_ob_type == dob_Male)
		{
			Dormouse_Male* new_Male;
			new_Male = new Dormouse_Male(a_data);
			TheArray[a_ob_type].push_back(new_Male);
			DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_Male);
			new_Male->Set_Age(random(100) + 1);  // +1 to avoid creating objects with age = 0
		}
		if (a_ob_type == dob_Female)
		{
			Dormouse_Female* new_Female;
			new_Female = new Dormouse_Female(a_data);
			TheArray[a_ob_type].push_back(new_Female);
			DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_Female);
			new_Female->Set_Age(random(100) + 1);  // +1 to avoid creating objects with age = 0
		}
		if (a_ob_type == dob_JuvenileFemale)
		{
			Dormouse_JuvenileFemale* new_JuvenileFemale;
			new_JuvenileFemale = new Dormouse_JuvenileFemale(a_data);
			TheArray[a_ob_type].push_back(new_JuvenileFemale);
			DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_JuvenileFemale);
			new_JuvenileFemale->Set_Age(1);
			new_JuvenileFemale->SetHibernation(a_data->hibernating);
		}
		if (a_ob_type == dob_JuvenileMale)
		{
			Dormouse_JuvenileMale* new_JuvenileMale;
			new_JuvenileMale = new Dormouse_JuvenileMale(a_data);
			TheArray[a_ob_type].push_back(new_JuvenileMale);
			DormouseMap->SetMapValue(a_data->x, a_data->y, (TAnimal*)new_JuvenileMale);
			new_JuvenileMale->Set_Age(1);
			new_JuvenileMale->SetHibernation(a_data->hibernating);
		}
	}
}
//-------------------------------------------------------------------------------------------------

/** \brief Checks if a location is suitable before warping in new dormice to start the simulation*/
bool Dormouse_Population_Manager::SuitableStartingLocation(int x, int y)
{
	TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);
	if (cfg_DormouseStartinOptimalOnly.value())
	{
		switch (tole)
		{
		case tole_YoungForest: // 60
		case tole_Scrub: // 70
		case tole_Hedges: // 130 (internal ALMaSS representation for Hedges)
		case tole_RiversidePlants: // 98
		case tole_RiversideTrees: // 97
		case tole_MixedForest: // 60
		case tole_DeciduousForest: // 40
		case tole_ConiferousForest: // 50
		case tole_Copse:
			return true;
		default:
			return false;
		}
	}
	else
	{
		switch (tole)
		{
		case tole_Orchard:
		case tole_OrchardBand:
		case tole_MownGrass:
		case tole_RoadsideVerge:
		case tole_RoadsideSlope:
		case tole_Railway:
		case tole_FieldBoundary:
		case tole_Field:
		case tole_PermPastureLowYield:
		case tole_PermPastureTussocky:
		case tole_PermanentSetaside:
		case tole_PermPasture:
		case tole_NaturalGrassDry:
		case tole_NaturalGrassWet:
		case tole_PitDisused:
		case tole_Garden:
		case tole_HedgeBank:
		case tole_BeetleBank:
			return true;
			/** @todo Decide where to classify new LE types for dormouse in dormouse population manager */
		case tole_Wasteland:
		case tole_IndividualTree:
		case tole_WoodyEnergyCrop:
		case tole_PlantNursery:
		case tole_Pylon:
		case tole_WindTurbine:
		case tole_WoodlandMargin:
		case tole_Vildtager:
		default:
			return false;
		}
	}
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void Dormouse_Population_Manager::DoFirst() {
	unsigned sz = m_TheLandscape->SupplyNumberOfPolygons();
	for (unsigned p = 0; p < sz; p++)
	{
		double qual = AssessHabitat(p);
		DormouseHabitatBaseQualities[p] = qual;	
	}
	int today = m_TheLandscape->SupplyDayInYear();
	if (TheArray[dob_Female].size() > 0)
	{
		if ((today >= cfg_BreedingSeasonStart.value()) && (today <= cfg_BreedingSeasonEnd.value()))
		{
			dynamic_cast<Dormouse_Female*>(TheArray[dob_Female][0])->SetBreedingSeason(true);
		}
		else
		{
			dynamic_cast<Dormouse_Female*>(TheArray[dob_Female][0])->SetBreedingSeason(false);

		}
	}
	/**@todo LADA: Commented out, apparently makes no difference, but it should.../
	/*if (TheArray[dob_Male].size() > 0)
	{
		if ((today >= cfg_BreedingSeasonStart.value()) && (today <= cfg_BreedingSeasonEnd.value()))
		{
			dynamic_cast<Dormouse_Male*>(TheArray[dob_Male][0])->SetBreedingSeason(true);
		}
		else
		{
			dynamic_cast<Dormouse_Male*>(TheArray[dob_Male][0])->SetBreedingSeason(false);

		}
	}*/



// ---- Output below here ---- //

	int DayInMonth = g_date->GetDayInMonth();
	int month = g_date->GetMonth();
	if (cfg_DormouseTerritoryProbe.value())
	{
		if (DayInMonth == 1 || DayInMonth == 14) TheTerritoryProbe();
	}
	if (cfg_DormousePregnancyProbe.value())
	{
		if (DayInMonth == 1 || DayInMonth == 14) ThePregnancyProbe();
	}
	if (cfg_DormouseMateProbe.value())
	{
		if (DayInMonth == 1 || DayInMonth == 14) TheMateProbe();
	}
	if (cfg_DormouseJuvenileProbe.value())
	{
		TheJuvenileProbe();
		YoungProducedToday = 0;  // Needs to be reset every day.
	}
	if (cfg_DormouseAgeProbe.value())
	{
		if (month == 11 && DayInMonth == 30)
		{
			TheAgeProbe();
		}
		
	}
	if (cfg_DormouseHibernationProbe.value())
	{
		if (DayInMonth == 1)
		{
			TheHibernationProbe();
		}

	}
}
//-------------------------------------------------------------------------------------------------

/** \brief Assess the quality of habitat at p_Polyref. */
double Dormouse_Population_Manager::AssessHabitat(int p_Polyref)
{
	/**
	* Each unit of area (1m2) is assigned a score based on the polygon type. These scores fall currently into
	* a limited number of catagories, with 3, 2, 1, & 0 being the only scores currently used. 3 denotes the best possible conditions.
	*/
	TTypesOfLandscapeElement ElementType;
	ElementType = m_TheLandscape->SupplyElementTypeFromVector(p_Polyref);
	double score = -9999;
	switch (ElementType)
	{
	case tole_Hedges: // 130 (internal ALMaSS representation for Hedges)
	case tole_Scrub: // 70
	case tole_MixedForest: // 60
	case tole_YoungForest: // 60
	case tole_Copse:
		score = 3;
		break;
	case tole_DeciduousForest: // 40
	case tole_ConiferousForest: // 50
		score = 2;
		break;
	case tole_RiversideTrees: // 97
	case tole_FieldBoundary: // 160
	case tole_RoadsideVerge: // 13
	case tole_RoadsideSlope:
	case tole_HedgeBank:
	case tole_Orchard: // 56
	case tole_OrchardBand: // 57
		score = 1;
		break;
	case tole_PermanentSetaside:
	case tole_Marsh: // 95
	case tole_Heath:
	case tole_RiversidePlants: // 98
	case tole_PitDisused: // 75
	case tole_BuiltUpWithParkland:
	case tole_Parkland:
	case tole_AmenityGrass:
	case tole_MownGrass: // 58
	case tole_BeetleBank:
	case tole_NaturalGrassWet:
	case tole_NaturalGrassDry: // 110
	case tole_PermPasture: // 35
	case tole_PermPastureLowYield:
	case tole_PermPastureTussocky:
	case tole_Garden: //11
	case tole_Track: // 123
	case tole_SmallRoad: // 122
	case tole_LargeRoad: // 121
	case tole_BareRock:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_SandDune:
	case tole_Churchyard:
	case tole_HeritageSite:
	case tole_Saltmarsh:
	case tole_Field: // 20 & 30
	case tole_UnsprayedFieldMargin:
		score = 0;
		break;
	case tole_Railway: // 118
	case tole_Building: // 5
	case tole_Carpark:
	case tole_ActivePit: // 115
	case tole_Freshwater: // 90
	case tole_FishFarm:
	case tole_Pond: // 219
	case tole_River: // 96
	case tole_Stream:
	case tole_Saltwater: // 80
	case tole_Coast: // 100
	case tole_StoneWall: // 15
	case tole_Fence:
		score = -1;
		break;
		/** @todo Decide where to classify new LE types for dormouse in dormouse population manager 2 */
	case tole_Wasteland:
	case tole_IndividualTree:
	case tole_WoodyEnergyCrop:
	case tole_PlantNursery:
	case tole_Pylon:
	case tole_WindTurbine:
	case tole_WoodlandMargin:
	case tole_Vildtager:	
	case tole_Foobar: // 999 !! type unknown - should not happen
	default:
		static char errornum[20];
		sprintf(errornum, "%d", ElementType);
		m_TheLandscape->Warn("Dormouse_PopulationManager:AssessHabitat: Unknown tole_type", errornum);
		exit(1);
	}
	return (score);
}
//---------------------------------------------------------------------------

/** \brief Prevents dormice from mating with an individual on the other side of af barrier. */
/** \return true if mating is possible, false if not. */
bool Dormouse_Population_Manager::BarrierSearch(int F_x, int F_y, int M_x, int M_y)
{
	int poly2 = 200;  /**@todo LADA: What is this value doing? */

	if (F_x == M_x){
		if (F_y == M_y) return true; // if they are on the same spot
		if (F_y < M_y){ // if they have the same x coordinate and different y coordinates and the female has the lowest y
			for (int j = F_y; j < M_y; j++){
				int poly1 = m_TheLandscape->SupplyPolyRef(F_x, j);
				if (poly2 != poly1){
					poly2 = poly1;
					int Elem = m_TheLandscape->SupplyElementType(poly1);

					switch (Elem){
					case tole_MetalledPath:
					case tole_Carpark:
					case tole_Churchyard:
					case tole_Saltmarsh:
					case tole_Stream:
					case tole_HeritageSite:
					case tole_Pond: // 219
					case tole_Freshwater:
					case tole_FishFarm:
					case tole_River:
					case tole_Saltwater:
					case tole_LargeRoad:
					case tole_Building:
					case tole_Coast:
					case tole_DeciduousForest:
					case tole_ConiferousForest:
						return false;
					default:
						break;
					}
				}
			}
			return true;
		}
		else { // if (F_y > M_y) if they have the same x coordinate and different y coordinates and the male has the lowest y
			for (int j = M_y; j < F_y; j++){
				int poly1 = m_TheLandscape->SupplyPolyRef(M_x, j);
				if (poly1 != poly2){
					poly2 = poly1;
					int Elem = m_TheLandscape->SupplyElementType(poly1);

					switch (Elem){
					case tole_MetalledPath:
					case tole_Carpark:
					case tole_Churchyard:
					case tole_Saltmarsh:
					case tole_Stream:
					case tole_HeritageSite:
					case tole_Pond: // 219
					case tole_Freshwater:
					case tole_FishFarm:
					case tole_River:
					case tole_Saltwater:
					case tole_LargeRoad:
					case tole_Building:
					case tole_Coast:
					case tole_DeciduousForest:
					case tole_ConiferousForest:
						return false;
					default:
						break;
					}
				}
			}
			return true;
		}
	}
	if (F_y == M_y){
		if (F_x < M_x) { // if they have the same y coordinate and different x coordinates and the female's x coordinate is the lowest
			for (int i = F_x; i < M_x; i++){
				int poly1 = m_TheLandscape->SupplyPolyRef(i, F_y);
				if (poly1 != poly2){
					poly2 = poly1;
					int Elem = m_TheLandscape->SupplyElementType(poly1);

					switch (Elem){
					case tole_MetalledPath:
					case tole_Carpark:
					case tole_Churchyard:
					case tole_Saltmarsh:
					case tole_Stream:
					case tole_HeritageSite:
					case tole_Pond: // 219
					case tole_Freshwater:
					case tole_FishFarm:
					case tole_River:
					case tole_Saltwater:
					case tole_LargeRoad:
					case tole_Building:
					case tole_Coast:
					case tole_DeciduousForest:
					case tole_ConiferousForest:
						return false;
					default:
						break;
					}
				}
			}
			return true;
		}
		else { // (F_x > M_x) if they have the same y coordinate and different x coordinates and the male's x coordinate is the lowest
			for (int i = M_x; i < F_x; i++){
				int poly1 = m_TheLandscape->SupplyPolyRef(i, M_y);
				if (poly1 != poly2){
					poly2 = poly1;
					int Elem = m_TheLandscape->SupplyElementType(poly1);

					switch (Elem){
					case tole_MetalledPath:
					case tole_Carpark:
					case tole_Churchyard:
					case tole_Saltmarsh:
					case tole_Stream:
					case tole_HeritageSite:
					case tole_Pond: // 219
					case tole_Freshwater:
					case tole_FishFarm:
					case tole_River:
					case tole_Saltwater:
					case tole_LargeRoad:
					case tole_Building:
					case tole_Coast:
					case tole_DeciduousForest:
					case tole_ConiferousForest:
						return false;
					default:
						break;
					}
				}
			}
			return true;
		}
	}
	if (F_x < M_x){ // if the x coordinates differ and the females is the lowest
		if (F_y < M_y){ // if the y coordinates differ and the female has the lowest y coordinate
			unsigned diff_x = M_x - F_x;
			unsigned diff_y = M_y - F_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = F_y;

				for (int i = F_x; i < M_x; i++){	// the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					j++;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest values
					int j_extent = F_y + diff_x;
					for (int j_y = j_extent; j_y < M_y; j_y++){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(M_x, j_y);
						if (poly1 != poly2){
							poly2 = poly1;
							int Elem = m_TheLandscape->SupplyElementType(poly1);

							switch (Elem){
							case tole_MetalledPath:
							case tole_Carpark:
							case tole_Churchyard:
							case tole_Saltmarsh:
							case tole_Stream:
							case tole_HeritageSite:
							case tole_Pond: // 219
							case tole_Freshwater:
							case tole_FishFarm:
							case tole_River:
							case tole_Saltwater:
							case tole_LargeRoad:
							case tole_Building:
							case tole_Coast:
							case tole_DeciduousForest:
							case tole_ConiferousForest:
								return false;
							default:
								break;
							}
						}
					}
				}
				return true;
			}
			if (diff_x > diff_y) { // if not diagonal but widened and the x and y coordinates differ with the female having the lowest values
				int i = F_x;
				for (int j = F_y; j < M_y; j++){ // the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				int i_extent = diff_y + F_x;
				for (int i_x = i_extent; i_x < M_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, M_y);
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				return true;
			}
		}
		if (F_y > M_y) { // and if (F_x < M_x)
			int diff_x = M_x - F_x;
			int diff_y = F_y - M_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = F_y;
				for (int i = F_x; i < M_x; i++){	// the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					j--;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest x value and highest y
					int j_extent = F_y - diff_x;
					for (int j_y = j_extent; j_y > M_y; j_y--){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(M_x, j_y);
						if (poly1 != poly2){
							poly2 = poly1;
							int Elem = m_TheLandscape->SupplyElementType(poly1);

							switch (Elem){
							case tole_MetalledPath:
							case tole_Carpark:
							case tole_Churchyard:
							case tole_Saltmarsh:
							case tole_Stream:
							case tole_HeritageSite:
							case tole_Pond: // 219
							case tole_Freshwater:
							case tole_FishFarm:
							case tole_River:
							case tole_Saltwater:
							case tole_LargeRoad:
							case tole_Building:
							case tole_Coast:
							case tole_DeciduousForest:
							case tole_ConiferousForest:
								return false;
							default:
								break;
							}
						}
					}
				}
				return true;
			}
			else { // if (diff_x > diff_y) if not diagonal but widened and the x and y coordinates differ with the female having the lowest x and highest y
				int i = F_x;
				for (int j = F_y; j > M_y; j--){ // the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				int i_extent = diff_y + F_x;
				for (int i_x = i_extent; i_x < M_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, M_y);
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				return true;
			}
		}
	}
	if (F_x > M_x){
		if (F_y < M_y){
			unsigned diff_x = F_x - M_x;
			unsigned diff_y = M_y - F_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = M_y;
				for (int i = M_x; i < F_x; i++){	// the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					j--;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}

				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest values
					int j_extent = M_y - diff_x;
					for (int j_y = j_extent; j_y > F_y; j_y--){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(F_x, j_y);
						if (poly1 != poly2){
							poly2 = poly1;
							int Elem = m_TheLandscape->SupplyElementType(poly1);

							switch (Elem){
							case tole_MetalledPath:
							case tole_Carpark:
							case tole_Churchyard:
							case tole_Saltmarsh:
							case tole_Stream:
							case tole_HeritageSite:
							case tole_Pond: // 219
							case tole_Freshwater:
							case tole_FishFarm:
							case tole_River:
							case tole_Saltwater:
							case tole_LargeRoad:
							case tole_Building:
							case tole_Coast:
							case tole_DeciduousForest:
							case tole_ConiferousForest:
								return false;
							default:
								break;
							}
						}
					}
				}
				return true;
			}
			else { // if (diff_x > diff_y) if not diagonal but widened and the x and y coordinates differ with the female having the lowest values
				int i = M_x;
				for (int j = M_y; j > F_y; j--){
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				int i_extent = diff_y + M_x;
				for (int i_x = i_extent; i_x < F_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, F_y);
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				return true;
			}
		}
		if (F_y > M_y) {// if (F_x > M_x)
			int diff_x = F_x - M_x;
			int diff_y = F_y - M_y;
			if (diff_x <= diff_y){ // if the area between them is enlongated (y) or diagonal
				int j = M_y;
				for (int i = M_x; i < F_x; i++){	// the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					j++;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				if (diff_x != diff_y){ // if not diagonal but enlongated and the x and y coordinates differ with the female having the lowest x value and highest y
					int j_extent = M_y + diff_x;
					for (int j_y = j_extent; j_y < F_y; j_y++){ // the enlongated part
						int poly1 = m_TheLandscape->SupplyPolyRef(F_x, j_y);
						if (poly1 != poly2){
							poly2 = poly1;
							int Elem = m_TheLandscape->SupplyElementType(poly1);

							switch (Elem){
							case tole_MetalledPath:
							case tole_Carpark:
							case tole_Churchyard:
							case tole_Saltmarsh:
							case tole_Stream:
							case tole_HeritageSite:
							case tole_Pond: // 219
							case tole_Freshwater:
							case tole_FishFarm:
							case tole_River:
							case tole_Saltwater:
							case tole_LargeRoad:
							case tole_Building:
							case tole_Coast:
							case tole_DeciduousForest:
							case tole_ConiferousForest:
								return false;
							default:
								break;
							}
						}
					}
				}
				return true;
			}
			else { // if (diff_x > diff_y) if not diagonal but widened and the x and y coordinates differ with the female having the lowest x and highest y
				int i = M_x;
				for (int j = M_y; j < F_y; j++){ // the diagonal part
					int poly1 = m_TheLandscape->SupplyPolyRef(i, j);
					i++;
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				int i_extent = diff_y + M_x;
				for (int i_x = i_extent; i_x < F_x; i_x++){ // the widened part
					int poly1 = m_TheLandscape->SupplyPolyRef(i_x, F_y);
					if (poly1 != poly2){
						poly2 = poly1;
						int Elem = m_TheLandscape->SupplyElementType(poly1);

						switch (Elem){
						case tole_MetalledPath:
						case tole_Carpark:
						case tole_Churchyard:
						case tole_Saltmarsh:
						case tole_Stream:
						case tole_HeritageSite:
						case tole_Pond: // 219
						case tole_Freshwater:
						case tole_FishFarm:
						case tole_River:
						case tole_Saltwater:
						case tole_LargeRoad:
						case tole_Building:
						case tole_Coast:
						case tole_DeciduousForest:
						case tole_ConiferousForest:
							return false;
						default:
							break;
						}
					}
				}
				return true; //returns true if both the diagonal and widened part dont hit return false
			}
		}
	}
	return true;
}
//---------------------------------------------------------------------------


Dormouse_Male* Dormouse_Population_Manager::FindClosestMale(int p_x, int p_y, int p_steps)
{

	/** looks for the closest male within p_Steps of p_x,p_y */
	// as usual there are 4 possibilities of overlap
	// First convert centre co-rdinates to square co-ordinates
	int x = p_x - p_steps;
	if (x < 0) x += SimW;  // ensure we start in the simulation area!
	int y = p_y - p_steps;
	if (y < 0) y += SimH;
	int range_x = p_steps * 2;
	int range_y = p_steps * 2;

	// create the extent variables
	int xextent0 = x + range_x;
	int yextent0 = y + range_y;
	int xextent1 = (x + range_x) - SimW;
	int yextent1 = (y + range_y) - SimH;
	// Create the looping variables needed
	int Dfinx;
	int Dfiny;
	int Afinx = 0;  //unless the finx values for A are changed this stop
	int Afiny = 0;  //the loop from executing
	int Asty = 0;
	// int Astx,Dstx,Dtsy are always default so variables not used from them
	// NB Astx, Asty and Dstx are always 0, 0 & x respectively
	// Dsty is always y, Afiny is always yextent1 if it is used
	// Now create the loop values;
	if (xextent0 <= SimW)   // No overlap with the eastern side
	{
		// Dstx, Dsty, Asty set by defaults
		//Astx & Afinx are not needed
		Dfinx = xextent0;
		// do we overlap the bottom?
		// Type B & D (overlap bottom, no overlap)
		if (yextent0 > SimH)
		{
			// Type B (overlap bottom only)
			Dfiny = SimH;  // stop at the end
			Afiny = yextent1; // the overlap with the top
		}
		else Dfiny = yextent0;
	}
	else
	{
		// Type A & C overlap bottom & eastern edgdes
		if (yextent0 > SimH)
		{
			// relies on the default start for Asty, Astx, Dstx, Dsty
			Afinx = xextent1;
			Afiny = yextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = SimH;
		}
		else
		{
			// Type C overlap left edge only
			// Astx & Afiny are not needed here
			//Astx, Dstx, Dsty set by default
			Afinx = xextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = yextent0;
		}
	}
	Dormouse_Male* AMale;
	Dormouse_Male* Found = NULL;
	int dist, disty;
	int FoundDist = SimW;  // too big
	Dormouse_Base* Ap;
	// A Loop
	for (int i = 0; i < Afinx; i++)
	{
		for (int j = Asty; j < Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					{
						AMale = (Dormouse_Male*)(Ap);
						if (AMale->SupplyTerritorial())
						{
							dist = abs(p_x - i); // remove the signed bit
							disty = abs(p_y - j);
							dist += disty;

							if (dist < FoundDist)
							{
								bool Barrier = BarrierSearch(p_x, p_y, i, j);
								if (Barrier == true){
									Found = AMale;
									FoundDist = dist;
								}
							}
						}
					}
				}
			}
		}
		// C Loop
		for (int j = y; j < Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					{
						AMale = (Dormouse_Male*)(Ap);
						if (AMale->SupplyTerritorial())
						{
							dist = abs(p_x - i); // remove the signed bit
							disty = abs(p_y - j);
							dist += disty;

							if (dist < FoundDist)
							{
								bool Barrier = BarrierSearch(p_x, p_y, i, j);
								if (Barrier == true){
									Found = AMale;
									FoundDist = dist;
								}
							}
						}
					}
				}
			}
		}
	}
	// D Loop
	for (int i = x; i < Dfinx; i++)
	{
		for (int j = y; j < Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					{
						AMale = (Dormouse_Male*)(Ap);
						if (AMale->SupplyTerritorial())
						{
							dist = abs(p_x - i); // remove the signed bit
							disty = abs(p_y - j);
							dist += disty;
							if (dist < FoundDist)
							{
								bool Barrier = BarrierSearch(p_x, p_y, i, j);
								if (Barrier == true){
									Found = AMale;
									FoundDist = dist;
								}
							}
						}
					}
				}
			}
		}
		// B Loop
		for (int j = 0; j < Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					AMale = (Dormouse_Male*)(Ap);
					{

						if (AMale->SupplyTerritorial())
						{
							dist = abs(p_x - i); // remove the signed bit
							disty = abs(p_y - j);
							dist += disty;
							if (dist < FoundDist)
							{
								bool Barrier = BarrierSearch(p_x, p_y, i, j);
								if (Barrier == true){
									Found = AMale;
									FoundDist = dist;
								}
							}
						}
				  }
				}
			}
		}
	}
	return Found;
}

/** \brief Counts dormouse postions on the map from p_x-p_range, p-y-p_range to p_x+p_size, p_y+p_range */
int Dormouse_Population_Manager::SupplyHowManyDormice(unsigned p_x, unsigned p_y, unsigned p_range)
{
	int x = p_x - p_range;
	if (x<0) x += SimW;  // ensure we start in the simulation area!
	int y = p_y - p_range;
	if (y<0) y += SimH;
	int range_x = p_range * 2;
	int range_y = p_range * 2;
	int Dormice = 0;
	// create the extent variables
	int xextent0 = x + range_x;
	int yextent0 = y + range_y;
	int xextent1 = (x + range_x) - SimW;
	int yextent1 = (y + range_y) - SimH;
	// Create the looping variables needed
	int Dfinx;
	int Dfiny;
	int Afinx = 0;  // unless the finx values for A are changed this stop
	int Afiny = 0;  // the loop from executing
	int Asty = 0;
	// int Astx,Dstx,Dtsy are always default so variables not used from them
	// NB Astx, Asty and Dstx are always 0, 0 & x respectively
	// Dsty is always y, Afiny is always yextent1 if it is used
	// Now create the loop values;
	if (xextent0 <= SimW)   // No overlap with the eastern side
	{
		// Dstx, Dsty, Asty set by defaults
		//Astx & Afinx are not needed
		Dfinx = xextent0;
		// do we overlap the bottom?
		// Type B & D (overlap bottom, no overlap)
		if (yextent0>SimH)
		{
			// Type B (overlap bottom only)
			Dfiny = SimH;  // stop at the end
			Afiny = yextent1; // the overlap with the top
		}
		else Dfiny = yextent0;
	}
	else
	{
		// Type A & C overlap bottom & eastern edgdes
		if (yextent0>SimH)
		{
			// relies on the default start for Asty, Astx, Dstx, Dsty
			Afinx = xextent1;
			Afiny = yextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = SimH;
		}
		else
		{
			// Type C overlap left edge only
			// Astx & Afiny are not needed here
			//Astx, Dstx, Dsty set by default
			Afinx = xextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = yextent0;
		}
	}
	// A Loop
	for (int i = 0; i<Afinx; i++)
	{
		for (int j = Asty; j<Afiny; j++)
		{
			if (DormouseMap->GetMapValue(i, j) != NULL) Dormice++;
		}
		// C Loop
		for (int j = y; j<Dfiny; j++)
		{
			if (DormouseMap->GetMapValue(i, j) != NULL) Dormice++;
		}
	}
	// D Loop
	for (int i = x; i<Dfinx; i++)
	{
		for (int j = y; j<Dfiny; j++)
		{
			if (DormouseMap->GetMapValue(i, j) != NULL) Dormice++;
		}
		// B Loop
		for (int j = 0; j<Afiny; j++)
		{
			if (DormouseMap->GetMapValue(i, j) != NULL) Dormice++;
		}
	}
	// End of search algorithm
	return Dormice;
}
//---------------------------------------------------------------------------

/** \brief Searches for older females on the map. */
/**
Search is done from p_x-p_range, p-y-p_range to p_x+p_size, p_y+p_range.
\param[in] p_x The x-coordinate of the individual at start
\param[in] p_y The y-coordinate of the individual at start
\param[in] p_Age The age of the individual
\param[in] p_range The range in which to search for older females (p_x, p_y +/- p_range)
\return true if older females were found in the area p_x, p_y +/- p_range, false otherwise.
*/
bool Dormouse_Population_Manager::SupplyOlderFemales(unsigned p_x, unsigned p_y, unsigned p_Age, unsigned p_range)
{
	// Before checking the map remove the caller individual so it doesn't count
	TAnimal* caller = (TAnimal*)DormouseMap->GetMapValue(p_x, p_y);
	DormouseMap->ClearMapValue(p_x, p_y);
	// This is reset when the result is known
	unsigned Age = p_Age + g_SignificantAgeDiff;
	int x = p_x - p_range;
	if (x<0) x += SimW;  // ensure we start in the simulation area!
	int y = p_y - p_range;
	if (y<0) y += SimH;
	int range_x = p_range * 2;
	int range_y = p_range * 2;
	// create the extent variables
	int xextent0 = x + range_x;
	int yextent0 = y + range_y;
	int xextent1 = (x + range_x) - SimW;
	int yextent1 = (y + range_y) - SimH;
	// Create the looping variables needed
	int Dfinx;
	int Dfiny;
	int Afinx = 0;  //unless the finx values for A are changed this stop
	int Afiny = 0;  //the loop from executing
	int Asty = 0;
	// int Astx,Dstx,Dtsy are always default so variables not used from them
	// NB Astx, Asty and Dstx are always 0, 0 & x respectively
	// Dsty is always y, Afiny is always yextent1 if it is used
	// Now create the loop values;
	if (xextent0 <= SimW)   // No overlap with the eastern side
	{
		// Dstx, Dsty, Asty set by defaults
		//Astx & Afinx are not needed
		Dfinx = xextent0;
		// do we overlap the bottom?
		// Type B & D (overlap bottom, no overlap)
		if (yextent0>SimH)
		{
			// Type B (overlap bottom only)
			Dfiny = SimH;  // stop at the end
			Afiny = yextent1; // the overlap with the top
		}
		else Dfiny = yextent0;
	}
	else
	{
		// Type A & C overlap bottom & eastern edgdes
		if (yextent0>SimH)
		{
			// relies on the default start for Asty, Astx, Dstx, Dsty
			Afinx = xextent1;
			Afiny = yextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = SimH;
		}
		else
		{
			// Type C overlap left edge only
			// Astx & Afiny are not needed here
			//Astx, Dstx, Dsty set by default
			Afinx = xextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = yextent0;
		}
	}
	Dormouse_Base* Ap;
	// A Loop
	for (int i = 0; i<Afinx; i++)
	{
		for (int j = Asty; j<Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{
					if (Ap->SupplyAge() >= Age) 
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
		// C Loop
		for (int j = y; j<Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{
					if (Ap->SupplyAge() >= Age)
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
	}
	// D Loop
	for (int i = x; i<Dfinx; i++)
	{
		for (int j = y; j<Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{
					if (Ap->SupplyAge() >= Age)
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
		// B Loop
		for (int j = 0; j<Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{
					if (Ap->SupplyAge() >= Age)
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
	}
	// End of search algorithm
	DormouseMap->SetMapValue(p_x, p_y, caller);
	return false;
}
//---------------------------------------------------------------------------

/** \brief Searches for older males on the map. */
/**
Search is done from p_x-p_range, p-y-p_range to p_x+p_size, p_y+p_range.
\param[in] p_x The x-coordinate of the individual at start
\param[in] p_y The y-coordinate of the individual at start
\param[in] p_Age The age of the individual
\param[in] p_range The range in which to search for older males (p_x, p_y +/- p_range)
\return true if older males were found in the area p_x, p_y +/- p_range, false otherwise.
*/
bool Dormouse_Population_Manager::SupplyOlderMales(unsigned p_x, unsigned p_y, unsigned p_Age, unsigned p_range)
{
	// Before checking the map remove the caller individual so it doesn't count
	TAnimal* caller = (TAnimal*)DormouseMap->GetMapValue(p_x, p_y);
	DormouseMap->ClearMapValue(p_x, p_y);
	// This is reset when the result is known
	unsigned Age = p_Age + g_SignificantAgeDiff;
	int x = p_x - p_range;
	if (x<0) x += SimW;  // ensure we start in the simulation area!
	int y = p_y - p_range;
	if (y<0) y += SimH;
	int range_x = p_range * 2;
	int range_y = p_range * 2;
	// create the extent variables
	int xextent0 = x + range_x;
	int yextent0 = y + range_y;
	int xextent1 = (x + range_x) - SimW;
	int yextent1 = (y + range_y) - SimH;
	// Create the looping variables needed
	int Dfinx;
	int Dfiny;
	int Afinx = 0;  //unless the finx values for A are changed this stop
	int Afiny = 0;  //the loop from executing
	int Asty = 0;
	// int Astx,Dstx,Dtsy are always default so variables not used from them
	// NB Astx, Asty and Dstx are always 0, 0 & x respectively
	// Dsty is always y, Afiny is always yextent1 if it is used
	// Now create the loop values;
	if (xextent0 <= SimW)   // No overlap with the eastern side
	{
		// Dstx, Dsty, Asty set by defaults
		//Astx & Afinx are not needed
		Dfinx = xextent0;
		// do we overlap the bottom?
		// Type B & D (overlap bottom, no overlap)
		if (yextent0>SimH)
		{
			// Type B (overlap bottom only)
			Dfiny = SimH;  // stop at the end
			Afiny = yextent1; // the overlap with the top
		}
		else Dfiny = yextent0;
	}
	else
	{
		// Type A & C overlap bottom & eastern edgdes
		if (yextent0>SimH)
		{
			// relies on the default start for Asty, Astx, Dstx, Dsty
			Afinx = xextent1;
			Afiny = yextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = SimH;
		}
		else
		{
			// Type C overlap left edge only
			// Astx & Afiny are not needed here
			//Astx, Dstx, Dsty set by default
			Afinx = xextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = yextent0;
		}
	}
	Dormouse_Base* Ap;
	// A Loop
	for (int i = 0; i<Afinx; i++)
	{
		for (int j = Asty; j<Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					if (Ap->SupplyAge() >= Age)
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
		// C Loop
		for (int j = y; j<Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					if (Ap->SupplyAge() >= Age)
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
	}
	// D Loop
	for (int i = x; i<Dfinx; i++)
	{
		for (int j = y; j<Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					if (Ap->SupplyAge() >= Age)
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
		// B Loop
		for (int j = 0; j<Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (Ap->SupplySex()) // male
				{
					if (Ap->SupplyAge() >= Age)
					{
						DormouseMap->SetMapValue(p_x, p_y, caller);
						return true;
					}
				}
			}
		}
	}
	// End of search algorithm
	DormouseMap->SetMapValue(p_x, p_y, caller);
	return false;
}
//---------------------------------------------------------------------------

int Dormouse_Population_Manager::SupplyCountFemales(unsigned p_x, unsigned p_y, unsigned p_TerrRange)
{
	// Before checking the map remove ourselves so we don't count
	TAnimal* caller = DormouseMap->GetMapValue(p_x, p_y);
	DormouseMap->ClearMapValue(p_x, p_y);
	int x = p_x - p_TerrRange;
	if (x<0) x += SimW;  // ensure we start in the simulation area!
	int y = p_y - p_TerrRange;
	if (y<0) y += SimH;
	int range_x = p_TerrRange * 2;
	int range_y = p_TerrRange * 2;
	int Females = 0;

	// create the extent variables
	int xextent0 = x + range_x;
	int yextent0 = y + range_y;
	int xextent1 = (x + range_x) - SimW;
	int yextent1 = (y + range_y) - SimH;
	// Create the looping variables needed
	int Dfinx;
	int Dfiny;
	int Afinx = 0;  //unless the finx values for A are changed this stop
	int Afiny = 0;  //the loop from executing
	int Asty = 0;
	// int Astx,Dstx,Dtsy are always default so variables not used from them
	// NB Astx, Asty and Dstx are always 0, 0 & x respectively
	// Dsty is always y, Afiny is always yextent1 if it is used
	// Now create the loop values;
	if (xextent0 <= SimW)   // No overlap with the eastern side
	{
		// Dstx, Dsty, Asty set by defaults
		//Astx & Afinx are not needed
		Dfinx = xextent0;
		// do we overlap the bottom?
		// Type B & D (overlap bottom, no overlap)
		if (yextent0>SimH)
		{
			// Type B (overlap bottom only)
			Dfiny = SimH;  // stop at the end
			Afiny = yextent1; // the overlap with the top
		}
		else Dfiny = yextent0;
	}
	else
	{
		// Type A & C overlap bottom & eastern edgdes
		if (yextent0>SimH)
		{
			// relies on the default start for Asty, Astx, Dstx, Dsty
			Afinx = xextent1;
			Afiny = yextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = SimH;
		}
		else
		{
			// Type C overlap left edge only
			// Astx & Afiny are not needed here
			//Astx, Dstx, Dsty set by default
			Afinx = xextent1;
			Dfinx = SimW;  // Stop at the end
			Dfiny = yextent0;
		}
	}
	Dormouse_Base* Ap;

	// A Loop
	for (int i = 0; i<Afinx; i++)
	{
		for (int j = Asty; j<Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{
					if (Ap->SupplyTerritorial() && Ap->SupplyMature())
					{
						Females++;
					}
				}
			}
		}
		// C Loop
		for (int j = y; j<Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{

					if (Ap->SupplyTerritorial() && Ap->SupplyMature())
					{
						Females++;
					}
				}
			}
		}
	}
	// D Loop
	for (int i = x; i<Dfinx; i++)
	{
		for (int j = y; j<Dfiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{
					if (Ap->SupplyTerritorial() && Ap->SupplyMature())
					{
						Females++;
					}
				}
			}
		}
		// B Loop
		for (int j = 0; j<Afiny; j++)
		{
			Ap = (Dormouse_Base*)(DormouseMap->GetMapValue(i, j));
			if (Ap)
			{
				if (!Ap->SupplySex()) // female
				{
					if (Ap->SupplyTerritorial() && Ap->SupplyMature())
					{
						Females++;
					}
				}
			}
		}
	}
	// End of search algorithm
	DormouseMap->SetMapValue(p_x, p_y, caller);
	return Females; // returns the number of adult females
}
//---------------------------------------------------------------------------

void Dormouse_Population_Manager::SendMessageMated(unsigned p_x, unsigned p_y, unsigned p_range)
{
	for (unsigned i = 0; i < TheArray[dob_Female].size(); i++)
	{
		Dormouse_Female *AFemale;
		AFemale = (Dormouse_Female *)TheArray[dob_Female][i];
		{
			// is it in the square defined by p_x,p_y & p_range
			unsigned x = AFemale->SupplyX();
			unsigned y = AFemale->SupplyY();
			// Need to know if female is within p_range of p_x, p_y
			unsigned dx = abs((int)(p_x - x));
			unsigned dy = abs((int)(p_y - y));
			if (dx > SimWH) dx = SimW - dx;
			if (dy > SimHH) dy = SimH - dy;
			if ((dx <= p_range) && (dy <= p_range))
			{
				// Mating occurs unless the female is already pregnant.
				if (!AFemale->SupplyPregnant())  /** @todo LADA: Should maybe also check if the female has got a litter*/
				AFemale->SetMated(true);
			}
		}
	}
}
//---------------------------------------------------------------------------





//---------------------------------------------------------------------------

//					Output probes

//---------------------------------------------------------------------------


void Dormouse_Population_Manager::TheTerritoryProbe()
{
	Dormouse_Base* DB;
	int year = m_TheLandscape->SupplyYearNumber();
	int day = m_TheLandscape->SupplyDayInYear();
	long Gday = m_TheLandscape->SupplyGlobalDate() - 365;

	char sex[4] = { 'm', 'f', 'M', 'F' };
	int Territory = 0;

	for (int dob = 0; dob <= (int)dob_Female; dob++)
	{
		unsigned int total = (unsigned int)TheArray[dob].size();
		for (unsigned j = 0; j<total; j++)
		{
			DB = dynamic_cast<Dormouse_Base*>(TheArray[dob][j]);
			if (DB->SupplyTerritorial()){
				Territory = 1;
			}
			else  Territory = 0;
			(*m_DormouseTerritoryFile) << year << '\t' << day << '\t' << Gday << '\t' << sex[dob] << '\t' << Territory << endl;
		}
	}
}
//---------------------------------------------------------------------------

void Dormouse_Population_Manager::ThePregnancyProbe()
{
	Dormouse_Female* DF;
	int year = m_TheLandscape->SupplyYearNumber();
	int day = m_TheLandscape->SupplyDayInYear();
	long Gday = m_TheLandscape->SupplyGlobalDate() - 365;

	int Pregnant = 0;

	// Adult females
		unsigned int total = (unsigned int)TheArray[3].size();
		for (unsigned j = 0; j<total; j++)
		{
			DF = dynamic_cast<Dormouse_Female*>(TheArray[3][j]);
			if (DF->SupplyPregnant()){
				Pregnant = 1;
			}
			else  Pregnant = 0;
			(*m_DormousePregnancyFile) << year << '\t' << day << '\t' << Gday << '\t' << Pregnant << endl;
		}
}
//---------------------------------------------------------------------------

void Dormouse_Population_Manager::TheMateProbe()
{
	Dormouse_Female* DF;
	int year = m_TheLandscape->SupplyYearNumber();
	int day = m_TheLandscape->SupplyDayInYear();
	long Gday = m_TheLandscape->SupplyGlobalDate() - 365;

	int Mated = 0;

	// Adult females
	unsigned int total = (unsigned int)TheArray[3].size();
	for (unsigned j = 0; j<total; j++)
	{
		DF = dynamic_cast<Dormouse_Female*>(TheArray[3][j]);
		if (DF->SupplyMated()){
			Mated = 1;
		}
		else  Mated = 0;
		(*m_DormouseMateFile) << year << '\t' << day << '\t' << Gday << '\t' << Mated << endl;
	}
}
//---------------------------------------------------------------------------

void Dormouse_Population_Manager::TheJuvenileProbe()
{
	int year = m_TheLandscape->SupplyYearNumber();
	int day = m_TheLandscape->SupplyDayInYear();
	long Gday = m_TheLandscape->SupplyGlobalDate() - 365;
	
	(*m_DormouseJuvenileFile) << year << '\t' << day << '\t' << Gday << '\t' << YoungProducedToday << endl;
}
//---------------------------------------------------------------------------

void Dormouse_Population_Manager::TheAgeProbe()
{
	Dormouse_Base* DB;
	int year = m_TheLandscape->SupplyYearNumber();
	int day = m_TheLandscape->SupplyDayInYear();
	long Gday = m_TheLandscape->SupplyGlobalDate() - 365;

	char sex[4] = { 'm', 'f', 'M', 'F' };
	int Age = 0;

	for (int dob = 0; dob <= (int)dob_Female; dob++)
	{
		unsigned int total = (unsigned int)TheArray[dob].size();
		for (unsigned j = 0; j<total; j++)
		{
			DB = dynamic_cast<Dormouse_Base*>(TheArray[dob][j]);
			Age = DB->SupplyAge();
			(*m_DormouseAgeFile) << year << '\t' << day << '\t' << Gday << '\t' << sex[dob] << '\t' << Age << endl;
		}
	}
}
//---------------------------------------------------------------------------

void Dormouse_Population_Manager::TheHibernationProbe()
{
	Dormouse_Base* DB;
	int year = m_TheLandscape->SupplyYearNumber();
	int day = m_TheLandscape->SupplyDayInYear();
	long Gday = m_TheLandscape->SupplyGlobalDate() - 365;

	char sex[4] = { 'm', 'f', 'M', 'F' };
	int hiber = 0;

	for (int dob = 0; dob <= (int)dob_Female; dob++)
	{
		unsigned int total = (unsigned int)TheArray[dob].size();
		for (unsigned j = 0; j<total; j++)
		{
			DB = dynamic_cast<Dormouse_Base*>(TheArray[dob][j]);
			if (DB->SupplyHibernation()){
				hiber = 1;
			}
			else  hiber = 0;
			(*m_DormouseHibernationFile) << year << '\t' << day << '\t' << Gday << '\t' << sex[dob] << '\t' << hiber << endl;
		}
	}
}
//---------------------------------------------------------------------------

void Dormouse_Population_Manager::TheRipleysOutputProbe(FILE* a_prb) {
	Dormouse_Female* DF;
	unsigned int totalF = (unsigned int)TheArray[dob_Female].size();
	int x, y;
	int w = m_TheLandscape->SupplySimAreaWidth();
	int h = m_TheLandscape->SupplySimAreaWidth();
	fprintf(a_prb, "%d %d %d %d %d\n", 0, w, 0, h, totalF);
	for (unsigned j = 0; j<totalF; j++)      //adult females
	{
		DF = dynamic_cast<Dormouse_Female*>(TheArray[dob_Female][j]);
		x = DF->Supply_m_Location_x();
		y = DF->Supply_m_Location_y();
		fprintf(a_prb, "%d\t%d\n", x, y);
	}
	fflush(a_prb);
}
//-----------------------------------------------------------------------------
