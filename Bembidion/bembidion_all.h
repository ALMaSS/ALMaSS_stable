/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------

#ifndef Bembidion_allH
#define Bembidion_allH

//---------------------------------------------------------------------------

extern const char* SimulationName;

typedef enum
{
  tobs_Initiation=0,
// Egg
  tobs_EDeveloping,
  tobs_Hatching,
  tobs_EDying,
// Larva
  tobs_LDeveloping,
  tobs_Pupating,
  tobs_LDying,
// Pupa
  tobs_PDeveloping,
  tobs_Emerging,
  tobs_PDying,
// Adult
  tobs_Foraging,
  tobs_Aggregating,
  tobs_Hibernating,
  tobs_Dispersing,
  tobs_ADying,
// Destroy
  tobs_Destroy,
} TTypesOfBeetleState;

//------------------------------------------------------------------------------

typedef enum
{
	bob_Egg = 0,
	bob_Larva,
	bob_Pupa,
	bob_Adult
} BeetleObjects;

//------------------------------------------------------------------------------

typedef vector<APoint> TListOfEggs;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Forward Declarations

class Bembidion_Population_Manager;
class Landscape;
class MovementMap;
class SimplePositionMap;



//------------------------------------------------------------------------------
/* Not used
class ListOfPositions
{
 public:
  int locs[2][30];
  int NoLocs;
};
//------------------------------------------------------------------------------
*/
class param_Point
{
 public:
  int x;
  int y;
  unsigned direction;
};
//------------------------------------------------------------------------------

class param_List15
{
 public:
  int BeenThereX[101];
  int BeenThereY[101];
  int nsteps;
};
//------------------------------------------------------------------------------

/** \brief A data class for Bembidion data */
class struct_Bembidion
{
 public:
  int DayDegrees;
  int x;
  int y;
  Landscape* L;
  Bembidion_Population_Manager * BPM;
  int HowMany;
};
//------------------------------------------------------------------------------

/**
\brief
Function class to compare to Eggs X
*/
class CompareEggX {
public:
  bool operator() ( APoint A1, APoint A2 ) const {
	  return (A1.m_x > A2.m_x);
  }
};
//------------------------------------------------------------------------------

/**
\brief
The class describing the base class for beetle objects objects
*/
class Bembidion_Base : public TAnimal
{
public:
	/** \brief Constructor */
	Bembidion_Base( int x, int y, Landscape* L,
		Bembidion_Population_Manager* BPM );
	/** \brief ReInit for object pool */
	void ReInit( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM );
	/** \brief  BeginStep - empty*/
    virtual void BeginStep(){}
	/** \brief  Step - empty */
    virtual void Step(){}
	/** \brief  EndStep - empty */
    virtual void EndStep(){}
	/** \brief  Common state Die */
    virtual void st_Die();
 // Attributes
	/** \brief  Current behavioural state */
    TTypesOfBeetleState CurrentBState;
	/** \brief  Pointer to the population manager */
    Bembidion_Population_Manager * m_OurPopulation;
	/** \brief  For experimental purposes */
	void CopyMyself(int a_beetle);
	/** \brief  For experimental purposes */
	void CopyMyselfB(int a_beetle);
};
//------------------------------------------------------------------------------
/**
\brief
The class describing the beetle Egg_List objects
*/
class Bembidion_Egg_List : public Bembidion_Base
{

/** The egg list is an optimisation to reduce memory and time to run
   it means that there are only 365 of them possible and that each on contains
   all the eggs laid on that day. The only real problem is that there is no longer a link from parent to
   offspring - so this version cannot be used with genetics without adding the genes to the APoint struct.\n
*/
public:
	/** \brief Egg_List class constructor */
    Bembidion_Egg_List(int today,Bembidion_Population_Manager* BPM, Landscape* L);
    /** \brief Egg_List class Step code */
	virtual void Step();
    /** \brief Egg_List class BeginStep code */
    virtual void BeginStep();
	/** \brief Add an egg to the list */
    void AddEgg(int x, int y) {
      APoint E;
      E.m_x=x;
	  E.m_y=y;
      EggList.push_back(E);
    };
	/** \brief Remove an egg from the list */
    void DeleteEgg(int x, int y); // Very slow do not use unless absolutely necessary
	/** \brief The list of eggs */
    TListOfEggs EggList;
protected:
    /** \brief Egg_List state development */
    int st_Develop();
    /** \brief Egg_List state hatching */
    void st_Hatch();
    /** \brief Egg_List non-temperature or density related mortality */
    void DailyMortality();
 // Attributes
	/** Records the day hatched for development calculations */
    int m_DayMade;
    // The list of eggs is in a vector of type APoint
	void SortXR();
};
//------------------------------------------------------------------------------

/**
\brief
The class describing the beetle larvae objects
*/
class Bembidion_Larvae : public Bembidion_Base {
public:
	/** \brief Larvae class constructor */
	Bembidion_Larvae( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM );
	/** \brief ReInit for object pool */
	void ReInit( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM );
	/** \brief Larvae class BeginStep code */
	virtual void BeginStep();
	/** \brief Larvae Step code */
	virtual void Step();
	/** \brief Kill this larva */
	virtual void KillThis();
protected:
	/** \brief Larvae state development */
	int  st_Develop();
	/** \brief Larvae state pupation */
	void st_Pupate();
	/** \brief Larvae non-temperature or density related mortality */
	virtual bool DailyMortality();
	/** \brief Larvae reactions to management events */
	virtual bool OnFarmEvent( FarmToDo event );
	/** \brief Determine larval temperature related mortality */
	inline bool TempRelatedLarvalMortality( int temp2 );
	// Attributes
	/** \brief Record larvae day degrees */
	double m_AgeDegrees;
	/** \brief Current larval growth stage (1-3) */
	int m_LarvalStage;
	/** Records the day hatched for development calculations */
	int m_DayMade;

};
//------------------------------------------------------------------------------

/**
\brief
The class describing the beetle pupae objects
*/
class Bembidion_Pupae : public Bembidion_Base
{
public:
	/** \brief Pupae class constructor */
    Bembidion_Pupae(int x, int y, Landscape* L,
                              Bembidion_Population_Manager* BPM);

	/** \brief ReInit for object pool */
	void ReInit( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM );
	/** \brief Pupae BeginStep code */
    virtual void BeginStep();
	/** \brief Pupae Step code */
    virtual void Step();
    /** \brief Kill this pupa */
    virtual void KillThis();
protected:
    /** \brief Pupal state development */
    int st_Develop();
    /** \brief Pupal state emergence */
    void st_Emerge();
    /** \brief Pupae non-temperature or density related mortality */
    virtual bool DailyMortality();
    /** \brief Pupal reactions to management events */
    virtual bool OnFarmEvent(FarmToDo event);
 // Attributes
	/** \brief Record pupal day degrees */
    double m_AgeDegrees;
	/** Records the day hatched for development calculations */
    int m_DayMade;

};
//------------------------------------------------------------------------------
/**
\brief
The class describing the adult (female) beetle objects
*/
class Bembidion_Adult : public Bembidion_Base
{
public:
	/** Constructor */
    Bembidion_Adult(int x, int y, Landscape* L,
                                            Bembidion_Population_Manager* BPM);
	/** \brief ReInit for object pool */
	void ReInit( int x, int y, Landscape* L, Bembidion_Population_Manager* BPM );
	/** Destructor */
    virtual ~Bembidion_Adult();
	/** \brief Adult Step */
    virtual void Step();
	/** \brief Adult BeginStep */
	virtual void BeginStep();
	/** \brief Adult EndStep */
	virtual void EndStep();
	/** Set body burden */
	void SetBodyBurden(double a_pcide) { m_body_burden = a_pcide; }
	/** Set body PPP threshold */
	void SetPPPThreshold(double a_thresh) { m_PPPThreshold = a_thresh; }
	/** Set body PPP effect probability */
	void SetPPPEffectProb(double a_conc) { m_PPPEffectProb = a_conc; }
	/** Set body PPP effect probability */
	void SetPPPEffectProbDecay(double a_decay) { m_PPPEffectProbDecay = a_decay; }
	/** Set body burden elimination rate for adults */
	void SetAdultPPPElimRate(double a_rate) { m_AdultPPPElimRate = a_rate; }
protected:
	/** \brief Intitialise attribute values */
    void Init();
	/** \brief Foraging behaviour */
    int st_Forage();
	/** \brief Aggregation behaviour */
    int st_Aggregate();
	/** \brief Hibernation behaviour */
    int st_Hibernate();
	/** \brief Intiate Dispersal behaviour */
    int st_Dispersal();
	/** \brief Daily ageing */
    bool st_Aging();
	/** \brief Density-independent winter mortality */
    bool WinterMort();
	/** \brief Density-dependent mortality */
    bool DDepMort();
	/** \brief Initiates the daily movement for the beelt */
    int DailyMovement(int p_distance, bool disp_aggreg);
	/** \brief Moves attempting egg laying under way */
    void MoveTo(int p_dist, unsigned p_direction, int p_turning);
    /** \brief Moves w.r.t. habitat quality only */
	inline int MoveTo_quality_assess();
	/** \brief Moves using a stopping rule for hibernation */
    int MoveToAggr(int p_dist, unsigned p_direction, int p_turning);
	/** \brief Produces the eggs */
    void Reproduce(int p_x, int p_y);
	/** \brief Does reproduction if possible */
    void CanReproduce();
	/** \brief Density-independent mortality */
    virtual bool DailyMortality();
    /** \brief Adult reactions to management events */
    virtual bool OnFarmEvent(FarmToDo event);
	/** \brief Hand pesticide events code for the beetle */
	virtual void InternalPesticideHandlingAndResponse();
  // Attributes
	/** \brief The number of negative day degrees experienced */
    double m_negDegrees;
    /** \brief The number of day degrees experienced in the spring which may trigger dispersal*/
	double m_HibernateDegrees;
	//double OldBest;
    /** \brief The last direction moved */
	int OldDirection;
    /** \brief The number of eggs produced */
	int m_EggCounter;
    /** \brief Signal reproductive readiness */
	bool m_CanReproduce;
    /** \brief A helper attribute when simulating movement */
	static param_List15 pList;
    /** \brief A helper attribute when simulating movement */
	static param_Point pPoint;
	/** \brief the daily elimination rate for pesticides */
	static double m_AdultPPPElimRate;
	/** \brief Current body burden of pesticide */
	double m_body_burden;
	/** \brief Current effect probability */
	double m_currentPPPEffectProb;
	/** \brief PPP effects threshold */
	static double m_PPPThreshold;
	/** \brief Effect probability on threshold excedence */
	static double m_PPPEffectProb;
	/** \brief Effect probability on threshold excedence */
	static double m_PPPEffectProbDecay;

};
//------------------------------------------------------------------------------

/** \brief The population manager class for beetles */
class Bembidion_Population_Manager: public Population_Manager
{
 public:
	/**   \brief Method to add beetles to the population */
   void CreateObjects(int ob_type, TAnimal *pvo,void* null ,
                                         struct_Bembidion * data,int number);
	/**   \brief Intialises the population manager */
	virtual void Init (void);
	/**   \brief Does day degree development calculations here */
   virtual void DoFirst (void);
	/**   \brief Replaces the Step function for the Egg_List */
   virtual void DoBefore (void);
	/**   \brief Removes dead or pupated larvae from the simulation */
   virtual void DoAlmostLast (void);
	/**   \brief Adds output adult locations to DoLast */
   virtual void DoLast (void);
	/**   \brief Overides the Population_Manager::Probe method */
   virtual float Probe(int ListIndex,probe_data* p_TheProbe );
   /** \brief Constructor */
   Bembidion_Population_Manager(Landscape* p_L);
   /** \brief Destructor */
   virtual ~Bembidion_Population_Manager();
   /** \brief Get adult population size */
   int SupplyAdPopSize() {return m_AdPopSize;}
   /** \brief Get egg population size */
   int SupplyEggPopSize() {return m_EPopSize;}
   /** \brief Get larval population size */
   int SupplyLarvaePopSize() {return m_LPopSize;}
   /** \brief Get pupal population size */
   int SupplyPupaePopSize() {return m_PPopSize;}
   /** \brief Get egg day degress for a specific day */
   double SupplyEDayDeg(int day) { return m_EDayDeg[day];}
   /** \brief Get larval day degress for a specific dayand instar */
   double SupplyLDayDeg(int L, int day) { return m_LDayDeg[day][L];}
   /** \brief Get pupal day degress for a specific day */
   double SupplyPDayDeg(int day) { return m_PDayDeg[day];}
 protected:
	/**   \brief Method to arbitrarily alter populations size */
   virtual void Catastrophe();
	/**   \brief Special output functionality */
   virtual void TheRipleysOutputProbe(FILE* a_prb);
	/**   \brief Special output functionality */
   virtual void TheReallyBigOutputProbe();
	/**   \brief Special output functionality */
   virtual void TheAOROutputProbe(  );
	/** \brief Used to specify legal starting habitats for simulation start-up */
	bool IsStartHabitat(int a_x, int a_y);
#ifdef __BEETLEPESTICIDE1
	/** \brief Locations output error handling */
    void LocOutputError( void );
	/** \brief Location output file open */
	void LocOutputOpen( void );
	/** \brief Records the locations of all beetles and classifies them as to infield, off-field or in-crop */
	void DoInFieldLocationOutput();
#endif
#ifdef __RECORD_RECOVERY_POLYGONS
   /** \brief Special pesticide recovery code */
   void RecordRecoveryPolygons();
   int m_RecoveryPolygons[101];
   int m_RecoveryPolygonsC[101];
#endif


//Attributes
 protected:
	/**   \brief Pointer to the landscape */
	Landscape* The_Landscape;
	/**   \brief  To store the current population size */
   int m_AdPopSize;
	/**   \brief  To store the current population size */
   int m_EPopSize;
	/**   \brief  To store the current population size */
   int m_LPopSize;
	/**   \brief  To store the current population size */
   int m_PPopSize;
	/**   \brief  Storage for daily day degrees for eggs */
   double m_EDayDeg[365];
	/**   \brief  Storage for daily day degrees for larve */
   double m_LDayDeg[365][3];
	/**   \brief  Storage for daily day degrees for pupae */
   double m_PDayDeg[365];
 public:
	/**   \brief Map of suitability for movement */
   MovementMap* m_MoveMap;
	/**   \brief  Optimised map of larval positions */
   SimplePositionMap* m_LarvaePosMap;  //   Modified ***CJT*** 26-05-2009
	/**   \brief  Optimsied map of adult positions */
   SimplePositionMap* m_AdultPosMap;  //   Modified ***CJT*** 26-05-2009
	/**   \brief  Storage for density-dependent mortality parameter */
   int LDDepMort0; // Must be range 1-8
	/**   \brief  Storage for density-dependent mortality parameter */
   double LDDepMort1; // 0-100
	/**   \brief  Storage for density-dependent mortality parameter */
   int ADDepMort0; //
	/**   \brief  Storage for density-dependent mortality parameter */
   double ADDepMort1; // 0-100
	/**   \brief  Daily temperature determined egg production */
   int TodaysEggProduction;
	/**   \brief  Replacement for TheArray[0] */
   Bembidion_Egg_List* m_EList[365];

#ifdef __BEETLEPESTICIDE1
protected:
	/**   \brief  In-field counter */
	int m_InFieldNo;
	/**   \brief  In-crop counter */
	int m_InCropNo;
	/**   \brief  Off-field counter */
	int m_OffFieldNo;
	/**   \brief  In crop tole reference */
	TTypesOfVegetation m_InCropRef;
	/**   \brief  Increments in field counter */
	void incInField() { m_InFieldNo++; }
	/**   \brief  Increments in crop counter */
	void incInCrop() { m_InCropNo++; }
	/**   \brief  Increments off field counter */
	void incOffField() { m_OffFieldNo++; }
	/** \brief Annual pesticide mortality output file open */
	void PestMortLocOutputOpen( void );
	/** \brief Annual pesticide mortality locations output */
	void PestMortLocOutput( void );
	/** \brief Pesticide mortality output error handling */
	void PestMortLocOutputError( void );
public:
	/**   \brief  Records the location of a beetle killed by a test pesticide */
	void RecordPesticideMortLoc(int a_x, int a_y)
	{
		// Need to get the element type, if a field need to check the crop
		TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(a_x, a_y);
		if  (tole == tole_Orchard) incInCrop();
		else if  (tole == tole_Field)
		{
			if (m_TheLandscape->SupplyVegType(a_x, a_y) == m_InCropRef) incInCrop(); 
		    else incInField();
		} else 
		{	
			incOffField();
		}
	}
#endif

};



#endif

