/**
\mainpage ALMaSS Rodenticide Model ODdox Documentation

\htmlonly 
<style type="text/css">
body {
    color: #000000
    background: white;
  }
h2  {
    color: #0022aa
    background: white;
  }
</style>
  <h1>
<small>
<small>
Version 1.0<p>
<small>
Created by:<br>
</small>
<br>
Chris J. Topping<br>
<br>
Department of Bioscience/n
Aarhus University<br>
Grenaavej 14<br>
DK-8410 Roende<br>
Denmark<br>
<br>
16th April 2014<br>
</small>
</h1>
<br>
\endhtmlonly
\n
<HR> 
<br>
\htmlonly
<h2><big>The Rodenticide Model description following ODdox protocol</big></h2>
\endhtmlonly
<br>
\htmlonly
<h2><big>Overview</big></h2>
\endhtmlonly
<br>
<h2>  1. Purpose </h2>
The rodenticide model is designed to predict the daily likelihood of a non-rat rodent encountered at any location in the landscape 
of having been exposed to anti-coagulents. This model is designed for use with further map analysis to determine the likelihood of a 
predator coinciding with the poisoned rodent, and therefore the spatially and temporally varying exposure of predators to anti-coagulents.<br>
Here we provide the model documentation in ODdox format simpley because it is part of the larger ALMaSS agent-based model (ABM) system. 
However, currently the rodenticide model is not an agent-based model, more a spatial simulation. Hence many of the headings below state simply that
this characteristic is not used in the model. In an ABM these would be filled out with more information. They are kept here simply to show that they 
have not been overlooked.<br>
<br>
<h2>  2. State variables and scales </h2>
Individuals in this model are the bait locations. Variables associated with these are the probability that a bait will be placed, the season for 
baiting, and the type of location (which affects the previous two).<br>
<br>
<h2>  3. Process Overview and Scheduling</h2>
The constructor for the RodenticideManager RodenticideManager::RodenticideManager reads in and handles the probability distributions for seasonal start dates for bait locations from BaitLocationsSeasonalStartProbs.txt. This file holds a column of daily probability for bait placement for each bait location type (values range from 0.0 to 1.0). Bait location types are woodland, urban building or countryside building. Death rate, 
immigration rate and dispersal rates are also stored and are common to all bait locations (these will be modified depending on the species modelled and are input variables).<br>
After this process is completed the rodenticide is modelled by calling RodenticideManager::Tick once each timestep. Tick arries out the following functions: <br>
1) Checks for bait location placement each day for each building or woodlot using RodenticideManager::DoPlaceBait<br>
2) For each active bait location creates simulated appearence of poisoned mice a the central bait point in RodenticideManager::DoImmigration<br>
3) Then calculate diffusion for every cell is calculated by RodenticideManager::DoDiffuse- this is time consuming! <br>
4) Finally RodenticideManager::DoDeath applies mortalities to all cells in the map which removes mice as they are assumed to die of the rodenticide poisoning.<br>
The overall result is that the whole rodenticide map is then traversed daily applying mortality, dispersal and immigration on a cell by cell basis generating a daily map of rodenticide exposed mouse activity for the whole landscape.<br>
Output can be generated per timestep or every interval of days, both specified as input variables (see Inputs below). Output can also be limited by year or a span of years.<br>
<br>
Predator modelling is handled by the RodenticidePredators_Population_Manager class and is handled as two separate stages. The first stage is to define predator territories where exposure to rodenticide could be evaluated. The second stage is collection of exposure information during the scenario runs.<br>

<h4>Defining predatory territories</h4>
Predator territories were assumed to be square. Determination of a territory was calculated separately for each predator type by the model based on three criteria:
1)	That the territory size was between a maximum and minimum size (measured as width), and based on literature estimates for each species; <br>
2)	That the territory does not overlap with another predator territory;<br>
3)	That the sum of the scores of the areas contained within the territory exceeded a minimum level. The territory score was built up of individual scores for each 1-m2 of territory. Each individual 1-m2 score was based on the landscape element type indexing a species specific set of landscape element type scores (e.g. a stone marten will score a building higher than a pine marten).<br>
<br>
On start-up the RodenticidePredators_Population_Manager constructore RodenticidePredators_Population_Manager::RodenticidePredators_Population_Manager reads the quality scores in from the correct predator input file. 
This depends on a config variable being correctly set: cfg_RodenticidePredatoryType and is done by RodenticidePredators_Population_Manager::ReadHabitatValues.<br>
Next, the habitat quality grid is created ready for predatory habitat assessment by RodenticidePredators_Population_Manager::CreatHabitatQualGrid(). This trawls through the landscape and produces a map of habitat quality as determined by the species specific habitat values read in previously.<br>
Once this is achieved the landscape should be populated with predators (although existing maps can also be used). Assuming new maps should be created (default) then RodenticidePredators_Population_Manager::CreateTerritories distributes predators and RodenticidePredators_Population_Manager::PredatorTerritoryOutput saves the new map once this is completed.
<br>
<h4>Collection of exposure information</h4>
This is done by RodenticidePredators_Population_Manager::Tick and simply takes each territory in turn and sums the amount of rodenticide present in that territory at each timestep. This information is used by RodenticideManager::DumpRodenticideMap and  RodenticideManager::RecordRodenticidePolyData to provide outputs either in map form or as mean exposure indices per territory.

<br>
\htmlonly
<h2><big>Design</big></h2>
\endhtmlonly
<br>
<h2>  4. Design Concepts</h2>
Since the model is a mechanistic simulation rather than an agent-based model most of these design concept categories have little relevance. <br>
<br>
<h3>  4.a Emergence</h3>
Emergence is in the form of rodenticide loads on landscape polygons in time and space.<br>
<h3>  4.b. Adaptation</h3>
Adaptation is not used in this model.<br>
<h3>  4.c Fitness</h3>
The fitness concept is not used in this model.<br> 
<h3>  4.d Prediction</h3>
Prediction is not used in this model.<br>
<h3>  4.e Sensing</h3>
Sensing is not used in this model.<br>
<h3>  4.f Interaction</h3>
The current model does not have interacting components per se, although territory placement is dependent on the placement of previous predator territories.<br>
<h3> 4.g Stochasticity</h3>
Stochasticity is used extensively in the model. Frequency of bait placement is a probabilistic function as is, diffusion and death rate of poisoned rodents.<br>
<h3>  4.h Collectives</h3>
Collectives are not used in this model.<br>
<h3>  4.i Observation</h3>
Output is produced in the form of a polygon table with time. The value in each cell represents the rodenticide load in rodents in that polygon and at 
that time. divided by the polygon area. Alternative outputs are time-stamped rodenticide loads per grid cell (1x1m) ( RodenticideManager::DumpRodenticideMap ).<br>
<h2> 5. Initialisation</h2>
Initialisation requires the provision of a map, and an input file listing possible bait locations and their types.<br>
<br>
<h2> 6. Inputs </h2>
<br>
The first four parameters are responsible for simulating the distribution in space of the probability of meeting a poisoned mouse. These are fitted to the 
small mammal dispersal curves observed from mark-release-recapture studies and bait box studies. The defaul values are for yellow-necked mouse.<br>
Parameter 1: #cfg_rodenticide_diffusionRate, input file name: "RODENTICIDE_DIFFUSIONRATE", default value 0.5<br>
Parameter 2: #cfg_rodenticide_immigrationRate, input file name: "RODENTICIDE_IMMIGRATIONRATE", default value 1.0<br>
Parameter 3: #cfg_rodenticide_deathRate, input file name: "RODENTICIDE_DEATHRATE", default value 0.048307<br>
Parameter 4: #cfg_rodenticide_gridsize, input file name: "RODENTICIDE_GRIDSIZE" default value 5<br>
<br>
The next three parameters determine the annnual probability of bait placement at any bait location (depending on type). This value tells us whether
a bait will be placed in a given year, but not when.<br>
Parameter 5: #cfg_rodenticide_BLtown_AnnFreq, input file name: "RODENTICIDE_BLTYPE_TOWN_ANNFREQ", default value 0.05)<br>
Parameter 6: #cfg_rodenticide_BLcountry_AnnFreq, input file name: "RODENTICIDE_BLTYPE_COUNTRY_ANNFREQ", default value 0.3333)<br>
Parameter 7: #cfg_rodenticide_BLwoodland_AnnFreq, input file name: "RODENTICIDE_BLTYPE_WOODLAND_ANNFREQ", default value 0.01)<br>
Parameter 8: #cfg_rodenticide_BLchristmastrees_AnnFreq, input file name: "RODENTICIDE_BLTYPE_CTREES_ANNFREQ", default value 0.0278<br>
<br>
These parameters are file inputs that determine the daily probability of bait placement (if one is to be placed that year) for each of the three 
bait location types. These files allow us to determine the seaonality of bait placement.<br>
Parameter 9: #cfg_rodenticide_BLtown_Length, input file name: "RODENTICIDE_BLTYPE_TOWN_LENGTH", default value 28<br>
Parameter 10: #cfg_rodenticide_BLcountry_Length, input file name: "RODENTICIDE_BLTYPE_COUNTRY_LENGTH", default value 365<br>
Parameter 11: #cfg_rodenticide_BLwoodland_Length, input file name: "RODENTICIDE_BLTYPE_WOODLAND_LENGTH", default value 180<br>
Parameter 12: #cfg_rodenticide_BLchristmasstrees_Length, input file name: "RODENTICIDE_BLTYPE_CTREE_LENGTH", default value 82<br>
<br>
The final set of parameters control output procedures. These fall into two groups:<br>
This first group is the output of a map showing the distribution of poisoned mice at any one moment. The map is dumped for the whole landscape and produces 
a very large file. This is normally used for small landscapes for fiiting the diffusion curves.<br>
Parameter 11: #cfg_rodenticide_dumpmapday, input file name: "RODENTICIDE_DUMPMAPDAY", default value 180. The day in the year to dump the rodenticide map.<br>
Parameter 12: #cfg_rodenticide_dumpmapstartyear, input file name: "RODENTICIDE_DUMPMAPSTARTYEAR", default value 99999. The first year to dump the rodenticide map. <br>
Parameter 13: #cfg_rodenticide_dumpmapendyear, input file name: "RODENTICIDE_DUMPMAPENDYEAR", default value -1. The last year to dump the rodenticide map. <br>
<br>
This second output group of parameters controls the primary output and will create an polygon by polygon total poisoned mouse total score (sum of per metre probabilities), as well
as the area, type and unique identifier for the polygon. The data will be saved at intervals thoughout the simulation as specified by these parameters.<br>
Parameter 14: #cfg_rodenticide_dumppolyinterval, input file name: "RODENTICIDE_DUMPPOLYINTERVAL", default value 180. The interval of days during the year after which to dump the polygon 
information. 180 will result in 3 dumps, day 0 day 180 and day 360.<br>
Parameter 15: #cfg_rodenticide_dumppolystartyear, input file name: "RODENTICIDE_DUMPPOLYSTARTYEAR", default value 99999. The first year to dump the rodenticide polygon information.<br>
Parameter 16: #cfg_rodenticide_dumppolyendyear, input file name: "RODENTICIDE_DUMPPOLYENDYEAR", default value -1. The last year to dump the rodenticide polygon information.<br>

<h2> 7. Interconnections</h2>
There are no sub-models or other interconnections not detailed above. However, the model does make use of the Landscape class in particular for distribution of building types, but also as a framework for landscape structure and habitat types.<br>
<br>
<br>
*/
