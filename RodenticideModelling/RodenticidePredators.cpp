/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file RodenticidePredators.cpp
\brief <B>The main source code for all rodenticide modelling predator behaviour and population manager</B>
*/
/**  \file RodenticidePredators.cpp
Version of  08 December 2013. \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/BinaryMapBase.h"
#include "../RodenticideModelling/RodenticidePredators.h"

//---------------------------------------------------------------------------

/** \brief Used to set the predator information to read in */
CfgInt cfg_RodenticidePredatoryType("RODPREDATOR_TYPE",CFG_CUSTOM,-1); // -1 will result in Zero predator habitat scores being created.
/** \brief Minimum territory width */
CfgInt cfg_RodenticidePredatoryMinTerr("RODPREDATOR_MINTERR",CFG_CUSTOM,100); 
/** \brief Maximum territory width */
CfgInt cfg_RodenticidePredatoryMaxTerr("RODPREDATOR_MAXTERR",CFG_CUSTOM,500); 
/** \brief Territory minimum quality */
CfgInt cfg_RodenticidePredatoryTargetQual("RODPREDATOR_TARGETQUAL",CFG_CUSTOM,100); 
/** \brief Unused */
CfgFloat cfg_RodenticidePredatoryTerrOverlap("RODPREDATOR_TERR_OVERLAP", CFG_CUSTOM, 1.0); // 1.0 = no overlap
/** \brief Used to flag whether we calculate or read in territory values */
CfgBool cfg_rodenticide_read_predators("RODENTICIDE_READ_PREDATORS",CFG_CUSTOM,false);

//---------------------------------------------------------------------------


RodenticidePredators_Population_Manager::~RodenticidePredators_Population_Manager()
{
	/**
	* When the manager is destroyed call the output functions for predator territories
	*/
	PredatorTerritoryOutput(false);
	delete m_qual_cache;
    delete m_qual_grid;
	delete m_occupancy_grid;
}

RodenticidePredators_Population_Manager::RodenticidePredators_Population_Manager(Landscape * a_L) : Population_Manager(a_L)
{
	m_predator_minHR = cfg_RodenticidePredatoryMinTerr.value() / 10; // Need divind by 10 for the grid (it is in 10 x 10m grids)
	m_predator_maxHR = cfg_RodenticidePredatoryMaxTerr.value() / 10;
	m_predator_targetQual = cfg_RodenticidePredatoryTargetQual.value();
	m_summeddays = 0.0;
	/**
	* On construction the first thing to do is to read the quality scores in from the correct predator input file. 
	* This depends on a config variable being correctly set: cfg_RodenticidePredatoryType
	*/
	ReadHabitatValues(cfg_RodenticidePredatoryType.value());
	/**
	* Create the habitat quality grid ready for predatory habitat assessment
	*/
	CreatHabitatQualGrid();
	/**
	* Now populate the landscape with predatory territories
	*/
	if (cfg_rodenticide_read_predators.value()) PredatorTerritoryInput(); 
	else 
	{
		CreateTerritories(); 
		PredatorTerritoryOutput(true);
	}

}
//---------------------------------------------------------------------------

void RodenticidePredators_Population_Manager::CreatHabitatQualGrid()
{
	/**
	* Need to do the basic read and quality assessment for the landscape,
	* before creating and allocating the predatory territories.
	* The first step is to get the map dimensions saved to local attributes.
	*/
	m_sim_w = m_TheLandscape->SupplySimAreaWidth();
	m_sim_h = m_TheLandscape->SupplySimAreaHeight();
	m_sim_w_div_10 = m_sim_w / 10;
	m_sim_h_div_10 = m_sim_h / 10;
	m_hash_size = m_TheLandscape->SupplyLargestPolyNumUsed() + 1;
	/**
	* Following that all the polygons in the landscape are pre-cached for their quality score - this is important for speed because it only needs to be done once per polygon.
	*/
	m_qual_cache = new double[m_hash_size];
	m_TheLandscape->RodenticidePredatorsEvaluation( this );
	/*
	* Next the memory required for operations is reserved for the quality grid and occupancy grid that we need for the territory finding operations. 
	* The occupancy grid also needs all elements set to false.
	*/
	int totalgrids = m_sim_w_div_10 * m_sim_h_div_10;
	m_qual_grid = new double[ totalgrids ];
	m_occupancy_grid = new bool [ totalgrids ];
	for (int i=0; i<totalgrids; i++)
	{
		m_occupancy_grid[ i ] = false;
	}
	/**
	* Finally go through the whole landscape in 10x10m blocks and fill in the quality grid scoresas a the mean of 100m2 i.e. 10 x 10 m squares
	*/
	for (int x=0; x< m_sim_w_div_10; x++)
	{
		for (int y = 0; y < m_sim_h_div_10; y++)
		{
			int cx = x*10;
			int cy = y*10;
			double qual = 0.0;
			for (int xx = cx; xx < cx+10; xx++)
			{
				for (int yy = cy; yy < cy+10; yy++)
				{
					int poly = m_TheLandscape->SupplyPolyRef(xx,yy);
					qual += m_qual_cache[poly];
				}
			}
			m_qual_grid[x + (y * m_sim_w_div_10)] = qual/=100.0;
		}
	}
}
//---------------------------------------------------------------------------

void RodenticidePredators_Population_Manager::ReadHabitatValues(int a_type)
{
	/**
	* This method picks the input file based on the parameter a_type and reads in the scores for each habitat for that predator type.
	* These are stored in #m_habquals for later use in habitat evaluation.
	*/
	fstream ifile;
	switch (a_type)
	{
	case 0:
		ifile.open("RodenticidePredator0_HabitatScores.txt",ios::in);
		break;
	default:
		ifile.open("RodenticidePredatorNone_HabitatScores.txt",ios::in);
	}
	if (!ifile)
	{
			m_TheLandscape->Warn("RodenticidePredators_Population_Manager::ReadHabitatValues","Could Not Open Predator Habitat Scores File");
			exit(0);
	}
	int n_values;
	ifile >> n_values;
	if (n_values != tole_Foobar)
	{
			m_TheLandscape->Warn("RodenticidePredators_Population_Manager::ReadHabitatValues"," - wrong number of habitat scores in RodenticidePredator_HabitatScores.txt");
			exit(0);
	}
	for (int i = 0; i< tole_Foobar; i++) ifile >> m_habquals[i];
	ifile.close();
}
//---------------------------------------------------------------------------

	
double RodenticidePredators_Population_Manager::EvaluatePoly(int a_poly)
{
	/**
	* Returns the specific score for this polygon based on the polygon type and the score stored in #m_habquals 
	*/
	TTypesOfLandscapeElement l_type = m_TheLandscape->SupplyElementType( a_poly );
	return m_habquals[(int) l_type];
}
//---------------------------------------------------------------------------

void RodenticidePredators_Population_Manager::CreateTerritories()
{
	m_NoTerritories = 0;
	int x0 = m_predator_maxHR / 2; // Our starting point x
	int x = (int) (x0+g_rand_uni()*(m_sim_w_div_10 - x0));
	int y = x0;  // Our starting point y
	int x1 = m_sim_w_div_10 - x0;   // End point x
	int y1 = m_sim_h_div_10 - x0;   // End point y
	//bool toggle = false;
	double qual;
	int range; 
	/** 
	* Starts with a minimum territory size territory and then increases the size until either a occupied square is found or max size is reached, or the quality level has been met.
	*/
	int tries = m_sim_w_div_10 * m_sim_h_div_10; // the number of grid points in the grid
	while ( tries-- > 0)
	{
		//if (toggle) if (y>=starty) break;
		range = m_predator_minHR; 
		bool found = false;
	    while ( ( range <= m_predator_maxHR ) && ( !found ))
	    {
			if ( !IsGridPositionValid( x, y, range ) ) 
			{
				if ( ++x >= x1 ) 
				{
					x = 0; //random( 3 );
					if ( ++y >= y1 ) 
					{
						tries = 0; // Gone all the way round so stop
						range = m_predator_maxHR +1;
						//toggle = true;
					}
				}
				continue;
			}
			// Location was OK, ie. not occupied.
			// Try the smallest possible territory located right at our starting
			// position.
			qual = EvaluateHabitat( x, y, range );
			// Is it suitable?
			if ( qual >= m_predator_targetQual ) 
			{
				// If so create it and claim the area on the grid
				// Is OK so create a territory
				NewTerritory( x, y, range, 0.0 );
				found = true;
			}
			else range++;
		}
		// Take a step and try again
		if (++x >= x1) 
		{
			x = x0;
			if (++y >= y1)
			{
				tries = 0; // Gone all the way round so stop
				range = m_predator_maxHR + 1;
				//toggle = true;
			}
		}
	}
}
//---------------------------------------------------------------------------

bool RodenticidePredators_Population_Manager::IsGridPositionValid( int & a_x, int & a_y, int a_range ) 
{
	/**
	* \return false if any occupied grid cell is found
	* If we are too near the edge then do not evaluate,
	*/
	int r2 = a_range / 2;
	int l_min_x = a_x - r2;
	int l_min_y = a_y - r2;
	int l_max_x = a_x + r2;
	int l_max_y = a_y + r2;
	/**
	* otherwise test each potential location to see if they are occupied - if so return false
	*/
	for (int i = l_min_x; i < l_max_x; i++)
	{
		for (int j = l_min_y; j < l_max_y; j++)
		{
			if ( m_occupancy_grid[ i + j * m_sim_w_div_10 ]  ) 
			{
				return false;
			}
		}
	}
	// No occupied squares found so return true
	return true;
}
//---------------------------------------------------------------------------

double RodenticidePredators_Population_Manager::EvaluateHabitat( int a_x, int a_y, int a_range )
{
	/** Looks through the area summing all scores. */
	double l_qual = 0.0;
	int r2 = a_range / 2;
	int l_min_x = a_x - r2;
	int l_min_y = a_y - r2;
	int l_max_x = a_x + r2;
	int l_max_y = a_y + r2;
	for (int x = l_min_x; x < l_max_x; x++)
	{
		for ( int y = l_min_y; y < l_max_y; y++ ) 
		{
			l_qual += m_qual_grid[ x + y * m_sim_w_div_10 ];
		}
	}
	return l_qual;
}
//---------------------------------------------------------------------------

void RodenticidePredators_Population_Manager::NewTerritory( int a_x, int a_y, int a_range, double a_exposure )
{
	RP_Territory rpt;
	rpt.m_exposure = a_exposure;
	rpt.m_range = (int) floor(0.5+(a_range * 10 * cfg_RodenticidePredatoryTerrOverlap.value()));
	rpt.m_x = a_x * 10;
	rpt.m_y = a_y * 10;
	m_Territories.push_back(rpt);
	// Claim the territory in the grid
	int r2 = a_range / 2;
	int l_min_x = a_x - r2;
	int l_min_y = a_y - r2;
	int l_max_x = a_x + r2;
	int l_max_y = a_y + r2;
	
	for (int i = l_min_x; i < l_max_x; i++)
	{
		for (int j = l_min_y; j < l_max_y; j++)
		{
			m_occupancy_grid[ i + j * m_sim_w_div_10 ] = true;
		}
	}
}
//---------------------------------------------------------------------------

void RodenticidePredators_Population_Manager::PredatorTerritoryOutput( bool a_initial)
{
	/**
	* Opens a standard output tab-delimited text file and dumps all predatory territory information in the format:<br>
	* Number of entries<br> Entry 1 x, Entry 1 y, Entry 1 size, Entry 1 exposure<br> 
	* Entry 2 x, Entry 2 y, Entry 2 size, Entry 2 exposure<br>
	* Entry 3 x etc....
	*/
	ofstream ofile("PredatorTerritoryOutputInfo.txt",ios::out);
	int sz =  (int) m_Territories.size();
	ofile << sz << endl;
	double asum=0.0;
	for (int i=0; i< sz; i++) asum+=m_Territories[i].m_exposure;
	if ((asum==0.0) || (m_summeddays<1)) a_initial = true;
	for (int i=0; i< sz; i++)
	{
		ofile << m_Territories[i].m_x << '\t';
		ofile << m_Territories[i].m_y << '\t';
		ofile << m_Territories[i].m_range << '\t';
		if (a_initial) ofile << 0.0 << endl; else ofile << m_Territories[i].m_exposure/m_summeddays << endl;
	}
	ofile.close();
}
//---------------------------------------------------------------------------

void RodenticidePredators_Population_Manager::PredatorTerritoryInput()
{
	/**
	* Opens a standard output tab-delimited text file and reads all predatory territory information in the format:<br>
	* Number of entries<br> Entry 1 x, Entry 1 y, Entry 1 size, Entry 1 exposure<br> 
	* Entry 2 x, Entry 2 y, Entry 2 size, Entry 2 exposure<br>
	* Entry 3 x etc....
	*/
	ifstream ifile("PredatorTerritoryOutputInfo.txt",ios::in);
	int sz;
	ifile >> sz;
	double skip;
	int x,y,range;
	m_Territories.resize(0);
	for (int i=0; i< sz; i++)
	{
		ifile >> x >> y  >> range >> skip;
		RP_Territory rpt;
		rpt.m_exposure = 0.0;
		rpt.m_range = range;
		rpt.m_x = x;
		rpt.m_y = y;
		m_Territories.push_back(rpt);
	}
	ifile.close();
}
//---------------------------------------------------------------------------

void RodenticidePredators_Population_Manager::Tick()
{
	/** 
	* Runs through all the territories and sums up the rodenticide exposure and accumulates this in each territory
	*/
	int sz =  (int) m_Territories.size();
	for (int t=0; t< sz; t++)
	{
		int x = m_Territories[t].m_x;
		int y = m_Territories[t].m_y;
		int r = m_Territories[t].m_range;
		int xr = x+r;
		int yr = y+r;
		if (xr >= m_sim_w ) xr = m_sim_w-1;
		if (yr >= m_sim_h ) yr = m_sim_h-1;
		for (int i=x; i<xr; i++)
			for (int j=y; j< yr; j++)
			{
				m_Territories[t].m_exposure += m_TheLandscape->SupplyRodenticide(i,j);
			}
	}
	m_summeddays++;
}
//---------------------------------------------------------------------------
int RodenticidePredators_Population_Manager::HowManyTerritoriesHere(int a_x, int a_y)
{
	int sum = 0;
	int sz = (int)m_Territories.size();
	for (int t = 0; t< sz; t++)
	{
		int r = m_Territories[t].m_range;
		int x = m_Territories[t].m_x - (r / 2);
		int y = m_Territories[t].m_y - (r / 2);
		if ((a_x >= x) && (a_x<x + r) && (a_y >= y) && (a_y <= y + r)) sum++;
	}
	return sum;
}