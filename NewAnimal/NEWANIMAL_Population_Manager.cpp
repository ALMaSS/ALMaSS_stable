/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file NewAnimalPopulationManager.cpp
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file NewAnimalPopulationManager.cpp
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../NEWANIMAL/NEWANIMAL.h"
#include "../NEWANIMAL/NEWANIMAL_Population_Manager.h"

//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------

NEWANIMAL_Population_Manager::~NEWANIMAL_Population_Manager (void)
{
   // Should all be done by the Population_Manager destructor
}
//---------------------------------------------------------------------------

NEWANIMAL_Population_Manager::NEWANIMAL_Population_Manager(Landscape* L) : Population_Manager(L)
{
    // Load List of Animal Classes
	m_ListNames[0] = "NEWANIMAL";
	m_ListNameLength = 1;
	// We need one vector for each life stage
	for (unsigned int i=0; i<(10-m_ListNameLength); i++)
	{
		TheArray.pop_back();
	}
    strcpy( m_SimulationName, "NEWANIMAL Simulation" );
	// Create some animals
	struct_NEWANIMAL* sp;
	sp = new struct_NEWANIMAL;
	sp->NPM = this;
	sp->L = m_TheLandscape;
	for (int i=0; i< 1000; i++) // This will need to be an input variable (config)
	{
		sp->x = random(SimW);
		sp->y = random(SimH);
		CreateObjects(0,NULL,sp,1); // 0 = our NEWANIMAL
	}
}

//---------------------------------------------------------------------------
void NEWANIMAL_Population_Manager::CreateObjects(int ob_type,
           TAnimal * ,struct_NEWANIMAL * data, int number)
{
   NEWANIMAL*  new_NEWANIMAL;
   for (int i=0; i<number; i++)
   {
       new_NEWANIMAL = new NEWANIMAL(data->x, data->y, data->L, data->NPM);
       TheArray[ob_type].push_back(new_NEWANIMAL);
   }
}
//---------------------------------------------------------------------------
