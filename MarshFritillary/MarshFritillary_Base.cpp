#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

MarshFritillary_Base::MarshFritillary_Base(int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape) : TAnimal(a_x, a_y, a_Landscape)
{
	m_OurPopulationManager = a_PopulationManager;
	m_StepDone=false;
	m_CurrentStateNo = abb_initiation;
	m_DayDegrees = 0;
}


MarshFritillary_Base::~MarshFritillary_Base()
{
	;
}

