#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

//  PARAMETERS
CfgFloat cfg_MarshFrit_EggDevelTarget("CFG_MARSHFRIT_EGGDEVELTARGET",CFG_CUSTOM,130.0);
CfgFloat cfg_MarshFrit_EggDevelThreshold("CFG_MARSHFRIT_EGGDEVELTHRESHOLD",CFG_CUSTOM,3.1);
CfgFloat cfg_MarshFrit_EggMortRate("CFG_MARSHFRIT_EGGMORTRATE",CFG_CUSTOM,0.0001);
// END PARAMETERS

	double MarshFritillary_EggMass::m_DevelopmentThreshold = 0;
	double MarshFritillary_EggMass::m_DevelopmentTarget = 0;
	double MarshFritillary_EggMass::m_EggMortRate = 0;


void MarshFritillary_EggMass::SetParameters()
{
	m_DevelopmentThreshold = cfg_MarshFrit_EggDevelThreshold.value();
	m_DevelopmentTarget = cfg_MarshFrit_EggDevelTarget.value();
	m_EggMortRate = cfg_MarshFrit_EggMortRate.value();
}

MarshFritillary_EggMass::MarshFritillary_EggMass( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape, int a_number)
	: MarshFritillary_Base( a_x, a_y, a_PopulationManager, a_Landscape)
{
	m_DayDegrees = 0.0;
	m_numberofeggs = a_number;
}

MarshFritillary_EggMass::~MarshFritillary_EggMass(void)
{
	;
}

void MarshFritillary_EggMass::Step()
{	/**
	Reimplementation from the base class to contain useful behaviour
	*/
	if (m_CurrentStateNo == abb_killthis || m_StepDone ) return;
	switch ((ButterflyBehaviour) m_CurrentStateNo)
	{
	  case abb_dead:
		m_CurrentStateNo = abb_killthis;
		m_StepDone = true;
		break;
	  case abb_initiation:
		  m_CurrentStateNo = abb_developing;
		  break;
	  case abb_developing:
		  m_CurrentStateNo = Develop();
		  m_StepDone = true;
		  break;
	  case abb_nextstage:
		  m_CurrentStateNo = Hatch();
		  break;
	  default:
		  m_OurLandscape->Warn("MarshFritillary_EggMass::Step- unknown behavioural state",NULL);
		  exit(1);
   }
}

/** 
* This code is identical to that of the larvae and pupae, but is needed because the developmental parameters are static members
* which means that any base class will be unable to access them. The alternative is pass them as parameters, but then that 
* will cost more time and the use of static is to save time and space.
*/
ButterflyBehaviour MarshFritillary_EggMass::Develop()
{	
/**
Porter, K. 1983. MULTIVOLTINISM IN APANTELES-BIGNELLII AND THE INFLUENCE OF WEATHER ON SYNCHRONIZATION WITH ITS HOST EUPHYDRYAS-AURINIA 
ENTOMOLOGIA EXPERIMENTALIS ET APPLICATA  Volume: 34   Issue: 2   Pages: 155-162

The eggs laid in early June take 35 +/- 2.6 days to hatch at 20 degrees C.

*/
	if (g_rand_uni() < m_EggMortRate) return abb_dead;
	double temptoday = m_OurLandscape->SupplyTemp();
	double dd = temptoday - m_DevelopmentThreshold;
	if (dd<0) dd = 0;
	m_DayDegrees+=dd;
	if (m_DayDegrees < m_DevelopmentTarget) return abb_developing;
	return abb_nextstage;
}

ButterflyBehaviour MarshFritillary_EggMass::Hatch()
{	
	struct_MFritillary amf;
	amf.x = m_Location_x;
	amf.y = m_Location_y;
	amf.number = m_numberofeggs;
	amf.L = m_OurLandscape;
	m_OurPopulationManager->CreateObjects(atomf_Larva,this,&amf,1); // larvae
	return abb_dead;
}
