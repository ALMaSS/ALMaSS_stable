#pragma once

class MarshFritillary_Base;
class Population_Manager;
class Landcape;
class ScalablePositionMap;

class struct_MFritillary
{
public:
	int x;
	int y;
	Landscape* L;
	int number;
};

class MarshFritillary_Population_Manager :
	public Population_Manager
{
public:
	MarshFritillary_Population_Manager(Landscape* p_L);
	virtual ~MarshFritillary_Population_Manager(void);
	void CreateObjectsStart(TAnimal * /* pvo */, struct_MFritillary * data, int number);
	void CreateObjects(ATypeOfMarshFritillary ob_type, TAnimal * /* pvo */, struct_MFritillary * data, int number);
	// Position map methods
	bool PosMapGet(int a_x, int a_y) { return m_LarvalPos->GetMapValue(a_x,a_y); }
	void PosMapSet(int a_x, int a_y) { m_LarvalPos->SetMapValue(a_x,a_y); }
	void PosMapClear(int a_x, int a_y) { m_LarvalPos->ClearMapValue(a_x,a_y); }
protected:
	void Init();
	virtual void DoBefore(void);
	ScalablePositionMap* m_LarvalPos;
	// POM & reporting methods
	int m_Females[365];
	int m_Pupae[365];
	int m_Larvae[365];
	int m_Eggs[365];
	double m_FirstEggHatchDate, m_FirstPupationDate, m_FirstEmergenceDate, m_FemaleDensity, m_PupaDensity, m_LarvaDensity, m_EggDensity;
	double m_MeanEggHatchN, m_MeanPupationN, m_MeanEmergenceN, m_FemaleDensityN, m_PupaDensityN, m_LarvaDensityN, m_EggDensityN;
	/** \brief Calculate a running egg density */
	void MeanEggDensityCalc(double a_Dens) { m_EggDensity = (m_EggDensity*m_EggDensityN++) + a_Dens; m_EggDensity/=m_EggDensityN; }
	/** \brief Calculate a running larval density */
	void MeanLarvaDensityCalc(double a_Dens) { m_LarvaDensity = (m_LarvaDensity*m_LarvaDensityN++) + a_Dens; m_LarvaDensity/=m_LarvaDensityN; }
	/** \brief Calculate a running pupal density */
	void MeanPupaDensityCalc(double a_Dens) { m_PupaDensity = (m_PupaDensity*m_PupaDensityN++) + a_Dens; m_PupaDensity/=m_PupaDensityN; }
	/** \brief Calculate a running female density */
	void MeanFemaleDensityCalc(double a_Dens) { m_FemaleDensity = (m_FemaleDensity*m_FemaleDensityN++) + a_Dens; m_FemaleDensity/=m_FemaleDensityN; }
	/** \brief Empty the POM data storage */
	void ZeroPOMCounts();
	/** \brief Collect data needed for POM stats */
	void POMStats();
	/** \brief Do POM stats output */
	void POMStatsOutput();
	/** \brief Calculate the density based on the previous years population numbers */
	void POMCalcDensities();
};

