#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

//  PARAMETERS
//

MarshFritillary_Female::MarshFritillary_Female( int a_x, int a_y, MarshFritillary_Population_Manager* a_PopulationManager, Landscape* a_Landscape)
	: MarshFritillary_Adult( a_x, a_y, a_PopulationManager, a_Landscape)
{
	DetermineTotalEggs();
	m_lastlayingdate = m_OurLandscape->SupplyDayInYear() - 6; // -6 to trigger laying on the first day after hatch
}


MarshFritillary_Female::~MarshFritillary_Female(void)
{
	;
}

void MarshFritillary_Female::Step(void)
{
	if (m_CurrentStateNo == abb_killthis || m_StepDone ) return;
	switch ((ButterflyBehaviour) m_CurrentStateNo)
	{
	  case abb_dead:
		m_CurrentStateNo = abb_killthis;
		m_StepDone = true;
		break;
	  case abb_initiation:
		  m_CurrentStateNo = abb_moving;
		  break;
	  case abb_dailyactivity:
		  m_CurrentStateNo = DailyActivity();
		  break;
	  case abb_moving:
		  m_CurrentStateNo = Moving();
		  break;
	  case abb_laying:
		  m_CurrentStateNo = EggLaying();
		  m_StepDone = true;
		  break;
	  default:
		  m_OurLandscape->Warn("MarshFritillary_EggMass::SubStep0- unknown behavioural state",NULL);
		  exit(1);
   }
}

ButterflyBehaviour MarshFritillary_Female::Moving(void)
{
/**
* \return { ButterflyBehaviour: abb_laying \n }
* Uses movement rules to determine the next position, then changes m_Location_x & m_Location_y \n
* Current version is random movement \n
* Distance moved depends on the weight, which depends on the number of eggs. However, in the first instance a random walk is used.
*/
	int d2 = 128; //2501 - m_Eggs;
	int d1 = d2*2+1;
	m_Location_x += (random(d1)-d2);
	m_Location_y += (random(d1)-d2);
	CorrectWrapRound();
	return abb_laying;
}

ButterflyBehaviour MarshFritillary_Female::EggLaying(void)
{
/**
* \return { ButterflyBehaviour: abb_dead or abb_dailyactivity \n }
* Eggs are laid in large batches on the underside of a leaf of the foodplant and, although the average batch contains around 300 eggs,
* some batches have been known to contain up to 600 eggs. \n
* Having laid their initial batch of eggs, additional eggs develop inside the female and these are subsequently laid in smaller 
* batches.
* Here we model this as a stochastically varying total potential egg production, of which 50% are laid in the first batch, then
* one quarter are laid in the next batch, and half again in the next, and so on. The trigger for a batch to be laid is being extant
* for one week since the last batch (first is laid immediately after emergence).\n
*/
	if (m_OurLandscape->SupplyVegType(m_Location_x, m_Location_y) == tov_NaturalGrass) 
	{
		int days = m_OurLandscape->SupplyDayInYear();
		if (days - m_lastlayingdate == 7) // She creates an egg mass, not individual eggs
		{
			struct_MFritillary amf;
			amf.x = m_Location_x;
			amf.y = m_Location_y;
			amf.number = DetermineEggNumber();
			m_Eggs-=amf.number;
			if (m_Eggs<0) amf.number += m_Eggs; // Prevent laying more eggs than she has
			amf.L = m_OurLandscape;
			m_OurPopulationManager->CreateObjects(atomf_Egg,this,&amf,1); 
		} 
	}
	return abb_dailyactivity;
}

int MarshFritillary_Female::DetermineEggNumber(void)
{
/**
* \return { int: the number of eggs produced \n }
* This method returns the number of eggs in a current egg mass. Here we use half the available eggs each time.
*/
	return 20; //m_Eggs/2;
}

void MarshFritillary_Female::DetermineTotalEggs(void)
{
/**
* \return { void }
* Determines the lifetime potential egg production. Should be 200-600 in the first batch, which is 50% of the total
*/
	m_Eggs = (int)(g_rand_uni()*800)+400;
}

