

#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string.h>

#include "../Landscape/ls.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;


MarshFritillary_Population_Manager::MarshFritillary_Population_Manager(Landscape* p_L)
   : Population_Manager(p_L)
{
	Init();
}


MarshFritillary_Population_Manager::~MarshFritillary_Population_Manager(void)
{
	delete m_LarvalPos;
}

void MarshFritillary_Population_Manager::Init(void)
{
 TheArray.resize(5);
 // Set the simulation name
 strcpy(m_SimulationName,"MarshFritillary");


 // Create the larval positon map
 m_LarvalPos = new ScalablePositionMap(m_TheLandscape, 2);
 
 
   MarshFritillary_EggMass* new_Egg;
   MarshFritillary_Larvae* new_Larva;
   MarshFritillary_Pupae* new_Pupa;
   MarshFritillary_Female* new_Female;
   new_Egg = new MarshFritillary_EggMass(0, 0, this, m_TheLandscape, 0);
   new_Egg->SetParameters();
   delete new_Egg;
   new_Larva = new MarshFritillary_Larvae(0, 0, this, m_TheLandscape, 0);
   new_Larva->SetParameters();
   delete new_Larva;
   new_Pupa = new MarshFritillary_Pupae(0, 0, this, m_TheLandscape, 0);
   new_Pupa->SetParameters();
   delete new_Pupa;
   new_Female = new MarshFritillary_Female(0, 0, this, m_TheLandscape);
   new_Female->SetParameters();
   delete new_Female;

   struct_MFritillary* as;
   as = new struct_MFritillary;
   as->L = m_TheLandscape;
   as->number = 2;
   for (int i=0; i<10; i++) // Start number
   {
	   as->x = random(m_TheLandscape->SupplySimAreaWidth());
	   as->y = random(m_TheLandscape->SupplySimAreaHeight());
	   CreateObjectsStart(NULL,as,100); // Initially create larvae randomly accross the map - TODO make more sensible
   }
   delete as;
   // Load List of Animal Classes
   m_ListNames[0]="EggMass";
   m_ListNames[1]="Larvae";
   m_ListNames[2]="Pupae";
   m_ListNames[3]="Male";
   m_ListNames[4]="Female";
   m_ListNameLength = 5;
   m_population_type = TOP_MarshFritillary;
   // Do the reporting initiation
   ZeroPOMCounts();
   ofstream ofile("MarshFrit_POM_Output.txt",ios::out);
   ofile << "Day" << '\t' << "FemaleDensity" << '\t' << "NoDaysExtant"  << '\t' << "Pupae:F" << '\t' 
	   << "Larvae:F" << '\t' << "EggMass:F" << '\t' << "FirstEggHatch" << '\t' << "FirstPupation"  << '\t' 
	   << "FirstEmergence"<<  endl;
   ofile.close();
}

void MarshFritillary_Population_Manager::CreateObjectsStart(TAnimal * /* pvo */, struct_MFritillary * data, int /* number */)
{
   MarshFritillary_Larvae* new_Larvae;
   new_Larvae = new MarshFritillary_Larvae(data->x, data->y, this, data->L, data->number);
   new_Larvae->InitiateDDStartOfSim();
   TheArray[atomf_Larva].push_back(new_Larvae);
}
//---------------------------------------------------------------------------

void MarshFritillary_Population_Manager::CreateObjects(ATypeOfMarshFritillary ob_type, TAnimal * /* pvo */, struct_MFritillary * data, int number)
{
   MarshFritillary_EggMass* new_Egg;
   MarshFritillary_Larvae* new_Larvae;
   MarshFritillary_Pupae* new_Pupae;
   MarshFritillary_Male* new_Male;
   MarshFritillary_Female* new_Female;

   for (int i=0; i<number; i++)
   {
    if (ob_type == 0)
    {
       new_Egg = new MarshFritillary_EggMass(data->x, data->y, this, data->L, data->number);
       TheArray[atomf_Egg].push_back(new_Egg);
    }
    if (ob_type == 1)
    {
       new_Larvae = new MarshFritillary_Larvae(data->x, data->y, this, data->L, data->number);
       TheArray[atomf_Larva].push_back(new_Larvae);
    }
    if (ob_type == 2)
    {
       new_Pupae = new MarshFritillary_Pupae(data->x, data->y, this, data->L, data->number);
       TheArray[atomf_Pupa].push_back(new_Pupae);
    }
    if (ob_type == 3)
    {
       new_Male = new MarshFritillary_Male(data->x, data->y, this, data->L);
       TheArray[atomf_Male].push_back(new_Male);
    }
    if (ob_type == 4)
    {
       new_Female = new MarshFritillary_Female(data->x, data->y, this, data->L);
       TheArray[atomf_Female].push_back(new_Female);
    }
   }
}
//---------------------------------------------------------------------------

void MarshFritillary_Population_Manager::DoBefore(void)
{
	POMStats();
	if (m_TheLandscape->SupplyDayInYear() == 364) 
	{
		POMCalcDensities();
		POMStatsOutput(); 
		ZeroPOMCounts();
	}
}
//---------------------------------------------------------------------------

void MarshFritillary_Population_Manager::POMStats()
{
	/** Needs to check each day to determine what statistics need to be created. Then collect and store the data for these. */
	int today = m_TheLandscape->SupplyDayInYear();
	m_Females[today] = (int) TheArray[atomf_Female].size();
	m_Pupae[today] = (int) TheArray[atomf_Pupa].size();
	m_Larvae[today] = (int) TheArray[atomf_Larva].size();
	m_Eggs[today] = (int) TheArray[atomf_Egg].size();
	;
}

void MarshFritillary_Population_Manager::POMCalcDensities()
{
	/** Here we calculate the mean number of females using all days where we have extant females.\n 
	  * The same procedure is used for eggs larvae and pupae. \n 
	  * It would be possible to calculate the median here too.\n
	*/
	double sumF = 0;
	double sumP = 0;
	double sumL = 0;
	double sumE = 0;
	double countF = 0;
	double countP = 0;
	double countL = 0;
	double countE = 0;
	for (int i=0; i<365; i++) 
	{
		if (m_Females[i]>0)
		{
			if (m_FirstEmergenceDate == -1) m_FirstEmergenceDate = i;
			countF++;
			sumF+=m_Females[i];
		}
		if (m_Pupae[i]>0)
		{
			if (m_FirstPupationDate == -1) m_FirstPupationDate = i;
			countP++;
			sumP+=m_Pupae[i];
		}
		if (m_Larvae[i]>0)
		{
			if ( (m_FirstEggHatchDate == -1) && (m_FirstEmergenceDate != -1) ) m_FirstEggHatchDate = i;
			countL++;
			sumL+=m_Larvae[i];
		}
		if (m_Eggs[i]>0)
		{
			countE++;
			sumE+=m_Eggs[i];
		}
	}
	m_EggDensity = sumE/countE;
	m_EggDensityN = countE;
	m_LarvaDensity = sumL/countL;
	m_LarvaDensityN = countL;
	m_PupaDensity = sumP/countP;
	m_PupaDensityN = countP;
	m_FemaleDensity = sumF/countF;
	m_FemaleDensityN = countF;
}

void MarshFritillary_Population_Manager::ZeroPOMCounts() 
{  
	m_FirstEggHatchDate   = -1;
	m_FirstPupationDate   = -1;
	m_FirstEmergenceDate  = -1;
	m_FemaleDensity  = 0;
	m_PupaDensity    = 0;
	m_LarvaDensity   = 0;
	m_EggDensity     = 0;
	m_FemaleDensityN = 0;
	m_PupaDensityN   = 0;
	m_LarvaDensityN  = 0;
	m_EggDensityN    = 0;	
	for (int i=0; i<365; i++) 
	{ 
		m_Females[i]=0; 
		m_Larvae[i]=0;
		m_Eggs[i]=0; 
		m_Pupae[i]=0; 
	} 
}

void MarshFritillary_Population_Manager::POMStatsOutput()
{
	ofstream ofile("MarshFrit_POM_Output.txt",ios::app);
	POMCalcDensities();
	double AgeStructureP,AgeStructureL,AgeStructureE;
	AgeStructureP = m_PupaDensity/m_FemaleDensity;
	AgeStructureL = m_LarvaDensity/m_FemaleDensity;
	AgeStructureE = m_EggDensity/m_FemaleDensity;
	if (ofile.is_open()) 
	{
		int day = m_TheLandscape->SupplyGlobalDate()-365;
		ofile << day << '\t' << m_FemaleDensity << '\t' << m_FemaleDensityN  << '\t' << AgeStructureP << '\t' 
			<< AgeStructureL << '\t' << AgeStructureE << '\t' << m_FirstEggHatchDate << '\t' << m_FirstPupationDate  << '\t' 
				<< m_FirstEmergenceDate << endl;
	}
	ofile.close();
}
