/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_all.h This file contains the header for all partridge lifestage classes</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 15th October 2008 \n
 \n
 Doxygen formatted comments in October 2008 \n
 Code by Chris Topping\n
*/
//---------------------------------------------------------------------------
#ifndef partridge_allH
  #define partridge_allH
//---------------------------------------------------------------------------


struct PartridgeCommunicationData;

class population_attributes {
public:
  void reset();
  void copyself(population_attributes* a_population_attributes);

  int m_NoClutches;
  int m_NoChicksHatched;
  int m_NoChicksSixWeeks;
  int m_NoMalesSept;
  int m_NoFemalesSept;
  int m_NoChicksSept;
  int m_NoShotBirds;
  int m_NoShotFemales;
  int m_NoOldMales;
  int m_NoOldFemales;
  double m_geomeanBroodSize;
  int m_NoClutchesReplacements;
  int m_NoClutchesPredated;
  int m_NoHensPredated;
  int m_NoPairsApril;
  int m_NoFemalesApril;
  int m_NoMalesApril;
  int m_NoTerritorialFemalesApril;
  int m_NoBirdsApril;
  int m_NoMalesDec;
  int m_NoFemalesDec;
  int m_NoDeadClutches;
  int m_NoAgDeadClutches;
  int m_NoDeadChicks;
  int m_NoDeadFemales;
  int m_NoDeadMales;
  int m_NoTerritorialFemalesMay;
  int m_NoNonTerritorialFemalesMay;
  int m_NoClutchesHatched;
  int m_FemalesYear1;
  int m_NoChick1sAug;
  int m_NoChick2sAug;
  int m_NoMalesAug;
  int m_NoFemsAug ;
  int m_NoStarvedChicks;

  // Increment functions
  void incNoClutches() {
    m_NoClutches++;
  }

  void decNoClutches() {
    m_NoClutches--;
    m_NoClutchesReplacements++;
  }

  void incNoClutchesHatched() {
    m_NoClutchesHatched++;
  }

  void incNoClutchesPredated() {
    m_NoClutchesPredated++;
  }

  void incNoHensPredated() {
    m_NoHensPredated++;
  }

  void incNoChicksHatched() {
    m_NoChicksHatched++;
  }

  void incNoChicksSixWeeks() {
    m_NoChicksSixWeeks++;
  }

  void SetPairsInApril( double pa ) {
    m_NoPairsApril = ( int )pa;
  }

  void SetFemalesInApril( double fa ) {
    m_NoFemalesApril = ( int )fa;
  }

  void SetMalesInApril( double fa ) {
    m_NoMalesApril = ( int )fa;
  }

  void SetTerritorialFemalesInApril( double fa ) {
    m_NoTerritorialFemalesApril = ( int )fa;
  }

  void SetBirdsInApril( double ba ) {
    m_NoBirdsApril = ( int )ba;
  }

  void SetBroodGeoMean( double gm ) {
    m_geomeanBroodSize = gm;
  }

  void setNoChicksSept( int n ) {
    m_NoChicksSept = n;
  }

  void setNoFemalesSept( int n ) {
    m_NoFemalesSept = n;
  }

  void setNoMalesSept( int n ) {
    m_NoMalesSept = n;
  }

  void setNoFemalesDec( int n ) {
    m_NoFemalesDec = n;
  }

  void setNoMalesDec( int n ) {
    m_NoMalesDec = n;
  }

  void incNoDeadClutches() {
    m_NoDeadClutches++;
  }

  void incNoAgDeadClutches() {
    m_NoAgDeadClutches++;
  }

  void incNoDeadChicks() {
    m_NoDeadChicks++;
  }

  void incNoDeadFemales() {
    m_NoDeadFemales++;
  }

  void incNoDeadMales() {
    m_NoDeadMales++;
  }

  void incNoShotBirds() {
    m_NoShotBirds++;
  }

  void incNoShotFemales() {
    m_NoShotFemales++;
  }

  void incNoStarvedChicks() {
    m_NoStarvedChicks++;
  }

  void setNoOldMales( int nm ) {
    m_NoOldMales = nm;
  }

  void setNoOldFemales( int nf ) {
    m_NoOldFemales = nf;
  }

  void SetTerritorialFemalesInMay(int nf) {
	m_NoTerritorialFemalesMay=nf;
  }

  void SetNonTerritorialFemalesInMay(int nf) {
	m_NoNonTerritorialFemalesMay=nf;
  }

  void AddToChicks6wks( int ch ) {
    m_NoChicksSixWeeks += ch;
  }

  void setNoChick1sAug( int ch ) {
	  m_NoChick1sAug = ch;
  }

  void setNoChick2sAug( int ch ) {
	  m_NoChick2sAug= ch;
  }

  void setNoMalesAug( int nm ) {
	  m_NoMalesAug = nm;
  }

  void setNoFemsAug( int nf ) {
	  m_NoFemsAug = nf;
  }
  population_attributes();
  ~population_attributes();
};

/**
\brief
Class for calculating k-factors
*/
class k_factors {
/**
This class contains the functionality required to make two types of k-factors the first type is the 'real' k-factor, the second is the way
Dick Potts calculated his. \n
Not yet functional/tested - 5thOct2008\n
*/

protected:
  FILE * kfactorsfile;
  double m_rk1, m_rk2, m_rk3, m_rk4, m_rk5, m_rk6;
  double m_dpk1, m_dpk2, m_dpk3, m_dpk4, m_dpk5;

  void reset();
  void DumpInfoDatabase1( int, int, double );
  void DumpInfoDatabase();
public:
  population_attributes* m_ThisYear;
  population_attributes* m_LastYear;
  void calcreal_1();
  void calcreal_2();
  void calcreal_3();
  void calcDickPottsk();
  //Output
  void Output_kfactors();
  void CreateInfoDatabaseFile();
  // Constructor/Destructor
  k_factors();
  ~k_factors();
};


typedef enum {
  pars_Initiation = 0,

  // Covey
  pars_CMoving,

  // Clutch
  pars_ClDeveloping, pars_ClHatching, pars_ClDying,

  // Chicks
  pars_ChDeveloping, pars_ChMaturing, pars_ChDying,

  // Male
  pars_MFlocking, pars_MFindingMate, pars_MPairing, pars_MGuardingMate, pars_MCaringForYoung, pars_MDying, pars_MFollowingMate,

  // Female
  pars_FFlocking, pars_FFindingTerritory, pars_FBuildingUpResources, pars_FMakingNest, pars_FLaying, pars_FStartingNewBrood,
       pars_FIncubating, pars_FCaringForYoung, pars_FAttractingMate, pars_FDying,
       // Covey
       pars_CoveyDissolve, pars_CoveyBeing,
       // Misc.
       pars_Destroy
}
Partridge_State;


/**
\brief
Partridge object types
*/
typedef enum {
  pob_Clutch, pob_Chick, pob_Chick2, pob_Male, pob_Female, pob_Covey
}
Partridge_Object;



class Partridge_Base;
class Partridge_Clutch;
class Partridge_Nestling;
class Partridge_PreFledgeling;
class Partridge_Female;
class Partridge_Male;
class Partridge_Covey;
class Partridge_Population_Manager;



template < class T >
class TPartridgeList : public vector < T > {
public:
  TPartridgeList() : vector < T > () {
    ;
  }
  /* double Probe(probe_data* p_TheProbe) { return 0.0; } */
};



/**
\brief
Struct to pass covey information
*/
class Covey_struct {
public:
  int x;
  int y;
  Partridge_Base * first_member;
};



/**
\brief
Struct to basic partridge information
*/
class Partridge_struct {
public:
  int x;
  int y;
  int bx;
  int by;
  Landscape * L;
  Partridge_Covey * m_covey;
  int family_counter;

  virtual ~Partridge_struct() {;
  }
};



/**
\brief
Struct to pass clutch information
*/
class Clutch_struct : public Partridge_struct {
public:
  int No;
  Partridge_Female * Mum;
};



/**
\brief
Struct to pass chick information
*/
class Chick_struct : public Partridge_struct {
public:
  Partridge_Female * Mum;
  bool sex;
};



/**
\brief
Struct to pass adult partridge information
*/
class AdultPartridge_struct : public Partridge_struct {
public:
  bool sex;
  int age;
};



typedef TPartridgeList < Partridge_Clutch * > Partridge_ClutchList;


/**
\brief
Base class for all partridge classes
*/
class Partridge_Base : public TAnimal {
protected:
	/** \brief Used to pass information to outputs */
	int m_signal;
  	/** \brief Individual bird ID */
	long m_id;
	/** \brief Family ID */
	int m_family_counter;
	/** \brief Age in days */
	int m_age;
	/** \brief x-coord of birth */
	int m_born_x;
	/** \brief y-coord of birth */
	int m_born_y;
	/** \brief If has uncle status in the covey */
	bool m_UncleStatus;
	/** \brief Type of pob object this is */
	Partridge_Object m_object_type;
	/** \brief Pointer to the covey */
	Partridge_Covey * m_covey;
	/** \brief Current behavioural state */
	Partridge_State m_state;
	/** \brief Background mortality test */
	virtual bool DailyMortality();
	/** \brief Debug */
	virtual void CheckMatePointers() {;}

public:
	/** \brief Pointer to the population manager */
	Partridge_Population_Manager * m_OurPopulationManager;
	/** \brief Supply object type */
	Partridge_Object GetObjectType() {
		return m_object_type;
	}

	/** \brief Supply uncle status */
	bool GetUncleStatus() {
		return m_UncleStatus;
	}

	/** \brief Set uncle status */
	void SetUncleStatus( bool a_Status ) {
		m_UncleStatus = a_Status;
	}

	/** \brief Can we mate? */
	bool PossibleMate( Partridge_Base * a_partridge );

	/** \brief Overridden base function */
	virtual bool ArePaired() { return false; }

	/** \brief Supply covey pointer */
	Partridge_Covey * GetCovey( void ) {
		return m_covey;
	}
	/** \brief Supply state */
	Partridge_State GetState( void ) {
		return m_state;
	}

	/** \brief Set state */
	void SetState( Partridge_State a_pars ) {
		m_state = a_pars;
	}

	/** \brief Supply family ID */
	int GetFamily( void ) {
		return m_family_counter;
	}

	/** \brief Supply age */
	int GetAge( void ) {
		return m_age;
	}

	/** \brief Set age */
	void SetAge( int a_age ) {
		m_age = a_age;
	}

	/** \brief Supply ID */
	long GetID( void ) {
		return m_id;
	}

	/** \brief Set family ID */
	void SetFamily( unsigned int family ) {
		m_family_counter = family;
	}

	/** \brief Set covey pointer */
	void SetCovey( Partridge_Covey * a_covey ) {
		m_covey = a_covey;
	}

	/** \brief Create our own covey */
	void MakeCovey();
	/** \brief Swap coveys */
	void SwitchCovey( Partridge_Covey * a_covey );

	/** \brief Constructor for Partridge_Base */
	Partridge_Base( int a_born_x, int a_born_y, int a_x, int a_y, int a_family_counter, Partridge_Covey * a_covey,
       Landscape * a_map, Partridge_Population_Manager * a_manager );

	/** \brief Destructor */
	/** The destructor removes an animal from its flock. */
	virtual ~Partridge_Base( void );

	/** \brief Duplicate this object */
	virtual void CopyMyself(int a_Ptype);
};



/**
\brief The partridge clutch class
*/
class Partridge_Clutch : public Partridge_Base {
	/** \brief Development state */
	Partridge_State ClDeveloping( void );
	/** \brief Dying state */
	void ClDying( void );
	/** \brief No. of eggs */
	int m_clutch_size;
	/** \brief Pointer to mother */
	Partridge_Female * m_mother;
	/** \brief Are we being incubated? */
	bool m_underincubation;

public:
  	/** \brief Generic dying handler */
	virtual void Dying() {
		ClDying();
	}
	/** \brief Flag under incubation */
	void OnStartIncubating() { m_underincubation = true; }
	/** \brief Clutch BeginStep */
	virtual void BeginStep( void );
	/** \brief ClutchStep */
	virtual void Step( void );
	/** \brief Clutch EndStep */
	virtual void EndStep( void );
	/** \brief Background mortality test */
	virtual bool DailyMortality();
	/** \brief Handle farm event */
	virtual bool OnFarmEvent( FarmToDo event );
	/** \brief Killed by management */
	void AgDying();
	/** \brief Message handler */
	void OnEaten();
	/** \brief Message handler */
	void OnGivenUp();
	/** \brief Message handler */
	void OnMumDead();
	/** \brief Set no. eggs */
	void SetClutchSize(int cs) {
	  m_clutch_size=cs;
	}

	/** \brief Supply no eggs */
	int GetClutchSize( void ) {
		return m_clutch_size;
	}

	/** \brief Supply mother pointer */
	Partridge_Female * GetMother( void ) {
		return m_mother;
	}

	/** \brief 	Constructor */
	Partridge_Clutch( int a_x, int a_y, Partridge_Female * a_mother, Partridge_Covey * a_flock, Landscape * a_map,
       int a_num_eggs, int a_family_counter, Partridge_Population_Manager * a_manager );

	/** \brief Destructor */
	virtual ~Partridge_Clutch( void ) {;}
};



/** \brief Partridge chick class */
class Partridge_Chick : public Partridge_Base {
protected:
	/** \brief Development */
  virtual Partridge_State ChDeveloping( void );
	/** \brief Maturation */
  virtual Partridge_State ChMaturing( void );
	/** \brief Chick Dying */
  void ChDying( void );
	/** \brief Sex */
  bool m_sex;
	/** \brief Days spent starving */
  int m_starve;

public:
	/** \brief Chick BeginStep */
	virtual void BeginStep( void );
	/** \brief Chick Step */
	virtual void Step( void );
	/** \brief Chick EndStep */
	virtual void EndStep( void );
	/** \brief Background mortality test */
	virtual bool DailyMortality();
	/** \brief Farm event handler */
	virtual bool OnFarmEvent( FarmToDo event );

	/** \brief Generic dying handler */
	virtual void Dying() {
		ChDying();
	}

	/** \brief Message handler */
	void OnYouAreDead();

	/** \brief Constructor */
	Partridge_Chick( int a_x, int a_y, Partridge_Female * a_mother, Partridge_Covey * a_flock, Landscape * a_map, bool a_sex,
       int a_family_counter, Partridge_Population_Manager * a_manager );

	/** \brief Destructor */
	virtual ~Partridge_Chick( void ) {;
	}
};



/**
\brief The partridge Chick2 class
*/
class Partridge_Chick2 : public Partridge_Chick {
	/** \brief State developing */
	virtual Partridge_State ChDeveloping( void );
	/** \brief State maturing */
	virtual Partridge_State ChMaturing( void );

public:
	/** \brief Chick2 BeginStep */
	virtual void BeginStep( void );
	/** \brief Chick2Step */
	virtual void Step( void );
	/** \brief Chick2 EndSte[ */
	virtual void EndStep( void );
	/** \brief Background mortality check */
	virtual bool DailyMortality();
	/** \brief Management event handler */
	virtual bool OnFarmEvent( FarmToDo event );
	/** \brief Constructor */
	Partridge_Chick2( int a_x, int a_y, Partridge_Female * a_mother, Partridge_Covey * a_flock, Landscape * a_map, bool a_sex,
       int a_family_counter, Partridge_Population_Manager * a_manager );
	/** \brief Destructor */
	virtual ~Partridge_Chick2( void ) {;}
};



/**
\brief The partridge male class
*/
class Partridge_Male : public Partridge_Base {
	/** \brief State male flocking */
	Partridge_State MFlocking( void );
	/** \brief State male finding mate */
	Partridge_State MFindingMate( void );
	/** \brief State male pairing */
	Partridge_State MPairing( void );
	/** \brief State male guarding mate */
	Partridge_State MGuardingMate( void );
	/** \brief State male caring for young */
	Partridge_State MCaringForYoung( void );
	/** \brief State male follow mate */
	Partridge_State MFollowingMate( void );
	/** \brief State male dying */
	void MDying( void );

	/** \brief Physiological lifespan */
	int m_lifespan;
	/** \brief Old mate pointer */
	Partridge_Female * m_MyOldWife;
	/** \brief Current mate pointer */
	Partridge_Female * m_MyMate;
	/** \brief A debug function */
	virtual void CheckMatePointers();

public:
	/** \brief A debug function */
  int AmIaMember();

	/** \brief General dying handler */
	virtual void Dying() {
		MDying();
	}

	/** \brief Male BeginStep */
	virtual void BeginStep( void );
	/** \brief Male Step */
	virtual void Step( void );
	/** \brief Male EndStep */
	virtual void EndStep( void );
	/** \brief Background mortality check */
	virtual bool DailyMortality();
	/** \brief Management event handler */
	virtual bool OnFarmEvent( FarmToDo event );

	/** \brief General direct mortality handler */
	virtual void KillThis() {
		MDying();
	}

	/** \brief remove mate pointer */
	void UnSetMate( void ) {
		m_MyMate=NULL;
	}

	/** \brief Are we paired? */
	virtual bool ArePaired() {
		if (m_MyMate!=NULL) return true;
		return false;
	}

	/** \brief Are we paired? */
	Partridge_Female * GetMate( void ) {
		return m_MyMate;
	}

	/** \brief Constructor */
	Partridge_Male( int a_born_x, int a_born_y, int a_x, int a_y, Partridge_Covey * a_flock, Landscape * a_map,
       int a_family_counter, Partridge_Population_Manager * a_manager );

	/** \brief Destructor */
	virtual ~Partridge_Male( void ) {;}

	/** \brief Message handler */
	void OnLookAfterKids( void );
	/** \brief Message handler */
	void StartBreedingBehaviour( void );
	/** \brief Message handler */
	void OnMating( Partridge_Female * a_mate );
	/** \brief Message handler */
	void OnMateDying();
	/** \brief Message handler */
	void OnFoundATerrtitory();
	/** \brief Message handler */
	void OnChicksMatured();
	/** \brief Message handler */
	void OnChicksDead();
	/** \brief Message handler */
	void OnStoppingBreeding();
	/** \brief Message handler */
	void OnFemaleGivingUp();
	/** \brief Message handler */
	void OnMovingHome();

	/** \brief Set old mate pointer */
	void SetOldMate( Partridge_Female * pf) {
		m_MyOldWife=pf;
	}

	/** \brief Supply old mate pointer */
	Partridge_Female * GetOldMate( void ) {
		return m_MyOldWife;
	}

	/** \brief Forget any old mate */
	void RemoveOldMate( bool a_knockon );
};



/**
\brief The partridge female class
*/
class Partridge_Female : public Partridge_Base {
protected:
// Methods
  /**  \brief Female flocking state */
  Partridge_State FFlocking( void );
  /**  \brief Female pairing state */
  Partridge_State FPairing( void );
  /**  \brief Female building up resources state */
  Partridge_State FBuildingUpResources( void );
  /**  \brief Female making nest state */
  Partridge_State FMakingNest( void );
  /**  \brief Female laying state */
  Partridge_State FLaying( void );
  /**  \brief Female starting new brood state */
  Partridge_State FStartingNewBrood( bool a_waslaying );
  /**  \brief Female incubating state */
  Partridge_State FIncubating( void );
  /**  \brief Female caring for young state */
  Partridge_State FCaringForYoung( void );
  /**  \brief Female finding 'territory' state */
  Partridge_State FFindingTerritory( void );
  /**  \brief Female attracting mate state */
  Partridge_State FAttractingMate( void );
  void FDying( void );
  /**  \brief Debug function - checking mate consistency  */
  virtual void CheckMatePointers();
  /** \brief Calls Partridge_Population_Manager::GetNestingCoverDensity  */
  double GetNestingCoverDensity();

// Attributes
  /** \brief Flag for having found a suitable breeding area */
  bool m_HaveTerritory;
  /** \brief Flag for having produced a clutch */
  bool m_clutchproduced;
  /** \brief Current clutch size */
  int m_clutch_size;
  /** \brief Current clutch attempt */
  int m_clutch_number;
  /** \brief Days spent nest building */
  int m_nest_counter;
  /** \brief Physiological lifespan in days */
  int m_lifespan;
  /** \brief Adds stochasticity to the light triggering of breeding */
  int m_triggerlayingvar;
  /** \brief Records the day laying started. */
  int m_startlayingday;
  /** \brief Delay before breeding can commence */
  int m_buildupwait;
  /** \brief A pointer to the current mate if any */
  Partridge_Male * m_MyMate;
  /** \brief A record of the previous mate if any */
  Partridge_Male * m_MyOldHusband;
  /** \brief A pointer to any current clutch */
  Partridge_Clutch * m_MyClutch;

public:
	/** \brief General dying handler area */
	virtual void Dying() {
		FDying();
	}

	/** \brief Female BeginStep */
	virtual void BeginStep( void );
	/** \brief Female Step */
	virtual void Step( void );
	/** \brief Female EndStep */
	virtual void EndStep( void );
	/** \brief Background mortality check */
	virtual bool DailyMortality();
	/** \brief Management event handler */
	virtual bool OnFarmEvent( FarmToDo event );

	/** \brief Message handler */
	void OnMateDying( PartridgeCommunicationData * pc_data );
	/** \brief Message handler */
	void OnClutchDeath();
	/** \brief Message handler */
	void OnClutchMown();
	/** \brief Message handler */
	void OnSetMyClutch( PartridgeCommunicationData * pc_data );
	/** \brief Message handler */
	void OnChicksMatured();
	/** \brief Message handler */
	void OnChicksDead();
	/** \brief Message handler */
	void OnMating( PartridgeCommunicationData * pc_datae );
	/** \brief Message handler */
	void OnWaitForMale();
	/** \brief Message handler */
	void OnEggsHatch();

	/** \brief �Supply clutch size */
	int GetClutchSize( void ) {
		return m_clutch_size;
	}

	/** \brief Supply clutch pointer */
	Partridge_Clutch * GetClutch( void ) {
		return m_MyClutch;
	}

	/** \brief Supply mate pointer */
	Partridge_Male * GetMate( void ) {
		return m_MyMate;
	}

	/** \brief Forget mate */
	void UnSetMate( void ) {
		m_MyMate=NULL;
	}

	/** \brief Are we paired? */
	virtual bool ArePaired() {
		if (m_MyMate!=NULL) return true;
		return false;
	}

	/** \brief Direct mortality message */
	virtual void KillThis() {
		FDying();
	}

	/** \brief  Supply old mate pointer */
	Partridge_Male * GetOldMate( void ) {
		return m_MyOldHusband;
	}

	/** \brief Set old mate pointer */
	void SetOldMate( Partridge_Male * pm) {
		m_MyOldHusband=pm;
	}

	/** \brief Forget old mate */
	void RemoveOldMate(bool a_knockon) {
		if (m_MyOldHusband!= NULL) {
			if (!a_knockon) { // if it is a knock on then don't start a recursive set of calls
				m_MyOldHusband->RemoveOldMate(true);
			}
			m_MyOldHusband= NULL;
		}
	}

	/** \brief Constructor */
	Partridge_Female( int a_born_x, int a_born_y, int a_x, int a_y, Partridge_Covey * a_flock, Landscape * a_map,
       int a_family_counter, Partridge_Population_Manager * a_manager );

	/** \brief Destructor */
	virtual ~Partridge_Female( void ) {;}

	/** \brief Have we a breeding 'territory'? */
	bool HaveTerritory( void ) {
		return m_HaveTerritory;
	}
};
#endif
