/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_Communication.cpp This file contains the code for the partridge communication class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 15th October 2008 \n
 \n
 Doxygen formatted comments in October 2008 \n
 Code by Chris Topping\n
*/
//---------------------------------------------------------------------------

// Includes from ALMaSS

using namespace std;

#include <cmath>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>

// Includes from ALMaSS
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Partridge/Partridge_Covey.h"
#include "../Partridge/Partridge_Communication.h"

//---------------------------------------------------------------------------

//
// Partridge_Communication.cpp
//

/**
Just makes sure that all message fields are empty. Used before putting new data in.
*/
void  Partridge_Communication::ClearData(PartridgeCommunicationData* pc_data) {
  pc_data->m_clutch=NULL;
  pc_data->m_chick=NULL;
  pc_data->m_chick2=NULL;
  pc_data->m_male=NULL;
  pc_data->m_female=NULL;
  pc_data->m_covey=NULL;
  pc_data->m_HaveTerritory=false;
  pc_data->m_boolean=false;
  pc_data->m_int=0;
  pc_data->m_float=0;
}
//------------------------------------------------------------------------------
/**
This passes messages listed in the TypeOfPartridge_Communication enum. It is a place that errors can be trapped but is not strictly necessary.
Data is passed in PartridgeCommunicationData which can be checked agains the sender and the receiver information if __PAR_PCDEBUG is enabled.\n
This complex method is not really necessary, but it is a wonderful place
to trap communication errors. If run-times really get to be a problem, then
it might be worth dispensing with this nicety.\n
 */
bool  Partridge_Communication::PassMessage(PartridgeCommunicationData* pc_data, TypeOfPartridge_Communication pc) {
	switch (pc) {
    case pcomm_KillChick:
      #ifdef __PAR_PCDEBUG
      if (pc_data->m_chick->GetObjectType() != pob_Chick) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_KillChick" );
        exit(101);
      }
      #endif
      pc_data->m_chick->OnYouAreDead(); // No data to pass
      break;
    case pcomm_Mating:
      #ifdef __PAR_PCDEBUG
      if (pc_data->m_female->GetMate() != NULL) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_Mating" );
        exit(102);
      }
      #endif
      pc_data->m_female->OnMating(pc_data);
      break;
    case pcomm_MatingM:
      #ifdef __PAR_PCDEBUG
      if (pc_data->m_male->GetMate() != NULL) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_MatingM" );
        exit(102);
      }
      #endif
      pc_data->m_male->OnMating(pc_data->m_female);
      break;
    case pcomm_MaleDying:
      #ifdef __PAR_PCDEBUG
      if (pc_data->m_female->GetMate() != pc_data->m_male) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_MaleDying" );
        exit(103);
      }
      #endif
      pc_data->m_female->OnMateDying(pc_data);
      break;
    case pcomm_EggsHatch:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetClutch() != pc_data->m_clutch) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_EggsHatch1" );
          exit(104);
        }
      if ( pc_data->m_male ) if (pc_data->m_male->GetMate() != pc_data->m_female) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_EggsHatch2" );
        exit (113);
      }
      if ((pc_data->m_female->GetMate() == NULL)&& ( pc_data->m_male != NULL)) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_EggsHatch3" );
        exit (113);
      }
      #endif
      // It is possible that there is no male if he was killed under incubation
      if ( pc_data->m_male ) pc_data->m_male->OnLookAfterKids( );
      pc_data->m_female->OnEggsHatch();
      break;
    case pcomm_SetClutch:
      #ifdef __PAR_PCDEBUG
      if (pc_data->m_female->WhatState() != pars_FMakingNest) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_SetClutch" );
        exit(111);
      }
      #endif
      pc_data->m_female->OnSetMyClutch(pc_data);
      break;
    case pcomm_WaitForMale:
      #ifdef __PAR_PCDEBUG
      if (pc_data->m_female->GetMate() != NULL) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_WaitForMale" );
        exit(106);
      }
      #endif
      pc_data->m_female->OnWaitForMale();
      break;
    case pcomm_ClutchDead:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetClutch() != pc_data->m_clutch) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_ClutchDead" );
          exit(107);
        }
      #endif
      pc_data->m_female->OnClutchDeath();
      break;
    case pcomm_AllInfertile:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetClutch() != pc_data->m_clutch) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_ClutchDead" );
          exit(107);
        }
      #endif
      pc_data->m_female->OnClutchDeath();
      break;
    case pcomm_ClutchMown:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetClutch() != pc_data->m_clutch) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_ClutchMown" );
          exit(107);
        }
      #endif
      pc_data->m_female->OnClutchMown();
      break;
    case pcomm_MumDeadC:
	  // Possible to get here and have an old mate but no mate
	  if (pc_data->m_female->GetOldMate() != NULL) {
      #ifdef __PAR_PCDEBUG
		  if (pc_data->m_female != pc_data->m_female->GetOldMate()->GetOldMate()) {
			g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_MumDeadC3 - old mate mismatch" );
			exit(117);
        }
      #endif
		pc_data->m_female->GetOldMate()->RemoveOldMate(false);
      }
      // Possible that this is called with no male
      if (pc_data->m_male != NULL) {
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetMate() != pc_data->m_male) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_MumDeadC1" );
        exit(108);
        }
      #endif
        pc_data->m_male->OnMateDying();
      }
      // Quite possible this is called with no clutch
      if (pc_data->m_clutch != NULL) {
      #ifdef __PAR_PCDEBUG
      if ((pc_data->m_female->WhatState() != pars_FIncubating) && (pc_data->m_female->WhatState() != pars_FLaying)) {
        g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_MumDeadC2" );
        exit(112);
      }
      #endif
        pc_data->m_clutch->OnMumDead();
      }
	  break;
    case pcomm_FemaleGivingUp:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetMate() != pc_data->m_male) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_FemaleGivingUp" );
        exit(109);
        }
      #endif
      pc_data->m_male->OnFemaleGivingUp();
      break;
    case pcomm_ClutchEaten:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetClutch() == NULL) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_ClutchEaten" );
        exit(110);
        }
      #endif
      pc_data->m_clutch->OnEaten();
      break;
    case pcomm_AllChicksDead:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female) if (pc_data->m_female->GetMate() != pc_data->m_male) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_AllChicksDead" );
        exit(111);
        }
      #endif
      // All chicks are dead so tell the sad news to the parents, if they live
      if (pc_data->m_male) pc_data->m_male->OnChicksDead();
      if (pc_data->m_female) {
        pc_data->m_female->OnChicksDead();
      }
      break;
    case pcomm_ChicksMature:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female) if (pc_data->m_female->GetMate() != pc_data->m_male) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_ChickMature" );
        exit(114);
        }
      #endif
      // All chicks are dead so tell the sad news to the parents, if they live
      if (pc_data->m_male) pc_data->m_male->OnChicksMatured();
      if (pc_data->m_female) pc_data->m_female->OnChicksMatured();
      break;
    case pcomm_StoppingBreeding:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetMate() != pc_data->m_male) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_StoppingBreeding" );
        exit(115);
        }
      #endif
      pc_data->m_male->OnStoppingBreeding();
      break;
    case pcomm_MovingHome:
      #ifdef __PAR_PCDEBUG
        if (pc_data->m_female->GetMate() != pc_data->m_male) {
          g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "pcomm_MovingHome" );
        exit(116);
        }
      #endif
      pc_data->m_male->OnMovingHome();
      break;
    default:
    g_msg->Warn( WARN_BUG, "Partridge_Communication::PassMessage():", "No matching message" );
     exit(199);
  }
  #ifdef __PARPCDEBUG
  ClearData(pc_data);
  #endif
  return true;
}
//------------------------------------------------------------------------------
