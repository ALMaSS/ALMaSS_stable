/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Partridge_Covey.cpp This file contains the code for the partridge covey class</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Original version 15th October 2008 \n
 \n
 Doxygen formatted comments in October 2008 \n
 Code by Frank Nikolaisen & Chris Topping\n
*/
//
// Partridge_Covey.cpp
//
//---------------------------------------------------------------------------

#include <cmath>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Partridge/Partridge_Communication.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Partridge/Partridge_Covey.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"

//---------------------------------------------------------------------------

// *********
// * Covey *
// *********


extern CfgInt cfg_par_mature_threshold;
extern double g_FoodNeed[ 200 ];
extern int g_MaxWalk[ 200 ];
//extern double g_move_veg_structure[ 4 ] [ 4 ];
extern CfgInt cfg_IndividualEmigration;

/** \brief Default for 2000s, 1950s=1.0 */
CfgFloat cfg_NOT1950sinsects("PAR_NOTFIFTIESINSECTSSCALER",CFG_CUSTOM,0.1);
/** \brief Default for 2000s, 1950s=1.0 */
CfgFloat cfg_parinsectScaler("PAR_INSECTSCALER",CFG_CUSTOM, 0.5);
/** \brief Nesting quality for OK areas */
CfgFloat cfg_terr_qual_good("PAR_TERR_QUAL_GOOD",CFG_CUSTOM,4.0);
/** \brief Nesting quality for fields */
CfgInt cfg_nest_field( "PAR_NEST_FIELD", CFG_CUSTOM, 1 );
/** \brief Nesting quality for roadsize verges */
CfgInt cfg_nest_roadside( "PAR_NEST_ROADSIDE", CFG_CUSTOM, 100 );
/** \brief Nesting quality for railway ebankments */
CfgInt cfg_nest_railway( "PAR_NEST_RAILWAY", CFG_CUSTOM, 100 );
/** \brief Nesting quality for field boundaries */
CfgInt cfg_nest_fieldboundary( "PAR_NEST_FIELDBOUNDARY", CFG_CUSTOM, 200 );
/** \brief Nesting quality for low yield pasture */
CfgInt cfg_nest_permpastlowyield( "PAR_NEST_PERMPASTLOWYIELD", CFG_CUSTOM, 1 );
/** \brief Nesting quality for very low grazed pasture */
CfgInt cfg_nest_permpasttussocky( "PAR_NEST_PERMPASTTUSSOCKY", CFG_CUSTOM, 1 );
/** \brief Nesting quality for setaside */
CfgInt cfg_nest_permsetaside( "PAR_NEST_PERMSETASIDE", CFG_CUSTOM, 100 );
/** \brief Nesting quality for natural grass*/
CfgInt cfg_nest_naturalgrass( "PAR_NEST_NATURALGRASS", CFG_CUSTOM, 100 );
/** \brief Nesting quality for hedgebank type 0 */
CfgInt cfg_nest_hedgebank0( "PAR_NEST_HEDGEBANKZERO", CFG_CUSTOM, 1 );
/** \brief Nesting quality for hedgebank type 1 */
CfgInt cfg_nest_hedgebank1( "PAR_NEST_HEDGEBANKONE", CFG_CUSTOM, 100 );
/** \brief Nesting quality for hedgebank type 2 */
CfgInt cfg_nest_hedgebank2( "PAR_NEST_HEDGEBANKTWO", CFG_CUSTOM, 200 );

/** \brief Limit to homerange radius */
/**
The territory maximum radius (width is not the right name for this)
heavily influences the movements of the covey. If a covey suddenly
decides to stray of an obvious path, it is most often due to the
distance to the peg starting to gain importance.
*/
CfgInt g_par_terr_max_width( "PAR_TERR_MAX_WIDTH", CFG_CUSTOM, 500 );
/** \brief Minimum nest distance to 'bad' areas */
CfgInt g_par_nest_min_dist_bad_areas( "PAR_NEST_MIN_DIST_BAD_AREAS", CFG_CUSTOM, 25 );
/** \brief Minimum nest distance to other nests */
CfgInt g_par_nest_min_dist_other_nest( "PAR_NEST_MIN_DIST_OTHER_NEST", CFG_CUSTOM, 50 );
/** \brief Mortalty chance on parent death, young */
CfgInt cfg_par_parentdead_age1( "PAR_PARENTDEAD_AGEONE", CFG_CUSTOM, 35 );
/** \brief Mortalty chance on parent death, older */
CfgInt cfg_par_parentdead_age2( "PAR_PARENTDEAD_AGETWO", CFG_CUSTOM, 63 );
/** \brief Mortalty chance on parent death */
CfgInt cfg_par_parentdead_mort( "PAR_PARENTDEAD_MORT", CFG_CUSTOM, 100 );
//CfgInt cfg_par_lowqualthreshold( "PAR_LOWQUALTHRESHOLD", CFG_CUSTOM, 18 );
/** \brief An arbitrary value for determine movement quality */
CfgInt cfg_par_highqualthreshold( "PAR_HIGHQUALTHRESHOLD", CFG_CUSTOM, 18 );
/** \brief Extra mortality applied to chicks on death of parents */
CfgInt cfg_par_chick_extra_mort( "PAR_CHICK_EXTRA_MORT", CFG_CUSTOM, 200 );

/** \brief The length of memory for backtracking */
CfgInt cfg_par_movequal_histlimit( "PAR_MOVEQUAL_HISTLIMIT", CFG_CUSTOM, 3 );

/** \brief Chance of imperfect movement decisions */
/**
This parameter heavily influences 'non-sensical' moves. As parts per 10.000. 100 is one percent.
*/
CfgInt cfg_par_move_fuzzy_chance( "PAR_MOVE_FUZZY_CHANCE", CFG_CUSTOM, 50 );

// Enter inverse values for speed!
/* \brief Used to calculate array indices */
CfgFloat cfg_par_bio_hindrance_inv( "PAR_BIO_HINDRANCE", CFG_CUSTOM, 0.00667 );
/* \brief Used to calculate array indices */
CfgFloat cfg_par_hei_hindrance_inv( "PAR_HEI_HINDRANCE", CFG_CUSTOM, 0.04 );


/* \brief Resolution of fly to testing */
/** Number of meters (approximately) that we will have between
points around the perimeter of our scanning circle during FlyTo().
Notice that if one flies out to a 1000 meter radius, then at
1 meter we will in a worst case scenario have to look at a total of
6283 locations of the map for a single call to FlyTo()!
However if we raise this value to more than 1, then we risk stepping
over thin hedges etc...\n
Cannot be zero or negative.\n
*/
CfgInt cfg_par_flyto_stepsize( "PAR_FLYTO_STEPSIZE", CFG_CUSTOM, 1 );


/**
\brief Enable the calculation of the repulsive inter-covey 'force'.
*/
CfgBool cfg_par_force_enable( "PAR_COVEY_FORCE_ENABLE", CFG_CUSTOM, true );

/* \brief Ignore limit */
/**
The distance beyond which the influence of a particular other covey is ignored, say, 1KM or so.
This is the distance in meters SQUARED!, ie 1000000.0 for 1KM.
*/
CfgFloat cfg_par_force_ignore_dist_sq( "PAR_COVEY_FORCE_IGNORE_DIST_SQ", CFG_CUSTOM, 250000.0 );

/** Force a daily update of the territory size for non-fixed coveys.
CfgBool cfg_par_force_terr_size( "PAR_COVEY_FORCE_TERR_SIZE", CFG_CUSTOM, true );
*/

/* \brief The value below which covey force is ignored */
/** If the absolute length of the vector force of all the other coveys
 within a distance of PAR_COVEY_FORCE_IGNORE_DIST_SQ is below
 PAR_COVEY_FORCE_IGNORE_BELOW as specified here, then the covey peg
 will not move, even though the total force on it isn't zero. This can
 used to introduce a certain 'sluggishness' in the peg position, so it
 is prevented from 'flittering' back and forth at the slightest
 influence. Note: Values needed here are small, as the forces
 on a given day are small unless covey pegs are *really* close
 together.*/
CfgFloat cfg_par_force_ignore_below( "PAR_COVEY_FORCE_IGNORE_BELOW", CFG_CUSTOM, 0.0 );

/* \brief Covey peg pulling force */
/** Which proportion of the daily speed delta provided by the force vector that is actually added to the peg position. */
CfgFloat cfg_par_force_speed( "PAR_COVEY_FORCE_SPEED", CFG_CUSTOM, 2.0 );

/* \brief unused */
/** If the total absolute length of the vector force induced on a covey
 from the surrounding ones is below this value, then the m_force_low
 flag is set. The value of this flag can be read at any time via the
 covey ForceLow( void ) method. Not used at present.\n
*/
CfgFloat cfg_par_force_below( "PAR_COVEY_FORCE_BELOW", CFG_CUSTOM, 0.1 );

/* \brief unused */
/**
If the total sum of forces on a covey is above this value, then the m_essence_low flag is set. This flag is read via the
LifeEssenceLow() and is supposed to signal a condition, where an area is overcrowded for some reason.
*/
CfgFloat cfg_par_force_abs_limit( "PAR_COVEY_FORCE_ABS_LIMIT", CFG_CUSTOM, 100.0 );

/* \brief Use peg drift? */
/**
Enable peg 'drift' towards the covey's actual, physical position. Not done for fixed pegs.
*/
CfgBool cfg_par_covey_drift_enable( "PAR_COVEY_DRIFT_ENABLE", CFG_CUSTOM, true );

/** \brief Drift force on the peg */
/** Daily speed in meters of the covey peg drift.
 Carefull! It is easy to exceed the daily force incluence
 by adjusting cfg_par_force_speed and cfg_par_covey_drift_speed.*/
CfgFloat cfg_par_covey_drift_speed( "PAR_COVEY_DRIFT_SPEED", CFG_CUSTOM, 2.0 );


// Stuff referenced elsewhere.
extern double m_move_veg_const[];

//double m_move_veg_structure[4][4];

/** \brief The minimum merging distance */
CfgFloat cfg_min_merge_dist( "PAR_MIN_MERGE_DIST", CFG_CUSTOM, 50.0 );
/** \brief The merging probability */
CfgInt cfg_par_merging_chance( "PAR_MERGE_CHANCE", CFG_CUSTOM, 5 ); //0-100
/** \brief The Earliest merging date */
CfgInt cfg_par_firstmerge( "PAR_FIRSTMERGE", CFG_CUSTOM, 150 ); //0-365
/** \brief The latest merging date */
CfgInt cfg_par_lastmerge( "PAR_LASTMERGE", CFG_CUSTOM, 59 ); //0-365

double m_move_veg_const[ tov_Undefined + 100 ];
// Vegetation height, vegetation biomass.
//extern double m_move_veg_structure[4][4];
int m_move_list[ 8 ];
double m_move_dir_qual[ 8 ];
bool m_move_allowed[ 8 ];
static int m_move_select_list[ 8 ];
static int m_move_select_size;
static unsigned int g_covey_id_counter = 0;

#define TRIG_TABLE_LENGTH 8000
#define TRIG_TABLE_LENGTH_INV 0.00025
static double * g_trig_sin;
static double * g_trig_cos;

#ifdef CONF_INLINES
inline
#endif
/**
Select either 1 or -1 or fuzzy chance 0
*/
int Partridge_Covey::MoveSelectFuzzy( void ) {
  if ( random( 10000 ) >= cfg_par_move_fuzzy_chance.value() )
    return 0;

  if ( random( 100 ) < 50 )
    return -1;

  return 1;
}


#ifdef CONF_INLINES
inline
#endif
/**
Select either 1 or -1 or fuzzy chance 0
*/
bool Partridge_Covey::MoveSelectLimit( int a_limit, int /* a_x */, int /* a_y */ ) {
  bool l_found = false;
  m_move_select_size = 0;

  int l_goto = NormInc( m_move_whence_we_came + 4, 8 );
  int l_goto_sub = NormDec( l_goto - 1, 8 );
  int l_goto_add = NormInc( l_goto + 1, 8 );

  if ( m_move_list[ l_goto ] == a_limit ) {
    m_move_select_list[ m_move_select_size++ ] = l_goto;
    l_found = true;
  }
  if ( m_move_list[ l_goto_sub ] == a_limit ) {
    m_move_select_list[ m_move_select_size++ ] = l_goto_sub;
    l_found = true;
  }
  if ( m_move_list[ l_goto_add ] == a_limit ) {
    m_move_select_list[ m_move_select_size++ ] = l_goto_add;
    l_found = true;
  }
  if ( l_found ) {
    return true;
  }

  l_goto_sub = NormDec( l_goto_sub - 1, 8 );
  l_goto_add = NormInc( l_goto_add + 1, 8 );
  if ( m_move_list[ l_goto_sub ] == a_limit ) {
    m_move_select_list[ m_move_select_size++ ] = l_goto_sub;
    l_found = true;
  }
  if ( m_move_list[ l_goto_add ] == a_limit ) {
    m_move_select_list[ m_move_select_size++ ] = l_goto_add;
    l_found = true;
  }
  if ( l_found ) {
    return true;
  }

  l_goto_sub = NormDec( l_goto_sub - 1, 8 );
  l_goto_add = NormInc( l_goto_add + 1, 8 );
  if ( m_move_list[ l_goto_sub ] == a_limit ) {
    m_move_select_list[ m_move_select_size++ ] = l_goto_sub;
    l_found = true;
  }
  if ( m_move_list[ l_goto_add ] == a_limit ) {
    m_move_select_list[ m_move_select_size++ ] = l_goto_add;
    l_found = true;
  }
  if ( l_found ) {
    return true;
  }

  // None of the 7 previous directions matched our current quality
  // limit. Consider backtracking if our previous quality history
  // seems reasonable.
  if ( m_move_list[ m_move_whence_we_came ] == a_limit
       && m_move_qual_memory[ a_limit ] >= cfg_par_movequal_histlimit.value() ) {
         m_move_select_list[ m_move_select_size++ ] = m_move_whence_we_came;
         l_found = true;
  }

  return l_found;
}



#ifdef CONF_INLINES
inline
#endif
int Partridge_Covey::MoveSelect( void ) {
  int l_rnd;
  if ( m_move_select_size > 1 )
    l_rnd = random( m_move_select_size ); else
    l_rnd = 0;

#ifdef PAR_DEBUG
  printf( "l_rnd   : %d\n", l_rnd );
#endif
  return m_move_select_list[ l_rnd ];
}



int Partridge_Covey::MoveWeighDirection( int a_x, int a_y ) {
  int l_dir = -1;
  bool loop=false;  // This is only needed to stop compiler warnings
  // The next section of code finds a direction with the best quality, medium then worst, then gives up if nothing
  do {  // This loop does not do anything except it prevents the MoveSelectLimit from being called unnecessarily
    if ( MoveSelectLimit( 3, a_x, a_y ) ) {
      l_dir = MoveSelect();
      break;
    }

    if ( MoveSelectLimit( 2, a_x, a_y ) ) {
      l_dir = MoveSelect();
      break;
    }

    if ( MoveSelectLimit( 1, a_x, a_y ) ) {
      l_dir = MoveSelect();
    }
  } while ( loop );

#ifdef PAR_DEBUG
  printf( "Select  : %d %d %d %d %d %d %d %d  :: %d\n", m_move_select_list[ 0 ], m_move_select_list[ 1 ],
       m_move_select_list[ 2 ], m_move_select_list[ 3 ], m_move_select_list[ 4 ], m_move_select_list[ 5 ],
       m_move_select_list[ 6 ], m_move_select_list[ 7 ], m_move_select_size );

#endif

  // If we couldn't find an useful direction so far, then
  // we just have to backtrack down the path from which we came.
  // Do try to sidestep just a little in a random direction though.
  if ( l_dir == -1 ) {
    if ( random( 100 ) < 50 ) {
      if ( m_move_list[ NormDec( m_move_whence_we_came - 1, 8 ) ] > 0 )
        l_dir = NormDec( m_move_whence_we_came - 1, 8 ); else if ( m_move_list[ NormInc( m_move_whence_we_came + 1, 8 ) ] > 0 )
        l_dir = NormInc( m_move_whence_we_came + 1, 8 ); else
        l_dir = m_move_whence_we_came;
    } else {
      if ( m_move_list[ NormInc( m_move_whence_we_came + 1, 8 ) ] > 0 )
        l_dir = NormInc( m_move_whence_we_came + 1, 8 ); else if ( m_move_list[ NormDec( m_move_whence_we_came - 1, 8 ) ] > 0 )
        l_dir = NormDec( m_move_whence_we_came - 1, 8 ); else
        l_dir = m_move_whence_we_came;
    }
  }
  // Move fuzzy if allowed to do so.
  int l_fuzzy = Norm( l_dir + MoveSelectFuzzy(), 8 );
  if ( m_move_list[ l_fuzzy ] > 0 )
    return l_fuzzy;

  return l_dir;
}



void Partridge_Covey::MoveDirectionsAllowed( int a_x, int a_y ) {
  // Finding the possible directions allowed by the location of the
  // peg is slightly complicated, because the actual covey position
  // may be on the other side of a map boundary.

  int l_dx = m_center_x - a_x;
  int l_dy = m_center_y - a_y;

  if ( l_dx < 0 ) {
    if ( -l_dx > ( m_width >> 1 ) ) {
      l_dx += m_width;
    }
  } else {
    if ( l_dx > ( m_width >> 1 ) ) {
      l_dx -= m_width;
    }
  }

  if ( l_dy < 0 ) {
    if ( -l_dy > ( m_height >> 1 ) ) {
      l_dy += m_height;
    }
  } else {
    if ( l_dy > ( m_height >> 1 ) ) {
      l_dy -= m_height;
    }
  }

#ifdef PAR_DEBUG
  printf( "dX, dY  : %d %d\n", l_dx, l_dy );
#endif

  // We now have l_dx and l_dy normalized wrt. to the map borders.
  // Finding the true distance and direction to the peg is now trivial.
  int l_dist_sq = l_dx * l_dx + l_dy * l_dy;

  if ( l_dx >= 0 ) {
    // The peg is in 1st or 2nd quadrant as seen from the covey.
    if ( l_dy < 0 ) {
      // 1st quadrant. Check octant!
      if ( -l_dy > l_dx ) {
        // 1st octant.
        m_move_allowed[ direction_n ] = true;
        m_move_allowed[ direction_ne ] = true;
        m_move_allowed[ direction_e ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_nw ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_se ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_s ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_sw ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_w ] = MoveTryExcludeHarder( l_dist_sq );
      } else {
        // 2nd octant.
        m_move_allowed[ direction_ne ] = true;
        m_move_allowed[ direction_e ] = true;
        m_move_allowed[ direction_n ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_se ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_s ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_sw ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_w ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_nw ] = MoveTryExcludeHarder( l_dist_sq );
      }
    } else {
      // 2nd quadrant clockwise from north.
      if ( l_dx > l_dy ) {
        // 3rd octant.
        m_move_allowed[ direction_e ] = true;
        m_move_allowed[ direction_se ] = true;
        m_move_allowed[ direction_ne ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_s ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_sw ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_w ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_nw ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_n ] = MoveTryExcludeHarder( l_dist_sq );
      } else {
        // 4th octant.
        m_move_allowed[ direction_se ] = true;
        m_move_allowed[ direction_s ] = true;
        m_move_allowed[ direction_e ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_sw ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_w ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_nw ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_n ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_ne ] = MoveTryExcludeHarder( l_dist_sq );
      }
    }
  } else {
    // l_dx < 0
    // The peg is in the 3rd or the 4th quadrant, clockwise from north.
    if ( l_dy >= 0 ) {
      // 3rd quadrant.
      if ( l_dy > -l_dx ) {
        // 5th octant.
        m_move_allowed[ direction_s ] = true;
        m_move_allowed[ direction_sw ] = true;
        m_move_allowed[ direction_se ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_w ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_n ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_ne ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_e ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_nw ] = MoveTryExcludeHarder( l_dist_sq );
      } else {
        // 6th octant.
        m_move_allowed[ direction_sw ] = true;
        m_move_allowed[ direction_w ] = true;
        m_move_allowed[ direction_s ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_nw ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_n ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_ne ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_e ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_se ] = MoveTryExcludeHarder( l_dist_sq );
      }
    } else {
      // l_dy < 0
      // 4th quadrant.
      if ( l_dx < l_dy ) {
        // 7th octant.
        m_move_allowed[ direction_w ] = true;
        m_move_allowed[ direction_nw ] = true;
        m_move_allowed[ direction_n ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_sw ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_ne ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_e ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_se ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_s ] = MoveTryExcludeHarder( l_dist_sq );
      } else {
        // 8th octant.
        m_move_allowed[ direction_nw ] = true;
        m_move_allowed[ direction_n ] = true;
        m_move_allowed[ direction_ne ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_w ] = MoveTryExclude( l_dist_sq );
        m_move_allowed[ direction_e ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_se ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_s ] = MoveTryExcludeHarder( l_dist_sq );
        m_move_allowed[ direction_sw ] = MoveTryExcludeHarder( l_dist_sq );
      }
    }
  }
}



void Partridge_Covey::MoveQualMemory( int a_qual ) {
  // Shift down the history array.
  for ( int i = 0; i < MOVE_QUAL_HIST_SIZE - 1; i++ ) {
    m_move_qual_hist[ 1 ] [ i ] = m_move_qual_hist[ 1 ] [ i + 1 ];
    m_move_qual_hist[ 2 ] [ i ] = m_move_qual_hist[ 2 ] [ i + 1 ];
    m_move_qual_hist[ 3 ] [ i ] = m_move_qual_hist[ 3 ] [ i + 1 ];
  }

  // Zero top of history.
  m_move_qual_hist[ 1 ] [ MOVE_QUAL_HIST_SIZE - 1 ] = 0;
  m_move_qual_hist[ 2 ] [ MOVE_QUAL_HIST_SIZE - 1 ] = 0;
  m_move_qual_hist[ 3 ] [ MOVE_QUAL_HIST_SIZE - 1 ] = 0;
  m_move_qual_memory[ 1 ] = 0;
  m_move_qual_memory[ 2 ] = 0;
  m_move_qual_memory[ 3 ] = 0;

  // Set top of qurrent qual
  m_move_qual_hist[ a_qual ] [ MOVE_QUAL_HIST_SIZE - 1 ] = 1;

  // Sum up the array.
  for ( int i = 0; i < MOVE_QUAL_HIST_SIZE; i++ ) {
    if ( m_move_qual_hist[ 1 ] [ i ] )
      m_move_qual_memory[ 1 ] ++;
    if ( m_move_qual_hist[ 2 ] [ i ] )
      m_move_qual_memory[ 2 ] ++;
    if ( m_move_qual_hist[ 3 ] [ i ] )
      m_move_qual_memory[ 3 ] ++;
  }
}



void Partridge_Covey::MoveTo( int a_max_distance, int a_step_size ) {

  m_dist_moved = 0.0;
  int l_x = m_covey_x;
  int l_y = m_covey_y;

  if ( a_step_size != m_move_step_size ) {
    m_move_step_size = a_step_size;
    m_move_step_size_inv = 1.0 / ( double )a_step_size;
  }

/*
	if ( m_move_get_enable ) {
		m_move_get_index = 0;
		m_move_get_size = 0;
	}
*/
  while ( m_dist_moved < ( double )a_max_distance ) {

    // Build list of possible directions, given the location
    // of the peg.
    MoveDirectionsAllowed( l_x, l_y );

#ifdef PAR_DEBUG
    printf( "Allowed : %d %d %d %d %d %d %d %d\n", m_move_allowed[ 0 ], m_move_allowed[ 1 ], m_move_allowed[ 2 ],
         m_move_allowed[ 3 ], m_move_allowed[ 4 ], m_move_allowed[ 5 ], m_move_allowed[ 6 ], m_move_allowed[ 7 ] );
#endif

    // Build list of movement evaluations in
    // m_move_list[] from our present location.
    if ( l_x - a_step_size >= 0 && l_x + a_step_size < m_width && l_y - a_step_size >= 0 && l_y + a_step_size < m_height ) {
      MoveOptimalDirectionFast( l_x, l_y );
    } else {
      MoveOptimalDirectionSlow( l_x, l_y );
    }

#ifdef PAR_DEBUG
    printf( "DirQual : %d %d %d %d %d %d %d %d\n", m_move_list[ 0 ], m_move_list[ 1 ], m_move_list[ 2 ], m_move_list[ 3 ],
         m_move_list[ 4 ], m_move_list[ 5 ], m_move_list[ 6 ], m_move_list[ 7 ] );
#endif

    // Take peg location and possibly past history/memory into
    // account.
    int l_dir = MoveWeighDirection( l_x, l_y );
    MoveQualMemory( m_move_list[ l_dir ] );

#ifdef PAR_DEBUG
    printf( "QualMemo: 1: %d 2: %d 3: %d\n", m_move_qual_memory[ 1 ], m_move_qual_memory[ 2 ], m_move_qual_memory[ 3 ] );
    printf( "Selected: %d\n", l_dir );
#endif
    m_move_whence_we_came = Norm( l_dir - 4, 8 );

    // We now have the direction that we want to move in l_dir.
    m_food_today += m_move_dir_qual[ l_dir ];

    // This call also updates l_x and l_y. Nasty, nasty. ;-)
    m_dist_moved += MoveDoIt( & l_x, & l_y, l_dir, a_step_size );
#ifdef PAR_DEBUG
    //printf("Dist Sum: %f\n", m_dist_moved );
    printf( "Coords  : %d %d\n\n", l_x, l_y );
#endif

    // Collect movement reports.
	/*
	if ( m_move_get_enable ) {
      m_move_get_size++;
      m_move_get_x.resize( m_move_get_size );
      m_move_get_y.resize( m_move_get_size );
      m_move_get_x[ m_move_get_index ] = l_x;
      m_move_get_y[ m_move_get_index ] = l_y;
      m_move_get_index++;
    }
	*/
}

  CoveyUpdateMemberPositions( l_x, l_y );
  m_move_get_index = 0;
  m_covey_x = l_x;
  m_covey_y = l_y;
  m_assess_done = false;
}

void Partridge_Covey::MoveDistance( int a_max_distance, int a_step_size, double a_food_density_needed ) {
	/**
	Move distance needs to take into accound the moves possible, the food available, the peg location, and the history of moves. \n
	\n
	This is where the partridge model spends much of its time (when there are chicks to feed).
	It is possible that these routines could be optimised, but they are also the most likely to be modified in the future,
	so too much optimisation may be a waste of time. \n
	*/
  if ( m_move_done )
    return;

  if ( !m_dist_done ) {
    m_dist_done = true;
    DistanceUpdate();
  }

  if ( m_nest_on_nest ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::MoveDistance(): ", "Asked to move while on nest." );
    exit( 1 );
  }

  m_dist_moved = 0.0;
  m_food_today = 0.0;
  int l_x = m_covey_x;
  int l_y = m_covey_y;

  if ( a_step_size != m_move_step_size ) {
    m_move_step_size = a_step_size;
    m_move_step_size_inv = 1.0 / ( double )a_step_size;
  }

/*
	if ( m_move_get_enable ) {
    m_move_get_index = 0;
    m_move_get_size = 0;
  }
*/
  while ( m_dist_moved < ( double )a_max_distance && m_food_today < a_food_density_needed ) {

    // Build list of possible directions, given the location
    // of the peg.
    MoveDirectionsAllowed( l_x, l_y );

#ifdef PAR_DEBUG
    printf( "Allowed : %d %d %d %d %d %d %d %d\n", m_move_allowed[ 0 ], m_move_allowed[ 1 ], m_move_allowed[ 2 ],
         m_move_allowed[ 3 ], m_move_allowed[ 4 ], m_move_allowed[ 5 ], m_move_allowed[ 6 ], m_move_allowed[ 7 ] );
#endif

    // Build list of movement evaluations in
    // m_move_list[] from our present location.
    if ( l_x - a_step_size >= 0 && l_x + a_step_size < m_width && l_y - a_step_size >= 0 && l_y + a_step_size < m_height ) {
      MoveOptimalDirectionFast( l_x, l_y );
    } else {
      MoveOptimalDirectionSlow( l_x, l_y );
    }

#ifdef PAR_DEBUG
    printf( "DirQual : %d %d %d %d %d %d %d %d\n", m_move_list[ 0 ], m_move_list[ 1 ], m_move_list[ 2 ], m_move_list[ 3 ],
         m_move_list[ 4 ], m_move_list[ 5 ], m_move_list[ 6 ], m_move_list[ 7 ] );
#endif

    // Take peg location and possibly past history/memory into
    // account.
    int l_dir = MoveWeighDirection( l_x, l_y );
    MoveQualMemory( m_move_list[ l_dir ] );

#ifdef PAR_DEBUG
    printf( "QualMemo: 1: %d 2: %d 3: %d\n", m_move_qual_memory[ 1 ], m_move_qual_memory[ 2 ], m_move_qual_memory[ 3 ] );
    printf( "Selected: %d\n", l_dir );
#endif
    m_move_whence_we_came = Norm( l_dir - 4, 8 );

    // We now have the direction that we want to move in l_dir.
    m_food_today += m_move_dir_qual[ l_dir ];

    // This call also updates l_x and l_y. Nasty, nasty. ;-)
    m_dist_moved += MoveDoIt( & l_x, & l_y, l_dir, a_step_size );
#ifdef PAR_DEBUG
    //printf("Dist Sum: %f\n", m_dist_moved );
    printf( "Coords  : %d %d\n\n", l_x, l_y );
#endif

    // Collect movement reports.
/*
if ( m_move_get_enable ) {
      m_move_get_size++;
      m_move_get_x.resize( m_move_get_size );
      m_move_get_y.resize( m_move_get_size );
      m_move_get_x[ m_move_get_index ] = l_x;
      m_move_get_y[ m_move_get_index ] = l_y;
      m_move_get_index++;
    }
*/
  }

  CoveyUpdateMemberPositions( l_x, l_y );
  m_move_get_index = 0;
  m_covey_x = l_x;
  m_covey_y = l_y;
  m_assess_done = false;
  m_move_done = true;
}

/*

#ifdef CONF_INLINES
inline
#endif
void Partridge_Covey::MoveGetEnable( bool a_state ) {
  m_move_get_enable = a_state;
}



int Partridge_Covey::MoveGetSteps( void ) {
  if ( !m_move_get_enable ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::MoveGetSteps(): Movement data collection", " not enabled!" );
    exit( 1 );
  }

  return m_move_get_size;
}



void Partridge_Covey::MoveGetPrepare( void ) {
  if ( !m_move_get_enable ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::MoveGetPrepare(): Movement data collection", " not enabled!" );
    exit( 1 );
  }
  m_move_get_index = 0;
}




bool Partridge_Covey::MoveGet( int * a_x, int * a_y ) {
  if ( !m_move_get_enable ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::MoveGet(): Movement data collection", " not enabled!" );
    exit( 1 );
  }

  if ( ( unsigned int ) m_move_get_index == m_move_get_x.size() ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::MoveGet(): Loop length exceeded!", "" );
    exit( 1 );
  }

  * a_x = m_move_get_x[ m_move_get_index ];
  * a_y = m_move_get_y[ m_move_get_index ];
  m_move_get_index++;

  if ( ( unsigned int ) m_move_get_index == m_move_get_x.size() ) {
    return false;
  }
  return true;
}

*/

double Partridge_Covey::MoveDoIt( int * a_x, int * a_y, int a_dir, int a_step_size ) {
  int l_diag_length = ( int )( ( double )a_step_size * 0.71 );
  double l_length = ( double )a_step_size;

  switch ( a_dir ) {
    case direction_n:
      * a_y -= a_step_size;
    break;

    case direction_ne:
      * a_y -= l_diag_length;
      * a_x += l_diag_length;
    break;
    case direction_e:
      * a_x += a_step_size;
    break;
    case direction_se:
      * a_y += l_diag_length;
      * a_x += l_diag_length;
    break;
    case direction_s:
      * a_y += a_step_size;
    break;
    case direction_sw:
      * a_y += l_diag_length;
      * a_x -= l_diag_length;
    break;
    case direction_w:
      * a_x -= a_step_size;
    break;
    case direction_nw:
      * a_y -= l_diag_length;
      * a_x -= l_diag_length;
    break;
  }

  * a_x = Norm( * a_x, m_width );
  * a_y = Norm( * a_y, m_height );
  return l_length;
}



#ifdef CONF_INLINES
inline
#endif
int Partridge_Covey::MoveCanMove( int a_x, int a_y ) {
  return MoveCanMove( m_map->SupplyElementType( a_x, a_y ) );
}



#ifdef CONF_INLINES
inline
#endif
int Partridge_Covey::Norm( int a_coord, int a_size ) {
/**
	A function to normalise co-ordinates that may have wrapped around and exceeded the landscape size (+/-)
*/
	while ( a_coord < 0 ) {
		a_coord += a_size;
	}
	while ( a_coord >= a_size ) {
		a_coord -= a_size;
	}
	return a_coord;
}

#ifdef CONF_INLINES
inline
#endif
int Partridge_Covey::NormDec( int a_coord, int a_size ) {
/**
	A function to normalise co-ordinates that may have wrapped around and become negative
*/
  if ( a_coord < 0 )
    return a_coord + a_size;

  return a_coord;
}

#ifdef CONF_INLINES
inline
#endif
int Partridge_Covey::NormInc( int a_coord, int a_size ) {
/**
	A function to normalise co-ordinates that may have wrapped around and exceeded the landscape maximum coord
*/
  if ( a_coord >= a_size )
    return a_coord - a_size;

  return a_coord;
}

double Partridge_Covey::SupplyFoodToday( void ) {
  if ( !m_move_done ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::SupplyFoodToday: Move not done yet!", "" );
    exit( 1 );
  }
  return m_food_today;
}


double Partridge_Covey::SupplyDistanceMoved( void ) {
  if ( !m_move_done ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::SupplyDistanceMoved: Move not done yet!", "" );
    exit( 1 );
  }
  return m_dist_moved;
}


#ifdef CONF_INLINES
inline
#endif
double Partridge_Covey::HabitatEvaluateFast( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl ) {
  int l_poly_cache = -1;
  double l_poly_value = 0.0;
  double l_qual = 0.0;

  for ( int y = a_min_y_incl; y < a_max_y_excl; y++ ) {
    for ( int x = a_min_x_incl; x < a_max_x_excl; x++ ) {
      // l_current_element_type
      TTypesOfLandscapeElement l_cet;
      int l_poly = m_map->SupplyPolyRef( x, y );
      if ( l_poly != l_poly_cache ) {
        // New polygon. Reevaluate the
        // value of the current coordinate.
        l_cet = m_map->SupplyElementType( x, y );
        l_poly_value = HabitatEvalPoly( l_cet, l_poly );
        l_poly_cache = l_poly;
      }
      l_qual += l_poly_value;
    }
  }
  return l_qual;
}

#ifdef CONF_INLINES
inline
#endif
double Partridge_Covey::HabitatEvaluateSlow( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl ) {
  int l_poly_cache = -1;
  double l_poly_value = 0.0;
  double l_qual = 0.0;

  ForIterator * l_fx = new ForIterator( a_min_x_incl, a_max_x_excl, 1, 0, m_width );
  int * l_lx = l_fx->GetList();

  ForIterator * l_fy = new ForIterator( a_min_y_incl, a_max_y_excl, 1, 0, m_height );
  int * l_ly = l_fy->GetList();

  for ( int iy = 0; iy < l_fy->GetLimit(); iy++ ) {
    int y = l_ly[ iy ];
    for ( int ix = 0; ix < l_fx->GetLimit(); ix++ ) {
      int x = l_lx[ ix ];

      // l_current_element_type
      TTypesOfLandscapeElement l_cet;
      int l_poly = m_map->SupplyPolyRef( x, y );
      if ( l_poly != l_poly_cache ) {
        // New polygon. Reevaluate the
        // value of the current coordinate.
        l_cet = m_map->SupplyElementType( x, y );
        l_poly_value = HabitatEvalPoly( l_cet, l_poly );
        l_poly_cache = l_poly;
      }
      l_qual += l_poly_value;
    }
  }
  delete l_fy;
  delete l_fx;
  return l_qual;
}


/**
Makes sure we have taken other covies into account then figures out which of the habitat evaluation functions to call depending how close to the edge of the world we are.\n
This way of doing things is used when we need to take other covies into account and cannot rely on the pre-assessed 10mgrid qualities of Partridge_Population_Manager::UpdateNestingCoverMap.\n
*/
double Partridge_Covey::HabitatEvaluate( int a_center_x, int a_center_y, int * a_size_used, bool a_rethink_size ) {
  if ( !m_dist_done && a_rethink_size ) {
    DistanceUpdate();
    m_dist_done = true;
  }
  // l_width_height_half
  int l_whh;
  if ( a_rethink_size ) {
    l_whh = ( ( int )m_nearest_covey_dist ) >> 1;
    if ( g_par_terr_max_width.value() < l_whh ) {
      l_whh = g_par_terr_max_width.value();
    } else if ( l_whh < 50 ) {
      l_whh = 50; // 100x100 = minimum possible size
    }
    * a_size_used = l_whh;
  } else {
    l_whh = * a_size_used;
  }

  int l_min_x_incl = a_center_x - l_whh;
  int l_max_x_excl = a_center_x + l_whh;
  int l_min_y_incl = a_center_y - l_whh;
  int l_max_y_excl = a_center_y + l_whh;

  if ( l_min_x_incl < 0 || l_max_x_excl > m_width || l_min_y_incl < 0 || l_max_y_excl > m_height ) {
    return HabitatEvaluateSlow( l_min_x_incl, l_max_x_excl, l_min_y_incl, l_max_y_excl );
  }
  return HabitatEvaluateFast( l_min_x_incl, l_max_x_excl, l_min_y_incl, l_max_y_excl );
}


  /**
  The search pattern for finding a territory is to find a place where the
  territory criteria are OK, then check that we are not too close to other coveys.
  This way of doing things is called when we want to do the job quickly using the pre-assessed qualmap array updated every week by Partridge_Population_Manager::UpdateNestingCoverMap.\n
  */
double Partridge_Covey::HabitatEvaluateFaster( int a_center_x, int a_center_y ) {
  //This simply returns the territory value at a_center_x,a_center_y with a
  //radius of 50m.\n
  a_center_x =  a_center_x  / 10;
  a_center_y =  a_center_y  / 10;
  int l_min_x_incl = a_center_x - 5;
  int l_max_x_excl = a_center_x + 5;
  int l_min_y_incl = a_center_y - 5;
  int l_max_y_excl = a_center_y + 5;

  double qual = 0;
  if ((l_min_x_incl<0) || (l_min_y_incl<0) || (l_max_x_excl >= (m_width/10)) || (l_max_y_excl >= (m_height /10)) ) return 0; // ignore near the edge - speed fudge
  for ( int i = l_min_x_incl; i < l_min_x_incl + 5; i++ )
  {
      for ( int j = l_min_y_incl; j < l_min_y_incl + 5; j++ )
	  {
        qual += m_manager->m_territoryqualmap->GetQualIndexed(i,j);
      }
  }
  return qual;
}


double Partridge_Covey::SupplyHabitatQuality( void ) {
  if ( !m_terr_done ) {
    m_terr_qual = HabitatEvaluate( m_center_x, m_center_y, & m_terr_size, false );
    m_terr_done = true;
  }
  return m_terr_qual;
}


double Partridge_Covey::AssessHabitatQuality( int & a_sz ) {
  if ( !m_assess_done ) {
    m_assess_qual = HabitatEvaluate( m_covey_x, m_covey_y, & m_assess_size, true );
    m_assess_done = true;
  }
  a_sz = m_assess_size;
  return m_assess_qual;
}

double Partridge_Covey::AssessHabitatQualityFaster() {
  m_assess_qual = HabitatEvaluateFaster( m_covey_x, m_covey_y );
  m_assess_done = true;
  return m_assess_qual;
}

void Partridge_Covey::FixHabitat( void ) {
  int sz = 0;
  if ( !m_assess_done ) {
    AssessHabitatQuality( sz );
  }
  m_center_x = m_covey_x;
  m_center_y = m_covey_y;
  m_center_x_float = ( double )m_covey_x;
  m_center_y_float = ( double )m_covey_y;
  m_terr_done = true;
  m_terr_size = m_assess_size;
  m_terr_qual = m_assess_qual;
  m_move_done = false;
}


/**
Sets all covey members to the location a_x, a_y and sets the covey x,y coords to this too.\n
*/
void Partridge_Covey::CoveyUpdateMemberPositions( int a_x, int a_y ) {
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    m_members[ i ]->SetX( a_x );
    m_members[ i ]->SetY( a_y );
  }
  m_Location_x = a_x;
  m_Location_y = a_y;
}



/**
Update covey position if we can find a suitable location and return true, or false if we failed to find a good spot.\n
*/
bool Partridge_Covey::NestFindLocation( void ) {
  // This algorithm can definitely be improved, time permitting!
  // FN, 16/6-2003.

  int l_whh = ( ( int )m_nearest_covey_dist ) >> 1;
  if ( g_par_terr_max_width.value() < l_whh ) {
    l_whh = g_par_terr_max_width.value();
  }

  int l_min_x_incl = m_center_x - l_whh;
  int l_max_x_excl = m_center_x + l_whh;
  int l_min_y_incl = m_center_y - l_whh;
  int l_max_y_excl = m_center_y + l_whh;

  if ( l_min_x_incl < 0 || l_max_x_excl > m_width || l_min_y_incl < 0 || l_max_y_excl > m_height ) {
    return NestFindSlow( l_min_x_incl, l_max_x_excl, l_min_y_incl, l_max_y_excl );
  }

  return NestFindFast( l_min_x_incl, l_max_x_excl, l_min_y_incl, l_max_y_excl );
}

#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::NestNearBadAreas( int a_x, int a_y ) {
/**
Determines if the x,y, positon is too close to something unacceptable
*/
	int l_min_x = a_x - g_par_nest_min_dist_bad_areas.value();
	int l_max_x = a_x + g_par_nest_min_dist_bad_areas.value() + 1;
	int l_min_y = a_y - g_par_nest_min_dist_bad_areas.value();
	int l_max_y = a_y + g_par_nest_min_dist_bad_areas.value() + 1;

	if ( l_min_x < 0 || l_max_x >= m_width || l_min_y < 0 || l_max_y >= m_height ) {
	return NestBadAreasScanSlow( l_min_x, l_min_y, l_max_x - l_min_x, a_x, a_y );
	}

	return NestBadAreasScanFast( l_min_x, l_min_y, l_max_x - l_min_x, a_x, a_y );
}

#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::NestBadAreasScanSlow( int a_min_x, int a_min_y, int a_length, int a_x, int a_y ) {
  int y_dec_base = NormInc( a_min_y + a_length, m_height );

  for ( int i = 0; i < a_length; i++ ) {
    int x_inc = Norm( a_min_x + i, m_width );
    int y_inc = Norm( a_min_y + i, m_height );
    int y_dec = Norm( y_dec_base - i, m_height );
    if ( NestBadArea( x_inc, y_inc ) || NestBadArea( x_inc, y_dec ) || NestBadArea( x_inc, a_y )
         || NestBadArea( a_x, y_inc ) ) {
           return true;
    }
  }

  return false;
}


#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::NestBadAreasScanFast( int a_min_x, int a_min_y, int a_length, int a_x, int a_y ) {
  for ( int i = 0; i < a_length; i++ ) {
    int x_inc = a_min_x + i;
    int y_inc = a_min_y + i;
    if ( NestBadArea( x_inc, y_inc ) || NestBadArea( x_inc, a_min_y + a_length - i ) || NestBadArea( x_inc, a_y )
         || NestBadArea( a_x, y_inc ) ) {
           return true;
    }
  }

  return false;
}



#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::NestNearNests( int a_x, int a_y, int a_min_dist_sq ) {
  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
    Partridge_Covey * l_covey = g_covey_list[ i ];
    // Skip coveys not on nest and ourselves.
    if ( !l_covey->NestOnNest() || l_covey->ID() == m_id ) {
      continue;
    }
    // Found covey on nest. See if this location is too close.
    int dx = l_covey->NestGetX() - a_x;
    int dy = l_covey->NestGetX() - a_y;

    if ( dx * dx + dy * dy < a_min_dist_sq ) {
      return true;
    }
  }
  return false;
}


bool Partridge_Covey::NestFindFast( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl ) {
  int l_poly_cache = -1;
  int l_min_dist_sq = g_par_nest_min_dist_other_nest.value() * g_par_nest_min_dist_other_nest.value();
  for ( int loops = 0; loops < 10; loops++ ) {
    // Allows us 10 goes at finding a site
    for ( int x = a_min_x_incl; x < a_max_x_excl; x++ ) {
      for ( int y = a_min_y_incl; y < a_max_y_excl; y++ ) {
        int l_poly = m_map->SupplyPolyRef( x, y );
        if ( l_poly == l_poly_cache )
          continue;

        l_poly_cache = l_poly;

        if ( NestGoodSpot( x, y ) && !NestNearBadAreas( x, y ) && !NestNearNests( x, y, l_min_dist_sq ) ) {
          CoveyUpdateMemberPositions( x, y );
          m_nest_on_nest = true;
          return true;
        }
      }
    }
    a_min_x_incl++;
    a_max_x_excl--;
    a_min_y_incl++;
    a_max_y_excl--;
  }
  return false;
}


bool Partridge_Covey::NestFindSlow( int a_min_x_incl, int a_max_x_excl, int a_min_y_incl, int a_max_y_excl ) {
  int l_poly_cache = -1;
  int l_min_dist_sq = g_par_nest_min_dist_other_nest.value() * g_par_nest_min_dist_other_nest.value();

  ForIterator * l_fx = new ForIterator( a_min_x_incl, a_max_x_excl, 1, 0, m_width );
  int * l_lx = l_fx->GetList();

  ForIterator * l_fy = new ForIterator( a_min_y_incl, a_max_y_excl, 1, 0, m_height );
  int * l_ly = l_fy->GetList();
  for ( int loop = 0; loop < 10; loop++ ) {
    for ( int iy = loop; iy < l_fy->GetLimit(); iy++ ) {
      int y = l_ly[ iy ];
      for ( int ix = loop; ix < l_fx->GetLimit(); ix++ ) {
        int x = l_lx[ ix ];

        int l_poly = m_map->SupplyPolyRef( x, y );
        if ( l_poly != l_poly_cache ) {
          l_poly_cache = l_poly;

          if ( NestGoodSpot( x, y ) && !NestNearBadAreas( x, y ) && !NestNearNests( x, y, l_min_dist_sq ) ) {
            CoveyUpdateMemberPositions( x, y );
            m_nest_on_nest = true;
            delete l_fy;
            delete l_fx;
            return true;
          }
        }
      }
    }
  }
  delete l_fy;
  delete l_fx;
  return false;
}

bool Partridge_Covey::TooClose() {
  // Returns true if the covey centre peg of any fixed covey is too close
  DistanceUpdate2();
  if ( m_nearest_covey_dist > 50 ) return false; // was 50 23/04/2008
  return true;
}

/**
Sorts through the list of nearby coveys and finds the distance to the nearest that is fixed. Stores the square root of this value. \n
*/
void Partridge_Covey::DistanceUpdate2( void ) {
  m_nearest_covey_dist = ( double )( m_width * m_height );

  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
    if ( m_id == g_covey_list[ i ]->ID() ) {
      continue;
    }
    if ( g_covey_list[ i ]->IsFixed() ) {
      double dist = DistanceToCovey( g_covey_list[ i ] );
      if ( dist < m_nearest_covey_dist ) {
        m_nearest_covey_dist = dist;
      }
    }
  }
  m_nearest_covey_dist = sqrt( m_nearest_covey_dist );
}

/**
Sorts through the list of nearby coveys and finds the distance to the nearest. Stores the square root of this value. \n
*/
void Partridge_Covey::DistanceUpdate( void ) {
  m_nearest_covey_dist = ( double )( m_width * m_height );

  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
    if ( m_id == g_covey_list[ i ]->ID() ) {
      continue;
    }
    double dist = DistanceToCovey( g_covey_list[ i ] );
    if ( dist < m_nearest_covey_dist ) {
      m_nearest_covey_dist = dist;
    }
  }
  m_nearest_covey_dist = sqrt( m_nearest_covey_dist );
}

#ifdef CONF_INLINES
inline
#endif
double Partridge_Covey::DistanceToCovey( Partridge_Covey * a_covey ) {
  double dx = m_center_x_float - a_covey->XCenterF();
  double dy = m_center_y_float - a_covey->YCenterF();
  // Divide by two becuase the maximum distance we can be away is half the world
  // in our wrap-around world
  double wf = ( double )( m_width >> 1 );
  double hf = ( double )( m_height >> 1 );
  // We need positive numbers
  if ( dx < 0 )
    dx = -dx;
  if ( dy < 0 )
    dy = -dy;
  // but we don't need numbers bigger than half the world
  if ( dx > wf )
    dx = m_width - dx;
  if ( dy > hf )
    dy = m_height - dy;
  // The next bit just makes sure that nothing goes out of whack by being too
  //small
  if ( dx < MIN_MATH_DIST_SQ ) {
    dx = MIN_MATH_DIST_SQ + ( double )( random( 100 ) ) / 100.0;
  }
  if ( dy < MIN_MATH_DIST_SQ ) {
    dy = MIN_MATH_DIST_SQ + ( double )( random( 100 ) ) / 100.0;
  }
  // Returns the squasre of the short sides or 0.0001, whichever is bigger
  return max( dx * dx + dy * dy, MIN_MATH_DIST_SQ );
}


bool Partridge_Covey::FlyToFast( int a_distance ) {
	// The distance we want to move as a double.
	double l_dist = ( double )a_distance;

  // Table Step size, as a double. Calculated so that
  // the difference in distance between points
  // around the perimeter of our scanning circle
  // are approximately 1 meter apart.
  // Eeewww... divisions... Avoid as far as possible.
  if ( a_distance != m_flyto_dist ) {
    m_flyto_dist = a_distance;
    m_flyto_steps = ( int )( floor( l_dist * TWO_PI ) );
    if ( cfg_par_flyto_stepsize.value() > 1 )
      m_flyto_steps /= cfg_par_flyto_stepsize.value();

    m_flyto_ts = ( double )cfg_par_flyto_stepsize.value() * ( double )TRIG_TABLE_LENGTH / ( l_dist * TWO_PI );
  }

  // Table Step Sum. Set to random start value, corresponding
  // to a random angle around the circle.
  double l_tss = ( double )random( TRIG_TABLE_LENGTH );

  bool l_found = false;
  int l_new_x = -1;
  int l_new_y = -1;
  int l_poly_cache = -1;
  int l_dx_cache = -20000;
  int l_dy_cache = -20000;

  for ( int i = 0; i < m_flyto_steps; i++ ) {
    if ( l_tss > ( double )( TRIG_TABLE_LENGTH - 1 ) ) {
      l_tss = 0.000001;
    }
    int index = ( int )floor( l_tss );
    l_tss += m_flyto_ts;

    int dx = ( int )floor( l_dist * g_trig_cos[ index ] + 0.5 );
    int dy = ( int )floor( l_dist * g_trig_sin[ index ] + 0.5 );

    if ( dx == l_dx_cache && dy == l_dy_cache )
      continue;
    l_dx_cache = dx;
    l_dy_cache = dy;
    int x = m_covey_x + dx;
    int y = m_covey_y - dy;

    int l_poly = m_map->SupplyPolyRef( x, y );
    if ( l_poly != l_poly_cache ) {
      l_poly_cache = l_poly;
      int l_geo = MoveCanMove( m_map->SupplyElementType( l_poly ) );
      if ( l_geo == 1 ) {
        // Excellent terrain found. Stop immediately.
        m_center_x = x;
        m_center_y = y;
        return true;
      } else if ( l_geo == 0 && l_new_x == -1 ) {
        // Remember first acceptable location if we cannot do better.
        l_new_x = x;
        l_new_y = y;
        l_found = true;
      }
    }
  }

  // If we get this far, then we only might have found something
  // acceptable place to move, though not good.
  if ( l_found ) {
    m_center_x = l_new_x;
    m_center_y = l_new_y;
  }
  return l_found;
}

bool Partridge_Covey::FlyToSlow( int a_distance ) {
  double l_dist = ( double )a_distance;

  if ( a_distance != m_flyto_dist ) {
    m_flyto_dist = a_distance;
    m_flyto_steps = ( int )( floor( l_dist * TWO_PI ) );
    if ( cfg_par_flyto_stepsize.value() > 1 )
      m_flyto_steps /= cfg_par_flyto_stepsize.value();

    m_flyto_ts = ( double )cfg_par_flyto_stepsize.value() * ( double )TRIG_TABLE_LENGTH / ( l_dist * TWO_PI );
  }

  double l_tss = ( double )random( TRIG_TABLE_LENGTH );

  bool l_found = false;
  int l_new_x = -1;
  int l_new_y = -1;
  int l_poly_cache = -1;
  int l_dx_cache = -20000;
  int l_dy_cache = -20000;

  for ( int i = 0; i < m_flyto_steps; i++ ) {
    if ( l_tss > ( double )( TRIG_TABLE_LENGTH - 1 ) ) {
      l_tss = 0.000001;
    }
    int index = ( int )floor( l_tss );
    l_tss += m_flyto_ts;

    int dx = ( int )floor( l_dist * g_trig_cos[ index ] + 0.5 );
    int dy = ( int )floor( l_dist * g_trig_sin[ index ] + 0.5 );

    if ( dx == l_dx_cache && dy == l_dy_cache )
      continue;
    l_dx_cache = dx;
    l_dy_cache = dy;
    int x = Norm( m_covey_x + dx, m_width );
    int y = Norm( m_covey_y - dy, m_height );

    int l_poly = m_map->SupplyPolyRef( x, y );
    if ( l_poly != l_poly_cache ) {
      l_poly_cache = l_poly;
      int l_geo = MoveCanMove( m_map->SupplyElementType( l_poly ) );
      if ( l_geo == 1 ) {
        m_center_x = x;
        m_center_y = y;
        return true;
      } else if ( l_geo == 0 && l_new_x == -1 ) {
        l_new_x = x;
        l_new_y = y;
        l_found = true;
      }
    }
  }

  if ( l_found ) {
    m_center_x = l_new_x;
    m_center_y = l_new_y;
  }
  return l_found;
}


bool Partridge_Covey::FlyTo( int a_distance ) {
  // New version, 4/8-2003.

  if ( a_distance <= 0 )
    return false;

  bool l_did_move = false;

  // The additional '2' is to ensure that rounding
  // in the calculations to follow will not cause
  // problems.
  if ( m_covey_x - a_distance - 2 >= 0 && m_covey_x + a_distance + 2 < m_width && m_covey_y - a_distance - 2 >= 0
       && m_covey_y + a_distance + 2 < m_height ) {
         l_did_move = FlyToFast( a_distance );
  } else {
    l_did_move = FlyToSlow( a_distance );
  }

  if ( l_did_move ) {
    m_covey_x = m_center_x;
    m_covey_y = m_center_y;
    m_terr_done = false;
    m_move_done = false;
    m_assess_done = false;
    m_dist_done = false;
    m_center_x_float = ( double )m_center_x;
    m_center_y_float = ( double )m_center_y;

    CoveyUpdateMemberPositions( m_center_x, m_center_y );
	return true;
  }
  else return false;
}

Partridge_Female * Partridge_Covey::FindMeAWife( Partridge_Male * a_male ) {
  Partridge_Female * l_female;
#ifdef __PAR_DEBUG2
  if ( a_female->GetMate() ) {
    int rubbish = 0;
  }
#endif
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( ( m_members[ i ]->GetFamily() != a_male->GetFamily() ) && ( m_members[ i ]->GetObjectType() == pob_Female ) ) {
      l_female = dynamic_cast < Partridge_Female * > ( m_members[ i ] );
      if ( !l_female->GetMate() ) return l_female;
    }
  }
  return NULL;
}

Partridge_Male * Partridge_Covey::FindMeAHusband( Partridge_Female * a_female ) {
  Partridge_Male * l_male;
#ifdef __PAR_DEBUG2
  if ( a_male->GetMate() ) {
    int rubbish = 0;
  }
#endif
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( (m_members[ i ]->GetFamily() != a_female->GetFamily()) && (m_members[ i ]->GetObjectType() == pob_Male )) {
      l_male = dynamic_cast < Partridge_Male * > ( m_members[ i ] );
      if ( !l_male->GetMate() ) return l_male;
    }
  }
  return NULL;
}

/*
Partridge_Base * Partridge_Covey::GetUncle() {
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetUncleStatus() ) {
      return m_members[ i ];
    }
  }
  return NULL;
}
*/

Partridge_Female * Partridge_Covey::FindMateInArea( int a_max_distance ) {
  int l_dsq = a_max_distance * a_max_distance;

  TAnimal * l_candidate;

  m_manager->ObjectLoopInit( pob_Female );

  l_candidate = m_manager->ObjectLoopFetch();
  while ( l_candidate ) {
    if ( dynamic_cast < Partridge_Base * > ( l_candidate )->GetState() == pars_FAttractingMate ) {
      int l_dx = m_covey_x - l_candidate->Supply_m_Location_x();
      int l_dy = m_covey_y - l_candidate->Supply_m_Location_y();
      if ( ( l_dx * l_dx + l_dy * l_dy ) < l_dsq ) {
        return dynamic_cast < Partridge_Female * > ( l_candidate );
      }
    }
    l_candidate = m_manager->ObjectLoopFetch();
  }
  return NULL;
}


void Partridge_Covey::BeginStep( void ) {
  if ( m_CurrentStateNo == -1 ) return;
  // Determine the movement distances and food requirements
  // Depends on the age of the chick
  //
  int age;
  // do we have any chicks?
  if ( GetOurChicks() > 0 ) {
    // if so then food requiremtns are dependent upon them
    age = m_ChickAge; // This set daily by the chicks
  }
  // otherwise then it is adult requirements
  age = cfg_par_mature_threshold.value();
  m_maxFoodNeedToday = g_FoodNeed[ age ];

  if ( m_state == pars_CoveyDissolve ) {
    if ( m_OurLandscape->SupplyDayInYear() >= m_CoveyDissolveDate ) {
      m_manager->DissolveCovey( this );
      m_state = pars_CoveyBeing;
      m_CoveyDissolveDate = -1;
    }
  }
}

void Partridge_Covey::Step( void ) {
  if ( m_StepDone ) {
    return;
  }
  m_StepDone = true;
}



void Partridge_Covey::EndStep( void ) {
  if ( m_CurrentStateNo == -1 ) return;
  m_step_done = false;
  m_move_done = false;
  m_assess_done = false;
  m_terr_done = false;
}

void Partridge_Covey::OnDissolve( int date ) {
  m_state = pars_CoveyDissolve;
  SetCoveyDissolveDate( date );
}


/* Partridge_Male * Partridge_Covey::GetMember() { if ( m_members[ 0 ]->GetObjectType() != pob_Male ) {
g_msg->Warn( WARN_BUG, "Partridge_Covey::GetMember(): ""Attempting to get a male when none are present!", "" ); exit( 1 ); }
return dynamic_cast < Partridge_Male * > ( m_members[ 0 ] ); } */

Partridge_Base * Partridge_Covey::GetAMember() {
  return m_members[ 0 ];
}

Partridge_Base * Partridge_Covey::GetMember( int a_member ) {
  return m_members[ a_member ];
}



void Partridge_Covey::AddMember( Partridge_Base * a_new_member ) {
  if ( m_CurrentStateNo == -1 ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::AddMember(): ""Attempting to add a member to non-existent flock!", "" );
    exit( 1 );
  }
  if ( IsMember( a_new_member ) ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::AddMember(): ""Attempting to add a member already belonging to the flock!", "" );
    exit( 1 );
  }
  if ( m_members_size>=99 ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::AddMember(): ""Covey too big!", "" );
    exit( 1 );
  }
  //m_members.resize( m_members.size() + 1 );
  m_members_size++;
  m_members[ m_members_size - 1 ] = a_new_member;
}



int Partridge_Covey::RemoveMember( Partridge_Base * a_former_member ) {
  if ( !IsMember( a_former_member ) ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::RemoveMember(): "
         "Attempting to remove a member not belonging to the flock!", "" );
    exit( 1 );
  }

  if ( a_former_member == m_theDad ) {
    OntheDadDead();
  } else if ( a_former_member == m_theMum ) {
    OntheMumDead();
  }

  if ( m_members_size == 1 ) {
    g_covey_manager->DelCovey( this );
    // Tell the population manager to delete us.
	m_members_size=0;
    m_CurrentStateNo = -1;
    m_StepDone = true;
    return 0;
  }

  //vector<Partridge_Base*> l_members;
  Partridge_Base * l_members[ 100 ];

  unsigned int j = 0;
  //l_members.resize( m_members.size() - 1 );

  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetID() == a_former_member->GetID() ) {
      continue;
    }
    l_members[ j++ ] = m_members[ i ];
  }

  //m_members.resize( m_members.size() -1 );
  m_members_size--;
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    m_members[ i ] = l_members[ i ];
  }

  return m_members_size;
}


bool Partridge_Covey::IsMember( Partridge_Base * a_possible_member ) {
  for ( unsigned int i = 0; i < m_members_size; i++ ) {

    //    if ( a_possible_member->GetID() == m_members[ i ]->GetID() ) {
    if ( a_possible_member == m_members[ i ] ) {
      return true;
    }
  }

  return false;
}

#ifdef CONF_INLINES
inline
#endif
double Partridge_Covey::Pressure( double a_distance ) {
	/**
		Returns the reciprocal of a_distance above a threshold given by MIN_MATH_DIST_SQ.
		Note that the calling function is responsible for ensuring a_distance is transformed correctly (sqrt()).\n
	*/
	return 1.0 / max( a_distance, MIN_MATH_DIST_SQ );
}



#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::PressureLimitExceeded( double a_pressure ) {
  if ( a_pressure > cfg_par_force_abs_limit.value() )
    return true;

  return false;
}


void Partridge_Covey::ManagerDriftPos( void ) {
  if ( !cfg_par_covey_drift_enable.value() || m_peg_is_fixed || m_permanent_marker )
    return;

  double dx = ( double )m_covey_x - m_center_x_float;
  double dy = ( double )m_covey_y - m_center_y_float;

  double l_x_sign = 1.0;
  double l_y_sign = 1.0;
  double wf = ( double )( m_width >> 1 );
  double hf = ( double )( m_height >> 1 );

  if ( dx < 0.0 ) {
    dx = -dx;
    if ( dx > wf ) {
      dx = ( double )m_width - dx;
    } else {
      l_x_sign = -1.0;
    }
  } else {
    // dx > 0
    if ( dx > wf ) {
      dx = ( double )m_width - dx;
      l_x_sign = -1.0;
    }
  }

  if ( dy < 0.0 ) {
    dy = -dy;
    if ( dy > hf ) {
      dy = ( double )m_height - dy;
    } else {
      l_y_sign = -1.0;
    }
  } else {
    if ( dy > hf ) {
      dy = ( double )m_height - dy;
      l_y_sign = -1.0;
    }
  }

  double dsum = sqrt( dx * dx + dy * dy );
  if ( dsum > 0.0 ) {
    m_new_center_x_float += l_x_sign * cfg_par_covey_drift_speed.value() * dx / ( dsum );
    m_new_center_y_float += l_y_sign * cfg_par_covey_drift_speed.value() * dy / ( dsum );
  }
}


void Partridge_Covey::ManagerCheckMerge( void ) {
	// This is called by the Tick function once a day between certain dates
	// for coveys that are not doing anything special and are only 1 or 2 birds
	// Test for merging
	if ( random( 100 ) >= cfg_par_merging_chance.value() ) return; // Only one chance per day
	unsigned int sz = unsigned(g_covey_list.size());
	for ( unsigned int i = 0; i < sz; i++ ) {
		Partridge_Covey* p_pc = g_covey_list[ i ];
		// Skip ourselves.
		if ( m_id == p_pc->ID() ) {
			continue;
		}
		if ( -1 == p_pc->m_CurrentStateNo ) {
			continue;
		}
		// Are we close to someone else?
		double dx = abs( m_center_x_float - p_pc->XCenterF() );
		double dy = abs( m_center_y_float - p_pc->YCenterF() );
		if ( ( dx < cfg_min_merge_dist.value() ) && ( dy < cfg_min_merge_dist.value() ) ) {
			// OK we feel like merging - will we be allowed to?
			// First find out if the target covey is full
			if ( p_pc->CanWeMerge( m_members_size ) ) {
				// Takes each member of a_covey and tells it to change covey
			    Partridge_Base * pb;
				//unsigned int nu = g_covey_list[ i ]->GetCoveySize();
				unsigned int nu = GetCoveySize(); // are we one or two or three?
				for ( unsigned int k = 0; k < nu; k++ ) {
					pb = GetAMember();
					RemoveMember( pb ); //when the last is removed this covey will vanish
					pb->SetUncleStatus( true );
					pb->SetCovey( p_pc );
					p_pc->AddMember( pb );
				 }
				 return; // Done
			} // Not allowed to merge if we get here
		} // end of in-distance test
	} // loop each covey (this must be optimised!)
}

bool Partridge_Covey::CanWeMerge( int a_noOfUncles ) {
	// The rules are that if we have no chicks then we can have up to 8 'uncles'
	// Otherwise only 4 are allowed.
	// The answer is no if we are still busy
	if ( !AllFlocking() ) return false;
	// Otherwise it depends on the number of uncles etc.
	int UncleNumber = GetUncleNumber();
	if ( m_ourChicks > 0 ) {
		if ( (UncleNumber + a_noOfUncles) > 4 ) return false;
	}
	else
		if ( m_map->SupplyDayInYear() < June ) {
			if ( (UncleNumber + a_noOfUncles) > 4 ) {
				return false;
			}
		} else if ( (UncleNumber + a_noOfUncles) > 8 ) return false;
	// This code is not perfect - it would allow up to 8 Uncles after all chicks have
	// matured. To stop this merging to >4 is only allowed between June and Dec 31st
	return true;
}


void Partridge_Covey::ManagerRethinkPos( void ) {
  m_force_low = false;
  m_new_center_x_float = m_center_x_float;
  m_new_center_y_float = m_center_y_float;

  if ( !cfg_par_force_enable.value() || m_peg_is_fixed || m_permanent_marker )
    return;

  double l_force_x = 0.0;
  double l_force_y = 0.0;
  double l_pressure_sum = 0.0;
  double wf = ( double )( m_width >> 1 );
  double hf = ( double )( m_height >> 1 );
  double l_x_sign = 1.0;
  double l_y_sign = 1.0;

  // Can anyone see this is a doubly nested for() loop?
  // Nah, nobody is looking... ;-)
  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {

    // Skip ourselves.
    if ( m_id == g_covey_list[ i ]->ID() ) {
      continue;
    }

    double dx = m_center_x_float - g_covey_list[ i ]->XCenterF();
    double dy = m_center_y_float - g_covey_list[ i ]->YCenterF();

    if ( dx < 0.0 ) {
      dx = -dx;
      if ( dx > wf ) {
        dx = ( double )m_width - dx;
        l_x_sign = 1.0;
      } else {
        l_x_sign = -1.0;
      }
    } else {
      // dx > 0
      if ( dx > wf ) {
        dx = ( double )m_width - dx;
        l_x_sign = -1.0;
      } else {
        l_x_sign = 1.0;
      }
    }

    if ( dy < 0.0 ) {
      dy = -dy;
      if ( dy > hf ) {
        dy = ( double )m_height - dy;
        l_y_sign = 1.0;
      } else {
        l_y_sign = -1.0;
      }
    } else {
      if ( dy > hf ) {
        dy = ( double )m_height - dy;
        l_y_sign = -1.0;
      } else {
        l_y_sign = 1.0;
      }
    }

    if ( dx < MIN_MATH_DIST_SQ ) {
      dx = MIN_MATH_DIST_SQ;
    }
    if ( dy < MIN_MATH_DIST_SQ ) {
      dy = MIN_MATH_DIST_SQ;
    }
    double dist = max( dx * dx + dy * dy, MIN_MATH_DIST_SQ );

    if ( dist < cfg_par_force_ignore_dist_sq.value() ) {
      l_pressure_sum += Pressure( sqrt( dist ) ); // Eeewww....
      l_force_x += l_x_sign * dx / dist; // 'Hurt me plenty.'
      l_force_y += l_y_sign * dy / dist;
    }
  } // End of covey loop

  double l_vector_force = sqrt( l_force_x * l_force_x + l_force_y * l_force_y );

  if ( ( l_vector_force > g_covey_manager->Getpar_force_ignore_below() ) && ( !m_peg_is_fixed ) && ( !m_permanent_marker ) ) {
    m_new_center_x_float = m_center_x_float + l_force_x * cfg_par_force_speed.value();
    m_new_center_y_float = m_center_y_float + l_force_y * cfg_par_force_speed.value();
  }

  if ( l_vector_force < cfg_par_force_below.value() )
    m_force_low = true;

  m_essence_low = PressureLimitExceeded( l_pressure_sum );
}



void Partridge_Covey::ManagerUpdatePos( void ) {
  if ( m_peg_is_fixed || m_permanent_marker )
    return;

  if ( m_new_center_x_float < 0.0 )
    m_new_center_x_float += ( double )m_width; else if ( m_new_center_x_float > ( double )m_width - 1.0 )
    m_new_center_x_float -= ( double )m_width;

  if ( m_new_center_y_float < 0.0 )
    m_new_center_y_float += ( double )m_height; else if ( m_new_center_y_float > ( double )m_height - 1.0 )
    m_new_center_y_float -= ( double )m_height;

  m_center_x_float = m_new_center_x_float;
  m_center_y_float = m_new_center_y_float;
  m_center_x = ( int )m_center_x_float;
  m_center_y = ( int )m_center_y_float;
  m_terr_done = false;
  m_assess_done = false;
  m_dist_done = false;
}



Partridge_Covey::~Partridge_Covey( void ) {
  g_covey_manager->DelCovey( this );
}



Partridge_Covey::Partridge_Covey( Partridge_Base * a_first_member, Partridge_Population_Manager * a_manager,
     unsigned int a_center_x, unsigned int a_center_y, Landscape * a_map ) : TAnimal( a_center_x, a_center_y, a_map ) {
       m_id = g_covey_id_counter++;
       m_map = a_map;
       m_width = a_map->SupplySimAreaWidth();
       m_height = a_map->SupplySimAreaHeight();

       m_maxAllowedMove = 1000; // default values
       m_maxFoodNeedToday = 10000; // default values

       m_manager = a_manager;
       m_center_x = a_center_x;
       m_center_y = a_center_y;
       m_center_x_float = ( double )m_center_x;
       m_center_y_float = ( double )m_center_y;
       m_covey_x = a_center_x;
       m_covey_y = a_center_y;

       m_nest_on_nest = false, m_nest_x = m_center_x; // Shouldn't matter.
       m_nest_y = m_center_y; //       "

       m_step_done = false;
       m_permanent_marker = false;
       m_move_done = false;
       m_dist_moved = 0.0; // Shouldn't matter.
       m_food_today = 0.0; //       "
       m_flyto_dist = -1;

       m_assess_done = false;
       //m_assess_qual  = 0.0;          // Shouldn't matter.
       m_assess_size = 0;

       m_terr_done = false;
       //m_terr_qual    = 0.0;          // Shouldn't matter.
       // Actually this matters if we start moving before
       // choosing a territory.
       m_terr_size = g_par_terr_max_width.value();

       m_dist_done = false;
       m_nearest_covey_dist = sqrt( ( double )m_width * m_height );
       m_peg_is_fixed = false;
       m_essence_low = false;
       m_force_low = true;

       //m_move_get_enable = false;

       /* m_members.resize( 1 ); */
       if ( a_first_member ) {
         m_members_size = 1;
         m_members[ 0 ] = a_first_member;
       } else {
         m_members_size = 0;
       }

       for ( int i = 0; i < MOVE_QUAL_HIST_SIZE; i++ ) {
         m_move_qual_hist[ 0 ] [ i ] = 0;
         m_move_qual_hist[ 1 ] [ i ] = 0;
         m_move_qual_hist[ 2 ] [ i ] = 0;
         m_move_qual_hist[ 3 ] [ i ] = 0;
       }

       m_move_qual_memory[ 0 ] = 0;
       m_move_qual_memory[ 1 ] = 0;
       m_move_qual_memory[ 2 ] = 0;
       m_move_qual_memory[ 3 ] = 0;

       MoveVegConstInit();
       CoveyUpdateMemberPositions( a_center_x, a_center_y );

       if ( g_covey_manager == NULL ) {
         g_covey_manager = new CoveyManager( m_map->SupplySimAreaWidth(), m_map->SupplySimAreaHeight(), m_map );
       }
       g_covey_manager->AddCovey( this );

       if ( a_manager )
         a_manager->AddObject( pob_Covey, this );

       m_state = pars_CoveyBeing;
       m_move_whence_we_came = random( 8 );
       m_move_step_size = 1;
       m_move_step_size_inv = 1.0;
       m_CoveyDissolveDate = -1; // never do it
       m_ourChicks = 0; // don't start with chicks or parents
       m_theDad = NULL;
       m_theMum = NULL;
}



// ****************
// * CoveyManager *
// ****************

class CoveyManager * g_covey_manager;

CoveyManager::CoveyManager( unsigned int a_world_width, unsigned int a_world_height, Landscape * a_map ) {
  m_world_width = a_world_width;
  m_world_height = a_world_height;
  m_world_width_div2 = m_world_width / 2;
  m_world_height_div2 = m_world_width / 2;
  m_map = a_map;

  g_trig_sin = new double[ TRIG_TABLE_LENGTH ];
  g_trig_cos = new double[ TRIG_TABLE_LENGTH ];

  Setpar_force_ignore_below( cfg_par_force_ignore_below.value() );


  if ( !g_trig_sin || !g_trig_cos ) {
    g_msg->Warn( WARN_MSG, "CoveyManager::CoveyManager(): Out of memory.", "" );
    exit( 1 );
  }

  double l_val = 0.0;
  double l_step = TWO_PI / TRIG_TABLE_LENGTH;
  for ( int i = 0; i < TRIG_TABLE_LENGTH; i++ ) {
    g_trig_sin[ i ] = sin( l_val );
    g_trig_cos[ i ] = cos( l_val );
    l_val += l_step;
  }
}

CoveyManager::~CoveyManager( void ) {
  delete g_trig_sin;
  delete g_trig_cos;
}

void CoveyManager::SanityCheck1( void ) {
  for ( PointerInt i = 0; i < (unsigned) g_covey_list.size(); i++ ) {
    if ( ( PointerInt )g_covey_list[ i ] < 1000 ) {
      g_msg->Warn( WARN_MSG, "CoveyManager::SanityCheck1(): NOT Sane", "" );
      exit( 0 );
    }
  }
}

void CoveyManager::Setpar_force_ignore_below( double ff ) {
  m_par_force_ignore_below = ff;
}

double CoveyManager::BroodGeoMean( void ) {
  double product = 0;
  int NoBr = 0;
  int sz = (int)g_covey_list.size();
  if ( sz < 2000 ) {
    for ( int i = 0; i < sz; i++ ) {
      double chs = ( double )g_covey_list[ i ]->GetOurChicks();
      // only count those with chicks
      if ( chs > 0 ) {
        product += log10( chs );
        NoBr++;
      }
    }
    if ( NoBr > 0 ) {
      double power = 1.0 / NoBr;
      product *= power; // Mulitply log by the power
      // Antilog it
      double result = pow( 10, product );
      return result;
    }
  }
  return 0.0;
}

/**
Moves all covey pegs as necessary and checks for merging
*/
void CoveyManager::Tick( void ) {
  // Copies m_center_*_float into m_new_center_*_float.
  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
    g_covey_list[ i ]->ManagerRethinkPos();
  }

  // Adds any deltas to m_new_center_*_float.
  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
    g_covey_list[ i ]->ManagerDriftPos();
  }

  // Copies the m_new_center_*_float position back
  // into m_center_*_float, updating covey positions.
  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
    g_covey_list[ i ]->ManagerUpdatePos();
  }

  // This code removed to stop merging 011004 CJT (why?)
  // Only test for merging between certain dates
  //
  if ( ( m_map->SupplyDayInYear() >= cfg_par_firstmerge.value() )
       || ( m_map->SupplyDayInYear() <= cfg_par_lastmerge.value() ) ) {
         for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
           if ( g_covey_list[ i ]->AllFlocking() ) {
             // Only singles and barren pairs are allowed to initiate merging
             if ( g_covey_list[ i ]->GetCoveySize() < 3 ) g_covey_list[ i ]->ManagerCheckMerge();
           }
         }
  }
//*/
}

//---------------------------------------------------------------------------

int CoveyManager::CoveyDensity( int x, int y, int radius ) {
  /** Search through the list of coveys and find out how many are within radius of x,y. */
  int tx, ty;
  int No = 0;
  int x1 = x - radius;
  int x2 = x + radius;
  int y1 = y - radius;
  int y2 = y + radius;
  // As usual this is complicated by the fact that x,y, might overlap boundries
  //
  if ( ( x - radius < 0 ) || ( x + radius >= m_world_width ) || ( y - radius < 0 ) || ( y + radius >= m_world_height ) ) {
    // Danger of running into the edge of the world
    // Also for speed and ease of calculation our radius is that of a square
    //
    for ( int i = 0; i < (int) g_covey_list.size(); i++ ) {
      tx = g_covey_list[ i ]->X();
      ty = g_covey_list[ i ]->Y();
      int dx = abs( x - tx );
      int dy = abs( y - ty );
      if ( dx > m_world_width_div2 ) dx = m_world_width - dx;
      if ( dy > m_world_height_div2 ) dy = m_world_height - dy;
      if ( ( tx < x2 ) && ( tx >= x1 ) && ( ty >= y1 ) && ( ty < y2 ) ) {
        // We have found one
        No++;
      }
    }
  } else {
    // Simple case, not near to the edge
    // Also for speed and ease of calculation our radius is that of a square
    //
    for ( int i = 0; i < (int) (g_covey_list.size()); i++ ) {
      tx = g_covey_list[ i ]->X();
      ty = g_covey_list[ i ]->Y();
      if ( ( tx <= x2 ) && ( tx >= x1 ) && ( ty >= y1 ) && ( ty <= y2 ) ) {
        // We have found one
        No++;
      }
    }
  }
  return No;
}

//---------------------------------------------------------------------------

void CoveyManager::AddCovey( Partridge_Covey * a_covey ) {
  unsigned int l_newindex = (int)g_covey_list.size();
  g_covey_list.resize( l_newindex + 1 );
  g_covey_list[ l_newindex ] = a_covey;
}

//---------------------------------------------------------------------------

bool CoveyManager::DelCovey( Partridge_Covey * a_covey ) {
  unsigned int l_id = a_covey->ID();

  if ( a_covey->ManagerIsPermanant() ) {
    // Raise hell. Someone thought a permanent landscape marker
    // belonged to them.
    assert( false );
  }

  for ( unsigned int i = 0; i < g_covey_list.size(); i++ ) {
    if ( l_id == g_covey_list[ i ]->ID() ) {
      // Throw away our local pointer to the covey by
      // moving the rest of the elements down in the list.
      for ( unsigned int j = i + 1; j < g_covey_list.size(); j++ ) {
        g_covey_list[ j - 1 ] = g_covey_list[ j ];
      }
      g_covey_list.resize( g_covey_list.size() - 1 );
      return true;
    }
  }

  // Didn't find requested ID.
  return false;
}
//---------------------------------------------------------------------------

ForIterator::ForIterator( int a_min_incl, int a_max_excl, int a_step_size, int a_norm_min_incl, int a_norm_max_excl ) {
	// a_min_incl or a_max_excl are goint to be <0 or > a_norm_max_excl
	if ( a_step_size > 1 )
	    m_limit = ( a_max_excl - a_min_incl ) / a_step_size;
	else
		m_limit = a_max_excl - a_min_incl;
	m_list = new int[ m_limit ];
	if ( !m_list ) assert( false );

	int i, index = 0;
	// this only use if a_min_incl < a_norm_min_incl (0)
	for ( i = a_min_incl; i < a_norm_min_incl && index < m_limit; i += a_step_size ) {
		m_list[ index++ ] = i + a_norm_max_excl;
	}
	// a_norm_max_excl is typically the landscape width
	// a_norm_min_incl is typically 0

	// This loops is for the normal co-ords
	for ( ; i < a_norm_max_excl && index < m_limit; i += a_step_size ) {
		m_list[ index++ ] = i;
	}
	// if we have exceeded the upper bound then this is used
	for ( ; i < a_max_excl && index < m_limit; i += a_step_size ) {
		m_list[ index++ ] = i - a_norm_max_excl;
	}
}
//---------------------------------------------------------------------------

ForIterator::~ForIterator( void ) {
  delete m_list;
}

//---------------------------------------------------------------------------

Partridge_Covey * Partridge_Covey::FindNeighbour() {
  // If there are no neighbours then do nothing
  if ( m_neighbourlist_size < 1 ) return NULL;
  int neighbour = random( m_neighbourlist_size );
  /* DEBUG
  if ( m_neighbourlist[ neighbour ]->m_CurrentStateNo == -1 ) {
    int rubbish = 0;
  }
  // END DEBUG */
  return m_neighbourlist[ neighbour ];
}

//---------------------------------------------------------------------------

Partridge_Female * Partridge_Covey::GetUnpairedFemale() {
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetObjectType() == pob_Female ) {
      Partridge_Female * pf = dynamic_cast < Partridge_Female * > ( m_members[ i ] );
      // Does she have an oldMate
      if ( pf->GetOldMate() ) {
        if ( IsMember( pf->GetOldMate() ) ) {
          return pf;
        } else {
          pf->RemoveOldMate(false);
        }
      }
    }
  }
  return NULL;
}

//---------------------------------------------------------------------------

Partridge_Male * Partridge_Covey::GetMaleInCovey() {
	for ( unsigned int i = 0; i < m_members_size; i++ ) {
		if ( m_members[ i ]->GetObjectType() == pob_Male ) {
			Partridge_Male * pm = dynamic_cast < Partridge_Male * > ( m_members[ i ] );
			return pm;
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

Partridge_Female * Partridge_Covey::FindMeAMateInCovey( int a_family_counter ) {
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetObjectType() == pob_Female ) {
      Partridge_Female * pf = dynamic_cast < Partridge_Female * > ( m_members[ i ] );
      // Does she have an oldMate
      if ( pf->GetMate() == NULL ) {
        if ( pf->GetFamily() != a_family_counter )
          return pf;
      }
    }
  }
  return NULL;
}

//---------------------------------------------------------------------------

Partridge_Female * Partridge_Covey::GetUnpairedFemale_virgin() {
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetObjectType() == pob_Female ) {
      Partridge_Female * pf = dynamic_cast < Partridge_Female * > ( m_members[ i ] );
      return pf;
    }
  }
  return NULL;
}

//---------------------------------------------------------------------------

bool Partridge_Covey::AllFlocking() {
  for ( unsigned i = 0; i < m_members_size; i++ ) {
    if ( ( m_members[ i ]->GetState() != pars_MFlocking ) && ( m_members[ i ]->GetState() != pars_FFlocking ) ) {
      return false;
    }
  }
  return true;
}

//---------------------------------------------------------------------------

bool Partridge_Covey::AllFlocking2() {
  for ( unsigned i = 0; i < m_members_size; i++ ) {
    if ( ( m_members[ i ]->GetState() != pars_MFlocking ) && ( m_members[ i ]->GetState() != pars_FFlocking ) ) {
      return false;
    }
  }
  return true;
}

//---------------------------------------------------------------------------

bool Partridge_Covey::ArePaired() {
  Partridge_Male * MP;
  Partridge_Female * FP;
  for ( unsigned i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetObjectType() == pob_Male ) {
      MP = dynamic_cast < Partridge_Male * > ( m_members[ i ] );
      if ( MP->GetMate() )
        return false;
    } else if ( m_members[ i ]->GetObjectType() == pob_Female ) {
      FP = dynamic_cast < Partridge_Female * > ( m_members[ i ] );
      if ( FP->GetMate() )
        return false;
    }
  }
  return true;
}

//---------------------------------------------------------------------------

void Partridge_Covey::SanityCheck() {
	if (m_CurrentStateNo==-1) return;
	for ( unsigned i = 0; i < m_members_size; i++ ) {
    // Checks that all members have pointers to here
    if ( m_members[ i ]->GetCovey() != this ) {
      g_msg->Warn( WARN_BUG, "Partridge_Covey::SanityCheck(): ""Non-mutual pointers", "" );
      exit( 1 );
    }
  }
}

//---------------------------------------------------------------------------

void Partridge_Covey::SanityCheck2( int no_chicks ) {
  // this one checks that the number of chicks in the covey
  // is the same as the number no_chicks
  int count = 0;
  for ( unsigned i = 0; i < m_members_size; i++ )
    if ( ( m_members[ i ]->GetObjectType() == pob_Chick ) || ( m_members[ i ]->GetObjectType() == pob_Chick2 ) ) count++;
  if ( count != no_chicks ) {
    g_msg->Warn( WARN_BUG, "Partridge_Covey::SanityCheck2(): ""wrong number of chicks", "" );
    exit( 1 );
  }
}

//---------------------------------------------------------------------------

void Partridge_Covey::SanityCheck3() {
	if ( m_move_list[ 0 ] > 7 ) {
      g_msg->Warn( WARN_BUG, "Partridge_Covey::SanityCheck3(): ""m_move_list illegal", "" );
      exit( 1 );
    }
}

//---------------------------------------------------------------------------

void Partridge_Covey::SanityCheck4() {
  for ( unsigned i = 0; i < m_members_size; i++ ) {
    // Checks that there are no clutches
    if ( m_members[ i ]->GetObjectType()==pob_Clutch ) {
      g_msg->Warn( WARN_BUG, "Partridge_Covey::SanityCheck(): ""Clutch present when it should not be!", "" );
      exit( 1 );
    }
  }
}

//---------------------------------------------------------------------------

void Partridge_Covey::OnAddChick( Partridge_Female * a_pf ) {
  if ( ++m_ourChicks == 1 ) {
    m_theMum = a_pf;
    m_theDad = m_theMum->GetMate();
  }
}

//------------------------------------------------------------------------------

void Partridge_Covey::OnChickMature( void ) {
  if ( --m_ourChicks == 0 ) {
    //Tell Both Parents
    // The line below is really debug information, this identifies the caller
    m_manager->m_comms_data->m_covey = this;
    //  Call the mother via the message centre
    m_manager->m_comms_data->m_female = m_theMum;
    m_manager->m_comms_data->m_male = m_theDad;
    m_manager->m_messagecentre.PassMessage( m_manager->m_comms_data, pcomm_ChicksMature );
    m_theDad = NULL;
    m_theMum = NULL;
    SetFixed( false );
  }
}

//------------------------------------------------------------------------------

void Partridge_Covey::OnChickDeath( void ) {
  if ( --m_ourChicks == 0 ) {
    //Tell Both Parents
    // The line below is really debug information, this identifies the caller
    m_manager->m_comms_data->m_covey = this;
    //  Call the mother via the message centre
    m_manager->m_comms_data->m_female = m_theMum;
    m_manager->m_comms_data->m_male = m_theDad;
    m_manager->m_messagecentre.PassMessage( m_manager->m_comms_data, pcomm_AllChicksDead );
    m_theDad = NULL;
    m_theMum = NULL;
    SetFixed( false );
  }
}

//------------------------------------------------------------------------------

void Partridge_Covey::OntheMumDead( void ) {
  //SanityCheck2(m_ourChicks );
  m_theMum = NULL;
  ActOnParentDeath();
}

//------------------------------------------------------------------------------

void Partridge_Covey::OntheDadDead( void ) {
  //SanityCheck2(m_ourChicks );
  m_theDad = NULL;
  ActOnParentDeath();
}

//------------------------------------------------------------------------------
/**
Determines the probability of mortality following the death of a parent. This will depend on the age of the chicks and whether it is one or both parents
that die and whether there are aunts or uncles present.\n
\image html ParChickMortTable.png
If we need to use aunts and uncles or if there is only one parent left then we assume only 8 chicks max can be brooded. The rest die, and the 8 are subject to a
mortality probability.
*/
void Partridge_Covey::ActOnParentDeath( void ) {
  int age = SupplyChickAge(); // depends on the fact that only one set of chicks
  if ( age >= cfg_par_parentdead_age2.value() ) return; // Old enough to cope
  if ( ( !m_theMum ) && ( !m_theDad ) ) {
    // Both parents dead - so need to look for uncles
    if ( GetUncleNumber()  > 0 ) { // Can use an uncle or aunt
		SetUncle();
    } else
		KillChicks( 100 );
		return;
  }
  // depends on the fact that only one set of chicks
  // can be in the covey at any one time.
  if ( age < cfg_par_parentdead_age1.value() ) {
    KillExcessChicks( 8 ); // removes any chicks in excess of 8
    KillChicks( cfg_par_parentdead_mort.value() );
  } else if ( age < cfg_par_parentdead_age2.value() )
    KillChicks( cfg_par_parentdead_mort.value() );
}

//------------------------------------------------------------------------------
/**
Called when both parents are dead but an uncle or aunt exist to take over the role.\n
*/
void Partridge_Covey::SetUncle( void ) {
  for ( unsigned i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetUncleStatus() ) {
      if ( m_members[ i ]->GetObjectType() == pob_Male ) {
        m_theDad = dynamic_cast<Partridge_Male*>( m_members[ i ]);
        m_theDad->SetState( pars_MCaringForYoung );
      } else {
        m_theMum = dynamic_cast<Partridge_Female*>( m_members[ i ]);
        m_theMum->SetState( pars_FCaringForYoung );
      }
	  m_members[ i ]->SetUncleStatus(false);
      return;
    }
  }
  // If we reach here there is an error
  g_msg->Warn( WARN_BUG, "Partridge_Covey::SetUncle(): ""Called but no uncles found", "" );
  exit( 1 );
}
//------------------------------------------------------------------------------

int Partridge_Covey::GetUncleNumber( void ) {
	int noUncs=0;
	for ( unsigned i = 0; i < m_members_size; i++ ) {
		if ( m_members[ i ]->GetUncleStatus() ) {
			noUncs++;
		}
	}
	return noUncs;
}
//------------------------------------------------------------------------------
/**
Kills chicks so that remaining remain.\n
*/
void Partridge_Covey::KillExcessChicks( int remaining ) {
  // Need to figure out if we need to kill any chicks
  if ( m_ourChicks < remaining ) return;
  Partridge_Chick * pc;
  int tokill = m_ourChicks - remaining;
  for ( int decimate = 0; decimate < tokill; decimate++ ) {
    for ( unsigned i = 0; i < m_members_size; i++ ) {
      if ( m_members[ i ]->GetObjectType() == pob_Chick ) {
        pc = dynamic_cast < Partridge_Chick * > ( m_members[ i ] );
        pc->OnYouAreDead();
        break;
      }
    }
  }
  //SanityCheck2(m_ourChicks );
}

//------------------------------------------------------------------------------

/**
Kills each chick if they fail a probability test.\n
*/
void Partridge_Covey::KillChicks( int percent ) {
  //SanityCheck2(m_ourChicks );
  for ( int i = m_members_size - 1; i >= 0; i-- ) {
    if ( m_members[ i ]->GetObjectType() == pob_Chick ) {
      if ( random( 100 ) < percent )
        dynamic_cast < Partridge_Chick * > ( m_members[ i ] )->OnYouAreDead();
    }
  }
  //SanityCheck2(m_ourChicks );
}

//------------------------------------------------------------------------------

int Partridge_Covey::SupplyChickAge( void ) {
  // Relies on the fact that a covey can only have one set of chicks at a time
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetObjectType() == pob_Chick ) {
      return m_members[ i ]->GetAge();
    }
  }
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetObjectType() == pob_Chick2 ) {
      return m_members[ i ]->GetAge();
    }
  }
  g_msg->Warn( WARN_BUG, "Partridge_Covey::SupplyChickAge(): ""no chicks", "" );
  exit( 1 );
}

//------------------------------------------------------------------------------

bool Partridge_Covey::HaveWeChicks( void ) {
  /**
  Relies on the fact that a covey can only have one set of chicks at a time.\n
  */
  for ( unsigned int i = 0; i < m_members_size; i++ ) {
    if ( m_members[ i ]->GetObjectType() == pob_Chick ) {
      return true;
    }
  }
  return false;
}
//------------------------------------------------------------------------------

/**
Emigration is a loss to the system, so this just kills everyone in the covey.\n
*/
bool Partridge_Covey::CoveyEmigrate( void ) {
	for ( unsigned i = m_members_size; i >0; i-- ) {
		m_members[i-1]->Dying();
	}
	return true;
}

//------------------------------------------------------------------------------


/**
Chance of each individual emigrating if they are not paired (emigration kills them just to remove them from the system - this is safe because if unpaired there will be no knock-on effects).\n
*/
void Partridge_Covey::CoveyIndividualEmigrate( void ) {
	for ( unsigned i = m_members_size; i >0; i-- ) {
		if (!m_members[i-1]->ArePaired()) {
			if (random(10000)<cfg_IndividualEmigration.value()) {
				m_members[i-1]->Dying();
			}
		}
	}
}

//------------------------------------------------------------------------------





void Partridge_Covey::ChickExtraMortality() {
  for ( int i = m_members_size - 1; i >= 0; i-- ) {
    if ( m_members[ i ]->GetObjectType() == pob_Chick ) {
      if (random(10000) <cfg_par_chick_extra_mort.value()) {
        dynamic_cast < Partridge_Chick * > ( m_members[ i ] )->OnYouAreDead();
    }
  }
}
}

#ifdef CONF_INLINES
inline
#endif
int Partridge_Covey::MoveEvalEdgesAndQual( int a_edges, double a_qual ) {
  /**
  This method must do an evaluation of this particular step given:\n
     * - a_edges: Number of 'edge' squares encountered if moving down
       this path. See Partridge_Covey::MoveCanMove() below.
     * - m_move_step_size: You guessed it. An int.
     * - m_move_step_size_inv: The inverse of m_move_step_size as a
       double. Both of these are updated automatically.
     * - a_qual: The sum of the values returned by MoveMagicVegToFood()
       above times the length of the segments with those values.
	 * .
   \n
   a_edges must be taken into account to ensure a preference
   of moving along field edges, hedges etc.\n

   Values returned should be within the range of [1:3], 3 being most preferable.\n
   */

#ifdef MOVE_EVAL_KLUDGE
  if ( a_edges > 2 )
    return 3;
  if ( a_edges > 0 )
    return 2;
  return 1;

#else
  int result = 2;
  double score = m_move_step_size_inv * a_qual;
  //  if (score<cfg_par_lowqualthreshold.value())
  //    result=1; //
  if ( score > cfg_par_highqualthreshold.value() )
    result = 3;
  // Result is 1 or 3 before edge evaluation
  // We have one special case, where food is 3 and edges are 2
  // correcting for this gives an even distributed result of 1,2,3
  if ( a_edges > 2 ) result += 3; else if ( a_edges > 0 ) {
    // The special case, where food is 3 and edges are 2
    if ( result == 3 ) result += 3; else result += 2;
  } else
    result += 1;
  // Result is now 2, 4 or 6
  result = result >> 1;
  return result;
#endif
}



#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::MoveTryExclude( int a_dist_sq ) {
  // Half Radius Squared
  int l_hrs = ( int )m_nearest_covey_dist >> 1;
  l_hrs = l_hrs * l_hrs;
  if ( a_dist_sq < l_hrs || ( random( 100 ) < 50 ) )
    return true;

  return false;
}



#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::MoveTryExcludeHarder( int a_dist_sq ) {
  // Half Radius Squared
  int l_hrs = ( int )m_nearest_covey_dist >> 1;
  l_hrs = l_hrs * l_hrs;
  if ( a_dist_sq < l_hrs )
    return true;

  if ( a_dist_sq < l_hrs * 1.6 && random( 100 ) < 10 )
    return true;

  return false;
}




#ifdef CONF_INLINES
inline
#endif
double Partridge_Covey::HabitatEvalPoly( TTypesOfLandscapeElement a_cet, int a_poly ) {
  int st;
  switch ( a_cet ) {

    // Terrible stuff
  case tole_IndividualTree:
  case tole_PlantNursery:
  case tole_WindTurbine:
  case tole_WoodyEnergyCrop:
  case tole_WoodlandMargin:
  case tole_Pylon:
  case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_Building:
	case tole_UrbanNoVeg:
	case tole_AmenityGrass:
      return -2.0;

      // Bad stuff.
	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Churchyard:
	case tole_Saltmarsh:
	case tole_Stream:
	case tole_HeritageSite:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_Garden:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_ActivePit:
	case tole_Pond:
	case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
    case tole_BareRock:
	case tole_Parkland:
	case tole_UrbanPark:
	case tole_BuiltUpWithParkland:
	case tole_SandDune:
	case tole_Copse:
		return -1.0;

      // Questionable.
    case tole_NaturalGrassWet:
    case tole_StoneWall:
	case tole_Fence:
    case tole_Hedges:
    case tole_Marsh:
    case tole_PitDisused:
    case tole_OrchardBand:
      return 0.0;

      // OK stuff
 	case tole_RoadsideSlope:
    case tole_PermPasture:
    case tole_PermPastureLowYield:
      return 2.0;
    // Good Stuff
    case tole_PermPastureTussocky:
    case tole_RoadsideVerge:
    case tole_Railway:
    case tole_FieldBoundary:
    case tole_PermanentSetaside:
    case tole_NaturalGrassDry:
    case tole_YoungForest: // ?
    case tole_Heath:
    case tole_Orchard:
    case tole_MownGrass:
    case tole_Scrub:
	case tole_Vildtager:
		return cfg_terr_qual_good.value();

      // Really good stuff!
    case tole_BeetleBank:
		return (double) cfg_nest_hedgebank1.value();
    case tole_HedgeBank:
	st = m_OurLandscape->SupplyElementSubType( a_cet );
    switch ( st ) {
      case 0:
        return (double) cfg_nest_hedgebank0.value();
      case 1:
         return (double) cfg_nest_hedgebank1.value();
     case 2:
        return (double) cfg_nest_hedgebank2.value();
      default:
        return 100;
    }
      // Variable, depending on vegetation and condition of same.
    case tole_Field:
	case tole_UnsprayedFieldMargin:
      return (double) HabitatEvalPolyField( a_poly );

    default:
    g_msg->Warn( WARN_BUG, "Partridge_Covey::TerrEvalPoly(): Unknown tole type", "" );
    exit( 1 );
  }
}



#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::NestGoodSpot( int a_x, int a_y ) {
/**
Evaluate the given x,y, coord in terms of nesting quality. These qualities are set as configuration variables.\n
*/
	TTypesOfLandscapeElement l_cet = m_map->SupplyElementType( a_x, a_y );
  int score, st;
  switch ( l_cet ) {
    case tole_Field:
      score = cfg_nest_field.value();
      // Need to add something about patchy and tramlines
      // TO SIMULATE 1950s SITUATION WITH ALL ACCESS GRANTED
#ifndef __P1950s
      if (m_map->SupplyVegPatchy( a_x, a_y )) score*=4; // doubled for patchy
      //else if (m_map->SupplyHasTramlines( a_x, a_y )) score*=2; // doubled for tramlines
#else
      score *=4;
#endif
    break;
    case tole_RoadsideVerge:
      score = cfg_nest_roadside.value();
    break;
    case tole_Railway:
      score = cfg_nest_railway.value();
    break;
    case tole_FieldBoundary:
      score = cfg_nest_fieldboundary.value();
    break;
    case tole_PermPastureLowYield:
      score = cfg_nest_permpastlowyield.value();
    break;
    case tole_PermPastureTussocky:
      score = cfg_nest_permpasttussocky.value();
    break;
    case tole_PermanentSetaside:
      score = cfg_nest_permsetaside.value();
    break;
    case tole_Heath:
    case tole_NaturalGrassDry:
      score = cfg_nest_naturalgrass.value();
    break;
	case tole_BeetleBank:
		score = cfg_nest_hedgebank1.value();
		break;
	case tole_HedgeBank:
      st = m_OurLandscape->SupplyElementSubType( a_x, a_y );
      switch ( st ) {
        case 0:
          score = cfg_nest_hedgebank0.value();
		  break;
        case 1:
          score = cfg_nest_hedgebank1.value();
          break;
        case 2:
          score = cfg_nest_hedgebank2.value();
          break;
     default:
          score = 100;
      }
    break;
    default:
      return false;
  }
  if ( score > random( 1000 ) ) return true;
  return false;
}



#ifdef CONF_INLINES
inline
#endif
bool Partridge_Covey::NestBadArea( int a_x, int a_y ) {
  TTypesOfLandscapeElement l_cet = m_map->SupplyElementType( a_x, a_y );

  switch ( l_cet ) {
    case tole_PermPasture:
    case tole_Scrub:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_StoneWall:
	case tole_Fence:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_ActivePit:
    case tole_Hedges:
    case tole_Marsh:
    case tole_PitDisused:
    case tole_RoadsideVerge:
    case tole_Railway:
    case tole_FieldBoundary:
	case tole_PermPastureLowYield:
	case tole_PermPastureTussocky:
    case tole_PermanentSetaside:
    case tole_NaturalGrassDry:
    case tole_Heath:
    case tole_Orchard:
    case tole_YoungForest:
    case tole_HedgeBank:
	case tole_BeetleBank:
	case tole_UnsprayedFieldMargin:
    case tole_Field:
    case tole_OrchardBand:
    case tole_MownGrass:
    case tole_NaturalGrassWet:
 	case tole_RoadsideSlope:
	case tole_Vildtager:
		return false;

	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Churchyard:
	case tole_Saltmarsh:
	case tole_Stream:
	case tole_HeritageSite:
    case tole_Garden:
    case tole_Building:
	case tole_Pond:
	case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
    case tole_BareRock:
	case tole_AmenityGrass:
	case tole_Parkland:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_BuiltUpWithParkland:
	case tole_SandDune:
	case tole_Copse:
	case tole_IndividualTree:
	case tole_PlantNursery:
	case tole_WindTurbine:
	case tole_WoodyEnergyCrop:
	case tole_WoodlandMargin:
	case tole_Pylon:
		return true;

    default:
    g_msg->Warn( WARN_BUG, "Partridge_Covey::NestBadArea(): Unknown tole type", "" );
    exit( 1 );
  }
}



#ifdef CONF_INLINES
inline
#endif
int Partridge_Covey::HabitatEvalPolyField( int a_field ) {

  TTypesOfVegetation l_tov = m_map->SupplyVegType( a_field );
  int score;
  switch ( l_tov ) {
    case tov_OFirstYearDanger:
    case tov_OGrazingPigs:
    case tov_OPermanentGrassGrazed:
    case tov_PermanentGrassGrazed:
    case tov_PermanentGrassLowYield:
      score = 0;
    break;

    case tov_Carrots:
    case tov_FieldPeas:
	case tov_FodderBeet:
	case tov_SugarBeet:
	case tov_OFodderBeet:
	case tov_Maize:
    case tov_MaizeSilage:
    case tov_OMaizeSilage:
    case tov_NoGrowth:
    case tov_None:
    case tov_OPotatoes:
    case tov_WinterRape:
    case tov_Potatoes:
    case tov_PotatoesIndustry:
      score = 1;
    break;

    case tov_PermanentGrassTussocky:
    case tov_PermanentSetaside:
	case tov_FodderGrass:
    case tov_CloverGrassGrazed1:
    case tov_CloverGrassGrazed2:
    case tov_Oats:
    case tov_OOats:
    case tov_OSeedGrass1:
    case tov_OSeedGrass2:
    case tov_OSpringBarley:
    case tov_OSpringBarleyExt:
    case tov_OSpringBarleyClover:
    case tov_OSpringBarleyGrass:
    case tov_OSpringBarleyPigs:
    case tov_OTriticale:
    case tov_OWinterBarley:
    case tov_OWinterBarleyExt:
    case tov_OWinterRape:
    case tov_OWinterRye:
	case tov_OWinterWheatUndersown:
	case tov_OWinterWheat:
	case tov_SeedGrass1:
    case tov_SeedGrass2:
	case tov_SpringBarley:
	case tov_SpringBarleySpr:
	case tov_SpringBarleyPTreatment:
	case tov_SpringBarleySKManagement:
    case tov_SpringBarleyCloverGrass:
    case tov_SpringBarleyGrass:
    case tov_SpringBarleySeed:
    case tov_SpringBarleySilage:
    case tov_SpringRape:
    case tov_SpringWheat:
    case tov_Triticale:
    case tov_WinterBarley:
    case tov_WinterRye:
    case tov_WinterWheat:
    case tov_WWheatPControl:
    case tov_WWheatPToxicControl:
    case tov_WWheatPTreatment:
    case tov_AgroChemIndustryCereal:
    case tov_WinterWheatShort:
    case tov_OCloverGrassGrazed1:
    case tov_OCloverGrassGrazed2:
    case tov_OBarleyPeaCloverGrass:
    case tov_OSBarleySilage:
    case tov_OCarrots:
    case tov_OCloverGrassSilage1:
    case tov_OFieldPeas:
    case tov_OFieldPeasSilage:
    case tov_YoungForest:
	case tov_Heath:
	case tov_BroadBeans:

		score = 2;
    break;

      /*  case tov_OGrassClover1: case tov_OGrassClover2: case tov_OBarleyPeaCloverGrass: case tov_OCarrots:
      case tov_OCloverGrassGrazed1: case tov_OCloverGrassGrazed2: case tov_OCloverGrassSilage1: case tov_OFieldPeas:
      case tov_OPermanentGrassGrazed: case tov_PermanentGrassGrazed: case tov_PermanentGrassTussocky: return 3.0; */

    case tov_Setaside:
    case tov_NaturalGrass:
    case tov_OSetaside:
      score = 4;
    break;

    case tov_Undefined:
    default:
      return 0;
  }
  return score;
}



  //  The next five lines were included in MoveMagicVegToFood when it was thought that the type of
  //  habitat might need to influence food collection
  //  TTypesOfVegetation tovt = m_map->SupplyVegType( a_polygon );
  //  double l_magic = m_move_veg_const[ tovt ] * m_map->SupplyInsects( a_polygon );
  //  TTypesOfLandscapeElement typ = m_map->SupplyElementType( a_polygon );
  //  if (typ==tole_HedgeBank) l_magic=l_magic*2.0;
#ifdef CONF_INLINES
inline
#endif
/**
This is the primary method responsible for generating food intake and local movement when chicks are present. The idea is to take each step optimally dependent on food availability,
with occaisional mis-steps, and if no one direction is optimal then pick randomly. If there is no choice we can back-track.
*/
double Partridge_Covey::MoveMagicVegToFood( int a_polygon ) {
	double l_magic = m_map->SupplyInsects( a_polygon ) * cfg_parinsectScaler.value();
#ifdef __P1950s
	// ***CJT***
	// Problem was that 1950s were calibrated with cfg_1950sinsects set to 1.0
	// we have therefore to scale the insects values we get for non-1950s to be lower somehow - preferably without making it run slow
	// Line removed
#else
	/** This section determine the effect of modern landscapes on food availability. Here the problem is that we asume fields are low in food
	due to a lower insect biomass. The effects of structure should be the same as in the 1950s. However, the question is whether we should treat
	all habitats as impoverished or just the fields, all fields or just arable fields. The default position is all fields and then allow a scaling
	factor for patchiness, and assume non-field habitats are not affected - this is not perfect an may need to be revisited in future versions */

	TTypesOfLandscapeElement Type = m_map->SupplyElementType(a_polygon);
	//if ( (!m_map->SupplyVegPatchy(a_polygon)) ||	(Type == tole_Field || Type == tole_PermPastureTussocky || Type == tole_PermPasture || Type == tole_PermanentSetaside || Type == tole_PermPastureLowYield))	{
	if ((!m_map->SupplyVegPatchy(a_polygon)) ) {
			if ((Type == tole_Field || Type == tole_PermPastureTussocky || Type == tole_PermPasture || Type == tole_PermanentSetaside || Type == tole_PermPastureLowYield))	{		l_magic*=(cfg_NOT1950sinsects.value());
			if (m_map->SupplyHasTramlines( a_polygon )) l_magic*=1.5; // extra for tramlines
		}
	} // otherwise we assume it is like the 1950s
#endif
	/* **CJT** 20/7/2010
	double height = m_map->SupplyVegHeight( a_polygon );
	int l_height_bin = ( int )( height * cfg_par_hei_hindrance_inv.value() );
	if ( l_height_bin > 3 ) l_height_bin = 3;
	*/
	// *** CJT *** 1 July 2010 Code below relies on biomass - but this is skylark based code and should be altered to
	// density. At the same time some alteration of the height response was required - hence change to HB_Impedence.txt
	/*
	double bio = m_map->SupplyVegBiomass( a_polygon );
	int l_biomass_bin = ( int )( bio * cfg_par_bio_hindrance_inv.value() );;
	if ( l_biomass_bin > 3 ) l_biomass_bin = 3;
	return l_magic * g_move_veg_structure[ l_height_bin ] [ l_biomass_bin ];

	/*/
	/*
	double dens = m_map->SupplyVegDensity( a_polygon );
	int l_density_bin = ( int )( dens * cfg_par_bio_hindrance_inv.value() );;
	if ( l_density_bin > 3 ) l_density_bin = 3;
	return l_magic * g_move_veg_structure[ l_height_bin ] [ l_density_bin ];
	*/
	// This section calculates the hindrance based on linear interpolation after the first height category
	// The slope (0.25) is scaled by cfg_par_hei_hindrance_inv and cfg_par_bio_hindrance_inv
	double height = m_map->SupplyVegHeight( a_polygon );
	double l_height_bin = ( height * cfg_par_hei_hindrance_inv.value() ) - 1.0;
	if (l_height_bin < 0.0 ) return l_magic; // So short there is no hindrance
	double dens = m_map->SupplyVegDensity( a_polygon );
	double l_density_bin = ( dens * cfg_par_bio_hindrance_inv.value() );
	// We need to restrict the upper end to avoid one factor outweighing the other e.g. if density is very high but height is only just >0.0
	if (l_density_bin > 4.0 ) l_density_bin = 4.0;
	if (l_height_bin > 4.0 ) l_height_bin = 4.0;
	double l_intermediate = 1.0 -(( l_density_bin + l_height_bin - 2.0 ) * 0.25);
	if (l_intermediate <0.0) return 0.0;
	if (l_intermediate >= 1.0) return l_magic;
	return l_magic * l_intermediate;

}



// Returns -1: Cannot move here.
//          0: OK to go here.
//          1: Exceptionally favorable terrain (for detecting
//             hedges etc.)
//
// Are areas like tole_LargeRoad really acceptable here? The coveys will
// move onto highway crossings etc. if they are. -- Frank.
//
#ifdef CONF_INLINES
inline
#endif
/**
Returns a code for whether the partridges can take chicks onto this polygon type at all (-1), if necessary (0) or by choice (1).\n
*/
int Partridge_Covey::MoveCanMove( TTypesOfLandscapeElement a_ele ) {
  switch ( a_ele ) {
    case tole_StoneWall:
	case tole_Fence:
    case tole_Garden:
    case tole_Building:
	case tole_Pond:
	case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
	case tole_Churchyard:
	case tole_Stream:
      return -1;

	case tole_IndividualTree:
	case tole_PlantNursery:
	case tole_WindTurbine:
	case tole_WoodyEnergyCrop:
	case tole_WoodlandMargin:
	case tole_Pylon:
	case tole_PermPasture:
    case tole_Scrub:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_Marsh:
    case tole_PitDisused:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_ActivePit:
    case tole_Hedges:
    case tole_Railway:
    case tole_Field:
	case tole_UnsprayedFieldMargin:
    case tole_PermPastureTussocky:
	case tole_PermPastureLowYield:
    case tole_PermanentSetaside:
    case tole_YoungForest:
    case tole_NaturalGrassDry:
    case tole_Heath:
    case tole_OrchardBand:
    case tole_MownGrass:
    case tole_Orchard:
    case tole_BareRock:
	case tole_AmenityGrass:
	case tole_Parkland:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_BuiltUpWithParkland:
	case tole_SandDune:
	case tole_Copse:
    case tole_NaturalGrassWet:
 	case tole_RoadsideSlope:
	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Saltmarsh:
	case tole_HeritageSite:
      return 0;

    case tole_RoadsideVerge:
    case tole_FieldBoundary:
    case tole_HedgeBank:
	case tole_BeetleBank:
	case tole_Vildtager:
		return 1;

    default:
    g_msg->Warn( WARN_BUG, "Partridge_Covey::MoveCanMove(): Unknown tole type", "" );
    exit( 1 );
    break;
  }
}


static bool m_move_veg_const_init_done = false;

/**
This was intended to be used to modify movement characteristics dependent on vegetation type, but lacking data all values are set to 1.0.\n
*/
void Partridge_Covey::MoveVegConstInit(void) {
	if (m_move_veg_const_init_done)
		return;
	m_move_veg_const_init_done = true;

	m_move_veg_const[tov_Carrots] = 1.0;
	m_move_veg_const[tov_FodderGrass] = 1.0;
	m_move_veg_const[tov_CloverGrassGrazed1] = 1.0;
	m_move_veg_const[tov_CloverGrassGrazed2] = 1.0;
	m_move_veg_const[ tov_BroadBeans ] = 1.0;
	m_move_veg_const[ tov_FieldPeas ] = 1.0;
	m_move_veg_const[ tov_FodderBeet ] = 1.0;
	m_move_veg_const[tov_SugarBeet] = 1.0;
	m_move_veg_const[tov_OFodderBeet] = 1.0;
	m_move_veg_const[tov_Maize] = 1.0;
	m_move_veg_const[tov_MaizeSilage] = 1.0;
	m_move_veg_const[tov_NaturalGrass] = 1.0;
	m_move_veg_const[tov_NoGrowth] = 1.0;
	m_move_veg_const[tov_None] = 1.0;
	m_move_veg_const[tov_Oats] = 1.0;

	// 10
	m_move_veg_const[tov_OBarleyPeaCloverGrass] = 1.0;
	m_move_veg_const[tov_OSBarleySilage] = 1.0;
	m_move_veg_const[tov_OCarrots] = 1.0;
	m_move_veg_const[tov_OCloverGrassGrazed1] = 1.0;
	m_move_veg_const[tov_OCloverGrassGrazed2] = 1.0;
	m_move_veg_const[tov_OCloverGrassSilage1] = 1.0;
	m_move_veg_const[tov_OFieldPeas] = 1.0;
	m_move_veg_const[tov_OFieldPeasSilage] = 1.0;
	m_move_veg_const[tov_OFirstYearDanger] = 1.0;
	m_move_veg_const[tov_OCloverGrassGrazed1] = 1.0;
	m_move_veg_const[tov_OCloverGrassGrazed2] = 1.0;
	m_move_veg_const[tov_OGrazingPigs] = 1.0;

	// 20
	m_move_veg_const[tov_OOats] = 1.0;
	m_move_veg_const[tov_OPermanentGrassGrazed] = 1.0;
	m_move_veg_const[tov_OPotatoes] = 1.0;
	m_move_veg_const[tov_OSeedGrass1] = 1.0;
	m_move_veg_const[tov_OSeedGrass2] = 1.0;
	m_move_veg_const[tov_OSetaside] = 1.0;
	m_move_veg_const[tov_OSpringBarley] = 1.0;
	m_move_veg_const[tov_OSpringBarleyClover] = 1.0;
	m_move_veg_const[tov_OSpringBarleyGrass] = 1.0;
	m_move_veg_const[tov_OSpringBarleyPigs] = 1.0;

	// 30
	m_move_veg_const[tov_OTriticale] = 1.0;
	m_move_veg_const[tov_OWinterBarley] = 1.0;
	m_move_veg_const[tov_OWinterRape] = 1.0;
	m_move_veg_const[tov_OWinterRye] = 1.0;
	m_move_veg_const[ tov_OWinterWheatUndersown ] = 1.0;
	m_move_veg_const[ tov_OWinterWheat ] = 1.0;
	m_move_veg_const[ tov_PermanentGrassGrazed ] = 1.0;
	m_move_veg_const[tov_PermanentGrassLowYield] = 1.0;
	m_move_veg_const[tov_PermanentGrassTussocky] = 1.0;
	m_move_veg_const[tov_PermanentSetaside] = 1.0;
	m_move_veg_const[tov_Potatoes] = 1.0;
	m_move_veg_const[tov_PotatoesIndustry] = 1.0;

	// 40
	m_move_veg_const[tov_SeedGrass1] = 1.0;
	m_move_veg_const[tov_SeedGrass2] = 1.0;
	m_move_veg_const[tov_Setaside] = 1.0;
	m_move_veg_const[tov_SpringBarley] = 1.0;
	m_move_veg_const[tov_SpringBarleySpr] = 1.0;
	m_move_veg_const[tov_SpringBarleyCloverGrass] = 1.0;
	m_move_veg_const[tov_SpringBarleyGrass] = 1.0;
	m_move_veg_const[tov_SpringBarleySeed] = 1.0;
	m_move_veg_const[tov_SpringBarleySilage] = 1.0;
	m_move_veg_const[tov_SpringRape] = 1.0;
	m_move_veg_const[tov_SpringWheat] = 1.0;

	// 50
	m_move_veg_const[tov_Triticale] = 1.0;
	m_move_veg_const[tov_WinterBarley] = 1.0;
	m_move_veg_const[tov_WinterRape] = 1.0;
	m_move_veg_const[tov_WinterRye] = 1.0;
	m_move_veg_const[tov_WinterWheat] = 1.0;
	m_move_veg_const[tov_WWheatPControl] = 1.0;
	m_move_veg_const[tov_WWheatPToxicControl] = 1.0;
	m_move_veg_const[tov_WWheatPTreatment] = 1.0;
	m_move_veg_const[tov_AgroChemIndustryCereal] = 1.0;
	m_move_veg_const[tov_WinterWheatShort] = 1.0;
	m_move_veg_const[tov_PermanentGrassGrazed] = 1.0;
	m_move_veg_const[tov_OSpringBarleyExt] = 1.0;
	m_move_veg_const[tov_SpringBarleyPTreatment] = 1.0;
	m_move_veg_const[tov_SpringBarleySKManagement] = 1.0;
	m_move_veg_const[tov_OMaizeSilage] = 1.0;
	m_move_veg_const[tov_Heath] = 1.0;

	// Must be here. tov_Undefined is returned by polygons
	// without growth.
	m_move_veg_const[tov_Undefined] = 1.0;
}




void Partridge_Covey::MoveOptimalDirectionSlow( int a_x, int a_y ) {
/**  Rule: Preferred direction is the one, which gives the best food return.\n
\n
Sum of distance times 'magic' vegetation2foodvaluepersitance() method.\n
Each tov type has a constant, which we can use to multiply the insect
biomass with (default is now that all are 1.0). We then have to modify this value with some function() of
vegetation structure. function() is a precalculated value from 0.0 to 1.0, given as a 4x4 array from vegetation height and biomass.\n
Also need to ensure the covey moves along the hedges etc.\n
All this is done whilst checking for wrap-around - therefore SLOW!\n
*/
  // The number of steps for a diagonal move is less than step size
  // by 1/sqrt(2), as each step is sqrt(2) meters long.
  int l_diag_length = ( int )( ( double )m_move_step_size * 0.71 );

  double l_quality = 0.0;
  double l_qual_cache = 0.0;
  int l_fantastic = 0;
  int l_poly_cache = -1;
  int l_geo_cache = 0;

  // Current pos to the north (decreasing y).

  // Use current spot in movement list as a temporary flag.
  m_move_list[ direction_n ] = m_move_allowed[ direction_n ];

  // Notice: y is allowed to grow negative, this is OK!
  // We normalize to the height of the map before using it.
  if ( m_move_allowed[ direction_n ] ) {
    for ( int y = a_y - 1; y >= a_y - m_move_step_size; y-- ) {
      int l_poly = m_map->SupplyPolyRef( a_x, NormDec( y, m_height ) );
      if ( l_poly != l_poly_cache ) {
        // Crossed into new polygon. Compute new values for
        // cache etc.
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          // This direction is blocked.
          m_move_list[ direction_n ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
    }
    // Sum up number of especially good edge squares.
    l_fantastic += l_geo_cache;
    // Sum up food quality.
    l_quality += l_qual_cache;
  }
  m_move_dir_qual[ direction_n ] = l_quality;
  if ( m_move_list[ direction_n ] ) {
    // This direction isn't blocked, so evaluate and assign
    // priority in movement list.
    m_move_list[ direction_n ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Northeast.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_ne ] = m_move_allowed[ direction_ne ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_ne ] ) {
    for ( int y = a_y - 1, x = a_x + 1; y >= a_y - l_diag_length; y--, x++ ) {
      int l_poly = m_map->SupplyPolyRef( NormInc( x, m_width ), NormDec( y, m_height ) );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_ne ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_ne ] = l_quality;
  if ( m_move_list[ direction_ne ] ) {
    m_move_list[ direction_ne ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // East.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_e ] = m_move_allowed[ direction_e ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_e ] ) {
    for ( int x = a_x + 1; x <= a_x + m_move_step_size; x++ ) {
      int l_poly = m_map->SupplyPolyRef( NormInc( x, m_width ), a_y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_e ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_e ] = l_quality;
  if ( m_move_list[ direction_e ] ) {
    m_move_list[ direction_e ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Southeast.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_se ] = m_move_allowed[ direction_se ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_se ] ) {
    for ( int x = a_x + 1, y = a_y + 1; x <= a_x + l_diag_length; x++, y++ ) {
      int l_poly = m_map->SupplyPolyRef( NormInc( x, m_width ), NormInc( y, m_height ) );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_se ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_se ] = l_quality;
  if ( m_move_list[ direction_se ] ) {
    m_move_list[ direction_se ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // South.
  l_quality = 0.0;
  l_fantastic = 0;
  l_qual_cache = 0.0;
  l_poly_cache = -1;
  m_move_list[ direction_s ] = m_move_allowed[ direction_s ];

  if ( m_move_allowed[ direction_s ] ) {
    for ( int y = a_y + 1; y <= a_y + m_move_step_size; y++ ) {
      int l_poly = m_map->SupplyPolyRef( a_x, NormInc( y, m_height ) );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_s ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_s ] = l_quality;
  if ( m_move_list[ direction_s ] ) {
    m_move_list[ direction_s ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Southwest.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_sw ] = m_move_allowed[ direction_sw ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_sw ] ) {
    for ( int x = a_x - 1, y = a_y + 1; x >= a_x - l_diag_length; x--, y++ ) {
      int l_poly = m_map->SupplyPolyRef( NormDec( x, m_width ), NormInc( y, m_height ) );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_sw ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_sw ] = l_quality;
  if ( m_move_list[ direction_sw ] ) {
    m_move_list[ direction_sw ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // West.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_w ] = m_move_allowed[ direction_w ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_w ] ) {
    for ( int x = a_x - 1; x >= a_x - m_move_step_size; x-- ) {
      int l_poly = m_map->SupplyPolyRef( NormDec( x, m_width ), a_y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_w ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_w ] = l_quality;
  if ( m_move_list[ direction_w ] ) {
    m_move_list[ direction_w ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Northwest.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_nw ] = m_move_allowed[ direction_nw ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_nw ] ) {
    for ( int y = a_y - 1, x = a_x - 1; y >= a_y - l_diag_length; y--, x-- ) {
      int l_poly = m_map->SupplyPolyRef( NormDec( x, m_width ), NormDec( y, m_height ) );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_nw ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_nw ] = l_quality;
  if ( m_move_list[ direction_nw ] ) {
    m_move_list[ direction_nw ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }
}



/**
Rule: Preferred direction is the one, which gives the best food return.\n
\n
Sum of distance times 'magic' vegetation2foodvaluepersitance() method.\n
Each tov type has a constant, which we can use to multiply the insect
biomass with (default is now that all are 1.0). We then have to modify this value with some function() of
vegetation structure. function() is a precalculated value from 0.0 to 1.0, given as a 4x4 array from vegetation height and biomass.\n
Also need to ensure the covey moves along the hedges etc.\n
*/
void Partridge_Covey::MoveOptimalDirectionFast( int a_x, int a_y ) {
  int l_diag_length = ( int )( ( double )m_move_step_size * 0.71 );

  int l_geo_cache = 0;
  double l_quality = 0.0;
  double l_qual_cache = 0.0;
  int l_fantastic = 0;
  int l_poly_cache = -1;

  // North.
  m_move_list[ direction_n ] = m_move_allowed[ direction_n ];

  if ( m_move_allowed[ direction_n ] ) {
    for ( int y = a_y - 1; y >= a_y - m_move_step_size; y-- ) {
      int l_poly = m_map->SupplyPolyRef( a_x, y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_n ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_n ] = l_quality;
  if ( m_move_list[ direction_n ] ) {
    m_move_list[ direction_n ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Northeast.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_ne ] = m_move_allowed[ direction_ne ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_ne ] ) {
    for ( int y = a_y - 1, x = a_x + 1; y >= a_y - l_diag_length; y--, x++ ) {
      int l_poly = m_map->SupplyPolyRef( x, y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_ne ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_ne ] = l_quality;
  if ( m_move_list[ direction_ne ] ) {
    m_move_list[ direction_ne ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // East.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_e ] = m_move_allowed[ direction_e ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_e ] ) {
    for ( int x = a_x + 1; x <= a_x + m_move_step_size; x++ ) {
      int l_poly = m_map->SupplyPolyRef( x, a_y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_e ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_e ] = l_quality;
  if ( m_move_list[ direction_e ] ) {
    m_move_list[ direction_e ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Southeast.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_se ] = m_move_allowed[ direction_se ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_se ] ) {
    for ( int x = a_x + 1, y = a_y + 1; x <= a_x + l_diag_length; x++, y++ ) {
      int l_poly = m_map->SupplyPolyRef( x, y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_se ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_se ] = l_quality;
  if ( m_move_list[ direction_se ] ) {
    m_move_list[ direction_se ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // South.
  l_quality = 0.0;
  l_fantastic = 0;
  l_qual_cache = 0.0;
  l_poly_cache = -1;
  m_move_list[ direction_s ] = m_move_allowed[ direction_s ];

  if ( m_move_allowed[ direction_s ] ) {
    for ( int y = a_y + 1; y <= a_y + m_move_step_size; y++ ) {
      int l_poly = m_map->SupplyPolyRef( a_x, y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_s ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_s ] = l_quality;
  if ( m_move_list[ direction_s ] ) {
    m_move_list[ direction_s ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Southwest.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_sw ] = m_move_allowed[ direction_sw ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_sw ] ) {
    for ( int x = a_x - 1, y = a_y + 1; x >= a_x - l_diag_length; x--, y++ ) {
      int l_poly = m_map->SupplyPolyRef( x, y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_sw ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_sw ] = l_quality;
  if ( m_move_list[ direction_sw ] ) {
    m_move_list[ direction_sw ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // West.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_w ] = m_move_allowed[ direction_w ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_w ] ) {
    for ( int x = a_x - 1; x >= a_x - m_move_step_size; x-- ) {
      int l_poly = m_map->SupplyPolyRef( x, a_y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_w ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly );
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_w ] = l_quality;
  if ( m_move_list[ direction_w ] ) {
    m_move_list[ direction_w ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }

  // Northwest.
  l_quality = 0.0;
  l_fantastic = 0;
  m_move_list[ direction_nw ] = m_move_allowed[ direction_nw ];
  l_qual_cache = 0.0;
  l_poly_cache = -1;

  if ( m_move_allowed[ direction_nw ] ) {
    for ( int y = a_y - 1, x = a_x - 1; y >= a_y - l_diag_length; y--, x-- ) {
      int l_poly = m_map->SupplyPolyRef( x, y );
      if ( l_poly != l_poly_cache ) {
        l_geo_cache = MoveCanMove( m_map->SupplyElementType( l_poly ) );
        if ( l_geo_cache == -1 ) {
          m_move_list[ direction_nw ] = 0;
          break;
        }
        l_poly_cache = l_poly;
        l_qual_cache = MoveMagicVegToFood( l_poly ) * 1.41;
      }
      l_fantastic += l_geo_cache;
      l_quality += l_qual_cache;
    }
  }
  m_move_dir_qual[ direction_nw ] = l_quality;
  if ( m_move_list[ direction_nw ] ) {
    m_move_list[ direction_nw ] = MoveEvalEdgesAndQual( l_fantastic, l_quality );
  }
}

