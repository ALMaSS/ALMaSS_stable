/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//


using namespace std;

#include "assert.h"
#include <cstdlib>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <cmath>
#include <vector>
#include <list>
#include <string>
#pragma warning( push )
#pragma warning( disable : 4100)
#pragma warning( disable : 4127)
#pragma warning( disable : 4244)
#pragma warning( disable : 4267)
#include <blitz/array.h>
#pragma warning( pop ) 
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Vole/GeneticMaterial.h"
#include "../Skylark/skylarks_all.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Vole/vole_all.h"
#include "../Vole/VolePopulationManager.h"
#include "../Vole/Predators.h"
#include "../Bembidion/bembidion_all.h"
#include "../Hare/hare_all.h"
#include "../Spider/spider_all.h"
#include "../Spider/SpiderPopulationManager.h"
#include "../MarshFritillary/MarshFritillaryHdrs.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../Hunters/Hunters_all.h"
#include "../GooseManagement/Goose_Population_Manager.h"
#include "../RoeDeer/Roe_all.h"
#include "../RoeDeer/Roe_pop_manager.h"
#include "../Rabbit/Rabbit.h"
#include "../Rabbit/Rabbit_Population_Manager.h"
#include "../Newt/Newt.h"
#include "../Newt/Newt_Population_Manager.h"
#include "../Osmia/Osmia.h"
#include "../Osmia/Osmia_Population_Manager.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

#define _CRT_SECURE_NO_DEPRECATE


const double g_randmaxp = RAND_MAX + 1.0;

static CfgInt cfg_DayInMonth( "PRB_DAYINMONTH", CFG_CUSTOM, 1 );
static CfgBool cfg_UseEasyPop( "VOLE_USEEASYPOP", CFG_CUSTOM, false );
static CfgBool cfg_VoleCatastrophe_on( "VOLE_CATASTROPHE_ON", CFG_CUSTOM, false );
static CfgInt cfg_VoleCatastrophe_interval( "VOLE_CATASTROPHE_I", CFG_CUSTOM, 365 * 5 );
static CfgInt cfg_VoleCatastrophe_mortality( "VOLE_CATASTROPHE_M", CFG_CUSTOM, 90 );
extern CfgBool cfg_dumpvegjan;
extern CfgStr cfg_dumpvegjanfile;
extern CfgBool cfg_dumpvegjune;
extern CfgStr cfg_dumpvegjunefile;
extern CfgBool cfg_fixed_random_sequence;

extern PopulationManagerList g_PopulationManagerList;

//---------------------------------------------------------------------------

void RunTheSim();
void CloseDownSim();
bool ReadBatchINI();
void GetProbeInput_ini();
void CreateLandscape();
bool CreatePopulationManager();
void SpeciesSpecificReporting();
void SpeciesSpecificActions();
bool BeginningOfMonth();
void DumpVegAreaData( int a_day );
void PredProbeReportDay0( int a_time );
void ProbeReport( int a_time );
void ImpactProbeReport( int a_time );

//-----------------------------------------------------------------------------

// Globals
    Landscape* g_ALandscape;
	Population_Manager *g_AManager;
	Hunter_Population_Manager *g_Hunter_Population_Manager;
    TPredator_Population_Manager *g_PredatorManager;
    int g_Species;
    int g_torun;
    char* g_files[100];
    char* g_Predfiles[100];
    char g_ResultsDir[255];
    char g_PredResultsDir[255];
    int g_time;
    int g_Year;
    int g_Steps;
    int g_NoProbes;
    unsigned g_NoOfPredProbes;
	PopulationManagerList g_PopulationManagerList;
	ALMaSS_MathFuncs g_AlmassMathFuncs;

	//-----------------------------------------------------------------------------------------

#ifndef __BORLANDC__
int random(int a_range) {
  /* Want to raise exception on this?
	 if ( a_range <= 0 )
	 return 0;
	*/
	//int result = (int)(((double) rand() / g_randmaxp ) * a_range);
	return (int) (g_rand_uni()*a_range);
	//return result;
}
#endif

//------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------

// int boost_random010000(int /* a_range */) {
    //unsigned result=*g_chance010000++;
	//return result;
// }
//------------------------------------------------------------------------------

void delay(int secs) {
	time_t start_time, cur_time;
	time(&start_time);
	do
	{
	   time(&cur_time);
	 } while((cur_time - start_time) < secs);
}
//------------------------------------------------------------------------------


#if (defined __UNIX) | ( defined __BORLANDC__)
void FloatToDouble(double &d, float f) {
	d = f;
}
#else
void FloatToDouble(double &d, float f) {
    char * num = 0;
    num = new char[_CVTBUFSIZE];
    errno_t err = _gcvt_s(num, _CVTBUFSIZE,f,8);
    if (err!=0) {
      assert(0);
    }
    d = atof(num);
	delete [] num;
}
#endif
//------------------------------------------------------------------------------

int main() {
#ifdef _DEBUG
	// Get the current state of the flag
	// and store it in a temporary variable
	int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );

	// Turn On (OR) - Keep freed memory blocks in the
	// heap's linked list and mark them as freed
	tmpFlag |= _CRTDBG_DELAY_FREE_MEM_DF;

	// Turn Off (AND) - prevent _CrtCheckMemory from
	// being called at every allocation request
	tmpFlag &= ~_CRTDBG_CHECK_ALWAYS_DF;
	// Set the new state for the flag
	_CrtSetDbgFlag( tmpFlag );
#endif
	// Must come first. Used by the configurator below.
	g_msg = new MapErrorMsg("ErrorFile.txt");
	g_msg->SetWarnLevel(WARN_ALL);

	// Configurator instantiation is automatic.
	g_cfg->ReadSymbols("TIALMaSSConfig.cfg");
	//g_cfg->DumpAllSymbolsAndExit( "allsymbols.cfg" );

	if (cfg_fixed_random_sequence.value()) srand( 0 ); else srand( (int)time( NULL ) );
	CreateLandscape();
	printf( "Landscape Created\n" ); // So now set up a Population Manager
	if (!ReadBatchINI()) {
		char ch;
		cout << "Problem with ini file";
		cin >> ch;
	}
	if (!CreatePopulationManager()) return false; else
		printf( "Population Created\n" ); // Now got to get the probe files read in
	g_ALandscape->SetThePopManager( g_AManager );
	GetProbeInput_ini();
	// Ready to go
	RunTheSim();
	CloseDownSim();
	return 0;
}
//------------------------------------------------------------------------------

void CreateLandscape() {
  // Create the landscape
  g_ALandscape = new Landscape();
}
//------------------------------------------------------------------------------

bool CreatePopulationManager() {
  // SET UP THE ANIMAL POPULATION
  // THE LANDSCAPE MUST BE SETUP BEFORE THE CALL HERE

    if ( g_Species == 0 ) {
		Skylark_Population_Manager * skMan = new Skylark_Population_Manager( g_ALandscape );
		g_AManager = skMan;
		g_PopulationManagerList.SetPopulation(g_AManager, TOP_Skylark);
		g_AManager->OpenTheBreedingPairsProbe();
		g_AManager->OpenTheBreedingSuccessProbe();
		g_AManager->OpenTheFledgelingProbe();
	}
    else if ( g_Species == 1 ) {
		Vole_Population_Manager * vMan = new Vole_Population_Manager( g_ALandscape );
		g_AManager = vMan;
	    g_PopulationManagerList.SetPopulation(g_AManager, TOP_Vole);
		g_PredatorManager = new TPredator_Population_Manager( g_ALandscape, vMan );
		g_PredatorManager->SetNoProbes( g_NoOfPredProbes);
		g_PopulationManagerList.SetPopulation(g_PredatorManager, TOP_Predators);
	}
  else if ( g_Species == 2 ) {
    Spider_Population_Manager * spMan = new Spider_Population_Manager( g_ALandscape );
	g_PopulationManagerList.SetPopulation(g_AManager, TOP_Spider);
    g_AManager = spMan;
  }
  else if ( g_Species == 3 ) {
    Bembidion_Population_Manager * bMan = new Bembidion_Population_Manager( g_ALandscape );
	g_PopulationManagerList.SetPopulation(g_AManager, TOP_Beetle);
    g_AManager = bMan;
  }
  else if ( g_Species == 4 ) { // Hare
    THare_Population_Manager * hMan = new THare_Population_Manager( g_ALandscape );
	g_PopulationManagerList.SetPopulation(g_AManager, TOP_Hare);
    g_AManager = hMan;
  }
  else if ( g_Species == 5 ) {
    Partridge_Population_Manager * pMan = new Partridge_Population_Manager( g_ALandscape );
    g_AManager = pMan;
	g_PopulationManagerList.SetPopulation(g_AManager, TOP_Partridge);
  }
  else if ( g_Species == 6 ) {
    Goose_Population_Manager * gMan = new Goose_Population_Manager( g_ALandscape );
    g_AManager = gMan;
	g_PopulationManagerList.SetPopulation(g_AManager, TOP_Goose);
    g_Hunter_Population_Manager = new Hunter_Population_Manager( g_ALandscape );
	g_PopulationManagerList.SetPopulation(g_Hunter_Population_Manager, TOP_Hunters);
  }
  else if ( g_Species == 7 ) {
    MarshFritillary_Population_Manager * mMan = new  MarshFritillary_Population_Manager( g_ALandscape );
    g_AManager = mMan;
	g_PopulationManagerList.SetPopulation(g_AManager, TOP_MarshFritillary);
  }
#ifdef __dormouse  //LADA
  else if (g_Species == 8) {
	  Dormouse_Population_Manager * dMan = new Dormouse_Population_Manager(g_ALandscape);
	  g_AManager = dMan;
	  g_PopulationManagerList.SetPopulation(g_AManager, TOP_Dormouse);
  }
#endif // __dormouse
  else if (g_Species == 9)
  {
	  RoeDeer_Population_Manager * rMan = new RoeDeer_Population_Manager( g_ALandscape );
	  g_AManager = rMan;
	  g_PopulationManagerList.SetPopulation( g_AManager, TOP_RoeDeer );
  }
  else if (g_Species == 10)
  {
	  Rabbit_Population_Manager * rMan = new Rabbit_Population_Manager(g_ALandscape);
	  g_AManager = rMan;
	  g_PopulationManagerList.SetPopulation(g_AManager, TOP_Rabbit);
  }
  else if (g_Species == 11)
  {
	  Newt_Population_Manager * nwMan = new Newt_Population_Manager(g_ALandscape);
	  g_AManager = nwMan;
	  g_PopulationManagerList.SetPopulation(g_AManager, TOP_Newt);
  }
  else if (g_Species == 12)
  {
	  Osmia_Population_Manager * osMan = new Osmia_Population_Manager(g_ALandscape);
	  g_AManager = osMan;
	  g_PopulationManagerList.SetPopulation(g_AManager, TOP_Osmia);
  }
  else {
	  g_msg->Warn("Population_Manager::CreatePopulationManager()  Unknown species number> ", g_Species);
	  exit(0);
  }

  g_AManager->SetNoProbes(g_NoProbes);
  return true;
}

//------------------------------------------------------------------------------

void GetProbeInput_ini() {
  char Nme[ 511 ];
 ofstream * AFile=NULL;
  for ( int NProbes = 0; NProbes < g_NoProbes; NProbes++ ) {
    // Must read the probe from a file
    g_AManager->TheProbe[ NProbes ] = new probe_data;
    g_AManager->ProbeFileInput( ( char * ) g_files[ NProbes ], NProbes );
    char NoProbesString[ 255 ];
    sprintf( NoProbesString, "%sProbe.res", g_ResultsDir );
    if ( NProbes == 0 ) {
      AFile = g_AManager->TheProbe[ NProbes ]->OpenFile( NoProbesString );
      if ( !AFile ) {
        g_ALandscape->Warn( "BatchALMSS - cannot open Probe File", NULL );
        exit( 1 );
      }
	} else
      g_AManager->TheProbe[ NProbes ]->SetFile( AFile );
  }
  if ( g_Species == 1 ) {
    for ( int NProbes = 0; NProbes < ( int )g_NoOfPredProbes; NProbes++ ) {
      g_PredatorManager->TheProbe[ NProbes ] = new probe_data;
      g_PredatorManager->ProbeFileInput( ( char * ) g_Predfiles[ NProbes ], NProbes );
      sprintf( Nme, "%sPredProbe%d.res", g_ResultsDir, NProbes + 1 );
      //      strcpy( Nme, g_PredResultsDir );
      if ( NProbes == 0 ) {
        AFile = g_PredatorManager->TheProbe[ NProbes ]->OpenFile( Nme );
        if ( !AFile ) {
          g_ALandscape->Warn( "BatchALMSS - cannot open Probe File", Nme );
          exit( 1 );
        }
      } else
        g_PredatorManager->TheProbe[ NProbes ]->SetFile( AFile );
    }
  }
  for ( int NProbes = 0; NProbes < g_NoProbes; NProbes++ ) {
	  delete [] g_files[NProbes];
  }
  for ( int NProbes = 0; NProbes < ( int )g_NoOfPredProbes; NProbes++ ) {
	  delete [] g_Predfiles[NProbes];
  }
}

//-----------------------------------------------------------------------------
bool ReadBatchINI() {
  // Must read the TIBatch.INI
  // Read the INI file
  FILE * Fi = NULL;
  char answer = 'X';
  Fi=fopen("BatchALMaSS.ini", "r" );
  while ( Fi==NULL ) {
      // Issue and error warning
      cout << "INI File Missing: ";
      cout << "BatchALMaSS.ini" << "\n";
	  cout << "Try Again?";
      cin >> answer;
    if ( answer != 'Y' ) exit(0);
	Fi = fopen("BatchALMaSS.ini", "r" );
  }
  char Data[ 255 ];
  fscanf( Fi, "%d\r", & g_NoProbes );
  for ( int i = 0; i < g_NoProbes; i++ ) {
    fscanf( Fi, "%s\r", Data);
    g_files[ i ] = new char[ 255 ];
    strcpy( g_files[ i ], Data );
  }
  fscanf( Fi, "%s\r", Data );
  strcpy( g_ResultsDir, Data );
  // Read the PredBatch.INI file
  if ( g_Species == 1) {
	  FILE * Fi2 = NULL;
	  answer = 'X';
	  while ( !Fi2 ) {
		Fi2=fopen("VoleToxPreds.ini", "r" );
		if ( !Fi2 ) {
		  // Issue and error warning
		  cout << "Predator Batch File Missing: VoleToxPreds.INI";
		  cout << "Try Again  (Y/N) ? ";
		  cin >> answer;
		  if ( answer != 'Y' ) return false;
		}
	  }
	  char Data2[ 255 ];
	  fscanf( Fi2, "%d\r", & g_NoOfPredProbes );
	  for ( int i = 0; i < ( int )g_NoOfPredProbes; i++ ) {
		fscanf( Fi2, "%s\r", Data2 );
		g_Predfiles[ i ] = new char[ 255 ];
		strcpy( g_Predfiles[ i ], Data2 );
	  }
	  fscanf( Fi2, "%s\r", Data2 );
	  strcpy( g_PredResultsDir, Data2 );
	  fclose( Fi2 );
  }
  fscanf( Fi, "%d\r", & g_torun );
  g_torun *= 365; // the number of years and multiplied by 365 to get days
  fscanf( Fi, "%d\r", & g_Species );
  fclose( Fi );
  switch (g_Species) {
	  case 0:
		  printf("Running Skylarks\n");
		  break;
	  case 1:
		  printf("Running Voles\n");
		  break;
	  case 2:
		  printf("Running Spiders\n");
		  break;
	  case 3:
		  printf("Running Beetles\n");
		  break;
	  case 4:
		  printf("Running Hares\n");
		  break;
	  case 5:
		  printf("Running Partridges\n");
		  break;
	  case 6:
		  printf("Running Goose Management\n");
		  break;
	  case 7:
		  printf("Running Marsh Fritillary\n");
		  break;
	  case 8:
		  printf("Running Dormouse\n");
		  break;
	  case 9:
		  printf("Running Roe Deer\n");
		  break;
	  case 10:
		  printf("Running Rabbits\n");
		  break;
	  case 11:
		  printf("Running Newts\n");
		  break;
  }
  return true;
}

//-----------------------------------------------------------------------------
void CloseDownSim() {
    if ( g_AManager ) 
	{
  	  g_ALandscape->SimulationClosingActions();
      // Close the probe file
      if ( g_AManager->TheProbe[ 0 ] != NULL ) g_AManager->TheProbe[ 0 ]->CloseFile();
      // delete all probes
      for ( int i = 0; i < g_NoProbes; i++ ) delete g_AManager->TheProbe[ i ];
      delete g_AManager;
      if ( g_Species == 1 ) {
        delete g_PredatorManager;
      }
    }
	delete g_ALandscape;
    delete g_cfg;
}

//------------------------------------------------------------------------------

void RunTheSim() {
  for ( int i = 0; i < g_torun; i++ ) {
    g_ALandscape->TurnTheWorld();
    // Update the Date
    g_time++;

	printf("%d\r",g_time);
    int day = g_ALandscape->SupplyDayInMonth();
    int month = g_ALandscape->SupplyMonth();
    if ( ( day == 1 ) && ( month == 1 ) ) g_Year++;

	if (g_Species == 1) g_PredatorManager->Run(1);
	if (g_Species == 6)
	{
		for (int tenmin = 0; tenmin<144; tenmin++)
		{
			g_AManager->Run(1); // Goose Model
			g_Hunter_Population_Manager->Run(1);
			if (g_date->TickMinute10()) g_date->TickHour();
		}
	}
	else g_AManager->Run(1);

    char str[255];
	strcpy(str,g_AManager->SpeciesSpecificReporting(g_Species,g_time));

    g_ALandscape->DumpVegAreaData( g_time );
  }
}
//---------------------------------------------------------------------------
