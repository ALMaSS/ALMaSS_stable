/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*//**
\file
\brief
<B>PopulationManager.h This is the header file for the population manager and associated classes</B> \n
*/
/**
\file
 by Chris J. Topping \n
 Version of 23rd July 2003 \n
 \n
 With additions as noted in: \n
 January 2008 \n
 Doxygen formatted comments in May 2008 \n
*/
//---------------------------------------------------------------------------

#ifndef PopulationManagerH
  #define PopulationManagerH

// Forwards
class TAnimal;
class ALMaSSGUI;
class AlleleFreq;
class AOR_Probe;

// Start defines
typedef char * AnsiString;
typedef vector < TAnimal * > TListOfAnimals;
// END defines


//------------------------------------------------------------------------------
/**
* \brief An enum to hold all the possible types of population handled by a Population_Manager class
*/
enum TTypesOfPopulation
{
	TOP_Skylark = 0,
	TOP_Vole,
	TOP_Spider,
	TOP_Beetle,
	TOP_Hare,
	TOP_Partridge,
	TOP_Goose,
	TOP_RoeDeer,
	TOP_Rabbit,
	TOP_Newt,
	TOP_Osmia,
	TOP_ApisRAM,
	TOP_OliveMoth,
	// Above this line are used for menu items and have to be in this order and present
	TOP_MarshFritillary,
	TOP_Dormouse,
	TOP_Predators,
	TOP_Hunters,
	TOP_LaceWing,
	TOP_foobar
};
//------------------------------------------------------------------------------

/**
\brief
A struct of 100 ints
*/
struct IntArray100 {
public:
  int n[ 100 ];
};

//------------------------------------------------------------------------------

/**
\brief
A struct defining two x,y coordinate sets of positive co-ords only
*/
struct rectangle {
public:
  unsigned m_x1;
  unsigned m_y1;
  unsigned m_x2;
  unsigned m_y2;
};
//------------------------------------------------------------------------------

/**
\brief
The base class of all ALMaSS objects requiring Step code
*/
//------------------------------------------------------------------------------

class TALMaSSObject {
protected:
	/** \brief The basic state number for all objects - '-1' indicates death */
	int m_CurrentStateNo;
	/** \brief Indicates whether the iterative step code is done for this timestep */
	bool m_StepDone;
public:
	/** \brief Returns the current state number */
	int GetCurrentStateNo() {
		return m_CurrentStateNo;
	}
	/** \brief Sets the current state number */
	void SetCurrentStateNo(int a_num) {
		m_CurrentStateNo = a_num;
	}
	/** \brief Returns the step done indicator flag */
	bool GetStepDone() {
		return m_StepDone;
	}
	/** \brief Sets the step done indicator flag */
	void SetStepDone(bool a_bool) {
		m_StepDone = a_bool;
	}
	/** \brief BeingStep  behaviour - must be implemented in descendent classes */
	virtual void BeginStep(void) {}
	/** \brief Step  behaviour - must be implemented in descendent classes */
	virtual void Step(void) {}
	/** \brief EndStep  behaviour - must be implemented in descendent classes */
	virtual void EndStep(void) {}
	/** \brief Used to re-use an object - must be implemented in descendent classes */
	virtual void ReinitialiseObject() {
		m_StepDone = false;
		m_CurrentStateNo = 0;
	}
	/** \brief The constructor for TALMaSSObject */
	TALMaSSObject();
	/** \brief The destructor for TALMaSSObject */
	virtual ~TALMaSSObject();
	/** \brief Used for debugging only, tests basic object properties */
	void OnArrayBoundsError();
#ifdef __CJTDebug_5
	int AmAlive;
	int IsAlive() {
		return AmAlive;
	}
	void DEADCODEError();
#endif
};

//------------------------------------------------------------------------------

/**
\brief
A class defining an animals position
*/
class AnimalPosition
// used to communicate the position of an animal to inquiring objects
{
public:
  unsigned m_x;
  unsigned m_y;
  TTypesOfLandscapeElement m_EleType;
  TTypesOfVegetation m_VegType;
};
//------------------------------------------------------------------------------

/**
\brief
Part of the basic ALMaSS system (obselete)
*/
/**
Communicates the range centre, age and size of animals to other objects
*/
class RoeDeerInfo : public AnimalPosition
{
public:
  int m_Size;
  int m_Age;
  unsigned m_Range_x;
  unsigned m_Range_y;
  unsigned m_OldRange_x;
  unsigned m_OldRange_y;
};
//------------------------------------------------------------------------------

/**
\brief
The base class for all ALMaSS animal classes.
*/
/**
Includes all the functionality required to be handled by classes derived from Population_Manager, hence a number of empty methods that MUST be reimplemented in descendent classes e.g. CopyMyself()
*/
class TAnimal : public TALMaSSObject {
public:
  unsigned SupplyFarmOwnerRef();
  AnimalPosition SupplyPosition();
  APoint SupplyPoint() { APoint p( m_Location_x, m_Location_y); return p; }
  int SupplyPolygonRef() {
	return m_OurLandscape->SupplyPolyRef( m_Location_x, m_Location_y );
  }
  int Supply_m_Location_x() {
    return m_Location_x;
  }
  int Supply_m_Location_y() {
    return m_Location_y;
  }
  virtual void KillThis()
  {
    m_CurrentStateNo = -1;
    m_StepDone = true;
  };
  virtual void CopyMyself() {
  };
protected:
  int m_Location_x;
  int m_Location_y;
  Landscape * m_OurLandscape;
  /** \brief Corrects wrap around co-ordinate problems */
  void CorrectWrapRound() {
	/**
	Does the standard wrap around testing of positions. Uses the addition and modulus operators to avoid testing for negative or > landscape extent.
	This would be an alternative that should be tested for speed at some point.
	*/
	m_Location_x = (m_Location_x + m_OurLandscape->SupplySimAreaWidth()) % m_OurLandscape->SupplySimAreaWidth();
	m_Location_y = (m_Location_y + m_OurLandscape->SupplySimAreaHeight()) % m_OurLandscape->SupplySimAreaHeight();
  }
public:
  void SetX( int a_x ) {
    m_Location_x = a_x;
  }
  void SetY( int a_y ) {

    m_Location_y = a_y;

  }
  TAnimal( int x, int y, Landscape * L );
  // Note that below must be replaced in a descendent class e.g. Skylark_Base
  virtual void BeginStep( void ) {
  }
  virtual void Step( void ) {
  }
  virtual void EndStep( void ) {
  }
  /** \brief Used to re-use an object - must be implemented in descendent classes */
  virtual void ReinitialiseObject(int x, int y, Landscape * L) {
	  m_OurLandscape = L;
	  m_Location_x = x;
	  m_Location_y = y;
	  TALMaSSObject::ReinitialiseObject();
  }
  virtual int WhatState() {
    return 0;
  }
  virtual void Dying() {
    KillThis();
  }
  void CheckManagement( void );
  void CheckManagementXY( int x, int y );
  virtual bool OnFarmEvent( FarmToDo /* event */ ) {
    return false;
  }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

/**
\brief
Data structure to hold & output probe data probe data is designed to be used to return the number of objects in a given area or areas in specific element or vegetation types or farms
 */
class probe_data
{
protected:
  ofstream * m_MyFile;
  int m_Time;
  char m_MyFileName[ 255 ];
public:
  bool m_FileRecord;
  unsigned m_ReportInterval; // from 1-10
  unsigned m_NoAreas; // from 1-10
  rectangle m_Rect[ 10 ]; // can have up to ten areas
  unsigned m_NoEleTypes;
  unsigned m_NoVegTypes;
  unsigned m_NoFarms;
  TTypesOfVegetation m_RefVeg[ 25 ]; // up to 25 reference types
  TTypesOfLandscapeElement m_RefEle[ 25 ]; // up to 25 reference types
  unsigned m_RefFarms[ 25 ]; // up to 25 reference types
  // Species Specific Code below:
  bool m_TargetTypes[ 10 ]; // eggs,nestlings,fledgelings,males,females etc.
  void FileOutput( int No, int time, int ProbeNo );
  void FileAppendOutput( int No, int time );
  probe_data();
  void SetFile( ofstream * F );
  ofstream * OpenFile( char * Nme );

  bool OpenForAppendToFile() {
	  m_MyFile = new ofstream(m_MyFileName, ios::app);
	  if (!(*m_MyFile).is_open())
	  {
		  g_msg->Warn( WARN_FILE, "PopulationManager::AppendToFile() Unable to open file for append: ", m_MyFileName );
		  exit(1);
	  }
	  return true;
  }
  void CloseFile();
  ~probe_data();
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class SimpleStatistics
{
	/**
	* This class is designed to provide the facility to create simple stats from data that comes in incrementally.
	* It can provide the mean, variance of the data set at any point in time
	*/
protected:
	double m_K;
	double m_n;
	double m_Sum;
	double m_SumX;
	double m_SumX2;
public:
	/** \brief SimpleStatistics constructor */
	SimpleStatistics()
	{
		ClearData();
	}
	/** \brief Add a value */
	void add_variable(double x){
		// This uses the computed shifted data equation. 
		if (m_n == 0) m_K = x;
		m_n++;
		m_Sum += x;
		m_SumX += x - m_K;
		m_SumX2 += (x - m_K) * (x - m_K);
	}
	/** \brief Remove a value */
	void remove_variable(double x){
		m_n--;
		m_Sum -= x;
		m_SumX -= (x - m_K);
		m_SumX2 -= (x - m_K) * (x - m_K);
	}
	/** \brief Returns the number of values */
	double get_N(){
		return m_n;
	}
	/** \brief Returns the mean */
	double get_Total(){
		return m_Sum;
	}
	/** \brief Returns the mean */
	double get_meanvalue(){
		if (m_n == 0) return -1;
		return m_K + m_SumX / m_n;
	}
	/** \brief Returns the population variance */
	double get_varianceP(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (m_SumX2 - (m_SumX*m_SumX) / m_n) / (m_n);
	}
	/** \brief Returns the sample variance */
	double get_varianceS(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (m_SumX2 - (m_SumX*m_SumX) / m_n) / (m_n - 1);
	}
	/** \brief Returns the sample standard deviation */
	double get_SD(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (sqrt(get_varianceS()));
	}
	/** \brief Returns the sample standard error */
	double get_SE(){
		if (m_n < 2)
		{
			return -1; // Ilegal n value, but don't want to exit
		}
		return (sqrt(get_varianceS()/get_N()));
	}
	/** \brief Clears the data */
	void ClearData()
	{
		m_K = 0;
		m_n = 0;
		m_Sum = 0;
		m_SumX = 0;
		m_SumX2 = 0;
	}
};
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
/**
\brief
Base class for all population managers
*/
/**
The core of the handling of animal populations. All time-step code and most input/output is handled by this class and its descendents. This class effectively implements a state machine to facilitate simulation of animal behaviours and handle potential issues with concurrency. The PopulationManager class is never instantiated but must be used by deriving a descendent class.
*/
class Population_Manager {
public:
	// Methods
	Population_Manager(Landscape * L);
	virtual ~Population_Manager(void);

	
	void SetNoProbes(int a_pn) { m_NoProbes = a_pn; }
	/** \brief Gets the number of 'live' objects for a list index in the TheArray */
	unsigned GetLiveArraySize(int a_listindex) {
		return m_LiveArraySize[a_listindex];
	}
	/** \brief Increments the number of 'live' objects for a list index in the TheArray */
	void IncLiveArraySize(int a_listindex) {
		m_LiveArraySize[a_listindex]++;
	}
	virtual void Catastrophe(int /* a_mort */) {
	}
	unsigned int FarmAnimalCensus(unsigned int a_farm, unsigned int a_typeofanimal);
	char* SpeciesSpecificReporting(int a_species, int a_time);
	char* ProbeReport(int a_time);
	char* ProbeReportTimed(int a_time);
	void ImpactProbeReport(int a_Time);
	bool BeginningOfMonth();
	void LOG(const char* fname);
	int SupplyStepSize() {
		return m_StepSize;
	}
	int SupplySimW() {
		return SimW;
	}
	int SupplySimH() {
		return SimH;
	}
	virtual void Run(int NoTSteps);
	virtual float Probe(int ListIndex, probe_data * p_TheProbe);
	virtual void ImpactedProbe();
	int SupplyListNameLength() {
		return m_ListNameLength;
	}
	/** \brief Returns the pointer indexed by a_index and a_animal. Note NO RANGE CHECK */
	TAnimal* SupplyAnimalPtr(int a_index, int a_animal) {
		return TheArray[a_index][a_animal];
	}
	unsigned SupplyListIndexSize() {
		return (unsigned)TheArray.size();
	}
	unsigned SupplyListSize(unsigned listindex) {
		return (unsigned)TheArray[listindex].size();
	}
	/** \brief Debug method to test for out of bounds coordinates */
	bool CheckXY(int l, int i);

	//---------------------------------------------------------------------------
	const char* SupplyListName(int i) {
            return m_ListNames[i];
	}
	bool IsLast(unsigned listindex) {
		if (TheArray[listindex].size() > 1) return false; else
			return true;
	}
	/**
	IntArray100 * SupplyStateList() {
	  return & StateList;
	}
	*/
	int SupplyState(unsigned listindex, unsigned j) {
		return TheArray[listindex][j]->WhatState();
	}
	virtual void SupplyLocXY(unsigned listindex, unsigned j, int & x, int & y) {
		x = TheArray[listindex][j]->Supply_m_Location_x();
		y = TheArray[listindex][j]->Supply_m_Location_y();
	}
	const char* SupplyStateNames(int i) {
		return StateNames[i];
	}
	unsigned SupplyStateNamesLength() {
		return StateNamesLength;
	}
	virtual void DisplayLocations();
	int ProbeFileInput(char * p_Filename, int p_ProbeNo);
	// Attributes
#ifdef __ALMASS_VISUAL
	ALMaSSGUI * m_MainForm; // Need to use this when drawing to main form
#endif
	int IndexArrayX[5][10000];
	probe_data * TheProbe[100];
	int SimH, SimW;
	unsigned SimHH, SimWH;
	char m_SimulationName[255];
	bool ProbesSet; // used to show when probes are needed to be set
	Landscape * m_TheLandscape;
	TAnimal * FindClosest(int x, int y, unsigned Type);
	// Output arrays
#ifndef __UNIX__
// unsigned Counts[ 10 ] [ 100 ];
#endif
protected:
	// Attributes
	  // Holds the number of live animals repsented in each element of vector of vectors TheArray
	vector<unsigned> m_LiveArraySize;
	int m_NoProbes;
	AOR_Probe* m_AOR_Probe;
	FILE* m_GeneticsFile;
	FILE* m_AlleleFreqsFile;
	FILE* m_EasyPopRes;
	//FILE* m_FledgelingFile;
	const char* StateNames[100];
	int m_catastrophestartyear;
	int m_StepSize;
	vector < TListOfAnimals > TheArray; // Creates an empty array of arrays
	unsigned StateNamesLength;
	/* IntArray100 StateList; */
	const char* m_ListNames[32];
	unsigned m_ListNameLength;
	FILE * TestFile;
	FILE * TestFile2;
	// dww. Hardcoded 12. Fix.
	unsigned BeforeStepActions[12];
	/** \brief Holds the season number. Used when running goose and hunter sims.*/
	int m_SeasonNumber;
	// Methods
	/** \brief  Overrides the population manager StepFinished - there is no chance that hunters do not finish a step behaviour. */
	virtual bool StepFinished();
	virtual void DoFirst();
	virtual void DoBefore();
	virtual void DoAfter();
	virtual void DoAlmostLast();
	virtual void DoLast();
	/** \brief Removes all objects from the TheArray by deleting them and clearing TheArray */
	void EmptyTheArray();
	void SortX(unsigned Type);
	void SortXIndex(unsigned Type);
	void SortY(unsigned Type);
	void SortState(unsigned Type);
	void SortStateR(unsigned Type);
	unsigned PartitionLiveDead(unsigned Type);
	void Shuffle_or_Sort(unsigned Type);
	void Shuffle(unsigned Type);
	virtual void Catastrophe();
public:
	// Grid related functions
	bool OpenTheRipleysOutputProbe(string a_NWordFilename);
	void OpenTheAOROutputProbe(string a_AORFilename);
	bool OpenTheMonthlyRipleysOutputProbe();
	bool OpenTheReallyBigProbe();
	virtual void TheAOROutputProbe();
	virtual void TheRipleysOutputProbe(FILE* a_prb);
	virtual void TheReallyBigOutputProbe();
	void CloseTheMonthlyRipleysOutputProbe();
	virtual void CloseTheRipleysOutputProbe();
	virtual void CloseTheReallyBigOutputProbe();
	TTypesOfPopulation GetPopulationType() { return m_population_type; }
	/** \brief Get the season number */
	int GetSeasonNumber() { return m_SeasonNumber; }
protected:
	TTypesOfPopulation m_population_type;
	ofstream* AOROutputPrb;
	FILE * RipleysOutputPrb;
	FILE * RipleysOutputPrb1;
	FILE * RipleysOutputPrb2;
	FILE * RipleysOutputPrb3;
	FILE * RipleysOutputPrb4;
	FILE * RipleysOutputPrb5;
	FILE * RipleysOutputPrb6;
	FILE * RipleysOutputPrb7;
	FILE * RipleysOutputPrb8;
	FILE * RipleysOutputPrb9;
	FILE * RipleysOutputPrb10;
	FILE * RipleysOutputPrb11;
	FILE * RipleysOutputPrb12;
	FILE * ReallyBigOutputPrb;

	long int lamdagrid[2][257][257]; // THIS ONLY WORKS UP TO 10x10 KM !!!!
public:
	void LamdaDeath(int x, int y) {
		// inlined for speed
		lamdagrid[1][x / __lgridsize][y / __lgridsize]++;
	}
	void LamdaBirth(int x, int y) {
		lamdagrid[0][x / __lgridsize][y / __lgridsize]++;
	}
	void LamdaBirth(int x, int y, int z) {
		lamdagrid[0][x / __lgridsize][y / __lgridsize] += z;
	}
	void LamdaClear() {
		for (int i = 0; i < 257; i++) {
			for (int j = 0; j < 257; j++) {
				lamdagrid[0][i][j] = 0;
				lamdagrid[1][i][j] = 0;
			}
		}
	}
	void LamdaDumpOutput();
	// end grid stuff
public: // Special ones for compatability to descended managers
	virtual int SupplyPegPosx(int) {
		return 0;
	}
	virtual int SupplyPegPosy(int) {
		return 0;
	}
	virtual int SupplyCovPosx(int) {
		return 0;
	}
	virtual int SupplyCovPosy(int) {
		return 0;
	}
	virtual bool OpenTheFledgelingProbe() {
		return false;
	}
	virtual bool OpenTheBreedingPairsProbe() {
		return false;
	}
	virtual bool OpenTheBreedingSuccessProbe() {
		return false;
	}
	virtual void BreedingPairsOutput(int) {
	}
	virtual int TheBreedingFemalesProbe(int) {
		return 0;
	}
	virtual int TheFledgelingProbe() {
		return 0;
	}
	virtual void BreedingSuccessProbeOutput(double, int, int, int, int, int, int, int) {
	}
	virtual int TheBreedingSuccessProbe(int &, int &, int &, int &, int &, int &) {
		return 0;
	}
	virtual void FledgelingProbeOutput(int, int) {
	}
	virtual void TheGeneticProbe(unsigned, int, unsigned &) {
	}
	virtual void GeneticsResultsOutput(FILE *, unsigned) {
	}
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

/**
\brief
A small class to hold the pointers to active population managers
*/
class PopulationManagerList
{
public:
  PopulationManagerList() { for (int i=0; i< TOP_foobar; i++) m_populationlist[i] = NULL; }
  void SetPopulation(Population_Manager* p_pm, TTypesOfPopulation a_pt) { m_populationlist[a_pt] = p_pm; }
  Population_Manager* GetPopulation(TTypesOfPopulation a_pt) { return m_populationlist[a_pt]; }
protected:
  Population_Manager* m_populationlist[TOP_foobar];
};
//------------------------------------------------------------------------------


#endif


