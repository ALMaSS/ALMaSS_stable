#include <iostream>
#include <fstream>
#include <string.h>
#include "../Landscape/ls.h"
#include "PopulationManager.h"
#include "../GooseManagement/Goose_Base.h"
#include "AOR_Probe.h"

using namespace std;

AOR_Probe::AOR_Probe(Population_Manager* a_owner, Landscape* a_TheLandscape, string a_filename)
{
	m_owner = a_owner;
	m_TheLandscape = a_TheLandscape;
	if (a_filename == "") a_filename = "NWordOutputPrb.txt"; // this is for backwards compatability - this output used to be called NWord
	m_ProbeFile.open(a_filename, ios::out);
	if (!m_ProbeFile) {
		g_msg->Warn(WARN_FILE, "Population_Manager::AOR_Probe: ""Unable to open NWord probe file", "");
		exit(1);
	}
	m_ProbeFile << "Year" << '\t' << "Day" << '\t' << "Total_no" << '\t' << "Cells50" << '\t' << "Occupied50" << '\t' << "Cells100" << '\t' << "Occupied100" << '\t' << "Cells200" << '\t' << "Occupied200" << '\t' << "Cells400" << '\t' << "Occupied400" << endl;
	m_gridcountsize[0] = 50;
	m_gridcountsize[1] = 100;
	m_gridcountsize[2] = 200;
	m_gridcountsize[3] = 400;
	for (int g = 0; g < 4; g++) {
		m_gridcountwidth[g] = m_TheLandscape->SupplySimAreaWidth() / m_gridcountsize[g];
		m_gridcountheight[g] = m_TheLandscape->SupplySimAreaHeight() / m_gridcountsize[g];
		m_totalcells[g] = m_gridcountwidth[g] * m_gridcountheight[g];
		m_gridcount[g].resize(m_totalcells[g]);
	}
}


void AOR_Probe::WriteData()
{
	/** A common output stub for the AOR output probe. Specialist counting for each species
	occurs as part of the AOR grid probe, then this method deals with the final output. */
	int Counted[4];
	int OccupiedCells[4];
	for (int gsz = 0; gsz < 4; gsz++) {
		Counted[gsz] = 0;
		OccupiedCells[gsz] = 0;
		for (int i = 0; i < m_gridcountwidth[gsz]; i++) {
			for (int j = 0; j < m_gridcountheight[gsz]; j++) {
				int res = m_gridcount[gsz][i + j*m_gridcountwidth[gsz]];
				Counted[gsz] += res;
				if (res > 0) OccupiedCells[gsz]++;
			}
		}
	}
	m_ProbeFile << m_TheLandscape->SupplyYearNumber() << '\t' << (int)m_TheLandscape->SupplyDayInYear() << '\t' << Counted[0] << '\t';
	for (int c = 0; c < 3; c++) {
		m_ProbeFile << m_totalcells[c] << '\t' << OccupiedCells[c] << '\t';
	}
	m_ProbeFile << m_totalcells[3] << '\t' << OccupiedCells[3] << endl;
}

void AOR_Probe::DoProbe(int a_lifestage) {
	/** Counts all a_lifestage animals in each grid of each size */
	unsigned int total = (unsigned)m_owner->GetLiveArraySize(a_lifestage);
	// Empty old data
	for (int grid = 0; grid < 4; grid++) {
		for (int i = 0; i < m_totalcells[grid]; i++) m_gridcount[grid][i] = 0;
	}
	// For each animal get the location and place it in each of the (4) grids
	for (unsigned j = 0; j < total; j++)      //adult females
	{
		APoint pt = m_owner->SupplyAnimalPtr(a_lifestage, j)->SupplyPoint();
		for (int grid = 0; grid < 4; grid++) {
			int gx = pt.m_x / m_gridcountsize[grid];
			int gy = pt.m_y / m_gridcountsize[grid];
			m_gridcount[grid][gx + gy*m_gridcountwidth[grid]]++;
		}
	}
	WriteData();
}
//-----------------------------------------------------------------------------

AOR_Probe_Goose::AOR_Probe_Goose(Population_Manager * a_owner, Landscape * a_TheLandscape, string a_filename) : AOR_Probe( a_owner, a_TheLandscape, a_filename)
{
	;
}

void AOR_Probe_Goose::DoProbe(int a_goosespecies)
{
	Goose_Base* aGoose;
	for (int grid = 0; grid < 4; grid++) {
		// The line below may be the fastest way to fill a vector of ints with '0', but the one following is the safe way
		// memset(&m_gridcount[grid][0], 0, m_gridcount[grid].size() * sizeof m_gridcount[grid][0]);
		fill(m_gridcount[grid].begin(), m_gridcount[grid].end(), 0);
	}
	for (int groups = 0; groups < 2; groups++)
	{
		int gooseindex = a_goosespecies * 2 + groups;
		unsigned int total = (unsigned)m_owner->GetLiveArraySize(gooseindex);
		for (int grid = 0; grid < 4; grid++) 
		{
			for (unsigned j = 0; j < total; j++)
			{
				aGoose = dynamic_cast<Goose_Base*>(m_owner->SupplyAnimalPtr(gooseindex, j));
				if (aGoose->GetCurrentStateNo() != -1) {
					APoint pt = aGoose->SupplyPoint();
					int gx = pt.m_x / m_gridcountsize[grid];
					int gy = pt.m_y / m_gridcountsize[grid];
					m_gridcount[grid][gx + gy*m_gridcountwidth[grid]] += aGoose->GetGroupsize();
				}
			}
		}
	}
	WriteData();
}
