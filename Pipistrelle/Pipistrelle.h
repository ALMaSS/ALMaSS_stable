/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Pipistrelle.h
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file Pipistrelle.h
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef PipistrelleH
#define PipistrelleH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Pipistrelle_Base;
class Pipistrelle_Population_Manager;

//------------------------------------------------------------------------------
/**
Used for the population manager's list of Pipistrelle
*/
//typedef vector<Pipistrelle*> TListOfPipistrelle;
//---------------------------------------------------------------------------

/**
Pipistrelle like other ALMaSS animals work using a state/transition concept.
These are the Pipistrelle behavioural states, these need to be altered, but some are here just to show how they should look.
*/
enum TTypeOfPipistrelleState
{
      toPipistrelles_InitialState=0,
      toPipistrelles_Develop,
      toPipistrelles_Move,
      toPipistrelles_Die
};


class Pipistrelle_Base : public TAnimal
{
	/**
	ALL Pipistrelles have to know which cave they were born in, as well as their age, state, and the information inherited from TAnimal
	*/

protected:
	/** \brief Variable to record current behavioural state */
	TTypeOfPipistrelleState m_CurrentPipistrelleState;
	/** The age of the Pipistrelle in days */
	unsigned m_Age;
	/** The polygon reference to the Cave of birth */
	int m_HomeCave;
	/** \brief This is a time saving pointer to the correct population manager object */
	Pipistrelle_Population_Manager*  m_OurPopulationManager;
public:
	/** \brief Pipistrelle constructor */
	Pipistrelle_Base(int a_x, int a_y, unsigned a_Cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM);
	/** \brief Pipistrelle destructor */
	~Pipistrelle_Base();
	/** \brief Behavioural state development */
	TTypeOfPipistrelleState st_Develop(void) {
		;
	}
	/** \brief Behavioural state movement */
	TTypeOfPipistrelleState st_Movement(void) {
		;
	}
	/** \brief Behavioural state dying */
	void st_Dying(void);
	/** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void BeginStep(void) {
		;
	}// NB this is not used in the Pipistrelle_Bae code
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void) {
		;
	}
	/** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
	virtual void EndStep(void) {
		;
	}// NB this is not used in the Pipistrelle_Base code
	/** \brief A typical interface function - this one returns the Age as an unsigned integer */
	unsigned GetAge() { return m_Age; }
	/** \brief A typical interface function - this one returns the Age as an unsigned integer */
	unsigned SetAge(unsigned a_age) { m_Age = a_age; }
	/** \brief A typical interface function - this one returns the Age as an unsigned integer */
	unsigned GetHomeCave() { return m_HomeCave; }
	/** \brief A typical interface function - this one returns the Age as an unsigned integer */
	unsigned SetHomeCave(int a_Cave) {
		m_HomeCave = a_Cave;
	}
};

class Pipistrelle_Young : public Pipistrelle_Base
{
protected:
	int m_AgeDegrees;
public:
	/** \brief Pipistrelle constructor */
	Pipistrelle_Young(int a_x, int a_y, unsigned a_Cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM);
	/** \brief Pipistrelle destructor */
	~Pipistrelle_Young();
protected:
	/** \brief Behavioural state development */
	TTypeOfPipistrelleState st_Develop(void);
};

class Pipistrelle_Juvenile : public Pipistrelle_Young
{
public:
	Pipistrelle_Juvenile(int a_x, int a_y, unsigned a_Cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM);
	/** \brief Pipistrelle_Juvenile destructor */
	~Pipistrelle_Juvenile();
};

class Pipistrelle_Male : public Pipistrelle_Juvenile
{
public:
	Pipistrelle_Male(int a_x, int a_y, unsigned a_Cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM);
	/** \brief Pipistrelle_Male destructor */
	~Pipistrelle_Male();
};

class Pipistrelle_Female : public Pipistrelle_Juvenile
{
public:
	Pipistrelle_Female(int a_x, int a_y, unsigned a_Cave, Landscape* a_L, Pipistrelle_Population_Manager* a_NPM);
	/** \brief Pipistrelle_Female destructor */
	~Pipistrelle_Female();
};

#endif
