//
// roe_pop_manager.cpp
//
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include <algorithm>
#include "Roe_all.h"
#include "Roe_constants.h"
#include "Roe_pop_manager.h"

extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_ReallyBigOutput_used;



RoeDeer_Population_Manager::RoeDeer_Population_Manager(Landscape* L)//changed from L to p_L 13/7/2012 ljk
   : Population_Manager(L)
{
  m_TheGrid = new Roe_Grid(L);
  Turnover=0;  //total number of individuals created during a simulation
  StepCounter=0;
  m_MaleDispThreshold = 0;
  m_FemaleDispThreshold = 0;
  m_MinWeight_Males = 0;
  m_MinWeight_Females = 0;
  m_CriticalWeight_Males = 0;
  m_CriticalWeight_Females = 0;
  m_totalarea = 0;

  // empty all the group pointers
  for (int i=0; i<MaxNoDeer; i++)
  {
     m_GroupList[i]=NULL;
     m_FixCounter[i] = -1;
	 m_Fightlist[i] =NULL; // ljk 2/7 2012
  }
  
   
  m_NoGroups=0;

  // Get SimW & SimH (landscape size)
  SimW= L->SupplySimAreaWidth();
  SimH= L->SupplySimAreaHeight();
  // Three lists are needed so need to remove 7 of the ten default arrays
  // Fawn, male, Female
  for (int i=0; i<7; i++)
  {
    TheArray.pop_back();
  }
  m_MoveMap=new MovementMap(L, 8); // 8 for roe deer //**ljk inserted on 25/7-2012
  Init();
}
//---------------------------------------------------------------------------
RoeDeer_Population_Manager::~RoeDeer_Population_Manager()
{
  delete m_TheGrid;
}
//---------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::Init - Initialises the run ( and is thus only accessed once at beginning),
by checking for output probes, preprocess landscape to get the quality of grid cells in m_TheGrid, 
adds female and male objects for year 0 and loads state names. Calls functions PreProcessLandscape() 
and SupplyElementType().
*/
void RoeDeer_Population_Manager::Init (void)
{
  
   strcpy(m_SimulationName,"RoeDeer");
 
   //ljk added 22/8-2012
 if ( cfg_RipleysOutput_used.value() ) {
    OpenTheRipleysOutputProbe("");
  }
  if ( cfg_ReallyBigOutput_used.value() ) {
	OpenTheReallyBigProbe();
  } else ReallyBigOutputPrb=0;

  m_gridextenty = int(SimH * InvCell_Size); 
  m_gridextentx = int(SimW * InvCell_Size);

  //First thing to do is to preprocess landscape to get the quality of grid
  //cells in m_TheGrid
  
  PreProcessLandscape();

  int number=0; //number of each sex created
  // Create the right number of males and females
  switch(SimW)
    {
    case 2000:
      number=50; //in 2x2
      break;
    case 5000:
      number=80; //in 5x5
      break;
    case 7000:
      number=250;
      break;
    case 10000:
      number=500; //in 10x10
      break;
    default:
		number = (int) (0.000005 * SimW * SimH); 
    }        
 Deer_struct* ads;
 ads = new Deer_struct;
 ads->Pop = this;
 ads-> L = m_TheLandscape;
 ads->mum = NULL;
 ads->sex = true;
 ads->group = -1;  //in constructor
 ads->ID = -1; //don't know yet
 ads->size = -1;  //in constructor
 ads->age = 575;  //born on average 1st of June. Just for 1st generation roe
 TTypesOfLandscapeElement El;
 for (int i=0; i<number; i++)
 {
   ads->x = random(m_TheLandscape->SupplySimAreaWidth());
   ads->y = random(m_TheLandscape->SupplySimAreaHeight());
   //make sure they are dropped in either forest or field
   El = m_TheLandscape->SupplyElementType(ads->x,ads->y);
   while((El!=tole_ConiferousForest)&&(El!=tole_DeciduousForest)&&
             (El!=tole_MixedForest)&&(El!=tole_YoungForest)&&(El!=tole_Field))
   {
     ads->x = random(m_TheLandscape->SupplySimAreaWidth());
     ads->y = random(m_TheLandscape->SupplySimAreaHeight());
     El = m_TheLandscape->SupplyElementType(ads->x,ads->y);
   }
   CreateObjects(1,NULL,NULL,ads,1);
 }
 ads->sex=false;    //females
 for (int i=0; i<number; i++)
 {
   ads->x = random(m_TheLandscape->SupplySimAreaWidth());
   ads->y = random(m_TheLandscape->SupplySimAreaHeight());
   //make sure they are dropped in either forest or field
   El = m_TheLandscape->SupplyElementType(ads->x,ads->y);
  while((El!=tole_ConiferousForest)&&(El!=tole_DeciduousForest)&&
             (El!=tole_MixedForest)&&(El!=tole_YoungForest)&&(El!=tole_Field))
   {
     ads->x = random(m_TheLandscape->SupplySimAreaWidth());
     ads->y = random(m_TheLandscape->SupplySimAreaHeight());
     El = m_TheLandscape->SupplyElementType(ads->x,ads->y);
   }
   CreateObjects(2,NULL,NULL,ads,1);
 }
 delete ads;

 // Set up the list names (i.e. what are our animals called in this simulation
 m_ListNames[0]="Fawn";
 m_ListNames[1]="Male Roe Deer";
 m_ListNames[2]="Female Roe Deer";
 m_ListNameLength=3;

 //load state names
 StateNames[rds_Initialise] = "Initial State";
 StateNames[rds_FOnMature] = "Female OnMature";
 StateNames[rds_MOnMature] = "Male OnMature";
 StateNames[rds_FUpdateGestation] = "Female Update Gestation";
 StateNames[rds_FUpdateEnergy] = "Female Update Energy";
 StateNames[rds_MUpdateEnergy] = "Male Update Energy";
 StateNames[rds_FAUpdateEnergy] = "Fawn Update Energy";
 StateNames[rds_FEstablishRange] = "Female Establish Range";
 StateNames[rds_MEstablishRange] = "Male Establish Range";
 StateNames[rds_MEstablishTerritory] = "Male Establish Territory";
 StateNames[rds_FDisperse] = "Female Disperse";
 StateNames[rds_MDisperse] = "Male Disperse";
 StateNames[rds_FOnNewDay] = "Female OnNewDay";
 StateNames[rds_MOnNewDay] = "Male OnNewDay";
 StateNames[rds_FAOnNewDay] = "Fawn OnNewDay";
 StateNames[rds_FFormGroup] = "Female Form Group";
 StateNames[rds_FInHeat] = "Female In Heat";
 StateNames[rds_FGiveBirth] = "Female Give Birth";
 StateNames[rds_FCareForYoung] = "Female Care For Young";
 StateNames[rds_FRun] = "Female Run";
 StateNames[rds_MRun] = "Male Run";
 StateNames[rds_FARunToMother] = "Fawn Run To Mother";
 StateNames[rds_FRecover] = "Female Recover";
 StateNames[rds_MRecover] = "Male Recover";
 StateNames[rds_FARecover] = "Fawn Recover";
 StateNames[rds_FIgnore] = "Female Ignore";
 StateNames[rds_MIgnore] = "Male Ignore";
 StateNames[rds_FEvade] = "Female Evade";
 StateNames[rds_MEvade] = "Male Evade";
 StateNames[rds_MEvade] = "Male Evade";
 StateNames[rds_FAHide] = "Fawn Hide";
 StateNames[rds_MFight] = "Male Fight";
 StateNames[rds_MAttendFemale] = "Male Attend Female";
 StateNames[rds_FRuminate] = "Female Ruminate";
 StateNames[rds_MRuminate] = "Male Ruminate";
 StateNames[rds_FARuminate] = "Fawn Ruminate";
 StateNames[rds_FFeed] = "Female Feed";
 StateNames[rds_MFeed] = "Male Feed";
 StateNames[rds_FAFeed] = "Fawn Feed";
 StateNames[rds_FASuckle] = "Fawn Suckle";
 StateNames[rds_FMate] = "Female Mate";
 StateNames[rds_MMate] = "Male Mate";
 StateNames[rds_FDie] = "Female Die";
 StateNames[rds_MDie] = "Male Die";
 StateNames[rds_FADie] = "Fawn Die";
 StateNames[rds_FDeathState] = "Female Death State";
 StateNames[rds_MDeathState] = "Male Death State";
 StateNames[rds_FADeathState] = "Fawn Death State";
 StateNames[rds_FAInit] = "Fawn Initial State";

 //determine whether we should shuffle or sort or do nothing
 BeforeStepActions[0] = 0; //'0' is shuffle, '1' is sortx, '2' is sorty, '3' is nothing
 BeforeStepActions[1] = 0;
 BeforeStepActions[2] = 0;
 BeforeStepActions[3] = 0;
 BeforeStepActions[4] = 0;
}

//---------------------------------------------------------------------------
/**
PreProcessLandscape starts at top-left corner (0,0) and runs through the
    whole landscape to create a grid of qualities. For each gridcell of a
    predefined size (=const int Cell_Size, minimum is 20x20) it evaluates the
    quality, using RangeQuality() and saves it in the array m_grid[][].
    This map of qualities is constant throughout the simulation.
*/
void RoeDeer_Population_Manager::PreProcessLandscape()
{
  
  int a_x = Cell_HalfSize;
  int a_y = Cell_HalfSize;    //centre of first cell
  int row = SimH / Cell_Size;
  int column = SimW / Cell_Size;
  int value;

  for(int i=0;i<column;i++)   //x-values, this many columns
  {
    for (int j=0;j<row;j++)  //y-values, this many rows
    {
      value =(int) RangeQuality(a_x,a_y,Cell_HalfSize);
      //add this value
      m_TheGrid->AddGridValue(i,j,value);
      a_y += Cell_Size; //move right to next column
    }
    a_x += Cell_Size;  //move down to next row
    a_y = Cell_HalfSize; //reset a_x to first cell in new row

  }

/*   //Lene: kommenter denne sektion ud naar du korer dine scenarier. Kun til at
   //teste fordelingen af kvaliteter
//*************************************
   int rand_x=-1;
   int rand_y=-1;
   int total=0;
   for(int i=0;i<NoRandomAreas;i++)
   {
     rand_x= random(SimW);
     rand_y= random(SimH);
     total = ScanGrid(rand_x,rand_y,true);
     WriteRandHR("RandomHR.txt", total);
     total=0;
   }
//**************************************
 */
}

//----------------------------------------------------------------------------
/**
 RoeDeer_Population_Manager::ScanGrid - adds up quality values stored in m_grid to see if a home
    range can be established here. Starts with a minimun area and extends it
    outwards until sufficient or maximum is reached. It returns the range size if the area is OK
	and zero if not. Uses functions Supply_GridCoord(), Supply_GridValue() and AddToArea().
*/
int RoeDeer_Population_Manager::ScanGrid(int p_x,int p_y,bool avq)
{
  /*
    This function adds up quality values stored in m_grid to see if a home
    range can be established here. Starts with a minimun area and extends it
    outwards until sufficient or maximum is reached. It returns the range size
    if the area is OK and zero if not.
  */
  //p_x and p_y are landscape coordinates, so find out what the grid coordinates
  //are here
  int g_x = m_TheGrid->Supply_GridCoord(p_x);
  int g_y = m_TheGrid->Supply_GridCoord(p_y);
  //what is the range measured in grid cells?
  int less = int (MinRange * InvCell_Size);
  //then find the starting coordinates in minimum area
  int start_x = g_x - less;
  int start_y = g_y - less;
  int end_x = g_x + less;
  int end_y = g_y + less;
  int quality = 0;
  int range = MinRange;
  //First sum up minimum area = 2*MinRange x 2*Minrange
  if ((start_x < less)||(start_y < less) ||
	  (end_x >= m_gridextentx - less) || (end_y >= m_gridextenty - less))  //cound be overlap
  {
	  start_x += m_gridextentx;
	  start_y += m_gridextenty;
	  end_x += m_gridextentx;
	  end_y += m_gridextenty;

    for (int i=start_x; i< end_x; i++)  //each column
    {
      for (int j=start_y; j<end_y; j++) //each row
      {
		  quality += m_TheGrid->Supply_GridValue(i%m_gridextentx, j%m_gridextenty);
      }
    }
    //minimum area is evaluated. If this is for average range quality return
    //quality here
    if(avq) return quality;
    else
    {
      while((quality <= MinRangeQuality)&&(range < MaxRange))
      {  //extend area outwards by adding layers of cells until quality is high enough
        start_x --;
        start_y --;
        end_x ++;
        end_y ++;
        range += Cell_Size;

		quality += (AddToAreaCC(start_x%m_gridextentx, start_y%m_gridextenty, int(range*InvCell_Size)));
      }
    }
  }
  else  //no overlap
  {
    for (int i=start_x; i<end_x; i++)  //each column
    {
      for (int j=start_y; j<end_y; j++) //each row
      {
         quality += m_TheGrid->Supply_GridValue(i,j);
      }
    }
    //minimum area is evaluated. If this is for average range quality return
    //quality here
    if(avq) return quality;
    else
    {
      while((quality <= MinRangeQuality)&&(range < MaxRange))
      {  //extend area outwards by adding layers of cells until quality is high enough
        start_x --;
        start_y --;
        end_x ++;
        end_y ++;
        range += Cell_Size;

        quality += (AddToArea(start_x, start_y, int(range*InvCell_Size)));
      }
    }
  }
  if(quality >= MinRangeQuality)  //can set up range here
  {
    return range;
  }
  else return 0;
}
//----------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::AddToArea - Coordinates not corrected for wrap-around.
  Function sums up the quality in a layer of grid cells of size p_range x p_range
  Calls function Supply_GridValue(). 

*/
int RoeDeer_Population_Manager::AddToArea(int g_x, int g_y, int p_range)
{
  /*Coordinates not corrected . Starting in grid coordinates g_x,g_y this
  function summs up the quality in a layer of grid cells of size p_range x p_range*/

  int quali = 0;
  int length = 2*p_range; //size of the square

  for (int i=0; i< length;i++) //top and bottom row
  {
    quali += m_TheGrid->Supply_GridValue(g_x,g_y);
    quali += m_TheGrid->Supply_GridValue (g_x,g_y+length);
    g_x ++; //move right to next cell
  }
  //Don't want to count corner cells twice so increment g_y before function call
  //and subtract 2 from length
  for (int i=0; i< length-2;i++) //left and right column
  {
    g_y ++;  //move down to next cell
    quali += m_TheGrid->Supply_GridValue(g_x-length,g_y);
    quali += m_TheGrid->Supply_GridValue (g_x,g_y);
  }
  return quali;
}
//----------------------------------------------------------------------------

/**
RoeDeer_Population_Manager::AddToAreaCC - Coordinates corrected for wrap-around landscape.
Function sums up the quality in a layer of grid cells of size p_range x p_range.
 Calls function Supply_GridValue(). 
*/
int RoeDeer_Population_Manager::AddToAreaCC(int g_x, int g_y, int p_range)
{
  /* Coordinates corrected. Starting in grid coordinates g_x,g_y this function
  summs up the quality in a layer of grid cells of size p_range x p_range*/

  int quali = 0;
  int length = 2 * p_range; //size of the square

  g_x += m_gridextentx;
  g_y += m_gridextenty;
  for (int i=0; i< length;i++) //top and bottom row
  {
	  quali += m_TheGrid->Supply_GridValue(g_x%m_gridextentx, g_y%m_gridextenty);
	  quali += m_TheGrid->Supply_GridValue(g_x%m_gridextentx, (g_y + length) % m_gridextenty);
    g_x ++; //move right to next cell
  }
  //Don't want to count corner cells twice so increment g_y before function call
  //and subtract 2 from length
  for (int i=0; i< length-2;i++) //left and right column
  {
    g_y ++;  //move down to next cell
	quali += m_TheGrid->Supply_GridValue((g_x - length) % m_gridextentx, g_y%m_gridextenty);
	quali += m_TheGrid->Supply_GridValue(g_x%m_gridextentx, g_y%m_gridextenty);
  }
  return quali;
}
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------

/**
DoFirst - for every time step, this function calls PreProcessLandscape,
Clears fightlists for all males, creates several output files, such as deer location, deer home range,
population age structure etc. Also, the average size of the male and female population is calculated 
to get dispersal thresholds and sums up the amount of floaters in the population
*/
void RoeDeer_Population_Manager::DoFirst()
{
   int today=m_TheLandscape->SupplyDayInYear();
   int year= m_TheLandscape->SupplyYear();

   //**ljk** commented out on May 11 2012
  //StepCounter is incremented every timestep for as long as the simulation runs
 if (StepCounter% 144==0)    //new day
  {  //individual debug log files
     //LOG(today,year);
	//#ifndef __BATCH_VER
	//OutputForm->Edit1->Text="Day: "+IntToStr(today);
	//#endif
	  if (today==0)   //once a year
	{
	//   // // Update the year counter in MainForm
	//#ifndef __BATCH_VER
	//MainForm->Year++;
	//MainForm->Edit1->Text=IntToStr(MainForm->Year);
	//MainForm->Update();
	//#endif
      if(StepCounter!=0)  //once a year, but not the first year
      {
        PreProcessLandscape(); //update the grid with qualities!!!
      //clear fightlists for all males
        for (unsigned i=0;i<5000;i++)
        {
          if(m_Fightlist[i]!=NULL)
          {
            for(unsigned j=0; j>m_Fightlist[i]->size();j++)
            {
              (*m_Fightlist[i])[j]=NULL;
            }
          }
          delete m_Fightlist[i];
          m_Fightlist[i]=NULL;
        }
        //tell all males that fightlists have been cleared
        for(unsigned j=0;j<TheArray[1].size(); j++)
        {
          Roe_Male* deer;
          deer = dynamic_cast< Roe_Male * > (TheArray[1][j]);
          deer->ClearFightlist();
        }
        //write deadfile and clear list of dead animals
        //WriteDeadFile("Mortality.txt",year);
        //write age structure file
        //WriteAgeStructFile("Agestruct.txt",year);
        for(int i=0;i<2000;i++)
        {
          for(int j=0;j<2;j++)
          {
            DeadList[i][j] = 0;
          }
        }
      }
   }
   if((today%30==0)&&(today!=0))  //every 30 days
    {
      //calculate average size of the male and female population to get dispersal thresholds
      int malesize=0;
      int femalesize=0;
      int male_float=0;
      int female_float=0;
      for (unsigned i=1; i<TheArray.size();i++)  //don't include fawns
      {
        for(unsigned j=0;j<TheArray[i].size(); j++)
        {
          Roe_Base* deer;
          deer = dynamic_cast< Roe_Base * > (TheArray[i][j]);
          #ifdef JUJ__Debug3
          if(deer->IsAlive()!=0x0DEADC0DE)
          {
             m_TheLandscape ->Warn("Roe_Population_Manager:DoFirst(): Deadcode warning,deer!","");
             exit( 1 );
           }
          #endif
          #ifdef JUJ__Debug1
           if(deer==NULL)
           {
             m_TheLandscape ->Warn("Roe_Population_Manager:DoFirst(): NULL pointer, deer!","");
             exit( 1 );
           }
           #endif
          if(i==1)
          {
            malesize += deer->m_Size; //males
            //Sum up how many floaters in population
            if(deer->m_HaveRange==false)male_float++;
          }
          else
          {
            femalesize += deer->m_Size; //females
            //Sum up how many floaters in population
            if(deer->m_HaveRange==false) female_float++;
          }
        }
      }
      //write this number to file for debug
      //WriteFloatFile("Float.txt",male_float,female_float);

      //calculate dispersal threshold and min weights
      if(TheArray[1].size()>0)   //to avoid division with zero if no males
      {
        m_MaleDispThreshold = int (malesize/TheArray[1].size());
        m_MinWeight_Males = int (m_MaleDispThreshold * 0.75);
        m_CriticalWeight_Males = int (m_MaleDispThreshold * 0.80);
      }
      if(TheArray[2].size()>0)
      {
        m_FemaleDispThreshold = int (femalesize/TheArray[2].size());
        m_MinWeight_Females = int (m_FemaleDispThreshold * 0.75);
        m_CriticalWeight_Females = int (m_FemaleDispThreshold * 0.80);
      }
    }
 }

 //from year 10 (i.e. 2000) onwards, every 40 hrs, store the positions of all animals

   Roe_Base* deer;
  if ((year >= 2000)&&(StepCounter%240==0))
  {
    for (unsigned i=1; i<TheArray.size();i++)  //adults only
    {
      for(unsigned j=0;j<TheArray[i].size(); j++)  //for every animal of each type
      {
         deer = dynamic_cast< Roe_Base * > (TheArray[i][j]);
         #ifdef JUJ__Debug3
         if(deer->IsAlive()!=0x0DEADC0DE)
         {
             m_TheLandscape ->Warn("Roe_Population_Manager:DoFirst():Deadcode warning, deer!","");
             exit( 1 );
           }
         #endif
         //m_FixCounter is initialised to 0 when assigned to a new deer,
         //so increment after the first position has been added
         int number=deer->m_FixlistNumber;
         AnimalPosition xy = deer->SupplyPosition();
         m_FixList[number][m_FixCounter[number]][0]=xy.m_x;
         m_FixList[number][m_FixCounter[number]][1]=xy.m_y;
         m_FixCounter[number]++;

         //every month = 18 locations, calculate MCP home range and write it to file
         if(m_FixCounter[number]==18)
         {
           #ifdef JUJ__Debug1
           if(deer==NULL)
           {
             m_TheLandscape ->Warn("Roe_Population_Manager:DoFirst(): NULL pointer, deer!","");
             exit( 1 );
           }
           #endif
           //CalculateArea(0,m_FixCounter[ID]-1,ID); //first and last position to be sorted
           //deer->m_HomeRange = (float) m_totalarea/1000000;  //sqkm
           m_totalarea=0;
           m_FixCounter[number] = 0;  //this animals list is empty
           //WriteToFixFile(deer);
           //WriteToHRFile("HRFile.txt",month, year);
         }
      }
    }
  }
  StepCounter++;
}
//-----------------------------------------------------------------------------
void RoeDeer_Population_Manager::UpdateRanges()
{
  Roe_Base* deer;

  for (unsigned listindex=1; listindex<TheArray.size();listindex++)
  {
    for (unsigned j=0; j<TheArray[listindex].size(); j++)
    {
      deer =dynamic_cast< Roe_Base * > (TheArray[listindex][j]);
      RoeDeerInfo info = deer->SupplyInfo();
      if(deer->m_HaveRange)
      {
         deer->m_SearchRange = ScanGrid(info.m_x,info.m_y,false);
      }
    }
  }
}   
//-----------------------------------------------------------------------------
//void RoeDeer_Population_Manager::LOG(int day, int year) //debug log
//{ //writes to individual specific files
//  Roe_Male* M_ptr;
//  Roe_Female* F_ptr;
//  Roe_Fawn* FA_ptr;
//#ifndef __UNIX__
//   std::string filename("");//**ljk May 18 2012**
//  char* f;
//#else
//  char f[20];
//#endif
//
//  for (unsigned listindex=0; listindex<TheArray.size();listindex++)
//  {
//    FILE *PFile;
//    for (unsigned j=0; j<TheArray[listindex].size(); j++)
//    {
//      switch(listindex)
//      {
//        case 0: //fawn
//          FA_ptr=dynamic_cast< Roe_Fawn * > (TheArray[0][j]);
//#ifdef __UNIX__
//          sprintf( f, "Fawn%d.txt", FA_ptr->m_ID);
//#else
//		   filename += "Fawn" + boost::lexical_cast<std::string>(FA_ptr->m_ID)+".txt";//**ljk May 18 2012**
//		   f=(char *)filename.c_str();
//#endif
//	  if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::LOG::Unable to open file for appending data!\n",
//                       0);
//                exit( 1 );
//	  }
//
//          fprintf(PFile," %i,%i, ",day,year);  //day in year
//          fprintf(PFile," %i,%i,%i,%i\n", FA_ptr->m_ID,
//                               FA_ptr->m_Size,FA_ptr->m_Age,FA_ptr->m_MyGroup);
//          fclose(PFile);
//          break;
//        case 1:  //male
//         M_ptr=dynamic_cast< Roe_Male * > (TheArray[1][j]);
//#ifdef __UNIX__
//         sprintf( f, "Male%d.txt", FA_ptr->m_ID);
//#else
//         filename += "Male" + boost::lexical_cast<std::string>(M_ptr->m_ID)+".txt";//**ljk May 18 2012**
//		 f=(char *)filename.c_str();
//#endif
//	 if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::LOG::Unable to open file for appending data!\n",
//                       0);	 }
//         fprintf(PFile," %i,%i, ",day,year);  //day in year
//         fprintf(PFile," %i,%i,%i \n",M_ptr->m_ID,M_ptr->m_Size,M_ptr->m_Age);
//         fclose(PFile);
//         break;
//        case 2: //female
//           F_ptr=dynamic_cast< Roe_Female * > (TheArray[2][j]);
//#ifdef __UNIX__
//           sprintf( f, "Female%d.txt", FA_ptr->m_ID);
//#else
//         
//           filename += "Female" + boost::lexical_cast<std::string>(F_ptr->m_ID)+".txt";//**ljk May 18 2012**
//		   f=(char *)filename.c_str();
//#endif
//	   if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::LOG::Unable to open file for appending data!\n",
//                       0);	   }
//           fprintf(PFile," %i,%i, ",day,year);  //day in year
//           fprintf(PFile," %i,%i,%i,%i,%i,%i \n",F_ptr->m_ID,
//             F_ptr->m_Size,F_ptr->m_Age,F_ptr->m_MyGroup,F_ptr->SupplyIsPregnant(),
//             F_ptr->SupplyYoung());
//           fclose(PFile);
//           break;
//      }
//    }
//  }
//}
//----------------------------------------------------------------------------
//***ljk commented out 24/7-2012
//void RoeDeer_Population_Manager::WriteAgeStructFile(string name, int year)
//{
////every year sum up the number of animals in each age group and write to file
//   FILE *PFile;
//
// const char *f = name.c_str();
//
//       if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::WriteAgeStructFile::Unable to open file!\n",
//                       0);
//  }
//  Roe_Base* deer;
//  int age_array[20] = {0};
//  for (unsigned listindex=0; listindex<TheArray.size();listindex++)
//  {
//   for (unsigned j=0; j<TheArray[listindex].size(); j++)
//   {
//     //convert to Roe_Base
//     deer =dynamic_cast< Roe_Base * > (TheArray[listindex][j]);
//     int age = deer->m_Age/365;
//     age_array[age]++; //add one to this age
//   }
//  }
//  fprintf(PFile,"%i " ,year);
//  for (unsigned j=0;j<20;j++)
//  {
//    fprintf(PFile,"%i ",age_array[j]);
//    age_array[j] = 0;
//  }
//  fprintf(PFile," \n"); //new line
//  fclose(PFile);
//}
////------------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteDeadFile(string name, int p_year)
//{
//   FILE *PFile;
//
//const char *f = name.c_str();
//
//
//  if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::WriteDeadFile::Unable to open file!\n",
//                       0);
//  }
//
//  for(int i=0;i<200;i++)
//  {
//     if(DeadList[i][0]!= 0)
//     {
//       fprintf(PFile," %i %i %i \n",p_year,DeadList[i][0],DeadList[i][1]);
//     }
//  }
//  fclose(PFile);
//}
////-----------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteToHRFile(string name, int month, int year)
//{
// FILE *PFile;
//
//
//const char *f = name.c_str();
//
//
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//               m_TheLandscape->Warn("RoeDeerPopulationManager::WriteToHRFile::Unable to open file!\n",
//                       0);
// }
// Roe_Base* deer;
//
// for (unsigned listindex=0; listindex<TheArray.size();listindex++)
// {
//#ifdef __UNIX__
//   fprintf(PFile,"%s :\n",ListNames[listindex]);
//#else
//   fprintf(PFile,"%s :\n",& m_ListNames[listindex]);
//#endif
//   for (unsigned j=0; j<TheArray[listindex].size(); j++)
//   {
//     //convert to Roe_Base
//     deer =dynamic_cast< Roe_Base * > (TheArray[listindex][j]);
//     fprintf(PFile,"%i %i %i %f \n",year,month,deer->m_ID,deer->m_HomeRange);
//   }
// }
// fclose(PFile);
//}   
////---------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteFloatFile(string name, int male_f,int female_f)
//{
// FILE *PFile;
//const char *f = name.c_str();
//
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteFloatFile::Unable to open file!\n",
//                       0);
// }
// int day = m_TheLandscape->SupplyDayInYear();
// int year = m_TheLandscape->SupplyYear();
//
// fprintf(PFile,"%i %i %i %i \n",year,day,male_f,female_f);
// fclose(PFile);
//}
////---------------------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteReproFile(string name, int id, int young)
//{
// FILE *PFile;
//const char *f = name.c_str();
// int year = m_TheLandscape->SupplyYear();
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteReproFile::Unable to open file!\n",
//                       0);
// }
// fprintf(PFile,"%i %i %i  \n",year, id, young);
// fclose(PFile);
//}
////---------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteRandHR(string name, int total)
//{
// FILE *PFile;
//const char *f = name.c_str();
//
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteRandHR::Unable to open file!\n",
//                       0);
// }
//
// fprintf(PFile,"%i \n",total);
// fclose(PFile);
//}
////---------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteIDFile(string name, int p_ID, bool p_sex)
//{
// FILE *PFile;
//const char *f = name.c_str();
// int Sex;
// if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//     m_TheLandscape->Warn("RoeDeerPopulationManager::WriteIDFile::Unable to open file!\n",
//                       0);
// }
// if(p_sex) Sex=1;
// else Sex=0;
// fprintf(PFile,"%i %i \n",p_ID,Sex);
// fclose(PFile);
//}
////---------------------------------------------------------------------------
//void RoeDeer_Population_Manager::WriteToFixFile(Roe_Base* deer)
//{
//  FILE *PFile;
//  int year = m_TheLandscape->SupplyYear();
//  int day=m_TheLandscape->SupplyDayInYear();
//#ifndef __UNIX__
// std::string filename="Fix"+boost::lexical_cast<std::string>(year)+".txt";//**ljk May 18 2012**
//  char* f=(char *)filename.c_str();
//
//#else
//  //char *f= name;
//#endif
//  int number = deer->m_FixlistNumber; //this entrance in the list
//  if (!(PFile = fopen(f,"a+"))) {
//                //throw an error
//                m_TheLandscape->Warn("RoeDeerPopulationManager::WriteToFixFile::Unable to open file!\n",
//                       0);
//  }
//  for (unsigned k=0; k<18;k++)   //for each location
//  {
//    fprintf(PFile,"%i %i %i %i %d %d \n",year,day,deer->m_ID, k,
//                     m_FixList[number][k][0],m_FixList[number][k][1]);
//
//  }
//  fclose(PFile);
//}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::RunFirst()
{
    //TODO: Add your source code here
}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::RunBefore()
{
    //TODO: Add your source code here
}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::RunAfter()
{
    //TODO: Add your source code here
}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::RunLast()
{
    //TODO: Add your source code here
}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::CalculateArea(int p_a,int p_b,int ID)
{
/*  Calculate the area of the convex hull containing all the locations
    in m_FixList and write the result to file
    1: sort according to increasing x and yvalues
    2: find Left, Right
    3: for each point decide which are above and below the lr-line
    4: find h that gives the triangle with max area
    5: eliminate all points within the triangle
    6: calculate area of lrh and add to total area
    7: repeat for lower hull
    8: repeat recursively for the 4 (max) new subsets adding the area of
       each triangle to total
    9: write total area to file with a reference to deer
    Makes use of 3 methods; Sort(),Divide1(), Divide2()  */
    //first sort the array
    Sort (ID,p_a,p_b);   //recursive
    //now divide the points above and below the line, m_UpperList and m_LowerList
    Divide1(ID,p_a,p_b);   //initial division of m_FixList, Divide1 calls Divide2
}
//----------------------------------------------------------------------------
void RoeDeer_Population_Manager::Divide1(int ID,int p_a,int p_b)
{   //TODO need to check home range results with fixfile!!

  //create 2 new subarrays, upper starts with min and ends with max
  long u_area=0;
  long l_area=0;
  int u_c=0;
  int l_c=0;
  int u_N=0;
  int l_N=0;
  for (int i=0; i< p_b;i++) //for all points on list
  {  //Calculates twice the area, if area >= 0 then a point belongs to upper,
     //else to lower
     long area = m_FixList[ID][i][0]*(m_FixList[ID][p_a][1]-m_FixList[ID][p_b][1]) +
             m_FixList[ID][p_a][0]*(m_FixList[ID][p_b][1]-m_FixList[ID][i][1]) +
             m_FixList[ID][p_b][0]*(m_FixList[ID][i][1]- m_FixList[ID][p_a][1]);

     if (area > 0) //belongs to upper
     {
       //add to upper
       m_UpperList[i][0]=m_FixList[ID][i][0];
       m_UpperList[i][1]=m_FixList[ID][i][1];
       u_N++;
       if (area > u_area) //so far this is the largest triangle
       {
         u_area = area;
         u_c=i; //the 3rd corner in largest triangle
       }
     }
     else if(area < 0)
     {
       //add to lower
       m_LowerList[i][0]=m_FixList[ID][i][0];
       m_LowerList[i][1]=m_FixList[ID][i][1];
       l_N++;
       if (area*-1 > l_area)
       {
         l_area = area*-1;  //positive area
         l_c = i;
       }
     }
  }
  //add areas to total
  m_totalarea += (u_area+l_area)/2;
  //now call Divide2 to do the calculations on the subarrays
  Divide2(0,u_N,u_c,true);  //upper list
  Divide2(0,l_N,l_c,false);  //lower list
}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::Divide2(int p_a, int p_b, int p_c, bool p_upper)
{   //divide the 2 subarrays further, recursive function
  int u_N=-1;
  int l_N=-1;
  long l_area=0;
  long r_area=0;
  int l_c=0;
  int r_c=0;
  int l_index=-1;
  int r_index=-1;
  int l_list[72],r_list[72];
  if (p_upper==true) //do upper array
  {
     for (int i=p_a; i< p_b;i++) //for all points on list
     {
       //Calculates twice the area with respect to aci and cbi, if area >= 0
       //then a point belongs to upper, else to lower
        long area1 = m_UpperList[i][0]*(m_UpperList[p_a][1]-m_UpperList[p_c][1]) +
          m_UpperList[p_a][0]*(m_UpperList[p_c][1]-m_UpperList[i][1]) +
          m_UpperList[p_c][0]*(m_UpperList[i][1]- m_UpperList[p_a][1]);
        if (area1 > 0)
        {
          l_index++;
          l_list[l_index]=i;
          u_N++;
          if (area1 > l_area)
          {
            l_area = area1;
            l_c = i;
          }
        }
        else if (area1 < 0)
        {
           long area2 = m_UpperList[i][0]*(m_UpperList[p_c][1]-m_UpperList[p_b][1]) +
            m_UpperList[p_c][0]*(m_UpperList[p_b][1]-m_UpperList[i][1]) +
            m_UpperList[p_b][0]*(m_UpperList[i][1]- m_UpperList[p_c][1]);

           if (area2 > 0)
           {
             r_index++;
             r_list[r_index]=i;
             u_N++;
             if (area2 > r_area) //so far this is the largest triangle
             {
                r_area = area2;
                r_c=i; //the 3rd corner in largest triangle
             }
           } //ignore lower part of the set
        }
     }
  }

  else //do lower subarray
  {
    for (int i=p_a; i< p_b;i++) //for all points on list
     {
       //Calculates twice the area with respect to aci and cbi, if area >= 0
       //then a point belongs to upper, else to lower
        long area1 = m_LowerList[i][0]*(m_LowerList[p_a][1]-m_LowerList[p_c][1]) +
          m_LowerList[p_a][0]*(m_LowerList[p_c][1]-m_LowerList[i][1]) +
          m_LowerList[p_c][0]*(m_LowerList[i][1]- m_LowerList[p_a][1]);
        if (area1>0)
        {
          l_index++;
          l_list[l_index]=i;
          l_N++;
          if (area1 > l_area)
          {
            l_area = area1;
            l_c = i;
          }
        }
        else
        {
           long area2 = m_LowerList[i][0]*(m_LowerList[p_c][1]-m_LowerList[p_b][1]) +
            m_LowerList[p_c][0]*(m_LowerList[p_b][1]-m_LowerList[i][1]) +
            m_LowerList[p_b][0]*(m_LowerList[i][1]- m_LowerList[p_c][1]);

           if (area2 > 0)
           {
             r_index++;
             r_list[r_index]=i;
             if (area2 > r_area) //so far this is the largest triangle
             {
                r_area = area2;
                r_c=i; //the 3rd corner in largest triangle
             }
           }
        }
     }
  }
  //add area to total
  m_totalarea += (l_area+r_area)/2;
  //r_index and l_index now indicates whether there are any locations left
  if(l_index>0)
  {
    if (r_index>0) //call for both left and right
    {
      //points left on both sides, make a local store of a, b and c
      int a = l_list[0];
      int b = l_list[l_index];
      int c = l_c;
      Divide2(r_list[0],r_list[r_index],r_c,p_upper);
      Divide2(a,b,c,p_upper);
    }
    else  //only call for left
    {
       Divide2(l_list[0],l_list[l_index],l_c,p_upper);
    }
  }
}
//---------------------------------------------------------------------------
void RoeDeer_Population_Manager::Sort(int ID, int p_a, int p_b)
{
 //Quicksort of m_FixList by increasing x values. ID is the individual who's
 //positions we wish to sort. a and b are the first and last positions in the
 //we want sorted
  if (p_a<p_b)
  {
    int x = m_FixList[ID][(p_a+p_b)/2][0]; //middle element
    int i=p_a;  //first position to be sorted
    int j=p_b; //last
    do
    {
      while (m_FixList[ID][i][0] < x) i++;
      while (m_FixList[ID][j][0] > x) j--;
      if (i<=j)
      {
        unsigned temp = m_FixList[ID][i][0];
        m_FixList[ID][i][0] = m_FixList[ID][j][0];
        m_FixList[ID][j][0] = temp;    //swap i and j
        i++;
        j--;
      }
    } while (i<=j);
    Sort(ID,p_a,j);    //recursive call
    Sort(ID,i,p_b);
  }
}
  
//----------------------------------------------------------------------------
//-------------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::SupplyTMales - searches a specific range, given coordinates and search 
   range and returns list of male objects that are territory holders within the range.
  
*/
ListOfMales* RoeDeer_Population_Manager::
                      SupplyTMales(int p_x,int p_y,int SearchRange)
   {
      ListOfMales* LoTM;
      // Create a new list
      LoTM=new ListOfMales;
      Roe_Male * male_ptr;
      // For each male on the global list
      for (unsigned j=0; j<TheArray[1].size(); j++)  // '1' == males
      {
        // convert from TAnimal* to Roe_Male*
        male_ptr=dynamic_cast< Roe_Male * > (TheArray[1][j]);
        {
          // Check if it is in the square and has a territory
          int x=male_ptr->Supply_m_Location_x();
          int y=male_ptr->Supply_m_Location_y();
          if (male_ptr->SupplyHaveTerritory())
          {
			  if ((InSquare(x, y, p_x, p_y, SearchRange)) && (male_ptr->GetCurrentStateNo() != -1))
            {
              // It is so add it to the list of males
              LoTM->push_back(male_ptr);
            }
          }
        }
      }
      // return the list
      return LoTM;
   }
//---------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::SupplyMaleRC - searches a specific range, given coordinates and search 
   range and returns list of male objects within the range.
   */
//should return array of all Males/TMales/Females in area
   ListOfMales* RoeDeer_Population_Manager::
                               SupplyMales (int p_x,int p_y, int SearchRange)
   {
      ListOfMales* LoM;
      // Create a new list
      LoM=new ListOfMales;
      Roe_Male * male_ptr;
      // For each male on the global list
      for (unsigned j=0; j<TheArray[1].size(); j++)  // '1' == males
      {
        // convert from TAnimal* to Roe_Male*
        male_ptr=dynamic_cast< Roe_Male * > (TheArray[1][j]);
        // Check if it is in the square and has a territory
        int x=male_ptr->Supply_m_Location_x();
        int y=male_ptr->Supply_m_Location_y();
		if ((InSquare(x, y, p_x, p_y, SearchRange)) && (male_ptr->GetCurrentStateNo() != -1))
        {
          // It is so add it to the list of males
          LoM->push_back(male_ptr);
        }
      }
      // return the list
      return LoM;
      // NB Calling function has the responsibility of deleting the list after use!
   }
//------------------------------------------------------------------------------
   /**
   RoeDeer_Population_Manager::SupplyMaleRC - searches a specific range, given coordinates and search 
   range and returns list of male objects with established ranges within the range.

   */
 ListOfMales* RoeDeer_Population_Manager::
                                SupplyMaleRC (int p_x,int p_y, int SearchRange)
   {
      ListOfMales* LoM;
      // Create a new list
      LoM=new ListOfMales;
      Roe_Male * male_ptr;
      // For each male on the global list
      for (unsigned j=0; j<TheArray[1].size(); j++)  // '1' == males
      {
        // convert from TAnimal* to Roe_Male*
        male_ptr=dynamic_cast< Roe_Male * > (TheArray[1][j]);
        // Check if its range centre is in the square
        RoeDeerInfo range=male_ptr->SupplyInfo();
        int x=range.m_Range_x;  
        int y=range.m_Range_y;
		if ((InSquare(x, y, p_x, p_y, SearchRange)) && (male_ptr->GetCurrentStateNo() != -1))
        {
          // It is so add it to the list of males
          LoM->push_back(male_ptr);
        }
      }
      // return the list
      return LoM;
      // NB Calling function has the responsibility of deleting the list after use!
   }
//-----------------------------------------------------------------------------
 /**
   RoeDeer_Population_Manager::SupplyFemales - searches a specific range, given coordinates and search 
   range and returns list of female objects within the range.
   */
ListOfFemales* RoeDeer_Population_Manager::
                                SupplyFemales(int p_x,int p_y,int SearchRange)
{
      ListOfFemales* LoF;
      LoF=new ListOfFemales;
      Roe_Female * female_ptr;
      // For each female on the global list
      for (unsigned j=0; j<TheArray[2].size(); j++)  // '2' == females
      {
         // convert from TAnimal* to Roe_Female*
         female_ptr=dynamic_cast< Roe_Female * > (TheArray[2][j]);
          // Check if it is in the square
          int x=female_ptr->Supply_m_Location_x();
          int y=female_ptr->Supply_m_Location_y();
		  if ((InSquare(x, y, p_x, p_y, SearchRange)) && (female_ptr->GetCurrentStateNo() != -1))
          {
            // It is so add it to the list of females
            LoF->push_back(female_ptr);
          }
      }
      return LoF;
}

//-----------------------------------------------------------------------------
/**
   RoeDeer_Population_Manager::SupplyFemaleRC - searches a specific range, given coordinates and search 
   range and returns list of female objects that have established ranges within the range.
   */
ListOfFemales* RoeDeer_Population_Manager::
                          SupplyFemaleRC(int p_x,int p_y,int SearchRange)
{   //returns the females already established with a range within the square in
    //question. Uses m_OldRange
      ListOfFemales* LoF;
      LoF=new ListOfFemales;
      Roe_Female * female_ptr;
      // For each female on the global list
      for (unsigned j=0; j<TheArray[2].size(); j++)  // '2' == females
      {
         // convert from TAnimal* to Roe_Female*
         female_ptr=dynamic_cast< Roe_Female * > (TheArray[2][j]);
          // Check if its range centre is in the square
          RoeDeerInfo range=female_ptr->SupplyInfo();
          int x=range.m_OldRange_x;
          int y=range.m_OldRange_y;
		  if ((InSquare(x, y, p_x, p_y, SearchRange)) && (female_ptr->GetCurrentStateNo() != -1))
          {
            // It is so add it to the list of females
            LoF->push_back(female_ptr);
          }
      }
      return LoF;
}
//----------------------------------------------------------------------------
void RoeDeer_Population_Manager::CreateObjects(int ob_type, TAnimal *pvo,void* null ,Deer_struct * data,int number)
{
  Roe_Fawn *New_Fawn;
  Roe_Male *New_Male;
  Roe_Female *New_Female;
  Roe_Female* mum;
  for (int i=0; i<number; i++)
  {
    Turnover++;

    if (ob_type == 0)
    {
       New_Fawn = new Roe_Fawn(data->x, data->y,data->size,data->group,data->sex,data-> L,this);
       TheArray[ob_type].push_back(New_Fawn);
       mum=dynamic_cast <Roe_Female*> (pvo);
       //Mum handles the adding to lists
       New_Fawn->Mum = mum;
       New_Fawn->m_ID=Turnover;  //fawns always need a new ID
       New_Fawn->Mum->AddFawnToList(New_Fawn);
       //WriteIDFile("Sex.txt", New_Fawn->m_ID, New_Fawn->m_Sex);
    }
    if (ob_type == 1)
    {
       // Male
       New_Male = new Roe_Male(data->x, data->y,data->size,data->age,data->mum,data-> L,this);
       TheArray[ob_type].push_back(New_Male);
       if (data->ID!=-1) //maturing fawn
       {
          Turnover--;
          New_Male->m_ID=data->ID;
       }
       else New_Male->m_ID = Turnover; //new individual
       New_Male->m_FixlistNumber = GetFixList();  //all adults need a place to store their fixes
    }
    if (ob_type == 2)
    {
// for posterity   data=dynamic_cast<AdultDeer_struct *>(data);
       // Female
       New_Female = new Roe_Female(data->x, data->y,data->size,data->age,data->group,data->mum,data-> L,this);
       TheArray[ob_type].push_back(New_Female);
       if (data->ID!=-1) //maturing fawn
       {
          Turnover--;
          New_Female->m_ID=data->ID;
          //debug
       }
       else New_Female->m_ID = Turnover;
       New_Female->m_FixlistNumber = GetFixList();  //all adults need a place to store their fixes
    }
  }
}
//---------------------------------------------------------------------------
int RoeDeer_Population_Manager::GetFixList()
{
  //runs through the Fixlist and returns a unique number to each adult animal
  bool found=false;
  int value =-1;
  for(int i=0;i<MaxNoDeer;i++)
  {
    if(m_FixCounter[i] == -1)  //vacant list
    {
      found=true;
      m_FixCounter[i] = 0; // "0" means occupied
      value = i;
      break;
    }
  }
  if(!found)  //no vacant list found...error
  {
    m_TheLandscape ->Warn("Roe_Population_Manager:GetFixList():List full!","");
    exit( 1 );
  }
  return value;
}
//-----------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::RemoveFixList - removes fixList of a particular deer object.
*/
void RoeDeer_Population_Manager::RemoveFixList(int listno)
{

  if(listno > MaxNoDeer)  //invalid listref......error
  {
    m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFixList():Invalid list number!","");
    exit( 1 );
  }

  m_FixCounter[listno] = -1;
  for(int i=0;i<20;i++)
  {
    m_FixList[listno][i][0] = 0;
    m_FixList[listno][i][1] = 0;
  }
}
//-----------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::CreateNewGroup - creates a new group and adds the calling 
deer to that group. 

*/
int RoeDeer_Population_Manager::CreateNewGroup(Roe_Base* p_deer)
{
  #ifdef JUJ__Debug3
  if(p_deer->IsAlive()!=0x0DEADC0DE)
  {
    m_TheLandscape ->Warn("Roe_Population_Manager:CreateNewGroup():Deadcode warning,p_deer!","");
    exit( 1 );
  }
  #endif
  // Creates a new group and adds the calling deer to that group
  //find a vacant group number
  int number=-1;
  for(int i=0;i<MaxNoGroups;i++)
  {
    if(m_GroupList[i]==NULL)
    {
      number=i;
      break;
    }
  }
  m_GroupList[number]=new ListOfDeer;
  // add the first group member
  m_GroupList[number]->push_back(p_deer);
  // Record the number of groups now
  m_NoGroups++;
  return number;

}
//---------------------------------------------------------------------------
bool RoeDeer_Population_Manager::AddToGroup(Roe_Base* p_deer, int TheGroup)
{
  // CJT Added
  if (TheGroup==-1) return false; // CJT return value is not used
  #ifdef JUJ__Debug1
  if(p_deer==NULL)
  {
    m_TheLandscape ->Warn("Roe_Population_Manager:AddToGroup(): NULL pointer,p_deer!","");
    exit( 1 );
  }
  #endif
  #ifdef JUJ__Debug3
  if(p_deer->IsAlive()!=0x0DEADC0DE)
  {
    m_TheLandscape ->Warn("Roe_Population_Manager:AddToGroup():Deadcode warning,p_deer!","");
    exit( 1 );
  }
  #endif

    if(m_GroupList[TheGroup]==NULL)
    {
      m_TheLandscape ->Warn("Roe_Population_Manager:AddToGroup(): NULL pointer,m_GroupList[TheGroup]!","");
      exit( 1 );
    }
    // record the new member
    m_GroupList[TheGroup]->push_back(p_deer);
    return true;
}
//---------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::RemoveFromGroup - removes deer object from a group by erasing deer from
list - if this causes group to have zero members, the entire group list will be deleted.
*/
bool RoeDeer_Population_Manager::RemoveFromGroup(Roe_Base* p_deer,int TheGroup)
{
  if(TheGroup != -1)
  {
    #ifdef JUJ__Debug1
    if(p_deer==NULL)
    {
      m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFromGroup():NULL pointer,p_deer!","");
      exit( 1 );
    }
    if(m_GroupList[TheGroup]==NULL)
    {
      m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFromGroup():NULL pointer,m_GroupList[TheGroup]!","");
      exit( 1 );
    }
    #endif
    #ifdef JUJ__Debug3
    if(p_deer->IsAlive()!=0x0DEADC0DE)
    {
      m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFromGroup():Deadcode warning,p_deer!","");
      exit( 1 );
    }
    #endif
    #ifdef JUJ__Debug2
    if (TheGroup>4999)
    {
      m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFromGroup():Variable out of range,TheGroup","");
      exit( 1 );
    }
    #endif
    int found=-1;
    int size= (int) m_GroupList[TheGroup]->size();
    for(int i=0;i<size;i++)
    {
      if(p_deer==(*m_GroupList[TheGroup])[i])
      {
        found=i;
        break;
      }
    }
    if(found!=-1)
    {
      // find and erase that deer
      m_GroupList[TheGroup]->erase(m_GroupList[TheGroup]->begin()+found);
    }
    //if group is now empty delete this group
    if(m_GroupList[TheGroup]->size()==0)
    {
      delete m_GroupList[TheGroup];
      m_GroupList[TheGroup]=NULL;
      m_NoGroups--;
    }
  }
  return true;
}
//----------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::RemoveFromFightlist - removes male object from fight list.
*/
void RoeDeer_Population_Manager::RemoveFromFightlist(Roe_Base* deer)
{
  #ifdef JUJ__Debug3
  if(deer->IsAlive()!=0x0DEADC0DE)
  {
    m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFromFightList():Deadcode warning,deer!","");
    exit( 1 );
  }
  #endif
  //remove this male from everybodys fightlist
  for (unsigned i=0;i<5000;i++)
  {
    if (m_Fightlist[i]!=NULL)
    {
      ListOfMales* fightlist = m_Fightlist[i];
      for (unsigned j=0;j<fightlist->size();j++)  //every male in every list
      {
        #ifdef JUJ__Debug3
        Roe_Male* male=(*fightlist)[j];
        if(male->IsAlive()!=0x0DEADC0DE)
        {
          m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFromFightList():Deadcode warning,male!","");
          exit( 1 );
        }
        #endif
        if((*fightlist)[j]==deer)
        {
          m_Fightlist[i]->erase(m_Fightlist[i]->begin()+j);
        }
      }
    }
  }
}
//----------------------------------------------------------------------------
/**
RoeDeer_Population_Manager::AddToFightlist - adds rival males to fightlist or creates a fightlist
if a male object does not have one. Also checks if rival is on list already.
*/
int RoeDeer_Population_Manager::AddToFightlist(Roe_Male* rival, int ListNo)
{
  #ifdef JUJ__Debug3
  if(rival->IsAlive()!=0x0DEADC0DE)
  {
    m_TheLandscape ->Warn("Roe_Population_Manager:AddToFightList():Deadcode warning,rival!","");
    exit( 1 );
  }
  #endif
  bool found=false;
  ListOfMales* fightlist = m_Fightlist[ListNo];
  
  if (ListNo==-1)  //need to create new list
  {
    for(int i=0;i<5000;i++)
    {
      if (m_Fightlist[i]==NULL)
      {
        // Creates a new list
        m_Fightlist[i]=new ListOfMales;
        // add the rival to the list
        m_Fightlist[i]->push_back(rival);
        ListNo=i;  //the number of this list
        found=true;
        break;
      }
    }
    if(found==false)
    {
      m_TheLandscape ->Warn("Roe_Population_Manager:RemoveFromFightList():List not found!","");
      exit( 1 );
    }
  }
  else  //a list exists already
  {

    //check if rival is on fightlist already
    for(unsigned i=0;i<fightlist->size();i++)
    {
      if((*fightlist)[i]==rival)
      {
        found=true;
        break;
      }
    }
    if(!found)
    {
      // record the new member
      m_Fightlist[ListNo]->push_back(rival);
    }
  }
  return ListNo;
}
//-----------------------------------------------------------------------------

bool RoeDeer_Population_Manager::InSquare(int p_x, int p_y,int p_sqx,
                                               int p_sqy, int p_range)
{
    // are p_x & p_y in the square p_sqx,p_sqy,p_range?
    int x_extent = p_sqx+p_range;
    int y_extent = p_sqy+p_range;
    if (x_extent >= SimW)
    {
      if (y_extent >= SimH)  // overlaps TR corner of sim area
      {
        // Top right square (limited by SimAreaHeight & SimAreaWidth
        if ((p_x >=p_sqx) && (p_y >=p_sqy)) return true;
        // Top Left Square (limited by 0,SimAreaHeight)
        if ((p_y >=p_sqy)&& (p_y<y_extent-SimH)) return true;
        // Bottom Left square (limited by 0,0)
        if ((p_x >=x_extent-SimW)&&(p_y<y_extent-SimH)) return true;
        // Bottom Right square (limited by SimAreaWidth,0)
        if ((p_x >=p_sqx)&& (p_y<y_extent-SimH)) return true;

      }
      else // Overlaps the western edge of the sim area
      {
        if ((p_x >=p_sqx) && (p_y >=p_sqy)&& (p_y<y_extent)) return true;
        if ((p_x <x_extent-SimW) && (p_y >=p_sqy)
                                              && (p_y<y_extent)) return true;
      }
    }
    else
    {
      if (y_extent >= SimH) // overlaps top of simulation area
      {
        if ((p_x >=p_sqx) && (p_x<x_extent) &&
                                           (p_y >=p_sqy)) return true;
        if ((p_x >=p_sqx) && (p_x<x_extent) &&
                                   (p_y<y_extent-SimH)) return true;
      }
      else // territory does not overlap end of simulation area
      {
        if ((p_x >=p_sqx) && (p_x<x_extent) &&
                         (p_y >=p_sqy) && (p_y<y_extent)) return true;
      }
    }
    return false; // not in square
}
//------------------------------------------------------------------------------
void RoeDeer_Population_Manager::DeleteFightlist(int TheList)
{
  if(m_Fightlist[TheList] !=NULL)
  {
    delete m_Fightlist[TheList];  //delete the list
    m_Fightlist[TheList]=NULL;
  }
}
//-----------------------------------------------------------------------------


/**
An output designed to be used as an input to calculating Ripley's k in R.\n
*/
void RoeDeer_Population_Manager::TheRipleysOutputProbe( FILE* a_prb ) { //ljk added 22/8-2012
  Roe_Female* FS;
  unsigned totalF=(unsigned) TheArray[2].size();
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  FS=dynamic_cast<Roe_Female*>(TheArray[2][j]);
	  x=FS->Supply_m_Location_x();
	  y=FS->Supply_m_Location_y();
	  fprintf(a_prb,"%d\t%d\n", x,y);
  }
  fflush(a_prb);
}
//-------------
void RoeDeer_Population_Manager::TheAOROutputProbe() {
	m_AOR_Probe->DoProbe(2);
}


