//
// roe_adult.cpp
//


#include <iostream>
#include <fstream>
#pragma hdrstop

#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"


#include "Roe_all.h"
#include "Roe_constants.h"

extern int FastModulus( int a_val, int a_modulus );


//---------------------------------------------------------------------------
//                              Roe_Adult_Base
//---------------------------------------------------------------------------
void Roe_Adult_Base::BeginStep (void)
{
 if (m_CurrentStateNo==-1) return;
}
//---------------------------------------------------------------------------
void Roe_Adult_Base::Step (void)
{
 if (m_StepDone || (m_CurrentStateNo==-1)) return;
}
//---------------------------------------------------------------------------
void Roe_Adult_Base::EndStep (void)
{
 if (m_CurrentStateNo==-1) return;
}
//---------------------------------------------------------------------------

//--------------------------------------------------------------------------
Roe_Adult_Base::Roe_Adult_Base (int x, int y, int size,int age,Landscape* L,
                      RoeDeer_Population_Manager* RPM) : Roe_Base (x, y, L,RPM)
{
  m_DispCount=0;
}
//---------------------------------------------------------------------------
Roe_Adult_Base::~Roe_Adult_Base ()
{
}

