//
// roe_female.cpp
//

#include <iostream>
#include <fstream>
#pragma hdrstop

#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"


#include "Roe_all.h"
#include "Roe_constants.h"


extern int FastModulus( int a_val, int a_modulus );

//---------------------------------------------------------------------------
//                              Roe_Female
//---------------------------------------------------------------------------

/**
Roe_Female::FOnMature - Checks if area is good enough to establish range. The function ScanGrid()
adds the quality values stored in m_grid starting with a minimum area of
400 x 400 m. If not sufficient it extends outwards in all directions
until max range is reached (800 x 800 m). It also checks if amount of other females nearby is below
a female density threshold. It returns the size of the resulting area if good enough and 0 if not 
good enough.
Calls ScanGrid(), SupplyFemaleRC().
*/
int Roe_Female::FOnMature(void)   //State 1
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FOnMature(): Deadcode warning","");
     exit( 1 );
  }
  #endif
    //Check whether this area is good enough. The function ScanGrid()
    //adds the quality values stored in m_grid starting with a minimum area of
    //400 x 400 m. If not sufficient it extends outwards in all directions
    //until max range is reached (800 x 800 m). It returns the size of the
    //resulting area if good enough and 0 if not good enough

      int range = m_OurPopulation->ScanGrid(m_Location_x,m_Location_y,false);
      if (range > 0) //good enough
      {
        //how many other females already in area?
        ListOfFemales* females= m_OurPopulation->
                                SupplyFemaleRC(m_Location_x,m_Location_y,range);
        if(females->size() < FemaleDensityThreshold)  //OK
        {
          m_SearchRange = range;
          delete females;
          return 7; // establish range
        }
        delete females;
      }
      return 10;  //no options in this area, so disperse
}
//---------------------------------------------------------------------------
/**
Roe_Female::FEstablishRange - sets up range for female object. Sets object to not be a disperser
or to have completed dispersal. Checks if object has a group and if not, assign it to a group.
Makes sure that range centre is not too close to road or houses and sets a range center.
When new range is set, calculate optimal group size in range based on % forest
Always returns state feed. 
Calls functions CreateNewGroup(), SupplyElementType(), DistanceTo(), PercentForest().
*/
int Roe_Female::FEstablishRange()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FEstablishRange(): Deadcode warning","");
     exit( 1 );
  }
  #endif

    m_MinInState[11]+=1; //add 1 to time spend in this state
    m_HaveRange=true;
    m_Disperse=false;   //either not disperser or dispersal is completed
    m_float=false;
    //this animal is now established, so check if it has a group, else give it one
    if(m_MyGroup == -1)
    {
      m_MyGroup = m_OurPopulation->CreateNewGroup(this);
    }
    //make sure that range centre is not too close to road or houses
    int t[8];
    TTypesOfLandscapeElement q[8];
    int dir=random(8);   //start looking in any direction
    int nogood[8]= {0};
    bool move=false;
    int Dist[8]={0};
    int x=m_Location_x;
    int y=m_Location_y;

    if((x <= 200)||(x >= SimW-200)||(y <= 200)||(y >= SimH-200)) //next step could take you out
    {
      //need to correct coordinates
      x=m_Location_y+SimW;
      y=m_Location_y+SimH;
      for (int i=200; i>=0; i-=2)   //every 2 meter closing in
      {
        t[0]=dir;
        t[0]=t[0] & 7;
        t[1]=dir+1;
        t[1]=t[1] & 7;
        t[2]=dir-1;
        t[2]=(t[2]+8) & 7;
        t[3]=dir+2;
        t[3]=(t[3]+8) & 7;
        t[4]=dir-2;
        t[4]=(t[4]+8) & 7;
        t[5]=dir+3;
        t[5]=(t[5]+8) & 7;
        t[6]=dir-3;
        t[6]=(t[6]+8) & 7;
        t[7]=dir+4;
        t[7]=(t[7]+8) & 7;

        q[0]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[0]]*i), y+(Vector_y[t[0]]*i));
        q[1]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[1]]*i), y+(Vector_y[t[1]]*i));
        q[2]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[2]]*i), y+(Vector_y[t[2]]*i));
        q[3]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[3]]*i), y+(Vector_y[t[3]]*i));
        q[4]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[4]]*i), y+(Vector_y[t[4]]*i));
        q[5]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[5]]*i), y+(Vector_y[t[5]]*i));
        q[6]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[6]]*i), y+(Vector_y[t[6]]*i));
        q[7]=m_OurLandscape->
	  SupplyElementTypeCC( x+(Vector_x[t[7]]*i), y+(Vector_y[t[7]]*i));

        for(int j=0; j<8;j++)
        {
          if ((q[j]>tole_Track)&&(q[j]<tole_ActivePit) ||
	      (q[j]==tole_Railway)                     ||
	      (q[j]==tole_Garden))
          {
            //too close in this direction
            nogood[j]=1;
            move=true;
            Dist[j]=DistanceToCC (m_Location_x, m_Location_y,
				  x+(Vector_x[t[j]]*i),
				  y+(Vector_y[t[j]]*i));
          }
        }
      }
      if (move==true)  //need to adjust range centre
      {
        int thisway=0;
        int closest=0;
        int furthest=0;
        for(int i=0;i<8;i++)
        {
          if(Dist[i] < closest)  closest=Dist[i];
          if (Dist[i]> furthest)
          {
            furthest=Dist[i];
            thisway=i;
          }
        }

        if (closest<furthest/2)  //move range centre halfway along furthest
        {
          for (int i=furthest/2;i<200;i++)
          {
	    int xi = x + Vector_x[t[thisway]]*i;
	    int yi = y + Vector_y[t[thisway]]*i;
	    m_OurLandscape->CorrectCoords( xi, yi );
            int quali=LegalHabitatCC( xi, yi);
            if (quali>0)
            {
               m_RangeCentre_x=m_OurLandscape->
		 CorrectWidth( m_Location_x+(Vector_x[t[thisway]]*i) );
               m_RangeCentre_y=m_OurLandscape->
		 CorrectHeight( m_Location_y+(Vector_y[t[thisway]]*i) );
            }
          }
        }
      }
      //nothing gained by moving or no reason to move, so stay
      m_RangeCentre_x=m_Location_x;
      m_RangeCentre_y=m_Location_y;
    }
    else //no need to correct coordinates
    {
      int x=m_Location_x;
      int y=m_Location_y;
      for (int i=200; i>=0; i-=2)   //every 2 meter closing in
      {
        t[0]=dir;
        t[0]=t[0] & 7;
        t[1]=dir+1;
        t[1]=t[1] & 7;
        t[2]=dir-1;
        t[2]=(t[2]+8) & 7;
        t[3]=dir+2;
        t[3]=(t[3]+8) & 7;
        t[4]=dir-2;
        t[4]=(t[4]+8) & 7;
        t[5]=dir+3;
        t[5]=(t[5]+8) & 7;
        t[6]=dir-3;
        t[6]=(t[6]+8) & 7;
        t[7]=dir+4;
        t[7]=(t[7]+8) & 7;

        q[0]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[0]]*i)),
                                                  (y+(Vector_y[t[0]]*i)));
        q[1]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[1]]*i)),
                                                  (y+(Vector_y[t[1]]*i)));
        q[2]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[2]]*i)),
                                                  (y+(Vector_y[t[2]]*i)));
        q[3]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[3]]*i)),
                                                  (y+(Vector_y[t[3]]*i)));
        q[4]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[4]]*i)),
                                                 (y+(Vector_y[t[4]]*i)));
        q[5]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[5]]*i)),
                                                    (y+(Vector_y[t[5]]*i)));
        q[6]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[6]]*i)),
                                                   (y+(Vector_y[t[6]]*i)));
        q[7]=m_OurLandscape->SupplyElementType((x+(Vector_x[t[7]]*i)),
                                                 (y+(Vector_y[t[7]]*i)));
        for(int j=0; j<8;j++)
        {
          if ((q[j]>tole_Track)&&(q[j]<tole_ActivePit)||(q[j]==tole_Railway)
                                                         ||(q[j]==tole_Garden))
          {
            //too close in this direction
            nogood[j]=1;
            move=true;
            Dist[j]=DistanceTo(m_Location_x,m_Location_y,(x+(Vector_x[t[j]]*i)),
                                                 (y+(Vector_y[t[j]]*i)));
          }
        }
      }
      if (move==true)  //need to adjust range centre
      {
        int thisway=0;
        int closest=0;
        int furthest=0;
        for(int i=0;i<8;i++)
        {
          if(Dist[i] < closest)  closest=Dist[i];
          if (Dist[i]> furthest)
          {
            furthest=Dist[i];
            thisway=i;
          }
        }

        if (closest<furthest/2)  //move range centre halfway along furthest
        {
          for (int i=furthest/2;i<200;i++)
          {
            int quali=LegalHabitat((x+(Vector_x[t[thisway]]*i)),
                                           (y+(Vector_y[t[thisway]]*i)));
            if (quali>0)
            {
               m_RangeCentre_x=m_Location_x+(Vector_x[t[thisway]]*i);
               m_RangeCentre_y=m_Location_y+(Vector_y[t[thisway]]*i);
            }
          }
        }
      }
      //nothing gained by moving or no reason to move, so stay
      m_RangeCentre_x=m_Location_x;
      m_RangeCentre_y=m_Location_y;
      m_OldRange_x=m_Location_x; //OldRange used to store range while in group
      m_OldRange_y=m_Location_y;
    }

   //new range is set, so calculate optimal group size in your range
   //first step is to get % forest
    double forest=m_OurPopulation->PercentForest(m_RangeCentre_x,m_RangeCentre_y,m_SearchRange);
    if(forest<4) m_OptGroupSize=10; //max group size
    else m_OptGroupSize= (int)(17.1*(pow(forest,-0.49))); //relation between % forest
   //and observed group sizes.
   m_DispCount=0;
   return 35; //always to feed after this
}
//--------------------------------------------------------------------------

/**
Roe_Female::MDisperse - models dispersal state of female objects. Sets state to 
dispersal. It then keeps track of amount of time in the dispersal
state and calculate probability of switching to feeding, keep dispersing, or dying if spending too long
in dispersal state.Calls functions DirectionTo() and NextStep(). 
*/
int Roe_Female::FDisperse(void)  //disperse
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FDisperse(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  m_DispCount++;
  m_MinInState[0]+=1; //add 1 to time spend in this state
  m_HaveRange=false;  //a dispersing animal cannot be a rangeholder per def.
  m_Disperse=true;  //indicates that dispersal has started
  //allow roe the opportunity to return to this state after disturbance
  m_LastState=10;
  if(m_DispCount==1)  //first timestep in disperse
  {
    //store range centre coordinates while dispersing
    m_OldRange_x=m_RangeCentre_x;
    m_OldRange_y=m_RangeCentre_y;
  }

  m_RangeCentre_x=m_Location_x; //these are drifting depending on roes position
  m_RangeCentre_y=m_Location_y;
  //int dist=DistanceTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
  int dir= DirectionTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
  int rand=0;

   if(m_DispCount>70)
  {
    for(int i=0;i<3;i++)
    {
      if( NextStep(0,0,0) == -1) return 41;
    }
  }
  else
  {
    for(int i=0;i<3;i++)
    {
      if( NextStep(1,((dir+4) & 0x07),0) == -1) return 41;
    }
  }

  switch (m_DispCount)
  {
    case 40:
      rand=40;
      break;
    case 50:
      rand=60;
      break;
    case 60:
      rand=80;
      break;
    default:
      if(m_DispCount>=70) rand=90;
      break;
  }

  if (rand>random(100))   //go to feed
  {
    return 35;
  }
  else return 10;   //stay in disperse
}
//--------------------------------------------------------------------------
/**
Roe_Female::FFeed - Models feeding of female object, including energy budget and search for better feeding 
area. Also determines need to ruminate. In theory a roe deer can spend time 
in more than 1 type of habitat within 1 timestep. For the energy budget only the habitat 
type where roe deer is at the beginning of each time step is recorded. Need to keep counter of minutes
in feed since the end of last ruminating period (m_FeedCount) to know when to return to ruminate. Returns values
for dispersal, ruminate or keep feeding (or die).
Calls function NutriValue()
*/
int Roe_Female::FFeed()     //State 35
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FFeed(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  //In theory a roe can spend time in more than 1 type of habitat within 1 timestep.
  //For the energy budget only the habitat type where roe is at the beginning of
  //each time step is recorded. m_AmFeeding is set to true on 2nd feeding step
  //(i.e. whenever roe decides to stay in feed). Need to keep counter of minuts
  //in feed since the end of last ruminating period (m_FeedCount) to know when to
  //return to ruminate
  m_MinInState[9]+=1; //add 1 to time spend in this state
  m_FeedCount++;   //add 1 to count for this feeding bout
  //allow roe the opportunity to return to this state after disturbance
  m_LastState=35;  //Feed
  //Find the nutritional value here
  int nutri=NutriValue(m_Location_x,m_Location_y);  //possible energy gain per minut
  if(nutri<MinNutri)  //not good enough for feeding
  {
    SeekNutri(MinNutri); //look for a better place first
    Feeding(m_Disperse);
  }
  else  //can feed here
  {
    m_EnergyGained+=nutri;   //add this to total count for today
    Feeding(m_Disperse);
  }
  //did roe die while taking feeding steps?
  if (m_IsDead)   //is dead
  {
    m_FeedCount=0;
    return 41;  //Die
  }
  else //still alive, so decide whereto next.
  {
    if (m_FeedCount>m_LengthFeedPeriod)   //need to ruminate
    {
      //for how long do you need to ruminate?
      int month= m_OurLandscape->SupplyMonth();
      m_LengthRuminate=(int) (m_FeedCount*IntakeRate[month-1]*0.1);
      m_FeedCount=0;   //counter to zero
      return 32; //Ruminate
    }
    else if (m_Disperse==true) //a disperser, go back to disperse with 30:70 chance
    //"Cheap" solution. No basis for setting this value. Return to this later....
    {
      int rand=random(100);
      if (rand<30)
      {
        m_FeedCount=0;
        return 10; //Disperse
      }
    }
  }
  return 35;
}

//---------------------------------------------------------------------------
/**
Roe_Female::FRun - Sets the female object to be in a running state - and checks if 
safe after 1 time step in this state.
Calls Running().
*/
int Roe_Female::FRun() //State 19
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FRun(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  m_MinInState[4]+=1; //add 1 to time spend in this state

  bool IsSafe=Running(m_danger_x,m_danger_y);
  if(m_IsDead) return 41; //Die
  else if (!IsSafe)   //still not safe after 1 time step
  {
    return 19;  //Run
  }
  else
  {
    m_RecovCount++; //add 1 to time spend in recovery
    return 22;   //recover
  }
}
//---------------------------------------------------------------------------
/**
Roe_Female::FRecover - Governs recovery after running from danger. Function counts 
amount of time in state during the present period of recovovery (m_RecovCount) and 
sets this to 0 when state is left. Female only transfers to this state if safe, whichs means
in good habitat. Returns states: feeding, recovery. 

*/
int Roe_Female::FRecover()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FRecover(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  //Need to count amount of time in state during the present
  //period of recov. (m_RecovCount) and set this to 0 when state is left.
  //Roe only transfers to this state if safe (i.e. cover>=3) so we know it is in
  //good habitat
  m_MinInState[5]+=1; //add 1 to time spend in this state

  //allow roe the opportunity to return to this state after disturbance
  m_LastState=22;
  if (m_RecovCount<=AverageRecoveryTime)
  {
    m_RecovCount++;   //add 1 to time spend in recover and stay here
    return 22;  //recover
  }
  else if ((m_RecovCount>AverageRecoveryTime)&&(m_RecovCount<MaxRecoveryTime))
  //return to feed with certain probability
  {
    int stop= random(100);
    if (stop<10)
    {
      m_RecovCount=0;    //leaves state, so set counter=0
      m_FeedCount=0;   //ruminating also takes place during recovery, so start
                       //from scratch in Feed
      return 35;
    }
    else
    {
      m_RecovCount++;   //add 1 to time spend in this state
      return 22;
    }
  }
  else   //if still around after this long return to feeding
  {
    m_RecovCount=0;
    return 35;
  }
}
//---------------------------------------------------------------------------
/**
Roe_Female::FEvade - checks direction to threath and takes 50 steps in opposite direction.
Returns to same state as before disturbance.
Calls DirectionTo(), NextStep()
*/
int Roe_Female::FEvade()  //Evade
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FEvade(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  m_MinInState[7]+=1; //add 1 to time spend in this state
  int weight=3;
  //get direction to threat
  int dir=DirectionTo(m_Location_x,m_Location_y,m_danger_x,m_danger_y);
   //take 50 steps in opposite direction
  NextStep(weight,((dir+4) & 0x07),0);
  //return to same state as before disturbance
  return m_LastState;
}
//---------------------------------------------------------------------------
/**
Roe_Female::FIgnore - Ignore threath and  1 to time spend in this state. Return to same 
state as before disturbance
*/
int Roe_Female::FIgnore()  //Ignore
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FIgnore(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  m_MinInState[6]+=1; //add 1 to time spend in this state
  //return to same state as before disturbance
  return m_LastState;
}
//---------------------------------------------------------------------------
/**
Roe_Female::FRuminate - Function for ruminating. Checks for got spot to rest and amount of time 
spent ruminating. Ruminate activity cannot be seperated from other kinds of inactivity. 
It is also assumed that ruminating takes place while recovering after disturbance. 
Need to count amount of time spend in this state during this period of ruminating and in total last 24 hrs. 
Returns values for either stay ruminating, feed or dispersal (or die).Calls function Cover().
*/
int Roe_Female::FRuminate()  //Ruminate
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FRuminate(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  //ruminate activity cannot be seperated from other kinds of inactivity.
  //It is also assumed that ruminating takes place while recovering after
  //disturbance. Need to count amount of time spend in this state during
  //this period of ruminating and in total last 24 hrs
  m_MinInState[8]+=1; //add 1 to time spend in this state
  int min;
  //First check if this is first timestep in this state
  //allow roe the opportunity to return to this state after disturbance
  m_LastState=32;
  if (m_RumiCount==0)   //first step so make sure this is a good spot to rest
  {
    int cover=Cover(m_Location_x,m_Location_y);
     //get month
    int month=m_OurLandscape->SupplyMonth();
    if((month>=5)&&(month<=9)) min=CoverThreshold2; //summer
    else min=CoverThreshold1; //winter
    if (cover<min)// habitat unsuitable or cover very poor, so look for better spot
    {
       SeekCover(2);
       if(m_IsDead) return 41; //Die
       else
       {
         m_RumiCount++;
         return 32;
       }
    }
    else //cover OK
    {
      m_RumiCount++;       //add 1 to counter
      return 32;    //return to this state next time step
    }
  }
  else if(m_RumiCount<m_LengthRuminate)
  {
    m_RumiCount++;  //add 1 to counter
    return 32;     //stay
  }
  else
  {
    if(m_Disperse==true)  return 10;
    else  //not disperser
    {
       if (m_RumiCount>=m_LengthRuminate)
       {
         m_RumiCount=0;
         return 35;     //feed
       }
       else
       {
         int rand=random(100);
         if(rand<50)
         {
           m_RumiCount++;
           return 32;   //stay
         }
         else
         {
           m_RumiCount=0;
           return 35;
         }
       }
    }
  }
}
//---------------------------------------------------------------------------
/**
Roe_Female::FCareForYoung - sets female object to be in care for young state. Care periods 
are initiated by message from fawn to mother. Fawn keeps counter of minuts care (=suckling) 
per hour. If counter drops below min value fawn initiates extra care period. If care is done
returns to feeding.
*/
int Roe_Female::FCareForYoung(void) //CareForYoung
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FCareForYoung(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  //Care periods are initiated by message from fawn to mother. Fawn
  //keeps counter of minuts care (=suckling) per hour. If counter drops below
  //min value fawn initiates extra care period.
  m_MinInState[3]+=1; //add 1 to time spend in this state
  if (m_Care==true) return 18; //stay here
  else
  {
    m_Care=false;
    return 35; //feed
  }
}
//--------------------------------------------------------------------------
/**
Roe_Female::FInHeat - sets female object to be in heat. Female object Start new day in this state 
and stays here until it has found at mate or heat is over. Female heat lasts for 3 days. The female will
get a list of all males in area and send them a message. If no males are around, the female will 
search a larger area. If find a male that will mate and is close enough to attend, male list will be deleted
and this particular male is set as the female's mate. 
Calls SupplyMales(), On_InHeat().
*/
int Roe_Female::FInHeat()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FInHeat(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  //start new day in this state and stay here until m_Have_Mate = true or heat
  //is over
  m_MinInState[1]+=1; //add 1 to time spend in this state
  //allow roe the opportunity to return to this state after disturbance
  m_LastState=16; //InHeat
  bool CanMate=false;
  ListOfMales* males;
  m_HeatCount ++; //female heat lasts for 3 days
  if(m_HeatCount%3==0) //every 30 minutes
  {
    //get a list of all Males in area and send them a message
    males = m_OurPopulation->SupplyMales(m_Location_x,m_Location_y,
                                                          2*m_SearchRange);
    if(males->size() == 0)  //no one around, so search a larger area
    {
      delete males;
      males = m_OurPopulation->SupplyMales(m_Location_x,m_Location_y,
                                                          4*m_SearchRange);
    }
    for (unsigned i=0; i<males->size(); i++)
    {
      #ifdef JUJ__Debug1
      if((*males)[i]==NULL)
      {
        m_OurLandscape ->Warn("Roe_Female::FInHeat(): (*males)[i]==NULL!","");
        exit( 1 );
      }
      #endif
      #ifdef JUJ__Debug3
      if((*males)[i]->IsAlive()!=0x0DEADC0DE)
      {
        m_OurLandscape ->Warn("Roe_Female::FEstablishRange():Deadcode warning, (*males)[i]!","");
        exit( 1 );
      }
      #endif

      //send them message
      CanMate = (*males)[i]->On_InHeat(this, m_Location_x,m_Location_y);
      if (CanMate==true) //this male will mate and is close enough to attend
      {
	m_My_Mate=(*males)[i];
        delete males;
        return 35;  //return to normal activity
      }
    }
    delete males;
  }
  if (m_HeatCount==432) //have to go back to normal activity after 3 days
  {
    m_HeatCount=0;
    return 35;
  }
  return 16;  //else stay in heat
}
//------------------------------------------------------------------------------
/**
Roe_Female::FOnNewDay - updates age of female object (+ 1 day), decides if life expectancy 
has been reached, which depends on age (Floaters have a higher mortality (added FloatMort)).
Checks whether dispersers and floaters can set up range by checking habitat and closeness to 
mothers range and amount of males already in the area.
If failed to find a range before october 1st become a floater. Also governs whether to dissolve or stay in
female group - dissolvement dependent on date. If dissolved group, sends female back to old range.
Returns values to set the following states: 
establish range, Form group, update energy (and die).
Calls functions DistanceTo(), ScanGrid(), SupplyFemaleRC(), RemoveFromGroup().
*/
int Roe_Female::FOnNewDay()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FOnNewDay(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  m_Age++;  //1 day older
  m_DestinationDecided=false;  //don't know yet whereto after this
  m_NewState=0;
  //First decide if life expectancy has been reached. Depends on age. Floaters
  //have a higher mortality than other (added FloatMort)
  int mort;
  int dist;
  long int die=random(100000);  //since mort rates are multiplied with 100000 to
                                //produce int values
  if (m_Age<=730)
    mort=(int)FemaleDMR[0]; //age in days
  else if (m_Age<=2555)   //7 yrs
    mort=(int)FemaleDMR[1];
  else if (m_Age<=3650)  //10 yrs
    mort=(int)FemaleDMR[2];
  else mort =(int)FemaleDMR[3]; //>10 yrs

  if((m_float)||(m_Disperse)) mort+=FloatMort;

  if (die<mort)
  {
    return 41;  //Die
  }
  else   //still alive
  {
    //RANGES
    if (m_Disperse) //disperser that needs a range
    {
      if(!m_float)
      {
        //dispersers check dist back to mothers RangeCentre
        dist=DistanceTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
      }
      else
      { //floaters check dist back to last attempt to set up range
        dist=DistanceTo(m_Location_x,m_Location_y,m_float_x,m_float_y);
      }
      if(dist>MinDispDistance)
      {
        //both floaters and regular dispersers go in here
        bool found = false;
        int range = m_OurPopulation->ScanGrid(m_Location_x,m_Location_y,false);
        if(range > 0)   //OK
        {
          //how many other females have already their range in this area?
          ListOfFemales* females = m_OurPopulation->
                                SupplyFemaleRC(m_Location_x,m_Location_y,range);

          if(females->size() < FemaleDensityThreshold)   //OK
          {
             //OK to settle here, so return to EstablishRange after this
              found=true;
              m_SearchRange=range;
              m_DestinationDecided=true;
              m_NewState=7;        //establishrange
          }
          delete females;
        }
        if (!found)  //quality too poor or still too close to mother, so return to disperse
        {
          //if failed to find a range before Sept. 1st become a floater
          if(m_OurLandscape->SupplyDayInYear() >= 242)
          { //floaters store present location for use in next attempt to set up range
            m_float=true;
            m_float_x=m_Location_x;
            m_float_y=m_Location_y;
            //remove this deer from its group
            m_OurPopulation->RemoveFromGroup(this,m_MyGroup);
            m_MyGroup=-1;
          }
          m_NewState=10;   //disperse
          m_DestinationDecided=true;
        }
      }
    }

    //GROUPS
    if(!m_float)
    {
      //get day
      int day=m_OurLandscape->SupplyDayInYear();
      if ((day==120)&&(m_GroupUpdated==true))  //time to dissolve groups
      {
        Send_EndGroup();
        m_GroupUpdated=false;
        ListOfFemales* females = m_OurPopulation->
                SupplyFemaleRC(m_OldRange_x, m_OldRange_y,m_SearchRange);
        delete females;
        m_RangeCentre_x=m_OldRange_x;  //go back to old rangecentre
        m_RangeCentre_y=m_OldRange_y;
      }
      if ((day == 274)&&(m_GroupUpdated == false)) //after Oct. 1st and still not in group
      {
        return 15;   //FormGroup
      }
    }
  }
  return 4; //update energy
}
//---------------------------------------------------------------------------
/**
Roe_Female::FUpdateEnergy - Calculates how much energy is used (in cal/minute) in different activities.
Calculates net energy (gained-used) and determines whether excess enery is added to reserves or is used to
put on weight. A negative net energy causes the female object to loose weight - if roe deer weight falls
below a minimum value, the deer will die. If the roe deer is in very bad condition, the female may abandon
fawns and increase forage time.
returns states such as feed, update gestation or die.

*/
int Roe_Female::FUpdateEnergy(void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FUpdateEnergy(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  //All energetic calculations in cal/timestep
  //find resting metabolic rate
  float RMR=FemaleRMR[0];
  float EnergyUsed=0;
  int month=m_OurLandscape->SupplyMonth();
  //FemaleRMR depends on reproductive state, so find out if pregnant
  if (m_Pregnant==true)
  {
    if (m_Gestationdays >=240)   RMR=FemaleRMR[1];  //late pregnancy
  }
  else if ((m_DaysSinceParturition > 0)&&(m_DaysSinceParturition <= 120))  //young fawn
  {
    if (m_DaysSinceParturition <=15) RMR=FemaleRMR[2];
      else if (m_DaysSinceParturition <=30)  RMR=FemaleRMR[3];
        else if (m_DaysSinceParturition <=60)  RMR=FemaleRMR[4];
          else if (m_DaysSinceParturition <=90)  RMR=FemaleRMR[5];
            else RMR=FemaleRMR[6];
  }
  if(RMR!=FemaleRMR[0])  //in a period of elevated RMR, so add the difference
  {
    EnergyUsed += 144 * (RMR-FemaleRMR[0]) * (m_Size*0.001f); // 144?? where does this come from - ljk 02/08-2012
  }
  //Calculate how much energy used in different activities
  for (int i=0; i<12; i++)
  {
    float cost;
    if (CostFemaleAct[i]==1)   cost=CostFemaleAct[i]*RMR;
    else cost= (float) CostFemaleAct[i];
    EnergyUsed += (cost * m_MinInState[i] * (m_Size*0.001f));
  }
  double result = m_EnergyGained - EnergyUsed;     //in cal
  if (result>0)
  {
    if (m_Reserves < 10)  m_Reserves++;
    else
    {
        m_Size += (int)floor (result*Anabolic_Inv);  //put on weight
    }
  }
  else if (result<0)
  {
    if (m_Reserves>0) m_Reserves--;  //subtract 1 from reserves
    else
    {
      m_Size += (int)floor(result*Catabolic_Inv);   //loose weight
    }
  }
  //Update m_Size. If m_Size is approaching a critical value, roe gives up its
  //range and starts dispersing. Fawns younger than 2 months dies.
  //If m_Size < MinSize roe dies

  if ((m_Reserves==0)&&(m_Pregnant==false))
  {
    if(m_Size < m_OurPopulation->m_MinWeight_Females)
    {
      return 41; //Die
    }
    else if(m_Size<m_OurPopulation->m_CriticalWeight_Females)
    {  // in very bad condition, abandon fawns and increase foraging time
      if(m_NoOfYoung>0)
      {
        for(unsigned i=0;i<20;i++)
        {
          if ((m_MyYoung[i]!=NULL)&&(m_MyYoung[i]->m_Age<=365))
          {
            m_MyYoung[i]->On_MumAbandon(this);   //abandon the fawns
            m_MyYoung[i]=NULL;
            m_NoOfYoung--;
          }
        }
      }
      m_LengthFeedPeriod = (int)((5*m_Size*0.001)/
				    Female_FeedBouts[month-1]);
      m_DestinationDecided=true;
      m_NewState=35;  //go to feed after this
    }
    else //everything OK
    {
      m_LengthFeedPeriod = (int)((3*m_Size*0.001)/
				    Female_FeedBouts[month-1]);
      if(m_LengthFeedPeriod>3) m_LengthFeedPeriod=3;
    }
  }
  //set counter of daily energy gain and MinInState to zero
  m_EnergyGained=0;
  for(int i=0;i<12;i++)
  {
      m_MinInState[i]=0;
  }
  return 3; //Update gestation
}
//---------------------------------------------------------------------------
/**
Roe_Female::FDie - function for managing death of a female object. The female object
sends message to offspring list and removes herself from group list. If attended by male 
when dying, she sends message to this male as well. The fixlist of the female object is emptied and
the female furthermore adds itself to list of dead animals. Function returns dead state.
Calls functions RemoveFixList()and On_IsDead().
*/
int Roe_Female::FDie (void)  //Die
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FDie(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  //When female dies she sends message to offspring list and removes
  //herself from group list
  //If attended by male when dying, she sends message to this male as well
  //add yourself to list of dead animals
  for(int i=0;i<2000;i++)
  {
    if(m_OurPopulation->DeadList[i][0] == 0) //empty entrance
    {
      m_OurPopulation->DeadList[i][0] = 2;  //'1' for male, '2' for female
      m_OurPopulation->DeadList[i][1] = m_Age; //age in days
      break;
    }
  }
  //empty the fixlist of this animal
  m_OurPopulation->RemoveFixList(m_FixlistNumber);

  //tell mum
  if(Mum!=NULL)
  {
    #ifdef JUJ__Debug3
    if(Mum->IsAlive()!=0x0DEADC0DE)
    {
       m_OurLandscape ->Warn("Roe_Female::FDie(): Deadcode warning, Mum!","");
       exit( 1 );
    }
    #endif
    Mum->On_IsDead(this,3,false);  //'3' indicates offspring
    Mum=NULL;
  }
  //tell offspring
  for (int i=0; i<20; i++)
  {
    if (m_MyYoung[i]!=NULL)
    {
      m_MyYoung[i]->On_IsDead(this,0,false);  //'0' indicates Mum
    }
  }
  // Is there a male attending, if so send him a message
  if(m_My_Mate!=NULL)
  {
    #ifdef JUJ__Debug3
    if(m_My_Mate->IsAlive()!=0x0DEADC0DE)
    {
      m_OurLandscape ->Warn("Roe_Female::FDie(): Deadcode warning, m_My_Mate!","");
      exit( 1 );
    }
    #endif
    m_My_Mate->On_IsDead(this,2,false); //'2' indicates mate
    m_My_Mate=NULL;
  }


  m_CurrentStateNo=-1;
  return 44;

}
//---------------------------------------------------------------------------
/**
Roe_Female::FUpdateGestation - Checks if it is time to give birth if pregnant. Controls taking care
of young, mating, the probability of going into heat (dependent on age and time of year).
Returns states giveBirth, inHeat, CareFOrYoung or mate.  

*/
int Roe_Female::FUpdateGestation (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FUpdateGestation(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  int day = m_OurLandscape->SupplyDayInYear();
  if(day==0) m_WasInHeat=false;  //new season
  if (m_Pregnant==true)
  {
    if ((m_Gestationdays == GestationPeriod)) 
    {
      m_DestinationDecided=true;
      m_NewState=18;   //CareForYoung
      return 17; //GiveBirth
    }
    else m_Gestationdays++;
  }
  else  //not pregnant
  {
    if (m_My_Mate!=NULL)
    {
      #ifdef JUJ__Debug3
       if(m_My_Mate->IsAlive()!=0x0DEADC0DE)
       {
         m_OurLandscape ->Warn("Roe_Female::FUpdateGestation():Deadcode warning. m_My_Mate","");
         exit( 1 );
       }
      #endif
      m_DestinationDecided=true;
      return 39; //Mate
    }

    else if (m_DaysSinceParturition >= 0) //with young fawn
    {
      if(m_DaysSinceParturition<30) m_DaysSinceParturition++;
      else m_DaysSinceParturition=-1; //no need to count anymore
    }
    else  //not mated and not with fawn
    {
      if ((m_WasInHeat==false)&&(m_HaveRange==true)&&(day >= 230)
                               && (day <= 250)
                                   && (m_Age <= 730))
      {
        int YearlHeat;
        YearlHeat=random(100);

        if (YearlHeat<20)  //20, small probability each day, 60 % ends up being mated
        {  //can go in heat
          m_DestinationDecided=true;
          m_NewState=16;   //start next day in InHeat
	  m_WasInHeat=true;  //only 1 heat period per season
        }
      }
      else if ((m_WasInHeat==false)&&(m_HaveRange==true)&&(day >= 230)
                                           && (day <= 250)
                                             && (m_Age > 730))
      { //can go in heat
        int AdultHeat;
        AdultHeat=random(100);
        if (AdultHeat<30)      //30, slightly larger prob., 85 % ends up being mated
        {
          m_DestinationDecided=true;
          m_NewState=16;   //start next day in InHeat
          m_WasInHeat=true;
        }
      }
    }
  }
  //if roe is not going to mate or givebirth we need to assure that a new
  //destination has been set
  if (m_DestinationDecided==true)
  {
    return m_NewState;
  }
  else   //no destination decided, so go back to same state as before OnNewDay
  {
    return m_LastState;
  }
}
//---------------------------------------------------------------------------
/**
Roe_Female::FMate - sets female object to be pregnant and sets gestation days to
be zero. Also, function stops female from being attended by male. Female returns to same 
state as before OnNewDay.
*/
int Roe_Female::FMate(void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FMate(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  m_MinInState[10]+=1; //add 1 to time spend in this state
  m_Gestationdays=0;
  m_Pregnant=true;
  m_My_Mate=NULL;  //not attended anymore
  //return to same state as before OnNewDay
  return m_LastState;
}

//---------------------------------------------------------------------------
/**
Roe_Female::FFormGroup - Function for formation of female group. All deer in groups 
of size less than optimum, will ask around for group sizes of other groups. If closer 
to optimum than their own, they can change groups. If this is the case  a roe has to add 
itself to new group, remove itself from old group and message this years fawns about new group. 
The roe gets a list of females within a certatin range of the roe location then send message 
to all these females that they can join group. The roe then evaluate answers and decide whether to change 
group. A new group needs to be larger than old group but still <=optimal group size. Once in a group 
oldes female in group is found and the range centre of this female serves as group range center.
Returns update energy.
Calls Supply_GroupSize(), SupplyFemales(), On_CanJoinGroup(), SupplyGroupNo(), RemoveFromGroup(),
AddToGroup(), SupplyInfo() On_ChangeGroup(), Supply_RoeGroup(), On_UpdateGroup()
*/
int Roe_Female::FFormGroup(void)    //FormGroup
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FFormGroup(): Deadcode warning","");
     exit( 1 );
  }
  #endif
  
  /*All deer in groups of size less than optimum, will ask around for groupsizes
  of other groups. If closer to optimum than their own, they can change groups*/
  m_WantGroup = false;
  size_t Group_Size = 1;
  if(m_MyGroup == -1) //was a floater
  {
    m_WantGroup=true;
  }
  else
  {
    Group_Size=m_OurPopulation->Supply_GroupSize(m_MyGroup); //my group size
    if (Group_Size<m_OptGroupSize) //always want to join another group if this is true
    {
      m_WantGroup=true;
    }
    else if((m_Disperse)&&(m_NatalGroup)) //disperser without a group of its own
    {
      m_WantGroup=true;
    }
  }

  if (m_WantGroup) //look for a better group
  {
    ListOfFemales* RFL;
    //first get list of female
    RFL=m_OurPopulation->SupplyFemales(m_Location_x,m_Location_y,m_SearchRange);
    int result;
    int bestgroup=m_MyGroup;

    for (unsigned i=0; i<RFL->size(); i++)
    {
      #ifdef JUJ__Debug3
      if((*RFL)[i]->IsAlive()!=0x0DEADC0DE)
      {
        m_OurLandscape ->Warn("Roe_Female::FFormGroup(): Deadcode warning, (*RFL)[i]","");
        exit( 1 );
      }
      #endif
      if((*RFL)[i]!=this)    //don't send to yourself
      {
        //Send message "CanJoinGroup" to all these females
	result=(*RFL)[i]->On_CanJoinGroup(this);
        //evaluate answers and decide whether to change group. new group needs
        //to be larger than old group but still <=optimal group size.
        if ((result>Group_Size)&&(result<=m_OptGroupSize))
        {
	  bestgroup=(*RFL)[i]->SupplyGroupNo();  //better than mine
          break;  //better group found
        }
      }
    }
    delete RFL;
    if (bestgroup!=m_MyGroup) //another group is better than mine
    {
      //remove yourself from your old group
      m_OurPopulation->RemoveFromGroup(this,m_MyGroup);
      // Get the new group number
      m_MyGroup=bestgroup;
      //Add yourself to the new group
      m_OurPopulation->AddToGroup(this,m_MyGroup);
      //make sure that this years fawns know about new group
      if(m_NoOfYoung!=0)
      {
        for (int i=0;i<20;i++)
        {
          if(m_MyYoung[i]!=NULL)
          {
             #ifdef JUJ__Debug3
             if(m_MyYoung[i]->IsAlive()!=0x0DEADC0DE)
             {
               m_OurLandscape ->Warn("Roe_Female::FFormGroup():Deadcode warning, m_MyYoung[i]","");
               exit( 1 );
             }
             #endif
            RoeDeerInfo info=m_MyYoung[i]->SupplyInfo();
            if (info.m_Age<365)
            {
              m_MyYoung[i]->On_ChangeGroup(m_MyGroup);
            }
          }
        }
      }
    }
  }
  if(m_GroupUpdated==false)
  { //Find group range centre and send message to group members "UpdateGroup"
    int oldest=0;
    unsigned x=0;
    unsigned y=0;
    //find oldest female in your group
    ListOfDeer* group=m_OurPopulation->Supply_RoeGroup(m_MyGroup);
    for (unsigned i=0; i<group->size(); i++)
    {
      #ifdef JUJ__Debug3
      if((*group)[i]->IsAlive()!=0x0DEADC0DE)
      {
          m_OurLandscape ->Warn("Roe_Female::FFormGroup():Deadcode warning, (*group)[i]!","");
          exit( 1 );
      }
      #endif
      RoeDeerInfo info=(*group)[i]->SupplyInfo(); //get info about each deer in group
      if(info.m_Age>oldest)
      {
        oldest=info.m_Age;
        x=info.m_Range_x;    //group centre=that females rangecentre
        y=info.m_Range_y;
      }
    }
    for (unsigned i=0; i<group->size(); i++)
    { //send to yourself as well to update your own attributes
      (*group)[i]->On_UpdateGroup(x,y);
    }
  }
  m_WantGroup=false;
  return 4; //UpdateEnergy
}
//-----------------------------------------------------------------------
/**
Roe_Female::FGiveBirth - Function for female object giving birth to fawns. 
Litter size is related to maternal weight, Littersize=0.15*W - 0.68, 
where 'W'=maternal weight in kg. After litter size has been calculated, the corresponding
number of fawn objects are created. Fawns are randomly assinged sex (50:50), sizes of fawn are
calculated (int)((pow(((float)m_Size*0.001),0.9)*0.166)*1000); //Oftedal 1985, mum is assigned 
and fawns are added to mother's offspring list and group list.
Calls CreateObjects(). 

*/
int Roe_Female::FGiveBirth(void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::FGiveBirth(): Deadcode warning","");
     exit( 1 );
  }
  #endif

  Deer_struct* fds;
  fds=new Deer_struct;
  bool sex;
  m_MinInState[2]+=1; //add 1 to time spend in this state
  //litter size is related to maternal weight (Hewison 1996)
  //Littersize=0.15*W - 0.68, where 'W'=maternal weight in kg. Multiply by
  //0.15/1000 since body weight is in grams.
  //To round to nearest int, add 0.5 and round down
  float young =  (0.00015f*m_Size)-0.68f;
  m_NoOfYoung = (int) floor(young + 0.5);
  
  //for lene's output
  //m_OurPopulation->WriteReproFile("Reproduction.txt",m_ID, m_NoOfYoung);

  for(int i=0; i<m_NoOfYoung; i++)
  {
    int rand = random(100);
    if (rand<prop_femalefawns)
    {
      sex=false;  //female
    }
    else
    {
      sex=true;  //males
    }
    //call CreateObjects to create the right no. of fawns
    fds->Pop = m_OurPopulation;
    fds-> L = m_OurLandscape;
    fds->sex = sex;
    fds->x = m_Location_x;
    fds->y = m_Location_y;
    fds->mum = this;
    fds->size = (int)((pow(((float)m_Size*0.001),0.9)*0.166)*1000); //Oftedal 1985
    fds->group = m_MyGroup;
    //debug
    
    fds->ID = -1; //don't know yet
    m_OurPopulation->CreateObjects(0,this,NULL,fds,1);
    //fawns are added to offspring list and mothers group list
  }
  delete fds;

  m_Pregnant=false;
  m_DaysSinceParturition=0;
  m_Gestationdays=-1;
  return 18; //CareForYoung
}
//---------------------------------------------------------------------------
/**
 Roe_Female::AddFawnToList - adds fawns to offfspring list of female object.
 Calls AddToGroup().
*/
void Roe_Female::AddFawnToList(Roe_Fawn*fawn)  //called for each fawn from createobjects
{
  //add to the end of offspring list
  for(int i=0;i<20;i++)
  {
    if(m_MyYoung[i]==NULL)  //empty space
    {
      m_MyYoung[i]=fawn;
      break;
    }
  }
  //group list

  if(m_MyGroup!=-1)
  {
    m_OurPopulation->AddToGroup(fawn,m_MyGroup);
  }
}
//----------------------------------------------------------------------------

//---------------------------------------------------------------------------
/**
Roe_Female::BeginStep - checks if female object is dead
*/
void Roe_Female::BeginStep (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::BeginStep(): Deadcode warning","");
     exit( 1 );
  }
  #endif

}
//---------------------------------------------------------------------------
/**
Roe_Female::Step - function called every time step. The functions checks if done with a given state (m_StepDone)
and governs states and transitions to other states. Calls functions: FOnMature(), FDie(), FOnNewDay(),
FUpdateGestation(),FMate(), FFormGroup(), FGiveBirth(), FEstablishRange(), FDisperse(), FFeed(), FRecover(), FRUn(), FEvade(),
FIgnore(), FRuminate, FCareForYoung(), FUpdateEnergy(), FInHeat(). 
*/
void Roe_Female::Step (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::Step(): Deadcode warning","");
     exit( 1 );
  }
  #endif

  
 if (m_StepDone || (CurrentRState==rds_FDeathState)) return;

 switch (CurrentRState)
 {
   case rds_Initialise:
     CurrentRState=rds_FOnMature;
     break;
   case rds_FOnMature:
    switch (FOnMature())
    {
      case 7: //rds_FEstablishRange:
        CurrentRState=rds_FEstablishRange;
        break;
      case 10: //rds_FDisperse:
        CurrentRState=rds_FDisperse;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FOnMature: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_FDie:
    switch (FDie())
    {
      case 44:
      CurrentRState=rds_FDeathState;
        m_StepDone=true;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FDie: No matching case!","");
           exit( 1 );
        break;
    }
    break;

   case rds_FOnNewDay:
    switch (FOnNewDay())
    {
     case 4: //rds_FUpdateEnergy:
       CurrentRState=rds_FUpdateEnergy;
       break;
     case 15: //rds_FormGroup
       CurrentRState=rds_FFormGroup;
       break;
     case 41: //rds_FDie:
       CurrentRState=rds_FDie;
       break;
     default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FOnNewDay: No matching case!","");
           exit( 1 );
       break;
    }
    break;
   case rds_FUpdateGestation:
    switch (FUpdateGestation())
    {
      case 7: //rds_FEstablishRange
        CurrentRState=rds_FEstablishRange;
        m_StepDone=true;
        break;
      case 10: // rds_FDisperse
        CurrentRState=rds_FDisperse;
        m_StepDone=true;
        break;
      case 16: //rds_FInHeat:
        CurrentRState=rds_FInHeat;
        m_StepDone=true;
        break;
      case 17: //rds_FGiveBirth:
        CurrentRState=rds_FGiveBirth;
        break;
      case 18: //rds_FCareForYoung
        CurrentRState=rds_FCareForYoung;
        m_StepDone=true;
        break;
      case 22: //rds_FRecover:
        CurrentRState=rds_FRecover;
        m_StepDone=true;
        break;
      case 32: //rds_FRuminate
        CurrentRState=rds_FRuminate;
        m_StepDone=true;
        break;
      case 35: //rds_FFeed
        CurrentRState=rds_FFeed;
        m_StepDone=true;
        break;
      case 39: //rds_FMate:
        CurrentRState=rds_FMate;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step():rds_FUpdategestation: No matching case!","");
           exit( 1 );
        break;
    }
    break;
   case rds_FMate:
    switch(FMate())
    {
      case 10: // rds_FDisperse
        CurrentRState=rds_FDisperse;
        break;
      case 16: //rds_FInHeat:
        CurrentRState=rds_FInHeat;
        break;
      case 18: //rds_FCareForYoung
        CurrentRState=rds_FCareForYoung;
        break;
      case 22: //rds_FRecover:
        CurrentRState=rds_FRecover;
        break;
      case 32: //rds_FRuminate
        CurrentRState=rds_FRuminate;
        break;
      case 35: //rds_FFeed
        CurrentRState=rds_FFeed;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FMate: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_FFormGroup:
    switch (FFormGroup())
    { 
      case 4: //rds_FUpdateEnergy:
        CurrentRState=rds_FUpdateEnergy;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FFormGroup: No matching case!","");
           exit( 1 );
        break;
    }
    break;
   case rds_FGiveBirth:
    switch (FGiveBirth())
    {
      case 18:
        CurrentRState=rds_FCareForYoung;
        m_StepDone=true;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FGiveBirth: No matching case!","");
           exit( 1 );
        break;
    }
    break;
   case rds_FEstablishRange:
    switch (FEstablishRange())
    {
      case 35: //rds_FFeed:
        CurrentRState=rds_FFeed;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step():rds_FEstablishRange: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_FDisperse:
    switch (FDisperse())
    {
     case 35: //rds_FFeed:
       CurrentRState=rds_FFeed;
       m_StepDone=true;
       break;
     case 7: //rds_FEstablishRange:
       CurrentRState=rds_FEstablishRange;
       m_StepDone=true;
       break;
     case 10:  //disperse
       CurrentRState=rds_FDisperse;
       m_StepDone=true;
       break;
     case 41:
       CurrentRState = rds_FDie;
       break;
     default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FDisperse: No matching case!","");
           exit( 1 );
       break;
    }
    break;
   case rds_FFeed:
    switch (FFeed())
    {
      case 10: //rds_FDisperse:
        CurrentRState=rds_FDisperse;
        break;
      case 32: //rds_FRuminate:
        CurrentRState=rds_FRuminate;
        break;
      case 35: //rds_FFeed:
        CurrentRState=rds_FFeed;
        break;
     case 41: //rds_FDie
       CurrentRState=rds_FDie;
       break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FFeed: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_FRecover:
    switch(FRecover())
    {
      case 35: //rds_FFeed:
        CurrentRState=rds_FFeed;
        break;
      case 22: //rds_FRecover
       CurrentRState=rds_FRecover;
       break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FRecover: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_FRun:
    switch(FRun())
    {
      case 22: //rds_FRecover:
        CurrentRState=rds_FRecover;
        break;
      case 19: //rds_FRun
       CurrentRState=rds_FRun;
       break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FRun: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_FEvade:
     switch(FEvade())
     {
       case 10:
         CurrentRState=rds_FDisperse;
         break;
       case 16:
         CurrentRState=rds_FInHeat;
         break;
       case 18:
         CurrentRState= rds_FCareForYoung;
         break;
       case 22:
         CurrentRState=rds_FRecover;
         break;
       case 32:
         CurrentRState=rds_FRuminate;
         break;
       case 35:
         CurrentRState=rds_FFeed;
         break;
       default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FEvade: No matching case!","");
           exit( 1 );
     }
     m_StepDone=true;
     break;
   case rds_FIgnore:
     switch(FIgnore())
     {
       case 10:
         CurrentRState=rds_FDisperse;
         break;
       case 16:
         CurrentRState=rds_FInHeat;
         break;
       case 18:
         CurrentRState= rds_FCareForYoung;
         break;
       case 22:
         CurrentRState=rds_FRecover;
         break;
       case 32:
         CurrentRState=rds_FRuminate;
         break;
       case 35:
         CurrentRState=rds_FFeed;
         break;
       default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FIgnore: No matching case!","");
           exit( 1 );
     }
     m_StepDone=true;
     break;
   case rds_FRuminate:
    switch (FRuminate())
    {
      case 10:
        CurrentRState=rds_FDisperse;
    m_StepDone=true;
        break;
      case 35:
        CurrentRState=rds_FFeed;
    m_StepDone=true;
        break;
      case 32:
       CurrentRState=rds_FRuminate;
    m_StepDone=true;
       break;
      case 41:
        CurrentRState=rds_FDie;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FRuminate: No matching case!","");
           exit( 1 );
        break;
    }
    break;
   case rds_FCareForYoung:
    switch (FCareForYoung())
    {
      case 35: //rds_FFeed:
        CurrentRState=rds_FFeed;
        break;
      case 18: //rds_FCareForYoung
       CurrentRState=rds_FCareForYoung;
       break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step():rds_FCareForYoung: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_FUpdateEnergy:
    switch (FUpdateEnergy())
    {
      case 41: //rds_FDie:
        CurrentRState=rds_FDie;
        break;
      case 3: //rds_FUpdateGestation:
        CurrentRState=rds_FUpdateGestation;
        break;
      case 10:
        CurrentRState=rds_FDisperse;
        break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FUpdateEnergy: No matching case!","");
           exit( 1 );
        break;
    }
    break;
   case rds_FInHeat:
    switch (FInHeat())
    {
      case 35: //rds_FFeed:
        CurrentRState=rds_FFeed;
        break;
      case 16: //rds_FInheat
       CurrentRState=rds_FInHeat;
       break;
      default:
          m_OurLandscape ->Warn("Roe_Female::Step(): rds_FInheat: No matching case!","");
           exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   default:
          m_OurLandscape ->Warn("Roe_Female::Step(): No matching case!","");
           exit( 1 );
 }
}

//---------------------------------------------------------------------------
/**Roe_Female::EndStep - checks if female object is dead and adds to number of timesteps.
If 1 whole day has passed, current state is set to FOnNewDay
*/
void Roe_Female::EndStep (void)
{
 #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
     m_OurLandscape ->Warn("Roe_Female::EndStep(): Deadcode warning!","");
     exit( 1 );
  }
  #endif
 if (CurrentRState==rds_FDeathState) return;

 timestep++;
 if (m_OurPopulation->StepCounter%144==0) //1 day has passed
 {
    m_weightedstep=0;
    CurrentRState=rds_FOnNewDay;
 }
}
//---------------------------------------------------------------------------

/**
Roe_Female::Roe_Female - constructor for female object. Sets all attributes and creates groups for 1st
generation roes. It also removes a matured fawn from group and adds it as adult female. It also adds fawns
to mother's offspring list.
Calls CreateNewGroup(), AddToGroup().
*/
Roe_Female::Roe_Female(int x,int y, int size,int age, int group,Roe_Female* mum,
    Landscape* L,RoeDeer_Population_Manager* RPM):Roe_Adult_Base (x,y,size,age,L,RPM)
{
  //set pointers to NULL
  for (int i=0;i<20;i++)
  {
    m_MyYoung[i]= NULL;
  }

  
  m_My_Mate=NULL;
  Mum=mum;  //NULL for 1st generation roes
  if(group==-1)  //1st generation roe
  {
    //create a new group and add yourself to that group
    m_MyGroup=m_OurPopulation->CreateNewGroup(this);
    m_GroupUpdated=true;  //Jan. 1st
  }
  else //2nd generation or more
  {
    m_MyGroup=group;
    m_GroupUpdated=false;  //summer
    m_OurPopulation->AddToGroup(this,m_MyGroup); //matured fawn removes itself
    //from group and the new female is added again
  }
  //add yourself to mothers offspring list
  if(Mum!=NULL)
  {
    #ifdef JUJ__Debug3
    if(Mum->IsAlive()!=0x0DEADC0DE)
    {
      m_OurLandscape ->Warn("Roe_Female::Roe_Female(): Deadcode warning, Mum!","");
      exit( 1 );
    }
    #endif
    for(int i=0;i<21;i++)
    {
      if(Mum->m_MyYoung[i]==NULL)
      {
        Mum->m_MyYoung[i]=this;
        break;
      }
    }
  }
  m_HaveGroup=true;
  m_Age = age;
  m_Pregnant = false;
  m_Reserves = 30;
  m_Gestationdays=-1;
  m_Sex = false;
  m_HeatCount=0;
  m_WasInHeat=false;
  m_LastState=35; //Feed. Could be any safe state
  m_HaveRange = false;
  m_OptGroupSize = 1;  //as long as roe doesn't have a range
  m_NoOfYoung = 0;
  m_Care=false;
  m_NatalGroup = true;
  m_IsDead=false;
  m_DaysSinceParturition = -1;
  if (size==-1) //1st generation roe
  {
    int rand = random(2000);
    m_Size = 16000 + rand;  //size is set for the first generation

  }
  else m_Size=size;
  for (int i=0;i<12;i++) m_MinInState[i] = 0;

}
//---------------------------------------------------------------------------
Roe_Female::~Roe_Female ()
{
  //remove pointers to fawns
  for(int i=0;i<20;i++)
  {
    if(m_MyYoung!=NULL)
    {
      m_MyYoung[i]=NULL;
    }
  }
  //remove yourself from group list
  if(!m_float)
  {
    m_OurPopulation->RemoveFromGroup(this,m_MyGroup);
    m_MyGroup=-1;
  }
}

//---------------------------------------------------------------------------

/**
Roe_Female::On_IsDead - governs a female objects "response" to death of another object. Checks
who is dead (mom, female, mate) and sets them to NULL. If mother is dead, the "responding" female object
deletes pointer to mom. If mate is dead, the female will go back into heat. If offspring is dead (both fawns
and adults), they will be removed from offspring list.

*/
void Roe_Female::On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Female::On_IsDead(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //If whos_dead is mother
  switch (whos_dead)
  {
    case 0:   //mother
    {
      if(Someone==Mum)  Mum=NULL;    //delete pointer to mother
    }
    break;
    case 2:   //attending male
    {
      if(m_My_Mate==dynamic_cast<Roe_Male*> (Someone))
      {
        m_My_Mate=NULL;  //remove pointer to male
        CurrentRState=rds_FInHeat;
      }
    }
    break;
    case 3:  //offspring, both fawns and adult offspring
    {
       for (int i=0; i<21; i++)
       {
         if((m_MyYoung[i]!=NULL)&&(m_MyYoung[i]==Someone))
         {
           if(m_MyYoung[i]->IsAlive()!= 0x0DEADC0DE)
           {
              m_OurLandscape ->Warn("Roe_Female::On_IsDead():Deadcode warning, m_MyYoung[i]!","");
              exit( 1 );
           }
           m_MyYoung[i]=NULL;
           break;
         }
         if (i==20)
         {
           m_OurLandscape ->Warn("Roe_Female::On_IsDead():Variable out of range, m_MyYoung!","");
           exit( 1 );
         }
       }
       //subtract 1 from NoOfYoung if Someone is a fawn from this year;
       if (fawn==true)
       {
         m_NoOfYoung--;
       }
    }
    break;
  }
}




//-----------------------------------------------------------------------------
/**
Roe_Female::On_ApproachOfDanger - Determines whether to run or evade a threat depending on
cover and distance to threath. In good cover, female will run if threath is less than 30 m 
away and else perform evasive behavior, In intermediate cover the flight distance is 50 m, 
and in poor cover it is 70 m.
Returns states evade or run.
Calls Cover(), DistanceTo().
*/
void Roe_Female::On_ApproachOfDanger(int p_To_x,int p_To_y)
{
  m_danger_x=p_To_x;
  m_danger_y=p_To_y;
  //get cover
  int cov=Cover(m_Location_x,m_Location_y);
  //get distance to threat
  int dist=DistanceTo(m_Location_x, m_Location_y,p_To_x,p_To_y);

  if (cov >= CoverThreshold2) //good cover
  {
    if(dist >= 30)
    {
      CurrentRState=rds_FEvade;
    }
    else CurrentRState=rds_FRun;
  }
  else if (cov >= CoverThreshold1)  //intermediate cover
  {
    if(dist>=50)
    {
      CurrentRState=rds_FEvade;
    }
    else  CurrentRState=rds_FRun;
  }
  else //poor cover
  {
    if(dist>=70)
    {
      CurrentRState=rds_FEvade;
    }
    else  CurrentRState=rds_FRun;
  }
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_InitCare - Returns true if female objects allows fawn to feed 
(if her reserves are > 0) otherwise it returns false.
*/

bool Roe_Female::On_InitCare(Roe_Fawn* /* Fawn */)
{
   //mother will feed fawns if her reserves are > 0
   if (m_Size>m_OurPopulation->m_CriticalWeight_Females)
   {
     m_Care=true;
     CurrentRState=rds_FCareForYoung;
   }
   else
   {
     CurrentRState=rds_FFeed;
     m_Care=false;
   }
   return m_Care;
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_EndCare - Fawn ends it's care period - returns false.
*/
void Roe_Female::On_EndCare(Roe_Fawn* /* Fawn */) //fawn ends careperiod
{
  m_Care=false;
  CurrentRState=rds_FFeed;
}
//---------------------------------------------------------------------------

/**
Roe_Female::Send_EndGroup - calls to dissolve female group.
Calls Supply_RoeGroup(), On_EndGroup()
*/

void Roe_Female::Send_EndGroup()
{
   ListOfDeer* MyGroup=m_OurPopulation->Supply_RoeGroup(m_MyGroup);
   for (unsigned i=0; i<MyGroup->size(); i++)
   {
     #ifdef JUJ__Debug3
     if((*MyGroup)[i]->IsAlive()!=0x0DEADC0DE)
     {
       m_OurLandscape ->Warn("Roe_Female::Send_EndGroup():Deadcode warning, m_MyGroup[i]!","");
       exit( 1 );
     }
     #endif
     if((*MyGroup)[i]!=this)
     {
        (*MyGroup)[i]->On_EndGroup();
     }
   }
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_UpdateGroup - updates group status by changing range center to that
of group while storing old range center for when ghroup is dissolved and roe needs to
go back to own range.
*/
void Roe_Female::On_UpdateGroup(int p_group_x,int p_group_y)
{
  m_GroupUpdated=true;
  m_OldRange_x=m_RangeCentre_x;  //stores own rangecentre while in group range
  m_OldRange_y=m_RangeCentre_y;
  m_RangeCentre_x=p_group_x;    //consider this as your rangecentre while in group
  m_RangeCentre_y=p_group_y;
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_CanJoinGroup - Called by female object, and evaluates whether a new 
female (found by searching for all females withing a range of the calling females location)
can  join a female group or not. Returns an int >0 if new roe can join group, returns 0 if 
roe cannot join. First makes sure that new female is not a floater, since floaters
cannot have a group. Then looks at new females current group size and whether or not the new female is the
calling female's mother. If so, function returns group size. If new female does not have group
function returns 1. If new female has group, function checks if this group size is less
than optimal group size andd returns group size if so and 0 otherwise (cannot have larger group than
optimum group size)
Calls Supply_GroupSize()
*/
int Roe_Female::On_CanJoinGroup(Roe_Female* Female)
{
  //returns an int >0 if new roe can join group, Returns 0 if roe cannot join
  int answer=0;
  if(!m_float)  //floaters don't have a group
  {
    size_t Group_Size=m_OurPopulation->Supply_GroupSize(m_MyGroup);
    //if sender is offspring
    for (int i=0; i<10; i++)
    {
      if (m_MyYoung[i]==Female) //is on list
      {
        answer=(int)Group_Size;
      }
    }

    if (m_HaveGroup==false) answer=1;
    else
    {
      if (Group_Size < m_OptGroupSize) answer=(int)Group_Size;
      else  answer=0;   //don't want a larger group
    }
  }
  return answer;
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_EndGroup - after group is dissolved this function sets range
centers to old individual centers.
*/

void Roe_Female::On_EndGroup()
{
  m_RangeCentre_x=m_OldRange_x;  //go back to own range
  m_RangeCentre_y=m_OldRange_y;
  m_GroupUpdated=false;         //group not "active" anymore
}
//---------------------------------------------------------------------------




