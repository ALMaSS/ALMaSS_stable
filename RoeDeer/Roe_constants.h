//
// Roe_constants.h
//

#ifndef ROE_CONSTANTS_H
#define ROE_CONSTANTS_H




//extern const char* SimulationName;
extern const int Cell_Size;
extern const int Cell_HalfSize;
extern const double InvCell_Size;   //1/Cell_Size
extern const int MinRange;
extern const int MaxRange;
extern const int Vector_x[8];
extern const int Vector_y[8];
extern const int MaxDistToMum;
extern const int MinDispDistance;
extern const int MaxNoGroups;
extern const int MaxNoDeer;

extern const unsigned MaleDensityThreshold;

extern const unsigned FemaleDensityThreshold;

extern const int NoRandomAreas;

extern const float DivideRandomAreas;
extern const int MinRangeQuality;
extern const int AverageRecoveryTime;
extern const int MaxRecoveryTime;
extern const int CoverThreshold1;
extern const int CoverThreshold2;

extern const float IntakeRate[12];

extern const int MinNutri;

extern const int MaxRoadWidth;
extern const int MaxTraffic;

extern const int Inact_Periods_Males[12];
extern const int Inact_Periods_Fawns[12];

extern const float FemaleRMR[7];
extern const float FawnRMR;
extern const float MaleRMR;

extern const int MinWeightFawn[5];

extern const int prop_femalefawns;

extern const int Male_FeedBouts[12];
extern const int Female_FeedBouts[12];

extern const int CostFemaleAct[12];
extern const int CostMaleAct[12];
extern const int CostFawnAct[6];

extern const float Anabolic;
extern const float Anabolic_Inv;

extern const float Catabolic;
extern const float Catabolic_Inv;

extern const int FloatMort;
extern const float FemaleDMR[4];
extern const float MaleDMR[4];

extern const float FawnDMR[2];

extern const int Minhrcare[2];
extern const int Flush_dist[2];
extern const int MalePrime;

extern const int GestationPeriod;

#endif // ROE_CONSTANTS_H
