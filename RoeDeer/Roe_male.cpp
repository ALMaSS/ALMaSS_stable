//
// roe_male.cpp
//

#include <iostream>
#include <fstream>

#pragma hdrstop

#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"

#include "Roe_all.h"
#include "Roe_constants.h"

extern int FastModulus( int a_val, int a_modulus );


//---------------------------------------------------------------------------
//                              Roe_Male
//---------------------------------------------------------------------------

/**
Roe_Male::MOnMature - Checks if area is good enough to establish range. The function ScanGrid()
adds the quality values stored in m_grid starting with a minimum area of
400 x 400 m. If not sufficient it extends outwards in all directions
until max range is reached (800 x 800 m). It also checks if amount of other males nearby is below
a male density threshold. It returns the size of the resulting area if good enough and 0 if not 
good enough
Calls ScanGrid(), SupplyMaleRC().
*/
int Roe_Male::MOnMature()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MOnMature(): Deadcode warning!","");
    exit( 1 );
  }
  #endif

    //Check whether this area is good enough. The function ScanGrid()
    //adds the quality values stored in m_grid starting with a minimum area of
    //400 x 400 m. If not sufficient it extends outwards in all directions
    //until max range is reached (800 x 800 m). It returns the size of the
    //resulting area if good enough and 0 if not good enough

      int range = m_OurPopulation->ScanGrid(m_Location_x,m_Location_y,false);
      if (range > 0) //good enough
      {
        //how many other males already in area?
        ListOfMales* males= m_OurPopulation->
                                SupplyMaleRC(m_Location_x,m_Location_y,range);
        if(males->size() < MaleDensityThreshold)  //OK
        {
          m_SearchRange = range;
          delete males;
          return 8; // establish range
        }
        delete males;
      }
      return 11;  //no options in this area, so disperse
}
//----------------------------------------------------------------------------
/**
Roe_Male::MEstablishRange - sets up range for male object. Contrary to the females, 
males does not just set up range in their mothers´territory. They have to position themselves
according to the distribution of male territories in the area. Two strategies: 1)satellite and 
2)periphere. Males heavier than average for their age class choose satellite and set rangecentre=
oldest male in area. Others choose periphere and avoid other males. Sets range center.
Returns state feed. Calls functions SupplyTMales(), SupplyInfo(), AddSatellite()
*/
int Roe_Male::MEstablishRange()   //State 8
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MEstablishRange(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //contrary to the females, males does not just set up range in their mothers
  //territory. They have to position themselves according to the distribution of male
  //territories in the area. Two strategies: 1)satellite and 2)periphere. Males
  //heavier than average for their ageclass choose satellite and set rangecentre=
  //oldest male in area. Others choose periphere and avoid other males

  m_MyTMale=NULL;
  m_MinInState[0]++;  //add 1 to timesteps spend in this state
  //Get list of TMales in area
  ListOfMales* males = m_OurPopulation->
                          SupplyTMales(m_Location_x,m_Location_y,1000);
  if((males->size()>0)&&(m_Size > m_OurPopulation->m_MaleDispThreshold))
  { //use satellite strategy
    //Choose the oldest male on list
    int oldest=0;
    for (unsigned i=0; i< males->size(); i++)
    {
      #ifdef JUJ__Debug3
      if((*males)[i]->IsAlive()!=0x0DEADC0DE)
      {
        m_OurLandscape ->Warn("Roe_Male::MOnMature(): Deadcode warning, (*males)[i]!","");
        exit( 1 );
      }
      #endif
      RoeDeerInfo info=(*males)[i]->SupplyInfo();
      if (info.m_Age>oldest)
      {
         oldest=info.m_Age;
         m_RangeCentre_x=info.m_Range_x;  //set your rangecentre=his
         m_RangeCentre_y=info.m_Range_y;
	 m_MyTMale=(*males)[i];
      }
    }
    m_MyTMale->AddSatellite(this);
  }
  else  //Either a periphere male or no males in area. Establish range on spot
  {
    m_RangeCentre_x=m_Location_x;
    m_RangeCentre_y=m_Location_y;
  }
  m_HaveRange=true;
  m_Disperse=false;
  m_float=false;
  m_DispCount=0;  //end of dipersal
  delete males;
  return 36;
}
//---------------------------------------------------------------------------
/**
Roe_Male::MDisperse - models dispersal state of male objects. Removes satellite males, sets state to 
dispersal. It then keeps track of amount of time in the dispersal
state and calculates probability of switching to feeding, keep dispersing, or dying if spending too long
in dispersal state.Calls functions RemoveSatellite(), DirectionTo(), NextStep().
*/
int Roe_Male::MDisperse (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MDisperse(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  if(m_MyTMale!=NULL)
  {
    m_MyTMale->RemoveSatellite(this);
    m_MyTMale=NULL;
  }
  m_DispCount++;
  m_MinInState[2]++; //add 1 to time spend in this state
  m_HaveRange=false;  //a dispersing animal cannot be a rangeholder per def.
  m_HaveTerritory=false;
  m_Disperse=true;  //indicates that dispersal has started
  //allow buck the opportunity to return to this state after disturbance
  m_LastState=11;
  if(m_DispCount==1)  //first timestep in disperse
  {
    //store range centre coordinates while dispersing
    m_OldRange_x=m_RangeCentre_x;
    m_OldRange_y=m_RangeCentre_y;
  }
  m_RangeCentre_x=m_Location_x; //these are drifting depending on bucks position
  m_RangeCentre_y=m_Location_y;
  //int dist=DistanceTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
  int dir= DirectionTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
  int rand=0;
  if(m_DispCount>70)
  {
    for(int i=0;i<3;i++)
    {
      if( NextStep(0,0,0) == -1) return 42; //
    }
  }
  else
  {
    for(int i=0;i<3;i++)
    {
      if( NextStep(1,((dir+4) & 0x07),0) == -1) return 42;
    }
  }

  switch (m_DispCount)
  {
    case 40:
      rand=40;
      break;
    case 50:
      rand=60;
      break;
    case 60:
      rand=80;
      break;
    default:
      if(m_DispCount>=70) rand=90;
      break;
  }

  if (rand>random(100))
  {
    return 36;
  }
  else return 11;
}
//---------------------------------------------------------------------------
/**
Roe_Male::MFeed - Models feeding of male object, including energy budget and search for better feeding 
area. Also determines need to ruminate. In theory a roe deer can spend time 
in more than 1 type of habitat within 1 timestep. For the energy budget only the habitat 
type where roe deer is at the beginning of each time step is recorded. Need to keep counter of minutes
in feed since the end of last ruminating period (m_FeedCount) to know when to return to ruminate. Returns values
for dispersal, ruminate or keep feeding (or die).
Calls function NutriValue()
*/
int Roe_Male::MFeed (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!= 0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MFeed(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //In theory a roe can spend time in more than 1 type of habitat within 1 timestep.
  //For the energy budget only the habitat type where roe is at the beginning of
  //each time step is recorded. Need to keep counter of minutes
  //in feed since the end of last ruminating period (m_FeedCount) to know when to
  //return to ruminate
  m_MinInState[10]++; //add 1 to time spend in this state
  m_FeedCount++;   //add 1 to count for this feeding bout
  //allow roe the opportunity to return to this state after disturbance
  m_LastState=36;  //feed
  //Find the nutritional value here
  int nutri=NutriValue(m_Location_x,m_Location_y);  //possible energy gain per minut
  if (nutri<MinNutri)   //not good enough for feeding
  { //search for better place
     SeekNutri(MinNutri);
     Feeding(m_Disperse);
  }
  else  //can feed here
  {
    m_EnergyGained+=nutri;   //add this to total count for today
    Feeding(m_Disperse);
  }
  //did roe die while taking feeding steps?
  if (m_IsDead)   //is dead
  {
      m_FeedCount=0;
      return 42;  //Die
  }
  else //still alive, so decide whereto next.
  {
    if (m_FeedCount>m_LengthFeedPeriod)   //need to ruminate
    {
      //for how long do you need to ruminate?
      int month= m_OurLandscape->SupplyMonth();
      m_LengthRuminate=(int) (m_FeedCount*IntakeRate[month-1]*0.1);
      m_FeedCount=0;   //counter to zero
      return 33;  //ruminate
    }
    else if (m_Disperse==true) //a disperser, go back to disperse with 50:50 chance
    //"Cheap" solution. No basis for setting this value. Return to this later....
    {
      int rand=random(100);
      if (rand<50)
      {
        m_FeedCount=0;
        return 11; //Disperse
      }
    }
  }
  return 36;  //stay in feed
}
//---------------------------------------------------------------------------
/**
Roe_Male::MEstablishTerritory - establishment of territory by male object. If male object is satellite male,
the male challenges his host and adds him to fightlist. If Male is stronger it takes over the territory, otherwise
no territory. If not a satellite male check all nearby males and establish rank by fighting them. If
win take over their territory. Always returns state feeding.
Calls AddToFightList(), On_Rank(), On_Expelled(), SupplyInfo(), SupplyTMales(), 
*/

int Roe_Male::MEstablishTerritory (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MEstablishTerritory(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  m_MinInState[1]++; //add 1 to time spend in this state
  bool result=false;
  int defeated=-1;
  if (m_MyTMale!=NULL)  //challenge your host first
  {
    #ifdef JUJ__Debug3
    if(m_MyTMale->IsAlive()!=0x0DEADC0DE)
    {
      m_OurLandscape ->Warn("Roe_Male::MEstablishTerritory(): Deadcode warning, m_MyTMale!","");
      exit( 1 );
    }
    #endif
    //Add host to fightlist
    m_MyFightlist=m_OurPopulation->AddToFightlist(m_MyTMale,m_MyFightlist);

    if(m_MyTMale->On_Rank(this, double(m_Size), m_Age, m_NoOfMatings,false,false) == true)
    { //you are stronger, take over territory
      m_MyTMale->On_Expelled(this);

      RoeDeerInfo range=m_MyTMale->SupplyInfo();
      m_RangeCentre_x=range.m_Range_x;  //set your rangecentre=his
      m_RangeCentre_y=range.m_Range_y;
      m_NumberOfFights++;
      m_HaveTerritory=true;
      m_Disperse=false;
      m_DispCount=0;
      m_MyTMale=NULL;
    }
    else //you lost to your host, situation is unchanged
    {
      m_NumberOfFights++;
    }
  }
  else //don't have a host
  {

    ListOfMales* TMales= m_OurPopulation->SupplyTMales(m_Location_x,
                                                m_Location_y,2*m_SearchRange);
    for(unsigned i=0; i<TMales->size(); i++) //for every male in area
    {
       #ifdef JUJ__Debug3
        if((*TMales)[i]->IsAlive()!=0x0DEADC0DE)
        {
          m_OurLandscape ->Warn("Roe_Male::MEstablishTerritory(): Deadcode warning,(*Tmales)[i]!","");
          exit( 1 );
        }
       #endif
      //add this male to fightlist
      m_MyFightlist=m_OurPopulation->AddToFightlist((*TMales)[i],m_MyFightlist);
      m_NumberOfFights++;
      m_MinInState[7]++;  //to place cost on male for 1 fight
      //send Rank-message to this male
      result=(*TMales)[i]->On_Rank(this,double (m_Size),m_Age,m_NoOfMatings,false,false);
      if(result)   //this male won the fight
      {
         defeated=i;
         break;
      }
    } //all males checked
    if(defeated==-1) //list must have been empty
    {
      m_HaveTerritory=true;
      m_RangeCentre_x=m_Location_x;
      m_RangeCentre_y=m_Location_y;
      m_Disperse=false;
      m_DispCount=0;
    }
    else  //must have won a fight. Can now take over that territory
    {
      RoeDeerInfo range=(*TMales)[defeated]->SupplyInfo();
      m_HaveTerritory=true;
      m_RangeCentre_x=range.m_Range_x;  //set your rangecentre=his
      m_RangeCentre_y=range.m_Range_y;
      //tell that male that he's been expelled from his territory
      (*TMales)[defeated]->On_Expelled(this);
      m_Disperse=false;
      m_DispCount=0;
    }
    delete TMales;
  }
  return 36;//Feed
}
//---------------------------------------------------------------------------
/**
Roe_Male::MRuminate - Function for ruminating. Checks for got spot to rest and amount of time 
spent ruminating. Ruminate activity cannot be seperated from other kinds of inactivity. 
It is also assumed that ruminating takes place while recovering after disturbance. 
Need to count amount of time spend in this state during this period of ruminating and in total last 24 hrs. 
Returns values for either stay ruminating, feed or dispersal (or die).Calls function Cover().
*/
int Roe_Male::MRuminate(void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MRuminate(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
 //ruminate activity cannot be seperated from other kinds of inactivity.
  //It is also assumed that ruminating takes place while recovering after
  //disturbance. Need to count amount of time spend in this state during
  //this period of ruminating and in total last 24 hrs
  m_MinInState[9]++; //add 1 to time spend in this state
  int min;
  //First check if this is first timestep in this state
  //allow roe the opportunity to return to this state after disturbance
  m_LastState=33;
  if (m_RumiCount==0)   //first step so make sure this is a good spot to rest
  {
    int cover=Cover(m_Location_x,m_Location_y);
     //get month
    int month=m_OurLandscape->SupplyMonth();
    if((month>=5)&&(month<=9)) min=CoverThreshold2; //summer
    else min=CoverThreshold1; //winter
    if (cover<min)// habitat unsuitable or cover very poor, so look for better spot
    {
       SeekCover(2);
       if(m_IsDead) return 42;  //Die
       else
       {
         m_RumiCount++;
         return 33;
       }
    }
    else //cover OK
    {
      m_RumiCount++;       //add 1 to counter
      return 33;    //return to this state next time step
    }
  }
  else if(m_RumiCount<m_LengthRuminate)
  {
    m_RumiCount++;  //add to counter
    return 33;     //stay
  }
  else
  {
    if(m_Disperse==true)  return 11;
    else  //not disperser
    {
       if (m_RumiCount>=m_LengthRuminate)
       {
         m_RumiCount=0;
         return 36;   //feed
       }
       else
       {
         int rand = random(100);
         if(rand<50)
         {
           m_RumiCount++;
           return 33;  //stay
         }
         else
         {
           m_RumiCount=0;
           return 36;
         }
       }
    }
  }
}
//----------------------------------------------------------------------------

/**
Roe_Male::MRecover - Governs recovery after running from danger. Function counts 
amount of time in state during the present period of recovovery (m_RecovCount) and 
sets this to 0 when state is left. Female only transfers to this state if safe, whichs means
in good habitat. Returns states: feeding, recovery. 

*/
int Roe_Male::MRecover (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MRecover(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //Need to count amount of time in state during the present
  //period of recov. (m_RecovCount) and set this to 0 when state is left.
  //Roe only transfers to this state if safe (i.e. cover>=3) so we know it is in
  //good habitat
  m_MinInState[4]++; //add 1 to time spend in this state

  //allow roe the opportunity to return to this state after disturbance
  m_LastState=23; //recover
  if (m_RecovCount<=AverageRecoveryTime)
  {
    m_RecovCount++;   //add 1 to time spend in recover and stay here
    return 23; //recover
  }
  else if ((m_RecovCount>AverageRecoveryTime)&&(m_RecovCount<MaxRecoveryTime))
  //return to feed with certain probability
  {
    int stop= random(100);
    if (stop<10)
    {
      m_RecovCount=0;    //leaves state, so set counter=0
      m_FeedCount=0;   //ruminating also takes place during recovery, so start
                       //from scratch in Feed
      return 36;   //feed
    }
    else
    {
      m_RecovCount++;   //add 1 to time spend in this state
      return 23;  //recover
    }
  }
  else   //if still around after this long return to feeding
  {
    m_RecovCount=0;
    return 36;
  }
}
//---------------------------------------------------------------------------
/**
Roe_Male::MRun - Sets the male object to be in a running state - and checks if 
safe after 1 time step in this state.
Calls Running().
*/

int Roe_Male::MRun() //State 20
{
 #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MRun(): Deadcode warning!","");
    exit( 1 );
  }
  #endif

  m_MinInState[3]++; //add 1 to time spend in this state
  bool IsSafe=Running(m_danger_x,m_danger_y);
  if(m_IsDead) return 42; //Die
  else if (!IsSafe)   //still not safe after 1 time step
  {
    return 20;
  }
  else
  {
    m_RecovCount++; //add 1 to time spend in recovery
    return 23;   //safe
  }
}
//---------------------------------------------------------------------------
/**
Roe_Male::MIgnore - Ignore threath and adds 1 to time spend in this state. Return to same 
state as before disturbance
*/

int Roe_Male::MIgnore ()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MIgnore(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  m_MinInState[6]++; //add 1 to time spend in this state
  return m_LastState;
}
//----------------------------------------------------------------------------

/**
Roe_Male::MEvade - checks direction to threath and takes 50 steps in opposite direction.
Returns to same state as before disturbance.
Calls DirectionTo(), NextStep()
*/
int Roe_Male::MEvade ()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MEvade(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  m_MinInState[5]++; //add 1 to time spend in this state
  int weight=3;
  //get direction to threat
  int dir=DirectionTo(m_Location_x,m_Location_y,m_danger_x,m_danger_y);
  NextStep(weight,((dir+4) & 0x07),0);

  //return to same state as before disturbance
  return m_LastState;
}
//---------------------------------------------------------------------------
/**
Roe_Male::MFight -add 1 to time spend in this state and returns feeding state.
*/
int Roe_Male::MFight(void)  //State 30
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MFight(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  m_MinInState[7]++; //add 1 to time spend in this state
  return 36; //MFeed
}
//---------------------------------------------------------------------------
/**
Roe_Male::MAttendFemale - function for male attending female in heat. Checks if has mate, if not
return to feeding. If has a mate, male stays in this state (attend female) until new day. Then 
he returns to normal activity. The male will attend to the female, and follow her closely. He 
does not eat during that day and looses energy comparable to walking.
Calls SupplyPosition(), WalkTo().
*/
int Roe_Male::MAttendFemale (void)    //State 31
{
  //male stays in this state until new day. Then he returns to normal activity
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MAttendFemale(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  if(m_My_Mate==NULL) return 36;
  else
  {
    #ifdef JUJ__Debug3
    if(m_My_Mate->IsAlive()!=0x0DEADC0DE)
    {
      m_OurLandscape ->Warn("Roe_Male::MAttendFemale(): Deadcode warningm m_My_Mate!","");
      exit( 1 );
    }
    #endif
    m_MinInState[8]++; //add 1 to time spend in this state
    //allow roe to return after disturbance
    m_LastState=31;
    //While attending the female the male follows her closely. He does not eat
    //during that day and looses energy comparable to walking.
    //get females position
    AnimalPosition xy=m_My_Mate->SupplyPosition();
    WalkTo(xy.m_x,xy.m_y);
    return 31;  //Attendfemale
  }
}
//---------------------------------------------------------------------------
/**
Roe_Male::MOnNewDay - updates age of male object (+ 1 day), decides if life expectancy 
has been reached, which depends on age (Floaters have a higher mortality (added FloatMort)).
Checks whether dispersers and floaters can set up range by checking habitat, closeness to mothers range
and amount of males already in the area. If failed to find a range before october 1st become a floater.
Also governs establishement of territory dependent on age. Returns values to set the following states: 
establish range, establish territory, mate, update energy (and die).
Calls functions DistanceTo(), ScanGrid(), SupplyMaleRC().
*/
int Roe_Male::MOnNewDay(void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MOnNewDay(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  m_Age++;  //1 day older

  m_DestinationDecided=false;  //don't know yet whereto after this
  m_NewState=0;
  int dist;
  int mort;
  //First decide if life expectancy has been reached. Depends on age. Floaters
  //have a higher mortality (added FloatMort)

  long int die=random(100000);  //since mort rates are multiplied with 100000 to
                                //produce int values
  if (m_Age<=730)
    mort=(int)MaleDMR[0]; //age in days
  else if (m_Age<=2555)   //7 yrs
    mort=(int)MaleDMR[1];
  else if (m_Age<=3650)  //10 yrs
    mort=(int)MaleDMR[2];
  else mort =(int)MaleDMR[3]; //>10 yrs, 90 % mortality

  if((m_float)||(m_Disperse)) mort += FloatMort;
  if (die<mort)
  {
    return 42;  //MDie
  }
  else   //still alive
  {
    // 1: DISPERSAL
    if(m_Disperse)
    {
      if(!m_float)
      {
        //get distance to mothers RangeCentre
        dist=DistanceTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
      }
      else
      { //floaters check distance beck to last failed attempt to set up range
        dist = DistanceTo(m_Location_x,m_Location_y,m_float_x,m_float_y);
      }
      if (dist>MinDispDistance) //check if this is good spot to set up range
      {
         //both floaters and regular dispersers go in here
          bool found = false;
          int range = m_OurPopulation->ScanGrid(m_Location_x,m_Location_y,false);
          if (range > 0)  //OK
          {
            //how many other males have already their range in this area?
            ListOfMales* males = m_OurPopulation->SupplyMaleRC(m_Location_x,m_Location_y,range);
            if(males->size() < MaleDensityThreshold)   //OK
            {
              m_SearchRange=range;
              m_DestinationDecided=true;
              found=true;
              m_NewState= 8; // establish range
            }
            delete males;
          }
          if(!found) //quality too poor or still too close to mother, so return to disperse
          {
            //if failed to find a range before october 1st become a floater
            if(m_OurLandscape->SupplyDayInYear()>270)
            { //floaters store present position for use in next attempt to set up range
              m_float_x=m_Location_x;
              m_float_y=m_Location_y;
              m_float=true;
            }
            m_DestinationDecided=true;
            m_NewState=11;   //MDisperse
          }
      }
    }
    if(m_Age>=730) //can attempt to establish territory
    {
      if ((m_HaveTerritory==false)&&(m_HaveRange==true))

      {
          m_DestinationDecided=true;
          m_NewState=9;         //MEstablish territory
      }
    }
    if(m_My_Mate!=NULL)  //is attending female
    {
      return 40;  //mate
    }
  }
  return 5; //MUpdateEnergy
}
//---------------------------------------------------------------------------
/**
Roe_Male::MUpdateEnergy - Calculates how much energy is used (in cal/minute) in different activities.
Calculates net energy (gained-used) and determines whether excess enery is added to reserves or is used to
put on weight. A negative net energy causes the male object to loose weight - if roe deer weight falls
below a minimum value, the deer will die. If the roe deer is in very bad condition, forage time is increased.
returns states such as mate, feed, newState or die.

*/
int Roe_Male::MUpdateEnergy(void)  //State 5
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MUpdateEnergy(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //All energetic calculations in cal/minute!
  //Calculate how much energy used in different activities
  double EnergyUsed=0;
  int month=m_OurLandscape->SupplyMonth();
  for (int i=0; i<12; i++)
  {
    double cost;
    if (CostMaleAct[i]==1)   cost=CostMaleAct[i]*MaleRMR;  //rest activity
    else cost=(double) CostMaleAct[i];
    EnergyUsed += double ((cost * m_MinInState[i] * (m_Size*0.001)));
  }
  double result = m_EnergyGained - EnergyUsed;     //in cal
  if (result>0)
  {
    if (m_Reserves <= 10) m_Reserves++; //add to reserves
    else
    {
      m_Size += (int)floor(result * Anabolic_Inv);  //put on weight
    }
  }
  else if (result<0)
  {
    if (m_Reserves > 0)  m_Reserves--;
    else
    {
      m_Size += (int)floor(result*Catabolic_Inv);   //loose weight
    }
  }

  //Update m_Size. If m_Size < MinSize roe dies

  if ((m_Reserves == 0)&&(m_Size < m_OurPopulation->m_MinWeight_Males))
  {
    return 42;  //MDie
  }
  else if ((m_Reserves==0)&&(m_Size<m_OurPopulation->m_CriticalWeight_Males))
  {
    //in very bad condition, need to increase forage time
    m_LengthFeedPeriod = (int)((5*m_Size*0.001)/Male_FeedBouts[month-1]);
  }
  else
  {
    m_LengthFeedPeriod = (int)((2.3*m_Size*0.001)/
				    Male_FeedBouts[month-1]);
    if(m_LengthFeedPeriod>3) m_LengthFeedPeriod=3;
  }
  //set counter of daily energy gain to zero
  m_EnergyGained=0;
  for(int i=0;i<12;i++)
  {
    m_MinInState[i]=0;
  }
   //if LastState was AttendFemale, go to mate
   if (m_LastState==31)
   {
      return 40; //Mate
   }
   else if (m_NewState!=0) //otherwise it's directed to m_NewState
   {
      return m_NewState;
   }
   else return 36;  //feed
}
//---------------------------------------------------------------------------
/**
Roe_Male::MMate - add 1 to number of matings, stop attending female. Return to feed

*/
int Roe_Male::MMate (void)
{
  #ifdef JUJ__Debug1
  if(m_My_Mate==NULL)
  {
    m_OurLandscape ->Warn("Roe_Male::MMate: NULL pointer, m_My_Mate!","");
    exit( 1 );
  }
  #endif
  #ifdef JUJ__Debug3
  if((IsAlive()!=0x0DEADC0DE)|| (m_My_Mate->IsAlive()!=0x0DEADC0DE))
  {
    m_OurLandscape ->Warn("Roe_Male::MMate(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  m_MinInState[11]++; //add 1 to time spend in this state
  //add 1 to matings
  m_NoOfMatings++;
  //remove pointer to the female
  m_My_Mate=NULL;
  //last state must have been AttendFemale, so continue to Feed
  return 36;  //MFeed
}
//---------------------------------------------------------------------------
/**
Roe_Male::MDie - function for managing death of a male object. The male object
removes itself from all fightlists and deletes its own fightlist. The fixlist of the male object
is emptied and the male furthermore adds itself to list of dead animals. 
If a territoryholder sends message to every male in the neighbourhood abput death. Messages potential mate 
and satellite males about death. Function returns dead state.
Calls functions DeleteFightList(), RemoveFromFightList(), RemoveFixList()and On_IsDead().
*/
int Roe_Male::MDie (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MDie(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  m_CurrentStateNo = -1;
  //remove yourself from all fightlists and delete your own fightlist
  if(m_MyFightlist!=-1)
  {
    m_OurPopulation->DeleteFightlist(m_MyFightlist);
    m_MyFightlist=-1;
  }
  m_OurPopulation->RemoveFromFightlist(this);
  //empty the fixlist of this animal
  m_OurPopulation->RemoveFixList(m_FixlistNumber);
  
  //add yourself to list of dead animals
  for(int i=0;i<2000;i++)
  {
    if(m_OurPopulation->DeadList[i][0] == 0) //empty entrance
    {
      m_OurPopulation->DeadList[i][0] = 1;  //'1' for male, '2' for female
      m_OurPopulation->DeadList[i][1] = m_Age; //age in days
      break;
    }
  }
  //tell everybody who needs to know
  if(Mum!=NULL) //tell mum
  {
    #ifdef JUJ__Debug3
     if(Mum->IsAlive()!=0x0DEADC0DE)
     {
       m_OurLandscape ->Warn("Roe_Male::MDie(): Deadcode warning,Mum!","");
       exit( 1 );
     }
     #endif
     Mum->On_IsDead(this,3,false);  //'3' indicates offspring
     Mum=NULL;  //remove pointer
  }

  if (m_My_Mate!=NULL)   //tell mate
  {
     #ifdef JUJ__Debug3
     if(m_My_Mate->IsAlive()!=0x0DEADC0DE)
     {
       m_OurLandscape ->Warn("Roe_Male::MDie(): Deadcode warning, m_My_Mate!","");
       exit( 1 );
     }
     #endif
     m_My_Mate->On_IsDead(this,2,false);  //'2' indicates mate
     m_My_Mate=NULL;  //remove pointer
  }

  for(unsigned i=0;i<50;i++) //tell satellite males
  {
    if(m_MySatellites[i]!=NULL)
    {
      #ifdef JUJ__Debug3
      if(m_MySatellites[i]->IsAlive()!=0x0DEADC0DE)
      {
        m_OurLandscape ->Warn("Roe_Male::MDie(): Deadcode warning, m_MySatellites[i]!","");
        exit( 1 );
      }
      #endif
      m_MySatellites[i]->On_IsDead(this,1,false);    //'1' indicates male
    }
  }

  if(m_MyTMale!=NULL)  //tell host
  {
    #ifdef JUJ__Debug3
    if(m_MyTMale->IsAlive()!=0x0DEADC0DE)
    {
      m_OurLandscape ->Warn("Roe_Male::MDie(): Deadcode warning, m_MyTMale!","");
      exit( 1 );
    }
    #endif
    m_MyTMale->On_IsDead(this,1,false);
    m_MyTMale=NULL;  //remove pointer
  }
  //if a territoryholder every male in the neighbourhood needs to know
  if(m_HaveTerritory==true)
  {
    //get list of males in area
    ListOfMales* males = m_OurPopulation->SupplyMales(m_Location_x,m_Location_y,2*m_SearchRange);
    for(unsigned i=0;i<males->size();i++)
    {
      #ifdef JUJ__Debug3
      if((*males)[i]->IsAlive()!=0x0DEADC0DE)
      {
        m_OurLandscape ->Warn("Roe_Male::MDie(): Deadcode warning, (*males)[i]!","");
        exit( 1 );
      }
      #endif
      if((*males)[i]!=this) //don't send to yourself
      {
	(*males)[i]->On_IsDead(this,1, false);
      }
    }
    delete males;
  }
  return 45; //MDeathState
}
//---------------------------------------------------------------------------

/**
Roe_Male::BeginStep goes through all male objects in the populations. Male objects holding a territory,
(in the territorial season, March 1st - Sept. 1st) check territory for other male objects every 4 hours. 
OnRank() keep track of male ranks. These ranks lasts for the rest of 
that season (i.e. m_FightList is cleared once a year). Young satellite males always try to stay 
clear of their older host and check distance every timestep and evade if too close
Calls SupplyMonth(), SupplyMales(), On_Rank(), AddToFightList(), SupplyPosition(), DistanceTo().
*/
void Roe_Male::BeginStep (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::BeginStep(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
 if (CurrentRState==rds_MDeathState) return;
 //In the territorial season (March 1st - Sept. 1st) territoryholders have to
 //check their area for other males. Whenever they meet a new male ranks
 //are established, which lasts for the rest of that season (i.e. m_FightList is
 //cleared once a year).
 //TMales search the area every 4 hours.
 int month=m_OurLandscape->SupplyMonth();
 if((month>=3)&&(month<=8))
 {
   if((m_HaveTerritory==true)&&(timestep%24==0))
   {
     //get other males in area
     ListOfMales* males = m_OurPopulation->SupplyMales(m_Location_x,m_Location_y,m_SearchRange+100);
     if(males!=NULL)  //only if other males are present
     {
       for(unsigned i=0;i<males->size();i++)
       {
         if((*males)[i]!=this)  //don't challenge yourself
         {
           (*males)[i]->On_Rank(this,(double) m_Size,m_Age,m_NoOfMatings,true,true); //need to establish rank
           //add each to fightlist
           m_NumberOfFights++;
           m_MinInState[7]++;  //to place cost per fight in update energy
           m_MyFightlist=m_OurPopulation->AddToFightlist((*males)[i],m_MyFightlist);
         }
       }
     }
     delete males;
   }
 }
 //Young satellite males always try to stay clear of their older host
 //check distance every timestep and evade if too close
 if((m_MyTMale!=NULL)&&(m_MyTMale->IsAlive()==0x0DEADC0DE))  //young satellite male
 {
    AnimalPosition xy=m_MyTMale->SupplyPosition();
    if(DistanceTo(m_Location_x,m_Location_y,xy.m_x,xy.m_y) < 50)
    {
      CurrentRState=rds_MEvade;
    }
 }
}
//---------------------------------------------------------------------------
/**
Roe_Male::Step - function called every time step. The functions checks if done with a given state (m_StepDone)
and governs states and transitions to other states. Calls functions: MOnMature(), MDie(), MOnNewDay(),
MMate(), MEstablishRange(), MEstablishTerritory(), MDisperse(), MFeed(), MRecover(), MRUn(), MEvade(),
MIgnore(), MRuminate(), MFight(), MAttendFemale(), MUpdateEnergy()
*/
void Roe_Male::Step (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::Step(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  
  if (m_StepDone || (CurrentRState == rds_MDeathState)) return;

 switch (CurrentRState)
 {
   case rds_Initialise:
    CurrentRState=rds_MOnMature;
    break;

   case rds_MOnMature:
    switch(MOnMature())
    {
      case 11:
        CurrentRState=rds_MDisperse;
        break;
      case 8:
        CurrentRState=rds_MEstablishRange;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Male::Step():rds_Initialise:No matching case","");
        exit( 1 );
        break;
    }
    m_StepDone=true; 
    break;

   case rds_MDie:
    switch (MDie())
    {
      case 45:
        CurrentRState=rds_MDeathState;
		m_StepDone=true;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Male::Step():rds_MDie:No matching case","");
        exit( 1 );
        break;
    }
   m_StepDone=true;
    break;

   case rds_MOnNewDay:
    switch (MOnNewDay())
    {
     case 5: //rds_MUpdateEnergy
       CurrentRState=rds_MUpdateEnergy;
       break;
     case 40:
       CurrentRState=rds_MMate;
       break;
     case 42:  //rds_MDie
       CurrentRState=rds_MDie;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MOnNewday:No matching case","");
        exit( 1 );
       break;
    }
    break;
   case rds_MMate:
    switch (MMate())
    {
      case 36:
        CurrentRState=rds_MFeed;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Male::Step():rds_MMate:No matching case","");
        exit( 1 );
        break;
    }
   m_StepDone=true;
    break;
   case rds_MEstablishRange:
    switch(MEstablishRange())
    {
     case 36:  //rds_MFeed
       CurrentRState=rds_MFeed;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MEstablishRange:No matching case","");
        exit( 1 );
       break;
    }
   m_StepDone=true;
    break;
   case rds_MEstablishTerritory:
    switch(MEstablishTerritory())
    {
     case 36: //rds_MFeed
       CurrentRState=rds_MFeed;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MEstablishTerritory:No matching case","");
        exit( 1 );
       break;
    }
    m_StepDone=true;
    break;
   case rds_MDisperse:
    switch (MDisperse())
    {
     case 36: //rds_MFeed
       CurrentRState=rds_MFeed;
       m_StepDone=true;
       break;
     case 8: //rds_MEstablishRange
       CurrentRState=rds_MEstablishRange;
       m_StepDone=true;
       break;
     case 11: //rds_MDisperse
       CurrentRState=rds_MDisperse;
       m_StepDone=true;
       break;
     case 42:
       CurrentRState=rds_MDie;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MDisperse:No matching case","");
        exit( 1 );
       break;
    }
    break;
   case rds_MFeed:
    switch (MFeed())
    {
     case 11: //rds_MDisperse
       CurrentRState=rds_MDisperse;
       m_StepDone=true;
       break;
     case 33: //rds_MRuminate
       CurrentRState=rds_MRuminate;
       m_StepDone=true;
       break;
     case 36: //rds_MFeed
       CurrentRState=rds_MFeed;
       m_StepDone=true;
       break;
     case 42: //rds_MFeed
       CurrentRState=rds_MDie;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MFeed:No matching case","");
        exit( 1 );
       break;
    }
    break;
   case rds_MRecover:
    switch(MRecover())
    {
     case 36: //rds_MFeed:
       CurrentRState=rds_MFeed;
       break;
     case 23: //rds_MRecover:
       CurrentRState=rds_MRecover;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MRecover:No matching case","");
        exit( 1 );
       break;
    }
    m_StepDone=true;
    break;
   case rds_MRun:
    switch(MRun())
    {
     case 23: //rds_MRecover:
       CurrentRState=rds_MRecover;
       break;
     case 20: //rds_MRun
       CurrentRState=rds_MRun;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MRun:No matching case","");
        exit( 1 );
       break;
    }
    m_StepDone=true;
    break;
   case rds_MEvade:
     switch (MEvade())
     {
       case 11:
         CurrentRState=rds_MDisperse;
         break;
       case 23:
         CurrentRState=rds_MRecover;
         break;
       case 31:
         CurrentRState= rds_MAttendFemale;
         break;
       case 33:
         CurrentRState= rds_MRuminate;
         break;
       case 36:
         CurrentRState= rds_MFeed;
         break;
       default:
         m_OurLandscape ->Warn("Roe_Male::Step():rds_MEvade:No matching case","");
         exit( 1 );
         break;
     }
     m_StepDone=true;
     break;
   case rds_MIgnore:
     switch (MIgnore())
     {
       case 11:
         CurrentRState=rds_MDisperse;
         break;
       case 23:
         CurrentRState=rds_MRecover;
         break;
       case 31:
         CurrentRState= rds_MAttendFemale;
         break;
       case 33:
         CurrentRState= rds_MRuminate;
         break;
       case 36:
         CurrentRState= rds_MFeed;
         break;
       default:
         m_OurLandscape ->Warn("Roe_Male::Step():rds_MIgnore:No matching case","");
        exit( 1 );
     }
     m_StepDone=true;
     break;
   case rds_MRuminate:
    switch (MRuminate())
    {
     case 11:
       CurrentRState=rds_MDisperse;
       m_StepDone=true;
       break;
     case 36: //rds_MFeed:
       CurrentRState=rds_MFeed;
       m_StepDone=true;
       break;
     case 33: //rds_MRuminate:
       CurrentRState=rds_MRuminate;
       m_StepDone=true;
       break;
     case 42:
       CurrentRState=rds_MDie;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MRuminate:No matching case","");
        exit( 1 );
       break;
    }
    break;
   case rds_MFight:
    switch (MFight())
    {
     case 36: //rds_MFeed:
       CurrentRState=rds_MFeed;
       break;
     case 31: //rds_MAttendFemale:
       CurrentRState=rds_MAttendFemale;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_Mfight:No matching case","");
        exit( 1 );
       break;
    }
   m_StepDone=true;
    break;
   case rds_MAttendFemale:
    switch (MAttendFemale())
    {
      case 36:  //Feed
        CurrentRState=rds_MFeed;
        m_StepDone=true;
        break;
      case 31: //rds_MAttendFemale:
        CurrentRState=rds_MAttendFemale;
        break;
      default:
        m_OurLandscape ->Warn("Roe_Male::Step():rds_MAttendFemale:No matching case","");
        exit( 1 );
        break;
    }
    m_StepDone=true;
    break;
   case rds_MUpdateEnergy:
    switch (MUpdateEnergy())
    {
     case 8: //rds_MEstablishRange
       CurrentRState=rds_MEstablishRange;
       m_StepDone=true;
       break;
     case 9: //rds_MEstablishTerritory
       CurrentRState=rds_MEstablishTerritory;
       m_StepDone=true;
       break;
     case 11: //rds_MDisperse
       CurrentRState=rds_MDisperse;
       m_StepDone=true;
       break;
     case 36: //rds_MFeed
       CurrentRState=rds_MFeed;
       m_StepDone=true;
       break;
     case 42: //rds_MDie:
       CurrentRState=rds_MDie;
       break;
     case 40: //rds_MMate:
       CurrentRState=rds_MMate;
       break;
     default:
       m_OurLandscape ->Warn("Roe_Male::Step():rds_MUpdateEnergy:No matching case","");
        exit( 1 );
       break;
    }
    break;
   default:
     m_OurLandscape ->Warn("Roe_Male::Step():No matching case","");
        exit( 1 );
 }
}
//---------------------------------------------------------------------------
/**
Roe_Male::EndStep - checks if male object is dead and adds to number of timesteps.
If 1 whole day has passed, current state is set to FOnNewDay
*/
void Roe_Male::EndStep (void)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::EndStep():Deadcode warning!","");
        exit( 1 );
  }
  #endif
 if (CurrentRState==rds_MDeathState) return;
 
 timestep++;
 if (m_OurPopulation->StepCounter %144==0) //1 day has passed
 {
    m_weightedstep=0;
    CurrentRState=rds_MOnNewDay;
 }

}
//---------------------------------------------------------------------------
/**
Roe_Male::Roe_Male -  constructor for male object. Sets all attributes. It also adds fawns
to mother's offspring list.

*/
Roe_Male::Roe_Male (int x, int y,int size,int age,Roe_Female* mum, Landscape* L, RoeDeer_Population_Manager* RPM):Roe_Adult_Base (x,y,size,age,L,RPM)
{
  m_Age = age;
  m_Sex=true;
  Mum=mum;
  //add yourself to mothers offspring list
  if(Mum!=NULL)
  {
    #ifdef JUJ__Debug3
    if(Mum->IsAlive()!=0x0DEADC0DE)
    {
      m_OurLandscape ->Warn("Roe_Male::Roe_Male():Deadcode warning, Mum","");
        exit( 1 );
    }
    #endif
    for(int i=0;i<20;i++)
    {
      if(Mum->m_MyYoung[i]==NULL)
      {
        Mum->m_MyYoung[i]=this;
        break;
      }
    }
  }
  m_HaveTerritory = false;
  m_HaveRange = false;
  m_My_Mate = NULL;
  m_MyTMale = NULL;
  m_Reserves = 30;
  m_IsDead=false;
  m_LastState=36; //Feed. Could be any safe state
  m_NoOfMatings = 0;
  m_NumberOfFights = 0;
  m_MyFightlist=-1;  //created when needed
  if(size==-1) //1st generation roe
  {
    int rand = random(2000);
    m_Size = 17000 + rand;  //just for the first generation, to make sure that
                          //some are dispersers and some are not
  }
  else m_Size=size;
  for (int i=0;i<12;i++) m_MinInState[i] = 0;
  for(unsigned i=0;i<50;i++)   m_MySatellites[i]=NULL;
}
//---------------------------------------------------------------------------
Roe_Male::~Roe_Male ()
{
  m_My_Mate=NULL;
  Mum=NULL;
}
//--------------------------------------------------------------------------
/**
Roe_Male::On_ApproachOfDanger - Determines whether to run or evade a threat depending on
cover and distance to threath. In good cover, male will run if threath is less than 30 m 
away and else perform evasive behavior, In intermediate cover the flight distance is 50 m, 
and in poor cover it is 70 m.
Returns states evade or run.
Calls Cover(), DistanceTo().
*/
void Roe_Male::On_ApproachOfDanger(int p_To_x,int p_To_y)
{
  m_danger_x=p_To_x;
  m_danger_y=p_To_y;
  //get cover
  int cov=Cover(m_Location_x,m_Location_y);
  //get distance to threat
  int dist=DistanceTo(m_Location_x, m_Location_y,p_To_x,p_To_y);

  if (cov >= CoverThreshold2) //good cover
  {
    if(dist >= 30)
    {
      CurrentRState=rds_MEvade;
    }
    else CurrentRState=rds_MRun;
  }
  else if (cov >= CoverThreshold1)  //intermediate cover
  {
    if(dist>=50)
    {
      CurrentRState=rds_MEvade;
    }
    else  CurrentRState=rds_MRun;
  }
  else //poor cover
  {
    if(dist>=70)
    {
      CurrentRState=rds_MEvade;
    }
    else  CurrentRState=rds_MRun;
  }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
/**
Roe_Male::On_IsDead - governs a male objects "response" to death of another object. Checks
who is dead (mom, male, mate) and sets them to NULL. If a male object is dead, the "responding" male object
checks if dead object is host ("responding" object is satellite male). If so it will send message 
to the dead host's other satellites. Rank is checked among satellites and if "responding" male wins a fight,
he will take over territory of dead male. If the dead object is a satellite of the "responding" male, it will
be removed from satellite list. If dead object holds a territory and "responding" male is not a satellite to the dead
object but is also not a territory holder, he can challenge nearby interested males (also without territory)
and win territory. Herefter if satellite to another host, the "responding" male will send a message and 
remove itself from satellite list.
If dead object is the "responding" male's mate, the "responding" male will seize to attend the female.
Calls On-Rank(), SupplyInfo(), SupplyMales(), RemoveSatellites().

*/
void Roe_Male::On_IsDead(Roe_Base* Base, int whos_dead, bool fawn)
{
  #ifdef JUJ__Debug3
  if((IsAlive()!= 0x0DEADC0DE)||(Base->IsAlive()!= 0x0DEADC0DE))
  {
    m_OurLandscape ->Warn("Roe_Male::On_IsDead():Deadcode warning!","");
    exit( 1 );
  }
  #endif
  Roe_Male* male;
  ListOfMales* rivals;
  RoeDeerInfo info;
  bool rank=true;
  bool found=false;
   //whos_dead: 0=mum 1=other male, 2=mate
 switch(whos_dead)
 {
   case 0:  //mum
     if(Base!=Mum)
     {
        m_OurLandscape ->Warn("Roe_Male::On_IsDead():Bad pointer, Base!","");
        exit( 1 );
     }
     else Mum=NULL;
     break;
   case 1: //a male is dead, check if he is your host
     if (Base==m_MyTMale)
     {
       for(unsigned i=0;i<50;i++) //
       {  //send message to any other satellites
	       if(m_MyTMale->m_MySatellites[i]!=NULL)
         {
           #ifdef JUJ__Debug3
           if(m_MyTMale->m_MySatellites[i]->IsAlive()!= 0x0DEADC0DE)
           {
             m_OurLandscape ->Warn("Roe_Male::On_IsDead():Deadcode warning, m_MySatellites!","");
             exit( 1 );
           }
           #endif
	        rank=m_MyTMale->m_MySatellites[i]->
                          On_Rank(this,(double) m_Size,m_Age,m_NoOfMatings,true,false);
         }
         if (rank==false) break;  //lost
       }
       if(rank==true)  //take this territory
       {
         m_HaveTerritory=true;
         m_Disperse=false;
         m_DispCount=0;
         m_MinInState[1] += 1;
       }
       m_MyTMale=NULL; //delete pointer
     }
     else //dead male is not your host
     {
       //first check if he is one of your satellites
       for(unsigned i=0;i<50;i++)
       {
         if(m_MySatellites[i]==Base)
         {
           m_MySatellites[i]=NULL;
           found=true;
           break;
         }
       }
       if(found==false) //not a satellite male
       {
         male=dynamic_cast<Roe_Male*> (Base);
         info=male->SupplyInfo();
         //If he is a territory holder and you're not, you can try to take over
         if((m_HaveTerritory==false)&&(male->SupplyHaveTerritory()==true))
         {
           //charge other males in the area, who might be interested
           rivals = m_OurPopulation->SupplyMales(info.m_Range_x,info.m_Range_y,2*m_SearchRange);
           for(unsigned i=0;i<rivals->size();i++)
           {
              #ifdef JUJ__Debug3
              if ((*rivals)[i]->IsAlive()!= 0x0DEADC0DE)
              {
                m_OurLandscape ->Warn("Roe_Male::On_IsDead():Deadcode warning, (*rivales)[i]!","");
                exit( 1 );
              }
              #endif
              if((*rivals)[i]!=Base) //no point in challenging the dead male
              {
                rank=(*rivals)[i]->On_Rank(this,(double) m_Size,m_Age,
				                   m_NoOfMatings,false,false);
              }
              if (rank==false) break;  //lost, at least 1 male is interested and stronger than you
           }
           delete rivals;
           if(rank==true)  //won, can take over
           {
             m_HaveTerritory=true;
             m_Disperse=false;
             m_DispCount=0;
             m_MinInState[1] += 1;
             m_RangeCentre_x=info.m_Range_x;
             m_RangeCentre_y=info.m_Range_y;
             if(m_MyTMale!=NULL) //you have a host, so tell him
             {
               m_MyTMale->RemoveSatellite(this);
               m_MyTMale=NULL;
             }
           }
         }
       }
     }
     break;

   case 2://sender is your mate
     m_My_Mate=NULL;
     //give up attending female
     CurrentRState=rds_MFeed;
     break;

   default:
     m_OurLandscape ->Warn("Roe_Male::On_IsDead():No matching case!","");
     exit( 1 );
 } //end of switch
}


//-----------------------------------------------------------------------------
/**
Roe_Male::On_InHeat - response of male object to "In Heat" message sent by female object. Check if already
have mate and if not check if distance to female is too large. If not too large, the male checks for rivals
around, checks rank (also dependent on whether he is in his own territory), challenges them and adds them to 
fightlist. If the male wins, he will attend the female otherwise he will give up attending this female.
Calls SupplyTMales(), DistanceTo(), On_Rank(), AddToFightList().
*/

bool Roe_Male::On_InHeat(Roe_Female* female,int p_x,int p_y)
{
  #ifdef JUJ__Debug3
  if((IsAlive()!= 0x0DEADC0DE)||(female->IsAlive()!= 0x0DEADC0DE))
  {
    m_OurLandscape ->Warn("Roe_Male::On_InHeat():Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //want to attend this female if not attending one already
  if (m_My_Mate == NULL)
  {
    WalkTo(p_x,p_y);
    int dist = DistanceTo(m_Location_x,m_Location_y,p_x,p_y);
    if (dist < 50)   //close enough to female,
    //now find out if there are potential rivals around
    {
      ListOfMales * Tmales = m_OurPopulation->SupplyTMales(m_Location_x,
                                                           m_Location_y,100);
      bool home=false;
      bool rank=true; //initiated to true and only altered if there are stronger rivals
      //are you still in your own territory?
      if (DistanceTo(m_Location_x,m_Location_y,
                                 m_RangeCentre_x,m_RangeCentre_y)<400)
      {
        home=true;  //yes
      }

      for(unsigned i=0;i<Tmales->size();i++)  //foe every male in area
      {
        m_NumberOfFights++;
        //challenge him and add him to fightlist
        rank=(*Tmales)[i]->On_Rank(this,(double) m_Size, m_Age, m_NoOfMatings,home,false);
        m_MyFightlist=m_OurPopulation->AddToFightlist((*Tmales)[i],m_MyFightlist);
        if (rank==false)  break;  //lost...give up attending this female
      }
      if (rank==true)
      {
         #ifdef JUJ__Debug1
         if(female==NULL)
         {
           m_OurLandscape ->Warn("Roe_Male::On_InHeat():NULL pointer, female!","");
           exit( 1 );
         }
         #endif

         m_My_Mate = female;
         CurrentRState=rds_MAttendFemale; //attend female
      }
      delete Tmales;
      return rank; //also returns true if no rivals around
    }
    else return false; //still too far away
  }
  else return false; //will not attend
}
//----------------------------------------------------------------------------
/**
Roe_Male::AddSatellite - adds satellite male to a male object's satellite male list.
*/

void Roe_Male::AddSatellite(Roe_Male* sat)
{
  #ifdef JUJ__Debug3
  if(sat->IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::AddSatellite():Deadcode warning, sat!","");
    exit( 1 );
  }
  #endif
  bool found=false;
  //add new satellite male to your list
  for(unsigned i=0;i<50;i++)
  {
    if(m_MySatellites[i]==NULL)
    {
      m_MySatellites[i]=sat;
      found=true;
      break;
    }
  }
  if(!found)
  {
    m_OurLandscape ->Warn("Roe_Male::AddSatellite():List is full!","");
    exit( 1 );
  }
}
//---------------------------------------------------------------------------
/**
Roe_Male::RemoveSatellite - clears the list of satellite males that belongs to a male object
*/
void Roe_Male::RemoveSatellite(Roe_Male* sat)
{
  #ifdef JUJ__Debug3
  if(sat->IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::RemoveSatellite():Deadcode warning, sat!","");
    exit( 1 );
  }
  #endif
  bool found=false;
  //remove satellite male from your list
  for(unsigned i=0;i<50;i++)
  {
    if(m_MySatellites[i]==sat)
    {
      m_MySatellites[i]=NULL;
      found=true;
      break;
    }
  }
  if(found==false)
  {
    m_OurLandscape ->Warn("Roe_Male::RemoveSatellite():satellite not found!","");
    exit( 1 );
  }
}
//---------------------------------------------------------------------------
/*
Roe_Male::ClearFightlist - clears fight list.
*/
void Roe_Male::ClearFightlist()
{
  m_MyFightlist=-1;
  m_NumberOfFights=0;
}
//---------------------------------------------------------------------------
/**
Roe_Male::On_Rank is used to establish ranks between male object and rival male object. 
The rank is dependent on male size, age, whether the males is in his own territory,
number of previous matings,and number of previous  fights. It also checks for location 
(if in adversarys territory, winning male can expell territory holder - and call On_Expelled()).
If male object is taking over a territory from a rival male, the male object will receive the 
rival's list of satllite males.
Calls AddToFightlist(), DistanceTo(), SupplyInfor(), On_Expelled(), RemoveSatellite().
*/
bool Roe_Male::On_Rank(Roe_Male* rival,double size,int age,int matings,
                                                        bool where, bool range)
{
  int MyScore=0;
  int HisScore=0;
  bool outcome = true;
  m_NumberOfFights++;
  m_MinInState[7]++;

  //add rival to your fightlist
  m_MyFightlist=m_OurPopulation->AddToFightlist(rival,m_MyFightlist);

  //Establish rank:
  //size
  if(m_Size>size) MyScore++;
  else if (size>m_Size) HisScore++;
  else
  {
    MyScore++;
    HisScore++;
  }
  //age
  if (m_Age==age)
  {
    MyScore++;
    HisScore++;
  }
  else
  {
    if ((m_Age<=MalePrime)&&(age<=MalePrime))  //both young
    {
      if (m_Age>age)  MyScore++;  //advantage to older roe
      else HisScore++;
    }
    else if  ((m_Age>MalePrime)&&(age>MalePrime))  //both old
    {
      if (m_Age>age)   HisScore++;
      else MyScore++;
    }
    else if ((m_Age>MalePrime)&&(age<=MalePrime)) HisScore++;
    else MyScore++;
  }
  //matings
  if (m_NoOfMatings==matings)
  {
    MyScore++;
    HisScore++;
  }
  else
  {
    if (m_NoOfMatings>matings) MyScore++;
    else HisScore++;
  }
  //where are we
  //first check if you are in own territory (< 400 m from rangecentre)
  bool home=false;
  if ((DistanceTo(m_Location_x,m_Location_y,m_RangeCentre_x,m_RangeCentre_y)<=400)
                                                           &&(!m_float))
  {
    home=true;
  }
  if ((home==true)&&(where==true))  //in overlap area
  {
    MyScore++;
    HisScore++;
  }
  else   //one or both are false
  {
    if (home==true) MyScore++;
    else if (where==true) HisScore++;
  }
  //now sum up score
  if (MyScore==HisScore)    //random outcome
  {
    int rand=random(100);
    if(rand<50)    //I win
    {
       outcome=false;
    }
    else outcome=true;    //rival wins
  }
  else if (MyScore>HisScore)
  {
    outcome=false;  //I win
    //if this is a territory fight, I can expell him from his territory if I want to
    if ((range==true)&&(m_HaveTerritory==false))
    {
      #ifdef JUJ__Debug1
      if(rival==NULL)
      {
        m_OurLandscape ->Warn("Roe_Male::On_Rank():NULL pointer, rival!","");
        exit( 1 );
      }
      #endif
      //rival tells his other satellites to add themselves
      //to your list. You just need to delete pointer
      if(m_MyTMale!=NULL)
      {
        m_MyTMale->RemoveSatellite(this);
        m_MyTMale=NULL;
      }
      rival->On_Expelled(this);
      RoeDeerInfo info=rival->SupplyInfo();
      m_RangeCentre_x=info.m_Range_x;
      m_RangeCentre_y=info.m_Range_y;
      m_HaveTerritory=true;
      m_HaveRange=true;
      m_Disperse=false;
      m_DispCount=0;
      m_float=false;
    }
  }
  else outcome=true; //rival wins
  return outcome;
}
//--------------------------------------------------------------------------
/**
Following the On_Rank() function - if a male territory holder loses a fight and 
gets expelled from his territory. Clears satellite list, sets object to be a floater 
and current state to dispersal
Calls OnNewHost().
*/
void Roe_Male::On_Expelled(Roe_Male* rival)
{
  #ifdef JUJ__Debug3
  if(rival->IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::On_Expelled():Deadcode warning, rival!","");
    exit( 1 );
  }
  #endif
  //for the moment floaters are just send dispersing, they can establish new range
  //somewhere else.
  //remove pointers to any satellite males

  for(unsigned i=0;i<50;i++)
  {
    if(m_MySatellites[i]!=NULL)
    {
      #ifdef JUJ__Debug3
      if(m_MySatellites[i]->IsAlive()!=0x0DEADC0DE)
      {
         m_OurLandscape ->Warn("Roe_Male::On_Expelled():Deadcode warning,m_Mysatellites!","");
         exit( 1 );
      }
      #endif
      if(m_MySatellites[i]!=rival)
      {
        m_MySatellites[i]->On_NewHost(rival); //they add themselves to the new list
      }

      m_MySatellites[i]=NULL; //set all to NULL including rival
    }
  }
  m_float=true;
  m_float_x=m_Location_x;
  m_float_y=m_Location_y;
  CurrentRState=rds_MDisperse; //floater
}
//---------------------------------------------------------------------------
/**
Roe_Male::On_NewHost - assigns new host to satellite male by adding satellite to host's satellite
list and setting host to new host.
Calls AddSatellite().
*/
void Roe_Male::On_NewHost(Roe_Male* host)
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::On_NewHost():Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //you have got a new host, the territory attributes stays the same
  m_MyTMale=host;
  m_MyTMale->AddSatellite(this); //tell him to add you to his list
}
