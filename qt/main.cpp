#include "mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    setlocale(LC_NUMERIC,"C");
    MainWindow w;
    // Uncomment to maximize window on startup
    //w.setWindowState(w.windowState() ^ Qt::WindowMaximized);
    w.show();

    return a.exec();
}
