#include <QString>

#include "habitatmap.h"
#include "../Landscape/ls.h"

#include "../RodenticideModelling/RodenticidePredators.h"
#include "../BatchALMaSS/PopulationManager.h"

// This is horrible. Just used to access sub box in debug pane
// Try and get rid.
#include "mainwindow.h"
#include "colmap.h"

extern CfgBool l_pest_enable_pesticide_engine;
extern CfgInt l_pest_NoPPPs;
extern CfgBool cfg_rodenticide_enable;
extern CfgBool cfg_fixed_random_sequence;
static CfgBool cfg_animate( "G_ANIMATE", CFG_CUSTOM, true );
static CfgBool cfg_dumpmap( "G_DUMPMAP", CFG_CUSTOM, true ); // Produces a set of bitmaps using the next three cfgs to determine start, range and interval
static CfgInt cfg_dumpmapstart( "G_DUMPMAPSTART", CFG_CUSTOM, 366  );
static CfgInt cfg_dumpmapend( "G_DUMPMAPEND", CFG_CUSTOM, 730 );
static CfgInt cfg_dumpmapstep( "G_DUMPMAPSTEP", CFG_CUSTOM, 5 );


palette::palette()
{
    for (int i=0; i < 100; ++i)
    {

        /*
        red.push_back(random(255));
        green.push_back(random(255));
        blue.push_back(random(255));
    */

      red.push_back(255);
      green.push_back(255);
      blue.push_back(255);

    }


    almasscmap::colourmap colours = almasscmap::makeColourMap();
    almasscmap::rgbarray colour;

    colour=colours["CORAL"];
    red[tole_Hedges]=colour[0];
    green[tole_Hedges]=colour[1];
    blue[tole_Hedges]=colour[2];

    colour=colours["GOLD"];
    red[tole_RoadsideVerge]=colour[0];
    green[tole_RoadsideVerge]=colour[1];
    blue[tole_RoadsideVerge]=colour[2];

    colour=colours["DARK ORCHID"];
    red[tole_Railway]=colour[0];
    green[tole_Railway]=colour[1];
    blue[tole_Railway]=colour[2];

    colour=colours["PALE GREEN"];
    red[tole_FieldBoundary]=colour[0];
    green[tole_FieldBoundary]=colour[1];
    blue[tole_FieldBoundary]=colour[2];

    colour=colours["SIENNA"];
    red[tole_Marsh]=colour[0];
    green[tole_Marsh]=colour[1];
    blue[tole_Marsh]=colour[2];

    colour=colours["THISTLE"];
    red[tole_Scrub]=colour[0];
    green[tole_Scrub]=colour[1];
    blue[tole_Scrub]=colour[2];

    colour=colours["WHEAT"];
    red[tole_Field]=colour[0];
    green[tole_Field]=colour[1];
    blue[tole_Field]=colour[2];

    colour=colours["MEDIUM SEA GREEN"];
    red[tole_PermPastureLowYield]=colour[0];
    green[tole_PermPastureLowYield]=colour[1];
    blue[tole_PermPastureLowYield]=colour[2];

    colour=colours["SPRING GREEN"];
    red[tole_PermPastureTussocky]=colour[0];
    green[tole_PermPastureTussocky]=colour[1];
    blue[tole_PermPastureTussocky]=colour[2];

    colour=colours["KHAKI"];
    red[tole_PermanentSetaside]=colour[0];
    green[tole_PermanentSetaside]=colour[1];
    blue[tole_PermanentSetaside]=colour[2];

    // Don't have the rgb for MFG
    colour=colours["MEDIUM FOREST GREEN"];
//    colour=colours["RED"];
    red[tole_PermPasture]=colour[0];
    green[tole_PermPasture]=colour[1];
    blue[tole_PermPasture]=colour[2];

    colour=colours["YELLOW"];
    red[tole_NaturalGrassDry]=colour[0];
    green[tole_NaturalGrassDry]=colour[1];
    blue[tole_NaturalGrassDry]=colour[2];

    colour=colours["YELLOW GREEN"];
    red[tole_RiversidePlants]=colour[0];
    green[tole_RiversidePlants]=colour[1];
    blue[tole_RiversidePlants]=colour[2];

    colour=colours["FIREBRICK"];
    red[tole_PitDisused]=colour[0];
    green[tole_PitDisused]=colour[1];
    blue[tole_PitDisused]=colour[2];

    colour=colours["SEA GREEN"];
    red[tole_RiversideTrees]=colour[0];
    green[tole_RiversideTrees]=colour[1];
    blue[tole_RiversideTrees]=colour[2];

    colour=colours["FOREST GREEN"];
    red[tole_DeciduousForest]=colour[0];
    green[tole_DeciduousForest]=colour[1];
    blue[tole_DeciduousForest]=colour[2];

    colour=colours["DARK OLIVE GREEN"];
    red[tole_ConiferousForest]=colour[0];
    green[tole_ConiferousForest]=colour[1];
    blue[tole_ConiferousForest]=colour[2];

    colour=colours["DARK GREEN"];
    red[tole_MixedForest]=colour[0];
    green[tole_MixedForest]=colour[1];
    blue[tole_MixedForest]=colour[2];

    colour=colours["GREEN YELLOW"];
    red[tole_YoungForest]=colour[0];
    green[tole_YoungForest]=colour[1];
    blue[tole_YoungForest]=colour[2];

    colour=colours["GREY"];
    red[tole_StoneWall]=colour[0];
    green[tole_StoneWall]=colour[1];
    blue[tole_StoneWall]=colour[2];

    colour=colours["MEDIUM ORCHID"];
    red[tole_Garden]=colour[0];
    green[tole_Garden]=colour[1];
    blue[tole_Garden]=colour[2];

    colour=colours["ORCHID"];
    red[tole_Track]=colour[0];
    green[tole_Track]=colour[1];
    blue[tole_Track]=colour[2];

    colour=colours["INDIAN RED"];
    red[tole_SmallRoad]=colour[0];
    green[tole_SmallRoad]=colour[1];
    blue[tole_SmallRoad]=colour[2];

    colour=colours["RED"];
    red[tole_LargeRoad]=colour[0];
    green[tole_LargeRoad]=colour[1];
    blue[tole_LargeRoad]=colour[2];

    colour=colours["BLACK"];
    red[tole_Building]=colour[0];
    green[tole_Building]=colour[1];
    blue[tole_Building]=colour[2];

    colour=colours["BROWN"];
    red[tole_Saltmarsh]=colour[0];
    green[tole_Saltmarsh]=colour[1];
    blue[tole_Saltmarsh]=colour[2];

    colour=colours["DARK GREY"];
    red[tole_ActivePit]=colour[0];
    green[tole_ActivePit]=colour[1];
    blue[tole_ActivePit]=colour[2];

    colour=colours["BLUE"];
    red[tole_Freshwater]=colour[0];
    green[tole_Freshwater]=colour[1];
    blue[tole_Freshwater]=colour[2];

    colour=colours["CORNFLOWER BLUE"];
    red[tole_River]=colour[0];
    green[tole_River]=colour[1];
    blue[tole_River]=colour[2];

    colour=colours["GOLDENROD"];
    red[tole_Coast]=colour[0];
    green[tole_Coast]=colour[1];
    blue[tole_Coast]=colour[2];

    colour=colours["AQUAMARINE"];
    red[tole_Saltwater]=colour[0];
    green[tole_Saltwater]=colour[1];
    blue[tole_Saltwater]=colour[2];

    colour=colours["GREEN"];
    red[tole_HedgeBank]=colour[0];
    green[tole_HedgeBank]=colour[1];
    blue[tole_HedgeBank]=colour[2];

    colour=colours["BLUE VIOLET"];
    red[tole_Heath]=colour[0];
    green[tole_Heath]=colour[1];
    blue[tole_Heath]=colour[2];

    colour=colours["PURPLE"];
    red[tole_Orchard]=colour[0];
    green[tole_Orchard]=colour[1];
    blue[tole_Orchard]=colour[2];

    colour=colours["DARK SLATE BLUE"];
    red[tole_OrchardBand]=colour[0];
    green[tole_OrchardBand]=colour[1];
    blue[tole_OrchardBand]=colour[2];

    colour=colours["MAROON"];
    red[tole_MownGrass]=colour[0];
    green[tole_MownGrass]=colour[1];
    blue[tole_MownGrass]=colour[2];

    colour=colours["MEDIUM SPRING GREEN"];
    red[tole_NaturalGrassWet]=colour[0];
    green[tole_NaturalGrassWet]=colour[1];
    blue[tole_NaturalGrassWet]=colour[2];

    colour=colours["LIGHT GREY"];
    red[tole_MetalledPath]=colour[0];
    green[tole_MetalledPath]=colour[1];
    blue[tole_MetalledPath]=colour[2];

    colour=colours["DARK SLATE GRAY"];
    red[tole_RoadsideSlope]=colour[0];
    green[tole_RoadsideSlope]=colour[1];
    blue[tole_RoadsideSlope]=colour[2];

    colour=colours["DIM GREY"];
    red[tole_HeritageSite]=colour[0];
    green[tole_HeritageSite]=colour[1];
    blue[tole_HeritageSite]=colour[2];

    colour=colours["CADET BLUE"];
    red[tole_Stream]=colour[0];
    green[tole_Stream]=colour[1];
    blue[tole_Stream]=colour[2];

    colour=colours["DARK TURQUOISE"];
    red[tole_Carpark]=colour[0];
    green[tole_Carpark]=colour[1];
    blue[tole_Carpark]=colour[2];

    colour=colours["MEDIUM AQUAMARINE"];
    red[tole_Churchyard]=colour[0];
    green[tole_Churchyard]=colour[1];
    blue[tole_Churchyard]=colour[2];

    colour=colours["ORANGE RED"];
    red[tole_Wasteland]=colour[0];
    green[tole_Wasteland]=colour[1];
    blue[tole_Wasteland]=colour[2];

    colour=colours["MEDIUM SEA GREEN"];
    red[tole_IndividualTree]=colour[0];
    green[tole_IndividualTree]=colour[1];
    blue[tole_IndividualTree]=colour[2];

    colour=colours["LIGHT BLUE"];
    red[tole_WindTurbine]=colour[0];
    green[tole_WindTurbine]=colour[1];
    blue[tole_WindTurbine]=colour[2];

    colour=colours["LIGHT STEEL BLUE"];
    red[tole_Vildtager]=colour[0];
    green[tole_Vildtager]=colour[1];
    blue[tole_Vildtager]=colour[2];

    colour=colours["MAGENTA"];
    red[tole_PlantNursery]=colour[0];
    green[tole_PlantNursery]=colour[1];
    blue[tole_PlantNursery]=colour[2];

    colour=colours["PINK"];
    red[tole_WoodyEnergyCrop]=colour[0];
    green[tole_WoodyEnergyCrop]=colour[1];
    blue[tole_WoodyEnergyCrop]=colour[2];

    colour=colours["PLUM"];
    red[tole_WoodlandMargin]=colour[0];
    green[tole_WoodlandMargin]=colour[1];
    blue[tole_WoodlandMargin]=colour[2];

    colour=colours["MAROON"];
    red[tole_Pylon]=colour[0];
    green[tole_Pylon]=colour[1];
    blue[tole_Pylon]=colour[2];

    colour=colours["MEDIUM BLUE"];
    red[tole_Pond]=colour[0];
    green[tole_Pond]=colour[1];
    blue[tole_Pond]=colour[2];

    colour=colours["STEEL BLUE"];
    red[tole_FishFarm]=colour[0];
    green[tole_FishFarm]=colour[1];
    blue[tole_FishFarm]=colour[2];



}

HabitatMap::HabitatMap(const unsigned SIZE)
{
    idata = new unsigned char[SIZE*SIZE*3];
    __MAPSIZE1=SIZE;
}

HabitatMap::HabitatMap()
{
    unsigned SIZE=948;
    idata = new unsigned char[SIZE*SIZE*3];
    __MAPSIZE1=SIZE;
    m_scalingW = 0;
    m_scalingH = 0;
    m_Zoom = 1;
    currentView=0;
}

HabitatMap::~HabitatMap()
{
    delete(idata);
}

void HabitatMap::DrawLandscape()
{
    float x, y;
    RodenticidePredators_Population_Manager* RPM = NULL;
    int xx, yy;
    x = m_TLx;
    if (cfg_rodenticide_enable.value()) RPM = m_ALandscape->SupplyRodenticidePredatoryManager();
    // show pesticide load ?

    if (currentView == 4)
    {
        //PlantProtectionProducts ppp = PlantProtectionProducts(m_MainForm->m_subselectionspin->GetValue()-1);
        PlantProtectionProducts ppp = PlantProtectionProducts(0);
        for (int i = 0; i < __MAPSIZE1; i++)
        {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++)
            {
                yy = (int)y;
                if (l_pest_enable_pesticide_engine.value())
                {
                    int col = (int)(m_ALandscape->SupplyPesticide(xx, yy, ppp) * 250);
                    if (col > 250) col = 250;
                    idata[3 * (j * __MAPSIZE1 + i)]       = col;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                }
                else if (cfg_rodenticide_enable.value()) // For rodenticide
                {
                    int rodenticide = rodenticide = (int)(m_ALandscape->SupplyRodenticide(xx, yy) * 250);
                    if (rodenticide > 255) rodenticide = 255;
                    int col = rodenticide;
                    idata[3 * (j * __MAPSIZE1 + i)] = col;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] = col;
                    // Draw predator territories
                    yy = (int)y;
                    if (idata[3 * (j * __MAPSIZE1 + i)] < 10)
                    {
                        col = RPM->HowManyTerritoriesHere(xx, yy);
                        idata[3 * (j * __MAPSIZE1 + i)] += col;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 1] += 64 * col;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 2] += 24 * col;
                    }
                }
                else
                {
                    idata[3 * (j * __MAPSIZE1 + i)] = 0;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                }
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else  if (currentView == 2) {
        for (int i = 0; i < __MAPSIZE1; i++) {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++) {
                yy = (int)y;
                int Biomass = (int)m_ALandscape->SupplyVegBiomass(xx, yy);
                if (Biomass > 250) Biomass = 250;
                int col = Biomass >> 0;
                idata[3 * (j * __MAPSIZE1 + i)] = 64;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = col;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 64;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 3) {
        // Show farm ownership
        for (int i = 0; i < __MAPSIZE1; i++) {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++) {
                yy = (int)y;
                int owner;
                owner = m_ALandscape->SupplyFarmOwnerIndex(xx, yy);
                if (owner != -1) {
                    int red, green, blue;
                    red = ((owner * 17) % 255);
                    green = (red + (owner * 19) % 255);
                    blue = (green + (owner * 29) % 255);
                    idata[3 * (j * __MAPSIZE1 + i)] = red;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] = green;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] = blue;
                }
                else {
                    idata[3 * (j * __MAPSIZE1 + i)] = 0;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                }
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 1) {
        // tov show
        for (int i = 0; i < __MAPSIZE1; i++) {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++) {
                yy = (int)y;
                unsigned ref = m_ALandscape->SupplyVegType(xx, yy);
                int red = ((ref * 17) % 255);
                int green = (red + (ref * 19) % 255);
                int blue = (green + (ref * 29) % 255);
                idata[3 * (j * __MAPSIZE1 + i)] = red;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = green;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = blue;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 5)  // Goose Numbers
    {
        for (int i = 0; i < __MAPSIZE1; i++)
        {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++)
            {
                yy = (int)y;
                int goosenumbers = m_ALandscape->GetGooseNumbers(xx, yy);
                if (goosenumbers > 255) goosenumbers = 255;
                idata[3 * (j * __MAPSIZE1 + i)] = 32;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 32;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = goosenumbers;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 6)  // Goose Food Density
    {
        for (int i = 0; i < __MAPSIZE1; i++)
        {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++)
            {
                yy = (int)y;
                double food = m_ALandscape->SupplyBirdSeedForage(xx, yy) * 17.7
                        + m_ALandscape->GetActualGooseGrazingForage(xx, yy, gs_Pinkfoot);
                if (food > 255) food = 255;
                idata[3 * (j * __MAPSIZE1 + i)] = food;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = food;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = food;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 7)  // Goose GrainFood Density
    {
        for (int i = 0; i < __MAPSIZE1; i++)
        {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++)
            {
                yy = (int)y;
                double food = m_ALandscape->SupplyBirdSeedForage(xx, yy) * 2;
                if (food > 255) food = 255;
                idata[3 * (j * __MAPSIZE1 + i)] = 32;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = food;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = food;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 8)  // Goose Grazing
    {
        for (int i = 0; i < __MAPSIZE1; i++)
        {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++)
            {
                yy = (int)y;
                double food = m_ALandscape->GetActualGooseGrazingForage(xx, yy, gs_Pinkfoot) * 4;
                if (food > 255) food = 255;
                idata[3 * (j * __MAPSIZE1 + i)] = 64;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = food;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 32;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 9)  // Soils
    {
        for (int i = 0; i < __MAPSIZE1; i++)
        {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++)
            {
                yy = (int)y;
                double soil = m_ALandscape->SupplySoilTypeR(xx, yy) * 16;
                if (soil > 255) soil = 255;
                idata[3 * (j * __MAPSIZE1 + i)] = 64;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = soil;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = soil;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else if (currentView == 10)  // Hunter mapping
    {
        // First we draw the standard landscape
        for (int i = 0; i < __MAPSIZE1; i++) {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++) {
                yy = (int)y;
                // Note that because of the way the data is stored, we have to reverse
                // the x,y co-ords
                unsigned ref = m_ALandscape->SupplyElementType(xx, yy);
                if (ref == 24)  // 24 is tole_Building
                {
                    if (m_ALandscape->SupplyCountryDesig(xx, yy) == 1) // Country residence
                    {
                        idata[3 * (j * __MAPSIZE1 + i)] = 255;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                    }
                    else
                    {
                        idata[3 * (j * __MAPSIZE1 + i)] = 0;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                    }
                }
                else
                {
                    if (ref > 67) ref = 67; // The max number of colours
                    idata[3 * (j * __MAPSIZE1 + i)] = colours.red[ref];
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] = colours.green[ref];
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] = colours.blue[ref];
                }
                y += m_scalingH;
            }
            x += m_scalingW;
        }
        // Next we add the hunter dots

 /*       unsigned noInList = m_MainForm->m_Hunter_Population_Manager->SupplyListSize(0);
        for (unsigned aHunter = 0; aHunter < noInList; aHunter++)
        {
            APoint pt = m_MainForm->m_Hunter_Population_Manager->GetHunterHome(aHunter, 0);
            if (pt.m_x < 0 || pt.m_y < 0 || pt.m_x >= m_ALandscape->SupplySimAreaWidth() || pt.m_y >= m_ALandscape->SupplySimAreaHeight())
            {
                pt.m_x = 0;
                pt.m_y = 0;
            }
            Spot(0, pt.m_x, pt.m_y);
            // Here we only show the first hunter hunting location
            pt = m_MainForm->m_Hunter_Population_Manager->GetHunterHuntLoc(aHunter, 0,0);
            Spot(1, pt.m_x, pt.m_y);
        }
        */
    }
    else  if (currentView == 11) {
        for (int i = 0; i < __MAPSIZE1; i++) {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++) {
                yy = (int)y;
                int Pollen = (int)m_ALandscape->SupplyPollen(xx, yy).m_quantity *1000;
                if (Pollen > 250) Pollen = 250;
                int col = Pollen >> 0;
                idata[3 * (j * __MAPSIZE1 + i)] = 0;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = col;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = col;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else  if (currentView == 12) {
        for (int i = 0; i < __MAPSIZE1; i++) {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++) {
                yy = (int)y;
                int Nectar = (int)m_ALandscape->SupplyNectar(xx, yy).m_quantity * 1000;
                if (Nectar > 250) Nectar = 250;
                int col = Nectar >> 0;
                idata[3 * (j * __MAPSIZE1 + i)] = col;
                idata[(3 * (j * __MAPSIZE1 + i)) + 1] = col;
                idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }
    else {
        // default
        for (int i = 0; i < __MAPSIZE1; i++) {
            y = m_TLy;
            xx = (int)x;
            for (int j = 0; j < __MAPSIZE1; j++) {
                yy = (int)y;
                // Note that because of the way the data is stored, we have to reverse
                // the x,y co-ords
                unsigned ref = m_ALandscape->SupplyElementType(xx, yy);
                if (ref == 24)  // 24 is tole_Building
                {
                    if (m_ALandscape->SupplyCountryDesig(xx, yy) == 1) // Country residence
                    {
                        idata[3 * (j * __MAPSIZE1 + i)] = 255;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                    }
                    else
                    {
                        idata[3 * (j * __MAPSIZE1 + i)] = 0;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 1] = 0;
                        idata[(3 * (j * __MAPSIZE1 + i)) + 2] = 0;
                    }
                }
                else
                {
                    if (ref > 67) ref = 67; // The max number of colours
                    idata[3 * (j * __MAPSIZE1 + i)] = colours.red[ref];
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] = colours.green[ref];
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] = colours.blue[ref];
                }
                if (cfg_rodenticide_enable.value()) // For rodenticide
                {
                    // Draw predator territories
                    yy = (int)y;
                    int col = RPM->HowManyTerritoriesHere(xx, yy);
                    idata[3 * (j * __MAPSIZE1 + i)] += 64 * col;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 1] += col;
                    idata[(3 * (j * __MAPSIZE1 + i)) + 2] += col;
                }
                y += m_scalingH;
            }
            x += m_scalingW;
        }
    }

}

void HabitatMap::ChangeZoom( int X, int Y ) {
  // Now place the TL corner
  m_TLx = X - ( m_Width / 2 );
  if ( m_TLx > m_SimW - m_Width ) m_TLx = m_SimW - m_Width;
  if ( m_TLx < 0 ) m_TLx = 0;
  m_TLy = Y - ( m_Height / 2 );
  if ( m_TLy > m_SimH - m_Height ) m_TLy = m_SimH - m_Height;
  if ( m_TLy < 0 ) m_TLy = 0;
  // Place the BL corner
  m_BRx = m_TLx + m_Width;
  m_BRy = m_TLy + m_Height;
  SetScale( m_Width, m_Height );
  //  DrawLandscape();
  //  m_AManager->DisplayLocations();
  //  if (RadioButton1->Checked)  {
  //    PredatorManager->DisplayLocations();
  //  }
}

void HabitatMap::SetSimExtent( int Width, int Height ) {
  // This function must be called before the form can be used
  m_SimW = Width;
  m_SimH = Height;
  m_TLx = 0;
  m_TLy = 0;
  m_BRx = m_SimW;
  m_BRy = m_SimH;
  m_Width = m_SimW;
  m_Height = m_SimH;
  SetScale( m_SimW, m_SimH );
}

void HabitatMap::SetScale( int Width, int Height ) {
  // This function must be called before the form can be used
  // It determines the multiplier for the display i.e how many pixels map to
  // one unit of the track area
  m_scalingW = Width / ( float )__MAPSIZE1;
  m_scalingH = Height / ( float )__MAPSIZE1;
}

void HabitatMap::SetUpMapImage() {
  //wxIm->LoadFile( _T( __TITLEBMP ), wxBITMAP_TYPE_BMP );
  //idata = wxIm->GetData();
}

void HabitatMap::Spot( int cref, int x, int y ) {
  if ( ( x >= m_TLx ) && ( x < m_BRx ) && ( y >= m_TLy ) && ( y < m_BRy ) ) {
    int i = (int) (( x - m_TLx ) / m_scalingW);
    int j = (int) (( y - m_TLy ) / m_scalingH);
    int row3 = j * __MAPSIZE1;
    int row2 = row3 - __MAPSIZE1;
    int row1 = row2 - __MAPSIZE1;
    int row4 = row3 + __MAPSIZE1;
    int row5 = row4 + __MAPSIZE1;

    if (row1 < 1 ) return;
    if (row2 < 0 ) return;
    if (row5 > __MAPSIZE1 * (__MAPSIZE1-1) ) return;
    if (row4 > __MAPSIZE1 * __MAPSIZE1 ) return;

    switch ( cref ) {
      case 0:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 1 ]        = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 255;

            idata[   3 * (( row1 + i +1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 1 ]        = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -1) ) + 1 ]        = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row3 + i +0) ) + 1 ]        = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +1) ) + 1 ]        = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row4 + i +0) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 245;

            idata[   3 * (( row4 + i +1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 250;

            idata[   3 * (( row4 + i +2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i -1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 245;

            idata[   3 * (( row4 + i -2) ) + 1 ]        = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 240;


            idata[   3 * (( row5 + i +0) ) + 1 ]        = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 220;

            idata[   3 * (( row5 + i +1) ) + 1 ]        = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 240;

            //idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) & (__MAPSIZE  *  __MAPSIZE) ) + 1 ]        = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 200;

            //idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 1:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 255;

            idata[   3 * (( row1 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 255;

            //idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row3 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row3 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row4 + i +0) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 245;

            idata[   3 * (( row4 + i +1) ) + 1 ]        = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i -1) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 255;

            idata[   3 * (( row4 + i -2) ) + 1 ]        = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 255;


            idata[   3 * (( row5 + i +0) ) + 1 ]        = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i +1) ) + 1 ]        = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 255;

            //idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) + 1 ]        = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 255;

            //idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 2:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 0;

            idata[   3 * (( row1 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

            idata[   3 * (( row2 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row2 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 64;

            idata[   3 * (( row2 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row3 + i +0) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 32;

            idata[   3 * (( row3 + i +1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -1) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row4 + i +0) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +1) ) + 1 ]        = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +2) ) + 1 ]        = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -1) ) + 1 ]        = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -2) ) + 1 ]        = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row5 + i +0) ) + 1 ]        = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row5 + i +1) ) + 1 ]        = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i +2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) + 1 ]        = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i -2) ) + 1 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
      break;
      case 3:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) + 2 ]        = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 0 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 0;

            idata[   3 * (( row1 + i +1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 0;

            //idata[   3 * (( row1 + i +2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;

            idata[   3 * (( row1 + i -1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 0;

            //idata[   3 * (( row1 + i -2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;

            idata[   3 * (( row2 + i +0) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 0 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;

            idata[   3 * (( row2 + i +1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 0 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;

            idata[   3 * (( row2 + i +2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 0;

            idata[   3 * (( row2 + i -1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 0 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 64;

            idata[   3 * (( row2 + i -2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 0;


            idata[   3 * (( row3 + i +0) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 0 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 32;

            idata[   3 * (( row3 + i +1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 0 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 128;

            idata[   3 * (( row3 + i +2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 0;

            idata[   3 * (( row3 + i -1) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 0;

            idata[   3 * (( row3 + i -2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 0;


            idata[   3 * (( row4 + i +0) ) + 2 ]        = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i +1) ) + 2 ]        = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i +2) ) + 2 ]        = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i -1) ) + 2 ]        = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 0;

            idata[   3 * (( row4 + i -2) ) + 2 ]        = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 0;


            idata[   3 * (( row5 + i +0) ) + 2 ]        = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 0;

            idata[   3 * (( row5 + i +1) ) + 2 ]        = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 0;

            //idata[   3 * (( row5 + i +2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;

            idata[   3 * (( row5 + i -1) ) + 2 ]        = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 0 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 0;

            //idata[   3 * (( row5 + i -2) ) + 2 ]        = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 0 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
            break;

      case 4:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 0;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 0;

            idata[   3 * (( row1 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row1 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

            idata[   3 * (( row2 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row2 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 64;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 64;

            idata[   3 * (( row2 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row3 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 32;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 32;

            idata[   3 * (( row3 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 128;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row3 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row4 + i +0) ) ]       = 245;
            idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +1) ) ]       = 250;
            idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -1) ) ]       = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 0;

            idata[   3 * (( row4 + i -2) ) ]       = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 0;


            idata[   3 * (( row5 + i +0) ) ]       = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 0;

            idata[   3 * (( row5 + i +1) ) ]       = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) ]       = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 0;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 0;

            //idata[   3 * (( row5 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
        break;
      case 5:
            // rather than loop I'm defining each pixel so I can draw shapes

            idata[   3 * (( row1 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +0) )) + 1 ]  = 172;
            idata[ ( 3 * (( row1 + i +0) )) + 2 ]  = 128;

            idata[   3 * (( row1 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row1 + i +1) ) ) + 2 ] = 128;

            //idata[   3 * (( row1 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row1 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row1 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row1 + i -1) ) ) + 2 ] = 128;

            //idata[   3 * (( row1 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row1 + i -2) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i +0) ) ) + 2 ] = 172;

            idata[   3 * (( row2 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 1 ] = 255;
            idata[ ( 3 * (( row2 + i +1) ) ) + 2 ] = 255;

            idata[   3 * (( row2 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i +2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i +2) ) ) + 2 ] = 128;

            idata[   3 * (( row2 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i -1) ) ) + 2 ] = 128;

            idata[   3 * (( row2 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row2 + i -2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row2 + i -2) ) ) + 2 ] = 128;


            idata[   3 * (( row3 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i +0) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i +2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i +2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i +2) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i -1) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i -1) ) ) + 2 ] = 128;

            idata[   3 * (( row3 + i -2) ) ]       = 255;
            idata[ ( 3 * (( row3 + i -2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row3 + i -2) ) ) + 2 ] = 128;


            idata[   3 * (( row4 + i +0) ) ]       = 255;
            idata[ ( 3 * (( row4 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i +0) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i +1) ) ]       = 255;
            idata[ ( 3 * (( row4 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i +1) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i +2) ) ]       = 245;
            idata[ ( 3 * (( row4 + i +2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i +2) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i -1) ) ]       = 245;
            idata[ ( 3 * (( row4 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i -1) ) ) + 2 ] = 128;

            idata[   3 * (( row4 + i -2) ) ]       = 240;
            idata[ ( 3 * (( row4 + i -2) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row4 + i -2) ) ) + 2 ] = 128;


            idata[   3 * (( row5 + i +0) ) ]       = 220;
            idata[ ( 3 * (( row5 + i +0) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row5 + i +0) ) ) + 2 ] = 128;

            idata[   3 * (( row5 + i +1) ) ]       = 240;
            idata[ ( 3 * (( row5 + i +1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row5 + i +1) ) ) + 2 ] = 128;

            //idata[   3 * (( row5 + i +2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i +2) ) ) + 2 ] = 255;

            idata[   3 * (( row5 + i -1) ) ]       = 200;
            idata[ ( 3 * (( row5 + i -1) ) ) + 1 ] = 172;
            idata[ ( 3 * (( row5 + i -1) ) ) + 2 ] = 128;

            //idata[   3 * (( row5 + i -2) ) ]       = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 1 ] = 255;
            //idata[ ( 3 * (( row5 + i -2) ) ) + 2 ] = 255;
        break;
      default:
        idata[ 3 * ( row1 + i ) ] = 200;
        idata[ ( 3 * ( row1 + i ) ) + 1 ] = 200;
        idata[ ( 3 * ( row1 + i ) ) + 2 ] = 200;
    }
  }
}

void HabitatMap::ZoomIn( int X, int Y ) {
  // First back transform X & Y by the scaling factors
  X = (int) (m_TLx + X * m_scalingW);
  Y = (int) (m_TLy + Y * m_scalingH);
  // Zoom in
  int NewZoom = m_Zoom + m_Zoom;
  m_Width = m_SimW / NewZoom;
  m_Height = m_SimH / NewZoom;
  if (m_Width < 100) {
     m_Width = 100; // Don't zoom in smaller than 100m
  } else
     m_Zoom = NewZoom;
  ChangeZoom( X, Y );
}

void HabitatMap::ZoomOut(int X, int Y) {
    // First back transform X & Y by the scaling factors
    X = (int)(m_TLx + X * m_scalingW);
    Y = (int)(m_TLy + Y * m_scalingH);
    // Zoom out
    m_Zoom /= 2;
    if (m_Zoom < 1) m_Zoom = 1; // don't go bigger than SimW
    m_Width = m_SimW / m_Zoom;
    m_Height = m_SimH / m_Zoom;
    ChangeZoom(X, Y);
}

QString HabitatMap::getMapText(const int x, const int y) {
    QString mytext;
    int xx, yy;
    xx = (int) (m_TLx + x * m_scalingW);
    yy = (int) (m_TLy + y * m_scalingH);
    int apolyref = m_ALandscape->SupplyPolyRef( xx, yy );

    mytext += "USED FOR DEBUGGING\n";
    mytext += "YOU CAN GET WHATEVER YOU\n";
    mytext += "WANT IN TERMS OF INFORMATION\n";
    mytext += "PRESENTED HERE\n";
    mytext += "x: " + QString::fromStdString(std::to_string(xx)) + "\n";
    mytext += "y: " + QString::fromStdString(std::to_string(yy)) + "\n";
    mytext += "Polygon Ref: : " + QString::fromStdString(std::to_string(apolyref)) + "\n";
    std::string astr = m_ALandscape->PolytypeToString( m_ALandscape->SupplyElementType( apolyref ));
    mytext += "Element Type: " + QString::fromStdString(astr) + "\n";
    double polyarea = m_ALandscape->SupplyPolygonArea(apolyref)/10000.0;
    mytext += "Polygon Area (ha): " + QString::fromStdString(std::to_string(polyarea)) + "\n";
    astr = m_ALandscape->VegtypeToString( m_ALandscape->SupplyVegType( apolyref )).c_str();
    mytext += "Veg Type: " + QString::fromStdString(astr) + "\n";
    mytext += "Veg. Total Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegBiomass(apolyref))) + "\n";
    mytext += "LAI Green: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyLAGreen(apolyref))) + "\n";
    mytext += "Veg. Dead Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegBiomass(apolyref)-m_ALandscape->SupplyGreenBiomass(apolyref))) + "\n";
    mytext += "Veg Height: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegHeight(apolyref))) + "\n";
    mytext += "Veg. Digestability: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegDigestability(apolyref))) + "\n";
    mytext += "Veg Density: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegDensity(apolyref))) + "\n";
    mytext += "Veg Cover: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegCover(apolyref))) + "\n";
    mytext += "Weed Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyWeedBiomass(apolyref))) + "\n";
    mytext += "Insect Biomass: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyInsects(apolyref))) + "\n";
    mytext += "Is Grazed: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyGrazingPressure(apolyref))) + "\n";
    mytext += "Is Patchy: " ;
    if ( m_ALandscape->SupplyVegPatchy( apolyref ) )
        mytext += "True";
    else
        mytext += "False";
    mytext += "Centroid x: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyCentroidX(apolyref))) + "\n";
    mytext += "Centroid y: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyCentroidY(apolyref))) + "\n";
    mytext += "Veg Phase: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyVegPhase(apolyref))) + "\n";
    //mytext += "Last Treatment: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyLastTreatment(apolyref,0))) + "\n";
    mytext += "Latest Sown Crop: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyLastSownVeg(apolyref))) + "\n";
    mytext += "Valid x: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyValidX(apolyref))) + "\n";
    mytext += "Valid y: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyValidY(apolyref))) + "\n";
    int fref = m_ALandscape->SupplyFarmOwner( apolyref );
    mytext += "Farm Owner Ref: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyFarmOwner(apolyref))) + "\n";
    if (fref!=-1)
        mytext += "Farm Area: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyFarmArea(apolyref))) + "\n";
    else
        mytext += "Farm Area: N/A\n";

    mytext += "Pollen g/m2: " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyPollen(apolyref).m_quantity *10000))/10000.0)) + "\n";
    mytext += "Total Pollen : " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyTotalPollen(apolyref)*10000))/10000.0)) + "\n";
    mytext += "Nectar g/m2: " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyNectar(apolyref).m_quantity *10000))/10000.0)) + "\n";
    mytext += "Total Nectar : " + QString::fromStdString(std::to_string(round((m_ALandscape->SupplyTotalNectar(apolyref)*10000))/10000.0)) + "\n";

    mytext += "NEXT LINE ONLY WORKS WITH\n";
    mytext += "THE PESTICIDE ENGINE\n";
    if (l_pest_enable_pesticide_engine.value())
    {
        PlantProtectionProducts ppp;
        if (mainWindow->getCurrentSpecies() == 4)
            ppp = PlantProtectionProducts(mainWindow->getSubValue()-1);
        else
            ppp = ppp_1;

        mytext += "Pesticide Load: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyPesticide(xx,yy,ppp))) + "\n";
    }
    else
    {
        mytext += "Pesticide Load: Temporarily Unavailable";
    }

    mytext += "Currenty Day Degrees: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyDayDegrees(apolyref))) + "\n";
    mytext += "Openness: " + QString::fromStdString(std::to_string(m_ALandscape->SupplyOpenness(apolyref))) + "\n";
    double goosefood = m_ALandscape->SupplyBirdSeedForage(apolyref);
    mytext += "Grain food density: " + QString::fromStdString(std::to_string(goosefood)) + "\n";

    mytext += "Grazing food density in Kj m2: " + QString::fromStdString(std::to_string(m_ALandscape->GetActualGooseGrazingForage(apolyref,gs_Pinkfoot))) + "\n";
    mytext += "Maximum goose nnumbers: " + QString::fromStdString(std::to_string(m_ALandscape->GetGooseNumbers(apolyref))) + "\n";
    int goosenums = m_ALandscape->GetGooseNumbers(apolyref);
    mytext += "Max. goose density in geese/ha: " + QString::fromStdString(std::to_string((double)goosenums / polyarea)) + "\n";

    mytext += "Hare Forage: " + QString::fromStdString(std::to_string(m_ALandscape->GetHareFoodQuality(apolyref))) + "\n";

    mytext += "\n";
    mytext += "\n";
    return mytext;
}

QString HabitatMap::getAnimalText(const int x, const int y) {
    QString mytext;
    int X = (int)(m_TLx + x * m_scalingW);
    int Y = (int)(m_TLy + y * m_scalingH);

    TAnimal* animal = NULL;
    TAnimal* animal_closest = NULL;
    double dist, dist2 = 99999999999999999999.0;

    for (int i=1; i <= 6; ++i) {
        if (mainWindow->isAnimalChecked(i)) {
            animal = NULL;
            animal = mainWindow->getPopManager()->FindClosest(X, Y, i-1);
            if (animal) {
                dist = sqrt(((animal->Supply_m_Location_x() - X)* (animal->Supply_m_Location_x() - X)) + ((animal->Supply_m_Location_y() - Y)* (animal->Supply_m_Location_y() - Y)));
                if (dist < dist2)
                {
                    dist2 = dist;
                    animal_closest = animal;
                }
            }
        }
    }
    if (animal_closest != NULL) {
        mytext += "ANIMAL INFORMATION AVAILABLE FROM THE TAnimal*\n";
        mytext += "x: " + QString::fromStdString(std::to_string(animal_closest->Supply_m_Location_x())) + "\n";
        mytext += "y: " + QString::fromStdString(std::to_string(animal_closest->Supply_m_Location_y())) + "\n";
        mytext += "Polygon ref: " + QString::fromStdString(std::to_string(animal_closest->SupplyPolygonRef())) + "\n";
        mytext += "Current State No: " + QString::fromStdString(std::to_string(animal_closest->GetCurrentStateNo())) + "\n";
        mytext += "WhatState returns: " + QString::fromStdString(std::to_string(animal_closest->WhatState())) + "\n";
    }
    return mytext;
}
