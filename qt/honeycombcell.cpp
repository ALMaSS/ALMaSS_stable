#include "honeycombcell.h"
#include <math.h>
HoneyCombCell::HoneyCombCell(const int x, const int y, const int size, QColor fillColour, QString label)
{
    xval=x;
    yval=y;
    sizeval=size;
    clabel=label;
    myFillColour=fillColour;
    Pressed=false;
    setFlag(ItemIsMovable);
}

QRectF HoneyCombCell::boundingRect() const
{
    return QRectF(-sizeval/2,-sizeval/2,sizeval/2,sizeval/2);
}

void HoneyCombCell::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    int x=xval;
    int y=yval;
    float r = sizeval / 2.0;
    float r2 = r / 2.0;
    float v = sqrt(pow(r,2) - pow(r2,2));
    QBrush brush(myFillColour);
    QPen blackpen(Qt::black);
    blackpen.setWidth(1);

    QPolygonF polyf;
    polyf << QPointF(x,y+r)
         << QPointF(x+v,y+r2)
         << QPointF(x+v,y-r2)
      << QPointF(x,y-r)
         << QPointF(x-v,y-r2)
         << QPointF(x-v,y+r2);

    painter->setPen(blackpen);
    painter->setBrush(brush);
    painter->drawPolygon(polyf);


    if (clabel.length()>0) {
        painter->setPen(QPen(Qt::white));
        painter->drawText(x-r,y-r,r*2,r*2,Qt::AlignHCenter | Qt::AlignVCenter,clabel);
    }

}

void HoneyCombCell::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Pressed=true;
    update();
    QGraphicsItem::mousePressEvent(event);
}

void HoneyCombCell::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Pressed=false;
    update();
    QGraphicsItem::mouseReleaseEvent(event);

}

