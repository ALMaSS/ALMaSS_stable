#include <stdlib.h>

#include <vector> // Needed for Pop manager below. In wrong place!

using namespace std;

#include "../ALMaSSDefines.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Vole/GeneticMaterial.h"
#include "../Skylark/skylarks_all.h"
#include "../Partridge/Partridge_All.h"
#include "../Partridge/Partridge_Population_Manager.h"
#include "../Vole/vole_all.h"
#include "../Vole/VolePopulationManager.h"
#include "../Vole/Predators.h"
#include "../Bembidion/bembidion_all.h"
#include "../Hare/hare_all.h"
#include "../Spider/spider_all.h"
#include "../Spider/SpiderPopulationManager.h"
#include "../GooseManagement/GooseMemoryMap.h"
#include "../GooseManagement/Goose_Base.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../Hunters/Hunters_all.h"
#include "../GooseManagement/Goose_Population_Manager.h"
#include "../RodenticideModelling/RodenticidePredators.h"
#include "../RoeDeer/Roe_all.h"
#include "../RoeDeer/Roe_pop_manager.h"
#include "../Rabbit/Rabbit.h"
#include "../Rabbit/Rabbit_Population_Manager.h"
#include "../Newt/Newt.h"
#include "../Newt/Newt_Population_Manager.h"
#include "../OliveMoth/olivemoth.h"

int g_torun;

int random(int a_range) {
  /* Want to raise exception on this?
    if ( a_range <= 0 )
    return 0;
  */
  //  int result = (int) (((double) rand() /  randmaxp) * a_range);
  int result = rand()%a_range;
  return result;
}

#ifdef __UNIX
void FloatToDouble(double &d, float f) {
    char * num = 0;
    num = (char*) malloc(128);
    gcvt(f,8,num);
    d = atof(num);
    delete num;
}
#else
void FloatToDouble(double &d, float f) {
    char * num = 0;
    num = (char*) malloc(_CVTBUFSIZE);
    errno_t err = _gcvt_s(num, _CVTBUFSIZE,f,8);
    if (err!=0) {
      assert(0);
    }
    d = atof(num);
    delete num;
}
#endif


ALMaSS_MathFuncs g_AlmassMathFuncs;

PopulationManagerList g_PopulationManagerList;
