#include "mwhivetool.h"
#include "ui_mwhivetool.h"
//#include "../BatchALMaSS/PopulationManager.h"
#include "mainwindow.h"
#include <math.h>
#include <QDialog>
#include <QWidget>
#include <QButtonGroup>
#include <QGraphicsPolygonItem>
#include <QBrush>
#include <QPen>
#include <QGraphicsScene>
#include <QActionGroup>
#include "honeycombcell.h"
#include <blitz/array.h>

#include "../ALMaSSDefines.h"
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"

#include "../BatchALMaSS/BinaryMapBase.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../HoneyBee/HoneyBee_Colony.h"

MWHiveTool::MWHiveTool(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MWHiveTool)
{
    ui->setupUi(this);
    MainWindow * mw = dynamic_cast<MainWindow*>(parent);
    pm =(HoneyBee_Colony*)mw->getPopManager();


    // Note - this is temporary. Should be in PM.
    hive = pm->getHive();
    //hive->clearCellType();
    //hive->setCellType(1,1,1,2);
    //std::cout << hive->getCellTypeArray() << std::endl;

    //hive=pm->getHive();

    seeGroup = new QActionGroup(this);
    seeGroup->addAction(ui->actionBee_count);
    seeGroup->addAction(ui->actionCell_Types);
    seeGroup->addAction(ui->actionTemperature);

    viewType=3;

    zoom=ui->zoomDial->value();
    ui->zoomLCD->display(zoom);

    //scene=new QGraphicsScene(this);
    //ui->graphicsView->setScene(scene);

    //genTemperature();
    //hive->makeSomeHoney();
//    doDraw();
}

void MWHiveTool::genTemperature()
{
    double mag=ui->t_MagnitudeSpinbox->value();
    double base=ui->t_BaseSpinbox->value();
    double rad=ui->t_RadiusSpinbox->value();
    double x=ui->t_xSpinbox->value();
    double y=ui->t_ySpinbox->value();
    double z=ui->t_Zspinbox->value();
    hive->setTemperatureGauss(mag,base,rad,x,y,z);
}

MWHiveTool::~MWHiveTool()
{
    delete ui;
    delete seeGroup;
    //delete scene;

    // Note - this is temporary. Should be in PM.
    //delete hive;
}

void MWHiveTool::paintEvent(QPaintEvent * p) {
    doDraw();
}

void MWHiveTool::doDraw()
{
    if (!ui->updateCB->isChecked())
        return;

    //genTemperature();
    //hive->makeSomeHoney();
    int frame=ui->frameSB->value();
    int box=ui->boxSB->value();

    //std::cout << "View Type: " << viewType << std::endl;

    double min=ui->minTempSB->value();
    double max=ui->maxTempSB->value();
    switch (viewType) {
    case 1: // Cell types
    {
        blitz::Array<int,2> f2=hive->getFrameCellType(frame,FrameSide::Front);
        blitz::Array<int,2> b2=hive->getFrameCellType(frame,FrameSide::Back);
        //drawFrameContinuous<int>(f2,b2,0,6);
        drawFrameCategorical<int>(f2,b2);
        break;
    }
    case 2: // Bee numbers
    {
        blitz::Array<unsigned,2> f2=hive->getFrameBeeNums(frame,FrameSide::Front);
        blitz::Array<unsigned,2> b2=hive->getFrameBeeNums(frame,FrameSide::Back);
        //std::cout << "Front Side Array" << std::endl;
        //std::cout << f2;
        drawFrameContinuous<unsigned>(f2,b2,0,3);
        break;
    }
    case 3: // Temp
    {
        blitz::Array<float,2> f2=hive->getFrameTemp(frame,FrameSide::Front);
        blitz::Array<float,2> b2=hive->getFrameTemp(frame,FrameSide::Back);
        drawFrameContinuous<float>(f2,b2,min,max);
        break;
    }
    case 4: // Honey
    {
        blitz::Array<float,2> f2=hive->getFrameHoney(frame,FrameSide::Front);
        blitz::Array<float,2> b2=hive->getFrameHoney(frame,FrameSide::Back);
        drawFrameContinuous<float>(f2,b2,min,max);
        break;
    }
    case 5: // Pollen
    {
        blitz::Array<float,2> f2=hive->getFramePollen(frame,FrameSide::Front);
        blitz::Array<float,2> b2=hive->getFramePollen(frame,FrameSide::Back);
        drawFrameContinuous<float>(f2,b2,min,max);
        break;
    }


    default:
        break;
    }

    ui->graphicsView->fitInView(0,0,zoom,zoom,Qt::KeepAspectRatio);
}

void MWHiveTool::on_zoomDial_sliderMoved(int position)
{
    zoom=position;
    ui->zoomLCD->display(zoom);
    doDraw();
}

void MWHiveTool::on_boxSB_valueChanged(int arg1)
{
    doDraw();
}

void MWHiveTool::on_frameSB_valueChanged(int arg1)
{
    doDraw();
}

void MWHiveTool::on_t_xSpinbox_valueChanged(double arg1)
{
    genTemperature();
    doDraw();
}

void MWHiveTool::on_t_ySpinbox_valueChanged(double arg1)
{
    genTemperature();
    doDraw();
}

void MWHiveTool::on_t_Zspinbox_valueChanged(double arg1)
{
    genTemperature();
    doDraw();
}

void MWHiveTool::on_t_BaseSpinbox_valueChanged(double arg1)
{
    genTemperature();
    doDraw();
}

void MWHiveTool::on_t_MagnitudeSpinbox_valueChanged(double arg1)
{
    genTemperature();
    doDraw();
}

void MWHiveTool::on_t_RadiusSpinbox_valueChanged(double arg1)
{
    genTemperature();
    doDraw();
}

void MWHiveTool::on_actionCell_Types_toggled(bool arg1)
{
}

void MWHiveTool::on_actionBee_count_toggled(bool arg1)
{
}

void MWHiveTool::on_actionCell_Types_triggered()
{
    viewType=1;
    doDraw();

}

void MWHiveTool::on_actionBee_count_triggered()
{
    viewType=2;
    doDraw();
}

void MWHiveTool::on_actionTemperature_triggered()
{
    viewType=3;
    doDraw();
}

void MWHiveTool::on_actionHoney_triggered()
{
    viewType=4;
    doDraw();
}

void MWHiveTool::on_actionPollen_triggered()
{
    viewType=5;
    doDraw();
}

