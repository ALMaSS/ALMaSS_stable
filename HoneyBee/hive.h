#ifndef HIVE_H
#define HIVE_H

#include <blitz/array.h>
#include <iostream>
#include <array>
#include "math.h"

//#include "HoneyBee.h"

//CfgFloat cfg_HB_pollenStoreFullWeight("HONEYBEE_POLLEN_STORE_FULL_WEIGHT", CFG_CUSTOM, 1.0);

typedef unsigned int hindex;

enum class FrameSide {Front=0, Back=1};
//enum class CellType {Empty=0, Nectar, Honey, Pollen, Worker_Egg, Worker_Pupa, Worker_Larva};
enum class CellType
{
    Empty=0,
    Nectar,
    Honey,
    Pollen,
    MarkedNectar,
    MarkedPollen,
    Worker_Egg,
    Worker_Larva,
    Worker_Pupa,
    Worker};




template <class T>
struct HiveCoords {
  blitz::Array<T,3> x;
  blitz::Array<T,3> y;
  blitz::Array<T,3> z;
  blitz::firstIndex i;
  blitz::secondIndex j;
  blitz::thirdIndex k;

  HiveCoords()
  {
  }

  HiveCoords(const unsigned XZ, const unsigned YZ, const unsigned ZZ)
  {
      reshape(XZ,YZ,ZZ);
  }

  void reshape(const unsigned XZ, const unsigned YZ, const unsigned ZZ)
  {
    x.resize(XZ,YZ,ZZ*2);
    y.resize(XZ,YZ,ZZ*2);
    z.resize(XZ,YZ,ZZ*2);
  }
};

// Returns a 2D array for one side of a frame
template<class T>
blitz::Array<T,2> getSide(HiveCoords<T> A,const int frame, const FrameSide side);

// Initialises a HiveCoords as indices.
template <class T>
void setAsIndex(HiveCoords<T> A);

// Initialises a HiveCoords as positions.
template <class T>
void setAsPosition(HiveCoords<T> A,
   const double cellRadius,
   const double frameSpacing,
   const double sideOffset);

// Calculates an array of distance from a point
template <class T>
blitz::Array<T,3> getDistances(HiveCoords<T> A, const T x, const T y, const T z);

struct Coords3D {
    int x, y, z;
};


class HoneyBee_Base;
class Hive
{
protected:
    HiveCoords<float> position;
    blitz::Array<int,3> celltype;
    blitz::Array<float,3> temperature;
    blitz::Array<unsigned, 3> beeCount;
    blitz::Array<HoneyBee_Base*,3> bee;
    blitz::Array<float,3> honey;
    blitz::Array<float,3> nectar;
    blitz::Array<float,3> pollen;
    blitz::Array<int,3> needs;
    blitz::Array<int,3> provides;
    unsigned xrange, yrange, zrange;
    unsigned modelStep;
    //Coords3D nectarStore;
    blitz::TinyVector<int,3> pollenStore;
    double pollenStoreFullWeight;
    blitz::TinyVector<int,3> nectarStore;
    double nectarStoreFullVolume;
    double sugarPool;
    double sugarPoolConcentration;
    blitz::Array<int,1> hivePriorities;

public:
    Hive() {}
    Hive(const unsigned x, const unsigned y, const unsigned z, const float cellRadius, const float frameSpacing, const float sideOffset);
    void initialise(const unsigned x, const unsigned y, const unsigned z, const float cellRadius, const float frameSpacing, const float sideOffset);
    bool isFree(const int x, const int y, const int z){return celltype(x,y,z)==(int)CellType::Empty;}
    void step() { modelStep++;}
    unsigned getModelStep() {return modelStep;}
    unsigned dayOfYear() {return modelStep%365;}
    blitz::Array<float,3> getX() {return position.x;}
    blitz::Array<float,3> getY() {return position.y;}
    blitz::Array<float,3> getZ() {return position.z;}
    void incBeeCount(const int x, const int y, const int z) {beeCount(x,y,z)+=1;}
    void decBeeCount(const int x, const int y, const int z) {beeCount(x,y,z)-=1;}
    blitz::Array<float,3>  getDist(const float x, const float y, const float z);


    //Accessing frames
    blitz::Array<float,2> getFrameTemp(const int frame, const FrameSide side) {return temperature(blitz::Range::all(),blitz::Range::all(),(frame*2) + (int)side);}
    blitz::Array<float,2> getFrameHoney(const int frame, const FrameSide side){return honey(blitz::Range::all(),blitz::Range::all(),(frame*2) + (int)side);}
    blitz::Array<float,2> getFramePollen(const int frame, const FrameSide side){return pollen(blitz::Range::all(),blitz::Range::all(),(frame*2) + (int)side);}
    blitz::Array<int,2> getFrameCellType(const int frame, const FrameSide side){return celltype(blitz::Range::all(),blitz::Range::all(),(frame*2) + (int)side);}
    blitz::Array<unsigned,2> getFrameBeeNums(const int frame, const FrameSide side){return beeCount(blitz::Range::all(),blitz::Range::all(),(frame*2) + (int)side);}
    blitz::Array<float,2> getFrameDist(const int frame, const FrameSide side);

    void setCellType(const int x, const int y, const int z, const int val){celltype(x,y,z)=val;}
    void setCellType(const blitz::TinyVector<int,3> p, const int val) {celltype(p(0),p(1),p(2))=val;}
    void setCellType(const blitz::TinyVector<int,3> p, const CellType val) {celltype(p(0),p(1),p(2))=(int)val;}

    blitz::Array<int,3> getCellTypeArray(){return celltype;}
    void clearCellTypeArray();
    CellType getCellType(blitz::TinyVector<int,3> cell) {return (CellType)celltype(cell);}

    HoneyBee_Base * getBee(const int x, const int y, const int z){return bee(x,y,z);}
    void setBee(const int x, const int y, const int z, HoneyBee_Base * theBee){bee(x,y,z)=theBee;}
    blitz::Array<unsigned,3> getBeeCountArray(){return beeCount;}
    void setTemperatureGauss(float amplitude, float base, float sig, const float x, const float y, const float z);
    void makeSomeHoney();
    void makeTypesFromTemp();
    blitz::Array<float,3> getTemperatureArray() { return temperature;}

    std::array<int,3> randomNeighbour(const int x, const int y, const int z);
    blitz::TinyVector<int,3> warmest(CellType cellType);
    blitz::TinyVector<int,3> shape() {return position.x.shape();}
    blitz::TinyVector<int,3> randomCell();
    blitz::TinyVector<int,3> randomCell(CellType cellType);


    // Pollen
    void setPollen(const blitz::TinyVector<int,3> p, double weight) {pollen(p) = weight;}
    double getPollen(const blitz::TinyVector<int,3> p) {return pollen(p);}
    void closePollenStore() {pollenStore(0)=-1;}
    bool isPollenStoreOpen() {return pollenStore(0) != -1;}
    bool isPollenStoreClosed() {return pollenStore(0) == -1;}
    void incPollenStore(const double weight) {pollen(pollenStore)+=weight;}
    bool isPollenStoreFull() {return (double)pollen(pollenStore) >= pollenStoreFullWeight;}
    void openWarmestPollenCell();
    void storePollen(const double weight);

    // Nectar
    void setNectar(const blitz::TinyVector<int,3> p, double volume) {nectar(p) = volume;}
    double getNectar(const blitz::TinyVector<int,3> p) {return nectar(p);}
    void closeNectarStore() {nectarStore(0)=-1;}
    bool isNectarStoreOpen() {return nectarStore(0) != -1;}
    bool isNectarStoreClosed() {return nectarStore(0) == -1;}
    void incNectarStore(const double volume) {nectar(nectarStore)+=volume;}
    bool isNectarStoreFull() {return (double)nectar(nectarStore) >= nectarStoreFullVolume;}
    void openWarmestNectarCell();
    void storeNectar(const double volume);

    // Sugar Pool
    double getSugarPool() {return sugarPool;}
    void setSugarPool(double value) {sugarPool=value;}
    void incSugarPool(double value) {sugarPool+=value;}
    void incSugarPool(double quantity, double fromConcentration);
    void decSugarPool(double value) {sugarPool-=value;}
    void decSugarPool(double quantity, double toConcentration);
    void storePoolInHoneyCell(blitz::TinyVector<int,3> cell);
    void storePoolInHoneyCell();
    void takeFromHoneyCellToPool(blitz::TinyVector<int,3> cell);
    void takeFromHoneyCellToPool();
};

#endif // HIVE_H
