#include "testlandscape.h"

#include <blitz/array.h>

void normDist(blitz::Array<float,1> arr, float A, float xd, float sig)
{
    blitz::firstIndex i;
    arr=A*blitz::exp((i-xd)/sig);
}

TestLandscape::TestLandscape()
{
    decHoney=0.040;
    decPollen=0.016;
    honeyA.resize(365);
    pollenA.resize(365);
    normDist(honeyA,10,70,50);
    normDist(pollenA,15,100,70);
}

