/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file BeeVirusInfectionLevels.h
Version of  11th May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef BeeVirusInfectionLevelsH
#define BeeVirusInfectionLevelsH
//---------------------------------------------------------------------------


/** \brief A small extendable class to hold an individual's virus infection load. */

class BeeVirusInfectionLevels
{
	/**
	* Currently two viruses are considered: Deformed Wing Virus (DWV)and Actute Bee Paralysis Virus (ABPV)
	* Each are currently considered as binary conditions - infected (true) or not infected (false)
	*/
protected:
	/** \brief Are we infected with ABPV? */
	bool m_ABPV;
	/** \brief Are we infected with DWV? */
	bool m_DWV;
public:
	/** \brief  Standard constructor */
	BeeVirusInfectionLevels(void) {
		SetABPV(false);
		SetDWV(false);
	}
	/** \brief  Copy assignment constructor */
	/* dww: corrected compile bug */
	BeeVirusInfectionLevels& operator = (const BeeVirusInfectionLevels& other) {
		// protect against self assignment
		if (this != &other)
		{
			m_ABPV = other.m_ABPV;
			m_DWV = other.m_DWV;
		}
		return *this;
	}
	/** \brief  Destructor */
	~BeeVirusInfectionLevels(void) { ; }
	/** \brief  Get ABPV status */
	bool GetABPV() { return m_ABPV; }
	/** \brief  Set ABPV status */
	void SetABPV(bool infection) { m_ABPV = infection; }
	/** \brief  Get DWV status */
	bool GetDWV() { return m_DWV; }
	/** \brief  Set DWV status */
	void SetDWV(bool infection) { m_DWV = infection; }
};

#endif
