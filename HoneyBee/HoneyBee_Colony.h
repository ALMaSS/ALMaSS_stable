/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file HoneyBee_Colony.h 

Version of  10th May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef HoneyBeeColonyH
#define HoneyBeeColonyH
//---------------------------------------------------------------------------


#include "hive.h"
#include "honeybeetypes.h"
#include "testlandscape.h"
#include "hbspreadsheet.h"

//#include "HoneyBee.h"
//---------------------------------------------------------------------------

class HoneyBee;
class HoneyBee_Worker;
class BeeVirusInfectionLevels;
class HoneyBee_Colony;
//------------------------------------------------------------------------------



/**
\brief
Used for creation of a new HoneyBee object
*/
class struct_HoneyBee
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief z-coord */
  int z;
  /** \brief age in days */
  int age;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief HoneyBee_Colony pointer */
  HoneyBee_Colony * BPM;
  /** \brief Virus information pointer */
  BeeVirusInfectionLevels * BVI;

};

/**
\brief
The class to handle all predator population related matters
*/
class HoneyBee_Colony : public Population_Manager
{
public:
    TestLandscape testlandscape;
// Methods
   /** \brief HoneyBee_Colony Constructor */
   HoneyBee_Colony(Landscape* L);
   /** \brief HoneyBee_Colony Destructor */
   virtual ~HoneyBee_Colony (void);
   /** \brief Method for creating a new individual HoneyBee */
   void CreateObjects(TTypesOfHoneyBee ob_type, TAnimal *pvo, struct_HoneyBee* data, int number);

   Hive * getHive() {
        return hive;
   }

   int nextID()
   {
       return 1;
        //return currentID;
            //return currentID++;
   }

    template <class Tfrom, class Tto>
    void add(TAnimal& animal)
    {
        int ob_type=Tto::myID;
        int x = animal.Supply_m_Location_x();
        int y = animal.Supply_m_Location_y();

        auto an=dynamic_cast<Tfrom&>(animal);
        int z = an.Supply_m_Location_z();

        if (unsigned(TheArray[ob_type].size())>GetLiveArraySize(ob_type))
        {
            //cout << "Reusing: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
            dynamic_cast<Tto*>(TheArray[ob_type][GetLiveArraySize(ob_type)])->ReInit(x,y,z,m_TheLandscape, this);
            IncLiveArraySize(ob_type);
        }
        else
        {
            //      cout << "Adding: " << ob_type << " Size: " << obsize << " Capacity: " << obcap  << "\n";
            TAnimal* newAnimal = new Tto(x,y,z,m_TheLandscape, this);
            TheArray[ob_type].push_back(newAnimal);
            IncLiveArraySize(ob_type);             
        }
        hive->setCellType(x,y,z,(int)Tto::cellType);
    }
    template <class T>
    bool isInHive(int ob_type, int j)
    {
        return dynamic_cast<T*>(TheArray[ob_type][j])->inHive;
    }

    void layEggs();


protected:
   /** \brief  Things to do before anything else at the start of a timestep  */
	virtual void DoFirst();
   /** \brief Things to do before the Step */
   virtual void DoBefore(){}
   /** \brief Things to do before the EndStep */
   virtual void DoAfter(){}
   /** \brief Things to do after the EndStep */
   virtual void DoLast(){}

   Hive * hive;
   Spreadsheet spreadsheet;
   int currentID;
   blitz::TinyVector<int,3> warmest(CellType celltype);
};

#endif
