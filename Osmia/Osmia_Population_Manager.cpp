/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file OsmiaPopulationManager.cpp
Version of  May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include<vector>
#pragma warning( push )
#pragma warning( disable : 4100)
#pragma warning( disable : 4127)
#pragma warning( disable : 4244)
#pragma warning( disable : 4267)
#include <blitz/array.h>
#pragma warning( pop ) 
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Osmia/Osmia.h"
#include "../Osmia/Osmia_Population_Manager.h"

//---------------------------------------------------------------------------------------

static CfgFloat cfg_OsmiaGeneralMovememnt("OSMIA_GENERALMOVEMENTCURVATURE", CFG_CUSTOM, 660.0);
static CfgFloat cfg_OsmiaGeneralMovememntCurvature("OSMIA_GENERALMOVEMENTCURVATURE", CFG_CUSTOM, 2.0);
static CfgFloat cfg_OsmiaDispersal("OSMIA_DISPERSALCURVATURE", CFG_CUSTOM, 1430.0);
static CfgFloat cfg_OsmiaDispersalCurvature("OSMIA_DISPERSALCURVATURE", CFG_CUSTOM, 2.0);

extern
CfgFloat cfg_OsmiaIncCocoonOverwinteringTempThreshold;

// Assign default static member values (these will be changed later).
int Osmia_Base::m_TempToday = -9999;
double Osmia_InCocoon::m_OverwinteringTempThreshold = 0.0;
double Osmia_Base::m_DailyDevelopmentMortOverWinter[80] = {};
double Osmia_Base::m_DailyDevelopmentMort[80] = {};
array<int, __OSMIA_DIST_SIZE> Osmia_Female::m_generalmovementdistances = {};
array<int, __OSMIA_DIST_SIZE> Osmia_Female:: m_dispersalmovementdistances = {};
PollenMap* Osmia_Female::m_ThePollenMap = NULL;

//---------------------------------------------------------------------------

Osmia_Population_Manager::~Osmia_Population_Manager (void)
{
   // Should all be done by the Population_Manager destructor
}
//---------------------------------------------------------------------------

Osmia_Population_Manager::Osmia_Population_Manager(Landscape* L) : Population_Manager(L)
{
	/** Loads the list of Animal Classes. */
	m_ListNames[0] = "Egg";
	m_ListNames[1] = "Larva";
	m_ListNames[2] = "Pupa";
	m_ListNames[3] = "In Cocoon";
	m_ListNames[4] = "Female";
	m_ListNameLength = 5;
	// We need one vector for each life stage
	for (unsigned int i=0; i<(10-m_ListNameLength); i++)
	{
		TheArray.pop_back();
	}
    strcpy( m_SimulationName, "Osmia Simulation" );
	// Create some animals
	struct_Osmia* sp;
	sp = new struct_Osmia;
	sp->OPM = this;
	sp->L = m_TheLandscape;
	for (int i=0; i< 10000; i++) // This will need to be an input variable (config)
	{
		sp->x = random(SimW);
		sp->y = random(SimH);
		CreateObjects(to_OsmiaFemale,NULL,sp,1);
	}
	// Init performs intialisation of the data to run the Osmia model
	Init();
}

void Osmia_Population_Manager::Init()
{
	/**
	* Initiates the Osmia nests on each landscape element
	* The temperature related mortality needs to be calculated for in nest development and stored in the static variable in the Osmia_Base

	*/
	m_ThePollenMap = new PollenMap(1, int(cfg_OsmiaGeneralMovememnt.value()),m_TheLandscape);
	m_TheLandscape->InitOsmiaBeeNesting();
	// **ela** all this is subject to change as we need it
	double morts[2][80];
	for (int i = 0; i < 80; i++)
	{
		// Below are the equations or methods needed to fill the arrays for -30 to 49
		morts[0][i] = 0.006*exp(0.2027*i); // all stages except in cocoon
		morts[1][i] = 0.03; // in cocoon
	}
	// Set the values of egg static variables
	Osmia_Egg egg(0, 0, m_TheLandscape, this, 0);
	egg.SetOverwinteringMortality(morts);

	// Set the values of InCocoon static variables
	Osmia_InCocoon ic(0, 0, m_TheLandscape, this, 0);
	ic.SetOverwinteringTempThreshold(cfg_OsmiaIncCocoonOverwinteringTempThreshold.value());
	// Create the dispersal functions in a look up array for 2 dispersal distances
	array<int, __OSMIA_DIST_SIZE> dist1;
	array<int, __OSMIA_DIST_SIZE> dist2;
	double max = cfg_OsmiaGeneralMovememnt.value();
	double curvature = cfg_OsmiaGeneralMovememntCurvature.value();
	double max2 = cfg_OsmiaDispersal.value();
	double curvature2 = cfg_OsmiaDispersalCurvature.value();
	for (int i = 0; i < __OSMIA_DIST_SIZE; i++)
	{
		dist1[i] = int(floor((max / pow(__OSMIA_DIST_SIZE, curvature))*(pow(i + 1, curvature))+0.5));
		dist2[i] = int(floor((max2 / pow(__OSMIA_DIST_SIZE, curvature2))*(pow(i + 1, curvature2))+0.5));
	}
	// Set the values of female static variables
	Osmia_Female female(0, 0, m_TheLandscape, this, 0);
	female.SetGeneralMovement(dist1);
	female.SetDispersalMovement(dist2);
	female.SetPollenMap(m_ThePollenMap);
}
//---------------------------------------------------------------------------
void Osmia_Population_Manager::CreateObjects(int ob_type, TAnimal *, struct_Osmia * data, int number) {
	Osmia_Egg*  new_Osmia_Egg;
	Osmia_Larva*  new_Osmia_Larva;
	Osmia_Pupa*  new_Osmia_Pupa;
	Osmia_InCocoon*  new_Osmia_InCocoon;
	Osmia_Female*  new_Osmia_Female;

#ifdef __RECORDOSMIAEGGPRODUCTION
	if (ob_type == to_OsmiaEgg) RecordEggProduction(number);
#endif
	for (int i = 0; i < number; i++) {
		switch (ob_type) {
		case to_OsmiaEgg:
			if (unsigned(TheArray[ob_type].size()) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				dynamic_cast<Osmia_Egg*>(TheArray[ob_type][GetLiveArraySize(ob_type)])->ReInit(data->x, data->y, data->L, data->OPM, 0);
				IncLiveArraySize(ob_type);
			}
			else {
				new_Osmia_Egg = new Osmia_Egg(data->x, data->y, data->L, data->OPM, 0);
				TheArray[ob_type].push_back(new_Osmia_Egg);
				IncLiveArraySize(ob_type);
			}
			break;
		case to_OsmiaLarva:
			if (unsigned(TheArray[ob_type].size()) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				new_Osmia_Larva = dynamic_cast<Osmia_Larva*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Osmia_Larva->ReInit(data->x, data->y, data->L, data->OPM, data->age);
				new_Osmia_Larva->SetAge(data->age);
			}
			else {
				new_Osmia_Larva = new Osmia_Larva(data->x, data->y, data->L, data->OPM, data->age);
				TheArray[ob_type].push_back(new_Osmia_Larva);
				new_Osmia_Larva->SetAge(data->age);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_OsmiaPupa:
			if (unsigned(TheArray[ob_type].size()) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				new_Osmia_Pupa = dynamic_cast<Osmia_Pupa*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Osmia_Pupa->ReInit(data->x, data->y, data->L, data->OPM, data->age);
				new_Osmia_Pupa->SetAge(data->age);
			}
			else {
				new_Osmia_Pupa = new Osmia_Pupa(data->x, data->y, data->L, data->OPM, data->age);
				TheArray[ob_type].push_back(new_Osmia_Pupa);
				new_Osmia_Pupa->SetAge(data->age);
			}
			IncLiveArraySize(ob_type);
			break;
		case to_OsmiaInCocoon:
			if (unsigned(TheArray[ob_type].size()) > GetLiveArraySize(ob_type)) {
				// We need to reuse an object
				new_Osmia_InCocoon = dynamic_cast<Osmia_InCocoon*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
				new_Osmia_InCocoon->ReInit(data->x, data->y, data->L, data->OPM, data->age);
				new_Osmia_InCocoon->SetAge(data->age);
			}
			else {
				new_Osmia_InCocoon = new Osmia_InCocoon(data->x, data->y, data->L, data->OPM, data->age);
				TheArray[ob_type].push_back(new_Osmia_InCocoon);
				new_Osmia_InCocoon->SetAge(data->age);
			}
			IncLiveArraySize(ob_type);
			break; 
		case to_OsmiaFemale:
				if (unsigned(TheArray[ob_type].size()) > GetLiveArraySize(ob_type)) {
					// We need to reuse an object
					new_Osmia_Female = dynamic_cast<Osmia_Female*>(TheArray[ob_type][GetLiveArraySize(ob_type)]);
					new_Osmia_Female->ReInit(data->x, data->y, data->L, data->OPM, data->age);
					new_Osmia_Female->SetAge(data->age);
				}
				else {
					new_Osmia_Female = new Osmia_Female(data->x, data->y, data->L, data->OPM, data->age);
					TheArray[ob_type].push_back(new_Osmia_Female);
					new_Osmia_Female->SetAge(data->age);
				}
				IncLiveArraySize(ob_type);
				break;
		}
	}
}
//---------------------------------------------------------------------------

void Osmia_Population_Manager::RecordEggProduction(int a_eggs) {
	// A Osmia has become larva, we need to record this for the statistics
	m_OsmiaEggProdStats.add_variable(a_eggs);
}
//---------------------------------------------------------------------------------------------------------------------------------
PollenMap::PollenMap(int a_smallcellsizeshift, int a_maskradius, Landscape* a_land)
{
	m_TheLandscape = a_land;
	m_smallcellsizeshift = a_smallcellsizeshift;
	m_smallcellsize = 1 << a_smallcellsizeshift;
	m_cellsize = m_smallcellsize * 8;
	int yext = m_TheLandscape->SupplySimAreaHeight();
	int xext = m_TheLandscape->SupplySimAreaWidth();
	m_maskradius = a_maskradius;
	m_masksize = 2 * m_maskradius / m_cellsize;
	m_x_bound = (xext / m_cellsize) - 1;
	m_y_bound = (yext / m_cellsize) - 1;
	m_pollenmap.resize(m_x_bound + 1, m_y_bound + 1);
	m_pollenmap = 0;
	m_nectarmap.resize(m_x_bound + 1, m_y_bound + 1);
	m_nectarmap = 0;
	CreateMask();
}

void PollenMap::RefillPollenMap()
{
	/**
	* Needs to go through the landscape and assign the pollen values to the pollen map
	* To speed things up a bit we assume we sample every m_cellsize m and multiply by m_cellsize X m_cellsize later
	*/
	int yext, xext;
	int cellx, celly;
	int shift = m_smallcellsizeshift + 3;
	yext = m_TheLandscape->SupplySimAreaHeight();
	xext = m_TheLandscape->SupplySimAreaWidth();
	m_pollenmap = 0; // zero all values
	for (int y = 0; y < yext; y += m_smallcellsize) {
		for (int x = 0; x < xext; x += m_smallcellsize)
		{
			PollenNectarQuality pollen = m_TheLandscape->SupplyPollen(m_TheLandscape->SupplyPolyRef(x, y));
			// PollenMap cells are m_cellsize X 8 m e.g. if m_cellsize is 2, then the area of the pollen map cell is (2X8)^2 = 256;
			cellx = x >> shift;
			celly = y >> shift;
			if (pollen.m_quantity>0.01) m_pollenmap(cellx, celly) += pollen.m_quality;
		}
	}
}

void PollenMap::RefillNectarMap()
{
	/**
	* Needs to go through the landscape and assign the pollen values to the pollen map
	* To speed things up a bit we assume we sample every m_cellsize m and multiply by m_cellsize X m_cellsize later
	*/
	int yext, xext;
	int cellx, celly;
	int shift = m_smallcellsizeshift + 3;
	yext = m_TheLandscape->SupplySimAreaHeight();
	xext = m_TheLandscape->SupplySimAreaWidth();
	m_nectarmap = 0; // zero all values
	for (int y = 0; y < yext; y += m_smallcellsize) {
		for (int x = 0; x < xext; x += m_smallcellsize)
		{
			PollenNectarQuality Nectar = m_TheLandscape->SupplyNectar(m_TheLandscape->SupplyPolyRef(x, y));
			// PollenMap cells are m_cellsize X 8 m e.g. if m_cellsize is 2, then the area of the pollen map cell is (2X8)^2 = 256;
			cellx = x >> shift;
			celly = y >> shift;
			if (Nectar.m_quantity>0.01) m_nectarmap(cellx, celly) += Nectar.m_quality;
		}
	}
}

void PollenMap::CreateMask()
{
	/**
	* Creates a mask to multiply pollen values with to get the overall quality
	*/
	// **ela** these will need to be configs at some point
	// Here I assume the weight is linearly decreasing with distance
	int rings = m_masksize / 2;
	m_mask.resize(m_masksize,m_masksize);
	int y = rings - 1;
	int y2 = y + 1;
	int x = rings - 1;
	int x2 = x + 1;
	for (int r = 1; r <= rings; r++)
	{
		// Here is our equation
		double weight = 1.0 - (r * 1.0/rings);
		// Now we have to loop round the centre four squares, then next loop is 12, then 20, 28 etc..
		// top row & bottom row
		for (int t = rings - r; t < rings + r; t++) {
			m_mask(t, y) = weight; 
			m_mask(t, y2) = weight;
		}
		// sides
		for (int s = y + 1; s < y2; s++) {
			m_mask(x, s) = weight; 
			m_mask(x2, s) = weight;
		}
		y--; y2++; x--;	x2++;
	}
	//cout << m_mask; // **Ela** you might want this to check your mask
}

double PollenMap::CalculatePollenQuality(int a_x, int a_y)
{
	// First locate the point in the PollenMap we want returned
	int lx = (a_x - m_maskradius) / m_cellsize;
	int ly = (a_y - m_maskradius) / m_cellsize;
	int lx2 = lx + m_masksize - 1;
	int ly2 = ly + m_masksize - 1;
	// This is just a range check. 
	int mx = 0;
	int my = 0;
	int mx2 = m_masksize - 1;
	int my2 = mx2;

	if (lx < 0) {
		mx -= lx;
		lx = 0;
	}
	if (ly < 0) {
		my -= ly;
		ly = 0;
	}
	if (lx2 > m_x_bound) {
		mx2 -= (lx2 - m_x_bound);
		lx2 = m_x_bound;
	}
	if (ly2 > m_y_bound) {
		my2 -= (ly2 - m_y_bound);
		ly2 = m_y_bound;
	}
	double  score = blitz::sum(m_mask(blitz::Range(mx, mx2), blitz::Range(my, my2)) * m_pollenmap(blitz::Range(lx, lx2), blitz::Range(ly, ly2)));
	return score;
}

double PollenMap::CalculateNectarQuality(int a_x, int a_y)
{
	// First locate the point in the PollenMap we want returned
	int lx = (a_x - m_maskradius) / m_cellsize;
	int ly = (a_y - m_maskradius) / m_cellsize;
	int lx2 = lx + m_masksize - 1;
	int ly2 = ly + m_masksize - 1;
	// This is just a range check. 
	int mx = 0;
	int my = 0;
	int mx2 = m_masksize - 1;
	int my2 = mx2;

	if (lx < 0) {
		mx -= lx;
		lx = 0;
	}
	if (ly < 0) {
		my -= ly;
		ly = 0;
	}
	if (lx2 > m_x_bound) {
		mx2 -= (lx2 - m_x_bound);
		lx2 = m_x_bound;
	}
	if (ly2 > m_y_bound) {
		my2 -= (ly2 - m_y_bound);
		ly2 = m_y_bound;
	}
	double  score = blitz::sum(m_mask(blitz::Range(mx, mx2), blitz::Range(my, my2)) * m_nectarmap(blitz::Range(lx, lx2), blitz::Range(ly, ly2)));
	return score;
}
