/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Osmia.h
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file Osmia.h
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef OsmiaH
#define OsmiaH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Osmia_Population_Manager;
class PollenMap;

//------------------------------------------------------------------------------
/**
Used for the population manager's list of Osmia
*/
//typedef vector<Osmia*> TListOfOsmia;
//---------------------------------------------------------------------------

#define __OSMIA_DIST_SIZE 10000

/**
Osmia like other ALMaSS animals work using a state/transition concept.
These are the Osmia behavioural states, these need to be altered, but some are here just to show how they should look.
*/
enum TTypeOfOsmiaState
{
	toOsmias_InitialState = 0,
	toOsmias_Develop,
	toOsmias_NextStage,
	toOsmias_Disperse,
	toOsmias_NestProvisioning,
	toOsmias_ReproductiveBehaviour,
	toOsmias_Die
};

/**
Osmia life stages modelled
*/
typedef enum
{
	to_OsmiaEgg = 0,
	to_OsmiaLarva,
	to_OsmiaPupa,
	to_OsmiaInCocoon,
	to_OsmiaFemale
} TTypeOfOsmiaLifeStages;

class Osmia_Base : public TAnimal
{
   /**
   A Osmia must have some simple functionality:
   Inititation and development
   And some simple characteristics, herein age.
   Inherits m_Location_x, m_Location_y, m_OurLandscape from TAnimal
   NB All areas are squares of size length X length
   */

protected:
   /** \brief Variable to record current behavioural state */
   TTypeOfOsmiaState m_CurrentOState;
   /** \brief A typical member variable - this one is the age in days */
   int m_Age;
   /** \brief This is a time saving pointer to the correct population manager object */
   Osmia_Population_Manager*  m_OurPopulationManager;
   /** \brief The temperature today to the nearest degree. This is static because there is only one temperature today */
   static int m_TempToday;
   /** \brief This holds the temperature related daily mortality for eggs-pupa (-30 to + 50*/
   static double m_DailyDevelopmentMort[80];
   /** \brief This holds the temperature related daily mortality for overwintering (-30 to + 50*/
   static double m_DailyDevelopmentMortOverWinter[80];
public:
	/** \brief Osmia constructor */
	Osmia_Base(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia reinitialise object methods */
	void ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia destructor */
   virtual ~Osmia_Base();
   /** \brief Behavioural state dying */
   virtual void st_Dying( void );
   /** \brief The BeginStep is the first 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void BeginStep(void) { ;  } // NB this is not used in the Osmia_Base code
   /** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
   virtual void Step(void) { ; } // NB this is not used in the Osmia_Base code
   /** \brief The EndStep is the third 'part' of the timestep that an animal can behave in. It is called once per timestep. */
   virtual void EndStep(void) { ; } // NB this is not used in the Osmia_Base code
   /** \brief A typical interface function - this one returns the age */
   int GetAge() { return m_Age; }
   /** \brief A typical interface function - this one returns the age */
   void SetAge(int a_age) { m_Age = a_age; }
   /** \brief Used to populate the static members holding mortaltiy per temperature data */
   void SetOverwinteringMortality(double a_MortsByTemp[2][80]) {
	   for (int i = 0; i < 80; i++) {
		   m_DailyDevelopmentMort[i] = a_MortsByTemp[0][i];
		   m_DailyDevelopmentMortOverWinter[i] = a_MortsByTemp[1][i];
	   }
   }
   void SetTemp(double a_temperature) { m_TempToday = int(floor(a_temperature+0.5)); }
};

class Osmia_Egg : public Osmia_Base
{
protected:
	/** \brief This contains the age in degrees for development */
	double m_AgeDegrees;
public:
	/** \brief Osmia_Egg constructor */
	Osmia_Egg(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia_Egg ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	virtual ~Osmia_Egg();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief A typical interface function - this one returns the agedegrees */
	double GetAgeDegrees() { return m_AgeDegrees; }
	/** \brief A typical interface function - this one returns the agedegrees */
	void SetAgeDegrees(unsigned a_agedegrees) { m_AgeDegrees = a_agedegrees; }
	/** \brief Daily mortality test */
	virtual bool DailyMortality() { if (g_rand_uni() < m_DailyDevelopmentMort[m_TempToday + 30]) return true; else return false; }
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state hatch */
	virtual TTypeOfOsmiaState st_Hatch(void);
};

class Osmia_Larva : public Osmia_Egg
{
protected:
public:
	/** \brief Osmia_Larva constructor */
	Osmia_Larva(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia_Larva ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	virtual ~Osmia_Larva();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state pupate */
	virtual TTypeOfOsmiaState st_Pupate(void);
};

class Osmia_Pupa : public Osmia_Larva
{
public:
	/** \brief Osmia_Pupa constructor */
	Osmia_Pupa(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia_Pupa ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	virtual ~Osmia_Pupa();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state for emerging from the pupa */
	virtual TTypeOfOsmiaState st_Emerge(void);
};

class Osmia_InCocoon : public Osmia_Pupa
{
protected:
	bool m_overwinterflag1;
	bool m_overwinterflag2;
	int m_emergencecounter;
public:
	/** \brief Osmia_Adult constructor */
	Osmia_InCocoon(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia_Adult ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	virtual ~Osmia_InCocoon();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief Set method for m_OverwinteringTempThreshold */
	void SetOverwinteringTempThreshold(double a_temp) { m_OverwinteringTempThreshold = a_temp; }
protected:
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Behavioural state for emerging from the InCocoon */
	virtual TTypeOfOsmiaState st_Emerge(void);;
	/** \brief Daily mortality test */
	virtual bool DailyMortality() { if (g_rand_uni() < m_DailyDevelopmentMortOverWinter[m_TempToday + 30]) return true; else return false; }
	/** \brief Parameter for overwintering day degrees threshold */
	static double m_OverwinteringTempThreshold;
};

class Osmia_Female : public Osmia_InCocoon
{
protected:
	/** \brief Flag for controlling provionsing behaviour switch on/off. When set to true the female dies */
	bool m_ProvisoningDone;
	/** \brief Contains the number of all eggs yet to lay */
	int m_EggsToLay;
	/** \brief Contains the number of female eggs yet to lay */
	int m_FemaleEggsToLay;
	/** \brief a flag determining if dispersal is necessary */
	bool m_ToDisperse;
	/** \brief Static array of distance probablilties for nest searching and foraging */
	static array<int, __OSMIA_DIST_SIZE> m_generalmovementdistances;
	/** \brief Static array of distance probablilties for dispersal */
	static array<int, __OSMIA_DIST_SIZE> m_dispersalmovementdistances;
	/** \brief pointer to the pollen map object */
	static PollenMap* m_ThePollenMap;

public:
	/** \brief Osmia_Female constructor */
	Osmia_Female(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia_Female ReInit for object pool */
	virtual void ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age);
	/** \brief Osmia_Female destructor */
	virtual ~Osmia_Female();
	/** \brief Osmia_Femae initialisation code for Constructor and ReInit */
	virtual void Init();
	/** \brief The Step is the second 'part' of the timestep that an animal can behave in. It is called continuously until all animals report that they are 'DONE'. */
	virtual void Step(void);
	/** \brief Movement distribution for normal everyday activity */
	void SetGeneralMovement(array<int, __OSMIA_DIST_SIZE> a_movedistribution) { m_generalmovementdistances = a_movedistribution; }
	/** \brief Movement distribution for dispersal activity */
	void SetDispersalMovement(array<int, __OSMIA_DIST_SIZE> a_movedistribution) { m_dispersalmovementdistances = a_movedistribution; }
	/** \brief Set the pollen map pointer */
	void SetPollenMap(PollenMap* a_map) { m_ThePollenMap = a_map;  }
protected:

	virtual void st_Dying(void)
	{
		// this one is needed because all other stages free a nest space when they die
		m_CurrentStateNo = -1; // this will kill the animal object and free up space
	}
	//--------------------------------------------------------------------------------------------------------------------------------
	/** \brief Behavioural state development */
	virtual TTypeOfOsmiaState st_Develop(void);
	/** \brief Find a suitable location for a nest */
	virtual bool FindNestLocation(void);
	/** \brief This checks for the need to dispese and does it if necessary. */
	virtual TTypeOfOsmiaState st_Dispersal(void);
	/** \brief This checks for the need to do something regarding reproduction and does it if necessary. */
	virtual TTypeOfOsmiaState st_ReproductiveBehaviour(void);
	/** \brief This calculates the number of eggs the female should lay. **Ela** */
	void CalculateEggLoad() { m_EggsToLay = 16; m_FemaleEggsToLay = 8; }
	/** \brief The location of the current nest, holds -1 in m_x when no nest */
	APoint m_CurrentNestLoc;
	/** \brief The number of days of post emergence life */
	int m_emerge_age;
	/** \brief The number needed to provision a nest cell  */
	int m_provisioningtime;
};



#endif
