/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Osmia.cpp
Version of  May 2017 \n
By Chris J. Topping \n \n
*/


#include <iostream>
#include <fstream>
#include<vector>


#pragma warning( push )
#pragma warning( disable : 4100)
#pragma warning( disable : 4127)
#pragma warning( disable : 4244)
#pragma warning( disable : 4267)
#include <blitz/array.h>
#pragma warning( pop ) 
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../Osmia/Osmia.h"
#include "../Osmia/Osmia_Population_Manager.h"

#include "../BatchALMaSS/BoostRandomGenerators.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
extern boost::variate_generator<base_generator_type&, boost::uniform_int<> > g_rand_uni2; // 0 to 9999

//---------------------------------------------------------------------------

using namespace std;

//---------------------------------------------------------------------------

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
extern MapErrorMsg *g_msg;
extern const int Vector_x[8];
extern const int Vector_y[8];

// Day degree development curves for Osmia
/** \brief Is the number of day degrees needed for egg hatch above the developmental threshold_E */
static CfgFloat cfg_OsmiaEggDevelTotalDD("OSMIA_EGGDEVELDD", CFG_CUSTOM, 93.0);
/** \brief Is temperature developmental threshold for egg development */
static CfgFloat cfg_OsmiaEggDevelThreshold("OSMIA_EGGDEVELTHRESHOLD", CFG_CUSTOM, 0.0);
/** \brief Is the number of day degrees needed for larval hatch above the developmental threshold_E */
static CfgFloat cfg_OsmiaLarvaDevelTotalDD("OSMIA_LARVADEVELDD", CFG_CUSTOM, 656.0);
/** \brief Is temperature developmental threshold for larval development */
static CfgFloat cfg_OsmiaLarvaDevelThreshold("OSMIA_LARVADEVELTHRESHOLD", CFG_CUSTOM, 0.0);
/** \brief Is the number of day degrees needed for pupal hatch above the developmental threshold_E */
static CfgFloat cfg_OsmiaPupaDevelTotalDD("OSMIA_PUPADEVELDD", CFG_CUSTOM, 1414.0);
/** \brief Is temperature developmental threshold for pupal development */
static CfgFloat cfg_OsmiaPupaDevelThreshold("OSMIA_PUPADEVELTHRESHOLD", CFG_CUSTOM, 0.0);
/** \brief Is the number of day degrees needed for InCocoon hatch above the developmental threshold_E */
//static CfgFloat cfg_OsmiaInCocoonDevelTotalDD("OSMIA_INCOCOONDEVELDD", CFG_CUSTOM, 100.0);
/** \brief Is temperature developmental threshold for InCocoon development */
//static CfgFloat cfg_OsmiaInCocoonDevelThreshold("OSMIA_INCOCOONDEVELTHRESHOLD", CFG_CUSTOM, 0.0);
/** \brief Is the number of day degrees needed for InCocoon hatch above the developmental threshold_E */
CfgFloat cfg_OsmiaIncCocoonOverwinteringTempThreshold("OSMIA_INCOCOONOVERWINTERINGTEMPTHRESHOLD", CFG_CUSTOM, 0.0);

// Mortality parameters for Osmia
/** \brief The daily background mortality rate for females */
static CfgFloat cfg_OsmiaFemaleBckMORT("OSMIA_FEMALEBACKMORT", CFG_CUSTOM, 0.05);

// Miscellaneous parameters


//********************************************************************************************************************************
//**************************************** Osmia_Base Definition ******************************************************************
//*******************************************************************************************************************************/

Osmia_Base::Osmia_Base(int a_x, int a_y,Landscape* p_L, Osmia_Population_Manager* p_NPM, int a_age) : TAnimal(a_x,a_y,p_L)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = p_NPM;
	m_CurrentOState = toOsmias_InitialState;
	SetAge(a_age); // Set the age
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Base::ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age) {
	TAnimal::ReinitialiseObject(a_x, a_y, p_L);
	// Assign the pointer to the population manager
	m_OurPopulationManager = p_OPM;
	m_CurrentOState = toOsmias_InitialState;
	SetAge(a_age); // Set the age
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_Base::~Osmia_Base(void)
{
	;
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Base::st_Dying( void )
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
	m_OurLandscape->ReleaseOsmiaNest(m_Location_x, m_Location_y);
}
//--------------------------------------------------------------------------------------------------------------------------------

//********************************************************************************************************************************
//**************************************** Osmia_Egg Definition ******************************************************************
//*******************************************************************************************************************************/

Osmia_Egg::~Osmia_Egg(void)
{
	;
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_Egg::Osmia_Egg(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age) : Osmia_Base(a_x, a_y, p_L, p_OPM, a_age)
{
	m_AgeDegrees = 0;
	m_Age = 0;
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Egg::ReInit(int a_x, int a_y, Landscape* p_L, Osmia_Population_Manager* p_OPM, int a_age) {
	Osmia_Base::ReInit(a_x, a_y, p_L, p_OPM, a_age);
	m_AgeDegrees = 0;
	m_Age = 0;
	;
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Egg::Step(void)
{
	/**
	* Osmia egg behaviour is simple. It calls develop until the egg hatches or dies.
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentOState)
	{
	case toOsmias_InitialState: // Initial state always starts with develop
		m_CurrentOState = toOsmias_Develop;
		break;
	case toOsmias_Develop:
		m_CurrentOState = st_Develop(); // Will return movement or die
		m_StepDone = true;
		break;
	case toOsmias_NextStage:
		m_CurrentOState = st_Hatch(); // Will return movement or die
		break;
	case toOsmias_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Osmia_Egg::Step()", "unknown state - default");
		std::exit(1);
	}
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Egg::st_Develop(void)
{
	/*
	* Development is preceded by a mortality test, then a day degree calculation is made to determine the development that occured in the last 24 hours.
	* When enough day degrees are achieved the egg hatches.If it does not hatch then the development behaviour is queued up for the next day.
	*/
	if (DailyMortality()) return toOsmias_Die; 
	double DD = m_OurLandscape->SupplyTemp()- cfg_OsmiaEggDevelThreshold.value();
	if (DD > 0) m_AgeDegrees += DD;
	if (m_AgeDegrees > cfg_OsmiaEggDevelTotalDD.value()) return toOsmias_NextStage;
	return toOsmias_Develop;
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Egg::st_Hatch(void)
{
	KillThis();
	/**
	* Creates a new larva object and passes the data from the egg to it, then signals young object removal.
	*/
	struct_Osmia sO;
	sO.OPM = m_OurPopulationManager;
	sO.L = m_OurLandscape;
	sO.age = m_Age;
	sO.x = m_Location_x;
	sO.y = m_Location_y;
	m_OurPopulationManager->CreateObjects(to_OsmiaLarva, NULL, &sO, 1); // 

	return toOsmias_Die;
}
//--------------------------------------------------------------------------------------------------------------------------------


//********************************************************************************************************************************
//**************************************** Osmia_Larva Definition ******************************************************************
//*******************************************************************************************************************************/

void Osmia_Larva::ReInit(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age)
{
	Osmia_Egg::ReInit(a_x, a_y, p_L, p_OPM, a_age);
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_Larva::~Osmia_Larva(void)
{
	;
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_Larva::Osmia_Larva(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age) : Osmia_Egg(a_x, a_y, p_L, p_OPM, a_age)
{
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Larva::Step(void)
{
	/**
	* Osmia larva behaviour is simple. It calls develop until the larva pupates or dies.
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentOState)
	{
	case toOsmias_InitialState: // Initial state always starts with develop
		m_CurrentOState = toOsmias_Develop;
		break;
	case toOsmias_Develop:
		m_CurrentOState = st_Develop(); 
		m_StepDone = true;
		break;
	case toOsmias_NextStage:
		m_CurrentOState = st_Pupate(); 
		break;
	case toOsmias_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Osmia_Larva::Step()", "unknown state - default");
		std::exit(1);
	}
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Larva::st_Develop(void)
{
	if (DailyMortality()) return toOsmias_Die;
	double DD = m_OurLandscape->SupplyTemp() - cfg_OsmiaLarvaDevelThreshold.value();
	if (DD > 0) m_AgeDegrees += DD;
	if (m_AgeDegrees > cfg_OsmiaLarvaDevelTotalDD.value()) return toOsmias_NextStage;
	return toOsmias_Develop;
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Larva::st_Pupate(void)
{
	KillThis();
	/**
	* Creates a new pupa object and passes the data from the larva to it, then signals young object removal.
	*/
	struct_Osmia sO;
	sO.OPM = m_OurPopulationManager;
	sO.L = m_OurLandscape;
	sO.age = m_Age;
	sO.x = m_Location_x;
	sO.y = m_Location_y;
	m_OurPopulationManager->CreateObjects(to_OsmiaPupa, NULL, &sO, 1); // 

	return toOsmias_Die;
}
//--------------------------------------------------------------------------------------------------------------------------------


//********************************************************************************************************************************
//**************************************** Osmia_Pupa Definition ******************************************************************
//*******************************************************************************************************************************/

void Osmia_Pupa::ReInit(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age)
{
	Osmia_Larva::ReInit(a_x, a_y, p_L, p_OPM, a_age);
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_Pupa::~Osmia_Pupa(void)
{
	;
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_Pupa::Osmia_Pupa(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age) : Osmia_Larva(a_x, a_y, p_L, p_OPM, a_age)
{
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Pupa::Step(void)
{
	/**
	* Osmia pupa behaviour is simple. It calls develop until the pupa emerges or dies.
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentOState)
	{
	case toOsmias_InitialState: // Initial state always starts with develop
		m_CurrentOState = toOsmias_Develop;
		break;
	case toOsmias_Develop:
		m_CurrentOState = st_Develop(); 
		m_StepDone = true;
		break;
	case toOsmias_NextStage:
		m_CurrentOState = st_Emerge(); // Will cause the pupa object to be replaced with an adult
		break;
	case toOsmias_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Osmia_Pupa::Step()", "unknown state - default");
		std::exit(1);
	}
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Pupa::st_Develop(void)
{
	if (this->DailyMortality()) return toOsmias_Die;
	double DD = m_OurLandscape->SupplyTemp() - cfg_OsmiaPupaDevelThreshold.value();
	if (DD > 0) m_AgeDegrees += DD;
	if (m_AgeDegrees > cfg_OsmiaPupaDevelTotalDD.value()) return toOsmias_NextStage;
	return toOsmias_Develop;
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Pupa::st_Emerge(void)
{
	/**
	* Determines sex, and creates a new Osmia adult object and passes the data from the pupa to it, then signals young object removal.
	*/
	struct_Osmia sO;
	sO.OPM = m_OurPopulationManager;
	sO.L = m_OurLandscape;
	sO.age = m_Age;
	sO.x = m_Location_x;
	sO.y = m_Location_y;
	m_OurPopulationManager->CreateObjects(to_OsmiaInCocoon, NULL, &sO, 1);
	KillThis(); // sets current state to -1 and StepDone to true;
	return toOsmias_Die;
}
//--------------------------------------------------------------------------------------------------------------------------------

//********************************************************************************************************************************
//**************************************** Osmia_InCocoon Definition ******************************************************************
//*******************************************************************************************************************************/

void Osmia_InCocoon::ReInit(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age)
{
	Osmia_Pupa::ReInit(a_x, a_y, p_L, p_OPM, a_age);
	m_overwinterflag1 = false;
	m_overwinterflag2 = false;
	m_emergencecounter = 99999;
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_InCocoon::~Osmia_InCocoon(void)
{
	;
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_InCocoon::Osmia_InCocoon(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age) : Osmia_Pupa(a_x, a_y, p_L, p_OPM, a_age)
{
	m_overwinterflag1 = false;
	m_overwinterflag2 = false;
	m_emergencecounter = 99999;
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_InCocoon::Step(void)
{
	/**
	* Osmia adult in cocoon behaviour is simple. It calls develop until the adult in cocoon emerges or dies.
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentOState)
	{
	case toOsmias_InitialState: // Initial state always starts with develop
		m_CurrentOState = toOsmias_Develop;
		break;
	case toOsmias_Develop:
		m_CurrentOState = st_Develop(); 
		m_StepDone = true;
		break;
	case toOsmias_NextStage:
		m_CurrentOState = st_Emerge(); // Will cause the pupa object to be replaced with an adult
		break;
	case toOsmias_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Osmia_InCocoon::Step()", "unknown state - default");
		std::exit(1);
	}
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_InCocoon::st_Develop(void)
{
	/**
	* This is/must be called each day. m_overwinterflag1 is set to false by the constructor.
	* Like the other stages we have a temperature related daily mortality chance.
	* Once we reach January then we start a counter to find March (because we start in the previous year at some point)
	* After March 1st we wait for cfg_OsmiaIncCocoonWarmDayThreshold days of cfg_OsmiaIncCocoonWarmDayTemp temperature then start the temperature related development.
	*/
	if (DailyMortality()) return toOsmias_Die;
	if (m_OurLandscape->SupplyDayInYear() == November) m_overwinterflag1 = true;
	if (m_overwinterflag1)
	{
		if (m_OurLandscape->SupplyDayInYear() == March) {
			m_overwinterflag2 = true;
			// Calculate the number of days **Ela**
			m_emergencecounter = int(((-0.014)*m_AgeDegrees) + 40.8);
		}
		if (!m_overwinterflag2)
		{
			double DD = m_TempToday - cfg_OsmiaIncCocoonOverwinteringTempThreshold.value();
			if (DD > 0) m_AgeDegrees += DD;
		}
		else
		{
			if (--m_emergencecounter < 1) return toOsmias_NextStage;
		}
	} 
	return toOsmias_Develop;
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_InCocoon::st_Emerge(void)
{
	/**
	* Determines sex, and creates a new Osmia adult object and passes the data from the pupa to it, then signals young object removal.
	*/
	struct_Osmia sO;
	sO.OPM = m_OurPopulationManager;
	sO.L = m_OurLandscape;
	sO.age = m_Age;
	sO.x = m_Location_x;
	sO.y = m_Location_y;
	m_OurLandscape->ReleaseOsmiaNest(m_Location_x, m_Location_y);
	m_OurPopulationManager->CreateObjects(to_OsmiaFemale, NULL, &sO, 1);
	KillThis(); // sets current state to -1 and StepDone to true;
	return toOsmias_Die;
}
//--------------------------------------------------------------------------------------------------------------------------------

//********************************************************************************************************************************
//**************************************** Osmia_Female Definition ******************************************************************
//*******************************************************************************************************************************/

void Osmia_Female::ReInit(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age)
{
	/** 
	* ReInit is used to enable the object pool, the method used to prevent many new/delete calls for objects that reuseable.
	*/
	Init();
	Osmia_InCocoon::ReInit(a_x, a_y, p_L, p_OPM, a_age);
}
//--------------------------------------------------------------------------------------------------------------------------------

Osmia_Female::~Osmia_Female(void)
{
	;
}
//--------------------------------------------------------------------------------------------------------------------------------
Osmia_Female::Osmia_Female(int a_x, int a_y, Landscape * p_L, Osmia_Population_Manager * p_OPM, int a_age) : Osmia_InCocoon(a_x, a_y, p_L, p_OPM, a_age)
{
	/**
	* Constructor needs to initiate reproductive flags and set the number of eggs that can be produced. This is done by Init - which is shared with ReiInit
	*/
	Init();
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Female::Init()
{
	/**
	* Constructor needs to initiate reproductive flags and set the number of eggs that can be produced. This is done by Init - which is shared with ReiInit
	*/
	m_ProvisoningDone = false;
	CalculateEggLoad(); // 
	m_ToDisperse = false;
	m_emerge_age = 0;
	m_CurrentNestLoc.m_x = -1;
}
//--------------------------------------------------------------------------------------------------------------------------------

void Osmia_Female::Step(void)
{
	/**
	* The Osmia female step code is the main behavioural control for the female Osmia. 
	* The main loop runs through Develop, which calls disperse. If dispersal is needed this is carried out before 
	* reproduction behaviour is called. Completion of this ends the step (each step is assumed to be one day).
	*/
	if (m_StepDone || m_CurrentStateNo == -1) return;
	switch (m_CurrentOState)
	{
	case toOsmias_InitialState: // Initial state always starts with develop, in this case it ages and determines the next action
		m_CurrentOState = toOsmias_Develop;
		break;
	case toOsmias_Develop:
		m_CurrentOState = st_Develop();
		if (m_CurrentOState == toOsmias_Develop) m_StepDone = true;
		break;
	case toOsmias_Disperse:
		m_CurrentOState = st_Dispersal(); // Will return movement or die
		break;
	case toOsmias_ReproductiveBehaviour:
		m_CurrentOState = st_ReproductiveBehaviour(); // Will return develop or die
		m_StepDone = true;
		break;
	case toOsmias_Die:
		st_Dying(); // No return value - no behaviour after this
		m_StepDone = true;
		break;
	default:
		m_OurLandscape->Warn("Osmia_Female::Step()", "unknown state - default");
		exit(1);
	}
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Female::st_Develop(void)
{
	/** 
	* First we need to check for death causes. If not dead then either the bee is in dispersal or reproduction behaviour. 
	*/
	if (g_rand_uni() < cfg_OsmiaFemaleBckMORT.value()) return toOsmias_Die;
	if (m_emerge_age++ > 2) return toOsmias_ReproductiveBehaviour;
	return toOsmias_Develop;
}
//--------------------------------------------------------------------------------------------------------------------------------

bool Osmia_Female::FindNestLocation(void)
{
	/**
	* Finds a suitable location (x,y) in a suitable polygon for placing a nest.
	* Check if we can make the nest here
	* If not makes a movement and check around. 
	*/
	if (m_OurLandscape->SupplyOsmiaNest(m_Location_x, m_Location_y)) {
		m_CurrentNestLoc.m_x = m_Location_x;
		m_CurrentNestLoc.m_y = m_Location_y;
		return true;
	}
	int movedist = m_generalmovementdistances[g_rand_uni2()];
	unsigned dir = m_Location_x;
	for (int d = 0; d < 8; d++) {
		dir = (dir + d) & 7;
		int x = m_Location_x + Vector_x[dir] * movedist;
		int y = m_Location_y + Vector_y[dir] * movedist;
		m_OurLandscape->CorrectCoords(x, y); // For wrap around
		if (m_OurLandscape->SupplyOsmiaNest(x,y)) {
			m_CurrentNestLoc.m_x = x;
			m_CurrentNestLoc.m_y = y;
			return true;
		}
	}
	return false;
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Female::st_Dispersal(void)
{
	/**
	* This is a single random direction jump
	*/
	int movedist = m_dispersalmovementdistances[g_rand_uni2()];
	unsigned dir = m_Location_x & 7;
	int x = m_Location_x + Vector_x[dir] * movedist;
	int y = m_Location_y + Vector_y[dir] * movedist;
	m_OurLandscape->CorrectCoords(x, y); // For wrap around
	m_Location_x = x;
	m_Location_y = y;
	return toOsmias_Develop;
}
//--------------------------------------------------------------------------------------------------------------------------------

TTypeOfOsmiaState Osmia_Female::st_ReproductiveBehaviour(void)
{
	/**
	* Finds a suitable location (x,y) in a suitable polygon for placing a nest.
	* - this is a stub to be expanded
	*/
	if ((m_EggsToLay < 1) && (!m_ProvisoningDone)) return toOsmias_Die;
	// Add code here to do the work
	// **Ela** here we might need to consider a probability of males or assume all are female first.
	if (!m_ProvisoningDone)
	{
		if (m_FemaleEggsToLay > 0)
		{
			if (!FindNestLocation()) return toOsmias_Disperse;
			if (m_ThePollenMap->CalculatePollenQuality(m_CurrentNestLoc.m_x, m_CurrentNestLoc.m_y) < 1.0) return toOsmias_Disperse; // **ela** 1.0 needs to be replaced with something sensible
			m_EggsToLay--;
			m_FemaleEggsToLay--;
			// We have a nest space, so now we need to know how long the nest provisioning will take
			if (true) // **Ela** we need the conditions here
			{
				m_ProvisoningDone = false;
			}
			else m_ProvisoningDone = true;
			struct_Osmia sO;
			sO.OPM = m_OurPopulationManager;
			sO.L = m_OurLandscape;
			sO.age = m_Age;
			sO.x = m_CurrentNestLoc.m_x;
			sO.y = m_CurrentNestLoc.m_y;
			m_OurLandscape->IncOsmiaNest(m_CurrentNestLoc.m_x, m_CurrentNestLoc.m_y);
			m_OurPopulationManager->CreateObjects(to_OsmiaEgg, NULL, &sO, 1);
		}
	}
	else m_ProvisoningDone = false;
	return toOsmias_Develop;
}
//--------------------------------------------------------------------------------------------------------------------------------

