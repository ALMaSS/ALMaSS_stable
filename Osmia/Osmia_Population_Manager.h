/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Osmia_Population_Manager.h 
Version of  May 2017 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef Osmia_Population_ManagerH
#define Osmia_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Osmia;

//------------------------------------------------------------------------------

class PollenMap
{
protected:
	/** \brief The number of m_smallcellsize that make a single grid square */
	int m_cellsize;
	/** \brief The thius is the power of 2 of the small cell size i. 0 for 1, 1 for 2, , 2 for 4, 3 for 8 etc. */
	int m_smallcellsizeshift;
	/** \brief The step size when sampling the landscape for pollen */
	int m_smallcellsize;
	/** \brief The max pollen foraging distance */
	int m_maskradius;
	/** \brief The max x extent possible for calculating pollen, the other bound is zero */
	int m_x_bound;
	/** \brief The max y extent possible for calculating pollen, the other bound is zero */
	int m_y_bound;
	int m_masksize;
	int m_maxX;
	int m_maxY;
	blitz::Array<double, 2> m_pollenmap;
	blitz::Array<double, 2> m_nectarmap;
	blitz::Array<double, 2> m_mask;
	Landscape* m_TheLandscape;
public:
	PollenMap(int a_smallcellsize, int a_maskradius, Landscape* a_land);
	PollenMap() {
		;
	}
	/** \brief Fills the pollen map with current information */
	void RefillPollenMap();
	/** \brief Fills the nectar map with current information */
	void RefillNectarMap();
	/** \brief Creates the quality with distance scaling mask */
	void CreateMask();
	/** \brief Get pollen quality returned as a double */
	double CalculatePollenQuality(int a_x, int a_y);
	/** \brief Get pollen quality returned as a double */
	double CalculateNectarQuality(int a_x, int a_y);
};


/**
\brief
Used for creation of a new Osmia object
*/
class struct_Osmia
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief age */
  int age;
  /** \brief the sex of the Osmia */
  bool sex;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief Osmia_Population_Manager pointer */
  Osmia_Population_Manager * OPM;
};

/**
\brief
The class to handle all predator population related matters
*/
class Osmia_Population_Manager : public Population_Manager
{
public:
// Methods
   /** \brief Osmia_Population_Manager Constructor */
   Osmia_Population_Manager(Landscape* L);
   /** \brief Used to collect data initialisation together */
   void Init();
   /** \brief Osmia_Population_Manager Destructor */
   virtual ~Osmia_Population_Manager (void);
   /** \brief Method for creating a new individual Osmia */
   void CreateObjects(int ob_type, TAnimal *pvo, struct_Osmia* data, int number);
   /** \brief Add a new egg production to the stats record */
   void RecordEggProduction(int a_eggs);

protected:
// Attributes
	/** \brief A class for holding the stats on Osmia egg production */
	SimpleStatistics m_OsmiaEggProdStats;
	/** \brief A popinter to the pollen map object */
	PollenMap* m_ThePollenMap;

// Methods
   /** \brief  Things to do before anything else at the start of a timestep  */
   virtual void DoFirst(){
	   double temp = m_TheLandscape->SupplyTemp();
	   Osmia_Base ob(0, 0, m_TheLandscape, this, 0);
	   ob.SetTemp(temp); // Sets the static variable for temperature for all Osmia (speed optimisation)
	   m_TheLandscape->UpdateOsmiaNesting();
   }
   /** \brief Things to do before the Step */
   virtual void DoBefore(){
	   m_ThePollenMap->RefillPollenMap();
   }
   /** \brief Things to do before the EndStep */
   virtual void DoAfter(){}
   /** \brief Things to do after the EndStep */
   virtual void DoLast(){}
};

#endif