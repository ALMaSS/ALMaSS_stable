/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
// Version of 30th January 2001
//---------------------------------------------------------------------------
#ifndef Spider_allH
#define Spider_allH
//---------------------------------------------------------------------------

// **FN**
//const AnsiString SimulationName="Spider";

typedef enum
{
  // Egg
  tosps_Initiation=0,
  tosps_Develop,
  tosps_Hatch,
  // Juvenile
  tosps_JDevelop,
  tosps_JBalloon,
  tosps_JAssessHabitat,
  tosps_Mature,
  tosps_JWalk,
  tosps_JMove,
  // Female
  tosps_FAssessHabitat,
  tosps_Reproduce,
  tosps_FBalloon,
  tosps_FWalk,
  tosps_FMove,
  tosps_Dying,
  // Destroy
  tosps_Destroy
} TTypesOfSpiderState;

//------------------------------------------------------------------------------
// Forward Declarations

class Spider_Population_Manager;
class Landscape;
class JuvPosMap;
class AdultPosMap;

//------------------------------------------------------------------------------

class struct_Spider
{
 public:
  int x;
  int y;
  Landscape* L;
  Spider_Population_Manager * SpPM;
  int noEggs;
};
//------------------------------------------------------------------------------

class Spider_Base : public TAnimal
{
public:
    // **FN**
    double m_pesticide_accum;
    Spider_Base(int x, int y,Landscape* L, Spider_Population_Manager* SpPM);
    virtual void BeginStep(){}
    virtual void Step(){}
    virtual void EndStep(){}
    TTypesOfSpiderState CurrentSpState;
    Spider_Population_Manager * m_OurPopulation;
    virtual int WhatState() {return CurrentSpState;}
    virtual bool OnFarmEvent(FarmToDo /* event */) { return true; }
	virtual bool GetPosMapPositive( unsigned , unsigned , unsigned  ) {return false; }
protected:
};
//------------------------------------------------------------------------------

class Spider_Egg : public Spider_Base
{
public:
    Spider_Egg(int x, int y,Landscape* L, Spider_Population_Manager* SpPM,
                                                                      int Eggs);
    virtual void BeginStep();
    virtual void Step();
protected:
    int st_Develop();
    void st_Hatch();
    int m_DateLaid;
    int m_NoEggs;
    void st_Die();
    bool HatchDensityMort(int tx, int ty);
    int CheckJuvPosMap(unsigned x, unsigned y);
public:
    void SetNoEggs(int Eggs) {m_NoEggs=Eggs;}
    int GetNoEggs() {return m_NoEggs;}
    virtual bool OnFarmEvent(FarmToDo event);
	virtual void CopyMyself(int);
	virtual bool GetPosMapPositive( unsigned x, unsigned y, unsigned range );
	//virtual bool GetPosMapPositiveB( unsigned x, unsigned y, unsigned range );
};
//------------------------------------------------------------------------------
class Spider_Mobile : public Spider_Base
{
public:
    Spider_Mobile(int x, int y,Landscape* L, Spider_Population_Manager* SpPM);
    virtual void BeginStep();
    virtual void Step();
    virtual void EndStep();
    virtual bool OnFarmEvent(FarmToDo /* event */) { return true; }
	virtual void CopyMyself(int a_sptype);
protected:
	char m_MyDirection;
    bool m_MustBalloon;
    int m_BadHabitatDays;
    bool BallooningMortality(int dist);
    int BalloonTo(int direction,int distance);
    int WalkTo(int direction);
    int st_AssessHabitat();
    int st_Balloon();
    int st_Walk();
    int AssessFood();
    int CheckToleTovIndex();
    void AddToStarvationDays() { if (m_BadHabitatDays<39) m_BadHabitatDays++;}
    bool StarvationMort();
    virtual int CheckPosMap(unsigned /* x */, unsigned /* y */) {return 0;}
    virtual int GetPosMapDensity(unsigned /* x */, unsigned /* y */, unsigned /* range */) {return 0;}
	virtual bool GetPosMapPositive( unsigned, unsigned , unsigned  ) { return false; }
	//virtual bool GetPosMapPositiveB( unsigned , unsigned , unsigned  ) { return false; }
	virtual void ClearPosMap(unsigned , unsigned ) {;}
	virtual void SetPosMap( unsigned , unsigned  ) {;}
};
//------------------------------------------------------------------------------
class Spider_Juvenile : public Spider_Mobile
{
public:
    Spider_Juvenile(int x, int y,Landscape* L, Spider_Population_Manager* SpPM);
    virtual void BeginStep();
    virtual void Step();
    virtual bool OnFarmEvent(FarmToDo event);
	void SpecialWinterMort();
protected:
    double m_AgeDegrees;
    int m_Age;
    int st_Develop();
    void Maturation();
    bool StarvationMort();
    virtual int CheckPosMap(unsigned x, unsigned y);
	virtual int GetPosMapDensity( unsigned x, unsigned y, unsigned range ) ;
	virtual bool GetPosMapPositive( unsigned x, unsigned y, unsigned range );
	//virtual bool GetPosMapPositiveB( unsigned x, unsigned y, unsigned range );
    virtual void ClearPosMap(unsigned x, unsigned y);
	virtual void SetPosMap( unsigned x, unsigned y );
};
//------------------------------------------------------------------------------

class Spider_Adult : public Spider_Mobile
{
public:
    Spider_Adult(int x, int y, Landscape* L,Spider_Population_Manager* SpPM);
    virtual void Step();
    virtual void BeginStep();
    virtual bool OnFarmEvent(FarmToDo /* event */) { return true; }
protected:
 virtual int CheckPosMap(unsigned x, unsigned y);
 virtual int GetPosMapDensity( unsigned x, unsigned y, unsigned range ) ;
 virtual bool GetPosMapPositive( unsigned x, unsigned y, unsigned range );
 //virtual bool GetPosMapPositiveB( unsigned x, unsigned y, unsigned range );
 virtual void ClearPosMap(unsigned x, unsigned y);
 virtual void SetPosMap( unsigned x, unsigned y );

};
//------------------------------------------------------------------------------

class Spider_Female : public Spider_Adult
{
public:
    Spider_Female(int x, int y, Landscape* L,
                              Spider_Population_Manager* SpPM);
    virtual void BeginStep();
    virtual void Step();
    virtual bool OnFarmEvent(FarmToDo event);
    void ZeroEggSacDegrees() {m_EggSacDegrees=0;}
protected:
    double m_EggSacDegrees;
    int m_EggsProduced;
    int m_lifespan;
    int st_Reproduce();
    bool ProduceEggSac();
    bool StarvationMort();
private:
};
//------------------------------------------------------------------------------

#endif

