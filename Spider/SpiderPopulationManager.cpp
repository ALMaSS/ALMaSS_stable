/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------

#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/MovementMap.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../BatchALMaSS/AOR_Probe.h"
#include "../BatchALMaSS/positionmap.h"
#include "../Spider/spider_all.h"
#include "../Spider/SpiderPopulationManager.h"
//---------------------------------------------------------------------------


extern CfgInt cfg_pm_eventfrequency;
extern CfgInt cfg_pm_eventsize;
extern CfgBool cfg_ReallyBigOutputMonthly_used;
extern CfgBool cfg_RipleysOutput_used;
extern CfgBool cfg_AOROutput_used;
extern CfgBool cfg_ReallyBigOutput_used;
extern CfgInt cfg_DailyJuvMort;

static CfgInt cfg_SpeciesRef( "SPID_SPECIES_REF",CFG_CUSTOM,0); // default=Erigone

static CfgInt cfg_BallooningStart( "SPID_BALLOONINGSTART",CFG_CUSTOM,0);
static CfgInt cfg_BallooningStop( "SPID_BALLOONINGSTOP",CFG_CUSTOM,366);
static CfgInt cfg_EggSacSpread( "SPID_EGG_SAC_SPREAD",CFG_CUSTOM,10);

static CfgFloat cfg_MinWalkTemp( "SPID_MIN_WALK_TEMP",CFG_CUSTOM,3);

static CfgFloat  cfg_MaxBalloonPercent( "SPID_MAXBALLOONPERCENT",CFG_CUSTOM,5 );
static CfgFloat  cfg_MaxDistBalloonable( "SPID_MAXDISTBALLOONABLE", CFG_CUSTOM,2200 );
//thermal threshold for developmet of eggsacs
static CfgFloat cfg_EggDevelThreshold("SPID_EGGDEVELTHRESHOLD",CFG_CUSTOM,0.0);
static CfgFloat cfg_JuvDevelThreshold("SPID_JUVDEVELTHRESHOLD",CFG_CUSTOM,0.0);
//good food levels
static CfgFloat cfg_EggProducSlope("SPID_EGGPRODUCSLOPE",CFG_CUSTOM,0.0317);
//factor to multiply Eggsac production rate by at intermediate food levels
static CfgFloat cfg_EggProducIntemediate("SPID_EGGPRODUCINTERMEDIATE",CFG_CUSTOM, 0.66);
//factor to multiply Eggsac production rate by at poor food levels
static CfgFloat cfg_EggProducPoor("SPID_EGGPRODUCPOOR",CFG_CUSTOM, 0.33);
static CfgFloat cfg_EggDevelRHO25("SPID_EGGVDEVELRH",CFG_CUSTOM,0.145043);
static CfgFloat cfg_EggDevelHA("SPID_EGGVDEVELHA",CFG_CUSTOM,21132.95);
static CfgFloat cfg_JuvDevelRHO25("SPID_EGGVDEVELRH",CFG_CUSTOM,0.131541);
static CfgFloat cfg_JuvDevelHA("SPID_JUVVDEVELHA",CFG_CUSTOM,31273.77);
//factor to multiply developmental rate by at intermediate food levels
static CfgFloat cfg_JuvDevelIntermediate("SPID_JUVDEVELINTERMEDIATE",CFG_CUSTOM,0.66);
// factor to multiply developmental rate by at poor food levels
static CfgFloat cfg_JuvDevelPoor("SPID_JUVDEVELPOOR",CFG_CUSTOM,0.33);
static CfgFloat cfg_EggProducThreshold("SPID_EGGPRODUCTHRESHOLD",CFG_CUSTOM,6.0);
// good food levels
static CfgFloat cfg_EggProducIntercept("SPID_EGGPRODUCINTERCEPT",CFG_CUSTOM,-0.1905);
static CfgFloat cfg_EggProducRHO25("SPID_EGGPRODUCRH",CFG_CUSTOM,0.5340);
static CfgFloat cfg_EggProducHA("SPID_EGGPRODUCHA",CFG_CUSTOM,30981.0);
static CfgInt cfg_SmallestBallooningDistance("SPID_SMALLESTBALLOONDIST",CFG_CUSTOM,70);
static CfgInt cfg_BallooningDistanceInterval("SPID_BALLOONINGSTEP",CFG_CUSTOM,10);
/*  Starvation removed because it happens too rarely
const int StarvationMortChanceAdsThresh    =  407;  // Starved day degrees
//const float StarvationMortChanceAdsPerDay  =  2.92;const float StarvationMortChanceAdsPerDay  =  2.92; //starvation mortality removed
 // percent per day (summed)
const int StarvationMortChanceJuvsThresh   =  275;
//const float StarvationMortChanceJuvsPerDay =  5.85;
const float StarvationMortChanceJuvsPerDay =  5.85;  //starvation mortality removed
*/
const double DroughtSlope =-2.8;
const int Droughtintercepts [3] = {112, 126, 140};
const int DroughtMortConst = 20;
//const int SmallestBallooningDistance = 1;  // 70
//const int BallooningDistanceInterval = 1;  // 50
const double MinBallooningTemp = 5.0;
const int DispDistances [31] =
{
    3,   11,   29,   71,  136,  235,  405,  673, 1054, 1567,
 2185, 2906, 3748, 4693, 5528, 6423, 7226, 7926, 8543, 8990,
 9360, 9614, 9782, 9882, 9950, 9979, 9991, 9997, 9999, 9999,
 10000
};
const double BallooningHours [52] = {
3.67,3.77,3.92,4.08,4.30,4.52,4.77,5.00,5.27,5.53,5.78,6.05,6.32,6.57,6.83,7.08,
7.33,7.58,7.80,8.03,8.22,8.40,8.53,8.62,8.67,8.67,8.60,8.52,8.37,8.20,8.02,7.78,
7.55,7.32,7.07,6.82,6.55,6.32,6.05,5.78,5.53,5.27,5.03,4.78,4.53,4.32,4.12,3.93,
3.77,3.67,3.62,3.60
};


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

/*
class ReturnLessThanX
{
  public:
    bool operator()(TAnimal* A1, int x) const
    {
      return (A1->Supply_m_Location_x()<x);
    }
};
*/
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class ReturnMoreThanX
{
  public:
    bool operator()(int x,TAnimal* A1) const
    {
      return (A1->Supply_m_Location_x()>x);
    }
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//                      Spider_POPULATION_MANAGER
//---------------------------------------------------------------------------


Spider_Population_Manager::Spider_Population_Manager(Landscape* p_L)
   : Population_Manager(p_L)
{
  // Three lists are needed so need to remove 6 of the ten default arrays
  // Eggs,Juvenils,Female
  for (int i=0; i<7; i++)
  {
    TheArray.pop_back();
  }
  AdultPosMap= new SimplePositionMap(p_L);
  JuvPosMap= new SimplePositionMap(p_L);
  EggPosMap= new SimplePositionMap(p_L);
  m_MoveMap=new MovementMap(p_L, 2); // 0 for spiders
  Init();
}
//---------------------------------------------------------------------------


Spider_Population_Manager::~Spider_Population_Manager()
{
 delete AdultPosMap;
 delete JuvPosMap;
 delete EggPosMap;
}
//---------------------------------------------------------------------------


void Spider_Population_Manager::Init()
{
  if ( cfg_RipleysOutput_used.value() ) {
    OpenTheRipleysOutputProbe("");
  }
  if ( cfg_ReallyBigOutput_used.value() ) {
    OpenTheReallyBigProbe();
  } else ReallyBigOutputPrb=0;
 // Set the simulation name
 strcpy(m_SimulationName,"Spider");
 // Other variables
 m_EggSacSpread=cfg_EggSacSpread.value();
 m_DoubleEggSacSpread=2*m_EggSacSpread;
 m_MinWalkTemp=false;
 m_BallooningMortalityPerMeter = cfg_MaxBalloonPercent.value()/cfg_MaxDistBalloonable.value();
 // Create adults
 struct_Spider* aps;
 aps = new struct_Spider;
 aps->SpPM = this;
 aps->L = m_TheLandscape;
 aps->x = random(m_TheLandscape->SupplySimAreaWidth());
 aps->y = random(m_TheLandscape->SupplySimAreaHeight());

 for (int i=0; i<50000; i++) // Start number of spiders
 {
   aps->x = random(m_TheLandscape->SupplySimAreaWidth());
   aps->y = random(m_TheLandscape->SupplySimAreaHeight());
   if (AdultPosMap->GetMapValue(aps->x,aps->y)==0)
   {
      AdultPosMap->SetMapValue(aps->x,aps->y);
      CreateObjects(2,NULL,aps,1);
   }
 }
 delete aps;
 // Load List of Animal Classes
  m_ListNames[0]="Egg";
  m_ListNames[1]="Juvenile";
  m_ListNames[2]="Female";
  m_ListNameLength = 3;
  m_population_type = TOP_Spider;

 // Load State Names
  StateNames[tosps_Initiation] = "Initiation";
  // Egg
  StateNames[tosps_Develop] = "Developing";
  StateNames[tosps_Hatch] = "Hatching";
  StateNames[tosps_Dying] = "Dying";
  // Juvenile
  StateNames[tosps_JDevelop] = "Developing";
  StateNames[tosps_JBalloon] = "Ballooning";
  StateNames[tosps_JAssessHabitat] = "AssessHabitat";
  StateNames[tosps_Mature] = "Maturation";
  StateNames[tosps_Dying] = "Dying";
  // Female
  StateNames[tosps_FAssessHabitat] = "AssessHabitat";
  StateNames[tosps_Reproduce] = "Reproduce";
  StateNames[tosps_FBalloon] = "Ballooning";
  StateNames[tosps_Dying] = "Dying";
  // Destroy
  StateNames[tosps_Destroy] = "Destroy";
 // Ensure that spider's execution order is shuffled after each time step
 // or do nothing if that is prefered
  BeforeStepActions[0]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[1]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing
  BeforeStepActions[2]=0; // 0 = Shuffle, 1 = SortX, 2 = SortY, 3 = do nothing

  m_BallooningWeather = false;

 // Zero the egg degrees
  for (int i=0; i<365; i++) m_EggDegrees[i]=0;
 // Create some data arrays
 // 1. Ballooning distances
 int count =0;
 int distance=cfg_SmallestBallooningDistance.value();
 for (int i=0; i<31; i++)
 {
   while (count<DispDistances[i])
   {
     DispersalDistances[count++]=distance;
   }
   distance+=cfg_BallooningDistanceInterval.value();
 }
/*  Starvation removed because it happens too rarely
 // 2. Starvation chance
 for (int i=0; i<40; i++)
 {
   StarvationMortChance[0][i]=(StarvationMortChanceJuvsPerDay*float(i+1))*100;
   StarvationMortChance[1][i]=(StarvationMortChanceAdsPerDay*float(i+1))*100;
 }
*/
 // 3. Dispersal Time
 count=0;
 for (int i=0; i<52; i++)
  for (int j=0; j<7; j++)
  {
    BallooningHrs[count++]=BallooningHours[i];
  }
  // Zero the rain counter
  m_DaysSinceRain=0;

  // Default set spiders to walking
  m_WalkingOnly=true;
  // Set out value for juv mort
  m_DailyJuvMort=cfg_DailyJuvMort.value();
  EggProdThresholdPassed=false;
  m_EggProdDDegsPoor=0;
  m_EggProdDDegsInt=0;
  m_EggProdDDegsGood=0;
#ifdef __RECORD_RECOVERY_POLYGONS
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::out);
	ofile << "This file records the number of females in each polygon each day" << endl;
	ofile.close();
	/* Open the polygon recovery file and read in polygons to m_RecoveryPolygons */
	ifstream ifile("RecoveryPolygonsList.txt",ios::in);
	int n;
	ifile >> n;
	m_RecoveryPolygons[0] = n;
	for (int i=0; i<n; i++) ifile >> m_RecoveryPolygons[1+i];
	for (int i=0; i<n; i++) m_RecoveryPolygonsC[1+i]=0;
	ifile.close();
#endif
}
//---------------------------------------------------------------------------
#ifdef __RECORD_RECOVERY_POLYGONS
void Spider_Population_Manager::RecordRecoveryPolygons()
{
	/**
	Only used for recovery pesticide experiment. 
	Loops through all beetle and records the numbers in any polygon identified as needing to be recorded in m_RecoveryPolygons[]
	*/
	int sz = (int) TheArray[1].size();
    for (int j=0; j<sz; j++)
    {
      int p = TheArray[1][j]->SupplyPolygonRef();
	  for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	  {
		  if (p == m_RecoveryPolygons[i]) 
		  {
			  m_RecoveryPolygonsC[i]++;
			  break;
		  }
	  }
	}
	sz = (int) TheArray[2].size();
    for (int j=0; j<sz; j++)
    {
      int p = TheArray[2][j]->SupplyPolygonRef();
	  for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	  {
		  if (p == m_RecoveryPolygons[i]) 
		  {
			  m_RecoveryPolygonsC[i]++;
			  break;
		  }
	  }
    }
	/* Open the output file and append */
	ofstream ofile("RecoveryPolygonsCounter.txt",ios::app);
	for (int i=1; i<=m_RecoveryPolygons[0]; i++)
	{
		ofile << m_RecoveryPolygonsC[i] << '\t';
		m_RecoveryPolygonsC[i] = 0;
	}
	ofile << endl;
	ofile.close();
}
//---------------------------------------------------------------------------
#endif

void Spider_Population_Manager::DoFirst()
{
#ifdef __RECORD_RECOVERY_POLYGONS
	/**
	There is also a special bit of code that is used for pesticide tests
	*/
	RecordRecoveryPolygons();
#endif
  int today=m_TheLandscape->SupplyDayInYear();
  if (today==364)
  {
    // Kill all eggs
    unsigned s=(unsigned) TheArray[0].size();
    for (unsigned j=0; j<s; j++)
    {
	  EggPosMap->ClearMapValue( TheArray[0][j]->Supply_m_Location_x(), TheArray[0][j]->Supply_m_Location_y() );
      //TheArray[0][j]->SetCurrentStateNo(-1); removed 15/10/2013 - seems superfluous given the call to KillThis
      TheArray[0][j]->KillThis();
    }
    // Ensure all females have zero egg sac devel. degrees
    s=(unsigned) TheArray[2].size();
    Spider_Female* SFem;
    for (unsigned j=0; j<s; j++)
    {
      SFem = (Spider_Female *)TheArray[2][j];
      SFem->ZeroEggSacDegrees();
    }
	// We also need to remove juveniles that are small and won't make it to adult before breeding
    Spider_Juvenile* SJuv;
    s=(unsigned) TheArray[1].size();
    for (unsigned j=0; j<s; j++)
    {
      SJuv= (Spider_Juvenile *) TheArray[1][j];
	  SJuv->SpecialWinterMort();
    }

	/* DEBUG CODE FOR SLOW LOSS OF POPULATION
	int te=EggPosMap->GetTotalN(SimW, SimH);
	if (te>0) {
		int rubbish=0;
	}
    int ta=AdultPosMap->GetTotalN(SimW, SimH);
    int tj=JuvPosMap->GetTotalN(SimW, SimH);
    unsigned se=TheArray[0].size();
    unsigned sj=TheArray[1].size();
    unsigned sa=TheArray[2].size();
    int total=ta+sa ;
	*/
  }
  // Zero the daydegrees for the beginning of the year
  if (today==0)
  {
    for (int i=0; i<365; i++) m_EggDegrees[i]=0;
    EggProdThresholdPassed=false;
  }
  // Zero the daydegrees for the beginning of the December
  if (today==364)
  {
    for (int i=0; i<365; i++) m_EggDegrees[i]=0;
    EggProdThresholdPassed=false;
  }
  //
  // Set the wind direction for today
//  m_WindDirection=TheLandscape->SupplyWindDirection();
m_WindDirection = random(8);  // until we have data

  // Is it ballooning time of year?

  // **per** New piece of code made by cjt 5th of september 02
   // Is it ballooning time of year?
  if (!((today >=cfg_BallooningStart.value())&&(today<cfg_BallooningStop.value())))
  {
    m_WalkingOnly=true;
  }
  else
  {
    m_WalkingOnly=false;
    // Is it ballooning weather today?
    double TheTemp=m_TheLandscape->SupplyTemp();
    if(TheTemp >=MinBallooningTemp)
    {
      double wi=m_TheLandscape->SupplyWind();
      if (wi<5.0) m_TodaysBallooningTime=17.0-(3.7*wi);
       else m_TodaysBallooningTime=0;
      if (m_TodaysBallooningTime>BallooningHrs[today])
                                   m_TodaysBallooningTime=BallooningHrs[today];
    }
    else m_TodaysBallooningTime=0;
    if (TheTemp>=cfg_MinWalkTemp.value()) m_MinWalkTemp=true;
    else m_MinWalkTemp=false;
  }

     //Create proportion (per mille, i.e. 1000 to hatch) of development finished for eggs and juveniles
  //eggs

  double temp=m_TheLandscape->SupplyTemp();
  double Added=0;
  if (temp>cfg_EggDevelThreshold.value())
        Added=1000*((cfg_EggDevelRHO25.value()*((273.15+temp)/298.15))* //1.987=Gas constant
             exp((cfg_EggDevelHA.value()/1.987)*((1/298.15)-(1/(273.15+temp)))));

      // Add today's day degrees to the day degrees experienced each day to date
  for (int d=0; d<=today; d++)
  {
   m_EggDegrees[d]+=Added;
  }


  // juveniles
  // these differ from the eggs because we have two rates of growth dependent
  // upon the food supply at the spiders location
  if (temp>cfg_JuvDevelThreshold.value())
  {
    m_JuvDegreesGood = 1000*((cfg_JuvDevelRHO25.value()*((273.15+temp)/298.15))*
                 exp((cfg_JuvDevelHA.value()/1.987)*((1/298.15)-(1/(273.15+temp)))));  ;
  }
  else m_JuvDegreesGood=0;
  if (temp>cfg_JuvDevelThreshold.value())
  {

    m_JuvDegreesPoor =cfg_JuvDevelPoor.value()*m_JuvDegreesGood  ;
  }
  else m_JuvDegreesPoor=0;
  if (temp>cfg_JuvDevelThreshold.value())
  {

    m_JuvDegreesIntermediate =cfg_JuvDevelIntermediate.value()*m_JuvDegreesGood  ;
  }
  else m_JuvDegreesIntermediate=0;
  //
  // Eggsac production
	if (EggProdThresholdPassed) {
		if (temp>0) {
			if (cfg_SpeciesRef.value()==0) // Erigone
			{
/* CJT Modified 6 December 2006

				m_EggProdDDegsGood=1000*(cfg_EggProducIntercept.value()
													  +cfg_EggProducSlope.value()*temp);
*/
/*New version   using biophysical model
*/
				//1.069	17794	89.9

				m_EggProdDDegsGood=(1000*((1.069*((273.15+temp)/298.15))* exp((17794/1.987)*((1/298.15)-(1/(273.15+temp))))))-89.9;
			}
			else {
				m_EggProdDDegsGood=1000*((cfg_EggProducRHO25.value()*((273.15+temp)/
						   298.15))* exp((cfg_EggProducHA.value()/1.987)*((1/298.15)-
																   (1/(273.15+temp)))));
			}
			if (m_EggProdDDegsGood<0) m_EggProdDDegsGood=0.0;
			m_EggProdDDegsPoor=cfg_EggProducPoor.value()*m_EggProdDDegsGood;
			m_EggProdDDegsInt=cfg_EggProducIntemediate.value()*m_EggProdDDegsGood;
		}
		else {
			m_EggProdDDegsPoor=0;
			m_EggProdDDegsInt=0;
			m_EggProdDDegsGood=0;
		}
	}
  else
  {
    long date=m_TheLandscape->SupplyGlobalDate();
    double SevenDayTemp=0;
    for (long d=date; d>date-14; d--)
    {
      SevenDayTemp+=(double)m_TheLandscape->SupplyTemp(d);
    }
    SevenDayTemp/=14.0;
	if (SevenDayTemp>cfg_EggProducThreshold.value() ) {
		EggProdThresholdPassed=true;
	  }
  }
  // Work out drought for each of three veg classes
  if (m_TheLandscape->SupplyRain()>0)
  {
    m_DaysSinceRain=0;
    m_TodaysDroughtScore[0]=0;
    m_TodaysDroughtScore[1]=0;
    m_TodaysDroughtScore[2]=0;
  }
  else
  {
    temp=m_TheLandscape->SupplyTemp();
    if (temp<0) temp=0; // means no extra drought at negative temps
    //drought function changed by **per**, instead of three slopes now one slope but three intercepts
    if (m_DaysSinceRain>=(Droughtintercepts[0]+DroughtSlope*temp)) m_TodaysDroughtScore[0]=1;
    if (m_DaysSinceRain>=(Droughtintercepts[1]+DroughtSlope*temp)) m_TodaysDroughtScore[1]=1;
    if (m_DaysSinceRain>=(Droughtintercepts[2]+DroughtSlope*temp)) m_TodaysDroughtScore[2]=1;
  }
  m_TodaysMonth=m_TheLandscape->SupplyMonth();
}
//---------------------------------------------------------------------------

bool Spider_Population_Manager::InSquare(int p_x, int p_y, int p_sqx, int p_sqy, int p_range)
{
    // Simply checks if co-ordinates p_x,p_y are in the square formed by
    // p_sqx,p_sqy,p_sqx+p_range,p_sqy+p_range
    // The function is long but reasonably efficient - I think

    int x_extent = p_sqx+p_range;
    int y_extent = p_sqy+p_range;
    if (x_extent >= SimW)
    {
      if (y_extent >= SimH )  // overlaps TR corner of sim area
      {
        // Top right square (limited by SimAreaHeight & SimAreaWidth
        if ((p_x >=p_sqx) && (p_y >=p_sqy)) return true;
        // Top Left Square (limited by 0,SimAreaHeight)
        if ((p_y >=p_sqy)&& (p_y<y_extent-SimH)) return true;
        // Bottom Left square (limited by 0,0)
        if ((p_x >=x_extent-SimW)&&
                                   (p_y<y_extent-SimH)) return true;
        // Bottom Right square (limited by SimAreaWidth,0)
        if ((p_x >=p_sqx)&& (p_y<y_extent-SimH)) return true;

      }
      else // Overlaps the west edge of the sim area
      {
        if ((p_x >=p_sqx) && (p_y >=p_sqy) && (p_y<y_extent)) return true;
        if ((p_x <x_extent-SimW) && (p_y >=p_sqy)&& (p_y<y_extent)) return true;
      }
    }
    else
    {
      if (y_extent >= SimH) // overlaps top of simulation area
      {
        if ((p_x >=p_sqx) && (p_x<x_extent) &&
                                           (p_y >=p_sqy)) return true;
        if ((p_x >=p_sqx) && (p_x<x_extent) &&
                                   (p_y<y_extent-SimH)) return true;
      }
      else // territory does not overlap end of simulation area
      {
        if ((p_x >=p_sqx) && (p_x<x_extent) &&
                         (p_y >=p_sqy) && (p_y<y_extent)) return true;
      }
    }
    return false; // not in territory
}

//---------------------------------------------------------------------------


void Spider_Population_Manager::CreateObjects(int ob_type,
           TAnimal * /* pvo */, struct_Spider * data,int number)
{
   Spider_Female* new_Female;
   Spider_Juvenile* new_Juvenile;
   Spider_Egg* new_Egg;

   for (int i=0; i<number; i++)
   {
    if (ob_type == 0)
    {
       new_Egg = new Spider_Egg(data->x, data->y, data->L,this,data->noEggs);
       TheArray[ob_type].push_back(new_Egg);
    }
    if (ob_type == 1)
    {
       new_Juvenile = new Spider_Juvenile(data->x, data->y, data->L,this);
       TheArray[ob_type].push_back(new_Juvenile);
    }
    if (ob_type == 2)
    {
       new_Female = new Spider_Female(data->x,data->y, data->L,this);
       TheArray[ob_type].push_back(new_Female);
    }
   }
}
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void Spider_Population_Manager::Catastrophe( void ) {
	// This version simply alters populations on 1st January - it is very dangerous to
	// add individuals in many of the models so beware!!!!

	// First do we have the right day?
	int today = m_TheLandscape->SupplyDayInYear();
	if (today!=1) return;
	// First do we have the right year?
	int year = m_TheLandscape->SupplyYearNumber();
	if (year%cfg_pm_eventfrequency.value()!=0) return;
    Spider_Egg* SpE = NULL;
	Spider_Juvenile* SpJ = NULL;
	Spider_Female*  SpF = NULL;
	// Now if the % decrease is higher or lower than 100 we need to do different things
	int esize=cfg_pm_eventsize.value();
	if (esize<100) {
		unsigned size2;
			size2 = (unsigned) TheArray[ 0 ].size();
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) >= esize)  {
                        SpE = dynamic_cast < Spider_Egg * > ( TheArray[ 0 ] [ j ] );
						SpE->CurrentSpState=tosps_Dying; // Kill it
				}
				}
			size2 = (unsigned) TheArray[ 1 ].size();
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) >= esize) {
                        SpJ = dynamic_cast < Spider_Juvenile * > ( TheArray[ 1 ] [ j ] );
						SpJ->CurrentSpState=tosps_Dying; // Kill it
				}
				}
			size2 = (unsigned) TheArray[ 2 ].size();
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) >= esize) {
                        SpF = dynamic_cast < Spider_Female * > ( TheArray[ 2 ] [ j ] );
						SpF->CurrentSpState=tosps_Dying; // Kill it
				}
				}
		}
	else if (esize>100) {
		// This is a tricky thing to do because we need to duplicate spiders without killing them by placing them on top of each other
		// This also requires a copy method in the target spider
		// esize also needs translating  120 = 20%, 200 = 100% 400 = 300%
		if (esize<200) {
			esize-=100;
			for (unsigned i=0; i<3; i++) {
			unsigned size2;
			size2 = (unsigned) TheArray[ i ].size();
			for ( unsigned j = 0; j < size2; j++ ) {
				if (random(100) < esize) {
                        Spider_Base* AS = dynamic_cast < Spider_Base * > ( TheArray[ i ] [ j ] );
					    AS->CopyMyself(); // Duplicate it
					}
				}
				}
			} else esize/=100; // this will throw away fractional parts so will get 2, 3 4 from 200, 350 400
			// TODO inplement thid code if necessary
		}
	else return; // No change so do nothing
}
//-----------------------------------------------------------------------------

void Spider_Population_Manager::TheAOROutputProbe()
{
	m_AOR_Probe->DoProbe(2);
}
//-----------------------------------------------------------------------------

void Spider_Population_Manager::TheRipleysOutputProbe( FILE* a_prb  ) {
  Spider_Female* FS;
  unsigned totalF=(unsigned) TheArray[2].size();
  int x,y;
  int w = m_TheLandscape->SupplySimAreaWidth();
  int h = m_TheLandscape->SupplySimAreaWidth();
  fprintf(a_prb,"%d %d %d %d %d\n", 0,w ,0, h, totalF);
  for (unsigned j=0; j<totalF; j++)      //adult females
  {
	  FS=dynamic_cast<Spider_Female*>(TheArray[2][j]);
	  x=FS->Supply_m_Location_x();
	  y=FS->Supply_m_Location_y();
	  fprintf(a_prb,"%d\t%d\n", x,y);
  }
  fflush(a_prb);
}

//-----------------------------------------------------------------------------


