// Landscape.h
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer. 
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#define _CRTDBG_MAP_ALLOC

#ifndef TLANDSCAPE_H
#define TLANDSCAPE_H

#include <vector>
#include <fstream>

// General
const int January   =   0;
const int February  =  31;
const int March     =  59;
const int April     =  90;
const int May       = 120;
const int June      = 151;
const int July      = 181;
const int August    = 212;
const int September = 243;
const int October   = 273;
const int November  = 304;
const int December  = 334;

// m_polymapping is a mapping from polygon numbers into
// the list of landscape elements, m_elems.

extern class Pesticide *g_pest;

class RasterMap;
class SkTerritories;
class RodenticidePredators_Population_Manager;
class PesticideMap;
/**  \brief ALMaSS 8 standard directions  */
enum TTypeOfDirections {
	direction_n = 0, direction_ne, direction_e, direction_se, direction_s, direction_sw, direction_w, direction_nw
};

enum TTypesOfPesticideCategory { insecticide = 0, fungicide, herbicide, testpesticide };

enum TTypesOfPesticide
{
	ttop_NoPesticide = -1,
	ttop_AcuteEffects = 0,
	ttop_ReproductiveEffects, //1
	ttop_Vinclozolin, //2
	ttop_ModelinkPesticide, // 3
	ttop_ModelinkPesticide21TWA, // 4
	ttop_GeneticDemo,
	ttop_MultipleEffects, // 6
	ttop_eggshellthinning, 
	ttop_AcuteDelayedEffects
};

/** \brief A list item entry of field polygon reference numbers with associated openness and goose food scores */
struct GooseFieldListItem
{
	int polyref;
	int geese;
	int geesesp[gs_foobar];
	int geeseTimed;
	int geesespTimed[gs_foobar];
	int roostdists[gs_foobar];
	double openness;
	double grain;
	double maize;
	double grass[gs_foobar];  // Available grass forage is species specific
	TTypesOfVegetation vegtype;
	std::string vegtypechr;
	double vegheight;
	double digestability;
	int vegphase;
	std::string previouscrop;
	std::string lastsownveg;
	std::string debugveg;
};

/** \brief A list of GooseFieldListItem s */
typedef std::vector<GooseFieldListItem> GooseFieldList;


//------------------------------------------------------------------------------

/**
\brief
The landscape class containing all environmental and topographical data.
*/
class Landscape
{
	// Version info. Initialized by the constructor.
	char m_versioninfo[30];

	/** \brief List of all the farms. */
	//vector<Farm*> m_farms;
	FarmManager* m_FarmManager;

	/** \brief List of all landscape elements. The index is a sequential number, to get the polynum look this number up in m_polymapping */
	vector<LE*>   m_elems;

	/** \brief The big map */
	RasterMap *m_land;

	/** m_polymapping is a mapping from polygon numbers into
	* the list of landscape elements, m_elems. When using this it is important that it is the poly num and not the
	*  map index that is used. */
	vector<int>          m_polymapping;
  
  /** \brief For specialised pesticide recording */
  PesticideMap* m_PesticideMap;

  // For correcting coordinates before modulus operations.
  // Put here so we saves an indirection when doing inline
  // function calls.
  int           m_width;
  int           m_height;
  int           m_width10;
  int           m_height10;
  int			m_minmaxextent;
  int			m_maxextent;

  /** \brief a flag to ensure centroid calculation on object construction */
  bool m_NeedCentroidCalculation;
  /** \brief a flag to ensure openness calculation on object construction */
  bool m_NeedOpennessCalculation;
  /** Flag to signal that missing polygons exist */
  bool m_DoMissingPolygonsManipulations;
  
  // For veg area dumps
  double        *l_vegtype_areas;
  /** \brief a pointer to the current main population manager */
  Population_Manager *m_ThePopManager;

  RodenticideManager *m_RodenticideManager;
  RodenticidePredators_Population_Manager*  m_RodenticidePreds;
  /** \brief Curve relatning goose intake rates in KJ/min to vegetation height */
  Polynomial2CurveClass* m_GooseIntakeRateVSVegetationHeight_PF;
  Polynomial2CurveClass* m_GooseIntakeRateVSVegetationHeight_BG;
  Polynomial2CurveClass* m_GooseIntakeRateVSVegetationHeight_GL;

#ifdef __RECORDFARMEVENTS
  ofstream *m_farmeventfile;
#endif

public:
  void FillVegAreaData();
  inline double GetVegArea(int v) { return l_vegtype_areas[v]; }
    void DumpVegAreaData(int a_day);
	void SkylarkEvaluation(SkTerritories* a_skt);
	void RodenticidePredatorsEvaluation(RodenticidePredators_Population_Manager* a_rppm);
  /** \brief Get the pointer to the current main population manager */
  Population_Manager * SupplyThePopManager() { return m_ThePopManager; }
  /** \brief Set the pointer to the current main population manager */
  void SetThePopManager(Population_Manager * a_ptr) { m_ThePopManager = a_ptr; }
  int SupplyFarmAnimalCensus(int a_farm_ref, int a_LifeStage);
  int SupplyVegPhase(int a_poly) {
	  return m_elems[m_polymapping[a_poly]]->GetVegPhase();
  }
  void SetPolymapping(int a_index, int a_val) { m_polymapping[a_index] = a_val; }
  int GetPolymapping(int a_index) { return m_polymapping[a_index]; }
#ifdef __RECORDFARMEVENTS
  void RecordEvent(int a_event, int a_day, int a_year) {
	  (*m_farmeventfile) << a_event << '\t' << a_day << '\t' << a_year << endl;
  }
#endif
protected:
	/** \brief Write ASCII file of the ALMaSS map */
	void GISASCII_Output(string outpfile, int UTMX, int UTMY);
// Array for containing the treatment counts.
  int   m_treatment_counts[last_treatment];
  int   m_LargestPolyNumUsed;
  void  AddGreenElement( LE *a_green );
  /** \brief List of pond indexes */
  vector <int>m_PondIndexList;
  /** \brief List of pond polyrefs */
  vector <int>m_PondRefsList;
  /*
  void  RenumberPolys(bool a_checkvalid);
  */
  void  ReadPolys(const char *a_polyfile);
  /** \brief reads in polygon information. Version 2 including centroid and openness information */
  void  ReadPolys2(const char *a_polyfile);
  void  PolysValidate(bool a_exit_on_invalid);
  bool  PolysRemoveInvalid( void );
  void  PolysDump( const char *a_filename );
  void  DumpMap( const char *a_filename );
  void  ConsolidatePolys( void );
  void  CountMapSquares(void);
  void  PolysRenumber(void);
  void RebuildPolyMapping() 
  {  // Rebuild m_polymapping.
	  unsigned int sz = (int)m_elems.size();
	  for (unsigned int i = 0; i < sz; i++) 
	  {
		  m_polymapping[m_elems[i]->GetMapIndex()] = i;
	  }
  }
  void  ForceArea(void);
  void  ChangeMapMapping( void );
  LE*   NewElement( TTypesOfLandscapeElement a_type );
  /** \brief A method for replacing missing values in the map with corrected ones - slow */
  void RemoveMissingValues();
  void  TestCropManagement( void ); // Optional, configurable.
  void VegDump(int x,int y);
  void EventDump(int x,int y, int x2, int y2);
  void EventDumpPesticides( int x1, int y1 );
  /** \brief Prints the sum of day degrees. See #FarmManager::daydegrees.*/
  void DegreesDump(); 
  bool	BorderNeed( TTypesOfLandscapeElement a_letype );
  void  BorderAdd(LE* a_field, TTypesOfLandscapeElement a_type);
  void  BorderRemoval( void );
  /** \brief Removes small polygons from the map */
  int RemoveSmallPolygons( void );
  /** \brief Creates a list of pond polygon refs/indexes for easy look up */
  void CreatePondList();
  //void  OrchardBorderAdd ( LE* a_field );
  void  UnsprayedMarginAdd ( LE* a_field );
  void  UnsprayedMarginScan( LE* a_field, int a_width );
  void  BorderScan( LE* a_field, int a_width );
  bool  BorderTest( int a_fieldpoly, int a_borderpoly, int a_x, int a_y );
  bool  StepOneValid( int a_polyindex, int a_x, int a_y, int step );
  bool  UMarginTest( int a_fieldpoly, int a_borderpoly,int a_x, int a_y, int a_width );
  bool  FindValidXY(int a_field, int &a_x, int &a_y);
  bool  BorderStep(int a_fieldpoly, int a_borderpoly, int* a_x, int* a_y);
  bool  BorderStep(int a_fieldpoly, int a_borderpoly, APoint* a_coord);
  //	int      BorderJump( int a_add );

  void AddBeetleBanks( TTypesOfLandscapeElement a_tole );
  bool BeetleBankPossible( LE * a_field, TTypesOfLandscapeElement a_tole );
  void BeetleBankAdd(int x, int y, int angle, int length , LE* a_field, TTypesOfLandscapeElement a_tole);
  bool FindFieldCenter(LE* a_field, int* x, int* y);
  int FindLongestAxis(int* x, int* y, int* a_length);
  void AxisLoop(int a_poly, int* a_x, int* a_y, int a_axis);
  void AxisLoop(int a_poly, APoint* a_cor, int a_axis);
  void AxisLoopLtd(int a_poly, APoint* a_cor, int a_axis, int a_limit);
  // List of private methods and member elements, which are needed
  // when adding artificial hedgebanks. Forget I did this. One should
  // never store what is essentially local variables within the main
  // class definition.
  vector<int> hb_hedges;
  vector<LE*> hb_new_hbs;

  int		m_x_add[ 8 ];
  int		m_y_add[ 8 ];
  int*        hb_map;
  int         hb_width;
  int         hb_height;
  int         hb_size;
  int         hb_min_x, hb_max_x;
  int         hb_min_y, hb_max_y;
  int         hb_first_free_poly_num;
  int         hb_core_pixels;
  int         hb_border_pixels;
  /** \brief An attribute to hold the pesticide type being tested, if there is one, if not default is -1 */
  TTypesOfPesticide	m_PesticideType;

  void hb_Add( void );
  void hb_AddNewHedgebanks( int a_orig_poly_num );
  int  hb_StripingDist( void );
  void hb_GenerateHBPolys( void );
  void hb_FindHedges( void );
  bool hb_FindBoundingBox( int a_poly_num );
  void hb_UpPolyNumbers( void );
  void hb_ClearPolygon( int a_poly_num );
  void hb_PaintBorder( int a_color );
  bool hb_MapBorder( int a_x, int a_y );
  bool hb_HasOtherNeighbour( int a_x, int a_y );
  bool hb_PaintWhoHasNeighbourColor( int a_neighbour_color,
				     int a_new_color );
  bool hb_HasNeighbourColor( int a_x, int a_y,
			     int a_neighbour_color );
  void hb_MarkTopFromLocalMax( int a_color );
  void hb_MarkTheBresenhamWay( void );
  int  hb_MaxUnpaintedNegNeighbour( int a_x, int a_y );
  void hb_ResetColorBits( void );
  void hb_RestoreHedgeCore( int a_orig_poly_number );
  void hb_DownPolyNumbers( void );
  void hb_Cleanup( void );
  
  //#define HB_TESTING

  void DumpMapGraphics( const char *a_filename );

#ifdef  HB_TESTING
  void hb_dump_map( int a_beginx, int a_width,
		    int a_beginy, int a_height,
		    char* a_filename,
		    bool a_high_numbers );
  int  hb_dump_color( int a_x, int a_y,
		      bool a_high_numbers );
#endif

  // The pesticide engine should I spray flag
  bool m_toxShouldSpray;
  // For the LE signal loop.
  int  le_signal_index;

  void DumpTreatCounters( const char* a_filename );

  bool CIPELandscapeMaker();  

public:
       ~Landscape( void );
        Landscape( void );
  //** \brief Things to do when closing the simulation */
  void SimulationClosingActions();
  // Tick() and TurnTheWorld() are identical, the last one is retained
  // for historical reasons.
  void  Tick( void );
  void  TurnTheWorld( void );

  /** \brief Returns the number of ponds in the landscape */
  int HowManyPonds() { return int(m_PondIndexList.size()); }
  /** \brief Returns random pond index */
  int SupplyRandomPondIndex();
  /** \brief Returns random pond polyref */
  int SupplyRandomPondRef();
  /** \brief Returns the index of a pond based on pondref or -1 if not found */
  int SupplyPondIndex(int a_pondref) {
	  for (int i = 0; i < m_PondIndexList.size(); i++) {
		  if (m_PondRefsList[i] == a_pondref) return m_PondIndexList[i];
	  }
	  return -1;
  }
  /** \brief Sets a male as being present in a pond */
  void SetMaleNewtPresent(int a_InPondIndex) { m_elems[a_InPondIndex]->SetMaleNewtPresent(true); }
  /** \brief Determines if a male is present in a pond */
  bool SupplyMaleNewtPresent(int a_InPondIndex) { return m_elems[a_InPondIndex]->IsMaleNewtPresent(); }
  Farm* SupplyFarmPtr(int a_owner) { return m_FarmManager->GetFarmPtr(a_owner); }
  FarmManager* SupplyFarmManagerPtr() { return m_FarmManager; }

  int SupplyLargestPolyNumUsed() { return m_LargestPolyNumUsed; }

  bool SupplyShouldSpray() {return m_toxShouldSpray;}

  double SupplyVegDigestabilityVector( unsigned int a_index );
  double SupplyVegDigestability( int a_polyref );
  double SupplyVegDigestability( int a_x, int a_y );

  double SupplyVegHeightVector( unsigned int a_index );
  double SupplyVegHeight( int a_polyref );
  double SupplyVegHeight( int a_x, int a_y );

  double SupplyVegBiomassVector( unsigned int a_index );
  double SupplyVegBiomass( int a_polyref );
  double SupplyVegBiomass( int a_x, int a_y );

  int SupplyVegDensity( int a_polyref );
  int SupplyVegDensity( int a_x, int a_y );

  double SupplyWeedBiomass( int a_polyref );
  double SupplyWeedBiomass( int a_x, int a_y );

  PollenNectarQuality SupplyPollen(int a_polyref) { return m_elems[a_polyref]->GetPollen(); };
  PollenNectarQuality SupplyPollen(int a_x, int a_y) { return m_elems[m_land->Get(a_x, a_y)]->GetPollen(); };
  double SupplyTotalPollen(int a_polyref) { return m_elems[a_polyref]->GetTotalPollen(); };
  double SupplyTotalPollen(int a_x, int a_y) { return m_elems[m_land->Get(a_x, a_y)]->GetTotalPollen(); };
  PollenNectarQuality SupplyNectar(int a_polyref) { return m_elems[a_polyref]->GetNectar(); };
  PollenNectarQuality SupplyNectar(int a_x, int a_y) { return m_elems[m_land->Get(a_x, a_y)]->GetNectar(); };
  double SupplyTotalNectar(int a_polyref) { return m_elems[a_polyref]->GetTotalNectar(); };
  double SupplyTotalNectar(int a_x, int a_y) { return m_elems[m_land->Get(a_x, a_y)]->GetTotalNectar(); };

  bool SupplySkScrapes(int a_polyref);

  bool SupplyVegPatchy( int a_polyref );
  bool SupplyVegPatchy( int a_x, int a_y );

  double SupplyGreenBiomass( int a_polyref );
  double SupplyGreenBiomass( int a_x, int a_y );

  double SupplyDeadBiomass( int a_polyref );
  double SupplyDeadBiomass( int a_x, int a_y );

  double SupplyLAGreen( int a_polyref );
  double SupplyLAGreen( int a_x, int a_y );
  double SupplyLATotal( int a_x, int a_y );

  double SupplyVegCover( int a_polyref );
  double SupplyVegCoverVector( unsigned int a_index );
  double SupplyVegCover( int a_x, int a_y );
  TTypesOfVegetation SupplyLastSownVeg(int a_polyref);
  TTypesOfVegetation SupplyLastSownVeg(int a_x, int a_y);
  TTypesOfVegetation SupplyLastSownVegVector(unsigned int a_index);

  double SupplyInsects( int a_polyref );
  double SupplyInsects( int a_x, int a_y );
  /** \brief Removes larval food from a pond and returns true if it was possible, otherwise false */
  bool SubtractPondLarvalFood(double a_food, int a_polyrefindex);
  /** \brief Check if needed and record pesticide application */
  void CheckForPesticideRecord(LE * a_field, TTypesOfPesticideCategory a_pcide);
  /** \brief Gets total rodenticide for a location */
  double SupplyRodenticide(int a_x, int a_y);
  /** \brief Returns true if there is any pesticide in the system at all at this point */
  bool SupplyPesticideDecay(PlantProtectionProducts a_ppp);
  /** \brief Gets total pesticide for a location */
  double SupplyPesticide(int a_x, int a_y, PlantProtectionProducts a_ppp);
  /** \brief Gets the overspray flag */
  bool SupplyOverspray(int a_x, int a_y);
  /** \brief Gets plant pesticide for a location */
  double SupplyPesticideP(int a_x, int a_y, PlantProtectionProducts a_ppp);
  /** \brief Gets soil pesticide for a location */
  double SupplyPesticideS(int a_x, int a_y, PlantProtectionProducts a_ppp);
  /** \brief Gets total pesticide for the centroid of a polygon */
  double SupplyPesticide(int a_polyref, PlantProtectionProducts a_ppp);
  /** \brief Gets plant pesticide for the centroid of a polygon */
  double SupplyPesticideP(int a_polyref, PlantProtectionProducts a_ppp);
  /** \brief Gets soil pesticide for the centroid of a polygon */
  double SupplyPesticideS(int a_polyref, PlantProtectionProducts a_ppp);
  RodenticidePredators_Population_Manager* SupplyRodenticidePredatoryManager() { return m_RodenticidePreds; }
  TTypesOfPesticide SupplyPesticideType( void ) { return m_PesticideType; }
  /** \brief Gets the list of suitable goose foraging fields today */
  GooseFieldList* GetGooseFields(double);
  /** \brief Resets all grain */
  void ResetGrainAndMaize();
  /** \brief Causes openness to be calulated and stored for all polygons */
  void CalculateOpenness(bool a_realcalc);
  /** \brief Stores openness for all polygons to a standard file*/
  void WriteOpenness( void );
  /** \brief Reads openness values from a standard input file for all polygons */
  void ReadOpenness( void );
  /** \brief Provides a measure of the shortest distance in 360 degree, e-g- looking NE % SW before tall obstacles are encountered at both ends. Searches from centroid.*/
  int CalulateFieldOpennessCentroid(int  a_pref);
  /** \brief Provides a measure of the shortest distance in 360 degree, e-g- looking NE % SW before tall obstacles are encountered at both ends. Checks all field 1m2 */
  int CalulateFieldOpennessAllCells(int  a_pref);
  /** \brief Provides a measure of the shortest distance in using a vector from a_cx,a_cy unitl tall obstacles are encountered in both +ve & -ve directions. */
   int LineHighTest(int a_cx, int a_cy, double a_offsetx, double a_offsety);
  /** \brief Get openness for a polygon */
  int SupplyOpenness(int a_poly) { return m_elems[m_polymapping[ a_poly ]]->GetOpenness(); }
  /** \brief Get openness for a location */
  int SupplyOpenness(int a_x, int a_y) { return m_elems[ m_land->Get( a_x, a_y )]->GetOpenness(); }
  /** \brief Tests whether the polygon at a_x,a_y is designated as high */
  bool SupplyLEHigh(int a_x, int a_y) { return m_elems[ m_land->Get( a_x, a_y ) ]->GetHigh(); }
    /** \brief Returns a pointer to a list of polygonrefs to large open fields within a range of location x,y */
  polylist* SupplyLargeOpenFieldsNearXY(int x, int y, int range, int a_openness );

  /** \brief Returns the soil type in ALMaSS types reference numbers */
  int SupplySoilType( int a_x, int a_y ) {
	  return m_elems[ m_land->Get( a_x, a_y ) ]->GetSoilType( );
  }
  /** \brief Returns the soil type in rabbit warren reference numbers */
  int SupplySoilTypeR( int a_x, int a_y ) {
	  return m_elems[ m_land->Get( a_x, a_y ) ]->GetSoilTypeR( );
  }

  APoint SupplyCentroid( int a_polyref );
  APoint SupplyCentroidIndex( int a_polyrefindex );
  int SupplyCentroidX( int a_polyref ) {
	  return m_elems[ m_polymapping[ a_polyref ] ]->GetCentroidX();
  }
  int SupplyCentroidY( int a_polyref ) { return m_elems[m_polymapping[ a_polyref ]]->GetCentroidY(); }
  int SupplyCentroidX( int a_x, int a_y ) { return m_elems[ m_land->Get( a_x, a_y )]->GetCentroidX(); }
  int SupplyCentroidY( int a_x, int a_y ) { return m_elems[ m_land->Get( a_x, a_y )]->GetCentroidY(); }

  int SupplyFarmIntensity( int a_x, int a_y );
  int SupplyFarmIntensity( int a_polyref );
  int SupplyFarmIntensityI( int a_polyindex );

  TTypesOfLandscapeElement SupplyElementType( int a_polyref );
  TTypesOfLandscapeElement SupplyElementType( int a_x, int a_y );
  TTypesOfLandscapeElement SupplyElementTypeCC( int a_x, int a_y );

  int SupplyCountryDesig( int a_x, int a_y );

  int SupplyElementSubType( int a_polyref );
  int SupplyElementSubType( int a_x, int a_y );

  TTypesOfVegetation       SupplyVegType(int a_x, int a_y);
  TTypesOfVegetation       SupplyVegType(int polyref);
  TTypesOfVegetation       SupplyVegTypeVector( unsigned int a_index );
  int  SupplyGrazingPressureVector(unsigned int a_index);
  int  SupplyGrazingPressure(int a_polyref);
  int  SupplyGrazingPressure(int a_x, int a_y);
  bool  SupplyIsCereal(int a_polyref);
  bool  SupplyIsMatureCereal(int a_polyref);
  bool  SupplyIsGrass(int a_polyref);
  bool  SupplyIsGrass2(TTypesOfVegetation a_vege_type);
  bool  SupplyIsCereal2(TTypesOfVegetation a_vege_type);
  bool  SupplyHasTramlines( int a_x, int a_y );
  bool  SupplyHasTramlines( int a_polyref );
  bool  SupplyJustMownVector( unsigned int a_index );
  bool  SupplyJustMown( int a_polyref );
  int   SupplyJustSprayedVector( unsigned int a_index );
  int   SupplyJustSprayed( int a_polyref );
  int   SupplyJustSprayed( int a_x, int a_y );
  int   SupplyTreeAge(int a_Polyref);
  int   SupplyTreeAge(int /* a_x */, int /* a_y */) {return 0;}
  int   SupplyVegAge(int a_Polyref);
  int   SupplyVegAge(int a_x, int a_y);
  int	SupplyNumberOfFarms();
  int   SupplyFarmOwner( int a_x, int a_y );
  int   SupplyFarmOwner( int a_polyref );
  int   SupplyFarmOwnerIndex( int a_x, int a_y );
  int   SupplyFarmOwnerIndex( int a_polyref );

  TTypesOfFarm SupplyFarmType( int a_polyref );
  TTypesOfFarm SupplyFarmType( int a_x, int a_y );
  TTypesOfOptFarms SupplyOptFarmType (int a_x, int a_y); //AM, 270813
  int SupplyFarmArea( int a_polyref );
  double SupplyPolygonAreaVector( int a_polyref );
  double SupplyPolygonArea( int a_polyref ) { return m_elems[ m_polymapping[ a_polyref ]]->GetArea(); }

  /**
  * \brief Sets the grain forage resource as seen from a goose standpoint at a polygon
  */
  void SetBirdSeedForage(int a_polyref, double a_fooddensity)
  {
	  m_elems[m_polymapping[a_polyref]]->SetBirdSeed(a_fooddensity);
  }
  /**
  * \brief Sets the maize forage resource as seen from a goose standpoint at a polygon
  */
  void SetBirdMaizeForage( int a_polyref, double a_fooddensity ) {
	  m_elems[ m_polymapping[ a_polyref ] ]->SetBirdMaize( a_fooddensity );
  }
  /**
  * \brief Returns the leaf forage resource as seen from a goose standpoint at a polygon based on the height only
  */
  double SupplyGooseGrazingForageH( double a_height, GooseSpecies a_goose)
  {
	  /**
	  * \param a_height [in] The vegetation height (assumed grass or cereals). This needs to be checked before calling
	  * \param a_goose [in] Is the type of goose calling which is needed to determine how to assess the value of the current forage availability (ie its different for different types of geese)
	  * \return KJ/min
	  */
	  if (a_goose == gs_Pinkfoot)
	  {
		  return m_GooseIntakeRateVSVegetationHeight_PF->GetY( a_height );
	  }
	  if (a_goose == gs_Barnacle)
	  {
		  if (a_height == 0.0) return 0.0;
		  else return m_GooseIntakeRateVSVegetationHeight_BG->GetY(a_height);
	  }
	  if (a_goose == gs_Greylag)
	  {
		  return m_GooseIntakeRateVSVegetationHeight_GL->GetY(a_height);
	  }
	  Warn("Landscape::SupplyGooseGrazingForage", "Unknown Goose Type");
	  exit(1);
  }

  /**
  * \brief Returns the leaf forage resource as seen from a goose standpoint at a polygon referenced by number based on height only */
  double SupplyGooseGrazingForageH(int a_polygon, GooseSpecies a_goose)
  {
  /**
  * \param a_polygon [in] The polygon refence number for the polygon we are interested in (assumed grass or cereals). This needs to be checked before calling
  * \param a_goose [in] Is the type of goose calling which is needed to determine how to assess the value of the current forage availability (ie its different for different types of geese)
  * \return KJ/min
  */
  if (a_goose == gs_Pinkfoot)
	  {
		  return m_GooseIntakeRateVSVegetationHeight_PF->GetY(m_elems[m_polymapping[a_polygon]]->GetVegHeight());
	  }
	  if (a_goose == gs_Barnacle)
	  {
		  return m_GooseIntakeRateVSVegetationHeight_BG->GetY(m_elems[m_polymapping[a_polygon]]->GetVegHeight());
	  }
	  if (a_goose == gs_Greylag)
	  {
		  return m_GooseIntakeRateVSVegetationHeight_GL->GetY(m_elems[m_polymapping[a_polygon]]->GetVegHeight());
	  }	  	 
	  Warn("Landscape::SupplyGooseGrazingForage", "Unknown Goose Type");
	  exit(1);
  }
  /**
  * \brief Returns the leaf forage resource as seen from a goose standpoint at a polygon referenced by x,y location
  * \param a_x [in] The x-coordinate in a polygon we are interested in (assumed grass or cereals). This needs to be checked before calling
  * \param a_y [in] The x-coordinate in a polygon we are interested in (assumed grass or cereals). This needs to be checked before calling
  * \param a_goose [in] Is the type of goose calling which is needed to determine how to assess the value of the current forage availability (ie its different for different types of geese)
  * \return KJ/min
  */
  double GetActualGooseGrazingForage(int a_x, int a_y, GooseSpecies a_goose)
  {
	  return m_elems[m_land->Get(a_x, a_y)]->GetGooseGrazingForage(a_goose);
  }
  /**
  * \brief Returns the leaf forage resource as seen from a goose standpoint at a polygon referenced by x,y location
  * The amount of food avaiable as grazing resource based on the vegetation height is species specific.
  * \param a_polygon [in] The polygon refence number for the polygon we are interested in (assumed grass or cereals). This needs to be checked before calling
  * \param a_goose [in] Is the type of goose calling which is needed to determine how to assess the value of the current forage availability (ie its different for different types of geese)
  * \return KJ/min
  */
  double GetActualGooseGrazingForage(int a_polygon, GooseSpecies a_goose)
  {
	  return m_elems[m_polymapping[a_polygon]]->GetGooseGrazingForage(a_goose);
  }
  /**
  * \brief Returns the grain forage resource
  */
  double SupplyBirdSeedForage(int a_polyref)
  {
	  return m_elems[m_polymapping[a_polyref]]->GetBirdSeed();
  }
  /**
  * \brief Returns the grain forage resource as seen from a goose standpoint at an x,y location
  */
  double SupplyBirdSeedForage(int a_x, int a_y)
  {
	  return m_elems[m_polymapping[m_land->Get(a_x, a_y)]]->GetBirdSeed();
  }
  /**
  * \brief Returns the maize forage resource
  */
  double SupplyBirdMaizeForage(int a_polyref) {
	  return m_elems[m_polymapping[a_polyref]]->GetBirdMaize();
  }
  /**
  * \brief Returns whether its cereal
  */
  bool SupplyInStubble(int a_polyref) {
	  return m_elems[m_polymapping[a_polyref]]->GetStubble();
  }
  /**
  * \brief Returns the maize forage resource as seen from a goose standpoint at an x,y location
  */
  double SupplyBirdMaizeForage( int a_x, int a_y ) {
	  return m_elems[ m_land->Get( a_x, a_y ) ]->GetBirdMaize();
  }
  /**
  * \brief This records the number of geese on the polygon the day before. To prevent lots of unnecessary clearing of values two values are saved, the number and simulation day - on reading the
  * simulation day number can be used to modify the return value - see GetGooseNumbers()
  */
  void RecordGooseNumbers(int a_poly, int a_number);
  /**
  * \brief This records the number of geese of each species on the polygon the day before. To prevent lots of unnecessary clearing of values two values are saved, the number and simulation day - on reading the
  * simulation day number can be used to modify the return value - see GetGooseNumbers()
  */
  void RecordGooseSpNumbers(int a_poly, int a_number, GooseSpecies a_goose);
  /**
  * \brief This records the number of geese on the polygon the day before at a predefined time. To prevent lots of unnecessary clearing of values two values are saved, the number and simulation day - on reading the
  * simulation day number can be used to modify the return value - see GetGooseNumbers()
  */
  void RecordGooseNumbersTimed(int a_poly, int a_number);
  /**
  * \brief This records the number of geese of each species on the polygon the day before at a predefined time. To prevent lots of unnecessary clearing of values two values are saved, the number and simulation day - on reading the
  * simulation day number can be used to modify the return value - see GetGooseNumbers()
  */
  void RecordGooseSpNumbersTimed(int a_poly, int a_number, GooseSpecies a_goose);
  /**
  * \brief Records the distance to the closest roost of goose species
  */
  void RecordGooseRoostDist(int a_polyref, int a_dist, GooseSpecies a_goose);
  /**
  * \brief Removes grazing forage from a poly per m2
  */
  void GrazeVegetation( int a_poly, double a_forage ) {
	  m_elems[ m_polymapping[ a_poly ] ]->GrazeVegetation( a_forage, true );
  }
  /**
  * \brief Removes grazing forage from a poly and divides this out per m2
  */
  void GrazeVegetationTotal( int a_poly, double a_forage ) {
	  m_elems[ m_polymapping[ a_poly ] ]->GrazeVegetationTotal( a_forage );
  }
  /**
  * \brief This returns the number of geese on the polygon the day before.
  */
  int GetGooseNumbers(int a_poly);
  /**
  * \brief This returns the number of geese which are legal quarry on the polygon the day before.
  */
  int GetQuarryNumbers(int a_poly);
  /**
  * \brief This returns the number of geese on the polygon specifed by a_x, a_y the day before.
  */
  int GetGooseNumbers(int a_x, int a_y);

  int   SupplyLastTreatment( int a_polyref, int *a_index );
  int   SupplyLastTreatment( int a_x, int a_y, int *a_index );
  double GetHareFoodQuality(int a_polygon);

  // Weather data.

  // Energizer Bunny Edition. Use these if at all possible.
  // Returns the requested data for *today* (whatever that is).
  double SupplyGlobalRadiation( );
  double SupplyGlobalRadiation( long a_date );
  double SupplyRain( void );
  double SupplyTemp( void );
  double SupplyHumidity(void);
  double SupplyMeanTemp(long a_date, unsigned int a_period);
  double SupplyWind( void );
  int   SupplyWindDirection( void );
  double  SupplySnowDepth( void );
  bool  SupplySnowcover( void );
  int   SupplyDaylength( void );

  // *Yawn*! These work for all dates as expected, but
  // they are slow, slow, slzzz....
  double SupplyRain( long a_date );
  double SupplyTemp( long a_date );
  double SupplyWind( long a_date );
  double SupplyDayDegrees(int a_polyref);
  double SupplyRainPeriod( long a_date, int a_period );
  double SupplyWindPeriod( long a_date, int a_period );
  double SupplyTempPeriod(long a_date, int a_period);
  //  double SupplyGlobalRadiation( a_date );

  // Warning: Known spooky behaviour, but it works, sort of...
  bool  SupplySnowcover( long a_date );

  // Misc.
  bool IsFieldType(TTypesOfLandscapeElement a_tole) {
	  if ((a_tole == tole_Field)
		  || (a_tole == tole_Orchard)
		  || (a_tole == tole_PermanentSetaside)
		  || (a_tole == tole_PermPasture)
		  || (a_tole == tole_PermPastureLowYield)
		  || (a_tole == tole_PermPastureTussocky)
		  || (a_tole == tole_PermPastureTussockyWet)
		  || (a_tole == tole_Vildtager)
		  || (a_tole == tole_YoungForest)
		  || (a_tole == tole_WoodyEnergyCrop)
		  ) return true;
	  return false;
  }
  int  SupplyPolyRef( int a_x, int a_y );
  int  SupplyPolyRefIndex( int a_x, int a_y );
  int  SupplyPolyRefCC( int a_x, int a_y );
  int  SupplySimAreaWidth( void );
  int  SupplySimAreaHeight( void );
  int  SupplySimAreaMaxExtent( void );
  int  SupplySimAreaMinExtent( void );
  int  SupplyDaylength( long a_date );
  int  SupplyDayInYear( void );
  int  SupplyHour( void );
  int  SupplyMinute( void );
  unsigned int SupplyNumberOfPolygons( void );
  TTypesOfLandscapeElement
    SupplyElementTypeFromVector( unsigned int a_index );
  int   SupplyPolyRefVector( unsigned int a_index );
  int   SupplyPesticideCell(int a_polyref);
  int   SupplyValidX(int a_polyref);
  int   SupplyValidY(int a_polyref);

  /** \brief Get the pesticide concentration per liter from a pond (must be a pond index on calling) */
  double SupplyPondPesticide(int a_poly_index) { return dynamic_cast<Pond*>(m_elems[a_poly_index])->SupplyPondPesticide(); }

  /** \brief Function to prevent wrap around errors with co-ordinates using x/y pair */
  void  CorrectCoords(int &x, int &y);
  /** \brief Function to prevent wrap around errors with co-ordinates using x/y pair */
  APoint  CorrectCoordsPt(int x, int y);
  /** \brief Function to prevent wrap around errors with co-ordinates using a APoint*/
  void  CorrectCoordsPointNoWrap( APoint &a_pt );
  int   CorrectWidth( int x );
  int   CorrectHeight( int y );
  void SetPolyMaxMinExtents( void );
  void CalculateCentroids( void );
  void DumpCentroids( void );
  /** \brief used to calculate whether a building is rural or town - for rodenticide use */
  void BuildingDesignationCalc();
  void CentroidSpiralOut(int a_polyref, int &a_x, int &a_y);
  //void CentroidSpiralOutBlocks(int a_polyref, int &a_x, int &a_y);
  const char* SupplyVersion(void) { return m_versioninfo; }

  // Debugging, warning and configuration methods.
  void DumpPublicSymbols( const char *a_dumpfile,
			  CfgSecureLevel a_level ) {
    g_cfg->DumpPublicSymbols( a_dumpfile, a_level );
  }
  void DumpAllSymbolsAndExit( const char *a_dumpfile ) {
    g_cfg->DumpAllSymbolsAndExit( a_dumpfile );
  }
  bool ReadSymbols( const char *a_cfgfile ) {
    return g_cfg->ReadSymbols( a_cfgfile );
  }
  void  DumpMapInfoByArea( const char *a_filename,
			   bool a_append,
			   bool a_dump_zero_areas,
			   bool a_write_veg_names
			   );

  void  Warn(  std::string a_msg1, std::string a_msg2 );

  // This is really, really naughty, but for the sake of efficiency...
  // SupplyMagicMapP( int a_x, int a_y ) returns a pointer to the map
  // at position x, y. The value found through it is an internal magic
  // number from the landscape simulator, that uniquely identifies a
  // polygon. It is however not the polygon number. To find that one
  // must convert the magic number via
  // MagicMapP2PolyRef( int a_magic ).
  //
  int*  SupplyMagicMapP( int a_x, int a_y );
  int   MagicMapP2PolyRef( int a_magic );

// For the Roe Deer
  int   SupplyRoadWidth(int,int) {return 0;}
  double SupplyTrafficLoad( int a_x, int a_y );
  double SupplyTrafficLoad( int a_polyref );
  int   SupplyTreeHeight(int,int) {return 0;}
  int   SupplyUnderGrowthWidth(int,int) {return 0;}
  int   SupplyTreeHeight(int /* polyref */ ) {return 0;}
  int   SupplyUnderGrowthWidth(int /* polyref */ ) {return 0;}

// For the debugging programmer.
  long SupplyGlobalDate( void );
  int  SupplyYear( void );
  int  SupplyYearNumber( void );
  int  SupplyMonth( void );
  int  SupplyDayInMonth( void );
  double SupplyDaylightProp() { return g_date->GetDaylightProportion(); }
  double SupplyNightProp() { return 1.0-g_date->GetDaylightProportion(); }

// General use. Introduced 1/12-2003.
// Reset internal loop counter.
  void      SupplyLEReset( void );
// Returns -1 at end-of-loop, polygon ref otherwise.
  int       SupplyLENext( void );
  int       SupplyLECount( void );
  LE_Signal SupplyLESignal( int a_polyref );
  void      SetLESignal( int a_polyref, LE_Signal a_signal );

  void IncTreatCounter( int a_treat );

  TTypesOfLandscapeElement TranslateEleTypes(int EleReference);
  TTypesOfVegetation       TranslateVegTypes(int VegReference);
  LE* SupplyLEPointer( int a_polyref );

  int BackTranslateEleTypes(TTypesOfLandscapeElement EleReference);
  int BackTranslateVegTypes(TTypesOfVegetation VegReference);

  std::string EventtypeToString( int a_event );
  std::string PolytypeToString( TTypesOfLandscapeElement a_le_type );
  std::string VegtypeToString( TTypesOfVegetation a_veg );


  /* *********************** Bee Helper Methods *********************** */
  /** \brief Read in the Osmia nest density files and allocate to each LE object */
  void InitOsmiaBeeNesting();

  /** \brief Tesll all LE objects to update their osmia nest status */
  void UpdateOsmiaNesting() {
	  for (unsigned int s = 0; s < m_elems.size(); s++) m_elems[s]->UpdateOsmiaNesting();
  }

  /** \brief Find out whether an osmia nest can be made here */
  bool SupplyOsmiaNest(int a_x, int a_y) {
	  return m_elems[m_land->Get(a_x, a_y)]->GetOsmiaNest();
  }

  /** \brief Reopen the osmia nest here  */
  void IncOsmiaNest(int a_x, int a_y) {
	  m_elems[m_land->Get(a_x, a_y)]->IncOsmiaNesting();
  }

  /** \brief Reopen the osmia nest here  */
  void ReleaseOsmiaNest(int a_x, int a_y) {
	  m_elems[m_land->Get(a_x, a_y)]->ReleaseOsmiaNest();
  }
  /* ********************* End Bee Helper Methods ********************* */
  // /** \brief Reset all polygons natural grazing level to zero */
 // void ResetAllVoleGrazing( void )
 // {
	//  for ( unsigned int i=0; i<m_elems.size(); i++ ) m_elems[i]->ResetVoleGrazing();
 // }
 // /** \brief Inc volegrazing at x,y */
 // void IncVoleGrazing(int a_x, int a_y) { m_elems[ m_land->Get( a_x, a_y ) ]->AddVoleGrazing(1); }
 // /** \brief Get volegrazing at x,y */
 // double SupplyVoleGrazingDensityVector(unsigned int a_index) { return m_elems[ a_index ]->GetVoleGrazingDensity(); }
 // /** \brief Calculate all vole grazing densities */
 // void CalcAllVoleGrazingDensity()
 // {
	//for ( unsigned int i=0; i<m_elems.size(); i++ ) { m_elems[i]->CalcVoleGrazingDensity(); }
 // }

protected:
/*
    **CJT** 19/07/2018 these appear not to be used
    void MakeCluster(void);
	void ReadInput(int* , int* , int* , APoint*);
	APoint RandomLocation(void);
	APoint GetNextSeed(int);
	int AddToClusterList(int* , int* , int*, int*, int*);
	void ModifyPolyRef(int* );
*/
};


extern Landscape *g_map;

inline void  Landscape::TurnTheWorld( void )
{
  Tick();
}

// We are interested in *speed*. Inlining all the lookup functions
// here and in the associated classes will cost us very little
// but a few KB of memory.

inline double Landscape::SupplyVegDigestabilityVector(unsigned int a_index )
{
  return m_elems[ a_index ]->GetDigestability();
}

inline double Landscape::SupplyVegDigestability( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetDigestability();
}


inline double Landscape::SupplyVegDigestability( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetDigestability();
}


inline double Landscape::SupplyVegHeightVector( unsigned int a_index )
{
  return m_elems[ a_index ]->GetVegHeight();
}

inline double Landscape::SupplyVegHeight( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetVegHeight();
}

inline double Landscape::SupplyVegHeight( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetVegHeight();
}



inline double Landscape::SupplyVegBiomassVector( unsigned int a_index )
{
  return m_elems[ a_index ]->GetVegBiomass();
}

inline double Landscape::SupplyVegBiomass( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetVegBiomass();
}

inline double Landscape::SupplyVegBiomass( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetVegBiomass();
}


inline double Landscape::SupplyWeedBiomass( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetWeedBiomass();
}


inline double Landscape::SupplyWeedBiomass( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetWeedBiomass();
}

inline int Landscape::SupplyVegDensity( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetVegDensity();
}


inline bool Landscape::SupplySkScrapes( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetSkScrapes();
}

inline bool Landscape::SupplyVegPatchy( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetVegPatchy();
}


inline bool Landscape::SupplyVegPatchy( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetVegPatchy();
}


inline int Landscape::SupplyVegDensity( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetVegDensity();
}


inline double Landscape::SupplyLATotal( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetLATotal();
}


inline double Landscape::SupplyLAGreen( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetLAGreen();
}



inline double Landscape::SupplyGreenBiomass( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetGreenBiomass();
}

inline double Landscape::SupplyGreenBiomass( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetGreenBiomass();
}



inline double Landscape::SupplyDeadBiomass( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetDeadBiomass();
}

inline double Landscape::SupplyDeadBiomass( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetDeadBiomass();
}



inline double Landscape::SupplyLAGreen( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetLAGreen();
}



inline double Landscape::SupplyVegCover( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetVegCover();
}

inline double Landscape::SupplyVegCoverVector( unsigned int a_index )
{
  return m_elems[ a_index ]->GetVegCover();
}

inline double Landscape::SupplyVegCover( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetVegCover();
}

inline TTypesOfVegetation Landscape::SupplyLastSownVegVector(unsigned int a_index)
{
	return m_elems[a_index]->GetLastSownVeg();
}

inline TTypesOfVegetation Landscape::SupplyLastSownVeg(int a_polyref)
{
	return m_elems[m_polymapping[a_polyref]]->GetLastSownVeg();
}

inline TTypesOfVegetation Landscape::SupplyLastSownVeg(int a_x, int a_y)
{
	return m_elems[m_land->Get(a_x, a_y)]->GetLastSownVeg();
}

inline int Landscape::SupplyVegAge(int a_polyref)
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetVegAge();
}

inline int Landscape::SupplyVegAge( int a_x, int a_y )
{
	return m_elems[m_land->Get(a_x, a_y)]->GetVegAge();
}

inline double Landscape::SupplyInsects( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetInsectPop();
}

inline double Landscape::SupplyInsects( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetInsectPop();
}


inline LE* Landscape::SupplyLEPointer( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]];
}

inline TTypesOfLandscapeElement Landscape::SupplyElementTypeFromVector( unsigned int a_index )
{
  return m_elems[ a_index ]->GetElementType();
}


inline TTypesOfLandscapeElement Landscape::SupplyElementType( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetElementType();
}

inline TTypesOfLandscapeElement Landscape::SupplyElementType( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetElementType();
}


inline int Landscape::SupplyElementSubType( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetSubType();
}

inline int Landscape::SupplyElementSubType( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetSubType();
}


inline int Landscape::SupplyCountryDesig( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetCountryDesignation();
}


inline TTypesOfLandscapeElement
  Landscape::SupplyElementTypeCC( int a_x, int a_y )
{
  a_x = (a_x + m_width10) % m_width;
  a_y = (a_y + m_height10) % m_height;
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetElementType();
}



inline int Landscape::SupplyNumberOfFarms( ) {
	return m_FarmManager->GetNoFarms();
}

inline int Landscape::SupplyFarmOwner( int a_x, int a_y ) {
	return m_elems[ m_land->Get( a_x, a_y ) ]->GetOwnerFile();
}



inline int Landscape::SupplyFarmOwner( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetOwnerFile();
}



inline int Landscape::SupplyFarmOwnerIndex( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetOwnerIndex();
}



inline int Landscape::SupplyFarmOwnerIndex( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetOwnerIndex();
}



inline TTypesOfFarm Landscape::SupplyFarmType( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetOwner()->GetType();
}



inline TTypesOfFarm Landscape::SupplyFarmType( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetOwner()->GetType();
}

inline TTypesOfOptFarms Landscape::SupplyOptFarmType (int a_x, int a_y)
{
	OptimisingFarm * opf;		
	opf =  dynamic_cast<OptimisingFarm*>(m_elems[ m_land->Get( a_x, a_y ) ]->GetOwner());
	return opf-> Get_farmType();
}


/** \brief Returns the area of a polygon using the vector index as a reference */
inline double Landscape::SupplyPolygonAreaVector( int a_polyref )
{
  return m_elems[ a_polyref ]->GetArea();
}


inline int Landscape::SupplyFarmArea( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetOwner()->GetArea();
}



inline TTypesOfVegetation
  Landscape::SupplyVegType( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ] ]->GetVegType();
}

inline TTypesOfVegetation
  Landscape::SupplyVegTypeVector( unsigned int a_index )
{
  return m_elems[ a_index ]->GetVegType();
}



inline int Landscape::SupplyGrazingPressure( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ] ]->GetCattleGrazing();
}

inline int Landscape::SupplyGrazingPressureVector( unsigned int a_index )
{
  return m_elems[ a_index ]->GetCattleGrazing();
}



inline int Landscape::SupplyGrazingPressure( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetCattleGrazing();
}



inline bool Landscape::SupplyHasTramlines( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ] ]->HasTramlines();
}


inline bool Landscape::SupplyHasTramlines( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->HasTramlines();
}


inline bool Landscape::SupplyJustMownVector( unsigned int a_index )
{
  return m_elems[ a_index ]->IsRecentlyMown();
}

inline bool Landscape::SupplyJustMown( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ] ]->IsRecentlyMown();
}



inline int Landscape::SupplyJustSprayedVector( unsigned int a_index )
{
  return m_elems[ a_index ]->IsRecentlySprayed();
}

inline int Landscape::SupplyJustSprayed( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ] ]->IsRecentlySprayed();
}

inline int Landscape::SupplyJustSprayed( int a_x, int a_y )
{
  return m_elems[  m_land->Get( a_x, a_y ) ]->IsRecentlySprayed();
}



inline double Landscape::SupplyTrafficLoad( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ] ]->GetTrafficLoad();
}


inline double Landscape::SupplyTrafficLoad( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetTrafficLoad();
}



inline bool Landscape::SupplyIsCereal(int a_polyref)
{
	return m_elems[m_polymapping[a_polyref]]->IsCereal();
}

inline bool Landscape::SupplyIsMatureCereal(int a_polyref)
{
	return m_elems[m_polymapping[a_polyref]]->IsMatureCereal();
}

inline bool Landscape::SupplyIsGrass(int a_polyref)
{
  return m_elems[ m_polymapping[ a_polyref ] ]->IsGrass();
}

inline int Landscape::SupplyTreeAge( int /* a_polyref */ )
{
  return 1;
}

inline TTypesOfVegetation
  Landscape::SupplyVegType( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetVegType();
}

inline double Landscape::SupplyDayDegrees( int a_polyref )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetDayDegrees();
}

inline int   Landscape::SupplyLastTreatment( int a_polyref, int *a_index )
{
  return m_elems[ m_polymapping[ a_polyref ]]->GetLastTreatment( a_index );
}

inline int   Landscape::SupplyLastTreatment( int a_x, int a_y, int *a_index )
{
  return m_elems[ m_land->Get( a_x, a_y ) ]->GetLastTreatment( a_index );
}

inline double Landscape::SupplyGlobalRadiation()
{
  return g_weather->GetGlobalRadiation( );
}

inline double Landscape::SupplyGlobalRadiation( long a_date )
{
  return g_weather->GetGlobalRadiation( a_date );
}

inline double Landscape::SupplyRainPeriod( long a_date, int a_period )
{
  return g_weather->GetRainPeriod( a_date, a_period );
}



inline double Landscape::SupplyRain( long a_date )
{
  return g_weather->GetRain( a_date );
}



inline double Landscape::SupplyRain( void )
{
  return g_weather->GetRain();
}



inline double Landscape::SupplyMeanTemp( long a_date, unsigned int a_period )
{
  return g_weather->GetMeanTemp( a_date, a_period );
}



inline double Landscape::SupplyTemp( long a_date )
{
  return g_weather->GetTemp( a_date );
}



inline double Landscape::SupplyTemp(void)
{
	return g_weather->GetTemp();
}



inline double Landscape::SupplyHumidity(void)
{
	return g_weather->GetHumidity();
}



inline double Landscape::SupplyWind( long a_date )
{
  return g_weather->GetWind( a_date );
}

inline double Landscape::SupplyWindPeriod( long a_date, int a_period )
{
  return g_weather->GetWindPeriod( a_date, a_period );
}

inline double Landscape::SupplyTempPeriod( long a_date, int a_period )
{
  return g_weather->GetTempPeriod( a_date, a_period );
}



inline double Landscape::SupplyWind( void )
{
  return g_weather->GetWind();
}


inline int Landscape::SupplyWindDirection( void )
{
	return g_weather->GetWindDirection();
}



inline bool  Landscape::SupplySnowcover( long a_date )
{
  return g_weather->GetSnow( a_date );
}



inline bool  Landscape::SupplySnowcover( void )
{
  return g_weather->GetSnow();
}



inline double  Landscape::SupplySnowDepth( void )
{
  return g_weather->GetSnowDepth();
}



inline int Landscape::SupplyPolyRefVector( unsigned int a_index )
{
  return m_elems[ a_index ]->GetPoly();
}





inline unsigned int
  Landscape::SupplyNumberOfPolygons( void )
{
  return (unsigned int) m_elems.size();
}



inline int Landscape::SupplyPesticideCell(int a_polyref)
{
  return m_elems[ m_polymapping[ a_polyref ] ]->GetPesticideCell();
}


inline int Landscape::SupplyValidX(int a_polyref)
{
  return m_elems[ m_polymapping[ a_polyref ] ]->GetValidX();
}



inline int Landscape::SupplyValidY(int a_polyref)
{
  return m_elems[ m_polymapping[ a_polyref ] ]->GetValidY();
}



inline int   Landscape::SupplyPolyRef( int a_x, int a_y )
{
  return m_elems[ m_land->Get( a_x, a_y )]->GetPoly();
}

inline int   Landscape::SupplyPolyRefIndex( int a_x, int a_y )
{
  return m_land->Get( a_x, a_y );
}



inline int   Landscape::SupplyPolyRefCC( int a_x, int a_y )
{
  a_x = (a_x + m_width10) % m_width;
  a_y = (a_y + m_height10) % m_height;
  return m_elems[ m_land->Get( a_x, a_y )]->GetPoly();
}



inline int*  Landscape::SupplyMagicMapP( int a_x, int a_y )
{
  return m_land->GetMagicP( a_x, a_y );
}



inline int   Landscape::MagicMapP2PolyRef( int a_magic )
{
  return m_elems[ a_magic ]->GetPoly();
}



inline int   Landscape::SupplyDaylength( long a_date )
{
  return g_date->DayLength( a_date );
}



inline int   Landscape::SupplyDaylength( void )
{
  return g_date->DayLength();
}

inline void Landscape::CorrectCoords(int &x, int &y)
{
	/**
	* m_width10 & m_height10 are used to avoid problems with co-ordinate values that are very large. Problems will only occur if coords passed are >10x the world width or height.
	*/
	x = (m_width10 + x) % m_width;
	y = (m_height10 + y) % m_height;
}

inline APoint Landscape::CorrectCoordsPt(int x, int y)
{
	/**
	* m_width10 & m_height10 are used to avoid problems with co-ordinate values that are very large. Problems will only occur if coords passed are >10x the world width or height.
	*/
	APoint pt;
	pt.m_x = (m_width10 + x) % m_width;
	pt.m_y = (m_height10 + y) % m_height;
	return pt;
}

inline void Landscape::CorrectCoordsPointNoWrap(APoint & a_pt)
{
	/**
	* This just cuts off extremes of coordinate values so that the point stays in landscape. Can't use a modulus or we get wrap around, and in this case we don't want that
	*/
	if (a_pt.m_x >= m_width) a_pt.m_x = m_width - 1;
	if (a_pt.m_y >= m_height) a_pt.m_y = m_height - 1;
	if (a_pt.m_x < 0) a_pt.m_x = 0;
	if (a_pt.m_y < 0) a_pt.m_y = 0;
}


inline int Landscape::CorrectWidth( int x )
{
   return (m_width10+x)%m_width;
}


inline int Landscape::CorrectHeight( int y )
{
  return (m_height10+y)%m_height;
}


inline void  Landscape::Warn( std::string a_msg1,  std::string a_msg2 )
{
  g_msg->Warn( WARN_MSG, a_msg1, a_msg2 );
}
/** 
\brief Get the hour of the day
*/
inline int Landscape::SupplyHour( void ) {
	return g_date->GetHour();
}
/**
\brief Get the minute of the hour
*/
inline int Landscape::SupplyMinute( void ) {
	return g_date->GetMinute();
}

inline int  Landscape::SupplyDayInYear( void )
{
  return g_date->DayInYear();
}

inline int Landscape::SupplyMonth( void )
{
  return g_date->GetMonth();
}

inline int Landscape::SupplyDayInMonth( void )
{
  return g_date->GetDayInMonth();
}

inline int Landscape::SupplyYear( void )
{
	return g_date->GetYear();
}

inline int Landscape::SupplyYearNumber( void )
{
	return g_date->GetYearNumber();
}

inline long Landscape::SupplyGlobalDate( void )
{
	return g_date->Date();
}

// Outdated.
//inline int   Landscape::SupplyMapSize( void )
//{
//  return m_land->Size();
//}

inline int   Landscape::SupplySimAreaWidth( void )
{
  return m_width;
}

inline int   Landscape::SupplySimAreaHeight( void )
{
  return m_height;
}

inline int   Landscape::SupplySimAreaMaxExtent( void )
{
  return m_maxextent;
}

inline TTypesOfLandscapeElement
  Landscape::TranslateEleTypes(int EleReference)
{
  return g_letype->TranslateEleTypes( EleReference );
}


inline TTypesOfVegetation
  Landscape::TranslateVegTypes(int VegReference)
{
  return g_letype->TranslateVegTypes( VegReference );
}


inline int
  Landscape::BackTranslateEleTypes(TTypesOfLandscapeElement EleReference)
{
  return g_letype->BackTranslateEleTypes( EleReference );
}


inline int
  Landscape::BackTranslateVegTypes(TTypesOfVegetation VegReference)
{
  return g_letype->BackTranslateVegTypes( VegReference );
}
#endif // LANDSCAPE_H

