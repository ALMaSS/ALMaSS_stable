/**
\file
\brief
<B>Farm.cpp This file contains the source for the Farm class and OptimisingFarm</B> \n
*/
/**
\file
 by Frank Nikolaisen & Chris J. Topping \n
 Version of June 2003 \n
 All rights reserved. \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//
// farm.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

//
#define _CRT_SECURE_NO_DEPRECATE

#include <string.h>
#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include "../BatchALMaSS/BoostRandomGenerators.h"
#include "../ALMaSSDefines.h"
#include <math.h> //for optimisation
#include <time.h> //for dec. making


#define _CRTDBG_MAP_ALLOC

using namespace std;

#define _CRT_SECURE_NO_DEPRECATE
#include <random> //for dec. making - draw from a dist.
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../Landscape/ls.h"
#include "../Landscape/cropprogs/BroadBeans.h"
#include "../Landscape/cropprogs/Carrots.h"
#include "../Landscape/cropprogs/CloverGrassGrazed1.h"
#include "../Landscape/cropprogs/CloverGrassGrazed2.h"
#include "../Landscape/cropprogs/FieldPeas.h"
#include "../Landscape/cropprogs/FieldPeasSilage.h"
#include "../Landscape/cropprogs/Fodderbeet.h"
#include "../Landscape/cropprogs/Sugarbeet.h"
#include "../Landscape/cropprogs/FodderGrass.h"
#include "../Landscape/cropprogs/Maize.h"
#include "../Landscape/cropprogs/MaizeSilage.h"
#include "../Landscape/cropprogs/OFodderbeet.h"
#include "../Landscape/cropprogs/OMaizeSilage.h"
#include "../Landscape/cropprogs/OBarleyPeaCloverGrass.h"
#include "../Landscape/cropprogs/OSBarleySilage.h"
#include "../Landscape/cropprogs/OCarrots.h"
#include "../Landscape/cropprogs/OCloverGrassGrazed1.h"
#include "../Landscape/cropprogs/OCloverGrassGrazed2.h"
#include "../Landscape/cropprogs/OCloverGrassSilage1.h"
#include "../Landscape/cropprogs/OFirstYearDanger.h"
#include "../Landscape/cropprogs/OGrazingPigs.h"
#include "../Landscape/cropprogs/OPermanentGrassGrazed.h"
#include "../Landscape/cropprogs/OSeedGrass1.h"
#include "../Landscape/cropprogs/OSeedGrass2.h"
#include "../Landscape/cropprogs/OSpringBarleyPigs.h"
#include "../Landscape/cropprogs/OFieldPeas.h"
#include "../Landscape/cropprogs/OFieldPeasSilage.h"
#include "../Landscape/cropprogs/OrchardCrop.h"
#include "../Landscape/cropprogs/OOats.h"
#include "../Landscape/cropprogs/Oats.h"
#include "../Landscape/cropprogs/OPotatoes.h"
#include "../Landscape/cropprogs/OSpringBarley.h"
#include "../Landscape/cropprogs/OSpringBarleyExt.h"
#include "../Landscape/cropprogs/OWinterBarley.h"
#include "../Landscape/cropprogs/OWinterBarleyExt.h"
#include "../Landscape/cropprogs/OWinterRape.h"
#include "../Landscape/cropprogs/OWinterRye.h"
#include "../Landscape/cropprogs/OWinterWheat.h"
#include "../Landscape/cropprogs/OWinterWheatUndersown.h"
#include "../Landscape/cropprogs/OWinterWheatUndersownExt.h"
#include "../Landscape/cropprogs/PermanentGrassGrazed.h"
#include "../Landscape/cropprogs/PermanentGrassLowYield.h"
#include "../Landscape/cropprogs/PermanentGrassTussocky.h"
#include "../Landscape/cropprogs/PermanentSetAside.h"
#include "../Landscape/cropprogs/Potatoes.h"
#include "../Landscape/cropprogs/PotatoesIndustry.h"
#include "../Landscape/cropprogs/seedgrass1.h"
#include "../Landscape/cropprogs/seedgrass2.h"
#include "../Landscape/cropprogs/setaside.h"
#include "../Landscape/cropprogs/SpringBarley.h"
#include "../Landscape/cropprogs/SpringBarleySpr.h"
#include "../Landscape/cropprogs/SpringBarleyPTreatment.h"
#include "../Landscape/cropprogs/SpringBarleySKManagement.h"
#include "../Landscape/cropprogs/SpringBarleyCloverGrass.h"
#include "../Landscape/cropprogs/SpringBarleySeed.h"
#include "../Landscape/cropprogs/SpringBarleySilage.h"
#include "../Landscape/cropprogs/SpringRape.h"
#include "../Landscape/cropprogs/Triticale.h"
#include "../Landscape/cropprogs/OTriticale.h"
#include "../Landscape/cropprogs/WinterBarley.h"
#include "../Landscape/cropprogs/winterrape.h"
#include "../Landscape/cropprogs/WinterRye.h"
#include "../Landscape/cropprogs/WinterWheat.h"
#include "../Landscape/cropprogs/WWheatPControl.h"
#include "../Landscape/cropprogs/WWheatPTreatment.h"
#include "../Landscape/cropprogs/WWheatPToxicControl.h"
#include "../Landscape/cropprogs/AgroChemIndustryCereal.h"

#include "../Landscape/cropprogs/WinterWheatStrigling.h"
#include "../Landscape/cropprogs/WinterWheatStriglingCulm.h"
#include "../Landscape/cropprogs/WinterWheatStriglingSingle.h"
#include "../Landscape/cropprogs/SpringBarleyCloverGrassStrigling.h"
#include "../Landscape/cropprogs/SpringBarleyStrigling.h"
#include "../Landscape/cropprogs/SpringBarleyStriglingCulm.h"
#include "../Landscape/cropprogs/SpringBarleyStriglingSingle.h"
#include "../Landscape/cropprogs/MaizeStrigling.h"
#include "../Landscape/cropprogs/winterrapestrigling.h"
#include "../Landscape/cropprogs/WinterRyeStrigling.h"
#include "../Landscape/cropprogs/WinterBarleyStrigling.h"
#include "../Landscape/cropprogs/FieldPeasStrigling.h"
#include "../Landscape/cropprogs/SpringBarleyPeaCloverGrassStrigling.h"
#include "../Landscape/cropprogs/YoungForest.h"
#include "../Landscape/cropprogs/NorwegianPotatoes.h"
#include "../Landscape/cropprogs/NorwegianOats.h"
#include "../Landscape/cropprogs/NorwegianSpringBarley.h"

#include "../Landscape/cropprogs/PLWinterWheat.h"
#include "../Landscape/cropprogs/PLWinterRape.h"
#include "../Landscape/cropprogs/PLWinterBarley.h"
#include "../Landscape/cropprogs/PLWinterTriticale.h"
#include "../Landscape/cropprogs/PLWinterRye.h"
#include "../Landscape/cropprogs/PLSpringWheat.h"
#include "../Landscape/cropprogs/PLSpringBarley.h"
#include "../Landscape/cropprogs/PLMaize.h"
#include "../Landscape/cropprogs/PLMaizeSilage.h"
#include "../Landscape/cropprogs/PLPotatoes.h"
#include "../Landscape/cropprogs/PLBeet.h"
#include "../Landscape/cropprogs/PLFodderLucerne1.h"
#include "../Landscape/cropprogs/PLFodderLucerne2.h"
#include "../Landscape/cropprogs/PLCarrots.h"
#include "../Landscape/cropprogs/PLSpringBarleySpr.h"
#include "../Landscape/cropprogs/PLWinterWheatLate.h"
#include "../Landscape/cropprogs/PLBeetSpr.h"
#include "../Landscape/cropprogs/PLBeans.h"

#include "../Landscape/cropprogs/NLBeet.h"
#include "../Landscape/cropprogs/NLCarrots.h"
#include "../Landscape/cropprogs/NLMaize.h"
#include "../Landscape/cropprogs/NLPotatoes.h"
#include "../Landscape/cropprogs/NLSpringBarley.h"
#include "../Landscape/cropprogs/NLWinterWheat.h"
#include "../Landscape/cropprogs/NLCabbage.h"
#include "../Landscape/cropprogs/NLTulips.h"
#include "../Landscape/cropprogs/NLGrassGrazed1.h"
#include "../Landscape/cropprogs/NLGrassGrazed1Spring.h"
#include "../Landscape/cropprogs/NLGrassGrazed2.h"
#include "../Landscape/cropprogs/NLGrassGrazedLast.h"
#include "../Landscape/cropprogs/NLPermanentGrassGrazed.h"
#include "../Landscape/cropprogs/NLBeetSpring.h"
#include "../Landscape/cropprogs/NLCarrotsSpring.h"
#include "../Landscape/cropprogs/NLMaizeSpring.h"
#include "../Landscape/cropprogs/NLPotatoesSpring.h"
#include "../Landscape/cropprogs/NLSpringBarleySpring.h"
#include "../Landscape/cropprogs/NLCabbageSpring.h"
#include "../Landscape/cropprogs/NLCatchPeaCrop.h"

#include "../Landscape/cropprogs/DummyCropPestTesting.h"


#include "../Landscape/map_cfg.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;

extern Landscape * g_landscape_p;

CfgBool cfg_organic_extensive( "FARM_ORGANIC_EXTENSIVE", CFG_CUSTOM, false );
// Enable this for reading the farm reference file instead of the
// automated (not at all random) farm generation. Should not really
// be set to false, ever.
static CfgBool l_map_read_farmfile( "MAP_READ_FARMFILE", CFG_PRIVATE, true );
static CfgStr l_map_farmref_file( "MAP_FARMREF_FILE", CFG_CUSTOM, "farmrefs.txt" );

static CfgStr l_emaize_price_file( "EMAIZE_PRICE_FILE", CFG_CUSTOM, "EM_price.txt" );

/** \brief If set to true, the farmer decision making model is active.*/
CfgBool cfg_OptimisingFarms("OPTIMISING_FARMS",CFG_CUSTOM, false);
/** \brief If set to true, the original farm optimisation model's crop set is used in the farmer decision making model
In this mode the optimisation is carried out only once; it is used for the comparioson of the optimisaiton results with the original model.*/
CfgBool cfg_OptimiseBedriftsmodelCrops("OPTIMISE_BEDRIFTSMODEL_CROPS",CFG_CUSTOM, false); //12.03.13, set it to true to run the optimisation like in the Bedriftsmodel (with the same crops)
/** \brief If set to true, an output file with farm areas is produced.*/
CfgBool cfg_DumpFarmAreas ("DUMP_FARM_AREAS", CFG_CUSTOM, false);
/** \brief If set to true, the farm areas from the original farm optimisation model are used in the optimisation. If set to false,
the farm areas based on a map from 2005 are used.*/
CfgBool cfg_UseBedriftsmodelFarmAreas ("USE_BEDRIFTSMODEL_FARM_AREAS", CFG_CUSTOM, false);
/** \brief If set to yes, the only decision mode/startegy the farmers can use is deliberation (i.e. simplified optimisation).*/
CfgBool cfg_OnlyDeliberation ("ONLY_DELIBERATION", CFG_CUSTOM, true); //041213 if true, farmers will always choose to deliberate (see function ChooseDecisionMode)

/** \brief A parameter setting the maximum distance from a farm to another farm that can be considred a neighbour [km].*/
CfgFloat cfg_Neighbor_dist ("NEIGHBOR_DIST",CFG_CUSTOM,1.5);
/** \brief A parameter setting the minimum certainty level.*/
CfgFloat cfg_Min_certainty("MIN_CERTAINTY",CFG_CUSTOM,0);
/** \brief A parameter setting the minimum satisfaction level for profit.*/
CfgFloat cfg_Min_need_satisfaction1 ("MIN_NEED_SATISFACTION_ONE",CFG_CUSTOM, 100); //profit, %
/** \brief A parameter setting the minimum satisfaction level for yield.*/
CfgFloat cfg_Min_need_satisfaction2 ("MIN_NEED_SATISFACTION_TWO",CFG_CUSTOM, 100); //yield, %
/** \brief A parameter setting the proportion of farmers of a type profit maximiser.*/
CfgFloat cfg_Profit_max_proportion ("PROFIT_MAX_PROPORTION",CFG_CUSTOM, 100);//proportion in the population
/** \brief A parameter setting the proportion of farmers of a type yield maximiser.*/
CfgFloat cfg_Yield_max_proportion ("YIELD_MAX_PROPORTION",CFG_CUSTOM, 0);
/** \brief A parameter setting the proportion of farmers of a type environmentalist.*/
CfgFloat cfg_Environmentalist_proportion ("ENVIRONMENTALIST_PROPORTION",CFG_CUSTOM, 0);
/** \brief A parameter of the yield maximizer farmer type: it increases the chance of necessity of carrying out a pesticide spraying event
(i.e. the probability of a pest attack). Used to represent the yield maximiser's higher tendency to spray pesticide to protect crops
even at the expense of a profit.*/
CfgFloat cfg_Yield_max_pest_prob_multiplier ("YIELD_MAX_PEST_PROB_MULITPLIER",CFG_CUSTOM, 1.5);
/** \brief A parameter of the environmentalist farmer type: increases the chance that environmentalist does not find using
pesticides profitable. Used to represent environmentalist's reluctance to use pesticides.*/
CfgFloat cfg_Env_pest_multiplier ("ENV_PEST_MULTIPLIER",CFG_CUSTOM, 1.25); //>1
/** \brief A parameter of the environmentalist farmer type: reduces environmentalist's use of fertiliser. Used to represent
environmentalist's tendency to minimise the usage of fertlizers.*/
CfgFloat cfg_Env_fert_multiplier ("ENV_FERT_MULTIPLIER",CFG_CUSTOM, 0.8);//<1
CfgBool cfg_Sensitivity_analysis ("SENSITIVITY_ANALYSIS", CFG_CUSTOM, false);
/** \brief If set to true, crops are assigned area based on their gross margin proportion in the total GM for all crops with non-negative GM.*/
CfgBool cfg_Areas_Based_on_Distribution ("AREAS_BASED_ON_DISTRIBUTION", CFG_CUSTOM, false);
/** \brief If set to true, the energy maize crop is included in the simulation.*/
CfgBool cfg_MaizeEnergy ("MAIZE_ENERGY", CFG_CUSTOM, false);

/** \brief This parameter specifies the proportion of average number of animals on a farm for previous 3 years, below which a farmer decides not to grow energy maize anymore.*/
 CfgFloat cfg_AnimalsThreshold ("ANIMALS_THRESHOLD",CFG_CUSTOM, 0);
/** \brief This parameter specifies the relative difference in energy maize price which causes a farmer to deliberate.*/
 CfgFloat cfg_PriceChangeThreshold ("PRICE_CHANGE_THRESHOLD",CFG_CUSTOM, 0.2);
/** \brief This parameter specifies the life stage of a species whose numbers farmers use during their decision making.*/
 CfgInt cfg_LifeStage ("LIFE_STAGE", CFG_CUSTOM, 0);
 /** \brief This parameter specifies the day at which farmers observe the number of animals residing at their farm.*/
 CfgInt cfg_Animals_number_test_day ("ANIMALS_NUMBER_TEST_DAY", CFG_CUSTOM, 152); 

/** \brief Price of a fodder unit. [DKK/FU]*/
CfgFloat cfg_Price_FU ("PRICE_FU",CFG_CUSTOM, 1.157);
/** \brief Price of fertilizer. [DKK/kg]*/
CfgFloat cfg_Price_Nt ("PRICE_NT",CFG_CUSTOM, 1.93);
CfgFloat cfg_Price_SBarley ("PRICE_SBARLEY",CFG_CUSTOM, 83);
CfgFloat cfg_Price_Oats ("PRICE_OATS",CFG_CUSTOM, 75);
CfgFloat cfg_Price_WBarley ("PRICE_WBARLEY",CFG_CUSTOM, 93);
CfgFloat cfg_Price_WWheat ("PRICE_WWHEAT",CFG_CUSTOM, 94);
CfgFloat cfg_Price_Triticale ("PRICE_TRITICALE",CFG_CUSTOM, 80);
CfgFloat cfg_Price_WRape ("PRICE_WRAPE",CFG_CUSTOM, 163);
CfgFloat cfg_Price_SRape ("PRICE_SRAPE",CFG_CUSTOM, 163);

/** \brief A parameter setting the minimum proportion of fodder demand that has to be supplied from own fodder production on pig farms.*/
CfgFloat cfg_Min_fodder_prod_pig ("MIN_FODDER_PROD_PIG", CFG_CUSTOM, 20);
/** \brief A parameter setting the minimum proportion of fodder demand that has to be supplied from own fodder production on cattle farms. Not in use.*/
CfgFloat cfg_Min_fodder_prod_cattle ("MIN_FODDER_PROD_CATTLE", CFG_CUSTOM, 35);


/** \brief A fitting parameter for the probability of hunter acceptance of a farm wiht distance from home - slope */
CfgFloat cfg_ClosestFarmProbParam1( "CLOSESTFARMPROBPARAMONE", CFG_CUSTOM, 0.005 );
/** \brief A fitting parameter for the probability of hunter acceptance of a farm that is smaller */
CfgFloat cfg_FarmSizeProbParam1( "FARMSIZEPROBPARAMONE", CFG_CUSTOM, 1.5 );
/** \brief A fitting parameter for the probability of hunter acceptance of a farm with distance from home - scaler */
CfgFloat cfg_ClosestFarmProbParam2("CLOSESTFARMPROBPARAMTWO", CFG_CUSTOM, 1.0);
FarmManager * g_farmmanager; //added 19.03.13
/** \brief A fitting parameter for the probability of hunter acceptance of a farm with distance from roost */
CfgFloat cfg_RoostDistProbParam1( "ROOSTDISTPROBPARAMONE", CFG_CUSTOM, 1.5 );

/** \brief A switch to fix the grain distribution to a specific year or make a random pick between years. 
* Used for the goose model. 0, = random, 1 = 2013, 2 = 2014 */
CfgInt cfg_grain_distribution("GOOSE_GRAIN_DISTRIBUTION", CFG_CUSTOM, 0, 0, 2);


/** \brief Used for sorting a farmers field size vector */
bool CompPcts (tpct i,tpct j) { return (i.pct<j.pct); }


Crop::Crop()
{
	m_ddegstoharvest = -1; // Set to -1 to indicate that this is not using ddegs to harvest, this will be reset in descendent classes as needed
	SetCropClassification(tocc_Winter); // Defualt classification is Winter - change this if necessary in the derived crop constructor
}

bool Crop::Do( Farm * /* a_farm */, LE * /* a_field */, FarmEvent* /* a_ev */ ) {
  return true;
}

void Crop::SimpleEvent(long a_date, int a_todo, bool a_lock) {

	m_farm->AddNewEvent(m_field->GetVegType(), a_date, m_field, a_todo, m_field->GetRunNum(), a_lock, 0, false, (TTypesOfVegetation)0);
}

void Crop::SimpleEvent_(long a_date, int a_todo, bool a_lock, Farm* a_farm, LE* a_field) {

	a_farm->AddNewEvent(a_field->GetVegType(), a_date, a_field, a_todo, a_field->GetRunNum(), a_lock, 0, false, (TTypesOfVegetation)0);
}


void Crop::ChooseNextCrop (int a_no_dates){

/**The function finds the next crop to grow on a field where the current crop's management has finished. If necessary, it
adjusts current crop's management to the new crop.*/

	if(cfg_OptimisingFarms.value()){ // do this only if we have optimising farms
		if(!cfg_OptimiseBedriftsmodelCrops.value()){ // do this only if we use almass crops!!

			if (g_date->GetYearNumber()>0){ //changed 030713 since we dont use m_rotation anymore - but we do in the hidden year!
				OptimisingFarm * opf;
				if(m_farm->GetType() == tof_OptimisingFarm){ //take only optimising farms
					opf =  dynamic_cast<OptimisingFarm*>(m_farm);
					opf->Match_crop_to_field (m_field);
					//move MDates if necessary, use the new_startdate:
					if (m_ev->m_startday > g_date->DayInYear(1,7)) {
						if (m_field->GetMDates(0,0) >=m_ev->m_startday)
						{
						  g_msg->Warn( WARN_BUG, "Crop::ChooseNextCrop(): ","Harvest too late for the next crop to start!!!" );
						  exit( 1 );
						}
						// Now fix any late finishing problems
						for (int i=0; i<a_no_dates; i++) {
							if(m_field->GetMDates(0,i)>=m_ev->m_startday) {
							  m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
							}
							if(m_field->GetMDates(1,i)>=m_ev->m_startday){
								m_field->SetMConstants(i,0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
								m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
							}
						}
					}

				}
			}
		}
	}

}

/**
\brief
Starts the main management loop for the farm and performs some error checking
*/
void Farm::Management( void ) {
  HandleEvents();
  for ( unsigned int i = 0; i < m_fields.size(); i++ ) {
    // Check for infinite loop in management plan.
    int count = m_fields[ i ]->GetVegStore();
    if ( count >= 0 )
      m_fields[ i ]->SetVegStore( ++count );
    if ( count > 800 ) {
      // More than two years where nothing happened.
      // Raise 'Merry Christmas'!
      char error_num[ 20 ];
      sprintf( error_num, "%d", m_fields[ i ]->GetVegType() );
      g_msg->Warn( WARN_BUG, "I the Farm Manager caught infinite loop in tov type:", error_num );
      sprintf( error_num, "%d", m_fields[ i ]->m_tried_to_do );
      g_msg->Warn( WARN_BUG, "It was last seen trying to perform action # ""(or thereabouts):", error_num );
      exit( 1 );
    }
  }
}


/**
\brief
Call do function for any crop with an outstanding event. Signal if the crop has terminated.
*/
bool Farm::LeSwitch( FarmEvent * ev ) {
	// Ignore this event if it is from the execution of
	// a previous management plan.
	if (ev->m_field->GetRunNum() > ev->m_run)
		return false;
	// Store what we are trying to do this time.
	// ***FN*** To be cleaned up later.
	ev->m_field->m_tried_to_do = ev->m_todo;
	bool done;

	switch (ev->m_event) {
		case tov_Carrots:
			done = m_carrots->Do( this, ev->m_field, ev );
			break;
		case tov_BroadBeans:
			done = m_broadbeans->Do( this, ev->m_field, ev );
			break;
		case tov_FodderGrass:
			done = m_foddergrass->Do( this, ev->m_field, ev );
			break;
		case tov_CloverGrassGrazed1:
			done = m_CGG1->Do( this, ev->m_field, ev );
			break;
		case tov_CloverGrassGrazed2:
			done = m_CGG2->Do( this, ev->m_field, ev );
			break;
		case tov_FieldPeas:
			done = m_fieldpeas->Do( this, ev->m_field, ev );
			break;
		case tov_FieldPeasSilage:
			done = m_fieldpeassilage->Do( this, ev->m_field, ev );
			break;
		case tov_FodderBeet:
			done = m_fodderbeet->Do( this, ev->m_field, ev );
			break;
		case tov_SugarBeet:
			done = m_sugarbeet->Do( this, ev->m_field, ev );
			break;
		case tov_OFodderBeet:
			done = m_ofodderbeet->Do( this, ev->m_field, ev );
			break;
		case tov_Maize:
			done = m_maize->Do( this, ev->m_field, ev );
			break;
		case tov_OBarleyPeaCloverGrass:
			done = m_OBarleyPCG->Do( this, ev->m_field, ev );
			break;
		case tov_OCarrots:
			done = m_ocarrots->Do( this, ev->m_field, ev );
			break;
		case tov_OCloverGrassSilage1:
			done = m_OCGS1->Do( this, ev->m_field, ev );
			break;
		case tov_OCloverGrassGrazed1:
			done = m_OCGG1->Do( this, ev->m_field, ev );
			break;
		case tov_OCloverGrassGrazed2:
			done = m_OCGG2->Do( this, ev->m_field, ev );
			break;
		case tov_OFieldPeas:
			done = m_ofieldpeas->Do( this, ev->m_field, ev );
			break;
		case tov_OFieldPeasSilage:
			done = m_ofieldpeassilage->Do( this, ev->m_field, ev );
			break;
		case tov_OGrazingPigs:
			done = m_ograzingpigs->Do( this, ev->m_field, ev );
			break;
		case tov_OrchardCrop:
			done = m_orchardcrop->Do( this, ev->m_field, ev );
			break;
		case tov_OOats:
			done = m_ooats->Do( this, ev->m_field, ev );
			break;
		case tov_Oats:
			done = m_oats->Do( this, ev->m_field, ev );
			break;
		case tov_OPermanentGrassGrazed:
			done = m_opermgrassgrazed->Do( this, ev->m_field, ev );
			break;
		case tov_OPotatoes:
			done = m_opotatoes->Do( this, ev->m_field, ev );
			break;
		case tov_OSpringBarley:
			done = m_ospringbarley->Do( this, ev->m_field, ev );
			break;
		case tov_OSBarleySilage:
			done = m_osbarleysilage->Do( this, ev->m_field, ev );
			break;
		case tov_OSeedGrass1:
			done = m_oseedgrass1->Do( this, ev->m_field, ev );
			break;
		case tov_OSeedGrass2:
			done = m_oseedgrass2->Do( this, ev->m_field, ev );
			break;
		case tov_OSpringBarleyExt:
			done = m_ospringbarleyext->Do( this, ev->m_field, ev );
			break;
		case tov_OSpringBarleyPigs:
			done = m_ospringbarleypigs->Do( this, ev->m_field, ev );
			break;
		case tov_OWinterBarley:
			done = m_owinterbarley->Do( this, ev->m_field, ev );
			break;
		case tov_OWinterBarleyExt:
			done = m_owinterbarleyext->Do( this, ev->m_field, ev );
			break;
		case tov_OWinterWheatUndersown:
			done = m_owinterwheatundersown->Do( this, ev->m_field, ev );
			break;
		case tov_OWinterWheat:
			done = m_owinterwheat->Do( this, ev->m_field, ev );
			break;
		case tov_OWinterWheatUndersownExt:
			done = m_owinterwheatundersownext->Do( this, ev->m_field, ev );
			break;
		case tov_OWinterRape:
			done = m_owinterrape->Do( this, ev->m_field, ev );
			break;
		case tov_OWinterRye:
			done = m_owinterrye->Do( this, ev->m_field, ev );
			break;
		case tov_PermanentGrassGrazed:
			done = m_permgrassgrazed->Do( this, ev->m_field, ev );
			break;
		case tov_PermanentGrassLowYield:
			done = m_permgrasslowyield->Do( this, ev->m_field, ev );
			break;
		case tov_PermanentGrassTussocky:
			done = m_permgrasstussocky->Do( this, ev->m_field, ev );
			break;
		case tov_PermanentSetaside:
			done = m_permanentsetaside->Do( this, ev->m_field, ev );
			break;
		case tov_Potatoes:
			done = m_potatoes->Do( this, ev->m_field, ev );
			break;
		case tov_PotatoesIndustry:
			done = m_potatoesindustry->Do( this, ev->m_field, ev );
			break;
		case tov_SeedGrass1:
			done = m_seedgrass1->Do( this, ev->m_field, ev );
			break;
		case tov_SeedGrass2:
			done = m_seedgrass2->Do( this, ev->m_field, ev );
			break;
		case tov_Setaside:
			done = m_setaside->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarley:
			done = m_springbarley->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleySpr:
			done = m_springbarleyspr->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleyPTreatment:
			done = m_springbarleyptreatment->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleySKManagement:
			done = m_springbarleyskmanagement->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleyCloverGrass:
			done = m_sbarleyclovergrass->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleySeed:
			done = m_springbarleyseed->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleySilage:
			done = m_springbarleysilage->Do( this, ev->m_field, ev );
			break;
		case tov_SpringRape:
			done = m_springrape->Do( this, ev->m_field, ev );
			break;
		case tov_Triticale:
			done = m_triticale->Do( this, ev->m_field, ev );
			break;
		case tov_OTriticale:
			done = m_otriticale->Do( this, ev->m_field, ev );
			break;
		case tov_WinterBarley:
			done = m_winterbarley->Do( this, ev->m_field, ev );
			break;
		case tov_WinterRape:
			done = m_winterrape->Do( this, ev->m_field, ev );
			break;
		case tov_WinterRye:
			done = m_winterrye->Do( this, ev->m_field, ev );
			break;
		case tov_WinterWheat:
			done = m_winterwheat->Do( this, ev->m_field, ev );
			break;
		case tov_WWheatPControl:
			done = m_wwheatpcontrol->Do( this, ev->m_field, ev );
			break;
		case tov_WWheatPToxicControl:
			done = m_wwheatptoxiccontrol->Do( this, ev->m_field, ev );
			break;
		case tov_WWheatPTreatment:
			done = m_wwheatptreatment->Do( this, ev->m_field, ev );
			break;
		case tov_AgroChemIndustryCereal:
			done = m_agrochemindustrycereal->Do( this, ev->m_field, ev );
			break;
		case tov_WinterWheatStrigling:
			done = m_winterwheatstrigling->Do( this, ev->m_field, ev );
			break;
		case tov_WinterWheatStriglingSingle:
			done = m_winterwheatstriglingsingle->Do( this, ev->m_field, ev );
			break;
		case tov_WinterWheatStriglingCulm:
			done = m_winterwheatstriglingculm->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleyCloverGrassStrigling:
			done = m_springbarleyclovergrassstrigling->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleyStrigling:
			done = m_springbarleystrigling->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleyStriglingSingle:
			done = m_springbarleystriglingsingle->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleyStriglingCulm:
			done = m_springbarleystriglingculm->Do( this, ev->m_field, ev );
			break;
		case tov_MaizeStrigling:
			done = m_maizestrigling->Do( this, ev->m_field, ev );
			break;
		case tov_MaizeSilage:
			done = m_maizesilage->Do( this, ev->m_field, ev );
			break;
		case tov_OMaizeSilage:
			done = m_omaizesilage->Do( this, ev->m_field, ev );
			break;
		case tov_WinterRapeStrigling:
			done = m_winterrapestrigling->Do( this, ev->m_field, ev );
			break;
		case tov_WinterRyeStrigling:
			done = m_winterryestrigling->Do( this, ev->m_field, ev );
			break;
		case tov_WinterBarleyStrigling:
			done = m_winterbarleystrigling->Do( this, ev->m_field, ev );
			break;
		case tov_FieldPeasStrigling:
			done = m_fieldpeasstrigling->Do( this, ev->m_field, ev );
			break;
		case tov_SpringBarleyPeaCloverGrassStrigling:
			done = m_springbarleypeaclovergrassstrigling->Do( this, ev->m_field, ev );
			break;
		case tov_PlantNursery:
			// uses youngforest management because this effectively does nothing, but stops the crop management creating looping error
		case tov_YoungForest:
			done = m_youngforest->Do(this, ev->m_field, ev);
			break;
		case tov_NorwegianPotatoes:
			done = m_norwegianpotatoes->Do(this, ev->m_field, ev);
			break;
		case tov_NorwegianOats:
			done = m_norwegianoats->Do(this, ev->m_field, ev);
			break;
		case tov_NorwegianSpringBarley:
			done = m_norwegianspringbarley->Do(this, ev->m_field, ev);
			break;

		case tov_PLWinterWheat:
			done = m_plwinterwheat->Do(this, ev->m_field, ev);
			break;
		case tov_PLWinterRape:
			done = m_plwinterrape->Do(this, ev->m_field, ev);
			break;
		case tov_PLWinterBarley:
			done = m_plwinterbarley->Do(this, ev->m_field, ev);
			break;
		case tov_PLWinterTriticale:
			done = m_plwintertriticale->Do(this, ev->m_field, ev);
			break;
		case tov_PLWinterRye:
			done = m_plwinterrye->Do(this, ev->m_field, ev);
			break;
		case tov_PLSpringWheat:
			done = m_plspringwheat->Do(this, ev->m_field, ev);
			break;
		case tov_PLSpringBarley:
			done = m_plspringbarley->Do(this, ev->m_field, ev);
			break;
		case tov_PLMaize:
			done = m_plmaize->Do(this, ev->m_field, ev);
			break;
		case tov_PLMaizeSilage:
			done = m_plmaizesilage->Do(this, ev->m_field, ev);
			break;
		case tov_PLPotatoes:
			done = m_plpotatoes->Do(this, ev->m_field, ev);
			break;
		case tov_PLBeet:
			done = m_plbeet->Do(this, ev->m_field, ev);
			break;
		case tov_PLFodderLucerne1:
			done = m_plfodderlucerne1->Do(this, ev->m_field, ev);
			break;
		case tov_PLFodderLucerne2:
			done = m_plfodderlucerne2->Do(this, ev->m_field, ev);
			break;
		case tov_PLCarrots:
			done = m_plcarrots->Do(this, ev->m_field, ev);
			break;
		case tov_PLSpringBarleySpr:
			done = m_plspringbarleyspr->Do(this, ev->m_field, ev);
			break;
		case tov_PLWinterWheatLate:
			done = m_plwinterwheatlate->Do(this, ev->m_field, ev);
			break;
		case tov_PLBeetSpr:
			done = m_plbeetspr->Do(this, ev->m_field, ev);
			break;
		case tov_PLBeans:
			done = m_plbeans->Do(this, ev->m_field, ev);
			break;

		case tov_NLBeet:
			done = m_nlbeet->Do(this, ev->m_field, ev);
			break;
		case tov_NLCarrots:
			done = m_nlcarrots->Do(this, ev->m_field, ev);
			break;
		case tov_NLMaize:
			done = m_nlmaize->Do(this, ev->m_field, ev);
			break;
		case tov_NLPotatoes:
			done = m_nlpotatoes->Do(this, ev->m_field, ev);
			break;
		case tov_NLSpringBarley:
			done = m_nlspringbarley->Do(this, ev->m_field, ev);
			break;
		case tov_NLWinterWheat:
			done = m_nlwinterwheat->Do(this, ev->m_field, ev);
			break;
		case tov_NLCabbage:
			done = m_nlcabbage->Do(this, ev->m_field, ev);
			break;
		case tov_NLTulips:
			done = m_nltulips->Do(this, ev->m_field, ev);
			break;
		case tov_NLGrassGrazed1:
			done = m_nlgrassgrazed1->Do(this, ev->m_field, ev);
			break;
		case tov_NLGrassGrazed1Spring:
			done = m_nlgrassgrazed1spring->Do(this, ev->m_field, ev);
			break;
		case tov_NLGrassGrazed2:
			done = m_nlgrassgrazed2->Do(this, ev->m_field, ev);
			break;
		case tov_NLGrassGrazedLast:
			done = m_nlgrassgrazedlast->Do(this, ev->m_field, ev);
			break;
		case tov_NLPermanentGrassGrazed:
			done = m_nlpermanentgrassgrazed->Do(this, ev->m_field, ev);
			break;
		case tov_NLBeetSpring:
			done = m_nlbeetspring->Do(this, ev->m_field, ev);
			break;
		case tov_NLCarrotsSpring:
			done = m_nlcarrotsspring->Do(this, ev->m_field, ev);
			break;
		case tov_NLMaizeSpring:
			done = m_nlmaizespring->Do(this, ev->m_field, ev);
			break;
		case tov_NLPotatoesSpring:
			done = m_nlpotatoesspring->Do(this, ev->m_field, ev);
			break;
		case tov_NLSpringBarleySpring:
			done = m_nlspringbarleyspring->Do(this, ev->m_field, ev);
			break;
		case tov_NLCabbageSpring:
			done = m_nlcabbagespring->Do(this, ev->m_field, ev);
			break;
		case tov_NLCatchPeaCrop:
			done = m_nlcatchpeacrop->Do(this, ev->m_field, ev);
			break;

		case tov_DummyCropPestTesting:
			done = m_dummycroppesttesting->Do(this, ev->m_field, ev);
			break;

		case tov_Lawn:
			// No current actions
			done = true;
			break;
			/* case tov_OFirstYearDanger: done = m_ofirstyeardanger->Do( this, ev->m_field, ev ); break; */
		default:
			char veg_type[ 20 ];
			sprintf( veg_type, "%d", ev->m_event );
			g_msg->Warn( WARN_FILE, "Farm::LeSwitch(): ""Unknown crop type: ", veg_type );
			exit( 1 );

	}
	return done;
}


/**
\brief
Adds an event to the event queue for a farm
*/
void Farm::AddNewEvent( TTypesOfVegetation a_event, long a_date, LE * a_field, int a_todo, long a_run, bool a_lock,
     int a_start, bool a_first_year, TTypesOfVegetation a_crop ) {
	 FarmEvent * ev = new FarmEvent( a_event, a_field, a_todo, a_run, a_lock, a_start, a_first_year, a_crop );
       m_queue.Push( ev, a_date );
}


/**
\brief
Return chance out of 0 to 100
*/
bool Farm::DoIt( double a_probability ) {
  return (g_rand_uni() < (a_probability/100.0));
}

/**
\brief
Return chance out of 0 to 1
*/
bool Farm::DoIt_prob(double a_probability) {
	return (g_rand_uni() < a_probability);
}

/**
\brief
Reads a rotation file into the rotation
*/
void Farm::ReadRotation(std::string str)
{
  ifstream ifile;
  ifile.open(str.c_str(),ios::in);
  if ( !ifile.is_open() ) {
      g_msg->Warn( "Cannot open file: ", str.c_str() );
      exit( 1 );
  }
  int nocrops;
  ifile >> nocrops;
  m_rotation.resize( nocrops );
  std::string cropref;
  for ( int i = 0; i < nocrops; i++ ) {
    ifile >> cropref;
    TTypesOfVegetation tov = g_farmmanager->TranslateCropCodes( cropref );
    m_rotation[ i ] = tov;
  }
  ifile.close();
}

/**
Rotation error check function
*/
void Farm::CheckRotationManagementLoop( FarmEvent * ev ) {
  if ( ev->m_field->GetMgtLoopDetectDate() == g_date->Date() ) {
    // The last crop managment plan stopped on the same day as
    // it was started.

    // Bump loop counter.
    ev->m_field->SetMgtLoopDetectCount( ev->m_field->GetMgtLoopDetectCount() + 1 );

    if ( ev->m_field->GetMgtLoopDetectCount() > ( long )( m_rotation.size() + 2 ) ) {
      // We have a loop.
      char errornum[ 20 ];
      sprintf( errornum, "%d", m_farmtype );
      g_msg->Warn( WARN_BUG, "Rotation management loop detected in farmtype ", errornum );
      exit( 1 );
    }
  } else {
    ev->m_field->SetMgtLoopDetectCount( 0 );
  }
}


/**
\brief
Returns the start date of the next crop in the rotation
*/
int Farm::GetNextCropStartDate( LE * a_field, TTypesOfVegetation & a_curr_veg ) {
  TTypesOfVegetation l_tov2;

  if ( a_field->GetRotIndex() < 0 || g_farm_fixed_crop_enable.value() //|| g_farm_test_crop.value()
	                                                                  ) {
    l_tov2 = a_curr_veg; // don't do it if no rotation
  } else {
    l_tov2 = m_rotation[ GetNextCropIndex( a_field->GetRotIndex() ) ];
  }
  a_curr_veg = l_tov2;
  return GetFirstDate( l_tov2 );
}


/**
\brief
If there are events to carry out do this, and perhaps start a new crop
*/
void Farm::HandleEvents( void ) {
  if ( m_queue.Empty() )
    return;

  LowPriPair < FarmEvent * > pair = m_queue.Bottom();
  FarmEvent * ev = pair.m_element;
  while ( pair.m_pri <= g_date->Date() ) {
    m_queue.Pop();
	/** Call the management plan for the current crop and it this return done=true starts the next management. */
	if ( LeSwitch( ev ) ) {
      // This crop management plan has terminated.

      // First check for an infinite loop in the rotation scheme,
      // ie. a scenario where all crops decide not to run given
      // the date.
      CheckRotationManagementLoop( ev );

      // Outdate any remaining events for this field.
      ev->m_field->BumpRunNum();

      // Crop treatment done, select and initiate new crop if in rotation.
      TTypesOfVegetation new_veg = ev->m_field->GetVegType();

      if ( ev->m_field->GetRotIndex() >= 0 ) {
        int new_index = GetNextCropIndex( ev->m_field->GetRotIndex() );
        new_veg = m_rotation[ new_index ];
        // Running in fixed crop mode?
        if ( g_farm_fixed_crop_enable.value() ) {
          new_veg = g_letype->TranslateVegTypes( g_farm_fixed_crop_type.value() );
        }
		/** Sets the new index to the rotation. */
        ev->m_field->SetRotIndex( new_index );
		/** Save the new veg type as the LE vegetation type */
		ev->m_field->SetVegType(new_veg, tov_Undefined);
      }

      // Reset the event list for this field.
      ev->m_field->ResetTrace();
      // Reset event timeout counter.
      ev->m_field->SetVegStore( 0 );

      // The next bit simply determines the start date of the next crop in
      // the rotation and passes this to the start crop event.
      // The crop is responsible for raising an error if the next crop is
      // not possible or otherwise handling the problem

      // 19/5-2003: Note: This code was moved out into a dedicated
      // method of the Farm class, GetNextCropStartDate(), as precisely
      // the same piece of code needs to be run during initialization of
      // farm management.
      TTypesOfVegetation l_tov = new_veg;
      int l_nextcropstartdate = GetNextCropStartDate( ev->m_field, l_tov );

      // Create 'start' event for today and put it on the queue.
      AddNewEvent( new_veg, g_date->Date(), ev->m_field, PROG_START, ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov );

      // Set starting date for rotation mgmt loop detection.
      ev->m_field->SetMgtLoopDetectDate( g_date->Date() );
    }

    delete ev;

    if ( m_queue.Empty() )
      return;
    pair = m_queue.Bottom();
    ev = pair.m_element;
  }
}


void OptimisingFarm::HandleEvents( void ) {
  if ( m_queue.Empty() )
    return;

  LowPriPair < FarmEvent * > pair = m_queue.Bottom();
  FarmEvent * ev = pair.m_element;
  while ( pair.m_pri <= g_date->Date() ) {
    m_queue.Pop();

	if ( LeSwitch( ev ) ) {
      // This crop management plan has terminated.

      // First check for an infinite loop in the rotation scheme,
      // ie. a scenario where all crops decide not to run given
      // the date.
      CheckRotationManagementLoop( ev );

      // Outdate any remaining events for this field.
      ev->m_field->BumpRunNum();

      // Crop treatment done, select and initiate new crop if in rotation.
      TTypesOfVegetation new_veg = ev->m_field->GetVegType();

      if ( ev->m_field->GetRotIndex() >= 0 ) {
        int new_index = GetNextCropIndex( ev->m_field->GetRotIndex() );
        new_veg = m_rotation[ new_index ];
        // Running in fixed crop mode?
        if ( g_farm_fixed_crop_enable.value() ) {
          new_veg = g_letype->TranslateVegTypes( g_farm_fixed_crop_type.value() );
        }
        /*
		if ( g_farm_test_crop.value() ) {
          new_veg = g_letype->TranslateVegTypes( g_farm_test_crop_type.value() );
        }
		*/
        ev->m_field->SetRotIndex( new_index );
        ev->m_field->SetVegType( new_veg, tov_Undefined );
		// ***CJT*** Testing removal 3/2/2015  ev->m_field->ForceGrowthTest(); 
      }


	  //------------05.03.12 AM - tell a field the crop has changed - here the OptimisingFarm part---------------------------------------
	  if(!cfg_OptimiseBedriftsmodelCrops.value()){ //don't do this if you simulate the bedriftsmodel (original farm optimization model) crops
		  //switch the previous crop (if it is still there) to position 1 in an array m_CropDataStorage; put a new crop at position 0
		  VegElement * pf =  dynamic_cast<VegElement*>(ev->m_field);
		  if(pf->Get_taken(0)){ //there is a crop at position 0 - so need to copy it to position 1 and clear all values at position 0
			  if(pf->Get_taken(1)){ //problem - we have 3rd crop when two previous crops were not calculated yet - it shouldn't happen!
				char error_num[ 20 ];
				sprintf( error_num, "%d", new_veg );
				g_msg->Warn( WARN_FILE, "Farm::HandleEvents(): there is 3rd crop starting when 2 previous were not calculated yet, the 3rd crop is", error_num );
				exit( 1 );
			  }
			  pf->Set_CropDataStorage(1, pf->Get_CropDataStorage(0)); //copy the content of a struct at position 0 of the m_CropDataStorage array to position 1
			  pf->Set_taken(true, 1); //mark the struct at position 1 as taken by a crop
			  pf->Clean_CropDataStorage(0); //clear struct at position 0

		  }
		  //now save the new crop at position zero - whether there was a crop here or not
		  pf->Clean_CropDataStorage(0); //clear struct at position 0 //do it cause the previous crop's operations could have been saved here - if there was some time between accoutning and start of this new crop
		  pf->Set_taken(true, 0); // the new crop is at position 0
		  pf->Set_tov_type(new_veg, 0); //save the tov_type of a new crop
		  pf->Set_area_in_crop_data(pf->GetArea());
	  }
	  //---------------------end 05.03.13----------------------------------------------------------------


      // Reset the event list for this field.
      ev->m_field->ResetTrace();
      // Reset event timeout counter.
      ev->m_field->SetVegStore( 0 );

      // The next bit simply determines the start date of the next crop in
      // the rotation and passes this to the start crop event.
      // The crop is responsible for raising an error if the next crop is
      // not possible or otherwise handling the problem

      // 19/5-2003: Note: This code was moved out into a dedicated
      // method of the Farm class, GetNextCropStartDate(), as precisely
      // the same piece of code needs to be run during initialization of
      // farm management.
      TTypesOfVegetation l_tov = new_veg;
      int l_nextcropstartdate = GetNextCropStartDate( ev->m_field, l_tov );

      // Create 'start' event for today and put it on the queue.
      AddNewEvent( new_veg, g_date->Date(), ev->m_field, PROG_START, ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov );

      // Set starting date for rotation mgmt loop detection.
      ev->m_field->SetMgtLoopDetectDate( g_date->Date() );
    }

    delete ev;

    if ( m_queue.Empty() )
      return;
    pair = m_queue.Bottom();
    ev = pair.m_element;
  }
}




/**
\brief
Farm constructor - creates an instance of each possible crop type
*/
Farm::Farm( FarmManager* a_manager ) 
{
	m_OurManager = a_manager;
	m_carrots = new Carrots;
	m_broadbeans = new BroadBeans;
	m_CGG1 = new CloverGrassGrazed1;
	m_CGG2 = new CloverGrassGrazed2;
	m_fieldpeas = new FieldPeas;
	m_fieldpeassilage = new FieldPeasSilage;
	m_fodderbeet = new Fodderbeet;
	m_sugarbeet = new Sugarbeet;
	m_ofodderbeet = new OFodderbeet;
	m_foddergrass = new FodderGrass;
	m_maize = new Maize;
	m_maizesilage = new MaizeSilage;
	m_omaizesilage = new OMaizeSilage;
	m_OBarleyPCG = new OBarleyPeaCloverGrass;
	m_ocarrots = new OCarrots;
	m_OCGG1 = new OCloverGrassGrazed1;
	m_OCGG2 = new OCloverGrassGrazed2;
	m_OCGS1 = new OCloverGrassSilage1;
	m_ofieldpeas = new OFieldPeas;
	m_ofieldpeassilage = new OFieldPeasSilage;
	m_ofirstyeardanger = new OFirstYearDanger;
	m_ograzingpigs = new OGrazingPigs;
	m_orchardcrop = new OrchardCrop;
	m_oats = new Oats;
	m_ooats = new OOats;
	m_opermgrassgrazed = new OPermanentGrassGrazed;
	m_opotatoes = new OPotatoes;
	m_oseedgrass1 = new OSeedGrass1;
	m_oseedgrass2 = new OSeedGrass2;
	m_ospringbarley = new OSpringBarley;
	m_osbarleysilage = new OSBarleySilage;
	m_ospringbarleyext = new OSpringBarleyExt;
	m_ospringbarleypigs = new OSpringBarleyPigs;
	m_owinterbarley = new OWinterBarley;
	m_owinterbarleyext = new OWinterBarleyExt;
	m_owinterrape = new OWinterRape;
	m_owinterrye = new OWinterRye;
	m_owinterwheat = new OWinterWheat;
	m_owinterwheatundersown = new OWinterWheatUndersown;
	m_owinterwheatundersownext = new OWinterWheatUndersownExt;
	m_permanentsetaside = new PermanentSetAside;
	m_permgrassgrazed = new PermanentGrassGrazed;
	m_permgrasslowyield = new PermanentGrassLowYield;
	m_permgrasstussocky = new PermanentGrassTussocky;
	m_potatoes = new Potatoes;
	m_potatoesindustry = new PotatoesIndustry;
	m_sbarleyclovergrass = new SpringBarleyCloverGrass;
	m_seedgrass1 = new SeedGrass1;
	m_seedgrass2 = new SeedGrass2;
	m_setaside = new SetAside;
	m_springbarley = new SpringBarley;
	m_springbarleyspr = new SpringBarleySpr;
	m_springbarleyptreatment = new SpringBarleyPTreatment;
	m_springbarleyskmanagement = new SpringBarleySKManagement;
	m_springbarleyseed = new SpringBarleySeed;
	m_springbarleysilage = new SpringBarleySilage;
	m_springrape = new SpringRape;
	m_triticale = new Triticale;
	m_otriticale = new OTriticale;
	m_winterbarley = new WinterBarley;
	m_winterrape = new WinterRape;
	m_winterrye = new WinterRye;
	m_winterwheat = new WinterWheat;
	m_wwheatpcontrol = new WWheatPControl;
	m_wwheatptoxiccontrol = new WWheatPToxicControl;
	m_wwheatptreatment = new WWheatPTreatment;
	m_agrochemindustrycereal = new AgroChemIndustryCereal;
	m_winterwheatstrigling = new WinterWheatStrigling;
	m_winterwheatstriglingsingle = new WinterWheatStriglingSingle;
	m_winterwheatstriglingculm = new WinterWheatStriglingCulm;
	m_springbarleyclovergrassstrigling = new SpringBarleyCloverGrassStrigling;
	m_springbarleystrigling = new SpringBarleyStrigling;
	m_springbarleystriglingsingle = new SpringBarleyStriglingSingle;
	m_springbarleystriglingculm = new SpringBarleyStriglingCulm;
	m_maizestrigling = new MaizeStrigling;
	m_winterrapestrigling = new WinterRapeStrigling;
	m_winterryestrigling = new WinterRyeStrigling;
	m_winterbarleystrigling = new WinterBarleyStrigling;
	m_fieldpeasstrigling = new FieldPeasStrigling;
	m_springbarleypeaclovergrassstrigling = new SpringBarleyPeaCloverGrassStrigling;
	m_youngforest = new YoungForestCrop;
	m_norwegianpotatoes = new NorwegianPotatoes;
	m_norwegianoats = new NorwegianOats;
	m_norwegianspringbarley = new NorwegianSpringBarley;
	m_plwinterwheat = new PLWinterWheat;
	m_plwinterrape = new PLWinterRape;
	m_plwinterbarley = new PLWinterBarley;
	m_plwintertriticale = new PLWinterTriticale;
	m_plwinterrye = new PLWinterRye;
	m_plspringwheat = new PLSpringWheat;
	m_plspringbarley = new PLSpringBarley;
	m_plmaize = new PLMaize;
	m_plmaizesilage = new PLMaizeSilage;
	m_plpotatoes = new PLPotatoes;
	m_plbeet = new PLBeet;
	m_plfodderlucerne1 = new PLFodderLucerne1;
	m_plfodderlucerne2 = new PLFodderLucerne2;
	m_plcarrots = new PLCarrots;
	m_plspringbarleyspr = new PLSpringBarleySpr;
	m_plwinterwheatlate = new PLWinterWheatLate;
	m_plbeetspr = new PLBeetSpr;
	m_plbeans = new PLBeans;

	m_nlbeet = new NLBeet;
	m_nlcarrots = new NLCarrots;
	m_nlmaize = new NLMaize;
	m_nlpotatoes = new NLPotatoes;
	m_nlspringbarley = new NLSpringBarley;
	m_nlwinterwheat = new NLWinterWheat;
	m_nlcabbage = new NLCabbage;
	m_nltulips = new NLTulips;
	m_nlgrassgrazed1 = new NLGrassGrazed1;
	m_nlgrassgrazed1spring = new NLGrassGrazed1Spring;
	m_nlgrassgrazed2 = new NLGrassGrazed2;
	m_nlgrassgrazedlast = new NLGrassGrazedLast;
	m_nlpermanentgrassgrazed = new NLPermanentGrassGrazed;
	m_nlbeetspring = new NLBeetSpring;
	m_nlcarrotsspring = new NLCarrotsSpring;
	m_nlmaizespring = new NLMaizeSpring;
	m_nlpotatoesspring = new NLPotatoesSpring;
	m_nlspringbarleyspring = new NLSpringBarleySpring;
	m_nlcabbagespring = new NLCabbageSpring;
	m_nlcatchpeacrop = new NLCatchPeaCrop;

	m_dummycroppesttesting = new DummyCropPestTesting;

	m_rotation_sync_index = -1;
	// Defaults that need to be overridden when necessary
	m_stockfarmer = false;
	m_intensity = 0;
	m_HuntersList.resize(0); // Set the number of hunters to zero at the start.
}


/**
\brief
Farm destructor - deletes all crop instances and empties event queues
*/
Farm::~Farm(void) {
	delete m_carrots;
	delete m_broadbeans;
	delete m_CGG2;
	delete m_CGG1;
	delete m_fieldpeas;
	delete m_fieldpeassilage;
	delete m_fodderbeet;
	delete m_sugarbeet;
	delete m_ofodderbeet;
	delete m_foddergrass;
	delete m_maizesilage;
	delete m_omaizesilage;
	delete m_maize;
	delete m_ocarrots;
	delete m_OCGG1;
	delete m_OCGG2;
	delete m_OCGS1;
	delete m_ofieldpeas;
	delete m_ofieldpeassilage;
	delete m_ofirstyeardanger;
	delete m_ograzingpigs;
	delete m_orchardcrop;
	delete m_ooats;
	delete m_oats;
	delete m_opermgrassgrazed;
	delete m_opotatoes;
	delete m_ospringbarley;
	delete m_ospringbarleyext;
	delete m_osbarleysilage;
	delete m_ospringbarleypigs;
	delete m_owinterbarley;
	delete m_owinterbarleyext;
	delete m_owinterrape;
	delete m_owinterrye;
	delete m_owinterwheatundersown;
	delete m_owinterwheatundersownext;
	delete m_OBarleyPCG;
	delete m_permanentsetaside;
	delete m_permgrassgrazed;
	delete m_permgrasstussocky;
	delete m_permgrasslowyield;
	delete m_potatoes;
	delete m_potatoesindustry;
	delete m_sbarleyclovergrass;
	delete m_seedgrass2;
	delete m_seedgrass1;
	delete m_setaside;
	delete m_springbarley;
	delete m_springbarleyptreatment;
	delete m_springbarleyskmanagement;
	delete m_springbarleyseed;
	delete m_springrape;
	delete m_springbarleysilage;
	delete m_agrochemindustrycereal;
	delete m_triticale;
	delete m_winterbarley;
	delete m_winterrape;
	delete m_winterrye;
	delete m_winterwheat;
	delete m_wwheatpcontrol;
	delete m_wwheatptoxiccontrol;
	delete m_wwheatptreatment;
	delete m_winterwheatstrigling;
	delete m_winterwheatstriglingsingle;
	delete m_winterwheatstriglingculm;
	delete m_springbarleyclovergrassstrigling;
	delete m_springbarleystrigling;
	delete m_springbarleystriglingsingle;
	delete m_springbarleystriglingculm;
	delete m_maizestrigling;
	delete m_winterrapestrigling;
	delete m_winterryestrigling;
	delete m_winterbarleystrigling;
	delete m_fieldpeasstrigling;
	delete m_springbarleypeaclovergrassstrigling;
	delete m_youngforest;
	delete m_norwegianoats;
	delete m_norwegianspringbarley;

	delete m_plwinterwheat;
	delete m_plwinterrape;
	delete m_plwinterbarley;
	delete m_plwintertriticale;
	delete m_plwinterrye;
	delete m_plspringwheat;
	delete m_plspringbarley;
	delete m_plmaize;
	delete m_plmaizesilage;
	delete m_plpotatoes;
	delete m_plbeet;
	delete m_plfodderlucerne1;
	delete m_plfodderlucerne2;
	delete m_plcarrots;
	delete m_plspringbarleyspr;
	delete m_plwinterwheatlate;
	delete m_plbeetspr;
	delete m_plbeans;

	delete m_nlbeet;
	delete m_nlcarrots;
	delete m_nlmaize;
	delete m_nlpotatoes;
	delete m_nlspringbarley;
	delete m_nlwinterwheat;
	delete m_nlcabbage;
	delete m_nltulips;
	delete m_nlgrassgrazed1;
	delete m_nlgrassgrazed1spring;
	delete m_nlgrassgrazed2;
	delete m_nlgrassgrazedlast;
	delete m_nlpermanentgrassgrazed;
	delete m_nlbeetspring;
	delete m_nlcarrotsspring;
	delete m_nlmaizespring;
	delete m_nlpotatoesspring;
	delete m_nlspringbarleyspring;
	delete m_nlcabbagespring;
	delete m_nlcatchpeacrop;

	delete m_dummycroppesttesting;

	LowPriPair < FarmEvent * > pair;

	while (!m_queue.Empty()) {
		pair = m_queue.Bottom();
		m_queue.Pop();
		delete pair.m_element;
	}
}

/** \brief Returns the area of arable fields owned by that farm */
int Farm::GetArea() {
	int area = 0;
	for (unsigned int i = 0; i < m_fields.size(); i++) {
		if (m_fields[i]->GetElementType() == tole_Field) area += (int)m_fields[i]->GetArea();
	}
	return area;
}

/** \brief Returns the area of all fields owned by that farm */
int Farm::GetTotalArea() {
	int area = 0;
	for (unsigned int i = 0; i < m_fields.size(); i++) 
	{
		area += (int)m_fields[i]->GetArea();
	}
	return area;
}

/** \brief Returns the area of arable fields owned by that farm */
double Farm::GetAreaDouble() {
	double area = 0;
	for ( unsigned int i = 0; i < m_fields.size(); i++ ) {
		if (m_fields[i]->GetElementType()==tole_Field) area += m_fields[i]->GetArea();
	}
	return area;
}

/** Returns the number of fields owned by that farm with an openness above a_openness */
int Farm::GetNoOpenFields(int a_openness)
{
	int num = 0;
	for (unsigned int i = 0; i < m_fields.size(); i++) {
		if (m_fields[i]->GetOpenness() > a_openness) num++;
	}
	return num;
}

/** Returns the area of fields owned by that farm with an openness above a_openness */
int Farm::GetAreaOpenFields(int a_openness)
{
	int area = 0;
	for (unsigned int i = 0; i < m_fields.size(); i++) {
		if (m_fields[i]->GetOpenness() > a_openness) area += int(m_fields[i]->GetArea());
	}
	return area;
}

/**
\brief
Gets the first crop for the farm.
*/
/**
This method also synchronises farm rotations either within or between farms if needed. This is useful to try simple what if scenarios.
*/
int Farm::GetFirstCropIndex( TTypesOfLandscapeElement /* a_type */ ) {
  // If g_farm_fixed_rotation, then determine the first
  // crop number in the rotation rotation number.
  if ( g_farm_fixed_rotation_enable.value() ) {

    if ( !g_farm_fixed_rotation_farms_async.value() ) {
      // We are running all the farms synchronized, so
      // simply set the first crop to run on all farm fields.
      return 0;
    }

    // Each farm runs its fields sync'ed but independently from
    // the other farmers.

    // Determine if this farm has selected its own start index
    // and set it if not. m_rotation_sync_index is initialized
    // to -1 by the Farm::Farm() constructor.
    if ( -1 == m_rotation_sync_index ) {
      m_rotation_sync_index = (int) (rand() % m_rotation.size());
    }
    // Return farm localized rotation index.
    return m_rotation_sync_index;
  }

  // Not synchronised, but we want to follow our rotation sequence, so check
  // if we have started this process, if not set the sync value.
  // afterwards just increment this.
  if ( -1 == m_rotation_sync_index ) {
	  if (m_rotation.size() > 0) m_rotation_sync_index = (int)(rand() % m_rotation.size()); else  m_rotation_sync_index = 0;
  }
  else m_rotation_sync_index = (int) ((m_rotation_sync_index+1) % m_rotation.size());
  return m_rotation_sync_index;
}


/**
\brief
Returns the next crop in the rotation
*/
/**
Also provides the possibility of over-riding rotations using configuration settings
*/
int Farm::GetNextCropIndex( int a_rot_index ) {
  if ( !g_farm_enable_crop_rotation.value() ) {
    // Rotation not enabled.
    return a_rot_index;
  }

  if ( a_rot_index == -1 )
    return -1;

  if ( ( unsigned int ) ( ++a_rot_index ) == m_rotation.size() ) //AM comm: the last crop was the last element of the vector  - so go back to the element zero; otherwise just add 1 to the index
    a_rot_index = 0;

  return a_rot_index;
}


/**
\brief
Adds a field to a farm
*/
void Farm::AddField( LE * a_newfield ) {
  int i = (int) m_fields.size();

  m_fields.resize( i + 1 );
  m_fields[ i ] = a_newfield;
  // Must set the rot index to something other than -1, but identify it as not usefully set as yet.
	TTypesOfLandscapeElement ele = a_newfield->GetElementType();
	switch (ele) {
		case tole_PermPastureLowYield:
		case tole_YoungForest:
		case tole_PermPasture:
		case tole_PermPastureTussocky:
		case tole_PermanentSetaside:
		case tole_Orchard:
		case tole_PlantNursery:
			m_fields[i]->SetRotIndex(-2);
			break;
		default:
			m_fields[ i ]->SetRotIndex(-1);
	}
}

/**
\brief
Removes a field from a farm
*/
void Farm::RemoveField( LE * a_field ) {
  int nf = (int) m_fields.size();
  for ( int i = 0; i < nf; i++ ) {
    if ( m_fields[ i ] == a_field ) {
      m_fields.erase( m_fields.begin() + i );
      return;
    }
  }
  // If we reach here there is something wrong because the field is not a
  // member of this farm
  g_msg->Warn( WARN_BUG, "Farm::RemoveField(LE* a_field): ""Unknown field! ", "" );
  exit( 1 );
}


void Farm::Assign_rotation(vector<TTypesOfVegetation>a_new_rotation) {

	m_rotation.clear();
	m_rotation = a_new_rotation;

}

/**
\brief
Kicks off the farm's management
*/
void UserDefinedFarm::InitiateManagement( void ) {
	// First we need to assign the permanent crops if any.
	if (m_PermCrops.size()>0) {
		// We have something to do
		for (int i=0; i<(int)m_PermCrops.size(); i++) {
			AssignPermanentCrop(m_PermCrops[i].Tov, m_PermCrops[i].Pct);
		}
	}
	Farm::InitiateManagement();
}

/**
\brief
Kicks off the farm's management
*/
void Farm::InitiateManagement( void ) {
	for ( unsigned int i = 0; i < m_fields.size(); i++ ) {
		int rot_index =  m_fields[ i ]->GetRotIndex();
		TTypesOfVegetation new_veg = tov_Undefined;
		// If the field has been designated as non-rotating and therefore already has its veg type, then skip it.
		if ( rot_index < -1 ) {
			// Check for any type of permanent element type with management plan.
			TTypesOfLandscapeElement ele = m_fields[ i ]->GetElementType();
			switch (ele) {
				case tole_PermPastureLowYield:
					new_veg = tov_PermanentGrassLowYield;
					break;
				case tole_YoungForest:
					new_veg = tov_YoungForest;
					break;
				case tole_PermPasture:
					new_veg = tov_PermanentGrassGrazed;
					break;
				case tole_PermPastureTussocky:
					new_veg = tov_PermanentGrassTussocky;
					break;
				case tole_PermanentSetaside:
					new_veg = tov_PermanentSetaside;
					break;
				case tole_Orchard:
					new_veg = tov_OrchardCrop;
					break;
				case tole_PlantNursery:
					new_veg = tov_PlantNursery;
					break;
				default:
					if (rot_index != -4) {
						// Error
						g_msg->Warn("Unexpected negative value in Farm::InitiateManagement","");
						exit(0);
					}
					else
					{
						new_veg = m_fields[i]->GetVegType();
					}
			}
		}
		else {
			rot_index = GetFirstCropIndex( m_fields[ i ]->GetElementType() );
			new_veg = m_rotation[ rot_index ];
		}
		// Running in fixed crop mode?
		if ( g_farm_fixed_crop_enable.value() ) {
			int fv = g_farm_fixed_crop_type.value();
			new_veg = g_letype->TranslateVegTypes( fv );
		}
		m_fields[ i ]->SetVegType( new_veg, tov_Undefined );
		m_fields[ i ]->SetRotIndex( rot_index );
		m_fields[ i ]->SetGrowthPhase( janfirst );
		// Reset event timeout counter. We are now 800 days from
		// oblivion.
		long prog_start_date = g_date->Date();
		m_fields[ i ]->SetVegStore( 0 );
		TTypesOfVegetation l_tov = new_veg;
		int l_nextcropstartdate = GetNextCropStartDate( m_fields[ i ], l_tov );
		AddNewEvent( new_veg, prog_start_date, m_fields[ i ], PROG_START, 0, false, l_nextcropstartdate, true, l_tov );
	}
}

/**
\brief
Gets the start date for a crop type
*/
int Farm::GetFirstDate(TTypesOfVegetation a_tov2) {
	switch (a_tov2) {
	case tov_Carrots:
		return m_carrots->GetFirstDate();
	case tov_BroadBeans:
		return m_broadbeans->GetFirstDate();
	case tov_FodderGrass:
		return m_foddergrass->GetFirstDate();
	case tov_CloverGrassGrazed1:
		return m_CGG1->GetFirstDate();
	case tov_CloverGrassGrazed2:
		return m_CGG2->GetFirstDate();
	case tov_FieldPeas:
		return m_fieldpeas->GetFirstDate();
	case tov_FieldPeasSilage:
		return m_fieldpeassilage->GetFirstDate();
	case tov_FodderBeet:
		return m_fodderbeet->GetFirstDate();
	case tov_SugarBeet:
		return m_sugarbeet->GetFirstDate();
	case tov_OFodderBeet:
		return m_ofodderbeet->GetFirstDate();
	case tov_Maize:
		return m_maize->GetFirstDate();
	case tov_OMaizeSilage:
		return m_omaizesilage->GetFirstDate();
	case tov_MaizeSilage:
		return m_maizesilage->GetFirstDate();
	case tov_OBarleyPeaCloverGrass:
		return m_OBarleyPCG->GetFirstDate();
	case tov_OCarrots:
		return m_ocarrots->GetFirstDate();
	case tov_OCloverGrassGrazed1:
		return m_OCGG1->GetFirstDate();
	case tov_OCloverGrassGrazed2:
		return m_OCGG2->GetFirstDate();
	case tov_OCloverGrassSilage1:
		return m_OCGS1->GetFirstDate();
	case tov_OFieldPeas:
		return m_ofieldpeas->GetFirstDate();
	case tov_OFirstYearDanger:
		return m_ofirstyeardanger->GetFirstDate();
	case tov_OGrazingPigs:
		return m_ograzingpigs->GetFirstDate();
	case tov_OrchardCrop:
		return m_orchardcrop->GetFirstDate();
	case tov_OOats:
		return m_ooats->GetFirstDate();
	case tov_Oats:
		return m_oats->GetFirstDate();
	case tov_OPermanentGrassGrazed:
		return m_opermgrassgrazed->GetFirstDate();
	case tov_OPotatoes:
		return m_opotatoes->GetFirstDate();
	case tov_OSpringBarley:
		return m_ospringbarley->GetFirstDate();
	case tov_OSBarleySilage:
		return m_osbarleysilage->GetFirstDate();
	case tov_OSpringBarleyExt:
		return m_ospringbarleyext->GetFirstDate();
	case tov_OSpringBarleyPigs:
		return m_ospringbarleypigs->GetFirstDate();
	case tov_OWinterBarley:
		return m_owinterbarley->GetFirstDate();
	case tov_OWinterBarleyExt:
		return m_owinterbarleyext->GetFirstDate();
	case tov_OWinterRape:
		return m_owinterrape->GetFirstDate();
	case tov_OWinterRye:
		return m_owinterrye->GetFirstDate();
	case tov_OWinterWheatUndersown:
		return m_owinterwheatundersown->GetFirstDate();
	case tov_OWinterWheat:
		return m_owinterwheat->GetFirstDate();
	case tov_OWinterWheatUndersownExt:
		return m_owinterwheatundersownext->GetFirstDate();
	case tov_PermanentGrassGrazed:
		return m_permgrassgrazed->GetFirstDate();
	case tov_PermanentGrassLowYield:
		return m_permgrasslowyield->GetFirstDate();
	case tov_PermanentGrassTussocky:  // Only used for tole_PermPastureTussocky
		return m_permgrasstussocky->GetFirstDate();
	case tov_PermanentSetaside:
		return m_permanentsetaside->GetFirstDate();
	case tov_Potatoes:
		return m_potatoes->GetFirstDate();
	case tov_SeedGrass1:
		return m_seedgrass1->GetFirstDate();
	case tov_SeedGrass2:
		return m_seedgrass2->GetFirstDate();
	case tov_Setaside:
		return m_setaside->GetFirstDate();
	case tov_SpringBarley:
		return m_springbarley->GetFirstDate();
	case tov_SpringBarleySpr:
		return m_springbarleyspr->GetFirstDate();
	case tov_SpringBarleyPTreatment:
		return m_springbarleyptreatment->GetFirstDate();
	case tov_SpringBarleySKManagement:
		return m_springbarleyskmanagement->GetFirstDate();
	case tov_SpringBarleyCloverGrass:
		return m_sbarleyclovergrass->GetFirstDate();
	case tov_SpringBarleySeed:
		return m_springbarleyseed->GetFirstDate();
	case tov_SpringBarleySilage:
		return m_springbarleysilage->GetFirstDate();
	case tov_SpringBarleyStrigling:
		return m_springbarleystrigling->GetFirstDate();
	case tov_SpringBarleyStriglingSingle:
		return m_springbarleystriglingsingle->GetFirstDate();
	case tov_SpringBarleyStriglingCulm:
		return m_springbarleystriglingculm->GetFirstDate();
	case tov_SpringRape:
		return m_springrape->GetFirstDate();
	case tov_Triticale:
		return m_triticale->GetFirstDate();
	case tov_OTriticale:
		return m_otriticale->GetFirstDate();
	case tov_WinterBarley:
		return m_winterbarley->GetFirstDate();
	case tov_WinterRape:
		return m_winterrape->GetFirstDate();
	case tov_WinterRye:
		return m_winterrye->GetFirstDate();
	case tov_WinterWheat:
		return m_winterwheat->GetFirstDate();
	case tov_WinterWheatStrigling:
		return m_winterwheatstrigling->GetFirstDate();
	case tov_WinterWheatStriglingSingle:
		return m_winterwheatstriglingsingle->GetFirstDate();
	case tov_WinterWheatStriglingCulm:
		return m_winterwheatstriglingculm->GetFirstDate();
	case tov_WWheatPControl:
		return m_wwheatpcontrol->GetFirstDate();
	case tov_WWheatPToxicControl:
		return m_wwheatptoxiccontrol->GetFirstDate();
	case tov_WWheatPTreatment:
		return m_wwheatptreatment->GetFirstDate();
	case tov_AgroChemIndustryCereal:
		return m_agrochemindustrycereal->GetFirstDate();
	case tov_YoungForest:
		return m_youngforest->GetFirstDate();
	case tov_NorwegianPotatoes:
		return m_norwegianpotatoes->GetFirstDate();
	case tov_NorwegianOats:
		return m_norwegianoats->GetFirstDate();
	case tov_NorwegianSpringBarley:
		return m_norwegianspringbarley->GetFirstDate();

	case tov_PLWinterWheat:
		return m_plwinterwheat->GetFirstDate();
	case tov_PLWinterRape:
		return m_plwinterrape->GetFirstDate();
	case tov_PLWinterBarley:
		return m_plwinterbarley->GetFirstDate();
	case tov_PLWinterTriticale:
		return m_plwintertriticale->GetFirstDate();
	case tov_PLWinterRye:
		return m_plwinterrye->GetFirstDate();
	case tov_PLSpringWheat:
		return m_plspringwheat->GetFirstDate();
	case tov_PLSpringBarley:
		return m_plspringbarley->GetFirstDate();
	case tov_PLMaize:
		return m_plmaize->GetFirstDate();
	case tov_PLMaizeSilage:
		return m_plmaizesilage->GetFirstDate();
	case tov_PLPotatoes:
		return m_plpotatoes->GetFirstDate();
	case tov_PLBeet:
		return m_plbeet->GetFirstDate();
	case tov_PLFodderLucerne1:
		return m_plfodderlucerne1->GetFirstDate();
	case tov_PLFodderLucerne2:
		return m_plfodderlucerne2->GetFirstDate();
	case tov_PLCarrots:
		return m_plcarrots->GetFirstDate();
	case tov_PLSpringBarleySpr:
		return m_plspringbarleyspr->GetFirstDate();
	case tov_PLWinterWheatLate:
		return m_plwinterwheatlate->GetFirstDate();
	case tov_PLBeetSpr:
		return m_plbeetspr->GetFirstDate();
	case tov_PLBeans:
		return m_plbeans->GetFirstDate();

	case tov_NLBeet:
		return m_nlbeet->GetFirstDate();
	case tov_NLCarrots:
		return m_nlcarrots->GetFirstDate();
	case tov_NLMaize:
		return m_nlmaize->GetFirstDate();
	case tov_NLPotatoes:
		return m_nlpotatoes->GetFirstDate();
	case tov_NLSpringBarley:
		return m_nlspringbarley->GetFirstDate();
	case tov_NLWinterWheat:
		return m_nlwinterwheat->GetFirstDate();
	case tov_NLCabbage:
		return m_nlcabbage->GetFirstDate();
	case tov_NLTulips:
		return m_nltulips->GetFirstDate();
	case tov_NLGrassGrazed1:
		return m_nlgrassgrazed1->GetFirstDate();
	case tov_NLGrassGrazed1Spring:
		return m_nlgrassgrazed1spring->GetFirstDate();
	case tov_NLGrassGrazed2:
		return m_nlgrassgrazed2->GetFirstDate();
	case tov_NLGrassGrazedLast:
		return m_nlgrassgrazedlast->GetFirstDate();
	case tov_NLPermanentGrassGrazed:
		return m_nlpermanentgrassgrazed->GetFirstDate();
	case tov_NLBeetSpring:
		return m_nlbeetspring->GetFirstDate();
	case tov_NLCarrotsSpring:
		return m_nlcarrotsspring->GetFirstDate();
	case tov_NLMaizeSpring:
		return m_nlmaizespring->GetFirstDate();
	case tov_NLPotatoesSpring:
		return m_nlpotatoesspring->GetFirstDate();
	case tov_NLSpringBarleySpring:
		return m_nlspringbarleyspring->GetFirstDate();
	case tov_NLCabbageSpring:
		return m_nlcabbagespring->GetFirstDate();
	case tov_NLCatchPeaCrop:
		return m_nlcatchpeacrop->GetFirstDate();

	case tov_DummyCropPestTesting:
		return m_dummycroppesttesting->GetFirstDate();

	default:
		return 0;
	}
}


TTypesOfVegetation FarmManager::TranslateCropCodes( std::string& astr ) { //changed to farm manager - was a Farm function, 19.03.13
	// This simply checks through the list of legal crop names and returns
	// the correct tov type

	string str = astr;

	// Unfortunately switch cannot use string so the long way:
	if (str == "SpringBarley") return tov_SpringBarley;
	if (str == "SpringBarleySpr") return tov_SpringBarleySpr;
	if (str == "SpringBarleyPTreatment") return tov_SpringBarleyPTreatment;
	if (str == "WinterBarley") return tov_WinterBarley;
	if (str == "SpringWheat") return tov_SpringWheat;
	if (str == "WinterWheat") return tov_WinterWheat;
	if (str == "WinterRye") return tov_WinterRye;
	if (str == "OrchardCrop") return tov_OrchardCrop;
	if (str == "Oats") return tov_Oats;
	if (str == "OOats") return tov_OOats;
	if (str == "Triticale") return tov_Triticale;
	if (str == "OTriticale") return tov_OTriticale;
	if (str == "Maize") return tov_Maize;
	if (str == "MaizeSilage") return tov_MaizeSilage;
	if (str == "SpringBarleySeed") return tov_SpringBarleySeed;
	if (str == "OSpringRape") return tov_SpringRape; /** @todo Create a OSpringRape crop class and management */
	if (str == "SpringRape") return tov_SpringRape;
	if (str == "WinterRape") return tov_WinterRape;
	if (str == "BroadBeans") return tov_BroadBeans;
	if (str == "FieldPeas") return tov_FieldPeas;
	if (str == "FieldPeasSilage") return tov_FieldPeasSilage;
	if (str == "Setaside") return tov_Setaside;
	if (str == "PermanentSetaside") return tov_PermanentSetaside;
	if (str == "SugarBeet") return tov_SugarBeet;
	if (str == "FodderBeet") return tov_FodderBeet;
	if (str == "OFodderBeet") return tov_OFodderBeet;
	if (str == "FodderGrass") return tov_FodderGrass;
	if (str == "CloverGrassGrazed1") return tov_CloverGrassGrazed1;
	if (str == "PotatoesIndustry") return tov_PotatoesIndustry;
	if (str == "Potatoes") return tov_Potatoes;
	if (str == "SeedGrass1") return tov_SeedGrass1;
	if (str == "OWinterBarley") return tov_OWinterBarley;
	if (str == "OWinterBarleyExt") return tov_OWinterBarleyExt;
	if (str == "SpringBarleySilage") return tov_SpringBarleySilage;
	if (str == "OWinterRye") return tov_OWinterRye;
	if (str == "OFieldPeasSilage") return tov_OFieldPeasSilage;
	if (str == "SpringBarleyGrass") return tov_SpringBarleyGrass;
	if (str == "SpringBarleyCloverGrass") return tov_SpringBarleyCloverGrass;
	if (str == "OBarleyPeaCloverGrass") return tov_OBarleyPeaCloverGrass;
	if (str == "OWinterRape") return tov_OWinterRape;
	if (str == "PermanentGrassGrazed") return tov_PermanentGrassGrazed;
	if (str == "PermanentGrassLowYield") return tov_PermanentGrassLowYield;
	if (str == "PermanentGrassTussocky") return tov_PermanentGrassTussocky;
	if (str == "CloverGrassGrazed2") return tov_CloverGrassGrazed2;
	if (str == "SeedGrass2") return tov_SeedGrass2;
	if (str == "OSpringWheat") return tov_OSpringBarley; /** @todo Create a OSpringWheat crop class and management */
	if (str == "OSpringBarley") return tov_OSpringBarley;
	if (str == "OSpringBarleyExt") return tov_OSpringBarleyExt;
	if (str == "OWinterWheat") return tov_OWinterWheat; 
	if (str == "OWinterWheatUndersown") return tov_OWinterWheatUndersown;
	if (str == "OWinterWheatUndersownExt") return tov_OWinterWheatUndersownExt;
	if (str == "OOats") return tov_OOats;
	if (str == "OFieldPeas") return tov_OFieldPeas;
	if (str == "OCloverGrassGrazed1") return tov_OCloverGrassGrazed1;
	if (str == "OGrazingPigs") return tov_OGrazingPigs;
	if (str == "OCarrots") return tov_OCarrots;
	if (str == "Carrots") return tov_Carrots;
	if (str == "OPotatoes") return tov_OPotatoes;
	if (str == "OSeedGrass1") return tov_OSeedGrass1;
	if (str == "OSpringBarleyGrass") return tov_OSpringBarleyGrass;
	if (str == "OSpringBarleyClover") return tov_OSpringBarleyClover;
	if (str == "OPermanentGrassGrazed") return tov_OPermanentGrassGrazed;
	if (str == "OCloverGrassSilage1") return tov_OCloverGrassSilage1;
	if (str == "OCloverGrassGrazed2") return tov_OCloverGrassGrazed2;
	if (str == "OSeedGrass2") return tov_OSeedGrass2;
	if (str == "WWheatPControl") return tov_WWheatPControl;
	if (str == "WWheatPToxicControl") return tov_WWheatPToxicControl;
	if (str == "WWheatPTreatment") return tov_WWheatPTreatment;
	if (str == "AgroChemIndustryCereal") return tov_AgroChemIndustryCereal;
	if (str == "WinterWheatShort") return tov_WinterWheatShort;
	if (str == "WinterWheatStrigling") return tov_WinterWheatStrigling;
	if (str == "WinterWheatStriglingCulm") return tov_WinterWheatStriglingCulm;
	if (str == "WinterWheatStriglingSgl") return tov_WinterWheatStriglingSingle;
	if (str == "SpringBarleyCloverGrassStrigling") return tov_SpringBarleyCloverGrassStrigling;
	if (str == "SpringBarleyStrigling") return tov_SpringBarleyStrigling;
	if (str == "SpringBarleyStriglingSingle") return tov_SpringBarleyStriglingSingle;
	if (str == "SpringBarleyStriglingCulm") return tov_SpringBarleyStriglingCulm;
	if (str == "MaizeStrigling") return tov_MaizeStrigling;
	if (str == "WinterRapeStrigling") return tov_WinterRapeStrigling;
	if (str == "WinterRyeStrigling") return tov_WinterRyeStrigling;
	if (str == "WinterBarleyStrigling") return tov_WinterBarleyStrigling;
	if (str == "FieldPeasStrigling") return tov_FieldPeasStrigling;
	if (str == "SpringBarleyPeaCloverGrassStrigling") return tov_SpringBarleyPeaCloverGrassStrigling;
	if (str == "YoungForest") return tov_YoungForest;
	if (str == "OMaizeSilage") return tov_OMaizeSilage;
	if (str == "OSpringBarleySilage") return tov_OSBarleySilage;
	if (str == "OSpringBarleyPigs") return tov_OSpringBarleyPigs;
	if (str == "NorwegianPotatoes") return tov_NorwegianPotatoes;
	if (str == "NorwegianOats") return tov_NorwegianOats;
	if (str == "NorwegianSpringBarley") return tov_NorwegianSpringBarley;
	if (str == "PlantNursery") return tov_PlantNursery;
	if (str == "PLWinterWheat") return tov_PLWinterWheat;
	if (str == "PLWinterRape") return tov_PLWinterRape;
	if (str == "PLWinterBarley") return tov_PLWinterBarley;
	if (str == "PLWinterTriticale") return tov_PLWinterTriticale;
	if (str == "PLWinterRye") return tov_PLWinterRye;
	if (str == "PLSpringWheat") return tov_PLSpringWheat;
	if (str == "PLSpringBarley") return tov_PLSpringBarley;
	if (str == "PLMaize") return tov_PLMaize;
	if (str == "PLMaizeSilage") return tov_PLMaizeSilage;
	if (str == "PLPotatoes") return tov_PLPotatoes;
	if (str == "PLBeet") return tov_PLBeet;
	if (str == "PLFodderLucerne1") return tov_PLFodderLucerne1;
	if (str == "PLFodderLucerne2") return tov_PLFodderLucerne2;
	if (str == "PLCarrots") return tov_PLCarrots;
	if (str == "PLSpringBarleySpr") return tov_PLSpringBarleySpr;
	if (str == "PLWinterWheatLate") return tov_PLWinterWheatLate;
	if (str == "PLBeetSpr") return tov_PLBeetSpr;
	if (str == "PLBeans") return tov_PLBeans;

	if (str == "NLBeet") return tov_NLBeet;
	if (str == "NLCarrots") return tov_NLCarrots;
	if (str == "NLMaize") return tov_NLMaize;
	if (str == "NLPotatoes") return tov_NLPotatoes;
	if (str == "NLSpringBarley") return tov_NLSpringBarley;
	if (str == "NLWinterWheat") return tov_NLWinterWheat;
	if (str == "NLCabbage") return tov_NLCabbage;
	if (str == "NLTulips") return tov_NLTulips;
	if (str == "NLGrassGrazed1") return tov_NLGrassGrazed1;
	if (str == "NLGrassGrazed1Spring") return tov_NLGrassGrazed1Spring;
	if (str == "NLGrassGrazed2") return tov_NLGrassGrazed2;
	if (str == "NLGrassGrazedLast") return tov_NLGrassGrazedLast;
	if (str == "NLPermanentGrassGrazed") return tov_NLPermanentGrassGrazed;
	if (str == "NLBeetSpring") return tov_NLBeetSpring;
	if (str == "NLCarrotsSpring") return tov_NLCarrotsSpring;
	if (str == "NLMaizeSpring") return tov_NLMaizeSpring;
	if (str == "NLPotatoesSpring") return tov_NLPotatoesSpring;
	if (str == "NLSpringBarleySpring") return tov_NLSpringBarleySpring;
	if (str == "NLCabbageSpring") return tov_NLCabbageSpring;
	if (str == "NLCatchPeaCrop") return tov_NLCatchPeaCrop;

	if (str == "DummyCropPestTesting") return tov_DummyCropPestTesting;

	// No match so issue a warning and quit
	g_msg->Warn( WARN_FILE, "FarmManager::TranslateCropCodes():"" Unknown Crop Code ", str.c_str() );
	exit( 1 );
}


void FarmManager::DumpFarmAreas(){

	//create a text file with the farms
	ofstream ofile ("FarmTotalAreas_almass.txt", ios::out);
	ofile << "Farm no" << '\t' << "Area" << endl;

	//print each farms no and then the numbers of its neighbours
	for(int i=0; i<(int)m_farms.size(); i++){
		OptimisingFarm * opf_i;
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			opf_i =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			ofile << opf_i->Get_almass_no() << '\t';
			ofile << opf_i->GetArea() <<'\t';
			ofile  << endl;
		}
	}
}

ConventionalCattle::ConventionalCattle(FarmManager* a_manager) : Farm(a_manager) // 0
{
  m_farmtype = tof_ConventionalCattle;
  m_stockfarmer = true;
  // Adjust as needed.
  m_rotation.resize( 9 );
  m_rotation[ 0 ] = tov_SpringBarleyCloverGrass;
  m_rotation[ 1 ] = tov_CloverGrassGrazed1;
  m_rotation[ 2 ] = tov_CloverGrassGrazed2;
  m_rotation[ 3 ] = tov_WinterWheat;
  m_rotation[ 4 ] = tov_SpringBarley;
  m_rotation[ 5 ] = tov_SpringBarleyCloverGrass;
  m_rotation[ 6 ] = tov_CloverGrassGrazed1;
  m_rotation[ 7 ] = tov_Maize; // was FodderBeet until 23/12/03
  m_rotation[ 8 ] = tov_SpringBarley;
}


ConventionalPig::ConventionalPig(FarmManager* a_manager) : Farm(a_manager) // 1
{
  m_farmtype = tof_ConventionalPig;
  m_stockfarmer = true;

  // Adjust as needed.
  m_rotation.resize( 9 );
  m_rotation[ 0 ] = tov_WinterRape;
  m_rotation[ 1 ] = tov_WinterWheat;
  m_rotation[ 2 ] = tov_SpringBarley;
  m_rotation[ 3 ] = tov_SpringBarley;
  m_rotation[ 4 ] = tov_Setaside;
  m_rotation[ 5 ] = tov_FieldPeas;
  m_rotation[ 6 ] = tov_WinterWheat;
  m_rotation[ 7 ] = tov_WinterRye;
  m_rotation[ 8 ] = tov_WinterBarley;
}


ConventionalPlant::ConventionalPlant(FarmManager* a_manager) : Farm(a_manager) // 2
{
  m_farmtype = tof_ConventionalPlant;
  m_stockfarmer = false;

  // Adjust as needed.
  m_rotation.resize( 9 );
  m_rotation[ 0 ] = tov_WinterRape;
  m_rotation[ 1 ] = tov_WinterWheat;
  m_rotation[ 2 ] = tov_SpringBarley;
  m_rotation[ 3 ] = tov_SpringBarley;
  m_rotation[ 4 ] = tov_Setaside;
  m_rotation[ 5 ] = tov_FieldPeas;
  m_rotation[ 6 ] = tov_WinterWheat;
  m_rotation[ 7 ] = tov_WinterRye;
  m_rotation[ 8 ] = tov_WinterBarley;
}


OrganicCattle::OrganicCattle(FarmManager* a_manager) : Farm(a_manager) // 3
{
  m_farmtype = tof_OrganicCattle;
  m_stockfarmer = true;
  ReadRotation("OrganicCattle.rot");
  }

OrganicPig::OrganicPig(FarmManager* a_manager) : Farm(a_manager) // 4
{
  m_farmtype = tof_OrganicPig;
  m_stockfarmer = true;
  ReadRotation("OrganicPig.rot");
}

OrganicPlant::OrganicPlant(FarmManager* a_manager) : Farm(a_manager) // 5
{
  m_farmtype = tof_OrganicPlant;
  m_stockfarmer = false;
  ReadRotation("OrganicPlant.rot");
}

PesticideTrialControl::PesticideTrialControl(FarmManager* a_manager) : Farm(a_manager) // 6
{
  m_farmtype = tof_PTrialControl;
  m_stockfarmer = false;
  m_rotation.resize( 1 );
  m_rotation[ 0 ] = tov_WWheatPControl;
}

PesticideTrialToxicControl::PesticideTrialToxicControl(FarmManager* a_manager) : Farm(a_manager) // 7
{
  m_farmtype = tof_PTrialToxicControl;
  m_stockfarmer = false;
  m_rotation.resize( 1 );
  m_rotation[ 0 ] = tov_WWheatPToxicControl;
}

PesticideTrialTreatment::PesticideTrialTreatment(FarmManager* a_manager) : Farm(a_manager) // 8
{
  m_farmtype = tof_PTrialTreatment;
  m_stockfarmer = false;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("PesticideTrialTreatment.rot");
}

ConvMarginalJord::ConvMarginalJord(FarmManager* a_manager ) : Farm(a_manager) // 9
{
  m_farmtype = tof_ConvMarginalJord;
  m_stockfarmer = true;

  m_rotation.resize( 6 );
  m_rotation[ 0 ] = tov_SpringBarleyCloverGrass;
  m_rotation[ 1 ] = tov_CloverGrassGrazed1;
  m_rotation[ 2 ] = tov_CloverGrassGrazed2;
  m_rotation[ 3 ] = tov_SpringBarleyCloverGrass;
  m_rotation[ 4 ] = tov_CloverGrassGrazed1;
  m_rotation[ 5 ] = tov_FodderBeet;
}

AgroChemIndustryCerealFarm1::AgroChemIndustryCerealFarm1(FarmManager* a_manager ) : Farm(a_manager) // 10
{
  m_farmtype = tof_AgroChemIndustryCerealFarm1;
  m_stockfarmer = true;
  // Adjust as needed.
  m_rotation.resize( 9 );
  m_rotation[ 0 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 1 ] = tov_CloverGrassGrazed1;
  m_rotation[ 2 ] = tov_CloverGrassGrazed2;
  m_rotation[ 3 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 4 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 5 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 6 ] = tov_FodderBeet;
  m_rotation[ 7 ] = tov_CloverGrassGrazed1;
  m_rotation[ 8 ] = tov_AgroChemIndustryCereal;
}

AgroChemIndustryCerealFarm2::AgroChemIndustryCerealFarm2(FarmManager* a_manager ) : Farm(a_manager) // 11
{
  m_farmtype = tof_AgroChemIndustryCerealFarm2;
  m_stockfarmer = true;

  // Adjust as needed.
  m_rotation.resize( 3 );
  m_rotation[ 0 ] = tov_WinterRape;
  m_rotation[ 1 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 2 ] = tov_AgroChemIndustryCereal;
  /*  m_rotation[ 3] = tov_AgroChemIndustryCereal; m_rotation[ 4] = tov_Setaside; m_rotation[ 5] = tov_FieldPeas;
  m_rotation[ 6] = tov_AgroChemIndustryCereal; m_rotation[ 7] = tov_WinterRye; m_rotation[ 8] = tov_AgroChemIndustryCereal; */
}



AgroChemIndustryCerealFarm3::AgroChemIndustryCerealFarm3(FarmManager* a_manager ) : Farm(a_manager) // 12
{
  m_farmtype = tof_AgroChemIndustryCerealFarm3;
  m_stockfarmer = false;

  // Adjust as needed.
  m_rotation.resize( 9 );
  m_rotation[ 0 ] = tov_Setaside;
  m_rotation[ 1 ] = tov_FieldPeas;
  m_rotation[ 2 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 3 ] = tov_WinterRye;
  m_rotation[ 4 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 5 ] = tov_WinterRape;
  m_rotation[ 6 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 7 ] = tov_AgroChemIndustryCereal;
  m_rotation[ 8 ] = tov_AgroChemIndustryCereal;
}

NoPesticideBaseFarm::NoPesticideBaseFarm(FarmManager* a_manager ) : Farm(a_manager) // 13
{
  m_farmtype = tof_NoPesticideBase;
  m_stockfarmer = false;

  // Adjust as needed.
  m_rotation.resize( 36 );
  m_rotation[ 0 ] = tov_SpringBarleyCloverGrass;
  m_rotation[ 1 ] = tov_CloverGrassGrazed1;
  m_rotation[ 2 ] = tov_CloverGrassGrazed2;
  m_rotation[ 3 ] = tov_WinterWheat;
  m_rotation[ 4 ] = tov_SpringBarley;
  m_rotation[ 5 ] = tov_SpringBarleyCloverGrass;
  m_rotation[ 6 ] = tov_CloverGrassGrazed1;
  m_rotation[ 7 ] = tov_FodderBeet;
  m_rotation[ 8 ] = tov_SpringBarley;
  m_rotation[ 9 ] = tov_WinterRape;
  m_rotation[ 10 ] = tov_WinterWheat;
  m_rotation[ 11 ] = tov_SpringBarley;
  m_rotation[ 12 ] = tov_SpringBarley;
  m_rotation[ 13 ] = tov_Setaside;
  m_rotation[ 14 ] = tov_FieldPeas;
  m_rotation[ 15 ] = tov_WinterWheat;
  m_rotation[ 16 ] = tov_WinterRye;
  m_rotation[ 17 ] = tov_WinterBarley;
  m_rotation[ 18 ] = tov_WinterRape;
  m_rotation[ 19 ] = tov_WinterWheat;
  m_rotation[ 20 ] = tov_SpringBarley;
  m_rotation[ 21 ] = tov_SpringBarley;
  m_rotation[ 22 ] = tov_Setaside;
  m_rotation[ 23 ] = tov_FieldPeas;
  m_rotation[ 24 ] = tov_WinterWheat;
  m_rotation[ 25 ] = tov_WinterRye;
  m_rotation[ 26 ] = tov_WinterBarley;
  m_rotation[ 27 ] = tov_WinterRape;
  m_rotation[ 28 ] = tov_WinterWheat;
  m_rotation[ 29 ] = tov_SpringBarley;
  m_rotation[ 30 ] = tov_SpringBarley;
  m_rotation[ 31 ] = tov_Setaside;
  m_rotation[ 32 ] = tov_FieldPeas;
  m_rotation[ 33 ] = tov_WinterWheat;
  m_rotation[ 34 ] = tov_WinterRye;
  m_rotation[ 35 ] = tov_WinterBarley;
}

NoPesticideNoPFarm::NoPesticideNoPFarm(FarmManager* a_manager ) : Farm(a_manager) // 14
{
  m_farmtype = tof_NoPesticideNoP;
  m_stockfarmer = false;

  // Adjust as needed.
  m_rotation.resize( 17 );
  m_rotation[ 0 ] = tov_OBarleyPeaCloverGrass;
  m_rotation[ 1 ] = tov_OCloverGrassGrazed1;
  m_rotation[ 2 ] = tov_OCloverGrassGrazed2;
  m_rotation[ 3 ] = tov_OWinterRape;
  m_rotation[ 4 ] = tov_OFieldPeas;
  m_rotation[ 5 ] = tov_OBarleyPeaCloverGrass;
  m_rotation[ 6 ] = tov_OCloverGrassGrazed1;
  m_rotation[ 7 ] = tov_OWinterWheatUndersown;
  m_rotation[ 8 ] = tov_OCloverGrassGrazed1;
  m_rotation[ 9 ] = tov_OCloverGrassGrazed2;
  m_rotation[ 10 ] = tov_OFieldPeas;
  m_rotation[ 11 ] = tov_OBarleyPeaCloverGrass;
  m_rotation[ 12 ] = tov_OCloverGrassGrazed1;
  m_rotation[ 13 ] = tov_Setaside;
  m_rotation[ 14 ] = tov_OFieldPeas;
  m_rotation[ 15 ] = tov_OWinterRye;
  m_rotation[ 16 ] = tov_OFieldPeas;
}


UserDefinedFarm1::UserDefinedFarm1(FarmManager* a_manager ) : Farm(a_manager) // 15
{
  m_farmtype = tof_UserDefinedFarm1;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file UserDefinedFarm1.rot
  ReadRotation("UserDefinedFarm1.rot");
 }

UserDefinedFarm2::UserDefinedFarm2(FarmManager* a_manager ) : Farm(a_manager) // 15
{
  m_farmtype = tof_UserDefinedFarm2;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file UserDefinedFarm1.rot
  ReadRotation("UserDefinedFarm2.rot");
 }

UserDefinedFarm3::UserDefinedFarm3(FarmManager* a_manager ) : Farm(a_manager) // 17
{
  m_farmtype = tof_UserDefinedFarm3;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("UserDefinedFarm3.rot");
}

UserDefinedFarm4::UserDefinedFarm4(FarmManager* a_manager ) : Farm(a_manager) // 18
{
  m_farmtype = tof_UserDefinedFarm4;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("UserDefinedFarm4.rot");
 }

UserDefinedFarm5::UserDefinedFarm5(FarmManager* a_manager ) : Farm(a_manager) // 19
{
  m_farmtype = tof_UserDefinedFarm5;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("UserDefinedFarm5.rot");
}

UserDefinedFarm6::UserDefinedFarm6(FarmManager* a_manager ) : Farm(a_manager) // 20
{
  m_farmtype = tof_UserDefinedFarm6;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("UserDefinedFarm6.rot");
}

UserDefinedFarm7::UserDefinedFarm7(FarmManager* a_manager ) : Farm(a_manager) // 21
{
  m_farmtype = tof_UserDefinedFarm7;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("UserDefinedFarm7.rot");
}

UserDefinedFarm8::UserDefinedFarm8(FarmManager* a_manager ) : Farm(a_manager) // 22
{
  m_farmtype = tof_UserDefinedFarm8;
  m_stockfarmer = true;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("UserDefinedFarm8.rot");
}

UserDefinedFarm9::UserDefinedFarm9(FarmManager* a_manager ) : Farm(a_manager) // 23
{
  m_farmtype = tof_UserDefinedFarm9;
  m_stockfarmer = false;
  // This farm type reads its rotation from a special file PesticideTrialTreatment.rot
  ReadRotation("UserDefinedFarm9.rot");
}

UserDefinedFarm10::UserDefinedFarm10(FarmManager* a_manager ) : Farm(a_manager) // 24
{
  m_farmtype = tof_UserDefinedFarm10;
  m_stockfarmer = false;
  ReadRotation("UserDefinedFarm10.rot");
}

UserDefinedFarm11::UserDefinedFarm11(FarmManager* a_manager ) : Farm(a_manager) // 25
{
  m_farmtype = tof_UserDefinedFarm11;
  m_stockfarmer = false;
  ReadRotation("UserDefinedFarm11.rot");
}

UserDefinedFarm12::UserDefinedFarm12(FarmManager* a_manager ) : Farm(a_manager) // 26
{
  m_farmtype = tof_UserDefinedFarm12;
  m_stockfarmer = false;
  ReadRotation("UserDefinedFarm12.rot");
}

UserDefinedFarm13::UserDefinedFarm13(FarmManager* a_manager ) : Farm(a_manager) // 27
{
  m_farmtype = tof_UserDefinedFarm13;
  m_stockfarmer = false;
  ReadRotation("UserDefinedFarm13.rot");
}

UserDefinedFarm14::UserDefinedFarm14( FarmManager* a_manager ) : Farm(a_manager) // 28
{
  m_farmtype = tof_UserDefinedFarm14;
  m_stockfarmer = true;
  ReadRotation("UserDefinedFarm14.rot");
}

UserDefinedFarm15::UserDefinedFarm15(FarmManager* a_manager) : Farm(a_manager) // 29
{
  m_farmtype = tof_UserDefinedFarm15;
  m_stockfarmer = false;
  ReadRotation("UserDefinedFarm15.rot");
}

UserDefinedFarm16::UserDefinedFarm16(FarmManager* a_manager ) : Farm(a_manager) // 30
{
  m_farmtype = tof_UserDefinedFarm16;
  m_stockfarmer = false;
  ReadRotation("UserDefinedFarm16.rot");
}


UserDefinedFarm17::UserDefinedFarm17(FarmManager* a_manager ) : Farm(a_manager) // 31
{
  m_farmtype = tof_UserDefinedFarm17;
  m_stockfarmer = false;
  ReadRotation("UserDefinedFarm17.rot");
}

UserDefinedFarm::UserDefinedFarm( const char* fname, FarmManager* a_manager ) : Farm(a_manager) // Base class for the real ones
{
	/**
    User defined farms derived from this class have greater functionality and greater flexibility than those defined from
    Farm. There are three areas of added functionality:\n
    1) The ability to define whether the farm is arable or stock\n
	2) The ability to define farm intensity \n
    3) The capability of defining permanent crops (e.g. permanent grass or silage maize).
    This means that whatever field is assigned this crop in the first year will retain it subsequently. Further functionality
    here is that the ideal crop area can also be specified. If so then the farmer will identify the field closest to that
    area and assign the crop to that.\n
    */
    ifstream ifile;
    ifile.open(fname,ios::in);
	if (!ifile.is_open()) {
		g_msg->Warn( WARN_FILE, " Unable to open file ", fname );
		exit( 1 );
    }
	int input, intensity;
	// First entry - arable/stock
	// Second entry - farm intensity. Zero = default, 1 extensive, 2 very extensive
	ifile >> input >> intensity;
	if (input==1) m_stockfarmer = true; else m_stockfarmer = false;
	// Thrid entry - no permanent crops
	int permcrops;
	std::string cropref ="";
	ifile >> permcrops;
	// The problem here is that at the time when we reach this code there are no fields attached to the farm - so we need
	// to store this information - hence the vector of the vector of PermCropData
	PermCropData pcd;
	for (int i=0; i<permcrops; i++) {
		int pct;
		ifile >> cropref;
		TTypesOfVegetation tov = g_farmmanager->TranslateCropCodes( cropref );
		ifile >> pct; // 0-100%
		pcd.Pct=pct;
		pcd.Tov=tov;
		m_PermCrops.push_back(pcd);
	}
	// 4th entry
  int nocrops;
  ifile >> nocrops;
  m_rotation.resize( nocrops );
  for ( int i = 0; i < nocrops; i++ ) {
    ifile >> cropref;
    TTypesOfVegetation tov = g_farmmanager->TranslateCropCodes( cropref );
    m_rotation[ i ] = tov;
  }
  ifile.close();
}

/** \brief Used to assign a permanent crop to an otherwise rotational field polygon */
void Farm::AssignPermanentCrop(TTypesOfVegetation a_tov, int a_pct) {
	// Assumes that m_fields has all the fields already in it, and those that are occupied by permanent tole types
	// are denoted by -2 or -4 in rotindex, -1 is unassigned - can't use enum because positive values have other meanings
	
	// Here we can save some time if we have to allocate all the fields to one permanent crop
	if (a_pct == 100)
	{
		for (auto cfi = m_fields.begin(); cfi != m_fields.end(); ++cfi)
		{
			// Set the field as non-rotating
			(*cfi)->SetRotIndex(-4);
			// Assign the crop, and weeds are assumed to be undefined
			(*cfi)->SetVegType(a_tov, tov_Undefined);
		}
		return;
	}

	tpct a_tpct;
	// create a copy of the field vector and call it fields_cpy
	vector<LE*> fields_cpy;
	fields_cpy.resize(m_fields.size());
	copy(m_fields.begin(), m_fields.end(), fields_cpy.begin());
	vector<tpct> pcts;
	double area = 0.0;

	
	// First get the areas of all fields and then convert these to percentages
	int sz = (int) fields_cpy.size();
	// loop through the fields vector and remove any fields already assigned to permanent crops
	for (int i = sz - 1; i >= 0; i--) {
		if (fields_cpy[i]->GetRotIndex() < -1) {
			fields_cpy.erase(fields_cpy.begin() + i);
		}
	}
	// sum up the area
	for (auto cfi = fields_cpy.begin(); cfi != fields_cpy.end(); ++cfi) area += (*cfi)->GetArea();
	
	// Here we can take action if we only have a single field 
	int fnos = (int) fields_cpy.size();
	if (fnos<1) return; // No fields to allocate, jump out
	else if (fnos==1) {
		// Use the pct as the chance that our single field is assigned
		if (random(100)>=a_pct) return;
		else {
			// Add field by default
			fields_cpy[0]->SetRotIndex(-4);
			// Assign the crop
			fields_cpy[0]->SetVegType(a_tov, tov_Undefined);
			return;
		}
	}
	// By here we have more than one field to potentially allocate to
	for (int i=0; i<fnos; i++) {
		a_tpct.pct = (int) floor(0.5+((fields_cpy[i]->GetArea()/area)*100));
		a_tpct.index = i;
		pcts.push_back(a_tpct);
	}
	// We need to look for combinations of fields that are close to our target.
	// First we can sort the array and ignore any that are greater than pct
    sort (pcts.begin(), pcts.end(), CompPcts); // results in ordering on increasing pct
	// now remove all those with pct>target+10% to save loads of time in the inverse integer partition
	int index=-1;
	int ind=0;
	int apct=a_pct+10; // Lets assume we have a tolerance of 10%
	while ((index==-1) && (ind< (int)pcts.size())) {
		if (pcts[ind++].pct>apct) index=ind;
	}
	if (index!=-1) pcts.erase(pcts.begin()+index,pcts.end());
	// Now find the best combination of fields to get close to our target & set them
	int bits = InvIntPartition(&pcts,a_pct);
	int mask=1;
	ind = 0;
	int used=0;
	//double pctused = 0.0;
	double check = 0.0;
	for (int h=0; h< (int)pcts.size(); h++)	check+=fields_cpy[pcts[h].index]->GetArea();
	while (bits>0) {
		if ((bits & mask) > 0) {
			// Set the field as non-rotating
			fields_cpy[pcts[ind].index]->SetRotIndex(-4);
			// Assign the crop
			fields_cpy[pcts[ind].index]->SetVegType(a_tov, tov_Undefined);
			used += (int) fields_cpy[pcts[ind].index]->GetArea();
			//pctused = used/area;
			bits -= mask;
		}
		mask = mask << 1;
		ind++;
	}
}

/** \brief Finds all possible sums of the integers in the items array */
int Farm::InvIntPartition(vector<tpct>* items, int target){
	//Figure out how many bitmasks we need...
	//4 bits have a maximum value of 15, so we need 15 masks.
	//Calculated as:
	//    (2 ^ ItemCount) - 1
	long long int sz = (long long int) items->size();
	if (sz>80) {
		// This is a warning that we did no consider all combinations of fields but 2^63 combinations should be enough :)
		g_msg->Warn("Too many potential fields in UserDefinedFarm::InvIntPartition: ",sz);
		g_msg->Warn("Farm Number: ",GetFarmNumber());
		sz = 63;
	}
	long int calcs = (1 << sz);
 	//Spit out the corresponding calculation for each bitmask
	int sum;
	int found = 0;
	int diff = 100;
	for (long int i=1; i<calcs; i++) {
		//Get the items from our array that correspond to
		//the on bits in our mask
		sum = 0;
		int mask = 1;
		for (int bit=0; bit<sz; bit++) {
			if ((i & mask) > 0) {
				sum+=(*items)[bit].pct;
			}
			mask = mask << 1;
		}

		if (abs(sum-target) < diff ){
			found = i;
			diff = abs(sum-target);
			if (diff<1) break; //added 01.11.12 to prevent from checking all the combinations when the right one is found
		}
	}
	return found;
}

UserDefinedFarm18::UserDefinedFarm18( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 32
{
  m_farmtype = tof_UserDefinedFarm18;
}

UserDefinedFarm19::UserDefinedFarm19( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 33
{
  m_farmtype = tof_UserDefinedFarm19;
}

UserDefinedFarm20::UserDefinedFarm20( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 34
{
  m_farmtype = tof_UserDefinedFarm20;
}

UserDefinedFarm21::UserDefinedFarm21( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 35
{
  m_farmtype = tof_UserDefinedFarm21;
}

UserDefinedFarm22::UserDefinedFarm22( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 36
{
  m_farmtype = tof_UserDefinedFarm22;
}

UserDefinedFarm23::UserDefinedFarm23( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 37
{
  m_farmtype = tof_UserDefinedFarm23;
}

UserDefinedFarm24::UserDefinedFarm24( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 38
{
  m_farmtype = tof_UserDefinedFarm24;
}

UserDefinedFarm25::UserDefinedFarm25( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 39
{
  m_farmtype = tof_UserDefinedFarm25;
}

UserDefinedFarm26::UserDefinedFarm26( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 40
{
  m_farmtype = tof_UserDefinedFarm18;
}

UserDefinedFarm27::UserDefinedFarm27( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 41
{
  m_farmtype = tof_UserDefinedFarm27;
}

UserDefinedFarm28::UserDefinedFarm28( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 42
{
  m_farmtype = tof_UserDefinedFarm28;
}

UserDefinedFarm29::UserDefinedFarm29( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 43
{
  m_farmtype = tof_UserDefinedFarm29;
}

UserDefinedFarm30::UserDefinedFarm30( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 44
{
  m_farmtype = tof_UserDefinedFarm30;
}

UserDefinedFarm31::UserDefinedFarm31( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 45
{
  m_farmtype = tof_UserDefinedFarm31;
}

UserDefinedFarm32::UserDefinedFarm32( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 46
{
  m_farmtype = tof_UserDefinedFarm32;
}

UserDefinedFarm33::UserDefinedFarm33( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 47
{
  m_farmtype = tof_UserDefinedFarm33;
}

UserDefinedFarm34::UserDefinedFarm34( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 48
{
  m_farmtype = tof_UserDefinedFarm34;
}

UserDefinedFarm35::UserDefinedFarm35( const char* fname, FarmManager* a_manager ) : UserDefinedFarm(fname, a_manager) // 49
{
  m_farmtype = tof_UserDefinedFarm35;
}


//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

FarmManager::FarmManager()
{

	g_farmmanager = this;

	if ( l_map_read_farmfile.value() ) {
		if (cfg_OptimisingFarms.value()){
			//make a data object
			pm_data=new DataForOptimisation;
			ReadFarmsData();
			ReadLivestockNumbers();
			ReadFarmsData_perFarmType();
			ReadLivestockData();
			//ReadGlobalData();
			ReadLookupTable();
			ofstream ofile ("Economic_figures.txt", ios::out);
			ofile.close();		
			ofstream ofile1 ("Daydegrees.txt", ios::out);
			ofile1.close();
			if(cfg_MaizeEnergy.value()){
				ofstream ofileEM ("Maize_energy_price_yearly.txt", ios::out);
				ofileEM.close();
			}

			if (cfg_OptimiseBedriftsmodelCrops.value()){
				ReadInitialCropAreas();
				ReadCropsData();
				ReadCropsData_perSoilType();
				ReadCropsData_perFarmType();
				ReadCropsData_perFarm_and_Soil();
				ReadCropsData_perFarm_and_Soil_and_Size();
			}
			else{
				ReadInitialCropAreas_almass();
				ReadCropsData_almass();
				ReadCropsData_perSoilType_almass();
				ReadCropsData_perFarmType_almass();
				ReadCropsData_perFarm_and_Soil_almass();
				ReadCropsData_perFarm_and_Soil_and_Size_almass();
				m_crops_summary_BIs.resize(pm_data->Get_noCrops()); //no_crops - from the input file on initial crop areas
				m_cropTotals_sum.resize(pm_data->Get_noCrops());
				m_cropTotals_plant_sum.resize(pm_data->Get_noCrops());
				m_cropTotals_pig_sum.resize(pm_data->Get_noCrops());
				m_cropTotals_cattle_sum.resize(pm_data->Get_noCrops());
				m_cropTotals_other_sum.resize(pm_data->Get_noCrops());
				m_crops_fertilizer.resize(pm_data->Get_noCrops());
				m_crops_fertilizer_trade.resize(pm_data->Get_noCrops());
				m_crop_areas_stages.resize( 2 * pm_data->Get_noCrops()); //use just the results of the baseline and original model
				for(int i=0; i<pm_data->Get_noCrops(); i++){
					m_crops_summary_BIs[i].Tov = pm_data->Get_cropTypes_almass(i);
					m_crops_summary_BIs[i].BI=0;
					m_crops_summary_BIs[i].BIherb=0;
					m_crops_summary_BIs[i].BIfi=0;
					m_cropTotals_sum[i]=0;
					m_cropTotals_plant_sum[i]=0;
					m_cropTotals_pig_sum[i]=0;
					m_cropTotals_cattle_sum[i]=0;
					m_cropTotals_other_sum[i]=0;
					m_crops_fertilizer[i]=0;
					m_crops_fertilizer_trade[i]=0;
				}

				if(cfg_MaizeEnergy.value()){
				  ReadEnergyMaizePrice();
				}
			}
		}//if optimising farm
		CreateFarms( l_map_farmref_file.value() );
	}

}


FarmManager::~FarmManager()
{

	for ( unsigned int i = 0; i < m_farms.size(); i++ )
		delete m_farms[ i ];
	if (cfg_OptimisingFarms.value()) delete pm_data;
}

void FarmManager::FarmManagement()
{
	for ( unsigned int i = 0; i < m_farms.size(); i++ )
	{
		m_farms[ i ]->Management();
	}

	//050214
	if(g_landscape_p->SupplyYearNumber()>0){
		if(g_landscape_p->SupplyDayInYear() == cfg_Animals_number_test_day.value()){ //check if this is the day we want to collect data on animal numbers
			for ( unsigned int i = 0; i < m_farms.size(); i++ )
			{
				OptimisingFarm * opf;
				if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
					opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
					if(cfg_AnimalsThreshold.value() > 0 && opf->Get_main_goal() == tofg_environment){ //take only environmentalists and check if we are in the feedback mode (no if the config is zero)
						int farm_ref = opf->GetFarmNumber();
						int animals_no = g_landscape_p->SupplyFarmAnimalCensus(farm_ref, cfg_LifeStage.value());
						opf->Set_animals_no(animals_no);
					}
				}
			}
		}
	}
}

void FarmManager::InitiateManagement()
{
	for ( unsigned int i = 0; i < m_farms.size(); i++ )
	{
		m_farms[ i ]->InitiateManagement();
	}
}

void FarmManager::AddField(int a_OwnerIndex, LE* a_newland, int a_Owner)
{
	m_farms[ a_OwnerIndex ]->AddField( a_newland );
	a_newland->SetOwner( m_farms[ a_OwnerIndex ], a_Owner, a_OwnerIndex );
}

void FarmManager::RemoveField(int a_OwnerIndex, LE* a_field)
{
	m_farms[ a_OwnerIndex ]->RemoveField( a_field );
}

int FarmManager::ConnectFarm( int a_owner )
{
  for ( unsigned int i = 0; i < m_farms.size(); i++ )
  {
    if ( a_owner == m_farms[ i ]->GetFarmNumber() )
	{
      // Found it. Set mapping and return.
      return i;
    }
  }
  // We didn't find the owner in the list of farms,
  // pregenerated in CreateFarms() above. Something
  // is not correct here, so raise an appropriate
  // error and exit.
  char error_num[ 20 ];
  sprintf( error_num, "%d", a_owner );
  g_msg->Warn( WARN_FILE, "FarmManager::ConnectFarm(): Unknown farm number"" referenced in polygon file:", error_num );
  exit( 1 );
}

void FarmManager::CreateFarms( const char * a_farmfile ) 
{
	int No, FType, NoFarms;
	FILE * inpf = fopen(a_farmfile, "r" );
	if (!inpf) {
		g_msg->Warn( WARN_FILE, "Landscape::CreateFarms(): Unable to open file", a_farmfile );
		exit( 1 );
	}

	fscanf( inpf, "%d", & NoFarms );

	m_farms.resize( NoFarms );

	m_farmmapping_lookup = new int[NoFarms * 2];

	for (int i = 0; i < NoFarms; i++)
	{
		// File format:
		//
		// Two colunms of numbers 0..number of farms, 0-number of farmtypes-1
		// the second column determines what type of farm we have
		fscanf(inpf, "%d %d", &No, &FType);
		m_farmmapping_lookup[i * 2] = No;
		m_farmmapping_lookup[i * 2 + 1] = FType;
	}
	fclose(inpf);

	for (int i = 0; i < NoFarms; i++)
	{
		/*
		//If we are testing crop management, then ignore farm type from
		// the file and set to fixed one instead.
		if ( g_farm_test_crop.value() ) {
		FType = g_farm_test_crop_farmtype.value();
		}
		*/
		// If we are running in fixed, sync'ed rotation mode, set all farms to
		// be of the requested type.
		if (g_farm_fixed_rotation_enable.value()) {
			FType = g_farm_fixed_rotation_farmtype.value();
		}

		switch (m_farmmapping_lookup[i * 2 + 1]) // FType
		{
		case 0:
			m_farms[i] = new ConventionalCattle(this);
			break;
		case 1:
			m_farms[i] = new ConventionalPig(this);
			break;
		case 2:
			m_farms[i] = new ConventionalPlant(this);
			break;
		case 3:
			m_farms[i] = new OrganicCattle(this);
			break;
		case 4:
			m_farms[i] = new OrganicPig(this);
			break;
		case 5:
			m_farms[i] = new OrganicPlant(this);
			break;
		case 6:
			m_farms[i] = new PesticideTrialControl(this);
			break;
		case 7:
			m_farms[i] = new PesticideTrialToxicControl(this);
			break;
		case 8:
			m_farms[i] = new PesticideTrialTreatment(this);
			break;
		case 9:
			m_farms[i] = new ConvMarginalJord(this);
			break;
		case 10:
			m_farms[i] = new AgroChemIndustryCerealFarm1(this);
			break;
		case 11:
			m_farms[i] = new AgroChemIndustryCerealFarm2(this);
			break;
		case 12:
			m_farms[i] = new AgroChemIndustryCerealFarm3(this);
			break;
		case 13:
			m_farms[i] = new NoPesticideBaseFarm(this);
			break;
		case 14:
			m_farms[i] = new NoPesticideNoPFarm(this);
			break;
		case 15:
			m_farms[i] = new UserDefinedFarm1(this);
			break;
		case 16:
			m_farms[i] = new UserDefinedFarm2(this);
			break;
		case 17:
			m_farms[i] = new UserDefinedFarm3(this);
			break;
		case 18:
			m_farms[i] = new UserDefinedFarm4(this);
			break;
		case 19:
			m_farms[i] = new UserDefinedFarm5(this);
			break;
		case 20:
			m_farms[i] = new UserDefinedFarm6(this);
			break;
		case 21:
			m_farms[i] = new UserDefinedFarm7(this);
			break;
		case 22:
			m_farms[i] = new UserDefinedFarm8(this);
			break;
		case 23:
			m_farms[i] = new UserDefinedFarm9(this);
			break;
		case 24:
			m_farms[i] = new UserDefinedFarm10(this);
			break;
		case 25:
			m_farms[i] = new UserDefinedFarm11(this);
			break;
		case 26:
			m_farms[i] = new UserDefinedFarm12(this);
			break;
		case 27:
			m_farms[i] = new UserDefinedFarm13(this);
			break;
		case 28:
			m_farms[i] = new UserDefinedFarm14(this);
			break;
		case 29:
			m_farms[i] = new UserDefinedFarm15(this);
			break;
		case 30:
			m_farms[i] = new UserDefinedFarm16(this);
			break;
		case 31:
			m_farms[i] = new UserDefinedFarm17(this);
			break;
			// NB the user defing farms below require an extra parameter in the rotation file denoting the intensity (0 or 1 = high low)
		case 32:
			m_farms[i] = new UserDefinedFarm18("UserDefinedFarm18.rot", this);
			break;
		case 33:
			m_farms[i] = new UserDefinedFarm19("UserDefinedFarm19.rot", this);
			break;
		case 34:
			m_farms[i] = new UserDefinedFarm20("UserDefinedFarm20.rot", this);
			break;
		case 35:
			m_farms[i] = new UserDefinedFarm21("UserDefinedFarm21.rot", this);
			break;
		case 36:
			m_farms[i] = new UserDefinedFarm22("UserDefinedFarm22.rot", this);
			break;
		case 37:
			m_farms[i] = new UserDefinedFarm23("UserDefinedFarm23.rot", this);
			break;
		case 38:
			m_farms[i] = new UserDefinedFarm24("UserDefinedFarm24.rot", this);
			break;
		case 39:
			m_farms[i] = new UserDefinedFarm25("UserDefinedFarm25.rot", this);
			break;
		case 40:
			m_farms[i] = new UserDefinedFarm26("UserDefinedFarm26.rot", this);
			break;
		case 41:
			m_farms[i] = new UserDefinedFarm27("UserDefinedFarm27.rot", this);
			break;
		case 42:
			m_farms[i] = new UserDefinedFarm28("UserDefinedFarm28.rot", this);
			break;
		case 43:
			m_farms[i] = new UserDefinedFarm29("UserDefinedFarm29.rot", this);
			break;
		case 44:
			m_farms[i] = new UserDefinedFarm30("UserDefinedFarm30.rot", this);
			break;
		case 45:
			m_farms[i] = new UserDefinedFarm31("UserDefinedFarm31.rot", this);
			break;
		case 46:
			m_farms[i] = new UserDefinedFarm32("UserDefinedFarm32.rot", this);
			break;
		case 47:
			m_farms[i] = new UserDefinedFarm33("UserDefinedFarm33.rot", this);
			break;
		case 48:
			m_farms[i] = new UserDefinedFarm34("UserDefinedFarm34.rot", this);
			break;
		case 49:
			m_farms[i] = new UserDefinedFarm35("UserDefinedFarm35.rot", this);
			break;
		case 50: //optimising farm!
			if ((pm_data->Get_farmType(No)) == toof_Pig) {
				m_farms[i] = new OptimisingPigFarm(this, No);
			}
			else if ((pm_data->Get_farmType(No)) == toof_Cattle){
				m_farms[i] = new OptimisingCattleFarm(this, No);
			}
			else if ((pm_data->Get_farmType(No)) == toof_Plant){
				m_farms[i] = new OptimisingPlantFarm(this, No);
			}
			else if ((pm_data->Get_farmType(No)) == toof_Other){
				m_farms[i] = new OptimisingOtherFarm(this, No);
			}
			break;

		default:
			g_msg->Warn(WARN_FILE, "Landscape::CreateFarms(): Unknown farm type reference number: ", m_farmmapping_lookup[i * 2 + 1]);
			exit(1);
		}
		m_farms[i]->SetFarmNumber(i); // We use 'i' here because we renumber the farms internally. If the file did not come in this way then a dump file is created - tested in ReadPolys2
	}

	if(cfg_OptimisingFarms.value())
	{
		DistributeFarmerTypes(); //added 240513
	}

	if (GetFarmNoLookup(NoFarms - 1) != NoFarms - 1) m_renumbered = false; else m_renumbered = true;
}

void FarmManager::DumpFarmrefs()
{
	ofstream opf("dump_farmrefs.txt");
	if (!opf.is_open())
	{
		g_msg->Warn(WARN_FILE, "Landscape::CreateFarms(): Unable to open file", "dump_farmrefs.txt");
		exit(1);
	}
	int NoFarms = (int) m_farms.size();
	opf << NoFarms << endl;
	for (int i = 0; i < NoFarms; i++)
	{
		opf << i << '\t' << m_farmmapping_lookup[i * 2 + 1] << endl;
	}
	opf.close();
}

void FarmManager::InitFarms(){


if(cfg_OptimisingFarms.value()){
	Create_Output_file_for_farms(); //needed only for the OpitmisingFarms!
}

for(unsigned j=0; j<m_farms.size(); j++){
	if(m_farms[j]->GetType()==tof_OptimisingFarm){
		OptimisingFarm * opf =  dynamic_cast<OptimisingFarm*>(m_farms[j]);
		opf->Init(pm_output_file);
	  }
  }

OptimisationOutput(); //03.05 - outputs of the optimisation

}


double FarmManager::GetSpilledGrain()
{
	/*
	* This data is grain distributions based on 2013 & 2014 data from cereal fields in Jutland counted in September.
	* Counts made in squares of 0.0196 m2 and scaled up, hence the decimal numbers.
	* The numbers represent number of grains.
	*/
	double graindist2013[26] = {
		29.59, 172.68, 60.59, 39.68, 51.02, 81.63, 268.71, 134.84, 57.40, 30.61, 204.08, 683.67, 108.04,
		141.29, 505.10, 444.61, 293.37, 355.18, 386.90, 381.83, 372.45, 377.55, 320.70, 392.46, 392.86, 435.17
	};
	double graindist2014[28] = {
		109.33, 382.65, 94.19, 765.31, 29.15, 70.15, 1096.94, 436.51, 309.21, 286.28, 480.44, 249.73, 784.10,
		688.78, 2035.45, 920.80, 341.61, 12.24, 113.38, 80.17, 178.57, 480.44, 0.00, 2.83, 1447.12, 1846.94, 1017.86,
		477.74
	};
	int grain_dist_value = cfg_grain_distribution.value();
	if (grain_dist_value == 0)  // The default - random pick between the two years
	{
		if (m_SpilledGrain) return graindist2013[(int)(g_rand_uni() * 26)];
		else return graindist2014[(int)(g_rand_uni() * 28)];
	}
	if (grain_dist_value == 1) {
		return graindist2013[(int)(g_rand_uni() * 26)];
	}
	else
	{
		return graindist2014[(int)(g_rand_uni() * 28)];
	}
}

double FarmManager::GetSpilledMaize()
{
	/*
	* This data is maize distributions in kJ/m2 based on 2015, 2016 & 2017 field data from maize fields in Jutland
	*/
	double maizedist[15] = {
		102.7905327,
		58.19878648,
		85.65877728,
		110.9055748,
		30.65682555,
		63.11699379,
		59.05947276,
		41.9277173,
		95.57716202,
		14.42674144,
		6.311699379,
		20.73844082,
		12.62339876,
		25.24679751,
		146.070757};
	return maizedist[(int)(g_rand_uni() * 15)];
}

bool FarmManager::InIllegalList( int a_farm_ref, vector<int> * a_farmlist ) {
	/**
	* a_farmlist is a pointer to a list of integers with illegal farm ref numbers. The first entry lists the number of illegal numbers.
	*/
	unsigned sz = (unsigned) a_farmlist->size();
	for (unsigned f = 0; f < sz; f++) {
		if ((*a_farmlist)[f] == a_farm_ref) return true;
	}
	return false;
}

void FarmManager::AddToIllegalList( int a_farm_ref, vector<int> * a_farmlist ) {
	/**
	* a_farmlist is a pointer to a list of integers with illegal farm ref numbers.
	*/
	bool found = false;
	unsigned sz = (unsigned)a_farmlist->size();
	for (unsigned f = 0; f < sz; f++) {
		if ((*a_farmlist)[f] == a_farm_ref) {
			found = true;
			break;
		}
	}
	if (!found) {
		a_farmlist->push_back( a_farm_ref );
	}
}

bool FarmManager::IsDuplicateRef(int a_ref, HunterInfo* a_hinfo)
{
	for (int i = 0; i < int( a_hinfo->FarmHuntRef.size() ); i++)
	{
		
		if (a_ref == a_hinfo->FarmHuntRef[ i ]) return true;
	}
	return false;
}

int FarmManager::FindClosestFarm(HunterInfo a_hinfo, vector<int> * a_farmlist) {
	/**
	* Centroid calculation on farms must be called before calling this method for the first time. \n
	* a_farmlist is a pointer to a list of integers with illegal farm ref numbers. The first entry lists the number of illegal numbers.
	*/
	double best = 99999999999999999.0;
	int bestref = -1;
	double dist = best;
	for (unsigned i = 0; i < m_farms.size(); i++) {
		int fnum = m_farms[i]->GetFarmNumber();
		if (!InIllegalList(fnum, a_farmlist)) {
			if (!IsDuplicateRef(fnum, &a_hinfo)) {
				// Is possible to use this farm, so test it.
				APoint FPt = m_farms[i]->GetCentroids();
				dist = sqrt((double( FPt.m_x - a_hinfo.homeX ) * double( FPt.m_x - a_hinfo.homeX ) + double( FPt.m_y - a_hinfo.homeY ) * double( FPt.m_y - a_hinfo.homeY )));
 				if (dist < best) {
					best = dist;
					bestref = fnum;
				}
			}
		}
	}
	if (bestref == -1) {
		g_msg->Warn( "FarmManager::FindClosestFarm - Cannot find open farm.", "" );
		exit( 0 );
	}
	return bestref;
}

int FarmManager::FindClosestFarmOpenness( HunterInfo a_hunterinfo, vector<int> * a_farmlist, int a_openness ) {
	// Centroid calculation on farms must be called before calling this method for the first time
	double best = 99999999999999999.0;
	int bestref = -1;
	double dist = best;
	for (unsigned i = 0; i < m_farms.size(); i++) {
		if (m_farms[ i ]->GetMaxOpenness() > a_openness) {
			int fref = m_farms[ i ]->GetFarmNumber();
			if (!InIllegalList( fref, a_farmlist )) {
				if (!IsDuplicateRef( fref, &a_hunterinfo )) {
					APoint FPt = m_farms[ i ]->GetCentroids();
					dist = sqrt( (double( FPt.m_x - a_hunterinfo.homeX ) * double( FPt.m_x - a_hunterinfo.homeX ) + double( FPt.m_y - a_hunterinfo.homeY ) * double( FPt.m_y - a_hunterinfo.homeY )) );
					if (dist < best) {
						best = dist;
						bestref = fref;
					}
				}
			}
		}
	}
	if (bestref == -1) {
		g_msg->Warn( "FarmManager::FindClosestFarmOpenness( ) - Cannot find open farm.", "" );
		exit( 0 );
	}
	return bestref;
}

int FarmManager::FindClosestFarmOpennessProb( HunterInfo a_hunterinfo, vector<int> * a_farmlist, int a_openness ) {
	/**
	* Centroid calculation on farms must be called before calling this method for the first time. \n
	* a_farmlist is a pointer to a list of integers with illegal farm ref numbers. The first entry lists the number of illegal numbers.
	* The chance of acceptance is probability based, taking the nearest farm first then testing the next etc.. The probability is based on y = 1*EXP(-x*cfg)
	* with cfg being a fitting parameter in cfg_ClosestFarmProbParam1
	*/

	/** We use typedef here to create our own name for APoint - but it is just two unsigned ints. We will use 'x' for the farm num and 'y' for the distance. */
	typedef  APoint AFarmDist;
	/** We use a vector of AFarmDists to be able to sort it easily */
	vector <AFarmDist> farmdists;
	/** \brief Struct redefining operator < - used for sorting distances from smallest to largest.*/
	struct FarmDistSort {
		bool operator()( AFarmDist a, AFarmDist b ) {
			return a.m_y < b.m_y;
		}
	};
	for (unsigned i = 0; i < m_farms.size(); i++) {
		int fnum = m_farms[ i ]->GetFarmNumber();
		if (!InIllegalList( fnum, a_farmlist )) {
			if (m_farms[ i ]->GetMaxOpenness() > a_openness) {
				if (!IsDuplicateRef( fnum, &a_hunterinfo )) {
					// Is possible to use this farm, so test it.
					APoint FPt = m_farms[ i ]->GetCentroids();
					int dist = int(sqrt( (double( FPt.m_x - a_hunterinfo.homeX ) * double( FPt.m_x - a_hunterinfo.homeX ) + double( FPt.m_y - a_hunterinfo.homeY ) * double( FPt.m_y - a_hunterinfo.homeY )) ));
					if (dist>40000) dist = 40000;
					AFarmDist fd( int( fnum ), dist );
					farmdists.push_back( fd );
				}
			}
		}
	}
	// By here we have a list of farms and their distances - now we have the option to sort it or randomise it - one or other line below should be commented out.
	// sort(farmdists.begin(), farmdists.end(), FarmDistSort()); // Sort
	random_shuffle( farmdists.begin(), farmdists.end() ); // Randomise
	// Now the vector is sorted/randomised we loop through and test probabilities
	for (int ch = 1; ch < 100000; ch++) // This loop just makes sure small chances don't mess up the run by tripping out early
	{
		int sz = int( farmdists.size() );
		for (int i = 0; i < sz; i++) {
			double chance = g_rand_uni();
			// Uses ch from the outer loop to scale probabilities - tripping out here will occur often for farms a long way from the area otherwise
			double calc = cfg_ClosestFarmProbParam2.value()* exp( (cfg_ClosestFarmProbParam1.value() / double( ch ))* farmdists[ i ].m_y );
			if (chance <= calc) return farmdists[ i ].m_x;
		}
	}
	g_msg->Warn( "FarmManager::FindClosestFarmProb", "- No suitable farm found" );
	exit( 0 );
}

int FarmManager::FindClosestFarmOpennessProbSmallIsBest( HunterInfo a_hunterinfo, vector<int> * a_farmlist, int a_openness, vector<int>* a_farmsizelist ) {
	/**
	* Centroid calculation on farms must be called before calling this method for the first time. \n
	* a_farmlist is a pointer to a list of integers with illegal farm ref numbers. The first entry lists the number of illegal numbers.
	* The chance of acceptance is probability based, taking the nearest farm first then testing the next etc.. The probability is based on y = 1*EXP(-x*cfg)
	* with cfg being a fitting parameter in cfg_ClosestFarmProbParam1
	*/

	/** We use typedef here to create our own name for APoint - but it is just two unsigned ints. We will use 'x' for the farm num and 'y' for the distance. */
	typedef  APoint AFarmDist;
	/** We use a vector of AFarmDists to be able to sort it easily */
	vector <AFarmDist> farmdists;
	/** \brief Struct redefining operator < - used for sorting distances from smallest to largest.*/
	struct FarmDistSort {
		bool operator()( AFarmDist a, AFarmDist b ) {
			return a.m_y < b.m_y;
		}
	};
	for (unsigned i = 0; i < m_farms.size(); i++) {
		int fnum = m_farms[ i ]->GetFarmNumber();
		if (!InIllegalList( fnum, a_farmlist )) {
			if (m_farms[ i ]->GetMaxOpenness() > a_openness) {
				if (!IsDuplicateRef( fnum, &a_hunterinfo )) {
					// Is possible to use this farm, so test it.
					APoint FPt = m_farms[ i ]->GetCentroids();
					int dist = int(sqrt( (double( FPt.m_x - a_hunterinfo.homeX ) * double( FPt.m_x - a_hunterinfo.homeX ) + double( FPt.m_y - a_hunterinfo.homeY ) * double( FPt.m_y - a_hunterinfo.homeY )) ));
					if (dist>40000) dist = 40000;
					AFarmDist fd( unsigned( fnum ), dist );
					farmdists.push_back( fd );
				}
			}
		}
	}
	// By here we have a list of farms and their distances - now we have the option to sort it or randomise it - one or other line below should be commented out.
	// sort(farmdists.begin(), farmdists.end(), FarmDistSort()); // Sort
	random_shuffle( farmdists.begin(), farmdists.end() ); // Randomise
	// Now the vector is sorted/randomised we loop through and test probabilities
	for (int ch = 1; ch < 100000; ch++) // This loop just makes sure small chances don't mess up the run by tripping out early
	{
		int sz = int( farmdists.size() );
		for (int i = 0; i < sz; i++) {
			double chance = g_rand_uni();
			// Uses ch from the outer loop to scale probabilities - tripping out here will occur often for farms a long way from the area otherwise
			double calc = cfg_ClosestFarmProbParam2.value()* exp( (cfg_ClosestFarmProbParam1.value() / double( ch ))* farmdists[ i ].m_y );
			if (chance <= calc) {
				// We passed the first test now take a second test based on farm size
				chance = g_rand_uni();
				calc = pow( double( (*a_farmsizelist)[ farmdists[ i ].m_x ] / 4000.0 ), cfg_FarmSizeProbParam1.value() );
				if (chance>calc) return farmdists[ i ].m_x;
			}
		}
	}
	g_msg->Warn( "FarmManager::FindClosestFarmProb", "- No suitable farm found" );
	exit( 0 );
}

int FarmManager::FindClosestFarmOpennessProbNearRoostIsBest( HunterInfo a_hunterinfo, vector<int> * a_farmlist, int a_openness, vector<APoint>* a_roostlist ) {
	/**
	* Centroid calculation on farms must be called before calling this method for the first time. \n
	* a_farmlist is a pointer to a list of integers with illegal farm ref numbers. The first entry lists the number of illegal numbers.
	* The chance of acceptance is probability based, taking the nearest farm first then testing the next etc.. The probability is based on y = 1*EXP(-x*cfg)
	* with cfg being a fitting parameter in cfg_ClosestFarmProbParam1
	*/

	/** We use typedef here to create our own name for APoint - but it is just two unsigned ints. We will use 'x' for the farm num and 'y' for the distance. */
	typedef  APoint AFarmDist;
	/** We use a vector of AFarmDists to be able to sort it easily */
	vector <AFarmDist> farmdists;
	/** \brief Struct redefining operator < - used for sorting distances from smallest to largest.*/
	struct FarmDistSort {
		bool operator()( AFarmDist a, AFarmDist b ) {
			return a.m_y < b.m_y;
		}
	};
	for (unsigned i = 0; i < m_farms.size(); i++) {
		int fnum = m_farms[ i ]->GetFarmNumber();
		if (!InIllegalList( fnum, a_farmlist )) {
			if (m_farms[ i ]->GetMaxOpenness() > a_openness) {
				if (!IsDuplicateRef( fnum, &a_hunterinfo )) {
					// Is possible to use this farm, so test it.
					APoint FPt = m_farms[ i ]->GetCentroids();
					int dist = int(sqrt( (double( FPt.m_x - a_hunterinfo.homeX ) * double( FPt.m_x - a_hunterinfo.homeX ) + double( FPt.m_y - a_hunterinfo.homeY ) * double( FPt.m_y - a_hunterinfo.homeY )) ));
					if (dist>40000) dist = 40000;
					AFarmDist fd( unsigned( fnum ), dist );
					farmdists.push_back( fd );
				}
			}
		}
	}
	// By here we have a list of farms and their distances - now we have the option to sort it or randomise it - one or other line below should be commented out.
	// sort(farmdists.begin(), farmdists.end(), FarmDistSort()); // Sort
	random_shuffle( farmdists.begin(), farmdists.end() ); // Randomise
	// Now the vector is sorted/randomised we loop through and test probabilities
	for (int ch = 1; ch < 100000; ch++) // This loop just makes sure small chances don't mess up the run by tripping out early
	{
		int sz = int( farmdists.size() );
		for (int i = 0; i < sz; i++) {
			double chance = g_rand_uni();
			// Uses ch from the outer loop to scale probabilities - tripping out here will occur often for farms a long way from the area otherwise
			double calc = cfg_ClosestFarmProbParam2.value()* exp( (cfg_ClosestFarmProbParam1.value() / double( ch ))* farmdists[ i ].m_y );
			if (chance <= calc) {
				// We passed the first test now take a second test based on roost distance
				chance = g_rand_uni();
				// Loop through each roost and find the closest to the farm - then do probability based on that distance.
				double dist = 10000;
				for (int r = 0; r < int( a_roostlist->size() ); r++) {
					double fdistroostx = farmdists[ i ].m_x - (*a_roostlist)[ r ].m_x;
					double fdistroosty = farmdists[ i ].m_y - (*a_roostlist)[ r ].m_y;
					double distf = sqrt( fdistroostx * fdistroostx + fdistroosty * fdistroostx );
					if (distf < dist) dist = distf;
				}
				calc = -0.01 + pow( dist / 10000.0, cfg_RoostDistProbParam1.value() );
				if (chance>calc) return farmdists[ i ].m_x;
			}
		}
	}
	g_msg->Warn( "FarmManager::FindClosestFarmProbNearRoostBest", "- No suitable farm found" );
	exit( 0 );
}

int FarmManager::FindFarmWithRandom( vector<int> * a_farmlist )
{
	int sz= (int)m_farms.size();
	int f = random(sz);
	while (InIllegalList(m_farms[f]->GetFarmNumber(), a_farmlist))
	{
		f = random(sz);
		if (a_farmlist->size() >= m_farms.size())
		{
			g_msg->Warn("FarmManager::FindFarmWithRandom"," - farm density rule means all hunters cannot be placed");
			exit(0);
		}
	}
	return m_farms[f]->GetFarmNumber();

}

int FarmManager::FindFarmWithOpenness(vector<int> * a_farmlist, int a_openness)
{
	// Centroid calculation on farms must be called before calling this method for the first time
	int sz = (int)m_farms.size();
	int seed = random( sz );
	for (unsigned i = 0; i < m_farms.size(); i++) {
		int index = (i + seed) % sz;
		if (m_farms[ index ]->GetMaxOpenness() > a_openness)
		{
				if (!InIllegalList( m_farms[ index ]->GetFarmNumber(), a_farmlist )) return m_farms[ index ]->GetFarmNumber();
		}
		else AddToIllegalList(m_farms[ index ]->GetFarmNumber(), a_farmlist );
	}
	g_msg->Warn("FarmManager::FindFarmWithOpenness", "- No suitable farm found");
	exit(0);
}

int FarmManager::FindOpennessFarm(int a_openness)
{
	int sz = (int) m_farms.size();
	int seed = random(sz);
	for (unsigned i = 0; i < m_farms.size(); i++)
	{
		int index = (i + seed) % sz;
		if (m_farms[index]->GetMaxOpenness() > a_openness) return m_farms[index]->GetFarmNumber();
	}
	return -1; // Should never happen but if it does we need to handle the -1 error code.
}

bool FarmManager::CheckOpenness( int a_openness, int a_ref ) {
	if (m_farms[ a_ref ]->GetMaxOpenness() > a_openness) return true;
	return false;
}

void FarmManager::Save_diff_farm_types_areas(){

	//get farms areas and save them
	totalOptFarmsArea=0;
	totalOptFarmsArea_plant=0;
	totalOptFarmsArea_pig=0;
	totalOptFarmsArea_cattle=0;
	totalOptFarmsArea_other=0;
	OptimisingFarm * opf;
	for(int j=0; j<(int)m_farms.size(); j++){
		if(m_farms[j]->GetType() == tof_OptimisingFarm){
			opf =  dynamic_cast<OptimisingFarm*>(m_farms[j]);
			totalOptFarmsArea += opf->GetAreaDouble();
			if(opf->Get_farmType() == toof_Plant){
				totalOptFarmsArea_plant += opf->GetAreaDouble();
			}
			else if(opf->Get_farmType() == toof_Pig){
				totalOptFarmsArea_pig += opf->GetAreaDouble();
			}
			else if(opf->Get_farmType() == toof_Cattle){
				totalOptFarmsArea_cattle += opf->GetAreaDouble();
			}
			else if(opf->Get_farmType() == toof_Other){
				totalOptFarmsArea_other += opf->GetAreaDouble();
			}
		}
	}
}

void FarmManager::ActualProfit(){

	for(int i=0; i<(int)m_farms.size(); i++){
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			OptimisingFarm * opf;
			opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			opf->ActualProfit();
		}
	}
}

void FarmManager::ChooseDecisionMode_for_farms(){

	for(int i=0; i<(int)m_farms.size(); i++){
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			OptimisingFarm * opf;
			opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			opf->ChooseDecisionMode();

		}
	}
}

void FarmManager::Save_last_years_crops_for_farms(){

	for(int i=0; i<(int)m_farms.size(); i++){
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			OptimisingFarm * opf;
			opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			opf->Save_last_years_crops();
		}
	}
}


//void FarmManager::Switch_rotational_crops_for_farms(){
//
//	for(int i=0; i<(int)m_farms.size(); i++){
//		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
//			OptimisingFarm * opf;
//			opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
//			opf->Switch_rotational_crops();
//		}
//	}
//}

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------


void FarmManager::ReadFarmsData(){

	int noFarms;
    int noParameters=6; //farm number, farm type, soil type, farm size, real ID, soil subtype

	//read the input file
	ifstream ifile("farms_data.txt", ios::in); //change it later - so that the nme can be set in the config file

	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "farms_data.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get the number of farms
	ifile>>noFarms;
	//get rid off the parameters' names
	string rubbish=""; //put here the names of the parameters;
	for(int i=0; i<noParameters; i++){
		ifile>>rubbish;
	}
	//read the data and assign values to the farm's variables
	for(int f=0; f<noFarms; f++){
		int almass_no, realID, soilSubType;
	    string farm_type, soil_type, farm_size;
		ifile >> almass_no >> farm_type >> soil_type >> farm_size >> realID >> soilSubType;
		TTypesOfOptFarms farmType = TranslateFarmCodes (farm_type);
		TTypesOfSoils soilType = TranslateSoilCodes (soil_type);
		TTypesOfFarmSize farmSize = TranslateFarmSizeCodes (farm_size);

		DataForOptimisation::FarmData * fd;
		fd = new DataForOptimisation::FarmData;
		pm_data->m_farm_data.push_back(fd);
		fd->md_almass_no=almass_no;
		fd->md_farmType=farmType;
		fd->md_soilType=soilType;
		fd->md_farmSize=farmSize;
		fd->md_farmRealID=realID;
		fd->md_soilSubType=soilSubType;
	 }

    ifile.close();
}

void FarmManager::ReadLivestockNumbers(){

	int noFarms;
	//vector<TTypesOfAnimals>livestock_types; //vector of livestock types (enums)

	//read the input file
	ifstream ifile("livestock_numbers.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "livestock_numbers.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get the number of farms //and of types of livestock
	ifile>>noFarms; //>>noLivestockTypes;

	//put the names of the livestock types to the this vector;
	for(int l=0; l<toa_Foobar; l++){
		string type;
		ifile>>type;
		//livestock_types.push_back(TranslateAnimalsCodes(type));
		pm_data->Set_livestockTypes(TranslateAnimalsCodes(type));
	}

	//loop through the farms; create an instance of each livestock type
	for(int f=0; f<noFarms; f++){
		int almass_no; //in the first column of the input table
		ifile>>almass_no;
		pm_data->Set_livestockNumber(almass_no); //put it here so that you can search for the part of a vector - that contains numbers of a given farm
		for(int l=0; l<toa_Foobar; l++){
			int number;
			ifile >> number;
			pm_data->Set_livestockNumber(number);
		}
	}
    ifile.close();

}

void FarmManager::ReadInitialCropAreas(){

	int noFarms;
	//vector<TTypesOfCrops>crops_types; //vector of crops types (enums)

	//read the input file
	ifstream ifile("crop_areas.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "crop_areas.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get the number of farms /
	ifile>>noFarms;

	//put the names of the crop types to TWO vectors: one with strings, one with enums
	for(int c=0; c<toc_Foobar; c++){
		string type;
		ifile>>type;
		TTypesOfCrops type_enum = TranslateCropsCodes(type);
		pm_data->Set_cropTypes(type_enum);
		pm_data->Set_cropTypes_str(type);
	}

	//loop through the farms; create an instance of each crop type - also when the initial area is zero!
	for(int f=0; f<noFarms; f++){
		double almass_no; //in the first column of the input table
		ifile>>almass_no;
		pm_data->Set_cropArea(almass_no); //put it here so that you can search for the part of a vector - that contains numbers of a given farm
		for(int c=0; c<toc_Foobar; c++){
			double area;
			ifile >> area;
			pm_data->Set_cropArea(area);
		}
	}
    ifile.close();
}

void FarmManager::ReadInitialCropAreas_almass(){

	int noFarms;
	int noCrops;

	//read the input file
	ifstream ifile("crop_areas.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "crop_areas.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get the number of crops
	ifile>>noCrops;
	//get the number of farms
	ifile>>noFarms;
	pm_data->Set_noCrops(noCrops);

	//put the names of the crop types to vector with enums and to the vector with strings - will be used for output
	for(int c=0; c<noCrops; c++){
		string type;
		ifile>>type;
		TTypesOfVegetation type_enum = TranslateCropCodes(type);
		pm_data->Set_cropTypes_almass(type_enum);
		pm_data->Set_cropTypes_almass_str(type);
	}

	//loop through the farms; create an instance of each crop type - also when the initial area is zero!
	for(int f=0; f<noFarms; f++){
		double almass_no; //in the first column of the input table
		ifile>>almass_no;
		pm_data->Set_cropArea(almass_no); //put it here so that you can search for the part of a vector - that contains numbers of a given far
		for(int c=0; c<noCrops; c++){
			double area;
			ifile >> area;
			pm_data->Set_cropArea(area);
		}
	}
    ifile.close();
}

void FarmManager::ReadCropsData(){

	//read the input file
	ifstream ifile("crop_parameters.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "crop_parameters.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get rid of the parameters names
	for(int p=0; p<top_Foobar; p++){
		string type;
		ifile>>type;
	}

	//loop through the crops - save values of the parameters in a vector of the DataForOptimisation class
	for(int c=0; c<toc_Foobar; c++){
		string crop; //in the first column of the input table
		ifile>>crop; //do nothing with it
		for(int p=0; p<top_Foobar; p++){
			double par_value;
			ifile>>par_value;
			int index=top_Foobar*c + p;
			pm_data->Set_cropParameter (par_value, index );
		}
	}
    ifile.close();
}

void FarmManager::ReadCropsData_almass(){

	//read the input file
	ifstream ifile("crop_parameters.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "crop_parameters.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	int noCrops=0;
	ifile>>noCrops;

	//get rid of the parameters names
	for(int p=0; p<top_Foobar; p++){
		string type;
		ifile>>type;
	}

	//loop through the crops - save values of the parameters in a vector of the DataForOptimisation class
	for(int c=0; c<noCrops; c++){
		string crop; //in the first column of the input table
		ifile>>crop; //do nothing with it
		for(int p=0; p<top_Foobar; p++){
			double par_value;
			ifile>>par_value;
			int index=top_Foobar*(pm_data->Get_cropTypes_almass(c)) + p;  //Get_cropTypes_almass(c) - gives as a tov type crop at position c
			pm_data->Set_cropParameter (par_value, index );
		}
	}
    ifile.close();

}

void FarmManager::ReadCropsData_perSoilType(){

	//read the input files
	ifstream ifile1("alfa.txt", ios::in);
	ifstream ifile2("beta1.txt", ios::in);
	ifstream ifile3("beta2.txt", ios::in);
	ifstream ifile4("Nnorm.txt", ios::in);
	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "alfa.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    } else if(!ifile2.is_open() ){
			cout << "Cannot open input file " << "beta1.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
	else if(!ifile3.is_open() ){
			cout << "Cannot open input file " << "beta2.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
	else if(!ifile4.is_open() ){
			cout << "Cannot open input file " << "Nnorm.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
	//get rid off the crops names;
	for(int c=0; c<toc_Foobar; c++){
		string type;
		ifile1>>type;
		ifile2>>type;
		ifile3>>type;
		ifile4>>type;
	}

	//loop through the soil types!
	for(int s=0; s<tos_Foobar; s++){
		string soil;
		ifile1>>soil; //get rid off the soil type name
		ifile2>>soil;
		ifile3>>soil;
		ifile4>>soil;
		for(int c=0; c<toc_Foobar; c++){
			double par_value;
			ifile1>>par_value;
			pm_data->Set_alfa(par_value, toc_Foobar*s + c);
			ifile2>>par_value;
			pm_data->Set_beta1(par_value, toc_Foobar*s + c);
			ifile3>>par_value;
			pm_data->Set_beta2(par_value, toc_Foobar*s + c);
			ifile4>>par_value;
			pm_data->Set_Nnorm(par_value, toc_Foobar*s + c);

		}
	}
    ifile1.close();
	ifile2.close();
	ifile3.close();
	ifile4.close();
}

void FarmManager::ReadCropsData_perSoilType_almass(){

	//read the input files
	ifstream ifile1("alfa.txt", ios::in);
	ifstream ifile2("beta1.txt", ios::in);
	ifstream ifile3("beta2.txt", ios::in);
	ifstream ifile4("Nnorm.txt", ios::in);
	ifstream ifile5("biomass_factor_almass.txt", ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "alfa.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    } else if(!ifile2.is_open() ){
			cout << "Cannot open input file " << "beta1.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
	else if(!ifile3.is_open() ){
			cout << "Cannot open input file " << "beta2.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
	else if(!ifile4.is_open() ){
			cout << "Cannot open input file " << "Nnorm.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
    if (!ifile5.is_open() ) {
            cout << "Cannot open input file " << "biomass_factor_almass.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	int noCrops, noCrops1, noCrops2, noCrops3, noCrops4=0;
	ifile1>>noCrops;
	ifile2>>noCrops1;
	ifile3>>noCrops2;
	ifile4>>noCrops3;
	ifile5>>noCrops4;  //should be always same number!
	if(noCrops!=noCrops1 || noCrops!=noCrops2 || noCrops!=noCrops3 || noCrops!=noCrops4){
		g_msg->Warn( WARN_FILE, "FarmManager::ReadCropsData_perSoilType_almass():"" Numbers of crops differ between input files! ", " " );
		exit( 1 );
	}

	//get rid off the crops names;
	for(int c=0; c<noCrops; c++){
		string type;
		ifile1>>type;
		ifile2>>type;
		ifile3>>type;
		ifile4>>type;
		ifile5>>type;
	}

	//loop through the soil types!
	for(int s=0; s<tos_Foobar; s++){
		string soil;
		ifile1>>soil; //get rid off the soil type name
		ifile2>>soil;
		ifile3>>soil;
		ifile4>>soil;
		ifile5>>soil;
		for(int c=0; c<noCrops; c++){
			double par_value;
			ifile1>>par_value;
			pm_data->Set_alfa(par_value, tov_Undefined*s + pm_data->Get_cropTypes_almass(c)); //Get_cropTypes_almass(c) - gives as a tov type crop at position c
			ifile2>>par_value;
			pm_data->Set_beta1(par_value, tov_Undefined*s + pm_data->Get_cropTypes_almass(c));
			ifile3>>par_value;
			pm_data->Set_beta2(par_value, tov_Undefined*s + pm_data->Get_cropTypes_almass(c));
			ifile4>>par_value;
			pm_data->Set_Nnorm(par_value, tov_Undefined*s + pm_data->Get_cropTypes_almass(c));
			ifile5>>par_value;
			pm_data->Set_biomass_factor(par_value, tov_Undefined*s + pm_data->Get_cropTypes_almass(c));

		}
	}
    ifile1.close();
	ifile2.close();
	ifile3.close();
	ifile4.close();
	ifile5.close();
}

void FarmManager::ReadCropsData_perFarmType(){

	//read the input files
	ifstream ifile1("fixed.txt", ios::in);
	ifstream ifile2("fodder.txt", ios::in);
	ifstream ifile3("FUKey.txt", ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "fixed.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    } else if(!ifile2.is_open() ){
			cout << "Cannot open input file " << "fodder.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
	else if(!ifile3.is_open() ){
			cout << "Cannot open input file " << "FUKey.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}

	//get rid off the crops names;
	for(int c=0; c<toc_Foobar; c++){
		string type;
		ifile1>>type;
		ifile2>>type;
		ifile3>>type;
	}

	//loop through the farm types!
	for(int f=0; f<toof_Foobar; f++){
		string farm_type;
		ifile1>>farm_type; //get rid off the farm type name
		ifile2>>farm_type;
		ifile3>>farm_type;
		for(int c=0; c<toc_Foobar; c++){
			string par;
			ifile1>>par;
			pm_data->Set_fixed(par, toc_Foobar*f + c);
			ifile2>>par;
			pm_data->Set_fodder(par, toc_Foobar*f + c);
			double par_value;
			ifile3>>par_value;
			pm_data->Set_FUKey(par_value, toc_Foobar*f + c);
		}
	}
    ifile1.close();
	ifile2.close();
	ifile3.close();
}

void FarmManager::ReadCropsData_perFarmType_almass(){

		//read the input files
	ifstream ifile1("fixed.txt", ios::in);
	ifstream ifile2("fodder.txt", ios::in);
	ifstream ifile3("FUKey.txt", ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "fixed.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    } else if(!ifile2.is_open() ){
			cout << "Cannot open input file " << "fodder.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}
	else if(!ifile3.is_open() ){
			cout << "Cannot open input file " << "FUKey.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}

	int noCrops, noCrops1, noCrops2=0;
	ifile1>>noCrops;
	ifile2>>noCrops1;
	ifile3>>noCrops2;  //should be always same number!
	if(noCrops!=noCrops1 || noCrops!=noCrops2){
		g_msg->Warn( WARN_FILE, "FarmManager::ReadCropsData_perSoilType_almass():"" Numbers of crops differ between input files! ", " " );
		exit( 1 );
	}


	//get rid off the crops names;
	for(int c=0; c<noCrops; c++){
		string type;
		ifile1>>type;
		ifile2>>type;
		ifile3>>type;
	}

	//loop through the farm types!
	for(int f=0; f<toof_Foobar; f++){
		string farm_type;
		ifile1>>farm_type; //get rid off the farm type name
		ifile2>>farm_type;
		ifile3>>farm_type;
		for(int c=0; c<noCrops; c++){
			string par;
			ifile1>>par;
			pm_data->Set_fixed(par, tov_Undefined*f + pm_data->Get_cropTypes_almass(c));
			ifile2>>par;
			pm_data->Set_fodder(par, tov_Undefined*f + pm_data->Get_cropTypes_almass(c));
			double par_value;
			ifile3>>par_value;
			pm_data->Set_FUKey(par_value, tov_Undefined*f + pm_data->Get_cropTypes_almass(c));
		}
	}
    ifile1.close();
	ifile2.close();
	ifile3.close();
}

void FarmManager::ReadCropsData_perFarm_and_Soil(){

	//read the input files
	ifstream ifile1("sellingPrice.txt", ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "sellingPrice.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get rid off the crops names;
	for(int c=0; c<toc_Foobar; c++){
		string type;
		ifile1>>type;
	}

	//loop through the farm and soil types!
	for(int f=0; f<(toof_Foobar * tos_Foobar); f++){
		string farm_type;
		string soil_type;
		ifile1>>farm_type>>soil_type; //get rid off the farm type and soil type names
		for(int c=0; c<toc_Foobar; c++){
			double par_value;
			ifile1>>par_value;
			pm_data->Set_sellingPrice(par_value, toc_Foobar*f + c);
		}
	}
    ifile1.close();
}

void FarmManager::ReadCropsData_perFarm_and_Soil_almass(){

	 //read the input files
	ifstream ifile1("sellingPrice.txt", ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "sellingPrice.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	int noCrops=0;
	ifile1>>noCrops;

	//get rid off the crops names;
	for(int c=0; c<noCrops; c++){
		string type;
		ifile1>>type;
	}

	//loop through the farm and soil types!
	for(int f=0; f<(toof_Foobar * tos_Foobar); f++){
		string farm_type;
		string soil_type;
		ifile1>>farm_type>>soil_type; //get rid off the farm type and soil type names
		for(int c=0; c<noCrops; c++){
			double par_value;
			ifile1>>par_value;
			pm_data->Set_sellingPrice(par_value, tov_Undefined*f + pm_data->Get_cropTypes_almass(c));
		}
	}
    ifile1.close();


 }

void FarmManager::ReadCropsData_perFarm_and_Soil_and_Size(){

	//read the input files
	ifstream ifile1("rotationMax.txt", ios::in);
	ifstream ifile2("rotationMin.txt", ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "rotationMax.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    } else if(!ifile2.is_open() ){
			cout << "Cannot open input file " << "rotationMin.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}

	//get rid off the crops names;
	for(int c=0; c<toc_Foobar; c++){
		string type;
		ifile1>>type;
		ifile2>>type;
	}

	//loop through the farm types, soil types and farm sizes!
	for(int f=0; f<(toof_Foobar *tos_Foobar * tofs_Foobar); f++){
		string farm_type, soil_type, farm_size;
		ifile1>>farm_type>>soil_type>>farm_size; //get rid off the farm type, soil type ad farm size names
		ifile2>>farm_type>>soil_type>>farm_size;
		for(int c=0; c<toc_Foobar; c++){
			double par_value;
			ifile1>>par_value;
			pm_data->Set_rotationMax(par_value, toc_Foobar*f + c);
			ifile2>>par_value;
			pm_data->Set_rotationMin(par_value, toc_Foobar*f + c);
		}
	}
    ifile1.close();
	ifile2.close();
}

void FarmManager::ReadCropsData_perFarm_and_Soil_and_Size_almass(){

	//read the input files

	string rot_max;
	string rot_min;

	if(cfg_Areas_Based_on_Distribution.value()){
		rot_max = "rotationMax_almass.txt";
		rot_min = "rotationMin_almass.txt";
	}
	else{ //rot max and min as specified in the Original Bedriftsmodel
		rot_max = "rotationMax.txt";
		rot_min = "rotationMin.txt";
	}

	ifstream ifile1(rot_max.c_str(), ios::in);
	ifstream ifile2(rot_min.c_str(), ios::in);



	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file with rotation max" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    } else if(!ifile2.is_open() ){
			cout << "Cannot open input file with rotation min" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
	}

	int noCrops, noCrops1=0;
	ifile1>>noCrops;
	ifile2>>noCrops1;  //should be always same number!
	if(noCrops!=noCrops1){
		g_msg->Warn( WARN_FILE, "FarmManager::ReadCropsData_perSoilType_almass():"" Numbers of crops differ between input files! ", " " );
		exit( 1 );
	}

	//get rid off the crops names;
	for(int c=0; c<noCrops; c++){
		string type;
		ifile1>>type;
		ifile2>>type;
	}

	//loop through the farm types, soil types and farm sizes!
	for(int f=0; f<(toof_Foobar *tos_Foobar * tofs_Foobar); f++){
		string farm_type, soil_type, farm_size;
		ifile1>>farm_type>>soil_type>>farm_size; //get rid off the farm type, soil type ad farm size names
		ifile2>>farm_type>>soil_type>>farm_size;
		for(int c=0; c<noCrops; c++){
			double par_value;
			ifile1>>par_value;
			pm_data->Set_rotationMax(par_value, tov_Undefined*f + pm_data->Get_cropTypes_almass(c));
			ifile2>>par_value;
			pm_data->Set_rotationMin(par_value, tov_Undefined*f + pm_data->Get_cropTypes_almass(c));
		}
	}
    ifile1.close();
	ifile2.close();

}

void FarmManager::ReadFarmsData_perFarmType(){

	//read the input files
	ifstream ifile1("winterMax.txt", ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
            cout << "Cannot open input file " << "winterMax.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get rid off the parameter name;
		string type;
		ifile1>>type;

	//loop through the farm types!
	for(int f=0; f<toof_Foobar; f++){
		string farm_type;
		ifile1>>farm_type; //get rid off the farm type name
			int par_value;
			ifile1>>par_value;
			pm_data->Set_winterMax(par_value, f);
	}
    ifile1.close();
}

void FarmManager::ReadLivestockData(){

	//read the input file
	ifstream ifile("livestock_parameters.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "livestock_parameters.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get rid off the parameter names
	for(int p=0; p<tolp_Foobar; p++){
		string type;
		ifile>>type;
	}

	//loop through the livestock types - save values of the parameters in a vector of the DataForOptimisation class
	for(int l=0; l<toa_Foobar; l++){
		string livestock; //in the first column of the input table
		ifile>>livestock; //do nothing with it
		for(int p=0; p<tolp_Foobar; p++){
			double par_value;
			ifile>>par_value;
			pm_data->Set_livestockParameter(par_value, tolp_Foobar*l + p);
		}
	}
    ifile.close();
}


void FarmManager::ReadEnergyMaizePrice(){


	//read the input files
	ifstream ifile1(l_emaize_price_file.value(), ios::in);

	//check if there are all input files
    if ( !ifile1.is_open() ) {
			cout << "Cannot open input file " << l_emaize_price_file.value() << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }
	//get th enumber of years he file has data for
	int no_years=0;
	ifile1>>no_years;

	//loop
	for(int i=0; i<no_years; i++){
		int year;
		ifile1>>year; //get rid off the year number
		double par_value; 
		ifile1>>par_value;			
		pm_data->Set_emaize_price(par_value);
	}
    ifile1.close();
}


TTypesOfOptFarms FarmManager::TranslateFarmCodes(string &astr ) {
  // This simply checks through the list of legal farm types and returns
  // the correct farm type
  string str = astr;
  if ( str == "Other" ) return toof_Other;
  if ( str == "Cattle" ) return toof_Cattle;
  if ( str == "Plant" ) return toof_Plant;
  if ( str == "Pig" ) return toof_Pig;
  // No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TranslateFarmCodes():"" Unknown Farm type Code ", str.c_str() );
  exit( 1 );
}

TTypesOfSoils FarmManager::TranslateSoilCodes(string &astr ) {
  // This simply checks through the list of legal soil types and returns
  // the correct soil type
  string str = astr;
  if ( str == "Other" ) return tos_Other;
  if ( str == "Sand" ) return tos_Sand;
  if ( str == "Clay" ) return tos_Clay;
  // No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TranslateSoilCodes():"" Unknown Soil type Code ", str.c_str() );
  exit( 1 );
}

TTypesOfFarmSize FarmManager::TranslateFarmSizeCodes(string &astr ) {
  // This simply checks through the list of legal farm sizes and returns
  // the correct farm size
  string str = astr;
  if ( str == "business" ) return tofs_Business;
  if ( str == "private" ) return tofs_Private;
  // No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TranslateFarmSizeCodes():"" Unknown Farm Size Code ", str.c_str() );
  exit( 1 );
}

TTypesOfAnimals FarmManager::TranslateAnimalsCodes(string &astr ) {
  // This simply checks through the list of legal animal (livestock) types and returns
  // the correct animal type
	string str = astr;
	if ( str == "Horse") return toa_Horse;
	if ( str == "DCow") return toa_DCow;
	if ( str == "Suckler") return toa_Suckler;
	if ( str == "DCalf") return toa_DCalf;
	if ( str == "MCalf") return toa_MCalf;
	if ( str == "MCattle") return toa_MCattle;
	if ( str == "Sheep") return toa_Sheep;
	if ( str == "Goat") return toa_Goat;
	if ( str == "So") return toa_So;
	if ( str == "Finisher") return toa_Finisher;
	if ( str == "Piglet") return toa_Piglet;
	if ( str == "Deer") return toa_Deer;
	if ( str == "Mink") return toa_Mink;
	if ( str == "EHen") return toa_EHen;
	if ( str == "MHen") return toa_MHen;
	if ( str == "Turkey") return toa_Turkey;
	if ( str == "Goose") return toa_Goose;
	if ( str == "Duck") return toa_Duck;
	if ( str == "MDuck") return toa_MDuck;
	if ( str == "Ostrich") return toa_Ostrich;
  // No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TranslateAnimalsCodes():"" Unknown Animal type Code ", str.c_str() );
  exit( 1 );
}

TTypesOfCrops FarmManager::TranslateCropsCodes (string &astr){
// This simply checks through the list of legal crops types and returns
// the correct crop type
	string str = astr;
	if ( str == "SBarley") return toc_SBarley;
	if ( str == "Oats") return toc_Oats;
	if ( str == "OSCrops") return toc_OSCrops;
	if ( str == "WBarley") return toc_WBarley;
	if ( str == "WWheat") return toc_WWheat;
	if ( str == "WRye") return toc_WRye;
	if ( str == "Triticale") return toc_Triticale;
	if ( str == "SRape") return toc_SRape;
	if ( str == "WRape") return toc_WRape;
	if ( str == "OOilseed") return toc_OOilseed;
	if ( str == "Peas") return toc_Peas;
	if ( str == "OLSeed") return toc_OLSeed;
	if ( str == "GrassSeed") return toc_GrassSeed;
	if ( str == "Potato") return toc_Potato;
	if ( str == "PotatoFood") return toc_PotatoFood;
	if ( str == "SugarBeet") return toc_SugarBeet;
	if ( str == "GrassClover") return toc_GrassClover;
	if ( str == "OLSeedCut") return toc_OLSeedCut;
	if ( str == "SCerealSil") return toc_SCerealSil;
	if ( str == "PeasSil") return toc_PeasSil;
	if ( str == "MaizeSil") return toc_MaizeSil;
	if ( str == "WCerealSil") return toc_WCerealSil;
	if ( str == "SCerealG") return toc_SCerealG;
	if ( str == "PerGrassLow") return toc_PerGrassLow;
	if ( str == "PerGrassNorm") return toc_PerGrassNorm;
	if ( str == "GrassEnv1") return toc_GrassEnv1;
	if ( str == "GrassEnv2") return toc_GrassEnv2;
	if ( str == "GrassRot") return toc_GrassRot;
	if ( str == "Setaside") return toc_Setaside;
	if ( str == "Uncult") return toc_Uncult;
	if ( str == "OUncult") return toc_OUncult;
	if ( str == "OFodderBeet") return toc_OFodderBeet;
	if ( str == "CloverGrz") return toc_CloverGrz;
	if ( str == "Veg") return toc_Veg;
	if ( str == "Fruit") return toc_Fruit;
	if ( str == "FruitTree") return toc_FruitTree;
	if ( str == "OSpecCrops") return toc_OSpecCrops;
	if ( str == "ChrisTree") return toc_ChrisTree;
	if ( str == "EnergyFor") return toc_EnergyFor;
	if ( str == "Other") return toc_Other;
	// No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TranslateCropCodes():"" Unknown Crop type Code ", str.c_str() );
  exit( 1 );
}

TTypesOfParameters FarmManager::TranslateParametersCodes (string &astr){
  // This simply checks through the list of legal crop parameters (those not varying with farm type etc.)
  //and returns the correct enum
  string str = astr;
    if ( str == "Subsidy" ) return top_Subsidy;
	if ( str == "PriceLM" ) return top_PriceLM;
	if ( str == "PriceHerb" ) return top_PriceHerb;
	if ( str == "PriceFi" ) return top_PriceFi;
	if ( str == "PriceG" ) return top_PriceG;
	if ( str == "PriceH" ) return top_PriceH;
	if ( str == "PriceW" ) return top_PriceW;
	if ( str == "AlfaHerb" ) return top_AlfaHerb;
	if ( str == "BetaHerb" ) return top_BetaHerb;
	if ( str == "AlfaFi" ) return top_AlfaFi;
	if ( str == "BetaFi" ) return top_BetaFi;
	if ( str == "AlfaG" ) return top_AlfaG;
	if ( str == "BetaG" ) return top_BetaG;
	if ( str == "AlfaH" ) return top_AlfaH;
	if ( str == "BetaH" ) return top_BetaH;
	if ( str == "AlfaW" ) return top_AlfaW;
	if ( str == "BetaW" ) return top_BetaW;

  // No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TranslateParametersCodes():"" Unknown Parameter Code ", str.c_str() );
  exit( 1 );
}

TTypesOfLivestockParameters FarmManager::TranslateLivestockParametersCodes (string &astr){
  // This simply checks through the list of legal livestock parameters and returns the correct enum
  string str = astr;
    if ( str == "AUKey" ) return tolp_AUKey;
	if ( str == "Nusable" ) return tolp_Nusable;
	if ( str == "FUuKey" ) return tolp_FUuKey;

  // No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TranslateLivestockParametersCodes():"" Unknown Livestock Parameter Code ", str.c_str() );
  exit( 1 );
}

TTypesOfCropVariables FarmManager::TranslateCropVariablesCodes (string &astr){
  // This simply checks through the list of legal crop variables and returns the correct enum
  string str = astr;
	if ( str == "m_areaPercent" ) return tocv_AreaPercent;
	if ( str == "m_area_ha" ) return tocv_AreaHa;
	if ( str == "m_n" ) return tocv_N;
	if ( str == "m_nt" ) return tocv_Nt;
	if ( str == "m_BIHerb" ) return tocv_BIHerb;
	if ( str == "m_BIFi" ) return tocv_BIFi;
	if ( str == "m_BI" ) return tocv_BI;
	if ( str == "m_grooming" ) return tocv_Grooming;
	if ( str == "m_hoeing" ) return tocv_Hoeing;
	if ( str == "m_weeding" ) return tocv_Weeding;
	if ( str == "m_totalLoss" ) return tocv_TotalLoss;
	if ( str == "m_resp" ) return tocv_Response;
	if ( str == "m_income" ) return tocv_Income;
	if ( str == "m_costs" ) return tocv_Costs;
	if ( str == "m_GM" ) return tocv_GM;
	if ( str == "m_savings" ) return tocv_Savings;

  // No match so issue a warning and quit
  g_msg->Warn( WARN_FILE, "FarmManager::TTypesOfCropVariables():"" Unknown crop variable Code ", str.c_str() );
  exit( 1 );
}

void FarmManager::OptimisationOutput(){

//1. crop variables
	PrintOutput(tocv_AreaPercent, "AreaPercent.txt");
	PrintOutput(tocv_AreaHa, "AreaHa.txt");
	PrintOutput(tocv_N, "N.txt");
	PrintOutput(tocv_Nt, "Nt.txt");
	PrintOutput(tocv_BIHerb, "BIHerb.txt");
	PrintOutput(tocv_BIFi, "BIFi.txt");
	PrintOutput(tocv_BI, "BI.txt");
	PrintOutput(tocv_Grooming, "Grooming.txt");
	PrintOutput(tocv_Hoeing, "Hoeing.txt");
	PrintOutput(tocv_Weeding, "Weeding.txt");
	PrintOutput(tocv_TotalLoss, "TotalLoss.txt");
	PrintOutput(tocv_Response, "Response.txt");
	PrintOutput(tocv_Income, "Income.txt");
	PrintOutput(tocv_Costs, "Costs.txt");
	PrintOutput(tocv_GM, "GM.txt");
	PrintOutput(tocv_Savings, "Savings.txt");

//2. crop total areas in ha at the landscape level

	CropDistribution(); //now there are total area values for each crop in the m_cropTotals vector
	ofstream ofile("CropDistribution.txt", ios::out);

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		//print crop names (using vector<string>m_cropTypes)
		for(int c=0; c<toc_Foobar; c++) ofile << pm_data->Get_cropTypes_str(c) << '\t';
		ofile  << endl;
		//print the values (total areas in ha)
		for(int c=0; c<toc_Foobar; c++) ofile << m_cropTotals[c] << '\t';
	}
	else{
		//print 'year'
		ofile << "Year" << '\t';
		//print crop names
		for(int c=0; c<pm_data->Get_noCrops(); c++) ofile << pm_data->Get_cropTypes_almass_str(c) << '\t';
		ofile  << endl;
		//it's the first optimisation, so say the year is zero
		ofile << "0" << '\t';
		//print the values (total areas in ha)
		for(int c=0; c<pm_data->Get_noCrops(); c++) ofile << m_cropTotals[c] << '\t';
		ofile << endl;
	}

	//restart the m_cropTotals:
	for(int i=0; i<(int)m_cropTotals.size(); i++){
		m_cropTotals[i]=0;
	}

	ofile.close();
}

void FarmManager::PrintOutput(TTypesOfCropVariables a_var, string a_fileName){

	ofstream ofile (a_fileName.c_str(), ios::out);
	ofile << "Farm no" << '\t';
	//print crop names (using vector<string>m_cropTypes)
	if(cfg_OptimiseBedriftsmodelCrops.value()){
		for(int c=0; c<toc_Foobar; c++) ofile << pm_data->Get_cropTypes_str(c) << '\t';
	}
	else{
		for(int c=0; c<pm_data->Get_noCrops(); c++) ofile << pm_data->Get_cropTypes_almass_str(c) << '\t';
	}
	ofile  << endl;

	for (int i=0; i<(int)m_farms.size(); i++){
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			OptimisingFarm * opf;
			opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			int no = opf->Get_almass_no();
			ofile  << no  << '\t';
			int size = opf->Get_cropsSize(); 
			for(int j = 0;  j < size; j++){
				double var_to_print = -1;
				CropOptimised* crop = opf->Get_crop(j);
				switch (a_var){
						case tocv_AreaPercent:
							var_to_print = crop ->m_areaPercent;
							break;
						case tocv_AreaHa:
							var_to_print = crop ->m_area_ha;
							break;
						case tocv_N:
							var_to_print = crop ->m_n;
							break;
						case tocv_Nt:
							var_to_print = crop ->m_nt;
							break;
						case tocv_BIHerb:
							var_to_print = crop ->m_BIHerb;
							break;
						case tocv_BIFi:
							var_to_print = crop ->m_BIFi;
							break;
						case tocv_BI:
							var_to_print = crop ->m_BI;
							break;
						case tocv_Grooming:
							var_to_print = crop ->m_grooming;
							break;
						case tocv_Hoeing:
							var_to_print = crop ->m_hoeing;
							break;
						case tocv_Weeding:
							var_to_print = crop ->m_weeding;
							break;
						case tocv_TotalLoss:
							var_to_print = crop ->m_totalLoss;
							break;
						case tocv_Response:
							var_to_print = crop ->m_resp;
							break;
						case tocv_Income:
							var_to_print = crop ->m_income_ha;
							break;
						case tocv_Costs:
							var_to_print = crop ->m_costs_ha;
							break;
						case tocv_GM:
							var_to_print = crop ->m_GM;
							break;
						case tocv_Savings:
							var_to_print = crop ->m_savings;
							break;
						default:
							g_msg->Warn( WARN_BUG, "FarmManager::PrintOutput(): ""Unknown crop variable type! ", "" );
							exit( 1 );
				}
				ofile  << var_to_print << '\t';
			}
			ofile  << endl;
		}
	}
    ofile.close();
}

void FarmManager::CropDistribution(){

	int no_crops = (cfg_OptimiseBedriftsmodelCrops.value())? toc_Foobar : pm_data->Get_noCrops(); //bedriftsmodel crops/almass crops
	m_cropTotals.resize(no_crops); //no_crops is ok for both almass and bedriftsmodel crop mode; it is taken from the input file on initial crop areas

	for(int i=0; i<(int)m_farms.size(); i++){

		OptimisingFarm * opf;
		int size=0;
		if(m_farms[i]->GetType()==tof_OptimisingFarm) { //take only optimising farms
			opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			size = opf->Get_cropsSize(); 
			for(int j=0; j<size; j++) {
				CropOptimised *crop = opf->Get_crop(j);
				double area = crop->m_area_ha;
				m_cropTotals[j] += area;
			}
		}
	}
}

void FarmManager::Create_Output_file_for_farms(){

	pm_output_file = new ofstream ("FarmVariables.txt", ios::out);

	(*pm_output_file) << "Farm no \t";

	//print variables' names (18 variables, 09.05.12)
	(*pm_output_file) << "mainGoal \t" << "totalArea \t" << "totalIncome \t" << "totalCosts \t" << "totalProfit \t";
	(*pm_output_file) << "FodderDemand \t" << "FodderDemand_bef \t" << "FodderTrade \t" << "FodderGrown \t" ;
	(*pm_output_file) << "Nanim \t" << "totalNanim \t" << "totalNt \t" << "totalN \t" ;
	(*pm_output_file) << "totalBI \t" << "totalBIHerb \t" << "totalBIFi \t" ;
	(*pm_output_file) << "totalGrooming \t" << "totalHoeing \t" << "totalWeeding \t";
	(*pm_output_file) << endl;

}

void FarmManager::ReadLookupTable(){

	//read the input file
	ifstream ifile("crops_lookup.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "crops_lookup.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }

	//get rid of the crop tov names - first line of the input file
	for(int p=0; p<tov_Undefined+1; p++){
		string tov_type;
		ifile>>tov_type;
	}

	//loop through the crops - save values (-1, 0 or 1) in the vector<int>m_crop_lookup_table;
	for(int r=0; r<tov_Undefined+1; r++){ //rows
		string tov_type; //in the first column of the input table
		ifile>>tov_type; //do nothing with it
		for(int c=0; c<tov_Undefined+1; c++){  //you are at row r, go through all the columns c
			int value;
			ifile >> value;
			m_crop_lookup_table.push_back(value);
		}
	}
    ifile.close();

}

void FarmManager::FindNeighbours(){
	//determine who is each farm's neighbour and  save them in the farm's vector of its neighbours
	//and print the neighbours

	OptimisingFarm * opf_i;
	OptimisingFarm * opf_k;

	for(int i=0; i<(int)m_farms.size(); i++){
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			opf_i =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			opf_i->Centroids(); //finds each farm's centroid
		}
	}

	for(int i=0; i<(int)m_farms.size(); i++){
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			opf_i =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			int x = opf_i->GetFarmCentroidX();
			int y = opf_i->GetFarmCentroidY();
			int farm_no_i = opf_i->Get_almass_no();

			for(int k = 0; k<(int)m_farms.size(); k++) { //find neighbours of farm i
				if(m_farms[k]->GetType() == tof_OptimisingFarm){ //take only optimising farms
					opf_k =  dynamic_cast<OptimisingFarm*>(m_farms[k]);
					int xb = opf_k->GetFarmCentroidX();
					int yb = opf_k->GetFarmCentroidY();
					double par = 1000 * cfg_Neighbor_dist.value(); //change km to m
					int farm_no_k = opf_k->Get_almass_no();

					if(abs(x - xb) <= par && abs(y - yb) <= par && farm_no_i!=farm_no_k){ //check neighbourship (make sure it is not the same farm)
						//this is a neighbour
						opf_i->Set_Neighbour(opf_k); //add farm k to the list of neighbours of farm i
					}
				}
				else break;
			}
		}
		else break; //break the for loop -there is no more optimising farms
	}

	PrintNeighbours();

}

void FarmManager::PrintNeighbours(){
	//create a text file with the farms and their neighbours
	ofstream ofile ("Neighbours.txt", ios::out);
	ofile << "Farm no" << endl;

	//print each farms no and then the numbers of its neighbours
	for(int i=0; i<(int)m_farms.size(); i++){
		OptimisingFarm * opf_i;
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			opf_i =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			ofile << opf_i->Get_almass_no() << '\t';
			for(int j=0; j<opf_i->Get_NeighboursSize(); j++){
				ofile << opf_i->Get_Neighbour(j)->Get_almass_no() <<'\t'; //pointer to the current, i.e. 'i' farm, takes its neighbour (j) and retrieves his almass_no
			}
			ofile  << endl;
		}
	}
}

void FarmManager::PrintDecModes_counts(){

	//create a text file and make headers
	ofstream ofile ("Decision_modes_counts.txt", ios::out);
	ofile << "Farm_no\t" << "imitation\t" <<  "social comparison\t" << "repeat\t"<<  "deliberation\t"<<endl;

	//print each farms no and then the content of the vector ...
	for(int i=0; i<(int)m_farms.size(); i++){
		OptimisingFarm * opf_i;
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			opf_i =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			ofile << opf_i->Get_almass_no() << '\t';
			for(int j=0; j<4; j++){
				ofile << opf_i->Get_decision_mode_counters(j) <<'\t';
			}
			ofile  << endl;
		}
	}

}

void FarmManager::PrintFinalResults(){

	PrintDecModes_counts();

	int no_years = g_date->GetYearNumber() - 21; //exclude first 9 years + one - the last one, which has just started now - it's Jan 1st//exclude first 20 (0-19) years for the wildlife runs
	double BIherb_sum=0;
	double BIfi_sum=0;
	double BI_sum=0;
	double fertilizer_sum=0;
	double fertilizer_trade_sum=0;

	totalOptFarmsArea *= 0.0001;//change to ha
	totalOptFarmsArea_plant *= 0.0001;
	totalOptFarmsArea_pig *= 0.0001;
	totalOptFarmsArea_cattle *= 0.0001;
	totalOptFarmsArea_other *= 0.0001;

	ofstream ofile ("CropAreasTotal_and_Average.txt", ios::out);
	// "This file  includes summed (over all optimising farms) areas of each crop during the whole simulation, the average areas " ;
	//"(equal to the sum divided by the number of years the simulation was run) and areas as percentage of total optimising farms area.\t" ;
	ofile << "CropName\t" << "Total\t" <<  "Average\t" << "Area_%\t"<< endl;
	for(int i=0; i<(int)m_cropTotals_sum.size(); i++){
		ofile << pm_data->Get_cropTypes_almass_str(i) << '\t';
		ofile << m_cropTotals_sum[i] << '\t';
		ofile << m_cropTotals_sum[i]/no_years << '\t';
		ofile << m_cropTotals_sum[i]/no_years/totalOptFarmsArea * 100 << '%';
		ofile  << endl;
	}
	ofile.close();

	//printing separate files for specific farm types - just areas in %.
	ofstream ofile5 ("CropAreas_plant_farms.txt", ios::out);
	ofile5 << "CropName\t" << "Area_%\t" << endl; //<< "Total\t" <<  "Average\t" <<
	for(int i=0; i<(int)m_cropTotals_plant_sum.size(); i++){
		ofile5 << pm_data->Get_cropTypes_almass_str(i) << '\t';
		ofile5 << m_cropTotals_plant_sum[i]/no_years/totalOptFarmsArea_plant * 100 << '%';
		ofile5  << endl;
	}
	ofile5.close();

	ofstream ofile6 ("CropAreas_pig_farms.txt", ios::out);
	ofile6 << "CropName\t" << "Area_%\t" << endl; //<< "Total\t" <<  "Average\t" <<
	for(int i=0; i<(int)m_cropTotals_pig_sum.size(); i++){
		ofile6 << pm_data->Get_cropTypes_almass_str(i) << '\t';
		ofile6 << m_cropTotals_pig_sum[i]/no_years/totalOptFarmsArea_pig * 100 << '%';
		ofile6  << endl;
	}
	ofile6.close();

	ofstream ofile7 ("CropAreas_cattle_farms.txt", ios::out);
	ofile7 << "CropName\t" << "Area_%\t" << endl; //<< "Total\t" <<  "Average\t" <<
	for(int i=0; i<(int)m_cropTotals_cattle_sum.size(); i++){
		ofile7 << pm_data->Get_cropTypes_almass_str(i) << '\t';
		ofile7 << m_cropTotals_cattle_sum[i]/no_years/totalOptFarmsArea_cattle * 100 << '%';
		ofile7  << endl;
	}
	ofile7.close();

	ofstream ofile8 ("CropAreas_other_farms.txt", ios::out);
	ofile8 << "CropName\t" << "Area_%\t" << endl; //<< "Total\t" <<  "Average\t" <<
	for(int i=0; i<(int)m_cropTotals_other_sum.size(); i++){
		ofile8 << pm_data->Get_cropTypes_almass_str(i) << '\t';
		ofile8 << m_cropTotals_other_sum[i]/no_years/totalOptFarmsArea_other * 100 << '%';
		ofile8  << endl;
	}
	ofile8.close();




	ofstream ofile1 ("PesticideTotals.txt", ios::out);
	ofile1 << "CropName\t" << "BIherb\t" <<  "BIfi\t" << "BI\t" ; //total values within a simulation
	ofile1 << "BIherb_yr\t" <<  "BIfi_yr\t" << "BI_yr\t"; //average values per year (total/no_years)
	ofile1 << "BIherb_ha\t" <<  "BIfi_ha\t" << "BI_ha\t"; //average values per ha (total BI/total area)
	ofile1 << endl;
	for(int i=0; i<(int)m_crops_summary_BIs.size(); i++){
		//ofile1 << m_crops_summary_BIs[i].Tov << '\t';
		ofile1 << pm_data->Get_cropTypes_almass_str(i) << '\t';

		ofile1 << m_crops_summary_BIs[i].BIherb << '\t';
		BIherb_sum += m_crops_summary_BIs[i].BIherb;

		ofile1 << m_crops_summary_BIs[i].BIfi << '\t';
		BIfi_sum += m_crops_summary_BIs[i].BIfi;

		ofile1 << m_crops_summary_BIs[i].BI << '\t';
		BI_sum += m_crops_summary_BIs[i].BI;

		ofile1 << m_crops_summary_BIs[i].BIherb/no_years << '\t';
		ofile1 << m_crops_summary_BIs[i].BIfi/no_years << '\t';
		ofile1 << m_crops_summary_BIs[i].BI/no_years << '\t';
		if(!m_cropTotals_sum[i]==0){
			ofile1 << m_crops_summary_BIs[i].BIherb/m_cropTotals_sum[i] << '\t';
			ofile1 << m_crops_summary_BIs[i].BIfi/m_cropTotals_sum[i] << '\t';
			ofile1 << m_crops_summary_BIs[i].BI/m_cropTotals_sum[i] << '\t';
		}
		else{
			ofile1 <<"0\t" << "0\t" << "0\t" ;
		}
		ofile1  << endl;
	}
	ofile1.close();

	ofstream ofile2 ("FertilizerTotals.txt", ios::out);
	ofile2 << "CropName\t" << "Fertilizer\t" <<  "Fertilizer_yr\t" << "Fertilizer_ha\t"  << endl;
	for(int i=0; i<(int)m_crops_summary_BIs.size(); i++){
		ofile2 << pm_data->Get_cropTypes_almass_str(i) << '\t';
		ofile2 << m_crops_fertilizer[i] << '\t'; //total - all farms, all years
		fertilizer_sum += m_crops_fertilizer[i];
		fertilizer_trade_sum += m_crops_fertilizer_trade[i];
		ofile2 << m_crops_fertilizer[i]/no_years << '\t'; // average per year
		if(m_cropTotals_sum[i]!=0){
			ofile2 << m_crops_fertilizer[i]/m_cropTotals_sum[i] << '\t'; //average per ha
		}
		else{
			ofile2 << "0\t";
		}
		ofile2  << endl;
	}
	ofile2.close();

	//READ the inputs on crop areas in diff model stages
	ifstream ifile("crop_results_stages.txt", ios::in);
	//check if there is an input file
    if ( !ifile.is_open() ) {
            cout << "Cannot open input file " << "crop_results_stages.txt" << endl;
            char ch;
            cin >> ch; //so that it doesn't close immedietly
            exit(1);
    }
	int no_crops = pm_data->Get_noCrops();
	//get rid of the crop names
	for(int c=0; c<no_crops; c++){
		string type;
		ifile>>type;
	}
	for(int stage=0; stage<2; stage++){ //get just the results of the baseline and of the original model
		//stage name
		string stage_name;
		ifile>>stage_name;
		//save crop areas
		for(int a=0; a<no_crops; a++){
			ifile>>m_crop_areas_stages[stage * no_crops + a];
		}
	}

	//determine the sums of squared differences in crop areas (araes as percentages)
	vector<double>sums;
	sums.resize(2);
	for(int s=0; s<2; s++){
		sums[s]=0;
	}
	
	for(int st=0; st<2; st++){
		for(int b=0; b<no_crops; b++){
			double sq_diff =  pow(m_cropTotals_sum[b]/no_years/totalOptFarmsArea * 100 - m_crop_areas_stages[st * no_crops + b]*100, 2);
			sums[st] += sq_diff;
		}
	}

	ofstream ofile3 ("Result_summary.txt", ios::out);
	ofile3 << "BIherb\t" <<  "BIfi\t" << "BI\t" << "Fert\t" << "Fert_trade\t" ;  // sum for all crops/no_years/total_opt_farms_area
	ofile3 << "Sum of sq. diff: Baseline\t" << "Sum of sq. diff: Original\t"; // << "Sum of sq. diff: New opt.\t" << "Sum of sq. diff: Almass_f.area\t" << "Sum of sq. diff: Crop_choice_ev_yr\t";
	//ofile3 << "Sum of sq. diff: Dec.modes\t" << "Sum of sq. diff: Farmer types\t" << "Sum of sq. diff: Types+modes\t" << endl;
	ofile3 << BIherb_sum/totalOptFarmsArea/no_years <<'\t';
	ofile3 << BIfi_sum/totalOptFarmsArea/no_years <<'\t';
	ofile3 << BI_sum/totalOptFarmsArea/no_years <<'\t';
	ofile3 << fertilizer_sum/totalOptFarmsArea/no_years <<'\t';
	ofile3 << fertilizer_trade_sum/totalOptFarmsArea/no_years <<'\t';
	for(int sta=0; sta<2; sta++){
		ofile3 << sums[sta] << '\t';
	}
	ofile3.close();

	//for calibration/sensitivty analysis: comparison to baseline
	ofstream ofile4 ("Output.txt", ios::out);
	//order: 1.BI 2.BI diff%; 3. BI_abs_diff; 4.fert; 5.fert diff%; 6.fert_abs_diff 7.sum of sq. diff. compared to baseline; 8. weigthed measure
	double BIdiff_percent = ((BI_sum/totalOptFarmsArea/no_years/2.2) - 1) *100;//BI in the baseline = 2.2 and is the country level score!
	double BIdiffabs = abs(BIdiff_percent);
	double fert_diff_percent =  ((fertilizer_sum/totalOptFarmsArea/no_years/140.07)-1) * 100;
	double fertdiffabs = abs(fert_diff_percent);
	//ofile4 << endl;
	ofile4 << '\t' << BI_sum/totalOptFarmsArea/no_years <<'\t' << BIdiff_percent << '\t'<< BIdiffabs  <<'\t';
	ofile4 << fertilizer_sum/totalOptFarmsArea/no_years << '\t' << fert_diff_percent << '\t' << fertdiffabs <<'\t'; //fert in baseline = 140.07 kg/ha
	ofile4 << sums[0] << '\t'; 
	ofile4 << 0.2 * abs(BIdiff_percent) + 0.2 * abs(fert_diff_percent) + 0.6 * sums[0] << endl;
	ofile4.close();

	//print also areas of all crops...
	ofstream ofile9 ("Output_crop_areas_percent.txt", ios::out);
	ofile9 << '\t';
	for(int i=0; i<(int)m_cropTotals_sum.size(); i++){
		ofile9 << m_cropTotals_sum[i]/no_years/totalOptFarmsArea * 100 << '\t';	//print just areas (no crop names!)
	}
	ofile9  << endl;
	ofile9.close();


}


void FarmManager::Switch_rotation(){

	//1. make a new rotation: include all rotational crops
	vector<TTypesOfVegetation> new_rotation;
	TTypesOfVegetation tov;
	for(int i=0; i<pm_data->Get_cropTypes_almass_size(); i++){
		tov = pm_data->Get_cropTypes_almass(i);
		if (tov!=tov_PermanentGrassGrazed && tov!=tov_PermanentGrassTussocky && tov!=tov_PermanentSetaside &&
			tov!=tov_PermanentGrassLowYield && tov!=tov_YoungForest  && tov!=tov_OrchardCrop) {
				new_rotation.push_back(tov);
		}
	}

	//2. assign it to each farm's m_rotation
	for(int i=0; i<(int)m_farms.size(); i++){
		OptimisingFarm * opf;
		if(m_farms[i]->GetType() == tof_OptimisingFarm){ //take only optimising farms
			opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			opf->Assign_rotation(new_rotation);
		}
	}
}

void FarmManager::DistributeFarmerTypes(){

	//get the shares of certain types. Parameters in the config file.
	double profit_proportion = cfg_Profit_max_proportion.value(); 
	double yield_proportion = cfg_Yield_max_proportion.value(); 
	double env_proportion = cfg_Environmentalist_proportion.value(); 

	//get the number of optimising farms (count them) and make a list of opt farms and its copy
	int number_opt_farms=0;
	vector <OptimisingFarm*> opt_farms;
	vector <OptimisingFarm*> opt_farms_copy;
	for(int i=0; i<(int)m_farms.size(); i++){
		if(m_farms[i]->GetType()==tof_OptimisingFarm){
			number_opt_farms++;
			OptimisingFarm * opf =  dynamic_cast<OptimisingFarm*>(m_farms[i]);
			opt_farms.push_back(opf);
		}
	}
	opt_farms_copy = opt_farms;

	//find the number of farms that should be of a certain type
	int number_profit = (int) (profit_proportion/100 *  number_opt_farms); // + 0.5); round down - to avoid having a total number of diff types higher than the number of farms
	int number_yield = (int) (yield_proportion/100 *  number_opt_farms + 0.5);
	int number_env = (int) (env_proportion/100 *  number_opt_farms + 0.5);

	//see: http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
	random_device rd;
    mt19937 gen(rd());

	int jj=0;
	int index_for_farm=0;

	//set farms with profit as a main goal
	for(int j=jj; j<number_profit; j++){
		 distribution_type2 dis(0, number_opt_farms - j - 1); //each time we remove one farm, so '-j' and '-1' casue the last element in the list is =size-1.
		 index_for_farm  = dis(gen);
		 opt_farms_copy[index_for_farm]->Set_main_goal(tofg_profit);
		 opt_farms_copy.erase(opt_farms_copy.begin() + index_for_farm); //remove the farm from the list after assignign the goal
		 jj = j;
	}
	jj++; //need to increase by one here!

	//set farms with yield as a main goal
	for(int j = jj; j<number_yield + number_profit; j++){ //start with the value of 'i' as it was after leaving the last for loop!
		distribution_type2 dis1(0, number_opt_farms - j - 1); //each time we remove one farm, so '-j'
		 index_for_farm  = dis1(gen);
		 opt_farms_copy[index_for_farm]->Set_main_goal(tofg_yield);
		 opt_farms_copy.erase(opt_farms_copy.begin() + index_for_farm); //remove the farm from the list after assignign the goal
		 jj = j;
	}
	jj++;

	//set farms with environment as a main goal
	int loop_limit = (number_env + number_profit + number_yield > number_opt_farms)? number_opt_farms : number_env + number_profit + number_yield;
	for(int j=jj; j<loop_limit; j++){
		distribution_type2 dis2(0, number_opt_farms - j - 1); //each time we remove one farm, so '-j'
		 index_for_farm  = dis2(gen);
		 opt_farms_copy[index_for_farm]->Set_main_goal(tofg_environment);
		 opt_farms_copy.erase(opt_farms_copy.begin() + index_for_farm); //remove the farm from the list after assignign the goal
	}

	//check if there are any farms left without assigned goal
	if((int)opt_farms_copy.size()>0){ //it should be not more than 1-2
		for(int p=(int)opt_farms_copy.size(); p>0; p--){
			opt_farms_copy[p-1]->Set_main_goal(tofg_profit);
		}
	}

}
//--------------------------------------------------------------------------------------------
//--------------------------OptimisingFarm----------------------------------------------------
//--------------------------------------------------------------------------------------------


void DataForOptimisation::InitializeVector(vector<double>&vector){

	for(int i=0; i<(int)vector.size(); i++){
		vector[i] = -1;
	}
}

/**
\brief
The constructor
*/
DataForOptimisation::DataForOptimisation(){

	int foobar = (cfg_OptimiseBedriftsmodelCrops.value())? (int)toc_Foobar : (int)tov_Undefined; //bedriftsmodel crops/almass crops

	m_cropParameters.resize(foobar*top_Foobar);
	m_alfa.resize(foobar*tos_Foobar);
	m_beta1.resize(foobar*tos_Foobar);
	m_beta2.resize(foobar*tos_Foobar);
	m_Nnorm.resize(foobar*tos_Foobar);
	m_biomass_factors.resize(foobar*tos_Foobar);
	m_fixed.resize(foobar*toof_Foobar);
	m_fodder.resize(foobar*toof_Foobar);
	m_FUKey.resize(foobar*toof_Foobar);
	m_sellingPrice.resize(foobar*toof_Foobar*tos_Foobar);
	m_sellingPrice_lastyr.resize(foobar*toof_Foobar*tos_Foobar);
	m_rotationMax.resize(foobar*toof_Foobar*tos_Foobar*tofs_Foobar);
	m_rotationMin.resize(foobar*toof_Foobar*tos_Foobar*tofs_Foobar);
	m_winterMax.resize(toof_Foobar);
	m_livestockParameters.resize(toa_Foobar*tolp_Foobar);

	if(!cfg_OptimiseBedriftsmodelCrops.value()){
		//fill them with -1
		InitializeVector (m_cropParameters);
		InitializeVector (m_alfa);
		InitializeVector (m_beta1);
		InitializeVector (m_beta2);
		InitializeVector (m_Nnorm);
		InitializeVector (m_biomass_factors);
		InitializeVector (m_FUKey);
		InitializeVector (m_sellingPrice);
		InitializeVector (m_rotationMax);
		InitializeVector (m_rotationMin);
	}
}


TTypesOfOptFarms DataForOptimisation::Get_farmType (int a_almass_no){
	for(int i=0; i< (int) m_farm_data.size(); i++){
		if(m_farm_data[i]->md_almass_no == a_almass_no) return m_farm_data[i] -> md_farmType;
	}
	g_msg->Warn( WARN_BUG, "almass_no from farms_data doesn't match any of the numbers from farmrefs file", "" ); 
	exit(0);
}

Livestock::Livestock(TTypesOfAnimals a_animalType, int a_number){
	m_animalType=a_animalType;
	m_number=a_number;
    m_NanimUsable=0;
    m_FUdemand=0;

}

/**
\brief
The constructor
*/
CropOptimised::CropOptimised(TTypesOfCrops a_cropType, double a_initialArea){
	m_cropType=a_cropType;
	m_cropType_almass = tov_Undefined; //just in case
	m_initialArea=a_initialArea;
}

CropOptimised::CropOptimised(TTypesOfVegetation a_cropType, double a_initialArea){
	m_cropType_almass = a_cropType;
	m_cropType = toc_Foobar; //just in case initialize it
	m_initialArea=a_initialArea;
}

CropOptimised::CropOptimised(){

}



void OptimisingFarm::InitiateManagement( void ) {

/**It assignes permanent crops (if there are any) to fields. In case there are no rotational crops it checks if there are any fields
that haven't been assigned any permanent crop. In case there are, it assignes to a field a first permanent crop from the list of this
farm's permanent crops.*/

	// First we need to assign the permanent crops if any.
	if (m_PermCrops.size()>0) {
		// We have something to do
		for (int i=0; i<(int)m_PermCrops.size(); i++) {
			AssignPermanentCrop(m_PermCrops[i].Tov, m_PermCrops[i].Pct);
		}

		//check if in case there are no rotational crops - all fields have been assigned a perm crop
		if(m_area_rot ==0) { //so there's  need to check
			for(int i=0; i<(int)m_fields.size(); i++){
				if(m_fields[i]->GetRotIndex() != -4){ //there is a field that is not assigned a perm crop!
					m_fields[i]->SetRotIndex(-4);
					TTypesOfVegetation tov = m_PermCrops[0].Tov; //just take the first perm crop?
					m_fields[i]->SetVegType(tov, tov_Undefined); //need a tov of one of the assigned perm crops
				}
			}
		}

	}

	Farm::InitiateManagement();
}

OptimisingFarm::OptimisingFarm(FarmManager* a_myfarmmanager, int a_No) : Farm(a_myfarmmanager)
{


	m_farmtype = tof_OptimisingFarm; //this is almass farm type (and not the optimising farm type - see OptimisingFarm::Initialize)
	m_almass_no = a_No; 
	force_deliberation = false; 
	Initialize(m_OurManager);

	//assign goals - DEFAULT  values. Later FarmManager assigns main goals again.
	m_main_goal = tofg_profit;
	//m_pest_goal = tofg_profit;
	//m_fert_goal = tofg_profit;

	//initialize the actual and expected values
	m_actual_profit=0;
	m_actual_income=0;
	m_actual_costs=0;
	m_actual_aggregated_yield=0;
	m_exp_profit=0;
	m_exp_income=0;
	m_exp_costs=0;
	m_previously_imitated_neighbour = this; 

	//initialize the dec. mode counters
	m_decision_mode_counters.resize(4);
	m_decision_mode_counters[0]=0; m_decision_mode_counters[1]=0; m_decision_mode_counters[2]=0; m_decision_mode_counters[3]=0;

	m_animals_numbers.resize(0);
	animals_no = 0;

}


void OptimisingFarm::Initialize(FarmManager * a_pfm){

	//assign to each farm: farm type, farm size, real ID and soil type + soil subtype
	int size=(int)a_pfm->pm_data->m_farm_data.size();
	for(int i=0; i<size; i++){
		if(m_almass_no==a_pfm->pm_data->m_farm_data[i]->md_almass_no){
			m_farmType=a_pfm->pm_data->m_farm_data[i]->md_farmType;
			m_farmSize=a_pfm->pm_data->m_farm_data[i]->md_farmSize;
			m_farmRealID=a_pfm->pm_data->m_farm_data[i]->md_farmRealID;
			m_soilType=a_pfm->pm_data->m_farm_data[i]->md_soilType;
			m_soilSubType=a_pfm->pm_data->m_farm_data[i]->md_soilSubType;
		}
	}

	//create livestock
	for(int i=0; i<toa_Foobar; i++){
		int index=-1;
		int livestocknum=a_pfm->pm_data->Get_livestockNumbersSize();
		int j;
		for(j=0; j<livestocknum; j += (toa_Foobar+1)){
			if((a_pfm->pm_data->Get_livestockNumber(j))==m_almass_no){
				index=j;
			}
		}
		//test for index!!
		if(index==-1){
			g_msg->Warn( WARN_BUG, "Farm's almass_no doesn't match any of the numbers within the livestock numbers vector", "" ); 
			exit(0);
		}
		int index2 = index + 1 + i;
		int number = a_pfm->pm_data->Get_livestockNumber(index2);
		if (number!=0){
			Livestock * p_lvs;
			TTypesOfAnimals livestock_type = a_pfm->pm_data->Get_livestockTypes(i);
			p_lvs = new Livestock(livestock_type, number);
			Set_Livestock(p_lvs);
		}
	}

	//create crops
	int no_crops = (cfg_OptimiseBedriftsmodelCrops.value())? toc_Foobar : a_pfm->pm_data->Get_noCrops(); //bedriftsmodel crops/almass crops

	for(int i=0; i<no_crops; i++){ //crop areas vector contains only the crops that are used, not all tov types!
		int index=-1;
		int croparea=a_pfm->pm_data->Get_cropAreasSize();
		int j;
		for(j=0; j<croparea; j+=(no_crops+1)){ //+1 becuase in the vector we store also farm numbers
			if((a_pfm->pm_data->Get_cropArea(j))==m_almass_no){
				index=j;
				break;
			}
		}
		//test for index!!
		if(index==-1){
			g_msg->Warn( WARN_BUG, "Farm's almass_no doesn't match any of the numbers within the crop areas vector", "" ); 
			exit(0);
		}
		int index2= index + 1 + i;
		double area=a_pfm->pm_data->Get_cropArea(index2);

		int foobar = (cfg_OptimiseBedriftsmodelCrops.value())? (int)toc_Foobar : (int)tov_Undefined; //bedriftsmodel crops/almass crops

		CropOptimised *p_crop;
		int index3 = -1;
		if (cfg_OptimiseBedriftsmodelCrops.value()){
			TTypesOfCrops crop_type = a_pfm->pm_data->Get_cropTypes(i);
			p_crop = new CropOptimised(crop_type, area);
			index3 = foobar*tofs_Foobar*tos_Foobar*m_farmType + foobar*tofs_Foobar*m_soilType + foobar*m_farmSize + crop_type; //crop_type is TTypesOfCrops
		}
		else{
			 TTypesOfVegetation crop_type = a_pfm->pm_data->Get_cropTypes_almass(i);
			 p_crop = new CropOptimised(crop_type, area);
			 index3 = foobar*tofs_Foobar*tos_Foobar*m_farmType + foobar*tofs_Foobar*m_soilType + foobar*m_farmSize + crop_type; //crop_type is TTypesOfVegetation
		}
		//added 01.05: save the rot max ad min
		p_crop->m_rotationMax = m_OurManager->pm_data->Get_rotationMax(index3);
		p_crop->m_rotationMin = m_OurManager->pm_data->Get_rotationMin(index3);

		//add crop to the list of crops
		Set_Crop(p_crop);
	}
}

AnimalFarm::AnimalFarm (FarmManager* a_fred, int a_No): OptimisingFarm(a_fred, a_No)
{
	m_fakeCrop = new CropOptimised(); //("FakeCrop", 0)
	cash_crops_allowed = true;
}

NonAnimalFarm::NonAnimalFarm (FarmManager* a_myfarmmanager, int a_No): OptimisingFarm(a_myfarmmanager, a_No){
	;
}

OptimisingPigFarm::OptimisingPigFarm(FarmManager* a_myfarmmanager, int a_No):AnimalFarm(a_myfarmmanager, a_No){

}

OptimisingCattleFarm::OptimisingCattleFarm(FarmManager* a_myfarmmanager, int a_No):AnimalFarm(a_myfarmmanager, a_No){
}

OptimisingPlantFarm::OptimisingPlantFarm(FarmManager* a_myfarmmanager, int a_No):NonAnimalFarm(a_myfarmmanager, a_No){

}

OptimisingOtherFarm::OptimisingOtherFarm(FarmManager* a_myfarmmanager, int a_No):NonAnimalFarm(a_myfarmmanager, a_No){
}

void OptimisingFarm::Init(ofstream * ap_output_file){

	int foobar = (cfg_OptimiseBedriftsmodelCrops.value())? (int)toc_Foobar : (int)tov_Undefined;

	FarmLevelCalculation(); //could stay in the constructor, but for clarity should be here
	createCropsLists(foobar);
	OptimiseFarm(foobar);
	Print_FarmVariables(ap_output_file);

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		Translate_crops_to_almass();
		Make_rotations();
	}
	else{ //need to create almass crop vector and do all the things that are done otherwise in Make_rotations
		Make_almass_crops_vector();
		Make_rotational_crops();
		m_rotation.push_back(tov_SpringBarley); //put there one crop for the hidden year - m_rotation is used to initialize

		//deal with perm crops (copied from make rotations):
		int area_perm = 0; //sum of areas of permanent crops
		int no_perm=0;
		for(int i = 0; i < (int)m_crops_almass.size(); i++){
			TTypesOfVegetation tov = m_crops_almass[i].Tov;

			if (tov==tov_PermanentGrassGrazed || tov==tov_PermanentGrassTussocky || tov==tov_PermanentSetaside ||
				tov==tov_PermanentGrassLowYield || tov==tov_YoungForest  || tov==tov_OrchardCrop) { //|| tov==tov_Orchard
				int pct = (int)(m_crops_almass[i].Number + 0.5); //round a double to int
				PermCropData pcd = {tov, pct};
				m_PermCrops.push_back(pcd);
				no_perm++;
				area_perm += pct;
			}
		}
	}
	if(cfg_OptimiseBedriftsmodelCrops.value()){
		Print_rotations(ap_output_file);
	}

}

void Farm::Centroids(){
/**Calculate a farm's centroid as an average of its fields' centroids.*/

	int sum_centroidx = 0;
	int sum_centroidy = 0;
	int no_fields = (int)m_fields.size();
	for (int i=0; i<no_fields; i++){
		sum_centroidx += m_fields[i]->GetCentroidX();
		sum_centroidy += m_fields[i]->GetCentroidY();
	}
	m_farm_centroidx = sum_centroidx/no_fields;
	m_farm_centroidy = sum_centroidy/no_fields;
}

bool OptimisingFarm::Spraying_herbicides( TTypesOfVegetation a_tov_type){

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		return true;
	}
	else{
		double BIherb = findCropByName_almass (a_tov_type)->m_BIHerb;
		if (BIherb > 0) return true;
		else return false;
	}
}

bool OptimisingFarm::Spraying_fungins(TTypesOfVegetation a_tov_type){

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		return true;
	}
	else{
		double BIfi = findCropByName_almass (a_tov_type)->m_BIFi;
		if (BIfi > 0) return true;
		else return false;
	}
}

double OptimisingFarm::Prob_multiplier (){

	if(m_main_goal==tofg_yield){	// || m_pest_goal == tofg_yield){ //in case we give farmers different goals - include also the pest_goal
		return cfg_Yield_max_pest_prob_multiplier.value(); 
	}
	else return 1; //for other farmer types
}

OptimisingFarm * OptimisingFarm::Find_neighbour_to_imitate(){

	/**Chooses a farmer for imitation and social comparison decision making modes. For business (>10 ha) farms it chooses a farmer
	with the same farm type and soil type. For private (<10 ha) farms it chooses a farmer with the same soil type.*/

	int no_farmers = (int)m_neighbours.size();

	if(no_farmers!=0){ //if the list is empty - no neighbours
		bool ok=false;
		vector<OptimisingFarm*>neighbours_copy = m_neighbours;
		for(int a=0; ok==false && neighbours_copy.size()>0; a++){
			srand ((unsigned)time(NULL)); //warning?
			int neighbour = rand() % (no_farmers-a); //returns a random int number from the range: <0, no_farmers-1-a> - need to subtract those remved from the list-there is 'a' removed neighbours
			m_previously_imitated_neighbour = neighbours_copy[neighbour];
			if(m_farmSize==tofs_Private){ //then look just at the soil type 
				if(m_soilType!=tos_Clay){ //private with sand or other soil
					if(m_previously_imitated_neighbour->Get_soilType()!=tos_Clay && m_previously_imitated_neighbour->Get_farmSize()==m_farmSize &&
						m_previously_imitated_neighbour->m_rotational_crops_visible.size()!=0){
						ok=true; //found somebody to imitate
					}
					else{
						neighbours_copy.erase(neighbours_copy.begin() + neighbour);
					}
				}
				else{ //private with clay
					if(m_previously_imitated_neighbour->Get_soilType()==this->Get_soilType()
						&& m_previously_imitated_neighbour->m_rotational_crops_visible.size()!=0){
						ok=true; //found somebody to imitate
					}
					else{
						neighbours_copy.erase(neighbours_copy.begin() + neighbour);
					}
				}
			}
			else if(m_soilType!=tos_Clay){ //business farm with sand or other soil
				if(m_previously_imitated_neighbour->Get_soilType()!=tos_Clay && m_previously_imitated_neighbour->Get_farmSize()==m_farmSize &&
					m_previously_imitated_neighbour->m_rotational_crops_visible.size()!=0 && m_previously_imitated_neighbour->m_farmType == m_farmType){ //the last condition added 210613
					ok=true; //found somebody to imitate
				}
				else{
					neighbours_copy.erase(neighbours_copy.begin() + neighbour);
				}
			}
			else{ //business farm with clay soil
				if(m_previously_imitated_neighbour->Get_soilType()==m_soilType && m_previously_imitated_neighbour->Get_farmSize()==m_farmSize &&
					m_previously_imitated_neighbour->m_rotational_crops_visible.size()!=0 && m_previously_imitated_neighbour->m_farmType == m_farmType){ //the last condition added 210613	){
					ok=true; //found somebody to imitate
				}
				else{
					neighbours_copy.erase(neighbours_copy.begin() + neighbour);
				}
			}

		}
		if(ok==true){ //found a neighbouring farmer to imitate
			return  m_previously_imitated_neighbour;
		}
		else{ //try to find a neighbour with at least the same farm type
			neighbours_copy = m_neighbours; //first restart the neighbours copy!
			for(int a=0; ok==false && neighbours_copy.size()>0; a++){ //040713 - do not accept a farm with empty rot crops
				srand ((unsigned)time(NULL)); //warning?
				int neighbour = rand() % (no_farmers-a); //returns a random int number from the range: <0, no_farmers-1-a> - need to subtract those remved from the list-there is 'a' removed neighbours
				m_previously_imitated_neighbour = neighbours_copy[neighbour];
				if(m_previously_imitated_neighbour->m_rotational_crops_visible.size()!=0 && m_previously_imitated_neighbour->m_farmType == m_farmType){ //ok
					ok=true; 
				}
				else{ 
					neighbours_copy.erase(neighbours_copy.begin() + neighbour);
				}
			}			
			if(ok==true){
				return  m_previously_imitated_neighbour;
			}
			else{
				m_previously_imitated_neighbour = this; //no neighbour that fits - copy yourself
				#ifdef _DEBUG
				char errornum[ 20 ];
				sprintf( errornum, "%d", m_almass_no );
				g_msg->Warn( WARN_BUG, "No farm with a matching farm type to imitate: ", errornum );
				#endif
				return  m_previously_imitated_neighbour;
			}
		}
	}


	else{ //no neighboring farmers //just set yourself
		m_previously_imitated_neighbour = this;
		return  m_previously_imitated_neighbour;
	}
}

void OptimisingFarm::Save_last_years_crops(){

	m_rotational_crops_visible = m_rotational_crops;

}

void OptimisingFarm::ChooseDecisionMode(){
	/**In the first year the function is called (8), it calls #Find_neighbour_to_imitate. Every year it determines a farmer's certainty level. Depending on farmer's satisfaction and certainty levels it picks a
	decision mode: imitation if a farmer is uncertain, but satisfied; social comparison if a farmer is uncertain and not satisfied; repetition if a farmer is certain and satisifed; or deliberation if a farmer
	is certain, but not satisifed.*/

	//pick a farmer to copy (imitation startegy)/ or compare with and possibly copy (social comparison strategy) if this is the first time this function is called: year 8
	if(g_date->GetYearNumber() == 8){
		Find_neighbour_to_imitate();
	}

	//1. get the values of parameters
	double min_certainty = cfg_Min_certainty.value(); 
	if(cfg_OnlyDeliberation.value()) min_certainty = 0; 

	//2. determine the satisfaction level: already done in the ActualProfit()

	//3. determine the certainty level
	double sum_of_sat_levels=0;
	for(int i=0; i<5; i++){
		sum_of_sat_levels += m_previous_satisfaction_levels[i];
	}
	m_certainty_level = sum_of_sat_levels/5.0; // i.e. take 5 years into account

	//4. choose the decision mode
	//'if' for forcing deliberation:
	if(force_deliberation){ //do the same as when deliberation is chosen normally
		m_decision_mode_counters[3]++;
		m_totalFUdemand=m_totalFUdemandBefore; //restart the fodder demand to the original value
		m_totalFUgrown = 0; //restart
		OptimiseFarm(tov_Undefined);
		Make_almass_crops_vector(); //makes m_crops_almass
		Make_rotational_crops(); //makes new m_rotational_crops - based on m_crops_almass
		m_rotational_crops_copy = m_rotational_crops;
		force_deliberation=false;
	}
	else{
		if(m_certainty_level < min_certainty){
			if(m_need_satisfaction_level  == 1 ){						//IMITATION (high sat., low cert.)
				m_decision_mode_counters[0]++;
				//now imitate:
				m_rotational_crops = m_previously_imitated_neighbour->Get_rotational_crops_visible();
				m_rotational_crops_copy = m_rotational_crops; //m_rotational_crops_copy is used for finding a crop for a given field - in Match_crop_to_field function.
			} //imitation over

			else{														//SOCIAL COMPARISON - similar to imitation, but includes comparison (low sat., low cert.)
				m_decision_mode_counters[1]++;
				//compare yourself to the neighbour: this depends on your goal
				bool imitate=false;
				if(m_main_goal == tofg_profit){
					if(m_actual_profit < m_previously_imitated_neighbour->Get_actual_profit()){
						imitate=true;
					}
				}
				else if (m_main_goal == tofg_yield){
					if(m_actual_aggregated_yield < m_previously_imitated_neighbour->Get_actual_aggregated_yield()){
						imitate=true;
					}
				}
				else if (m_main_goal == tofg_environment) {
					if(m_actual_aggregated_yield < m_previously_imitated_neighbour->Get_actual_aggregated_yield() && m_actual_profit < m_previously_imitated_neighbour->Get_actual_profit()){//compare both
						imitate=true;
					}
				}

				if(imitate){
					m_rotational_crops = m_previously_imitated_neighbour->Get_rotational_crops_visible();
				}
				//else: just continue
				m_rotational_crops_copy = m_rotational_crops; //this happens for both cases - imitate or not
			} //social comparison over
		} //high uncertainty

		else{ //low uncertainty (= high certainty)
			int value_for_satisfaction = 1; //1 for a simulation with not only deliberation
			if(cfg_OnlyDeliberation.value()) value_for_satisfaction = 10; //10 - then everybody deliberates 
			if(m_need_satisfaction_level == value_for_satisfaction){							//REPETITION (high sat., high cert.)
				m_decision_mode_counters[2]++;
				//continue with the same set of crops - see line after else!
			}
			else{														//DELIBERATION - i.e. optimise to make a new m_rotational_crops (low sat., high cert.)
				m_decision_mode_counters[3]++;
				m_totalFUdemand=m_totalFUdemandBefore; //restart the fodder demand to the original value
				m_totalFUgrown = 0; //restart
				OptimiseFarm(tov_Undefined);
				Make_almass_crops_vector(); //makes m_crops_almass
				Make_rotational_crops(); //makes new m_rotational_crops - based on m_crops_almass
			}
			m_rotational_crops_copy = m_rotational_crops; //do this for both cases
		}
	}
}

void OptimisingFarm::createCropsLists(int a_foobar){
	createVariableCrops(a_foobar);
}

void AnimalFarm::createCropsLists(int a_foobar){
	OptimisingFarm::createVariableCrops(a_foobar);
	createFodderCrops(a_foobar);
}

void OptimisingFarm::createVariableCrops(int a_foobar){
/**Creates two vectors: vector of pointers to fixed crops and a vector of structs containing variable crops.*/

    int n = (int)m_crops.size();
    for (int i=0; i < n; ++i) {
		int crop_type=(cfg_OptimiseBedriftsmodelCrops.value())? (int)m_crops[i]->m_cropType : (int)m_crops[i]->m_cropType_almass;
		int index=a_foobar*m_farmType + crop_type;
		if (m_OurManager->pm_data->Get_fixed(index)){ //if the crop is fixed, attach it to the list fixedCrops
            m_fixedCrops.push_back(m_crops[i]);
        }
        else{
            CropSort cs = {0., m_crops[i]}; //an object with a key and a pointer to crop
            m_variableCrops.push_back(cs);
        }
    }
}

void AnimalFarm::createFodderCrops(int a_foobar){
/**Creates a vector of pointers to variable fodder crops (those fixed excluded!).*/

    int n = (int)m_crops.size();
    for (int i=0; i < n; ++i) {
		int crop_type=(cfg_OptimiseBedriftsmodelCrops.value())? (int)m_crops[i]->m_cropType : (int)m_crops[i]->m_cropType_almass;
		int index = a_foobar*m_farmType + crop_type;
        if(m_OurManager->pm_data->Get_fodder(index)){ //if the crop is a fodder crop...
            if(!(m_OurManager->pm_data->Get_fixed(index))){ //check if it is not fixed and attach it to the list
                CropSort cs={0., m_crops[i]};
                m_fodderCrops.push_back(cs);
            }
        }
    }
}

/**Calls functions determining farm level values before the initial optimisation (i.e.: amount of animal fertilizer (total - #m_totalNanim,  and per ha - #m_Nanim), farm's area - #m_totalArea, and fodder demand - #m_totalFUdemand).*/
void OptimisingFarm::FarmLevelCalculation() {

     findTotalArea();
     findTotalNanim();
     findNanim();
     findFodderDemand(); //this function cannot be called in the constructor!
	 if(cfg_Areas_Based_on_Distribution.value()){
		 preventCashCrops();
	 }
}

void OptimisingFarm::findTotalArea(){
/**Function determining farm's total area. Depending on which area is to be used it either sums initial crop areas on a farm (to get area equal to area
used in Bedriftsmodel) or deteremines area based on farm's fields size.*/

	m_area_scaling_factor = 0;
	m_totalArea=0.;
	for (int i=0; i< (int) m_crops.size(); i++) {
		m_totalArea+=m_crops[i]->m_initialArea;
	}
	m_totalArea_original = m_totalArea;

	if(!cfg_UseBedriftsmodelFarmAreas.value()){ //almass farm areas used: just sum the fields area
		double area_copy = m_totalArea; //ha
		m_totalArea = GetAreaDouble()/10000; //change from sq m to ha
		m_area_scaling_factor = m_totalArea/area_copy;
	}
	//otherwise do nothing: total area = sum of initial crop areas (see the for loop)
}

void OptimisingFarm::findTotalNanim(){
/**Function determining farm's total animal fertilizer. Sums usable fertilizer from all types of livestock.*/

     m_totalNanim = 0;
     for (int i=0; i< (int) m_livestock.size(); i++){
		 string str_AUKey="AUKey";
		 string str_Nusable="Nusable";
		 TTypesOfAnimals index=m_livestock[i]->m_animalType;
		 double AUKey = m_OurManager->pm_data->Get_livestockParameter(tolp_Foobar*index + m_OurManager->TranslateLivestockParametersCodes(str_AUKey)); //Number of animal units (AU, DE-dyreenheder) -> hkg N per year
         double Nusable = m_OurManager->pm_data->Get_livestockParameter(tolp_Foobar*index + m_OurManager->TranslateLivestockParametersCodes(str_Nusable)); // [%] Usable fertilizer from livestock
		 double number = m_livestock[i]->m_number;

		 if(!cfg_UseBedriftsmodelFarmAreas.value()){ //almass farm areas - need to scale the livestock numbers; we don't even have to take integer...
			number *= m_area_scaling_factor;
		 }

         double NanimUsable = number*AUKey*Nusable*100; // [1*hkg/yr*1*100 = kg/yr] Usable fertilizer from livestock
         m_livestock[i]->m_NanimUsable = NanimUsable;
         m_totalNanim += NanimUsable;
     }
}

void OptimisingFarm::findNanim(){
/**Determines #m_Nanim - amount of animal fertilizer per ha for a farm.*/
    m_Nanim = m_totalNanim/m_totalArea;
 }

void OptimisingFarm::findFodderDemand(){
/**Function determining farm's demand for fodder. Sums fodder needed for all types of livestock.*/

    m_totalFUdemand=0;
    for(int i=0; i< (int) m_livestock.size(); i++){
		string str_FUuKey="FUuKey";
		TTypesOfAnimals index=m_livestock[i]->m_animalType;
        double FUuKey=m_OurManager->pm_data->Get_livestockParameter(tolp_Foobar*index + m_OurManager->TranslateLivestockParametersCodes(str_FUuKey)); //number of fodder units needed per one animal per year
        double number = m_livestock[i]->m_number;

		if(!cfg_UseBedriftsmodelFarmAreas.value()){ //almass farm areas - need to scale the livestock numbers; we don't even have to take integer...
			number *= m_area_scaling_factor;
		}

        if(number>0){
            double FUdemand=FUuKey*number;
			m_livestock[i]->m_FUdemand = FUdemand;
            m_totalFUdemand+=FUdemand;
        }
    }
    m_totalFUdemandBefore=m_totalFUdemand; //just to know afterwards the total demand
}

void NonAnimalFarm::findFodderDemand(){

/**For non-animal farms, the function assigns the value of the total fodder demand to the variable #m_totalFUt (total fodder purchased)
and it sets #m_totalFUgrown (total fodder grown) to zero.*/

	OptimisingFarm::findFodderDemand();
    m_totalFUt=m_totalFUdemand;
    m_totalFUdemand=0;
    m_totalFUgrown=0;
}

void OptimisingCattleFarm::preventCashCrops(){

/**Prevents cattle farms on sandy soils with area < 15 ha from growing cash crops. For cattle farms on sandy soils larger than 15 ha and with the worse soil subtype - it prevents cash crops in 90% of cases.
Farms above 15 ha with a good soil subtype are allowed to grow cash crops.
Additionally, the function prevents cattle farms with area < 30 ha from growing maize silage.*/

	CropOptimised *ms = findCropByName_almass ("MaizeSilage");
	if(m_totalArea_original < 30){ //cattle farms with area < 30 ha do not have maize silage!
		ms->m_rotationMax = 0;
	}

	CropOptimised *wwheat = findCropByName_almass ("WinterWheat");
	CropOptimised *wrye = findCropByName_almass ("WinterRye");
	CropOptimised *srape = findCropByName_almass ("SpringRape");
	CropOptimised *wrape = findCropByName_almass ("WinterRape");
	CropOptimised *fpeas = findCropByName_almass ("FieldPeas");
	CropOptimised *potatoes = findCropByName_almass ("Potatoes");
	CropOptimised *potind = findCropByName_almass ("PotatoesIndustry");
	CropOptimised *seedg1 = findCropByName_almass ("SeedGrass1");
	CropOptimised *seedg2 = findCropByName_almass ("SeedGrass2");

	if(m_soilType==tos_Sand && m_totalArea_original < 15){

		wwheat->m_rotationMax = 0;
		wrye->m_rotationMax = 0;
		srape->m_rotationMax = 0;
		wrape->m_rotationMax = 0;
		fpeas->m_rotationMax = 0;
		potatoes->m_rotationMax = 0;
		potind->m_rotationMax = 0;
		seedg1->m_rotationMax = 0;
		seedg2->m_rotationMax = 0;

		cash_crops_allowed=false;
	}
	else if(m_soilType==tos_Sand && m_totalArea_original >= 15 && m_soilSubType == 0){ //v. small prob. of having cash crops

			//see: http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
			std::random_device rd;
			std::mt19937 gen(rd());
			distribution_type2 dis(0, 1);
			double random_no = dis(gen);

			if(random_no > 0.1){ //so in 90% of cases
				wwheat->m_rotationMax = 0;
				wrye->m_rotationMax = 0;
				srape->m_rotationMax = 0;
				wrape->m_rotationMax = 0;
				fpeas->m_rotationMax = 0;
				potatoes->m_rotationMax = 0;
				potind->m_rotationMax = 0;
				seedg1->m_rotationMax = 0;
				seedg2->m_rotationMax = 0;

				cash_crops_allowed=false;
			}
	}
	//else nothing - allow cash crops

}

void OptimisingFarm::OptimiseFarm(int a_foobar){

    optimizeCrops(a_foobar); 
    sortCrops(m_variableCrops, "GM" );//ORDER OPTIMISED CROPS WRT HIGHEST GM
    randomizeCropList(m_variableCrops, "GM"); //shuffles elements of the list with equal GMs
	assignFixed();
    sumMinAreas();
    determineAreas(a_foobar);

    if(m_farmSize == tofs_Business){ 
        checkRestrictions();
    }

	Check_if_area_100();

    determineAreas_ha(m_crops);

    m_totalN=total(tocv_N);
    m_totalNt=total(tocv_Nt); //total amount of Nt at a farm - summed for all crops
    m_totalBIHerb=total(tocv_BIHerb);
    m_totalBIFi=total(tocv_BIFi);
    m_totalBI=total(tocv_BI);
    m_totalGrooming=total(tocv_Grooming);
    m_totalHoeing=total(tocv_Hoeing);
    m_totalWeeding=total(tocv_Weeding);
    m_totalIncome=total(tocv_Income);
    m_totalCosts=total(tocv_Costs);
	if(m_totalFUt>0) m_totalProfit = total(tocv_GM) - m_totalFUt * cfg_Price_FU.value();
    else m_totalProfit = total(tocv_GM);
}

void OptimisingFarm::optimizeCrops(int a_foobar){

	/**Function carrying out the optimisation of each crop for a given farm. It contains functions determining the optimal amounts (per ha) of fertilizer (animal and purchased),
	pesticides and mechanical weed control as well as functions finding response (yield per ha), yield loss and gross margin (profitability) of each crop.*/

    for (int i=0; i< (int) m_crops.size(); i++){

        //1. define the benefit; for non-fodder crops it is simply a selling price ;
		double benefit;
		int crop_type=(cfg_OptimiseBedriftsmodelCrops.value())? (int)m_crops[i]->m_cropType : (int)m_crops[i]->m_cropType_almass;
		int index = a_foobar*m_farmType + crop_type;
		bool fodder = m_OurManager->pm_data->Get_fodder(index);
        if(fodder){
			double FUKey = m_OurManager->pm_data->Get_FUKey(index);
			benefit = FUKey * cfg_Price_FU.value(); 
        }
		else {benefit = m_OurManager->pm_data->Get_sellingPrice(a_foobar*tos_Foobar*m_farmType + a_foobar*m_soilType + crop_type);}

		CropOptimised * crop = m_crops[i]; //define a pointer which is passed in the functions below

		crop->m_benefit = benefit; //added 090114

		//for SA: modify the prices of crops included in the analysis
		if(cfg_Sensitivity_analysis.value()){ //modify the prices only if this is a sensitivity analysis/calibration run
			if(cfg_OptimiseBedriftsmodelCrops.value()){
				if(crop_type==toc_Oats){ 
					if(m_farmType != toof_Pig && m_farmType != toof_Cattle){//don't modify for these farms - for them the price is zero for this crop - becasue it is a fodder crop!
						benefit = cfg_Price_Oats.value();
					}
				}
				else if(crop_type==toc_WWheat){ //not fodder
					benefit = cfg_Price_WWheat.value();
				}
			}
			else{//almass crop set
				if(crop_type==tov_SpringBarley){ //fodder for cattle and pig farms: price is zero if crop is a fodder crop. so changing price will affect only non-animal farms.
					if(m_farmType != toof_Pig && m_farmType != toof_Cattle){//don't modify for these farms - for them the price is zero for this crop - becasue it is a fodder crop!
						benefit = cfg_Price_SBarley.value();
					}
				}
				else if(crop_type==tov_Oats){ //f for c and p
					if(m_farmType != toof_Pig && m_farmType != toof_Cattle){//don't modify for these farms - for them the price is zero for this crop - becasue it is a fodder crop!
						benefit = cfg_Price_Oats.value();
					}
				}
				else if(crop_type==tov_WinterBarley){ //f for c and p
					if(m_farmType != toof_Pig && m_farmType != toof_Cattle){//don't modify for these farms - for them the price is zero for this crop - becasue it is a fodder crop!
						benefit = cfg_Price_WBarley.value();
					}
				}
				else if(crop_type==tov_WinterWheat){ //not fodder
					benefit = cfg_Price_WWheat.value();
				}
				else if(crop_type==tov_Triticale){ 
					if(m_farmType != toof_Cattle){//don't modify for these farms - for them the price is zero for this crop - becasue it is a fodder crop!
						benefit = cfg_Price_Triticale.value();
					}
				}
				else if(crop_type==tov_WinterRape){ //not fodder
					benefit = cfg_Price_WRape.value();
				}
				else if(crop_type==tov_SpringRape){ //not fodder
					benefit = cfg_Price_SRape.value();
				}
			}
		}


        //2. optimize the crop
        findFertilizer(crop, a_foobar, benefit);
        findResponse(crop, a_foobar);
        findBIs(crop, benefit);
		fixBI(); //added 22.03.13
        findMWeedControl(crop);
        findYieldLoss(crop);
        findGrossMargin(crop, a_foobar, benefit);
        
    }
}


void OptimisingFarm::Check_SG_and_CGG(){

	CropOptimised *CGG1 = findCropByName_almass("CloverGrassGrazed1");
	CropOptimised *CGG2 = findCropByName_almass("CloverGrassGrazed2");
	CropOptimised *GS1 = findCropByName_almass("SeedGrass1");
	CropOptimised *GS2 = findCropByName_almass("SeedGrass2");

	double CGG1area = CGG1->m_areaPercent;
	double CGG2area = CGG2->m_areaPercent;
	double GS1area = GS1->m_areaPercent;
	double GS2area = GS2->m_areaPercent;
	if(CGG1area!= CGG2area){ //even if this is a cattle farm - where CGG are fodder - it won't have any effect here since the areas are zero or at min, so there won't be any change.
		double area_sum = CGG1area + CGG2area;
		double CGG1min = CGG1->m_rotationMin; double CGG2min = CGG2->m_rotationMin;
		CGG1->m_areaPercent = area_sum/2; CGG2->m_areaPercent = area_sum/2;
		CGG1->m_areaVariable = area_sum/2 - CGG1min; CGG2->m_areaVariable = area_sum/2 - CGG2min;
	}
	if(GS1area!=GS2area){
		double area_sum = GS1area + GS2area;
		double GS1min = GS1->m_rotationMin; double GS2min = GS2->m_rotationMin;
		GS1->m_areaPercent = area_sum/2; GS2->m_areaPercent = area_sum/2;
		GS1->m_areaVariable = area_sum/2 - GS1min; GS2->m_areaVariable = area_sum/2 - GS2min;
	}
}

//-------------------------------------------------------------------------------------
//------------------OPTIMISATION - THE SIMPLIFIED METHOD-------------------------------
//-------------------------------------------------------------------------------------

void OptimisingFarm::findFertilizer(CropOptimised * a_crop, int a_foobar, double benefit){

/**Function determining the optimal amounts of: total fertilizer (#CropOptimised::m_n) and purchased
fertilizer (#CropOptimised::m_nt) per ha of a crop at a given farm. [kg/ha]*/

	int index = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar * m_soilType + a_crop->m_cropType : a_foobar * m_soilType + a_crop->m_cropType_almass;

	double beta1=m_OurManager->pm_data->Get_beta1(index);
    double beta2=m_OurManager->pm_data->Get_beta2(index);
	double nNorm=m_OurManager->pm_data->Get_Nnorm(index);
	double priceNt=cfg_Price_Nt.value(); 

	if(!beta2==0){
		a_crop->m_optimalN = -(beta1/beta2)*0.5; //01.03.12 save the optimal free fertilizer 
	}
	else{ //if beta2 is =0, beta1 is also =0 -> so optimum is at 0.
		a_crop->m_optimalN = 0;
	}

	if(m_main_goal == tofg_profit){

		if(!beta2==0){ //beta2 is not 0
			double n1=-(beta1/beta2)*0.5; //optimal fertilizer supposing it's for free
			if(m_Nanim >= n1){ //there is more than you need, so check the norm:
				if (n1<nNorm){
				   a_crop->m_n = n1; //apply optimal amount
				}
				else {
				   a_crop->m_n = nNorm; //apply max allowed amount
				}
				a_crop->m_nt = 0;  //don't buy fertilizer
			}
			else { //the optimal amount is larger than Nanim
			   double nt1= 0.5*(priceNt/(benefit*beta2) - beta1/beta2) - m_Nanim; //optimal fertilizer Nt
			   if (nt1>0){
				   double n2=nt1+m_Nanim; //optimal total N
				   if (n2<nNorm){
					 a_crop->m_nt = nt1;
					 a_crop->m_n = n2;
				   }
				   else {
					   double p=nNorm-m_Nanim;
					   if(p>=0){a_crop->m_nt = p;} //buy the diff between what you have (Nanim) and what is the max allowed
					   else{a_crop->m_nt = 0;}// your Nanim is higher than the norm!
					   a_crop->m_n = nNorm; //total amount is the max allowed, Nanim+(nNorm-Nanim)=nNorm
				   }
			   }
			   else { //it doesn't pay off to buy fertilizer, so just use Nanim
				   if(m_Nanim<=nNorm){a_crop->m_n = m_Nanim;} //apply fertilizer
				   else {a_crop->m_n = nNorm;}
				   a_crop->m_nt = 0;
			   }
			}
		}
		else{ //beta2=0, so don't apply fertilizer, but in this model - apply Nanim if there is any
			if(m_Nanim<=nNorm){a_crop->m_n = m_Nanim;} //apply fertilizer
			else {a_crop->m_n = nNorm;}
			a_crop->m_nt = 0;
		}

	}

	else if (m_main_goal == tofg_yield) { //modified profit optimization algorithm
		if(!beta2==0){ //beta2 is not 0
			double n1=-(beta1/beta2)*0.5; //optimal fertilizer supposing it's for free
			if(m_Nanim >= n1){ //there is more than you need, so check the norm:
				if (n1<nNorm){
				   a_crop->m_n = n1; //apply optimal amount
				}
				else {
				   a_crop->m_n = nNorm; //apply max allowed amount
				}
				a_crop->m_nt = 0;  //don't buy fertilizer
			}
			else { //the optimal amount is larger than Nanim
				if(n1>=nNorm){
					if(m_Nanim > nNorm){a_crop->m_nt = 0;}
					else{a_crop->m_nt = nNorm - m_Nanim;}//buy the diff between what you have (Nanim) and what is the max allowed
					a_crop->m_n = nNorm; //total amount is the max allowed
				}
				else{
					a_crop->m_nt = n1 - m_Nanim;
					a_crop->m_n = n1;
				}
			}
		}
		else{ //beta2=0, so don't apply fertilizer, but in this model - apply Nanim if there is any
			if(m_Nanim<=nNorm){a_crop->m_n = m_Nanim;} //apply fertilizer
			else {a_crop->m_n = nNorm;}
			a_crop->m_nt = 0;
		}

	}
	else if (m_main_goal == tofg_environment) { //apply less than a norm: the code copied from yield max - the only change is in calculation of n1 - mulitplied by 0.8.
		if(!beta2==0){ //beta2 is not 0
			double n1=-(beta1/beta2)*0.5 * cfg_Env_fert_multiplier.value(); //optimal fertilizer supposing it's for free; multiplier - to make this type use less fertilizer
			if(m_Nanim >= n1){ //there is more than you need, so check the norm:
				if (n1<nNorm){
				   a_crop->m_n = n1; //apply optimal amount
				}
				else {
				   a_crop->m_n = nNorm; //apply max allowed amount
				}
				a_crop->m_nt = 0;  //don't buy fertilizer
			}
			else { //the optimal amount is larger than Nanim
				if(n1>=nNorm){
					if(m_Nanim > nNorm){a_crop->m_nt = 0;}
					else{a_crop->m_nt = nNorm - m_Nanim;}//buy the diff between what you have (Nanim) and what is the max allowed
					a_crop->m_n = nNorm; //total amount is the max allowed
				}
				else{
					a_crop->m_nt = n1 - m_Nanim;
					a_crop->m_n = n1;
				}
			}
		}
		else{ //beta2=0, so don't apply fertilizer, but in this model - apply Nanim if there is any
			if(m_Nanim<=nNorm){a_crop->m_n = m_Nanim;} //apply fertilizer
			else {a_crop->m_n = nNorm;}
			a_crop->m_nt = 0;
		}
	}
}

void OptimisingFarm::findResponse (CropOptimised * a_crop, int a_foobar){

/**Function determining the response of a crop (yield per ha), #CropOptimised::m_resp. It is a function of a total fertilizer (#CropOptimised::m_n) applied
for a crop at a given farm. [hkg/ha]*/

	int index = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar * m_soilType + a_crop->m_cropType : a_foobar * m_soilType + a_crop->m_cropType_almass;

    double beta1=m_OurManager->pm_data->Get_beta1(index);
    double beta2=m_OurManager->pm_data->Get_beta2(index);
	double alfa=m_OurManager->pm_data->Get_alfa(index);

	double ntotal=a_crop->m_n;
    double resp1=alfa + beta1*ntotal + beta2*pow(ntotal,2);
    a_crop->m_resp = resp1;
}

void OptimisingFarm::findBIs(CropOptimised * a_crop, double benefit){ 
/**Function determining the optimal 'behandling indeks' (treatment frequency index) for herbicides (#CropOptimised::m_BIHerb), fungicides and insecticides (#CropOptimised::m_BIFi),
and a summary BI per ha (#CropOptimised::m_BIHerb). For each crop there is a specified amount of herbicides, fungicides and insecticides for which the yield loss is equal to zero. The farmer can choose between
this value and not using pesticide at all (since yield and profit are linear functions of pesticide usage). However, when a given crop is actually grown, farmer might not apply pesticide if there is no pest problem
(even though he originally planned to apply pesticides), or might apply more than he planned to if there is an excess pest problem. Therefore, there is a variation in the actual pesticide usage between farmers.*/

	int index = (cfg_OptimiseBedriftsmodelCrops.value())? (int)a_crop->m_cropType : (int)a_crop->m_cropType_almass;

	double alfaFi = crop_parameter(index, "AlfaFi");
    double alfaHerb = crop_parameter(index, "AlfaHerb");
    double alfaG = crop_parameter(index, "AlfaG");
    double alfaH = crop_parameter(index, "AlfaH");
    double alfaW = crop_parameter(index, "AlfaW");
    double betaFi = crop_parameter(index, "BetaFi");
    double betaHerb = crop_parameter(index, "BetaHerb");

    double priceFi = crop_parameter(index, "PriceFi");
    double priceHerb = crop_parameter(index, "PriceHerb");
    double priceG = crop_parameter(index, "PriceG");
    double priceH = crop_parameter(index, "PriceH");
    double priceW = crop_parameter(index, "PriceW");

    double resp1=a_crop->m_resp;

    if(alfaHerb>0){//make sure it is not zero!it is for Fodder beet
        double BIHerbMax = betaHerb/alfaHerb;
		double gainHerb = 0;
		double BIHerb = 0;

		if(m_main_goal == tofg_profit){
			gainHerb = benefit*resp1*alfaHerb/100 + priceG*alfaG + priceH*alfaH + priceW*alfaW;
			BIHerb = (gainHerb>priceHerb)? BIHerbMax : 0;
			a_crop->m_BIHerb = BIHerb;
		}
		else if (m_main_goal == tofg_yield){
			a_crop->m_BIHerb = BIHerbMax; //12.12.12
		}
		else if (m_main_goal == tofg_environment){ 
			gainHerb = benefit*resp1*alfaHerb/100 + priceG*alfaG + priceH*alfaH + priceW*alfaW;
			BIHerb = (gainHerb> (priceHerb * cfg_Env_pest_multiplier.value()))? BIHerbMax : 0; 
			a_crop->m_BIHerb = BIHerb;
		}
    }
    else a_crop->m_BIHerb = 0; //BI for fodder beet in the original model (Bedriftsmodel) 

    if(alfaFi>0){
		double BIFiMax = betaFi/alfaFi;
		double gainFi = 0;
		double BIFi = 0;
		if(m_main_goal == tofg_profit){
			gainFi = benefit*resp1*alfaFi/100;
			BIFi = (gainFi > priceFi) ? BIFiMax : 0;
			a_crop->m_BIFi = BIFi;
		}
		else if (m_main_goal == tofg_yield){
			a_crop->m_BIFi = BIFiMax;
		}
		else if (m_main_goal == tofg_environment){ 
			gainFi = benefit*resp1*alfaFi/100;
			if (gainFi > (priceFi * cfg_Env_pest_multiplier.value())){ 
				BIFi = BIFiMax;
			}
			else{
				BIFi = 0;
			}
			a_crop->m_BIFi = BIFi;
		}
    }
    else a_crop->m_BIFi = 0;

    double BIFi = a_crop->m_BIFi;
    double BIHerb = a_crop->m_BIHerb;
    double BI = BIFi+BIHerb;
    a_crop->m_BI = BI;
}

void OptimisingFarm::fixBI(){

	if(cfg_OptimiseBedriftsmodelCrops.value()){ //b. crops
		CropOptimised *fodderBeet = findCropByName ("FodderBeet");
		CropOptimised *sugarBeet = findCropByName ("SugarBeet");
		CropOptimised *potato = findCropByName ("Potato");
		CropOptimised *potatoFood = findCropByName ("PotatoFood");

		fodderBeet->m_BIHerb = 2.28; //seems it's constant - in the results of Bedriftsmodel, but herbicide parameters are = zero!
		sugarBeet->m_BIHerb = 2.28;
		fodderBeet->m_BI = fodderBeet->m_BIFi + fodderBeet->m_BIHerb;
		sugarBeet->m_BI = sugarBeet->m_BIFi + sugarBeet->m_BIHerb;

		//potatoes - everything fixed
		potato->m_BIHerb = 1.41;	potato->m_BIFi = 9.28;	potato->m_BI = 10.69;
		potatoFood->m_BIHerb = 1.41;	potatoFood->m_BIFi = 9.28;	potatoFood->m_BI = 10.69;
	}
	else{ //almass crops
		CropOptimised *FodderBeet = findCropByName_almass ("FodderBeet");
		CropOptimised *PotatoesIndustry = findCropByName_almass ("PotatoesIndustry");
		CropOptimised *Potatoes = findCropByName_almass ("Potatoes");

		FodderBeet->m_BIHerb = 2.28; //the values have to be now like in the bedriftsmodel //modified probability
		FodderBeet->m_BI = FodderBeet->m_BIHerb + FodderBeet->m_BIFi;
		PotatoesIndustry->m_BIHerb = 1.41; PotatoesIndustry->m_BIFi = 9.28; PotatoesIndustry->m_BI = 10.69;
		Potatoes->m_BIHerb = 1.41; Potatoes->m_BIFi = 9.28; Potatoes->m_BI = 10.69;
	}

}

void OptimisingFarm::findMWeedControl(CropOptimised * a_crop){
/**Function determining optimal values of mechanical weed control means: grooming (#CropOptimised::m_grooming), hoeing (#CropOptimised::m_hoeing) and weeding (#CropOptimised::m_weeding).
All of them are functions of treatment frequency index (behandling indeks) for herbicides (#CropOptimised::m_BIHerb).*/

	int index = (cfg_OptimiseBedriftsmodelCrops.value())? (int) a_crop->m_cropType : (int) a_crop->m_cropType_almass;

	double betaG = crop_parameter(index, "BetaG");
    double betaH = crop_parameter(index, "BetaH");
    double betaW = crop_parameter(index, "BetaW");
    double alfaG = crop_parameter(index, "AlfaG");
    double alfaH = crop_parameter(index, "AlfaH");
    double alfaW = crop_parameter(index, "AlfaW");
	double BIHerb = a_crop->m_BIHerb;

    double grooming, hoeing, weeding =0;
	double g = betaG - BIHerb*alfaG;
    double h = betaH - BIHerb*alfaH;
	double w = betaW - BIHerb*alfaW;
	double BIHerbCorr = BIHerb;

	if(g <= 0){
		grooming=0;
		if(alfaG!=0) BIHerbCorr=betaG/alfaG;
	}
	else{grooming=g;}
	if(h <= 0){
		hoeing=0;
		if(betaH/alfaH < BIHerbCorr) BIHerbCorr=betaH/alfaH;
	}
	else{hoeing=h;}
	if(w <= 0){
		weeding=0;
		if(betaW/alfaW < BIHerbCorr) BIHerbCorr = betaW/alfaW;
	}
	else{weeding=w;}


    a_crop->m_grooming = grooming;
    a_crop->m_hoeing = hoeing;
    a_crop->m_weeding = weeding;
	if(BIHerbCorr < BIHerb){
		a_crop->m_BIHerb = BIHerbCorr;
		a_crop->m_BI = a_crop->m_BIHerb + a_crop->m_BIFi;
	}

}

void OptimisingFarm::findYieldLoss(CropOptimised * a_crop){
/**Function determining yield losses resulting from not using maximal amount of herbicides (#CropOptimised::m_lossHerb),
fungicides and insecticides (#CropOptimised::m_lossFi) and a total yield loss, which is a sum of m_lossHerb and m_lossFi (#CropOptimised::m_totalLoss).
Losses are expressed as percentages of yield (take values from 0 to 100).*/

	int index = (cfg_OptimiseBedriftsmodelCrops.value())? (int) a_crop->m_cropType : (int) a_crop->m_cropType_almass;

	double alfaFi = crop_parameter(index, "AlfaFi");
    double alfaHerb = crop_parameter(index, "AlfaHerb");
    double betaFi = crop_parameter(index, "BetaFi");
    double betaHerb = crop_parameter(index, "BetaHerb");

    double BIHerb=a_crop->m_BIHerb;
    double BIFi=a_crop->m_BIFi;

    double lossHerb = betaHerb - alfaHerb*BIHerb;
	a_crop->m_lossHerb = lossHerb;

    double lossFi = betaFi - alfaFi*BIFi;
	a_crop->m_lossFi = lossFi;

    double totalLoss = lossHerb + lossFi;// [%]
	a_crop->m_totalLoss = totalLoss;
}

void OptimisingFarm::findGrossMargin(CropOptimised * a_crop, int a_foobar, double benefit){
/**Function determining gross margin (#CropOptimised::m_GM, i.e. profit per ha for a given crop; it might be positive only for non-fodder crops)
and savings (#CropOptimised::m_savings, savings from growing a fodder crop per ha, it equals zero for all non-fodder crops). [DKK]*/

	int index = (cfg_OptimiseBedriftsmodelCrops.value())? (int) a_crop->m_cropType : (int) a_crop->m_cropType_almass;

    double priceFi = crop_parameter(index, "PriceFi");
    double priceHerb =crop_parameter(index, "PriceHerb");
    double priceG =crop_parameter(index, "PriceG");
    double priceH =crop_parameter(index, "PriceH");
    double priceW =crop_parameter(index, "PriceW");
    double priceLM =crop_parameter(index, "PriceLM");
    double subsidy =crop_parameter(index, "Subsidy");

	double resp = a_crop->m_resp;
	double BIHerb = a_crop->m_BIHerb;
	double BIFi = a_crop->m_BIFi;
	double grooming = a_crop->m_grooming;
	double hoeing = a_crop->m_hoeing;
	double weeding = a_crop->m_weeding;
	double totalLoss = a_crop->m_totalLoss;
	double nt = a_crop->m_nt;
	double priceNt = cfg_Price_Nt.value(); 

    double income_ha = benefit*resp*(1-totalLoss/100)+subsidy;
    double costs_ha = BIHerb*priceHerb + BIFi*priceFi + grooming*priceG + hoeing*priceH + weeding*priceW + nt*priceNt + priceLM;
    double profit = income_ha - costs_ha;

	a_crop->m_costs_ha = costs_ha;
	a_crop->m_GM_Savings = profit;

    //29.02.12
	int index1 = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar * m_farmType + a_crop->m_cropType : a_foobar * m_farmType + a_crop->m_cropType_almass;
	bool fodder = m_OurManager->pm_data->Get_fodder(index1);
    if (fodder){
		a_crop->m_income_ha = subsidy; //because fodder crops can't be sold
		a_crop->m_GM = subsidy-costs_ha; //because fodder crops can't be sold
		a_crop->m_savings = profit;
    }
    else{
        a_crop->m_income_ha = income_ha;
        a_crop->m_GM = profit;
		a_crop->m_savings = 0; //this is not a fodder crop!
    }
}

//------------------------------------------------------------------------------//
//-----------------FUNCTIONS RELATED TO CROPS AREA------------------------------//
//------------------------------------------------------------------------------//

void OptimisingFarm::assignFixed(){

/**Function assigning to fixed crops their initial areas as percentages of a farm's total area. It determines the share of a farm (%) reserved
for fixed crops - excluded from the optimisation - and saves it in a variable #m_assigned.*/

    m_assigned = 0;
    for (int i=0; i<(int)m_fixedCrops.size(); i++){//take only crops that are fixed
		double area = m_fixedCrops[i]->m_initialArea;

		if(!cfg_UseBedriftsmodelFarmAreas.value()){ //almass farm areas are in use, so scale the fixed crop area
			area *= m_area_scaling_factor;
		}

        double areaPercent= area/m_totalArea*100;
		m_fixedCrops[i]->m_areaPercent = areaPercent; //assign initial area in % for fixed crops
        m_assigned += areaPercent;//add areaPercent of a fixed crop to assigned
    }//now assigned is a number <0, 100> - excluded from the optimisation
}

void OptimisingFarm::sumMinAreas(){
/**The function adds the minimum required areas of variable crops to the variable #m_assigned
(which holds a share of the farm area excluded from the optimisation). */

    for(int i=0; i<(int)m_variableCrops.size(); i++){
		double rotationMin = m_variableCrops[i].crop->m_rotationMin;
        m_assigned+=rotationMin; //the min area added to the area already assigned
    }
}

void OptimisingFarm::determineAreas(int a_foobar){
/**Function determining areas of all not fixed, non-fodder crops at a farm as percentages of a farm's total area.
It goes through a list of variable crops sorted from the highest to the lowest Gross Margin (i.e., most to least profitable) and assigns a maximal
allowed area for each non-fodder crop - until the remaining farm's area is equal to or lower than a maximal allowed area for a given crop.
In such case it assigns the remaining area to this crop. All remaining crops are assigned areas equal to zero (above their minimal required areas).

If the #cfg_Areas_Based_on_Distribution variable is set to true, the area is assigned on the basis og each crop's GM proportion in the
summary GM based on crops with a positive GM.*/

	m_grownVariableCrops.clear();

    for(int i=0; i<(int)m_variableCrops.size(); i++){
        double areaPercent=0;
		double minArea=m_variableCrops[i].crop->m_rotationMin;

		int index1 = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar*m_farmType + m_variableCrops[i].crop->m_cropType : a_foobar*m_farmType + m_variableCrops[i].crop->m_cropType_almass;

		//next two lines necessary for animal farms' function version
		bool fodder = m_OurManager->pm_data->Get_fodder(index1);
        if (!fodder){ //this should be checked for animal farms, for other and plant - will always be false
            if (m_assigned<100){ //check if there is any area left at a farm
				double rotationMax = m_variableCrops[i].crop->m_rotationMax; //same as in else

				if(cfg_Areas_Based_on_Distribution.value()){ //added 170713

					double GMsum = 0;
					for(int k=i; k<(int)m_variableCrops.size(); k++){ //do it for each crop (calculate prob) to avoid getting area without any crop. Start with the current crop (k=i), i.e. exclude the crops that are already assigned area.
						if(m_variableCrops[k].crop->m_GM > 0 && m_variableCrops[k].crop->m_rotationMax>0){ //make sure you dont include crops that have rot max set to zero! then the area won't sum up to 100%
							GMsum += pow (m_variableCrops[k].crop->m_GM, 1);
						}
					}
					double base_prob = 0;
					if(m_variableCrops[i].crop->m_GM > 0){
						base_prob = pow(m_variableCrops[i].crop->m_GM, 1)/GMsum;
					}
					else base_prob = 0;

					//draw a random number
					std::random_device rd;
					distribution_type3 dis(-1.0, 1.0);
					std::mt19937 engine(rd());
					double random_number = dis(engine);
					if(base_prob == 1) random_number = 0; // this is the last crop on the list - don't manipulate the area
					double area_1 = base_prob * (1 + 0.2 * random_number) * (100 - m_assigned); //take only a percent of the remaining area!
					areaPercent = ( area_1 <= rotationMax)? area_1 : rotationMax;
					if (areaPercent > 100 - m_assigned) areaPercent = 100 - m_assigned; //added 280813

				}
				else{ //standard area assignment
					areaPercent = (rotationMax-minArea <= 100-m_assigned)? rotationMax : (100 - m_assigned+minArea); //check if the remaining area (100-assigned) is enough to assign max allowed area for a crop (max-min, which was already assigned, if not - assign the remaining area+min area
				}

				m_assigned += areaPercent - minArea; //add the assigned area (final area percent minus minimum; minimum was assigned before) to the total assigned area
				if(areaPercent>minArea){m_grownVariableCrops.push_back(m_variableCrops[i].crop);} //this might not be used for other than animal farms...but maybe - it would be nice to have such a list anyway

			}//m_assigned < 100

            else {areaPercent = minArea;}

            m_variableCrops[i].crop->m_areaPercent = areaPercent;
            double areaVar=areaPercent-minArea;
			m_variableCrops[i].crop->m_areaVariable = areaVar; //save the value of the area that can be changed! For farms other than animal this maybe could be used just in restrictions functions...
        }
        else{ //it is a fodder crop, so just save its min area under areaPercent
			m_variableCrops[i].crop->m_areaPercent = minArea;
        }
    }
}

void AnimalFarm::determineAreas(int a_foobar){
/** Animal farm version of the determineAreas function.
In case of some farms (i.e. cattle farms) the sum of maximum allowed areas of non-fodder crops does not reach 100%, therefore it is necessary
to introduce a virtual crop (AnimalFarm::m_fakeCrop)to 'fill' the area til 100%. This virtual crop is substituted by fodder crops in #AnimalFarm::determineFodderAreas function.*/

	OptimisingFarm::determineAreas(a_foobar);

/*If after this loop area assigned is < 100%, this means that the sum of the maximum allowed areas of non-fodder crops doesn't reach 100%;
in such case introduce a fake crop to fill up the space till 100% (temporarily - until the determineFodderAreas function is called).*/

    m_fakeCropTest=false;
    if(m_assigned<100){ //use virtual 'fake crop'
        m_fakeCropTest = true;
        m_grownVariableCrops.push_back(m_fakeCrop); //append at the end (it is - it must be - the least profitable crop); this list is used in the determineFodderAreas function
        double areaP = 100-m_assigned;
		m_fakeCrop->m_GM = -1000000;
		m_fakeCrop->m_rotationMax = 100;
		m_fakeCrop->m_rotationMin = 0; 
		m_fakeCrop->m_areaPercent = areaP;
		m_fakeCrop->m_areaVariable = areaP;
        m_assigned += areaP;
    }

	sortCrops(m_fodderCrops, "Savings"); //order fodder crops from the highest to the lowest savings resulting from growing a fodder crop - in relation to purchased fodder
	randomizeCropList(m_fodderCrops, "Savings"); //shuffles elements of the list with equal Savings

	sortCrops(m_variableCrops, "GM_Savings"); //has to be done before restrictions, but only for animal farms.
	randomizeCropList(m_variableCrops, "GM_Savings"); //shuffles elements of the list with equal GM_Savings. Fodder crops have their savings saved under GM_Savings, non-fodder - GM.

	correctFodderDemand(a_foobar); //correct demand for min areas and fixed crops
	determineFodderAreas(a_foobar);
}

//------------------------------------------------------------------------------//
//---------FUNCTIONS RELATED TO RESTRICTIONS ON RELATIVE CROP AREAS-------------//
//------------------------------------------------------------------------------//

void OptimisingFarm::checkRestrictions(){
/**Function checking if restrictions concerning crop rotations are fulfilled. If not, it corrects the crop areas in order to
fulfill the restrictions. It starts with checking the restriction on winter crops rotation. Then it checks restriction on
max winter crops' area. Finally, for cattle farms it checks the restriction on crop rotation. 

The order of checking the first two restrictions does matter: in case the first restriction is not fullfilled, the first function
always decreases winter wheat's area and only in a particular case increases area of winter rape. Thus, the total area of winter crops 
(->second restriction) most likely is decreased or, otherwise - unchanged by the function dealing with the first restriction. 
The second restriction, on max area of winter crops, does not allow to decrease area of winter rape; this, in certain cases, might
result in suboptimal final result, but ensures that the first restriction is not violated. In the third restriction  
(only in the version for cattle farms) in the Bedriftsmodel crop mode, winter wheat is removed from the list of crops which is 
used to fill up the gap after decreasing some of the restriction crops. 

In the ALMaSS crops mode the third restriction (for cattle farms) is implemented in a simplified version, i.e. it is limited to changing 
only areas of crops that form the restriction. Additionally, in the ALMaSS crops mode there is a function which ensures that areas of both 
CloverGrassGrazed1 and  CloverGrassGrazed2 as well as SeedGrass1 and SeedGrass2 are equal (since '1' and '2' are the same crops, but with
different management in the first and next year they are grown).*/


    //1st restriction: winter rotation
	if(!cfg_Areas_Based_on_Distribution.value()){ //if this is true, skip this restriction
		checkWinterRotation1();
	}

    //2nd restriction: max share of winter crops area
    checkWinterCrops(); 

	if (!cfg_OptimiseBedriftsmodelCrops.value()){ //just for almass crop mode: make sure cgg1 and cgg2, sgg1 and sgg2 - have equal areas. added 26.03.13 -function since 110613
		Check_SG_and_CGG();
	}
}

void OptimisingCattleFarm::checkRestrictions(){
/** Includes additionally the function dealing with the cattle rotation restricton.*/

	AnimalFarm::checkRestrictions();

	//3rd restriction: cattle crops
	if(cfg_OptimiseBedriftsmodelCrops.value()){
		checkCattleRotation();
	}
	else{
		checkCattleRotation_almass();
	}

}

void OptimisingPigFarm::checkRestrictions(){
/**Includes additionally the function dealing with the winter rape - winter barley restriction.*/

	AnimalFarm::checkRestrictions();

	//restriction - grow winter barley if you have winter rape. Just in Almass crops mode when crops assignement is based on GM distribution:
	if(!cfg_OptimiseBedriftsmodelCrops.value() && cfg_Areas_Based_on_Distribution.value() ) {
		check_WRape_WBarley();
	}

}

void NonAnimalFarm::checkWinterRotation1(){

/**Function checks if the restriction on rotation:
area(WWheat) - (area(WRape) + area(SRape) + area(FieldPeas) + area(Oats)) <=0, is fulfilled.
If not, it corrects crops areas (excluding the fixed crop, FieldPeas).*/

	CropOptimised *wWheat;
    CropOptimised *wRape;
    CropOptimised *sRape;
    CropOptimised *oats;
    CropOptimised *peas;

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		wWheat = findCropByName("WWheat");
		wRape = findCropByName("WRape");
		sRape = findCropByName("SRape");
		oats = findCropByName("Oats");
		peas = findCropByName("Peas"); //fixed
	}
	else{
		wWheat = findCropByName_almass("WinterWheat");
		wRape = findCropByName_almass("WinterRape");
		sRape = findCropByName_almass("SpringRape");
		oats = findCropByName_almass("Oats");
		peas = findCropByName_almass("FieldPeas"); //fixed
	}

	double areaWWheat = wWheat->m_areaPercent;
    double areaWRape = wRape->m_areaPercent;
    double areaSRape = sRape->m_areaPercent;
    double areaOats = oats->m_areaPercent;
    double areaPeas = peas->m_areaPercent;

    double diff = areaWWheat - (areaWRape + areaSRape + areaOats + areaPeas); //difference;
    if (diff > 0){ //restriction is not fulfilled
        CropSort cs1 = {0., wRape}; //an object with a key for sorting and a pointer to crop
        CropSort cs2 = {0., sRape};
        CropSort cs3 = {0., oats};
		m_rotationCrops.clear();
        m_rotationCrops.push_back(cs1);
        m_rotationCrops.push_back(cs2);
        m_rotationCrops.push_back(cs3);

        sortCrops(m_rotationCrops, "GM"); //sorting the rotation crops wrt GM

        m_variableCrops2 = m_variableCrops; //initialize the new list and remove all 'winter rotation' crops:
        for(int s =(int)m_variableCrops2.size()-1; s>=0; s--){
            if(m_variableCrops2[s].crop==wWheat || m_variableCrops2[s].crop==wRape || m_variableCrops2[s].crop==sRape || m_variableCrops2[s].crop==oats){
				m_variableCrops2.erase(m_variableCrops2.begin() + s);
            }
        }

        double GM_WWheat = wWheat->m_GM;
		double areaMin = wWheat->m_rotationMin;

        for(int i=0; i<(int)m_rotationCrops.size() && diff!=0; i++){
			double areaRC = m_rotationCrops[i].crop->m_areaPercent;
			double rotationMaxRC = m_rotationCrops[i].crop->m_rotationMax;
            if(areaRC < rotationMaxRC){
				double GM_RC = m_rotationCrops[i].crop->m_GM;
                bool stopInnerLoop = false;

                for(int j=0; j<(int)m_variableCrops2.size() && stopInnerLoop==false; j++){

					double GM_var = m_variableCrops2[j].crop->m_GM;

                    if(GM_RC + GM_WWheat < 2*GM_var){
                        //add the variable crop
						double areaVC = m_variableCrops2[j].crop->m_areaPercent;
						double rotationMaxVC = m_variableCrops2[j].crop->m_rotationMax;
                        if(areaWWheat - areaMin >= diff){
                            if(rotationMaxVC - areaVC >= diff){
                                areaVC += diff;
                                areaWWheat -= diff;
                                diff = 0; //finito
                                stopInnerLoop = true;
                            }
                            else{
                                areaWWheat -= rotationMaxVC - areaVC;
                                diff -= (rotationMaxVC - areaVC);
                                areaVC = rotationMaxVC;
                            }
                        }
                        else{
                            if(rotationMaxVC - areaVC >= areaWWheat - areaMin){
                                areaVC += areaWWheat - areaMin;
                                diff -= (areaWWheat - areaMin);
                                areaWWheat = areaMin; // now you can only add rot crops! so stop the inner loop
                                stopInnerLoop = true;
                            }
                            else{
                                areaWWheat -= rotationMaxVC - areaVC;
                                diff -= (rotationMaxVC - areaVC);
								areaVC = rotationMaxVC;
                            }
                        }
						m_variableCrops2[j].crop->m_areaPercent = areaVC;
                    }

                    else{
                            //add rotational crop

                            if(areaWWheat - areaMin >= diff/2){
                                if(rotationMaxRC - areaRC >= diff/2){
                                    areaRC += diff/2;
                                    areaWWheat -= diff/2;
                                    diff = 0; //finito
                                    stopInnerLoop = true;
                                }
                                else{
                                    areaWWheat -= rotationMaxRC - areaRC;
                                    diff -= 2 * (rotationMaxRC - areaRC);
                                    areaRC = rotationMaxRC;
                                    stopInnerLoop = true; //take next rotation crop
                                }
                            }
                            else{
                                if(rotationMaxRC - areaRC >= areaWWheat - areaMin){
                                    areaRC += areaWWheat - areaMin;
                                    diff -= 2 * (areaWWheat - areaMin);
                                    areaWWheat = areaMin; //now you can only add rot crops!//so stop the inner loop:
                                    stopInnerLoop = true;
                                }
                                else{
                                    areaWWheat -= rotationMaxRC - areaRC;
                                    diff -= 2 * (rotationMaxRC - areaRC);
                                    areaRC = rotationMaxRC;
                                    stopInnerLoop = true; //take the next rotation crop
                                }
                            }
                        }
                    }//inner for-loop
				m_rotationCrops[i].crop->m_areaPercent = areaRC;
                }//if rot crop<rotmax //else - take the next rot crop, i++
            }//outer for

        //now the diff can be positive in two cases:

        //1. wwheat area is at zero - need to increase rotation crops - if possible
        if(areaWWheat==areaMin && diff > 0){
            double diff1=diff;
            increaseCrops(m_rotationCrops, diff1);
            if(diff1==0) {decreaseCrops(m_variableCrops2, diff);} //do it only if we actually managed to add the diff (=diff1)
			//to the rotation crops,i.e. diff1=0; if it's >0, then we added smth, but not enough and restriction can't be fulfilled.Then:
            else{
                double toCut = diff - diff1;
                decreaseCrops(m_variableCrops2, toCut);
                diff = diff1; //restriction will be broken by diff1
				ofstream ofile("Restrictions.txt",ios::app); 
				ofile << m_almass_no << '\t' << "winter rotation1 restriction broken, WWheat area at 0. Diff is: " << diff << endl;
				ofile.close();
            }
        }

        //2. it was always more profitable to grow rotation crops, but they were not
        //enough (maybe at max from the beginning) - increase variable crops, cut wwheat

        if(diff > 0   &&   areaWWheat - areaMin >= diff){
            areaWWheat -= diff;
            increaseCrops(m_variableCrops2, diff);
        }

		wWheat->m_areaPercent = areaWWheat;

	}//if
}//end of the function

void AnimalFarm::checkWinterRotation1(){

/**Function checks if the restriction on rotation: area(WWheat) - (area(WRape) + area(SRape) + area(FieldPeas) + area(Oats)) <=0, is fulfilled.
If not, it corrects crops areas excluding the fixed crop (FieldPeas). In case area of a fodder crop is changed, the value of #OptimisingFarm::m_totalFUt is updated.*/


	CropOptimised *wWheat;
    CropOptimised *wRape;
    CropOptimised *sRape;
    CropOptimised *oats;
    CropOptimised *peas;

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		wWheat = findCropByName("WWheat");
		wRape = findCropByName("WRape");
		sRape = findCropByName("SRape");
		oats = findCropByName("Oats");//fodder
		peas = findCropByName("Peas"); //fixed
	}
	else{
		wWheat = findCropByName_almass("WinterWheat");
		wRape = findCropByName_almass("WinterRape");
		sRape = findCropByName_almass("SpringRape");
		oats = findCropByName_almass("Oats");//fodder
		peas = findCropByName_almass("FieldPeas"); //fixed
	}

	double areaWWheat = wWheat->m_areaPercent;
    double areaWRape = wRape->m_areaPercent;
    double areaSRape = sRape->m_areaPercent;
    double areaOats = oats->m_areaPercent;
    double areaPeas = peas->m_areaPercent;

    double diff = areaWWheat - (areaWRape + areaSRape + areaOats + areaPeas); //difference;
    if (diff > 0){ //restriction is not fulfilled
        CropSort cs1 = {0., wRape}; //an object with a key for sorting and a pointer to crop
        CropSort cs2 = {0., sRape};
        CropSort cs3 = {0., oats};
		m_rotationCrops.clear();
        m_rotationCrops.push_back(cs1);
        m_rotationCrops.push_back(cs2);
        m_rotationCrops.push_back(cs3);

        sortCrops(m_rotationCrops, "GM_Savings"); //sorting the rotation crops wrt GM


        m_variableCrops2 = m_variableCrops; //initiate the new list and remove all 'winter rotation' crops:
        for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
            if(m_variableCrops2[s].crop==wWheat || m_variableCrops2[s].crop==wRape || m_variableCrops2[s].crop==sRape || m_variableCrops2[s].crop==oats){
                m_variableCrops2.erase(m_variableCrops2.begin() + s);
            }
        }

		double GM_WWheat = wWheat->m_GM;
		double areaMin = wWheat->m_rotationMin;

        for(int i=0; i<(int)m_rotationCrops.size() && diff!=0; i++){
			double areaRC = m_rotationCrops[i].crop->m_areaPercent;
			double rotationMaxRC = m_rotationCrops[i].crop->m_rotationMax;
            double areaBeforeRC = areaRC;
            if(areaRC < rotationMaxRC){
				double GM_RC = m_rotationCrops[i].crop->m_GM_Savings;
                bool stopInnerLoop = false;

                for(int j=0; j<(int)m_variableCrops2.size() && stopInnerLoop==false; j++){

					double GM_var = m_variableCrops2[j].crop->m_GM_Savings;

                    if(GM_RC + GM_WWheat < 2 * GM_var){
                        //add variable
						double areaVC = m_variableCrops2[j].crop->m_areaPercent;
						double rotationMaxVC = m_variableCrops2[j].crop->m_rotationMax;
                        double areaBefore = areaVC;
                        if(areaWWheat - areaMin >= diff){
                            if(rotationMaxVC - areaVC >= diff){
                                areaVC += diff;
                                areaWWheat -= diff;
                                diff = 0; //finished
                                stopInnerLoop = true;
                            }
                            else{
                                areaWWheat -= rotationMaxVC - areaVC;
                                diff -= (rotationMaxVC - areaVC);
                                areaVC = rotationMaxVC;
                            }
                        }
                        else{
                            if(rotationMaxVC - areaVC >= areaWWheat - areaMin){
                                areaVC += (areaWWheat - areaMin);
                                diff -= (areaWWheat - areaMin);
                                areaWWheat = areaMin; //now you can only add rot crops! so stop the inner loop
                                stopInnerLoop=true;
                            }
                            else{
                                areaWWheat -= rotationMaxVC - areaVC;
                                diff -= (rotationMaxVC - areaVC);
								areaVC = rotationMaxVC;
                            }
                        }
						m_variableCrops2[j].crop->m_areaPercent = areaVC;
                        //this just in an animal version, add if - checking if area has changed
                        if(areaVC > areaBefore){
							int index = (cfg_OptimiseBedriftsmodelCrops.value())? toc_Foobar*m_farmType + m_variableCrops2[j].crop->m_cropType : tov_Undefined*m_farmType + m_variableCrops2[j].crop->m_cropType_almass;
							bool fodder = m_OurManager->pm_data->Get_fodder(index);
                            if(fodder){ //this is a fodder crop - so you need to buy less fodder - totalFUt
								double resp = m_variableCrops2[j].crop->m_resp;
								double loss = m_variableCrops2[j].crop->m_totalLoss;
								double FUKey = m_OurManager->pm_data->Get_FUKey(index);
                                double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
                                double changeFU = (areaVC - areaBefore) * m_totalArea/100 * FUha; //positive
                                m_totalFUt -= changeFU;
                                m_totalFUgrown+=changeFU;
                            }
                        }
                    }
                    else{
                            //add rotational

                            if(areaWWheat - areaMin >= diff/2){
                                if(rotationMaxRC - areaRC >= diff/2){
                                    areaRC += diff/2;
                                    areaWWheat -= diff/2;
                                    diff = 0; //finito
                                    stopInnerLoop = true;
                                }
                                else{
                                    areaWWheat -= rotationMaxRC - areaRC;
                                    diff -= 2 * (rotationMaxRC - areaRC);
                                    areaRC = rotationMaxRC;
                                    stopInnerLoop = true; //take next rotation crop
                                }
                            }
                            else{
                                if(rotationMaxRC - areaRC >= areaWWheat - areaMin){
                                    areaRC += areaWWheat - areaMin;
                                    diff -= 2 * (areaWWheat - areaMin);
                                    areaWWheat = areaMin; //now you can only add rot crops! so stop the inner loop:
                                    stopInnerLoop = true;
                                }
                                else{
                                    areaWWheat -= rotationMaxRC - areaRC;
                                    diff -= 2 * (rotationMaxRC - areaRC);
                                    areaRC = rotationMaxRC;
                                    stopInnerLoop = true; //take the next rotation crop
                                }
                            }
                        }
                    }//inner for-loop

				m_rotationCrops[i].crop->m_areaPercent = areaRC;
                    //this just in an animal version, add if - checking if area has changed
                if(areaRC>areaBeforeRC){
					int index = (cfg_OptimiseBedriftsmodelCrops.value())? toc_Foobar*m_farmType + m_rotationCrops[i].crop->m_cropType : tov_Undefined*m_farmType + m_rotationCrops[i].crop->m_cropType_almass;
					bool fodder = m_OurManager->pm_data->Get_fodder(index);
                    if(fodder){ //this is a fodder crop - so you need to buy less fodder - totalFUt
						double resp = m_rotationCrops[i].crop->m_resp;
						double loss = m_rotationCrops[i].crop->m_totalLoss;
						double FUKey = m_OurManager->pm_data->Get_FUKey(index);
                        double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
                        double changeFU = (areaRC - areaBeforeRC) * m_totalArea/100 * FUha; //positive
                        m_totalFUt -= changeFU;
                        m_totalFUgrown += changeFU;
                    }
                }
            }//if rot crop<rotmax//else - take the next rot crop, i++
        }//outer for

        //now the diff can be positive in two cases:

        //1. wwheat area is at zero - need to increase rotation crops - if possible
        if(areaWWheat==areaMin && diff>0){
            double diff1 = diff;
            increaseCrops(m_rotationCrops, diff1);
            if(diff1==0) {decreaseCrops(m_variableCrops2, diff);} //do it only if we actually
            //managed to diff (=diff1) to the rotation crops,so diff1=0; if it's (0, diff)-then
            //we added smth, but not enough and restriction can't be fulfilled...- then...
            else{
                double toCut = diff-diff1;
                decreaseCrops(m_variableCrops2, toCut);
                diff = diff1; //restriction will be broken by diff1
				ofstream ofile("Restrictions.txt",ios::app); 
				ofile << m_almass_no << '\t' << "winter rotation1 restriction broken. Animal farm. WWheat area at 0. Diff is: " << diff << endl;
				ofile.close();
            }
        }

        //2. it was always more profitable to grow rotation crops, but they were not
        //enough (maybe at max from the beginning) - increase variable crops, cut wwheat

        if(diff>0 && areaWWheat-areaMin >= diff){
            areaWWheat -= diff;
            increaseCrops(m_variableCrops2, diff);
        }

		wWheat->m_areaPercent = areaWWheat;

	}//if

}//end of the function

void NonAnimalFarm::checkWinterCrops(){

/**Function checking if the restriction on maximum winter crops area is fulfilled. If not, it corrects winter crops areas
(starting with the one with the lowest GM) and assigns the area above the limit for winter crops to the most profitable
variable crop(s) that haven't reached the maximimum area after the #OptimisingFarm::determineAreas function was called. None of the winter crops
is fixed.*/

	CropOptimised *wBarley;
    CropOptimised *wWheat;
    CropOptimised *wRye;
    CropOptimised *wRape;
    CropOptimised *wCerealSil = NULL; // Prevents warning

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		wBarley = findCropByName("WBarley");
		wWheat = findCropByName("WWheat");
		wRye = findCropByName("WRye");
		wRape = findCropByName("WRape"); //should not be decreased - might cause violation of winterRotation restriction
		wCerealSil = findCropByName("WCerealSil");
	}
	else{
		wBarley = findCropByName_almass("WinterBarley");
		wWheat = findCropByName_almass("WinterWheat");
		wRye = findCropByName_almass("WinterRye");
		wRape = findCropByName_almass("WinterRape"); //should not be decreased - might cause violation of winterRotation restriction
	}


	double areaWBarley = wBarley->m_areaPercent;
	double areaWWheat = wWheat->m_areaPercent;
	double areaWRye = wRye->m_areaPercent;
	double areaWRape = wRape->m_areaPercent;
	double areaWCerealSil = 0;

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		areaWCerealSil = wCerealSil->m_areaPercent; //it is used here only if it was initilized - in the bedriftsmodel crop mode
	}

	double sum = areaWBarley + areaWWheat + areaWRye + areaWRape + areaWCerealSil;

	int winterMax = m_OurManager->pm_data->Get_winterMax(m_farmType);
	if(sum > winterMax){

		m_variableCrops2 = m_variableCrops; //initiate the new list and remove all winter crops:

		if(cfg_OptimiseBedriftsmodelCrops.value()){
			for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
				if(m_variableCrops2[s].crop==wBarley || m_variableCrops2[s].crop==wWheat || m_variableCrops2[s].crop==wRye || m_variableCrops2[s].crop==wRape || m_variableCrops2[s].crop==wCerealSil){
					m_variableCrops2.erase(m_variableCrops2.begin() + s);
				}
			}
		}
		else{
			for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
				if(m_variableCrops2[s].crop==wBarley || m_variableCrops2[s].crop==wWheat || m_variableCrops2[s].crop==wRye || m_variableCrops2[s].crop==wRape){
					m_variableCrops2.erase(m_variableCrops2.begin() + s);
				}
			}
		}

		double diff = sum - winterMax;
		double diffBefore = diff;

		CropSort cs1 = {0., wBarley}; //an object with a key for sorting and a pointer to crop
		CropSort cs2 = {0., wWheat};
		CropSort cs3 = {0., wRye}; //CropSort cs4 = {0., wRape};
		m_winterCrops.clear();
		m_winterCrops.push_back(cs1);
		m_winterCrops.push_back(cs2);
		m_winterCrops.push_back(cs3);

		if(cfg_OptimiseBedriftsmodelCrops.value()){
			CropSort cs5 = {0., wCerealSil};
			m_winterCrops.push_back(cs5);
		}

		sortCrops(m_winterCrops, "GM");

		//decrease area of one or more winter crops by diff
		decreaseCrops(m_winterCrops, diff);

		if(diff > 0){ //loop in decreaseCrops was stopped by the other condition: impossible to cut more
			//violation of the restriction; do not exit, print warning
			ofstream ofile("Restrictions.txt",ios::app); 
 			ofile << m_almass_no << '\t' << "winter crops restriction broken, non-animal farm. Diff is: " << diff << endl;
			ofile.close();
		}

		//assign area cut from winter crops to one or more variable crops (that haven't reached the max yet)
		double toIncrease = diffBefore - diff; 
		increaseCrops(m_variableCrops2, toIncrease);

	}//else nothing!:) it's ok

}//end of checkWinterCrops

void AnimalFarm::checkWinterCrops(){

/**Function for animal farms checking if restriction on maximum winter crops area is fulfilled.
Works in the same way as the NonAnimalFarm version, but additionally accounts for fodder changes.
In Bedriftsmodel crop mode, both for cattle and pig farms two of the winter crops, WBarley and WCerealSil, are fodder crops.
In the ALMaSS crop mode, WCerealSil does not exist (is aggregated to another crop) and is thus excluded from the restriction's condition.*/

	CropOptimised *wBarley;
    CropOptimised *wWheat;
    CropOptimised *wRye;
    CropOptimised *wRape;
    CropOptimised *wCerealSil = NULL; // To prevent the annoying warning

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		wBarley = findCropByName("WBarley");
		wWheat = findCropByName("WWheat");
		wRye = findCropByName("WRye");
		wRape = findCropByName("WRape"); //should not be decreased - might cause violation of winterRotation restriction
		wCerealSil = findCropByName("WCerealSil");
	}
	else{
		wBarley = findCropByName_almass("WinterBarley");
		wWheat = findCropByName_almass("WinterWheat");
		wRye = findCropByName_almass("WinterRye");
		wRape = findCropByName_almass("WinterRape"); //should not be decreased - might cause violation of winterRotation restriction
	}

	double areaWBarley = wBarley->m_areaPercent;
	double areaWWheat = wWheat->m_areaPercent;
	double areaWRye = wRye->m_areaPercent;
	double areaWRape = wRape->m_areaPercent;
	double areaWCerealSil = 0;

	if(cfg_OptimiseBedriftsmodelCrops.value()){
		areaWCerealSil = wCerealSil->m_areaPercent;
	}

	double sum = areaWBarley + areaWWheat + areaWRye + areaWRape + areaWCerealSil;

	int winterMax = m_OurManager->pm_data->Get_winterMax(m_farmType);
	if(sum > winterMax){
		double diff = sum - winterMax;
		double diffBefore = diff;

		m_variableCrops2 = m_variableCrops; //initiate the new list and remove all winter crops:
		if(cfg_OptimiseBedriftsmodelCrops.value()){
			for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
				if(m_variableCrops2[s].crop==wBarley || m_variableCrops2[s].crop==wWheat || m_variableCrops2[s].crop==wRye || m_variableCrops2[s].crop==wRape || m_variableCrops2[s].crop==wCerealSil){
					m_variableCrops2.erase(m_variableCrops2.begin() + s);
				}
			}
		}
		else{
			for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
				if(m_variableCrops2[s].crop==wBarley || m_variableCrops2[s].crop==wWheat || m_variableCrops2[s].crop==wRye || m_variableCrops2[s].crop==wRape){
					m_variableCrops2.erase(m_variableCrops2.begin() + s);
				}
			}
		}

		//assign keys for sorting - for fodder crops these will be Savings!
		CropSort cs1 = {0., wBarley}; //an object with a key for sorting and a pointer to crop
		CropSort cs2 = {0., wWheat};
		CropSort cs3 = {0., wRye};//CropSort cs4 = {wRape->pullVariable<double>("GM"), wRape};

		m_winterCrops.clear();
		m_winterCrops.push_back(cs1);
		m_winterCrops.push_back(cs2);
		m_winterCrops.push_back(cs3);

		if(cfg_OptimiseBedriftsmodelCrops.value()){
			CropSort cs5 = {0., wCerealSil};
			m_winterCrops.push_back(cs5);
		}

		sortCrops(m_winterCrops, "GM_Savings");

		//decrease area of one or more winter crops by diff
		decreaseCrops(m_winterCrops, diff);

		if(diff>0){ //loop stopped by the other condition: impossible to cut more
			//violation of the restriction; do not exit, just print a warning
			ofstream ofile("Restrictions.txt",ios::app); 
			ofile << m_almass_no << '\t' << "winter crops restriction broken, Animal farm. Diff is: " << diff << endl;
			ofile.close();
		}

		//assign the area cut from winter crops to one or more variable crops (that haven't reached the max yet)
		double toIncrease = diffBefore - diff; 
		increaseCrops(m_variableCrops2, toIncrease);

	}//else nothing!:) it's ok

}//end of checkWinterCropsAnimal

void OptimisingCattleFarm::setCattleCropsAtMin(){
/** In Bedriftsmodel crop mode, it decreases area of the two cattle crops (Grass Seed, Clover Grass) to their minimum required area (RotationMin).*/

    for(int i=0; i<(int)m_cattleCrops.size(); i++){ //just go through all 2 of them:)
		double area = m_cattleCrops[i].crop->m_areaPercent;
        double areaBefore = area;
		double rotationMin = m_cattleCrops[i].crop->m_rotationMin;
        area = rotationMin; //set all at min 
		m_cattleCrops[i].crop->m_areaPercent = area;

		if(areaBefore > area){
			int index = (cfg_OptimiseBedriftsmodelCrops.value())? toc_Foobar*m_farmType + m_cattleCrops[i].crop->m_cropType : tov_Undefined*m_farmType + m_cattleCrops[i].crop->m_cropType_almass;
			bool fodder = m_OurManager->pm_data->Get_fodder(index);
			if(fodder){
				double resp = m_cattleCrops[i].crop->m_resp;
				double loss = m_cattleCrops[i].crop->m_totalLoss;
				double FUKey = m_OurManager->pm_data->Get_FUKey(index);
				double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
				double changeFU = (areaBefore - area) * m_totalArea/100 * FUha; //positive number
				m_totalFUt += changeFU; //so you buy more
				m_totalFUgrown -= changeFU; //and grow less:)
			}
		}
    }
}

void OptimisingCattleFarm:: checkCattleRotation(){
/**Used in the Bedriftsmodel crop mode. Function checking if the restriction for cattle farms on attachment of spring barley, clover-grass and grass seed is fulfilled.  If not,
it corrects crops' areas - if possible, reduces the area of grass clover and/or seed grass and increases area of other crops.
The function first checks if it pays off to substitute one or two of the cattle crops (clovergrass and grass seed) with a variable crop
other than Spring Barley. If yes, it decreases the area of one or two of the cattle crops. If the condition is not fulfilled yet, the
function reduces the area of one or two of the cattle crops and adds a corresponding area to the Spring Barley.
None of the three crops in the condition is fixed. Spring barley and grass-clover are fodder crops.*/

//This function can be called only after fodder crops were assigned areas because two of them are fodder crops! So before assiging the fodder crops, their areas will be zero!


	CropOptimised *sBarley = findCropByName("SBarley"); //fodder
	CropOptimised *gClover = findCropByName("GrassClover"); //fodder
	CropOptimised *gSeed = findCropByName("GrassSeed");

	CropOptimised *wWheat = findCropByName("WWheat");

	double areaSBarley = sBarley->m_areaPercent;
	double areaGClover = gClover->m_areaPercent;
	double areaGSeed = gSeed->m_areaPercent;

	double diff = areaGClover + areaGSeed - areaSBarley;

	if(diff > 0){ //restriction is not fulfilled

		//1. Make a vector of (two) cattle crops and sort them from highest to lowest GM/Savings
		CropSort cs1 = {0., gClover}; //an object with a key for sorting and a pointer to crop
		CropSort cs2 = {0., gSeed};
		m_cattleCrops.push_back(cs1);
		m_cattleCrops.push_back(cs2);

		sortCrops(m_cattleCrops, "GM_Savings"); //sorting the two crops

		//2. Make a new list of variable crops - excluding all crops forming the condition
		m_variableCrops2 = m_variableCrops; //initiate the new list and remove all three crops:
		for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
			if(m_variableCrops2[s].crop==sBarley || m_variableCrops2[s].crop==gClover || m_variableCrops2[s].crop==gSeed || m_variableCrops2[s].crop==wWheat){ //14.02 - added winter wheat - do not increase it!maybe i should even add here all winter crops...
				m_variableCrops2.erase(m_variableCrops2.begin() + s);
			}
		}

		//3. check if it pays off to substitute one of the two cattle crops with variable crops other than SBarley; if so - change the areas
		for(int i=(int)m_cattleCrops.size()-1; i>=0 && diff>0; i--){ //start with the worse one
			double areaCC = m_cattleCrops[i].crop->m_areaPercent; //don't know which of the two it is, so have to get the values this way
			double areaMinCC = m_cattleCrops[i].crop->m_rotationMin;
			double areaCCBefore = areaCC;
			if(areaCC > areaMinCC){
				bool cCropAtMin = false;
				for(int j=0; j<(int)m_variableCrops2.size() && cCropAtMin==false; j++){ //start with the best variable crop
					double GM_SBarley = sBarley->m_GM_Savings; 
					double GM_CC = m_cattleCrops[i].crop->m_GM_Savings; //in case it is a fodder crop - then it takes savings, as now, after findFodderCropSavings() - savings are saved under GM_Savings for fodder crops
					double GM_var = m_variableCrops2[j].crop->m_GM_Savings;
					if(GM_SBarley + GM_CC > 2*GM_var){
						//yes: it doesn't pay off to remove this cattle crop; go to the next part of the cattle rot. function: increase SBarley, cut one of the cattle crops
					}
					else{
						double areaVar = m_variableCrops2[j].crop->m_areaPercent;
						double areaMaxVar = m_variableCrops2[j].crop->m_rotationMax;
						double areaVarBefore = areaVar;

						if(areaCC - areaMinCC >= diff){
							if(areaMaxVar - areaVar >= diff){
								areaCC -= diff;
								areaVar += diff;
								diff = 0; //finished -> cattle rotation fulfilled
								cCropAtMin = true; //stop the inner loop!!!
							}
							else{
								diff -= (areaMaxVar - areaVar);
								areaCC -= (areaMaxVar - areaVar);
								areaVar = areaMaxVar; //go to the next var. crop (j++)
							}
						}
						else{
							if(areaMaxVar - areaVar >= areaCC - areaMinCC){
								diff -= areaCC - areaMinCC;
								areaVar += areaCC - areaMinCC;
								areaCC = areaMinCC;
								cCropAtMin = true; //stop the inner loop - take the next cattle crop(i--)
							}
							else{
								diff -= areaMaxVar - areaVar;
								areaCC -= areaMaxVar - areaVar;
								areaVar = areaMaxVar; //go to the next var crop (j++)
							}
						}

						//save the areas
						m_variableCrops2[j].crop->m_areaPercent = areaVar;
						m_cattleCrops[i].crop->m_areaPercent = areaCC;

						//correct fodder variables for the changes - both for cattle and var crop

						if(areaVar > areaVarBefore){
							int index = toc_Foobar*m_farmType + m_variableCrops[j].crop->m_cropType;
							bool fodder = m_OurManager->pm_data->Get_fodder(index);
							if(fodder){ //this is a fodder crop - so you need to buy less fodder - totalFUt
								double resp = m_variableCrops[j].crop->m_resp;
								double loss = m_variableCrops[j].crop->m_totalLoss;
								double FUKey = m_OurManager->pm_data->Get_FUKey(index);
								double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
								double changeFU = (areaVar - areaVarBefore) * m_totalArea/100 * FUha; //positive
								m_totalFUt -= changeFU;
								m_totalFUgrown += changeFU;
							}
						}

						if(areaCC < areaCCBefore){
							int index = toc_Foobar*m_farmType + m_cattleCrops[i].crop->m_cropType;
							bool fodder = m_OurManager->pm_data->Get_fodder(index);
							if(fodder){ //this is a fodder crop - so you need to buy less fodder - totalFUt
								double resp = m_cattleCrops[i].crop->m_resp;
								double loss = m_cattleCrops[i].crop->m_totalLoss;
								double FUKey = m_OurManager->pm_data->Get_FUKey(index);
								double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
								double changeFU = (areaCCBefore - areaCC) * m_totalArea/100 * FUha; //positive
								m_totalFUt += changeFU;
								m_totalFUgrown -= changeFU;
							}
						}
					}

				}//inner for
			}
			else{
				//nothing - go to the next cattle crop; if there's no more - then the diff must be zero - so the loop will stop
			}

		}//outer for

		//4. Check if diff is still > 0 (after point 3.), if so - substitute some area of one of the cattle crops with SBarley

		if(diff>0){

			double areaSBarleyMax = sBarley->m_rotationMax;
			double SBarleyAvailableArea = areaSBarleyMax-areaSBarley;
			double areaSBarleyBefore = areaSBarley;

			double available = 0; //available to cut from GClover and GSeed
			for(int i=0; i<(int)m_cattleCrops.size(); i++){  //or simply: i<2
				double areaMin = m_cattleCrops[i].crop->m_rotationMin;
				double area = m_cattleCrops[i].crop->m_areaPercent;
				available += (area - areaMin);
			}

			if(areaSBarley < areaSBarleyMax){ //Y1
				if(SBarleyAvailableArea >= diff/2){ //Y2
					if(available >= diff/2){ //Y 3.2
						areaSBarley += diff/2; //increase Sbarley by diff/2
						diff /= 2; //updating diff
						decreaseCrops(m_cattleCrops, diff);//decrease the two crops by diff/2 (now=to diff)
					}

					else{ //N 3.2
						areaSBarley += available; 
						diff -= 2 * available;//update diff
						setCattleCropsAtMin(); //decrease the two crops to min each (by available<diff/2),

						if(SBarleyAvailableArea >= diff){//if possible:
							areaSBarley += diff;//increase Sbarley by diff
							decreaseCrops(m_variableCrops2, diff);//& decrease the worst (available) normal crop by diff
						}
						else{
							//do as much as possible
							double toDecrease = areaSBarleyMax - areaSBarley; //neccesary -area has changed
							areaSBarley = areaSBarleyMax; //increase Sbarley to max
							diff-=toDecrease; //update diff
							decreaseCrops(m_variableCrops2, toDecrease);//decrease normal crops
							//and throw an exception! restriction must be violated
							ofstream ofile("Restrictions.txt",ios::app); //changed 06.06 out to app
							ofile << m_almass_no << '\t' << "cattle restriction: broken after N 3.2" << endl;
							ofile.close();
						}
					}
				}
				else{ //N2
					//increase SBarley to max in both cases
					if(available >= SBarleyAvailableArea){ //Y 3.1
						areaSBarley = areaSBarleyMax; //increase Sbarley to max
						diff -= 2 * SBarleyAvailableArea; //update diff
						available -= SBarleyAvailableArea; //decrease by what you add in the next line
						decreaseCrops(m_cattleCrops, SBarleyAvailableArea);//increase needed by

						double diffBefore = diff;
						if(available >= diff){
							decreaseCrops(m_cattleCrops, diff);//decrease the two crops by diff
							increaseCrops(m_variableCrops2, diffBefore);//increase normal crops by diff
						}
						else{
							//do as much as possible
							setCattleCropsAtMin();
							diff -= available; //update diff
							double toDecrease = available;
							increaseCrops(m_variableCrops2, toDecrease); //add some normal crops!
							//and throw exception - restriction must be violated!
							ofstream ofile("Restrictions.txt",ios::out);
							ofile << m_almass_no << '\t' << "cattle restriction: broken after Y 3.1" << endl;
							ofile.close();
						}
					}
					else{ //N 3.1 - restriction violated, but try to do as much as possible
						areaSBarley += available; 
						setCattleCropsAtMin(); 
						diff -= 2 * available; //update diff
						double toDecrease = areaSBarleyMax - areaSBarley; //here it is necessary! because the area has changed
						areaSBarley = areaSBarleyMax;//
						diff -= toDecrease; //update diff
						decreaseCrops(m_variableCrops2, toDecrease);//add normal crops-but still-violation!
						//AND !!! throw an exception - violation
						ofstream ofile("Restrictions.txt",ios::out);
						ofile << m_almass_no << '\t' << "cattle restriction: broken after N 3.1" << endl;
						ofile.close();
					}
				}

				sBarley->m_areaPercent = areaSBarley;

				//this is a fodder crop - so you need to buy less fodder - totalFUt
				double resp = sBarley->m_resp;
				double loss = sBarley->m_totalLoss;
				double FUKey = m_OurManager->pm_data->Get_FUKey (toc_SBarley);
				double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
				double changeFU = (areaSBarley - areaSBarleyBefore) * m_totalArea/100 * FUha; //positive
				m_totalFUt -= changeFU;
				m_totalFUgrown += changeFU;
			}

			else{ // N 1
				 if(available >= diff){
					 double diffBefore = diff;
					 decreaseCrops(m_cattleCrops, diff); //cover the diff
					 increaseCrops(m_variableCrops2, diffBefore); //increase some normal crops
				 }
				 else{//restr must be violated
					 setCattleCropsAtMin(); //set the two crops at min
					 diff -= available;
					 double toIncrease = available;
					 increaseCrops(m_variableCrops2, toIncrease);
					 //throw an exception - restriction must be violated!
					ofstream ofile("Restrictions.txt",ios::out);
					ofile << m_almass_no << '\t' << "cattle restriction: broken after N 1" << endl;
					ofile.close();
				 }
			}

		}//end of the second if(diff>0)

	}//end of the if(diff>0)?

}//end of check CattleRotation

void OptimisingCattleFarm:: checkCattleRotation_almass(){
/** Used in ALMaSS crop mode. Function checks if the restriction for cattle farms on attachment of spring barley, CloverGrassGrazed1 and
CloverGrassGrazed2 is fulfilled. If not, it corrects these crops' areas.*/

	CropOptimised *sBarley = findCropByName_almass("SpringBarley"); //fodder
	CropOptimised *CGG1 = findCropByName_almass("CloverGrassGrazed1"); //fodder
	CropOptimised *CGG2 = findCropByName_almass("CloverGrassGrazed2"); //fodder

	double areaSBarley = sBarley->m_areaPercent;
	double areaCGG = 2*(CGG1->m_areaPercent); //cgg1 and 2 have equal areas - it waa checked  in Check_sg_and_cgg (called in check restrictions)

	double diff = areaCGG/2 - areaSBarley; //SpringBarley has to have an area at least as big as CGG1 = CGG2 = areaCGG/2.

	if(diff > 0){ //restriction is not fulfilled

		//need to make new variableCrops2 - without the grasses and sbarley!
		m_variableCrops2 = m_variableCrops; //initiate the new list and remove crops:
		for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
			if(m_variableCrops2[s].crop==sBarley || m_variableCrops2[s].crop==CGG1 || m_variableCrops2[s].crop==CGG2){
				m_variableCrops2.erase(m_variableCrops2.begin() + s);
			}
		}

		double new_area_sbarley = areaSBarley;
		double new_area_cgg = areaCGG;

		double available=0; //all area except for fixed crops and min. areas of variable crops
		for(int i=0; i<(int)m_variableCrops.size(); i++){  //can't use m_grownVariableCrops - it doesn't include fodder crops!
			double areaVariableCrop = m_variableCrops[i].crop->m_areaPercent - m_variableCrops[i].crop->m_rotationMin;
            available += areaVariableCrop;
        }

		if(areaCGG/2 *3 <= available){ //then Sbarley can get as much as each CGG1; cgg area doesn't change
			new_area_sbarley = areaCGG/2;
			double area_to_decrease = new_area_sbarley - areaSBarley; // must be > 0 if this code is executed
			decreaseCrops(m_variableCrops2, area_to_decrease);
		}
		else{ //need to also cut grass, not just increase sbarley
			new_area_cgg = available/3 * 2; // = new area for sbarley = cgg1 = cgg2 (these 3 crops take all available area)
			new_area_sbarley = available/3;
			double current_sbarley_and_grasses = areaSBarley + areaCGG; // so other crops area (variable area) = 100 - current_sbarley_and_grasses
			double other_crops_area = available - current_sbarley_and_grasses;
			if(current_sbarley_and_grasses < available){ //there are other crops - and we're now substituting them with sbarley
				decreaseCrops(m_variableCrops2, other_crops_area);
			}
			//else - just these 3 crops - do nothing - grass is already cut :), sbarley area is increased
		}

		//correct fodder....
		double resp = sBarley->m_resp;
		double loss = sBarley->m_totalLoss;
		int index = tov_Undefined*m_farmType + tov_SpringBarley;
		double FUKey = m_OurManager->pm_data->Get_FUKey(index);
		double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
		double changeFU = (new_area_sbarley - areaSBarley) * m_totalArea/100 * FUha; //positive
		m_totalFUt -= changeFU;
		m_totalFUgrown += changeFU;

		resp = CGG1->m_resp;
		loss = CGG1->m_totalLoss;
		index = tov_Undefined*m_farmType + tov_CloverGrassGrazed1;
		FUKey = m_OurManager->pm_data->Get_FUKey(index);
		FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
		changeFU = (areaCGG - new_area_cgg) * m_totalArea/100 * FUha; //positive
		m_totalFUt += changeFU;
		m_totalFUgrown -= changeFU;

		//and change the areas
		CGG1->m_areaPercent = new_area_cgg/2; //areaCGG is a summary area for both CGG
		CGG2->m_areaPercent = new_area_cgg/2;
		sBarley->m_areaPercent = new_area_sbarley;

	}

}//end of check CattleRotation for almass crops

void OptimisingPigFarm::check_WRape_WBarley(){
/** Used in ALMaSS crop mode, when crop assignemnt is based on GM distribution.
90% of pig farms grow winter barley if they have winter rape, since winter barley is usually grown before winter rape.
Function checks if there is enough Winter Barley relative to Winter Rape. If not,
it modifies these crops areas aiming at keeping as much as possibel of Winter Rape. There is also a check if after the changes the winter crops 
restricitons is not broken, in case it is, the areas of winter barley and winter rape are decreased.*/

	CropOptimised *wBarley;
    CropOptimised *wRape;
	wBarley = findCropByName_almass("WinterBarley");
	wRape = findCropByName_almass("WinterRape");
	double area_WB = wBarley->m_areaPercent;
	double area_WR = wRape->m_areaPercent;

	if(area_WB < area_WR){ //then need to make changes!

		//1. need to make new variableCrops2 - without wrape and wbarley
		m_variableCrops2 = m_variableCrops; //initiate the new list and remove crops:
		for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
			if(m_variableCrops2[s].crop==wBarley || m_variableCrops2[s].crop==wRape){
				m_variableCrops2.erase(m_variableCrops2.begin() + s);
			}
		}

		//check how much area available is there:
		double available=0;
		for(int i=0; i<(int)m_variableCrops2.size(); i++){  //can't use m_grownVariableCrops - it doesn't include fodder crops!
			double areaVariableCrop = m_variableCrops2[i].crop->m_areaPercent - m_variableCrops2[i].crop->m_rotationMin;
            available += areaVariableCrop;
        }

		double new_area_wbarley = area_WB;
		double new_area_wrape = area_WR;

		if(2 * area_WR <= available){ //then WBarley can get area equal to the current WRape's area; WRape's area doesn't change
			new_area_wbarley = area_WR;
			double area_to_decrease = new_area_wbarley - area_WB; // must be > 0 if this code is executed
			decreaseCrops(m_variableCrops2, area_to_decrease); //increased the WBarley's area, so need to cut the same amount in other crops
		}
		else{ //need to cut wrape, not just increase wbarley
			new_area_wrape = available/2; // these 2 crops take the whole available area
			new_area_wbarley = available/2;
			double current_wbarley_and_wrape = area_WB + area_WR; // so other crops area (variable area) = 100 - current_wbarley_and_wrape
			double other_crops_area = available - current_wbarley_and_wrape;
			if(current_wbarley_and_wrape < available){ //there are other crops - and we're now substituting them with wbarley
				decreaseCrops(m_variableCrops2, other_crops_area);
			}
			//else nothing
		}

		//check how much winter crops is there now - before changes:
		double winter_crops=0;
		for(unsigned j=0; j<m_crops.size(); j++){
			if(m_crops[j]->m_cropType_almass == tov_WinterRape || m_crops[j]->m_cropType_almass == tov_WinterBarley ||
				m_crops[j]->m_cropType_almass == tov_WinterWheat || m_crops[j]->m_cropType_almass == tov_WinterRye){
					winter_crops += m_crops[j]->m_areaPercent;
			}
		}
		int winterMax = m_OurManager->pm_data->Get_winterMax(m_farmType);

		//now save the changes in the areas
		wRape->m_areaPercent = new_area_wrape;
		wBarley->m_areaPercent = new_area_wbarley;

		//check how much winter crops there is now:
		winter_crops=0;
		for(unsigned j=0; j<m_crops.size(); j++){
			if(m_crops[j]->m_cropType_almass == tov_WinterRape || m_crops[j]->m_cropType_almass == tov_WinterBarley ||
				m_crops[j]->m_cropType_almass == tov_WinterWheat || m_crops[j]->m_cropType_almass == tov_WinterRye){
					winter_crops += m_crops[j]->m_areaPercent;
			}
		}

		//need to check if the winter crops restriction is still fine:
		if(winter_crops > winterMax){ //problem
			double diff = winter_crops - winterMax;
			//need to make new variableCrops2 - without all winter crops!
			m_variableCrops2 = m_variableCrops; //initiate the new list and remove crops:
			for(int s=(int)m_variableCrops2.size()-1; s>=0; s--){
				if(m_variableCrops2[s].crop==wBarley || m_variableCrops2[s].crop==wRape || m_variableCrops2[s].crop->m_cropType_almass==tov_WinterWheat || m_variableCrops2[s].crop->m_cropType_almass == tov_WinterRye){
					m_variableCrops2.erase(m_variableCrops2.begin() + s);
				}
			}
			new_area_wrape -= diff/2;
			new_area_wbarley -= diff/2;
			increaseCrops(m_variableCrops2, diff);

			//and change the areas again
			wRape->m_areaPercent = new_area_wrape;
			wBarley->m_areaPercent = new_area_wbarley;
		}

		//correct fodder. only winter barley is a fodder crop for pig farms
		double resp = wBarley->m_resp;
		double loss = wBarley->m_totalLoss;
		int index = tov_Undefined*m_farmType + tov_WinterBarley;
		double FUKey = m_OurManager->pm_data->Get_FUKey(index);
		double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
		double changeFU = (new_area_wbarley - area_WB) * m_totalArea/100 * FUha; //positive
		m_totalFUt -= changeFU;
		m_totalFUgrown += changeFU;
	}
}

//------------------------------------------------------------------------------//
//------------------------FUNCTIONS RELATED TO FODDER---------------------------//
//------------------------------------------------------------------------------//

void AnimalFarm::correctFodderDemand(int a_foobar){
/**Function determining how much fodder is produced from fixed crops and minimial areas
of variable crops. Updates the variable holding the value of the total fodder demand
for a farm, #m_totalFUdemand. Updates a variable holding the value of the fodder that has to be obtained from own production.*/

    m_totalFUgrown=0; //initialize

	double min_fodder = (m_farmType == toof_Pig) ? cfg_Min_fodder_prod_pig.value() : cfg_Min_fodder_prod_cattle.value(); //there are only two types of animal farms
	m_totalFodderToProduce = (min_fodder/100)*m_totalFUdemand; //[FU]

    double fodderFromFixed=0;
    for (int i=0; i<(int)m_fixedCrops.size(); i++){
		double area_percent = m_fixedCrops[i]->m_areaPercent;
		if(area_percent!=0){ 
			double area = area_percent/100* m_totalArea; 
			double resp = m_fixedCrops[i]->m_resp;
			double loss = m_fixedCrops[i]->m_totalLoss;

			int index = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar*m_farmType + m_fixedCrops[i]->m_cropType : a_foobar*m_farmType + m_fixedCrops[i]->m_cropType_almass;
			double FUKey = m_OurManager->pm_data->Get_FUKey(index);//if it is not a fodder crop, key=0
			double FU = area*resp*(1-loss/100)*FUKey; //[ha * hkg/ha * FU/hkg = FU]
			fodderFromFixed += FU;//0 will be added in case of non-fodder crops
			m_totalFUgrown += FU;
		}
    }
    double fodderFromMinAreas=0;
    for (int i=0; i<(int)m_variableCrops.size(); i++){
		double areaMinPercent = m_variableCrops[i].crop->m_rotationMin;
		if(areaMinPercent!=0){ //150713
			double area = areaMinPercent/100*m_totalArea; //min area in ha
			double resp = m_variableCrops[i].crop->m_resp;
			double loss = m_variableCrops[i].crop->m_totalLoss;

			int index = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar*m_farmType + m_variableCrops[i].crop->m_cropType : a_foobar*m_farmType + m_variableCrops[i].crop->m_cropType_almass;
			double FUKey = m_OurManager->pm_data->Get_FUKey(index);//if it is not a fodder crop, key=0
			double FU = area*resp*(1-loss/100)*FUKey; //[ha * hkg/ha * FU/hkg = FU]
			fodderFromMinAreas += FU;
			m_totalFUgrown += FU;
		}
    }
    //this much fodder HAS to be obtained from self-production IF the requirment
    //on min fodder production holds; might be negative!

    m_fodderToProduce = m_totalFodderToProduce - (fodderFromFixed + fodderFromMinAreas);
    m_fodderToProduceBefore = m_fodderToProduce;

    //decrease the fodder demand by the amount obtained from growing fixed crops and from
    //min areas of variable crops; could be also negative now;
    m_totalFUdemand -= (fodderFromFixed + fodderFromMinAreas);

}


void AnimalFarm::determineMinFodder(int a_foobar){
/**Function assigning area to fodder crops in order to cover the min fodder production if it is not covered by the fodder from fixed crops and min. areas of variable crops. */

    double areaAssignedToFodder=0; //holds the value of area that has to be grown to cover the min fodder production requirement
    if(m_fodderToProduce > 0){
        double available=0; //area of  normal (non-fodder) crops available to substitute with fodder crops
        for(int i=0; i<(int)m_grownVariableCrops.size(); i++){
			double areaVariableCrop = m_grownVariableCrops[i]->m_areaVariable;
            available += areaVariableCrop;
        }

        for(int i=0; available>0 && i<(int)m_fodderCrops.size() && m_fodderToProduce > 0 ; i++){// if so, more has to be produced (than it is already-from min areas and fixed crops)
			int index = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar*m_farmType + + m_fodderCrops[i].crop->m_cropType : a_foobar*m_farmType + + m_fodderCrops[i].crop->m_cropType_almass;
			double rotationMax = m_fodderCrops[i].crop->m_rotationMax;
			double area = m_fodderCrops[i].crop->m_areaPercent; //fodder crop's area
            double areaBefore = area; //this is positive if areaMin>0, so save it
            double FUKey = m_OurManager->pm_data->Get_FUKey(index); //if it is not a fodder crop, key=0; number of FU per hkg of a crop
			double resp = m_fodderCrops[i].crop->m_resp;
			double loss = m_fodderCrops[i].crop->m_totalLoss;
            double FUha = resp * (1-loss/100) * FUKey;
            double haNeeded = m_fodderToProduce/FUha;
            double percNeeded = haNeeded*100/m_totalArea;
            if(percNeeded <= rotationMax - area){ //area might be positive! (min area)
                if(percNeeded <= available){
                    area += percNeeded; //the end, fodderToProduce will be 0.
                    available -= percNeeded;
                }
                else{
                    area+=available; //so we can't grow any  more fodder - no space left!
                    available=0;
                }
            }
            else{
                if(rotationMax-area >= available){
                    area += available;
                    available=0; //no space left
                }
                else{
                    available -= rotationMax-area;
                    area = rotationMax;
                }
            }
			m_fodderCrops[i].crop->m_areaPercent = area;
            double Funits = FUha * (area-areaBefore) * m_totalArea/100;
            m_fodderToProduce -= Funits; //update fodderToProduce; in 1st case - it's now = 0.
            m_totalFUdemand -= Funits; //update the total demand as well!
            m_totalFUgrown += Funits; //update the amount of grown fodder units
            areaAssignedToFodder += (area-areaBefore);

        }
        if(m_fodderToProduce>0.1){
            //restriction broken! impossible to produce the min amount of fodder - send a message, but don't stop.
			char index[ 20 ];
			sprintf( index, "%d", m_almass_no);
			g_msg->Warn( WARN_BUG, "AnimalFarm::determineMinFodder(): impossible to cover the min. fodder prod., farm no:  ", index );
			//exit( 1 );
        }
        //decreasing area of variable crops that has just been assigned to fodder crops
		//need to create a vector of type CropSort
		vector<CropSort>grownVarCrops_key;
		for(int g=0; g < (int)m_grownVariableCrops.size(); g++){
			CropSort gc = {0., m_grownVariableCrops[g]};
			grownVarCrops_key.push_back(gc);
		}

		OptimisingFarm::decreaseCrops(grownVarCrops_key, areaAssignedToFodder); //m_grownVariableCrops does not contain fodder crops-call optimising farms version. there might be here a fake crop; here use grownVarCrops_key - need CropSort type vector to use decreaseCrops;
        if(areaAssignedToFodder>0){
            //there is not enough space to grow fodder to cover the min required.
			char index[ 20 ];
			sprintf( index, "%d", m_almass_no);
			g_msg->Warn( WARN_BUG, "AnimalFarm::determineMinFodder(): not enough space to grow the min. fodder, farm no.: ", index );
			//exit( 1 );

        }
    }
} //determineMinFodder


void AnimalFarm::determineFodderAreas(int a_foobar){

/**First, it calls a function determining areas for fodder that has to be grown to fulfill the condition on min. requirement
of own fodder production.

Next, the function chooses the most profitable alternative of covering the remaining fodder demand:
purchasing fodder, growing fodder crops, or a mix of these two. In case any fodder crops are grown (above their minimum area),
one or more of the variable crops' areas has to be corrected (decreased).

The general structure of the part of the function determining whether to grow fodder, buy it or mix the two:
The outer for-loop goes through fodder crops until the total fodder demand is zero � and it starts with the crop with highest savings
(the best one). At the beginning it checks if the fodder crop has reached its max rotation area. The inner for-loop goes through all
non-fodder variable crops that after they were assigned area had an area above area minimum (there�s no point in checking other crops �
their areas cannot be decreased). It starts with the variable crop with the lowest GM (the worst one) and the condition in the loop is
a Boolean value which turns to true once a current fodder crop reached its area max. Then the outer loop takes the next best
fodder crop.

In some cases (happens when the non-fodder crops are assigned areas according to the rules from the Bedriftsmodel - i.e. using the maximum 
rotations) even when the #m_totalFUdemand is covered in 100% by fodder crops, there is still a virtual (non-fodder) crop present
at a farm (#m_fakeCrop). In such case this virtual crop has to be substituted by fodder crops and the total fodder production is higher than
the total fodder demand (#m_totalFUdemand).*/


//1. assigning area to fodder crops in order to cover the min fodder production
	if(!cash_crops_allowed) determineMinFodder(a_foobar); 

//2. now the choice - produce or purchase fodder

	m_totalFUt=0;


    //go through fodder crops - start with the one with max savings. Check if it reached its max area (could have happened in the previous step)

for(int p=0; m_totalFUdemand>0 && p<(int)m_fodderCrops.size(); p++){ //outer for loop - fodder crops;

    double rotationMax = m_fodderCrops[p].crop->m_rotationMax;
    double areaFodder = m_fodderCrops[p].crop->m_areaPercent;
    if(areaFodder < rotationMax){ //if not -> go to the next fodder crop
		double savings = m_fodderCrops[p].crop->m_savings;
        bool assigningFinished = false; //changed to true when a fodder crop reaches rotationMax

        int v = (int)m_grownVariableCrops.size()-1; 
		double minGM = 1000000; //set it high so that if there are no grown var crops - we go directly to the fodder trade code (as savings are not > minGM)
        do{ //inner loop; normal crops; starts with the crop with the lowest GM;
			if(v>=0){ 
				minGM = m_grownVariableCrops[v]->m_GM;
			}
            if(savings > minGM){  //compare savings to GM of the worst 'available' normal crop - which is currently planted and have areaVariable>0
				double areaVariable = m_grownVariableCrops[v]->m_areaVariable;
				int index = (cfg_OptimiseBedriftsmodelCrops.value())? a_foobar*m_farmType + m_fodderCrops[p].crop->m_cropType : a_foobar*m_farmType + m_fodderCrops[p].crop->m_cropType_almass;
				double FUKey = m_OurManager->pm_data->Get_FUKey(index);//if it is not a fodder crop, key=0
				double resp = m_fodderCrops[p].crop->m_resp;
				double loss = m_fodderCrops[p].crop->m_totalLoss;
				double FUha = resp*(1-loss/100)*FUKey;
				double haNeeded = m_totalFUdemand/FUha;
				double percNeeded = haNeeded*100/m_totalArea;

               if(percNeeded <= rotationMax - areaFodder){//you can plant as much as need of fodder crop, but check if there is enough space in the normal crop
                   if(percNeeded <= areaVariable){//if so, just plant as much as you need of this one fodder crop and cut the normal crop by percNeeded
                        areaFodder += percNeeded; //increase the area (not just assign!)
                        m_totalFUdemand = 0; //finish outer loop
                        m_totalFUgrown += percNeeded*FUha*m_totalArea/100;
                        areaVariable -= percNeeded;
                        assigningFinished = true; //finish inner loop
                   }
                   else{//you can plant fodder crop on an area of a normal crop and then check with the next normal crop
                        areaFodder += areaVariable; //increase by the whole 'available' area of a normal crop
                        m_totalFUdemand -= FUha*areaVariable*m_totalArea/100; //update fodder demand
                        m_totalFUgrown += areaVariable*FUha*m_totalArea/100;
                        areaVariable = 0;
                   }
               }
               else{//this fodder crop will not cover all the needs; will have to check the next one
                   if(rotationMax - areaFodder <= areaVariable){//if so, just plant as much as you need of this one fodder crop and cut the normal crop by...
                        areaVariable -= rotationMax-areaFodder;
                        m_totalFUgrown += (rotationMax-areaFodder)*FUha*m_totalArea/100;
                        m_totalFUdemand -= FUha*(rotationMax-areaFodder)*m_totalArea/100;
                        areaFodder = rotationMax; //max
                        assigningFinished = true;
						//finish inner loop-you have to take the next fodder crop
                   }
                   else{//you can plant fodder crop on an area of a normal crop and then check with the next normal crop
                        areaFodder += areaVariable; //increase by the whole 'available' area of a normal crop
                        m_totalFUdemand -= FUha*areaVariable*m_totalArea/100;
                        m_totalFUgrown += areaVariable*FUha*m_totalArea/100;
                        areaVariable = 0;
						//whole replaced by a fodder crop
                   }
               }
			   double areaMin = m_grownVariableCrops[v]->m_rotationMin;
               double areaPer = areaMin + areaVariable;
			   m_grownVariableCrops[v]->m_areaPercent = areaPer;
			   m_grownVariableCrops[v]->m_areaVariable = areaVariable;

            }
            else{//buy all the remaining fodder demand
                m_totalFUt = m_totalFUdemand; //demand is already updated for fodder from fixed crops and min areas
                m_totalFUdemand = 0; //finish outer loop
                assigningFinished = true; //finish inner loop
            }
            v--;    
        }while (assigningFinished==false && v>=0);//inner loop (normal crops)

		m_fodderCrops[p].crop->m_areaPercent = areaFodder;//update fodder crop - after the inner loop

        if(assigningFinished==false){ //loop was stopped by the condition->no more space on normal crops available
            //there's no more space on normal crops-have to buy the remaining totalFUdemand
            m_totalFUt += m_totalFUdemand;
        }
    }
}//outer for-loop (fodder crops)


if(m_totalFUdemand > 0){ //added 30.01 - for some farms the condition 'if(areaFodder<rotationMax)' is never fulfilled, so the inner loop will never run
    m_totalFUt = m_totalFUdemand;
    m_totalFUdemand = 0;
}

if(m_fakeCropTest==true){ //only if it was used in determineAreas - check if it is >0
    //check if the fake crop is still there - if so, substitute it with fodder crops
	int s = (int)m_grownVariableCrops.size()-1;
    double areaFakeCrop = m_grownVariableCrops[s]->m_areaPercent;
    if(areaFakeCrop > 0){
        increaseCrops(m_fodderCrops, areaFakeCrop);
        m_grownVariableCrops[s]->m_areaPercent = areaFakeCrop; //sets fake crop's area at 0.
    }
	//remove the fake crop from the grownVariable crops vector
	m_grownVariableCrops.erase(m_grownVariableCrops.begin() + s);
}

}// end of determineFodderAreas


//------------------------------------------------------------------------------//
//-----------------------OTHER FUNCTIONS----------------------------------------//
//------------------------------------------------------------------------------//

double OptimisingFarm::crop_parameter(int index, string par_name){
	double par_value;
	par_value = m_OurManager->pm_data->Get_cropParameter(top_Foobar*index + m_OurManager->TranslateParametersCodes(par_name));
	return 	par_value;
}

CropOptimised * OptimisingFarm::findCropByName (string crop_name){
	for(int c=0; c< (int)m_crops.size(); c++){
		if(m_crops[c]->m_cropType==m_OurManager->TranslateCropsCodes(crop_name)) return m_crops[c];
	}
	g_msg->Warn( WARN_FILE, "OptimisingFarm::findCropByName():" "Unknown Code Identificator", crop_name.c_str() );
	exit(0);
}

CropOptimised * OptimisingFarm::findCropByName_almass (string crop_name){
	for(int c=0; c< (int)m_crops.size(); c++){
		if(m_crops[c]->m_cropType_almass == m_OurManager->TranslateCropCodes(crop_name)) return m_crops[c];
	}
	g_msg->Warn( WARN_FILE, "OptimisingFarm::findCropByName_almass():" "Unknown Code Identificator", crop_name.c_str() );
	exit(0);
}

CropOptimised * OptimisingFarm::findCropByName_almass (TTypesOfVegetation a_tov_type){
	for(int c=0; c< (int)m_crops.size(); c++){
		if(m_crops[c]->m_cropType_almass == a_tov_type) return m_crops[c];
	}
	char error_num[ 20 ];
    sprintf( error_num, "%d",  a_tov_type);
	g_msg->Warn( WARN_BUG, "OptimisingFarm::findCropByName_almass():" "Unknown Code Identificator", error_num );
	exit(0);
}

void OptimisingFarm::sortCrops(vector<CropSort> &cropsToSort, string sortKey){
/**Function for sorting crops using sortingKey as a criterion for sorting. First argument is a vector of structs containing a pointer to
a crop and a key.*/

	if(sortKey=="GM"){
		for (int i=0; i<(int)cropsToSort.size(); i++){//define the key to sorting
				cropsToSort[i].key = cropsToSort[i].crop->m_GM;
			}
	}
	else if (sortKey=="Savings"){
		for (int i=0; i<(int)cropsToSort.size(); i++){//define the key to sorting
				cropsToSort[i].key = cropsToSort[i].crop->m_savings;
			}
	}
	else {
		for (int i=0; i<(int)cropsToSort.size(); i++){//define the key to sorting
			cropsToSort[i].key = cropsToSort[i].crop->m_GM_Savings;
			}
	}

	sort(cropsToSort.begin(), cropsToSort.end(), reverseSort());
}

void OptimisingFarm::randomizeCropList(vector<CropSort> &listToRandomize, string key){
/**Function randomly swaps elements of a sorted vector holding equal values of the key used for sorting. Uses a vector to carry out shuffling.*/

    std::vector<CropSort>helper; //to store and shuffle elements with an equal value of a key
    helper.push_back(listToRandomize[0]); //save the first element of the list in the helper list

    for(int i=1; i<(int)listToRandomize.size(); i++){ 
        double key_current, key_previous;
		if(key=="GM"){
			key_current=listToRandomize[i].crop->m_GM;
			key_previous=listToRandomize[i-1].crop->m_GM;
		}
		else if (key=="Savings"){
			key_current=listToRandomize[i].crop->m_savings;
			key_previous=listToRandomize[i-1].crop->m_savings;
		}
		else if(key=="GM_Savings"){
			key_current=listToRandomize[i].crop->m_GM_Savings;
			key_previous=listToRandomize[i-1].crop->m_GM_Savings;
		}
		else{
			key_current=key_previous=0;
			g_msg->Warn( WARN_FILE, "OptimisingFarm::randomizeCropList():" "Unknown sorting key identificator", key.c_str() );
			exit(0);
		}

        if(key_current==key_previous){
            helper.push_back(listToRandomize[i]);
        }
        else{
            std::random_shuffle(helper.begin(), helper.end());
            //insert the elements of the vector - back to the original list
            //assign the elements of the vector in the reverse order (that's easier then the
            //normal order). The last element of the list has index = i-1.
            for(int j=(int)helper.size()-1; j>=0; j--){
                listToRandomize[i-1-j]=helper[j];
            }
            helper.clear(); //remove the elements...
            helper.push_back(listToRandomize[i]); //and 'start' a new vector with the current element
        }
    }
}

void OptimisingFarm::increaseCrops(vector<CropSort>cropsToIncrease, double &howMuchToIncrease){
/**Function increases area of one or more crops from the list given as a first argument.The total area added to the crops in the list is equal to the value of the second argument.*/

    for(int i=0; howMuchToIncrease>0 && i<(int)cropsToIncrease.size(); i++){
        double area = cropsToIncrease[i].crop->m_areaPercent;
		double rotationMax = cropsToIncrease[i].crop->m_rotationMax;
		double rotMinCrop = cropsToIncrease[i].crop->m_rotationMin;
        if(area + howMuchToIncrease <= rotationMax){
            area += howMuchToIncrease; //finito
            howMuchToIncrease = 0;
        }
        else{
            howMuchToIncrease -= rotationMax-area;
            area = rotationMax;
        }
		cropsToIncrease[i].crop->m_areaPercent = area;
		cropsToIncrease[i].crop->m_areaVariable = area - rotMinCrop; 
    }
}

void AnimalFarm::increaseCrops(vector<CropSort>cropsToIncrease, double &howMuchToIncrease){
/**Function increases area of one or more crops from the list given as a first argument.The total area added to the crops in the list is equal to the value of the second argument.
In case an area of a fodder crop is increased, the #m_totalFUgrown is increased and the #m_totalFUt is decreased by the amount of fodder corresponding
to the added area of a given fodder crop.*/

	for(int i=0; howMuchToIncrease>0 && i<(int)cropsToIncrease.size(); i++){
        double area = cropsToIncrease[i].crop->m_areaPercent;
        double areaBefore = area; 
		double rotationMax = cropsToIncrease[i].crop->m_rotationMax;
		double rotMinCrop = cropsToIncrease[i].crop->m_rotationMin;
        if(area + howMuchToIncrease <= rotationMax){
            area += howMuchToIncrease; //finito
            howMuchToIncrease = 0;
        }
        else{
            howMuchToIncrease -= rotationMax-area;
            area = rotationMax;
        }
		cropsToIncrease[i].crop->m_areaPercent = area;
		cropsToIncrease[i].crop->m_areaVariable = area - rotMinCrop; 

    //checking if area has changed
		if(area > areaBefore){
			int index = (cfg_OptimiseBedriftsmodelCrops.value())? toc_Foobar*m_farmType + cropsToIncrease[i].crop->m_cropType : tov_Undefined*m_farmType + cropsToIncrease[i].crop->m_cropType_almass;
			bool fodder = m_OurManager->pm_data->Get_fodder(index);
			if(fodder){ //this is a fodder crop - so you need to buy less fodder (totalFUt)
				double resp = cropsToIncrease[i].crop->m_resp;
				double loss = cropsToIncrease[i].crop->m_totalLoss;
				double FUKey = m_OurManager->pm_data->Get_FUKey(index);
				double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
				double changeFU = (area-areaBefore) * m_totalArea/100 * FUha; //positive
				m_totalFUt -= changeFU;
				m_totalFUgrown += changeFU;
			}
		}
    }
}

void OptimisingFarm::decreaseCrops(vector<CropSort>cropsToDecrease, double &howMuchToDecrease){
/**Function decreases area of one or more crops from the list given as a first argument.The total area subtracted from the crops in the list is equal to the value of the second argument.*/

    for(int j=(int)cropsToDecrease.size()-1; howMuchToDecrease>0 && j>=0; j--){ //start with the worst
		double areaCrop = cropsToDecrease[j].crop->m_areaPercent;
		double rotMinCrop = cropsToDecrease[j].crop->m_rotationMin;
        if(areaCrop - howMuchToDecrease >= rotMinCrop){
            areaCrop -= howMuchToDecrease;
            howMuchToDecrease = 0; //finito
        }
        else{
            howMuchToDecrease -= areaCrop - rotMinCrop;
            areaCrop = rotMinCrop;
        }
		cropsToDecrease[j].crop->m_areaPercent = areaCrop;
		cropsToDecrease[j].crop->m_areaVariable = areaCrop - rotMinCrop; 
    }
}

void AnimalFarm::decreaseCrops(vector<CropSort>cropsToDecrease, double &howMuchToDecrease){
/**Function decreases area of one or more crops from the list given as a first argument.The total area subtracted from the crops in the list 
is equal to the value of the second argument. In case an area of a fodder crop is decreased, the #m_totalFUgrown is decreased and the totalFUt is increased by the amount of
fodder corresponding to the removed area of a given fodder crop.*/

    for(int j=(int)cropsToDecrease.size()-1; howMuchToDecrease>0 && j>=0; j--){ //start with the worst
		double areaCrop = cropsToDecrease[j].crop->m_areaPercent;
		double areaBefore = areaCrop; 
		double rotMinCrop = cropsToDecrease[j].crop->m_rotationMin;
        if(areaCrop - howMuchToDecrease >= rotMinCrop){
            areaCrop -= howMuchToDecrease;
            howMuchToDecrease = 0; //finito
        }
        else{
            howMuchToDecrease -= areaCrop - rotMinCrop;
            areaCrop = rotMinCrop;
        }
		cropsToDecrease[j].crop->m_areaPercent = areaCrop;
		cropsToDecrease[j].crop->m_areaVariable = areaCrop - rotMinCrop; 

        if(areaCrop < areaBefore){
			int index = (cfg_OptimiseBedriftsmodelCrops.value())? toc_Foobar*m_farmType + cropsToDecrease[j].crop->m_cropType : tov_Undefined*m_farmType + cropsToDecrease[j].crop->m_cropType_almass;
			bool fodder = m_OurManager->pm_data->Get_fodder(index);
            if(fodder){ //this is a fodder crop - so you need to buy more fodder (totalFUt)
				double resp = cropsToDecrease[j].crop->m_resp;
				double loss = cropsToDecrease[j].crop->m_totalLoss;
                double FUKey = m_OurManager->pm_data->Get_FUKey(index);
                double FUha = resp * (1-loss/100) * FUKey; //[ha * hkg/ha * FU/hkg = FU]
                double changeFU = (areaBefore - areaCrop) * m_totalArea/100 * FUha; //positive number
                m_totalFUt += changeFU;
                m_totalFUgrown -= changeFU;
            }
        }
    }
}

void OptimisingFarm::determineAreas_ha(vector<CropOptimised*>allCrops){ //area in ha
/**Determines areas of crops in ha: #CropOptimised::m_area_ha = #CropOptimised::m_areaPercent/100 * #m_totalArea. */

    for(int i=0; i<(int)allCrops.size(); i++){
		double areaPercent = allCrops[i]->m_areaPercent;
        double area_ha = (areaPercent==0) ? 0 : (areaPercent/100 * m_totalArea);
		allCrops[i]->m_area_ha = area_ha;
    }
}

double OptimisingFarm::total(TTypesOfCropVariables a_var){
/**Function for determining total values of crop variables per farm. Goes through the list of all crops. If an area of a crop is positive, it adds
the area of this crop (in ha) multiplied by the value of a variable (per ha) to the total value per farm.*/

    double totalAmount = 0;
    for (int i=0; i<(int)m_crops.size(); i++){
		double area_ha = m_crops[i]->m_area_ha;
        if(!area_ha==0){ //otherwise don't change totalAmount
           double value_per_ha;
		   switch (a_var){
				case tocv_AreaPercent:
					value_per_ha = m_crops[i] ->m_areaPercent;
					break;
				case tocv_AreaHa:
					value_per_ha = m_crops[i] ->m_area_ha;
					break;
				case tocv_N:
					value_per_ha = m_crops[i] ->m_n;
					break;
				case tocv_Nt:
					value_per_ha = m_crops[i] ->m_nt;
					break;
				case tocv_BIHerb:
					value_per_ha = m_crops[i] ->m_BIHerb;
					break;
				case tocv_BIFi:
					value_per_ha = m_crops[i] ->m_BIFi;
					break;
				case tocv_BI:
					value_per_ha = m_crops[i] ->m_BI;
					break;
				case tocv_Grooming:
					value_per_ha = m_crops[i] ->m_grooming;
					break;
				case tocv_Hoeing:
					value_per_ha = m_crops[i] ->m_hoeing;
					break;
				case tocv_Weeding:
					value_per_ha = m_crops[i] ->m_weeding;
					break;
				case tocv_TotalLoss:
					value_per_ha = m_crops[i] ->m_totalLoss;
					break;
				case tocv_Response:
					value_per_ha = m_crops[i] ->m_resp;
					break;
				case tocv_Income:
					value_per_ha = m_crops[i] ->m_income_ha;
					break;
				case tocv_Costs:
					value_per_ha = m_crops[i] ->m_costs_ha;
					break;
				case tocv_GM:
					value_per_ha = m_crops[i] ->m_GM;
					break;
				case tocv_Savings:
					value_per_ha = m_crops[i] ->m_savings;
					break;
				default:
					g_msg->Warn( WARN_BUG, "OptimisingFarm::total(): ""Unknown crop variable type! ", "" );
					exit( 1 );
				}
				totalAmount += value_per_ha * area_ha; //each crop's/(field's) optimal values per ha multiplied by acreage of a crop
		   }
        }
		return totalAmount;
    }

void OptimisingFarm::Print_FarmVariables(ofstream * ap_output_file){
/**Prints farm level variables into one file (FarmVariables.txt). */

	//print the almass farm number
	(*ap_output_file) << m_almass_no << '\t';

	//print the values
	(*ap_output_file) <<  m_main_goal << '\t' << m_totalArea << '\t' <<  m_totalIncome<<'\t'<<  m_totalCosts <<'\t'<<  m_totalProfit <<'\t';
	(*ap_output_file) <<  m_totalFUdemand <<'\t' <<  m_totalFUdemandBefore << '\t' <<  m_totalFUt <<'\t'<< m_totalFUgrown << '\t';
	(*ap_output_file) <<  m_Nanim <<'\t'<<  m_totalNanim <<'\t'<<  m_totalNt <<'\t'<<  m_totalN <<'\t';
	(*ap_output_file) <<  m_totalBI <<'\t'<<  m_totalBIHerb <<'\t'<<  m_totalBIFi <<'\t';
	(*ap_output_file) <<  m_totalGrooming <<'\t'<<  m_totalHoeing <<'\t'<<  m_totalWeeding <<'\t';
	(*ap_output_file) << endl;
}

void OptimisingFarm::Check_if_area_100(){
/**Determines the sum of all crops' #CropOptimised::m_areaPercent. If the sum is not equal to 100, it issues a warning in a "Check_if_area_is_100%.txt" file. */

	double area_to_check = 0.0;
	for(int i=0; i<(int)m_crops.size(); i++) {
		area_to_check += m_crops[i]->m_areaPercent;
	}

	if(area_to_check > 100.0001 || area_to_check < 99.999){ //issue a warning
		ofstream ofile("Check_if_area_is_100%.txt",ios::app); 
		ofile << m_almass_no << '\t' << "Farms area is not equal to 100%. The sum of crops areaPercent is: " << area_to_check << endl;
		ofile.close();
	}
}

void OptimisingFarm::Translate_crops_to_almass(){
/**The function takes a vector m_crops as an input. It goes through all crops; for those with a positive areaPercent,
it finds an equivalent in almass crops. The output is a vector of structs AlmassCrop (with 2 members:
area in percent and tov, type of vegetation).*/

	for(int i = 0;  i < (int)m_crops.size(); i++){
		if(m_crops[i]->m_areaPercent > 0){
			TTypesOfCrops toc = m_crops[i]->m_cropType; //Bedriftsmodel crop type
			TTypesOfVegetation tov;

			switch (toc) {
				case 0: //toc_SBarley
					tov =  tov_SpringBarley;
					break;
				case 1: //toc_Oats
					tov = tov_Oats;
					break;
				case 2: //toc_OSCrops
					tov = tov_SpringBarley; //spring wheat - no code for this crop
					break;
				case 3: //toc_WBarley
					tov = tov_WinterBarley;
					break;
				case 4: //toc_WWheat
					tov = tov_WinterWheat;
					break;
				case 5: //toc_WRye
					tov = tov_WinterRye;
					break;
				case 6: //toc_Triticale
					tov = tov_Triticale;
					break;
				case 7: //toc_SRape
					tov = tov_SpringRape;
					break;
				case 8: //toc_WRape
					tov = tov_WinterRape;
					break;
				case 9: //toc_OOilseed
					tov = tov_WinterRape; 
					break;
				case 10: //toc_Peas
					tov = tov_FieldPeas;
					break;
				case 11: //toc_OLSeed
					tov = tov_FieldPeas;
					break;
				case 12: //toc_GrassSeed //TRANSLATES TO TWO ALMASS CROPS
					tov = tov_SeedGrass1; //this translates to both tov_SeedGrass1 and tov_SeedGrass2; thus sg2 has to be considered in the rotation vector after the sg1; both get area percent = half of the original crop area percent
					break;
				case 13: //toc_Potato
					tov = tov_PotatoesIndustry;
					break;
				case 14: //toc_PotatoFood
					tov = tov_Potatoes; 
					break;
				case 15: //toc_SugarBeet
					tov = tov_FodderBeet;
					break;
				case 16: //toc_GrassClover //TRANSLATES TO TWO ALMASS CROPS
					tov = tov_CloverGrassGrazed1; // also tov_CloverGrassGrazed2; need to first aggregate with CloverGrz and then divide the sum by two!
					break;
				case 17: //toc_OLSeedCut
					tov = tov_FieldPeasSilage; 
					break;
				case 18: //toc_SCerealSil
					tov = tov_SpringBarleySilage;  //undersown
					break;
				case 19: //toc_PeasSil
					tov = tov_FieldPeasSilage; 
					break;
				case 20: //toc_MaizeSil
					tov = tov_MaizeSilage; //
					break;
				case 21: //toc_WCerealSil
					tov = tov_SpringBarleySilage;
					break;
				case 22: //toc_SCerealG
					tov = tov_SpringBarleySilage;
					break;
				case 23: //toc_PerGrassLow
					tov = tov_PermanentGrassLowYield;
					break;
				case 24: //toc_PerGrassNorm
					tov = tov_PermanentGrassGrazed;
					break;
				case 25: //toc_GrassEnv1
					tov =  tov_PermanentGrassTussocky; // is aggregated with GrassEnv2
					break;
				case 26: //toc_GrassEnv2
					tov = tov_PermanentGrassTussocky; // is aggregated with GrassEnv1 
					break;
				case 27: //toc_GrassRot
					tov = tov_FodderGrass; 
					break;
				case 28: //toc_Setaside
					tov = tov_Setaside;
					break;
				case 29: //toc_Uncult
					tov = tov_PermanentSetaside; 
					break;
				case 30: //toc_OUncult
					tov = tov_PermanentSetaside; 
					break;
				case 31: //toc_FodderBeet
					tov = tov_FodderBeet;
					break;
				case 32: //toc_CloverGrz //TRANSLATES TO TWO ALMASS CROPS
					tov = tov_CloverGrassGrazed1; // also tov_CloverGrassGrazed2; need to first aggregate with GrassClover and then divide the sum by two!
					break;
				case 33: //toc_Veg
					tov = tov_Carrots;
					break;
				case 34: //toc_Fruit
					tov = tov_OrchardCrop;
					break;
				case 35: //toc_FruitTree
					tov = tov_OrchardCrop;
					break;
				case 36: //toc_OSpecCrops
					tov = tov_Carrots; // could be also smth else
					break;
				case 37: //toc_ChrisTree
					tov = tov_YoungForest;
					break;
				case 38: //toc_EnergyFor
					tov = tov_YoungForest; // ideally should be willow
					break;
				case 39: //toc_Other
					tov = tov_Carrots; // this also could be smth else
					break;
				default:
					g_msg->Warn( WARN_BUG, "OptimisingFarm::Translate_crops_to_almass(): ""Unknown crop type! ", "" );
					exit( 1 );
				}

				//try not separating fixed from variables crops
				AlmassCrop crop = { m_crops[i]->m_areaPercent, tov};
				m_crops_almass.push_back(crop);

		} //if
	}//for

	//aggregate the almass crops that are in >1 position in the  m_crops vector - so that they are listed only once

	vector<AlmassCrop>crops_almass_aggregated;
	int size = (int)m_crops_almass.size();

	for(int i = 0;  i < size; i++){
		TTypesOfVegetation tov1 = m_crops_almass[i].Tov;
		double summaryArea = m_crops_almass[i].Number; //initialize
		bool skip = false;

		for(int p = 0; p < i; p++){ //check if this tov was already aggregated (so check elements of the vector before this one)
			if(m_crops_almass[p].Tov == tov1){ //this tov is already aggregated, so skip i element
				skip = true;
			}
		}
		if (!skip){
			for(int j = i+1; j < size; j++){ // start comparing with the crop following the one at position i
				if(m_crops_almass[j].Tov == tov1){
					summaryArea += m_crops_almass[j].Number; //add area to the summary area
				}
			}
			AlmassCrop crop = {summaryArea, m_crops_almass[i].Tov, };
			crops_almass_aggregated.push_back(crop);
		}
	}
	m_crops_almass = crops_almass_aggregated; //save the aggregated crops in m_crops_almass vector


	//now deal with the crops that translate to more than one almass crop, i.e.:
	//1) GrassClover + CloverGrz = CGG1 + CGG2
	//2) GrassSeed = SG1 + SG2


	for(int i = 0; i < (int)m_crops_almass.size(); i++){
		if(m_crops_almass[i].Tov == tov_CloverGrassGrazed1){
			double pct = (m_crops_almass[i].Number)/2; //have to divide the area between CGG1 and CGG2
			m_crops_almass[i].Number = pct;
			AlmassCrop cgg2 = {pct, tov_CloverGrassGrazed2};
			m_crops_almass.push_back(cgg2);
		}
		if(m_crops_almass[i].Tov == tov_SeedGrass1){
			double pct = (m_crops_almass[i].Number)/2; //have to divide the area between SG1 and SG2
			m_crops_almass[i].Number = pct;
			AlmassCrop sg2 = {pct, tov_SeedGrass2};
			m_crops_almass.push_back(sg2);
		}
	}

}


void OptimisingFarm::Make_almass_crops_vector(){
/** The function goes throught the list of all crops and attach them to the m_crops_almass vector if the area resulting from optimisation
is larger than zero. */

	m_crops_almass.clear();

	for(int i = 0;  i < (int)m_crops.size(); i++){
		if(m_crops[i]->m_areaPercent > 0){
			TTypesOfVegetation tov = m_crops[i]->m_cropType_almass;
			AlmassCrop crop = { m_crops[i]->m_areaPercent, tov};
			m_crops_almass.push_back(crop);
		}
	}

}

void OptimisingFarm::Make_rotational_crops(){

	//first deal with the probelmatic crops:CGG1 + CGG2 and SB, and SG1 + SG2
	double area_cgg=0;
	for(int i = 0; i < (int)m_crops_almass.size(); i++){
		if(m_crops_almass[i].Tov == tov_CloverGrassGrazed1){
			bool cg2=false;
			for(int j = 0; j < (int)m_crops_almass.size(); j++){
				if(m_crops_almass[j].Tov == tov_CloverGrassGrazed2){
					double pct = (m_crops_almass[i].Number + m_crops_almass[j].Number)/2; //take average
					m_crops_almass[i].Number = pct;
					m_crops_almass[j].Number = pct;
					area_cgg = pct;
					cg2=true;
				}
			}
			if(!cg2){ //there is cgg1 but not cgg2
				double pct = (m_crops_almass[i].Number)/2; //divide cgg1 area by two and save it in cgg1 and create cgg2
				m_crops_almass[i].Number = pct;
				area_cgg = pct;
				AlmassCrop cgg2 = {pct, tov_CloverGrassGrazed2};
				m_crops_almass.push_back(cgg2);
			}
			break;
		}
	}

	//cgg and sb
	bool sb=false;
	double new_area = area_cgg; //initialize with the current area of cgg - maybe it won't change
	for(int i = 0; i < (int)m_crops_almass.size(); i++){
		if(m_crops_almass[i].Tov == tov_SpringBarley){
			sb=true;
			double area_sb = m_crops_almass[i].Number;
			if(area_sb < area_cgg){ //problem
				new_area = (area_sb + 2*area_cgg)/3;
				m_crops_almass[i].Number = new_area;
			}
			break;
		}
	}
	if(!sb){ //there wasn't spring barley - need to add
		new_area = area_cgg*2/3;
		if(new_area >0){ //otherwise do not add sbarley
			AlmassCrop sbarley = {new_area, tov_SpringBarley};
			m_crops_almass.push_back(sbarley);
		}
	}
	//now find both cgg and change their areas
	for(int j = 0; j < (int)m_crops_almass.size(); j++){
		if(m_crops_almass[j].Tov == tov_CloverGrassGrazed1){
			m_crops_almass[j].Number = new_area;
		}
		if(m_crops_almass[j].Tov == tov_CloverGrassGrazed2){
			m_crops_almass[j].Number = new_area;
		}
	}



	//sg1 and sg2
	for(int i = 0; i < (int)m_crops_almass.size(); i++){
		if(m_crops_almass[i].Tov == tov_SeedGrass1){
			bool sg2=false;
			for(int j = 0; j < (int)m_crops_almass.size(); j++){
				if(m_crops_almass[j].Tov == tov_SeedGrass2){
					double pct = (m_crops_almass[i].Number + m_crops_almass[j].Number)/2; //take average
					m_crops_almass[i].Number = pct;
					m_crops_almass[j].Number = pct;
					sg2=true;
				}
			}
			if(!sg2){ //there is cgg1 but not cgg2
				double pct = (m_crops_almass[i].Number)/2; //divide cgg1 area by two and save it in cgg1 and create cgg2
				m_crops_almass[i].Number = pct;
				AlmassCrop sgrass2 = {pct, tov_SeedGrass2};
				m_crops_almass.push_back(sgrass2);
			}
			break;
		}
	}

	//first clear the current m_rotational_crops
	m_rotational_crops.clear();

	//then add rotational crops from the list of all crops - both rotational and permanent
	m_area_rot = 0;//sum of areas of rotational crops - need it for checking if there is any field without perm crops in case there are no rot. crops
	for(int i = 0; i < (int)m_crops_almass.size(); i++){
		TTypesOfVegetation tov = m_crops_almass[i].Tov;

		if (tov==tov_PermanentGrassGrazed || tov==tov_PermanentGrassTussocky || tov==tov_PermanentSetaside ||
			tov==tov_PermanentGrassLowYield || tov==tov_YoungForest  || tov==tov_OrchardCrop) { 
			//do nothing - just need rotational crops
		}
		else {
			double pct = m_crops_almass[i].Number;
			if(pct<0.5) pct+=0.5; //thanks to that such crop won't disappear! (won't have area=0 after rounding)
			AlmassCrop pcd = {pct, tov};
			m_rotational_crops.push_back(pcd);
			m_area_rot += pct;
		}
	}
}

void OptimisingFarm::Print_rotations(ofstream * ap_output_file){
/**Prints the content of m_rotation vector into the FarmVariables text file. */

	//print the almass farm number
	//(*ap_output_file) << m_almass_no << '\t';

	//print the content of the m_rotation vector
	for(int i=0; i<(int)m_rotation.size(); i++){
		TTypesOfVegetation crop = m_rotation[i];
		(*ap_output_file) << i << '\t' << crop << endl;
	}
	(*ap_output_file) << endl;
}

void OptimisingFarm::Make_rotations(){
/** Used in the Bedriftsmodel crop mode. The function provides the functionality of the UserDefinedFarm's constructor:
- it assigns true or false to the variable #Farm::m_stockfarmer
- creates a vector of permanent crops
- creates a vector of rotational crops, m_rotation vector (size equal 100 or 0 in case there are only permanent crops) with a number of 
crop ocurrences corresponding to the optimised crop areas in percent of the total farm area. */


	//1. STOCK FARM
	if (m_farmType==toof_Cattle || m_farmType==toof_Pig) m_stockfarmer = true;
	else m_stockfarmer = false;

	//2. INTENSITY - skip

	//3. PERMANENT vs ROTATIONAL CROPS
		//sort them and put into two vectors: m_PermCrops (member of Farm class, type: struct PermCropData) + local vector rotational_crops (type: struct AlmassCrop)

	int no_perm = 0;
	int no_rotational = 0;
	int area_perm = 0; //sum of areas of permanent crops
	m_area_rot = 0; //sum of areas of rotational crops

	for(int i = 0; i < (int)m_crops_almass.size(); i++){
		TTypesOfVegetation tov = m_crops_almass[i].Tov;

		if (tov==tov_PermanentGrassGrazed || tov==tov_PermanentGrassTussocky || tov==tov_PermanentSetaside ||
			tov==tov_PermanentGrassLowYield || tov==tov_YoungForest  || tov==tov_OrchardCrop) { 
			int pct = (int)(m_crops_almass[i].Number + 0.5); //round a double to int
			PermCropData pcd = {tov, pct};
			m_PermCrops.push_back(pcd);
			no_perm++;
			area_perm += pct;
		}
		else {
			double pct = m_crops_almass[i].Number;
			if(pct<0.5) pct+=0.5; //thanks to that such crop won't disappear! (won't have area=0 after rounding)
			AlmassCrop pcd = {pct, tov};
			m_rotational_crops.push_back(pcd);
			no_rotational++;
			m_area_rot += pct;
		}
	}

	//4. ROTATIONAL CROPS

	//first check if there are any rotational crops!
	if(m_rotational_crops.size()!=0){

		m_rotation.resize(100);
		vector<TTypesOfVegetation>all_crops; //vector the has a right proportion of each crop tov type
		vector<MakeRotation>rotation;
		rotation.resize(100);

		//scale the rotational crop areas to account for the area that is reserved for permanent crops
		//and
		//save scaled areas of SBarley and CGG1 to make a check in the next step
		int area_rot_int = (int)(m_area_rot + 0.5); //area in percent of rot crops - as an integer
		int SBarley_area=0;
		int CGG1_area=0;

		for(int i=0; i<(int)m_rotational_crops.size(); i++){
			double area = m_rotational_crops[i].Number;
			int area_scaled = (int)(100 * area / area_rot_int + 0.5);
			m_rotational_crops[i].Number = area_scaled;
			if(m_rotational_crops[i].Tov == tov_CloverGrassGrazed1) CGG1_area = (int) m_rotational_crops[i].Number; //scaled area
			if(m_rotational_crops[i].Tov == tov_SpringBarley) SBarley_area = (int) m_rotational_crops[i].Number; //(this is scaled area)
		}

		//check if we have enough spring barley for CGG1 and CGG2 (area CGG1 = area CGG2); if not - change the areas so that
		//SBarley = CGG1 = CGG2 area.
		if(SBarley_area < CGG1_area){ //problem
			if(SBarley_area == 0){ //there's no sbarley! - need to create
				AlmassCrop sbarley  = {0, tov_SpringBarley};
				m_rotational_crops.push_back(sbarley); //add sbarley
			}
			int new_area = (int)((CGG1_area * 2 + SBarley_area)/3 + 0.5); //= (CGG1 + CGG2 + SBarley)/3 ->and take integer
			for(int i=0; i<(int)m_rotational_crops.size(); i++){
				if(m_rotational_crops[i].Tov == tov_CloverGrassGrazed1 || m_rotational_crops[i].Tov == tov_CloverGrassGrazed2 ||
					m_rotational_crops[i].Tov == tov_SpringBarley){
					m_rotational_crops[i].Number = new_area;
				}
			}
		}


		//fill the all_crops vector with tov types -> in the right proportions
		for(int i = 0; i<(int)m_rotational_crops.size(); i++){
			int area = (int) m_rotational_crops[i].Number; //conversion to int - OK! - this is already int after the previous for loop
			for(int j = 0; j<area; j++){
				all_crops.push_back(m_rotational_crops[i].Tov);
			}
		}//now the size of all_crops might NOT be 100! - so check and make it to be 100?

		//sort the all_crops vector and rearrange it so that the probability of two crops that cannot follow each other is minimised
		//so - the order of the for loops below matter!!!
		vector<TTypesOfVegetation>all_crops_helper; //vector helper

		for(int i = (int)all_crops.size()-1; i>=0; i--){ 
			if(all_crops[i] == tov_SpringBarley) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}

		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_CloverGrassGrazed1) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_CloverGrassGrazed2) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_FodderBeet) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_WinterRape) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_SpringRape) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_Potatoes) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_SeedGrass1) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = 0; i<(int)all_crops.size(); i++){
			if(all_crops[i] == tov_SeedGrass2) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_FieldPeas) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_FieldPeasSilage) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_WinterBarley) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_WinterWheat) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}
		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_Setaside) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}

		for(int i = (int)all_crops.size()-1; i>=0; i--){
			if(all_crops[i] == tov_MaizeSilage) {
				all_crops_helper.push_back(all_crops[i]);
				all_crops.erase(all_crops.begin() + i);
			}
		}

		//now place the other - remaining crops (in the all_crops vector) in further positions of the helper vector
		for(int i = 0; i<(int)all_crops.size(); i++){
				all_crops_helper.push_back(all_crops[i]);
		}

		//check if the size is 100 - correct if not
		if(all_crops_helper.size()!=100){
			if(all_crops_helper.size()>100){
				for(int i = (int)all_crops_helper.size(); i>100; i--){
					all_crops_helper.erase(all_crops_helper.end()-1); //just remove the last element
				}
			}
			else{ //<100
				for(int i = (int)all_crops_helper.size(); i<100; i++){
					if(all_crops_helper[i-2] != tov_CloverGrassGrazed2) all_crops_helper.push_back(all_crops_helper[i-2]); //this way I duplicate the ending (max 2/4 last elements)
					else all_crops_helper.push_back(tov_SpringBarley); //add spring barley
				}
			}
		}

		//copy the helper to all_crops
		all_crops = all_crops_helper;


		//fill the rotation vector with tov_Undefined
		for(int i = 0; i<(int)rotation.size(); i++){
				rotation[i].Tov = tov_Undefined;
		}


		// go through all_crops - should be 100...

		for(int i = 0; i < (int)all_crops.size(); i++){

			//find a place for the tov in the rotation vector:
			bool test=false;
			int count=0;
			TTypesOfVegetation crop_index = all_crops[i]; //current crop (tov) you want to place in a vector

			int step=no_rotational;
			int pp=0; 

			for(int j = pp;  count<101 && test==false; j=j+step){	
				double sb_area = (cfg_OptimiseBedriftsmodelCrops.value())? findCropByName ("SBarley")->m_areaPercent : findCropByName_almass ("SpringBarley")->m_areaPercent;
				if (crop_index == tov_SpringBarley && sb_area > (100/step)) step = (int)(100/sb_area + 0.5);
				if(j>=100) {//without this we would always start out in the same position
					if (crop_index == tov_SpringBarley) {pp+=3; j=pp;} //special case - sbarley, cgg1 and cgg2; has to be 3 - doesn't work with 4
					else j=++pp;
				}

				if(!(rotation[j].taken)){ //not taken - so check if it is allowed here - lookup

					TTypesOfVegetation crop_before, crop_after;

					crop_before = (j==0) ? rotation[99].Tov : rotation[j-1].Tov;
					crop_after = (j==99) ? rotation[0].Tov : rotation[j+1].Tov;

					int lookup_before = m_OurManager->Get_lookup_table(crop_before * (tov_Undefined+1) + crop_index); //check with a preceeding crop; tov_undef+1 - because this tov_undef is included in the lookup table
					int lookup_after = m_OurManager->Get_lookup_table(crop_index * (tov_Undefined+1) + crop_after); //check with a following crop

					if(lookup_before==-1 || lookup_after==-1){ //issue a warning: wrong index...
						char index[ 20 ];
						sprintf( index, "%d", crop_index);
						g_msg->Warn( WARN_BUG, "OptimisingFarm::Make_rotations(): Error (possibly caused by a new tov type that is not incuded in the crops_lookup table) in reading the lookup table; tov type that should not appear: ", index );
						exit( 1 );
					}

					if(lookup_before==1 && lookup_after==1){
						rotation[j].Tov = all_crops[i]; //same as crop_index
						rotation[j].taken = true;
						test=true;
					}
					else count++; //did not succeed in placing the crop, so increase the count
				}
				else{
					count++; //did not succeed in placing the crop, so increase the count
				}

			} //inner for loop: searching for a place for a given crop

			if(test==false){ //there is no place that does not break the lookup table rules

				//so try to go through the whole rotation and find a place where the current crop fits

				for(int r=0; r<(int)rotation.size() && test==false; r++){

					TTypesOfVegetation crop_before, crop_after;

					crop_before = rotation[r].Tov; 
					crop_after = (r==99) ? rotation[0].Tov : rotation[r+1].Tov;
					int lookup_before = m_OurManager->Get_lookup_table(crop_before * (tov_Undefined+1) + crop_index); //check with a preceeding crop; tov_undef+1 - because this tov_undef is included in the lookup table
					int lookup_after = m_OurManager->Get_lookup_table(crop_index * (tov_Undefined+1) + crop_after); //check with a following crop

					if(lookup_before==1 && lookup_after==1){
						MakeRotation crop = {true, all_crops[i]};
						rotation.insert(rotation.begin() + r+1, crop); //->insert before element r+1
						test=true;

						//need to remove 1 element of the rotation! as 1 additional was just inserted
						//but this might cause troubles: two already placed elements might become incompatible...

						for(int p=(int)rotation.size()-1; p>=0; p--){//start at the end - more efficient, as here should be empty elements
							if(rotation[p].Tov == tov_Undefined){
								rotation.erase(rotation.begin() + p);
								//check if the 'new neighbours' can be next to each other
								crop_before = (p==0) ? rotation[99].Tov : rotation[p-1].Tov; 
								crop_after = (p==100) ? rotation[0].Tov : rotation[p].Tov; //100 because the index is p
								int crop_after_position = (p==100) ? 0 : p;

								lookup_before = m_OurManager->Get_lookup_table(crop_before * (tov_Undefined+1) + crop_after); //check with a preceeding crop; tov_undef+1 - because this tov_undef is included in the lookup table

								TTypesOfVegetation crop_before1, crop_after1, crop_before2;
								crop_before2=tov_Undefined; //for the begininng
								int crop_before2_position = 0; // **CJT** Added to remove warning 09-08-2018
								int count2=0;

								while(lookup_before!=1){ //need to move somewhere the crop_after

									//check if there is no correct rotation possible
									if(count2>100){ //problem: impossible to make a rotation
										//so switch some crop...to Spring Barley
										TTypesOfVegetation problem_crop = rotation[crop_before2_position].Tov;
										rotation[crop_before2_position].Tov = tov_SpringBarley; 
										//issue a message
										char index[ 20 ];
										sprintf( index, "%d", problem_crop);
										g_msg->Warn( WARN_BUG, "OptimisingFarm::Make_rotations(): The following crop was changed to spring barley: ", index );
										break; //break the if
									}

									crop_index = crop_after; //now crop_after is searching for a place
									bool test2=false;
									for(int t=0; t<(int)rotation.size() && test2==false; t++){
										crop_before1 = rotation[t].Tov; 
										crop_after1 = (t==99) ? rotation[0].Tov : rotation[t+1].Tov;
										int lookup_before1 = m_OurManager->Get_lookup_table(crop_before1 * (tov_Undefined+1) + crop_index); //check with a preceeding crop; tov_undef+1 - because this tov_undef is included in the lookup table
										int lookup_after1 = m_OurManager->Get_lookup_table(crop_index * (tov_Undefined+1) + crop_after1); //check with a following crop

										if(lookup_before1==1 && lookup_after1==1){
											//check for indefinite loop

											bool skip=true;
											for(int s=crop_after_position; s!=t+1; s++){ //check locations between;
												if(s==100) s=0;
												TTypesOfVegetation crop = rotation[s].Tov;
												int lookup = m_OurManager->Get_lookup_table(crop_before2 * (tov_Undefined+1) + crop);
												if( lookup == 1){
													skip = false;
													break; //there is a different crop that the one that we removed (crop_index) - so there shouldnt be an infinite loop
												}
											}

											if(!skip){

												MakeRotation crop = {true, crop_index};
												rotation.insert(rotation.begin() + t+1, crop);
												//now the crop to be removed can be at the same or changed position, depending on where we inserted the crop

												if(t+1 > crop_after_position){ //OK
													rotation.erase(rotation.begin() + crop_after_position); //remove the crop that was just placed in a new position-from the old one
													crop_before2 = (crop_after_position==0) ? rotation[99].Tov : rotation[crop_after_position-1].Tov;
													crop_before2_position = (crop_after_position==0) ? 99 : crop_after_position-1;
													crop_after = (crop_after_position==99) ? rotation[0].Tov : rotation[crop_after_position].Tov; 
													crop_after_position = (crop_after_position==99) ? 0 : crop_after_position;
													lookup_before = m_OurManager->Get_lookup_table(crop_before2 * (tov_Undefined+1) + crop_after);
												}
												else{
													rotation.erase(rotation.begin() + crop_after_position + 1); //can be 99+1 -ok, now the size is 101
													crop_before2 = (crop_after_position==0) ? rotation[99].Tov : rotation[crop_after_position].Tov;
													crop_before2_position = (crop_after_position==0) ? 99 : crop_after_position;
													crop_after = (crop_after_position==99) ? rotation[0].Tov : rotation[crop_after_position+1].Tov; 
													crop_after_position = (crop_after_position==99) ? 0 : crop_after_position+1;
													lookup_before = m_OurManager->Get_lookup_table(crop_before2 * (tov_Undefined+1) + crop_after);
												}
												test2=true;
											}

										}

									}
									count2++;
								} //while

								break; //break the for (index p) loop, need to remove just one element
							}//undefined removed
						}//for:searching for an undef element to remove
					}//if:found a place to put an element that doesnt fit in any empty space left
				}//for: searching for a place for a problematic crop

				if(test==false){ //we have a problem
					char index[ 20 ];
					sprintf( index, "%d", crop_index);
					//sprintf( index, "%d", m_almass_no);
					g_msg->Warn( WARN_BUG, "OptimisingFarm::Make_rotations(): The following tov type cannot be assigned a place in the rotation: ", index );
					exit( 1 );
				}

			}//there's no empty space that fits for the current crop
		}//outer for loop: searcin for an empty spae for a current crop


		//copy the tovs from rotation vector to the m_rotation vector
		for(int i=0; i<100; i++){
			m_rotation[i] = rotation[i].Tov;
		}

	}//if there are any rotational crops

else{ //no rotational crops
	m_rotation.resize(0);
	}
}

void OptimisingFarm::ActualProfit(){ 
/**  The function calculates actual yields, income, costs, profits etc. for a given farm in a previous year by accounting for crops grown at its fields.
	Determines farmer's need satisfaction level (#m_need_satisfaction_level) which depends on farmer's actual results relative to the expected ones; the values compared depend on a farmer type, profit maximizer
	and environmentalist look at profit, yield maximizer looks at aggregated yield.
	The function also accounts for the pesticide and fertilizer usage in order to obtain aggregate results.
	The function is used only in ALMaSS crop set mode. */


	vector<TTypesOfVegetation> veg_type;
	//vector<double>profits; //profit from each field
	//profits.resize(m_fields.size());

	double priceNt = cfg_Price_Nt.value(); 

	//for energy maize impacts on wildlife
	bool emaize=false;

	//exp. agg. yield - for now it is not necessary to do this every year since the fields size is constant, but later it might change!
	m_exp_aggregated_yield = (double)m_fields.size(); //cause each field is expected to give 100% = 1. Except the fields with a crop with exp yield = 0.

	//go through all farm's field:
	for(int i=0; i<(int)m_fields.size(); i++){

		CropActualValues crop_data;
		TTypesOfVegetation crop;
		double biomass=0; double area=0; double yield_loss_pest=0; double no_herb_app=0; double no_fi_app=0;
		double unit_loss_herb=0; double unit_loss_fi=0; //to determine or take from field's data
		double yield_ha=0; double income_ha=0; double income=0; double costs_ha=0; double costs=0; double profit_ha=0; double profit=0; //to determine
		double yield_exp=0; double yield_ratio=0; double income_exp=0; double costs_exp=0; double profit_exp=0; //to account for - it was already calculated per crop in the determine GM function
		double sellingPrice=0; double priceHerb=0; double priceFi=0; double priceLM=0; double priceH=0; double priceW=0; double priceG=0; double subsidy=0; //economic data - need to be given as input;
		double no_missed_herb_app, no_missed_fi_app = 0; //no_missed - how many applications of herb/fi were missed
		double fert_applied=0; double fert_opt=0; double fert_trade=0; double beta1=0; double beta2=0; double alfaH=0; double alfaFI=0;
		double yield_factor = 0; //this serves both as biomass-to-yield factor and day degrees-to-yield factor - they are saved in one input table (a single crop has just one of these factors!)
		VegElement * pf =  dynamic_cast<VegElement*>(m_fields[i]);

		int cropDataStorage_index=-1; //initialize

		if(pf->Get_taken(1)){ //there is a crop at position one - so account for this crop; otherwise go to position 0
			crop_data = pf->Get_CropDataStorage(1);
			cropDataStorage_index = 1;
		}
		else{
			crop_data = pf->Get_CropDataStorage(0);
			cropDataStorage_index = 0;
			if(crop_data.taken==false){//error
				g_msg->Warn( WARN_BUG, "OptimisingFarm::ActualProfit(): There is no crop data in the fields m_CropDataStorage", "" );
				exit( 1 );
			}
		}

		crop = crop_data.tov_type;

		//for energy maize impacts on wildlife
		if (crop==tov_Maize) emaize = true;
		//

		bool do_accounting = true;
		if(cropDataStorage_index==0){ //could be this crop was not harvested yet
			bool harvested = crop_data.harvested;
			if(crop!= tov_PotatoesIndustry && crop!=tov_Potatoes && crop!=tov_CloverGrassGrazed1 && crop!=tov_CloverGrassGrazed2 && crop!= tov_SpringBarleySilage && crop!=tov_PermanentGrassLowYield
				&& crop!=tov_PermanentGrassGrazed && crop!=tov_PermanentGrassTussocky && crop!=tov_YoungForest && crop!=tov_OrchardCrop && crop!=tov_FodderGrass
				&& !harvested){
				//this is a crop that shoud be harvested, but so far it wasn't - so do not account for this crop yet! - just skip this field in accounting
				do_accounting = false;
			}
		}

		if(do_accounting){
			area = crop_data.area * 0.0001; //change the units from sq. meters to ha!

			if(crop==tov_SeedGrass1 || crop==tov_SeedGrass2){ //alfaherb is more than 100! - so problem with negative yield
				double betaH = crop_parameter(crop, "BetaHerb");
				alfaFI = crop_parameter(crop, "AlfaFi");
				unit_loss_herb = betaH/100/2;
				unit_loss_fi = alfaFI/100;
			}
			else{
				alfaH = crop_parameter(crop, "AlfaHerb");
				alfaFI = crop_parameter(crop, "AlfaFi");
				unit_loss_herb = alfaH/100; //yield loss per unit of herb application, = max yield loss/optBI = betaH/optBI = alfa. (and /100).
				unit_loss_fi = alfaFI/100; //yield loss per unit of fung-insecticide application
			}

			//draw random numbers to modify the pest related yield loss
			//method from: http://stackoverflow.com/questions/9651024/using-stdtr1normal-distribution-in-qt
			std::random_device rd;
			std::normal_distribution<double> dist(1, 0.2);
			std::mt19937 engine(rd());
			double r_number_1 = dist(engine);
			double r_number_2 = dist(engine);


			//determine the pest related yield loss
			no_missed_herb_app = crop_data.missed_herb_app;
			no_missed_fi_app = crop_data.missed_fi_app;
			yield_loss_pest = no_missed_herb_app * unit_loss_herb * r_number_1 + no_missed_fi_app * unit_loss_fi * r_number_2; //total yield loss due to missed pesticide applications

			//determine the yield
			biomass = crop_data.biomass_at_harvest;
			yield_factor = m_OurManager->pm_data->Get_biomass_factor(tov_Undefined * m_soilType + crop); //crop is tov - integer

			fert_applied = findCropByName_almass (crop)->m_n;
			fert_opt = findCropByName_almass (crop)->m_optimalN;
			fert_trade = findCropByName_almass (crop)->m_nt;

			int index = tov_Undefined * m_soilType + crop;
			beta1=m_OurManager->pm_data->Get_beta1(index);
			beta2=m_OurManager->pm_data->Get_beta2(index);

			double dd=m_OurManager->GetDD();

			yield_ha = (biomass==-1)? dd : biomass; //if biomass=-1 this crop is not harvested - use daydegress to estimate the yield
			yield_ha *= yield_factor; //multiply by the factor to convert biomass to yield - works both for harvested and not harvested crops.
			if(fert_opt!=0){
				yield_ha -= (1 - fert_applied/fert_opt) * (beta1 * fert_opt + beta2 * fert_opt*fert_opt); //subtract the amount which depends on the applied ferilizer - if the applied=optimal, then amount subtracted=0. The expression in brackets - with betas - corresponds to the max yield loss-when fert=0.
			}
			yield_ha *= (1-yield_loss_pest); //finally account for the yield loss due to pests

			//get other numbers for economic calculations
			sellingPrice = m_OurManager->pm_data->Get_sellingPrice(tov_Undefined*tos_Foobar*m_farmType + tov_Undefined*m_soilType + crop); //always tov_Undefined because this funtion is called ONLY  in almass crop mode!
			priceFi = crop_parameter(crop, "PriceFi");
			priceHerb = crop_parameter(crop, "PriceHerb");
			priceG = crop_parameter(crop, "PriceG");
			priceH = crop_parameter(crop, "PriceH");
			priceW = crop_parameter(crop, "PriceW");
			priceLM = crop_parameter(crop, "PriceLM");
			subsidy =crop_parameter(crop, "Subsidy");

			double grooming = findCropByName_almass (crop)->m_grooming;
			double hoeing = findCropByName_almass (crop)->m_hoeing;
			double weeding = findCropByName_almass (crop)->m_weeding;

			no_herb_app = crop_data.no_herb_app;
			no_fi_app = crop_data.no_fi_app;

			//special case for potatoes and fodder beet
			if(crop==tov_Potatoes || crop==tov_PotatoesIndustry){
				no_herb_app = 1.41; //need to be consistent with numbers in the fix BI function!
				no_fi_app = 9.28;
			}
			if(crop==tov_FodderBeet){
				no_herb_app = 2.28; //need to be consistent with numbers in the fix BI function!
			}

			//saving the amount of pesticides and fertilizer
			if(g_date->GetYearNumber()>9){ //do it for the first time in year 10 ->only then we account for new dec.- modes
				for(int c=0; c<m_OurManager->pm_data->Get_noCrops(); c++){
					if(m_OurManager->Get_crops_summary_BIs_tov(c) == crop){
						m_OurManager->Set_crops_summary_BIs_herb(c, no_herb_app * area);
						m_OurManager->Set_crops_summary_BIs_fi(c, no_fi_app * area);
						m_OurManager->Set_crops_summary_BIs(c, (no_fi_app+no_herb_app) * area);
						m_OurManager->Set_crops_fertilizer(c, fert_applied * area);
						m_OurManager->Set_crops_fertilizer_trade(c, fert_trade * area);
						break;
					}
				}
			}


		//GET THE ACTUAL FIGURES:
			//1. determine the actual income from this crop: use selling price also for fodder crops! they do not add to income, just diminish the costs of fodder...hm
			income_ha = yield_ha * sellingPrice + subsidy;
			income = income_ha * area; //total income from this crop

			//2. account for the costs incurred for this crop
			costs_ha = no_herb_app*priceHerb + no_fi_app*priceFi + grooming*priceG + hoeing*priceH + weeding*priceW + fert_trade*priceNt+ priceLM;
			costs = costs_ha * area;

			//3. determine the actual profit
			profit_ha = income_ha - costs_ha;
			profit = profit_ha * area;

			//4. determine what part of the expected yield you actually got from this crop - but do this by ha! here there are only crops that were grown this year - because we go through the list of fields
			yield_exp = findCropByName_almass (crop)->m_resp;
			if(yield_exp!=0){
				yield_ratio = yield_ha/yield_exp; // must be min. 0, can be > than 1.
			}
			else{
				m_exp_aggregated_yield -=1; //this crop wasn't expected to yield anything, so exclude it from the sum of expected yields
			}

		//add actual figures for this crop to the totals at a farm this year
			m_actual_income += income;
			m_actual_costs += costs;
			m_actual_profit += profit;
			m_actual_aggregated_yield += yield_ratio;


		//add expected figures for this crop to the totals at a farm  this year
			//get the expected values! use the actual area - otherwise a comparison doesn't make sense!

			//to determine the expected income use last year's price
			double resp = findCropByName_almass (crop)->m_resp;
			double totalLoss = findCropByName_almass (crop)->m_totalLoss;
			double sellingPrice_lastyr = m_OurManager->pm_data->Get_sellingPrice_lastyr(tov_Undefined*tos_Foobar*m_farmType + tov_Undefined*m_soilType + crop); //always tov_Undefined because this funtion is called ONLY  in almass crop mode!

			int index1 = tov_Undefined * m_farmType + findCropByName_almass (crop)->m_cropType_almass;
			bool fodder = m_OurManager->pm_data->Get_fodder(index1);
			if (fodder) income_exp = subsidy;
			else income_exp = (sellingPrice_lastyr*resp*(1-totalLoss/100)+subsidy)*area;

			costs_exp = findCropByName_almass (crop)->m_costs_ha *area;
			profit_exp = income_exp - costs_exp; 

			m_exp_income += income_exp; //add income expected from this crop to the total expected income
			m_exp_costs += costs_exp;
			m_exp_profit += profit_exp;

			//clean data in this field's struct: remove the data that you just accounted for
			pf->Clean_CropDataStorage(cropDataStorage_index);
		}
		else{//there was no accounting done, print the crop and year in which this happened
			#ifdef _DEBUG
			g_msg->Warn("OptimisingFarm::ActualProfit(): There was no accounting for tov: ", crop);
			//g_msg->Warn("Farm Number: ",GetFarmNumber());
			g_msg->Warn("Year Number: ",g_date->GetYearNumber());
			#endif
		}

	//send the info on this field's crop for the landscape total crop areas accounting
		//first find the right index in m_cropTotals:
		int index_for_croptotals=0;
		int size = m_OurManager->pm_data->Get_cropTypes_almass_size();
		for(int k = 0; k<size; k++){
			TTypesOfVegetation crop_type = m_OurManager->pm_data->Get_cropTypes_almass(k);
			if(crop_type == crop){
				index_for_croptotals = k;
				break;
			}
		}
		//then add the area of this field to the right element of m_cropTotals; change the units from sq meters to ha.
		m_OurManager->Add_to_cropTotals(index_for_croptotals, m_fields[i]->GetArea() * 0.0001); //yearly totals
		if(g_date->GetYearNumber()>20){ //do it for the first time in year 10 ->only then we account for new dec.- modes//for wildlife runs - only since year 20 when emaize appears, but here in year 20 we only account for year 19...that's why it has to be '>20'
			m_OurManager->Set_cropTotals_sum(index_for_croptotals, m_fields[i]->GetArea() * 0.0001);
			if(m_farmType == toof_Plant){
				m_OurManager->Set_cropTotals_plant_sum(index_for_croptotals, m_fields[i]->GetArea() * 0.0001);
			}
			else if(m_farmType == toof_Pig){
				m_OurManager->Set_cropTotals_pig_sum(index_for_croptotals, m_fields[i]->GetArea() * 0.0001);
			}
			else if(m_farmType == toof_Cattle){
				m_OurManager->Set_cropTotals_cattle_sum(index_for_croptotals, m_fields[i]->GetArea() * 0.0001);
			}
			else if(m_farmType == toof_Other){
				m_OurManager->Set_cropTotals_other_sum(index_for_croptotals, m_fields[i]->GetArea() * 0.0001);
			}
		}

		//then add also to farm type specific crop totals



	}//for: going through m_fields


	//print the expected and actual values
	ofstream ofile ("Economic_figures.txt", ios::app);
	ofile << "farm_no: " << m_almass_no << '\t' << "year_no " << g_date->GetYearNumber() << endl;
	ofile << "Expected: " << '\t' <<"Income " << m_exp_income << "\tCosts "<< m_exp_costs << "\tProfit " << m_exp_profit<< "\tAgg.yield " << m_exp_aggregated_yield;
	ofile << endl;
	ofile << "Actual: " << '\t' << "Income " << m_actual_income << "\tCosts "<< m_actual_costs << "\tProfit " << m_actual_profit<< "\tAgg.yield " << m_actual_aggregated_yield;
	ofile << endl <<endl;
	ofile.close();


	//save the economic figures for this year (or rather past - we account for things happen in the year before the current March 1st, xxxx)
	m_previous_profits.push_back(m_actual_profit);
	m_previous_incomes.push_back(m_actual_income);
	m_previous_costs.push_back(m_actual_costs);
	m_previous_aggregated_yields.push_back(m_actual_aggregated_yield);


	double min_need_satisfaction1 = cfg_Min_need_satisfaction1.value(); 
	double min_need_satisfaction2 = cfg_Min_need_satisfaction2.value();  
	if(cfg_OnlyDeliberation.value()){ //then everybody deliberates (and: min cert is set to 0 when config var OnlyDeliberation is set to true)
		min_need_satisfaction1 = 100;
		min_need_satisfaction2 = 100;
	}

	if(m_main_goal == tofg_profit || m_main_goal == tofg_environment){
		if(m_exp_profit>0){
			if(m_actual_profit >= min_need_satisfaction1*m_exp_profit){
				m_need_satisfaction_level=1;
			}
			else{
				m_need_satisfaction_level=0;
			}
		}
		else if(m_exp_profit<0){
			if(m_actual_profit >= (2 - min_need_satisfaction1) * m_exp_profit){ //e.g. EP=-1000, AP=-1100 -> ok for MNS<=0.9.
				m_need_satisfaction_level=1;
			}
			else{
				m_need_satisfaction_level=0;
			}
		}
		else{ //exp profit = 0
			m_need_satisfaction_level  = (m_actual_profit >0)? 1 : 0; //need to deal with this case - not very likely to happen
		}
	}
	else if (m_main_goal == tofg_yield){
		m_need_satisfaction_level = (m_actual_aggregated_yield >= m_exp_aggregated_yield * min_need_satisfaction2)? 1 : 0 ;
	}

	m_previous_satisfaction_levels.push_back(m_need_satisfaction_level); //add it to the list of satisfaction levels from last 5 years
	if(m_previous_satisfaction_levels.size()>5){ // it's been more than 5 years since the first accoutning, so...
		m_previous_satisfaction_levels.erase(m_previous_satisfaction_levels.begin()); //remove the first element: satisfaction level 6 years ago
	}

	//for energy maize impact on wildlife - January 2014
	if(cfg_MaizeEnergy.value()){
		double threshold = cfg_AnimalsThreshold.value();
		double sum = 0;
		for(int i=0; i<m_animals_numbers.size(); i++){
			sum += m_animals_numbers[i];
		}
		double avrg_animals_no = 0;
		if(m_animals_numbers.size()>0){
			avrg_animals_no = sum/m_animals_numbers.size();
		}

		if(emaize && animals_no < threshold * avrg_animals_no && m_animals_numbers.size()>2){ //three conditions: 1. you actually have grown energy maize 
			// 2. the animal numbers have decreased below the acceptable level 3. you have data on animal numbers from min. 3 years
			//then set the rotation max. for energy maize to zero and force deliberation to stop growing energy maize
			CropOptimised *em = findCropByName_almass ("Maize"); 
			em->m_rotationMax = 0;
			force_deliberation = true;
		}
		m_animals_numbers.push_back(animals_no);
		if(m_animals_numbers.size() > 3){ //have data for more than 3 years now
			m_animals_numbers.erase(m_animals_numbers.begin()); //remove the oldest data - first element
		}

		//force optimisation when energy maize price changed by more than a given %
		double EMPrice_lastyr = m_OurManager->pm_data->Get_sellingPrice_lastyr(tov_Undefined*tos_Foobar*m_farmType + tov_Undefined*m_soilType + tov_Maize); 
		double EMPrice_current = m_OurManager->pm_data->Get_sellingPrice(tov_Undefined*tos_Foobar*m_farmType + tov_Undefined*m_soilType + tov_Maize); 
		if(abs(EMPrice_current - EMPrice_lastyr) > EMPrice_lastyr * cfg_PriceChangeThreshold.value()){
			force_deliberation = true;
		}
	}
	//

	//and restart the actual and expected values - for next year
	m_exp_income = 0;
	m_exp_costs = 0;
	m_exp_profit = 0;
	//exp agg yield is determined at the begininng of this function;
	m_actual_income = 0;
	m_actual_costs = 0;
	m_actual_profit = 0;
	m_actual_aggregated_yield = 0;

}


void OptimisingFarm::Match_crop_to_field(LE* a_field){

	/**The function chooses a crop to grow in a given filed after the management plan of the current crop is finished.

	1. Creates a probability distribution of choosing rotational crops based on their planned areas (as percentages of a farm's area).\n
	2. Draws a number from the distribution and identifies which crop the number refers to.\n
	3. Checks if the crop is allowed to follow the current crop in a field, and if the difference between the field's area and crop's planned area is within acceptable range
	  (to avoid growing too much of a given crop). If either of the conditions is not fulfilled, point 2 is repeated until a crop fulfilling both conditions is chosen.
	  If this procedure fails in choosing a crop 20 times, the 2nd condition is released and the search is continued.\n
	4. If there is no crop fulfilling even just the 1st condition, the farm vector #m_rotational_crops_copy is restarted, so that it includes all rotational crops a farmer wants to plant this year, including those
	   that might have already been planted on other field this year.\n
	5. If there is still no crop that can follow a current crop, spring barley is grown (this situation happens when in the rotation there are only crops that can follow one
	   specific crop, e.g. CloverGrassGrazed1 can follow only SpringBarley and CloverGrassGrazed2 can only follow CloverGrassGrazed1).\n
	6. When a crop to be grown is found, the function switches the rotation index to point to the crop placed in m_rotation before the chosen crop.
	*/


	double field_size_pct = a_field->GetArea()/GetArea() * 100; //convert field size to pct of total arable farm area (a_rotational_crops will contain values of areas in %)
	TTypesOfVegetation current_crop =a_field->GetVegType();
	TTypesOfVegetation crop_to_grow = tov_Undefined;

	if(current_crop==tov_PermanentGrassGrazed || current_crop==tov_PermanentGrassTussocky || current_crop==tov_PermanentSetaside ||
			current_crop==tov_PermanentGrassLowYield || current_crop==tov_YoungForest  || current_crop==tov_OrchardCrop) {

				crop_to_grow = current_crop; //and do not switch rot index - has to stay as it is to mark the field as perm
	}
	else{

		if((int)m_rotational_crops_copy.size()==0 ){ // in the 1st year it is empty since the choose dec. mode was not called yet!
			m_rotational_crops_copy = m_rotational_crops; //restart the copy - it is empty and we still need crops
			//debug
			if((int)m_rotational_crops_copy.size()==0){ //should not happen!
				char index[ 20 ];
				sprintf( index, "%d", m_almass_no);
				g_msg->Warn( WARN_BUG, "OptimisingFarm::Match_crop_to_field(): rot crops vector is empty! Farm no: ", index );
				exit( 1 );
			}
		}

		if (current_crop==tov_SeedGrass1){ 
			bool sg2=false;
			for(int i=0; i< (int)m_rotational_crops_copy.size(); i++){
				TTypesOfVegetation crop = m_rotational_crops_copy[i].Tov;
				if(crop==tov_SeedGrass2) sg2 = true;
			}
			if (!sg2){ //there is no seedgrass2 in the list of crops. Otherwise just go to the normal procedure of choosing crop
				crop_to_grow = tov_SeedGrass2;
			}
		}
		if (current_crop==tov_CloverGrassGrazed1){
			bool cgg2=false;
			for(int i=0; i< (int)m_rotational_crops_copy.size(); i++){
				TTypesOfVegetation crop = m_rotational_crops_copy[i].Tov;
				if(crop==tov_CloverGrassGrazed2) cgg2 = true;
			}
			if (!cgg2){ //there is no tov_CloverGrassGrazed2 in the list of crops. Otherwise just go to the normal procedure of choosing crop
				crop_to_grow = tov_CloverGrassGrazed2;
			}
		}

		if (current_crop!=tov_SpringBarley){ 
			bool other_crop_than_cgg = false;
			for(int i=0; i< (int)m_rotational_crops_copy.size(); i++){
				TTypesOfVegetation crop = m_rotational_crops_copy[i].Tov;
				if(crop!=tov_CloverGrassGrazed2 && crop!=tov_CloverGrassGrazed1){
					other_crop_than_cgg = true;
					break;
				}
			}
			if (!other_crop_than_cgg){ //there is no other tov than CGG1 or CGG2 in the list of crops. So need to grow smth which will fit - SB.
				crop_to_grow = tov_SpringBarley;
			}
		}

		bool Sb_inserted=false;
		if(crop_to_grow==tov_Undefined){ //if not, it is already CGG2 or SG

			//create the vector with numbers
			vector<int>probability_line;
			probability_line.push_back(0); //add the first element
			for(int i=0; i< (int)m_rotational_crops_copy.size(); i++){
				if(m_rotational_crops_copy[i].Number<0.5) m_rotational_crops_copy[i].Number += 0.5; //just in case - to avoid having crops that disappeear
				int area = (int)(m_rotational_crops_copy[i].Number + 0.5); //change to integer
				probability_line.push_back(area + (int)probability_line[probability_line.size() - 1]); //add a number equal to the sum of the current last element and this crop's area
			}

			//pick an index of a crop
				//see: http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
			std::random_device rd;
			std::mt19937 gen(rd());
			int last_number = probability_line[(int)probability_line.size() - 1]; //the last number
			distribution_type2 dis(0, last_number - 1);
			double crop_index = 0;

			double diff = 0;
			int index_j = 0;
			bool crop_found = false;
			for(int c = 0; !crop_found; c++){
				crop_index = dis(gen);	 //int from a range 0 - sum of crop areas in percent - only crops that haven't been grown yet - on their 'full' i.e. planned area
				//which crop is it?
				for(int j=0; j < (int)probability_line.size(); j++){
					if(j==(int)probability_line.size()-1) { //unless j=0 smth's wrong: we didnt find this index
						if(j==0){ //there are no crops left - prob. line contains just one element - 0.why?
							char index[ 20 ];
							sprintf( index, "%d", (int) crop_index);
							g_msg->Warn( WARN_BUG, "OptimisingFarm::Match_crop_to_field(): no crops left, index drawn is ", index );
							exit( 1 );
						}
						else{
							char index[ 20 ];
							sprintf( index, "%d", (int) crop_index);
							g_msg->Warn( WARN_BUG, "OptimisingFarm::Match_crop_to_field(): index not found in the prob. line", index );
							exit( 1 );
						}
					}
					if(crop_index >= probability_line[j] && crop_index < probability_line[j+1]) { 
						if(j >= (int)m_rotational_crops_copy.size()) { //problem
							char index[ 20 ];
							sprintf( index, "%d", j);
							g_msg->Warn( WARN_BUG, "OptimisingFarm::Match_crop_to_field(): index out of a range", index );
							exit( 1 );
						}
						crop_to_grow = m_rotational_crops_copy[j].Tov;
						index_j = j;
						break; //got the crop type
					}

				}
				//make a check
				int lookup_before = m_OurManager->Get_lookup_table(current_crop * (tov_Undefined+1) + crop_to_grow); //check with a preceeding crop; tov_undef+1 - because this tov_undef is included in the lookup table
				if(lookup_before==-1){ //issue a warning: wrong index...
					char index[ 20 ];
					sprintf( index, "%d", current_crop);
					g_msg->Warn( WARN_BUG, "OptimisingFarm::Match_crop_to_field(): Error (possibly caused by a new tov type that is not incuded in the crops_lookup table) in reading the lookup table; tov type that should not appear: ", index );
					exit( 1 );
				}
				if(lookup_before==1){ //if so, make another check - but not if we tried already 20 times - then release this constraint

					bool WRape_grown = false;
					if(cfg_Areas_Based_on_Distribution.value()){ //then another check: spring rape cannot be grown if the farm grows winter rape within 1000 m (www.landbrugsinfo.dk)
						if(crop_to_grow == tov_SpringRape){
							for(int w=0; w<(int)m_fields.size(); w++){
								if(m_fields[w]->GetVegType() == tov_WinterRape) WRape_grown = true;
							}
						}
					}

					if(c<20){
						diff = field_size_pct - m_rotational_crops_copy[index_j].Number;
						if(diff <= 20 && !WRape_grown){ //diff in size smaller than 20 percent points - ok. Plus there is no winter rape around in case the chosen crop is SRape
							crop_found=true;
							break; //break the for - this crop is OK
						}
					}
					else{
						diff = field_size_pct - m_rotational_crops_copy[index_j].Number; 
						if(!WRape_grown){ 
							crop_found=true; //this crop must be accepted
							break;
						}
					}
				}
				if(c==1000 && !crop_found){ //seems there's no crop that can follow the current one, so restart the crops instead and continue the search!  
					m_rotational_crops_copy = m_rotational_crops; //restart - that's not enough - need a new prob line
					probability_line.clear();
					probability_line.push_back(0); //add the first element
					for(int i=0; i< (int)m_rotational_crops_copy.size(); i++){
						if(m_rotational_crops_copy[i].Number<0.5) m_rotational_crops_copy[i].Number += 0.5; //necessary to avoid having crop disappeeared 
						int area = (int)(m_rotational_crops_copy[i].Number + 0.5); //change to integer
						probability_line.push_back(area + (int)probability_line[probability_line.size() - 1]); //add a number equal to the sum of the current last element and this crop's area
					}

					last_number = probability_line[(int)probability_line.size() - 1]; //the last number
					distribution_type2 dis(0, last_number - 1);
				}
				if(c>1500){ //seems there's no crop that can follow the current one - issue a warning; but now do this only after restarting
					#ifdef _DEBUG
						char index[ 20 ];
						sprintf( index, "%d", current_crop);
						g_msg->Warn( WARN_BUG, "OptimisingFarm::Match_crop_to_field(): There is no crop that can follow the current crop. Sbarley will be grown. The current crop is: ", index );
					#endif
					crop_to_grow=tov_SpringBarley;
					Sb_inserted=true;
					break;
					//exit( 1 );
				}
			}

			//crop found, so update the vector of crops
			if(!Sb_inserted){
				if(diff < 0){ //crop area should be larger than this field's area
					m_rotational_crops_copy[index_j].Number -= field_size_pct;
				}
				else{
					if(m_rotational_crops_copy.size() <1){ //smth is wrong
						char index[ 20 ];
						sprintf( index, "%d", current_crop);
						g_msg->Warn( WARN_BUG, "OptimisingFarm::Match_crop_to_field(): m_rotational_crops_copy is empty, the crop chosen is: ", index );
						exit( 1 );
					}
					m_rotational_crops_copy.erase(m_rotational_crops_copy.begin() + index_j);
				}
			}
		}

		//switch the rotational index:

			//1. find the chosen crop in the m_rotation
		int position_in_mrotation=-1;
		int size=(int)m_rotation.size();

		for(int c=0; c< (int)m_rotation.size(); c++){
			if(m_rotation[c] == crop_to_grow){
				position_in_mrotation=c;
				break;
			}
		}

		if(position_in_mrotation==-1){
			char error_num[ 20 ];
			sprintf( error_num, "%d", crop_to_grow );
			g_msg->Warn( WARN_FILE, "OptimisingFarm::Match_crop_to_field(): ""Unknown vegetation type:", error_num );
			exit( 1 );
		}
			//2. switch the rot index to a crop just before the chosen one 
		if(position_in_mrotation!=0){
			a_field->SetRotIndex(position_in_mrotation-1);
		}
		else{
			a_field->SetRotIndex(size-1);
		}
	}//current crop is a rotational crop
}

/**
\brief
Returns the list of fields more open than a_openness
*/
polylist* Farm::ListOpenFields( int a_openness )
{
	polylist* p_list = new polylist;
	int nf = (int) m_fields.size();
	for ( int i = 0; i < nf; i++ )
	{
		if ( m_fields[ i ]->GetOpenness() >= a_openness )
		{
			p_list->push_back( m_fields[i]->GetPoly());
		}
	}
	return p_list;
}
