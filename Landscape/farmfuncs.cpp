/**
\file
\brief
<B>Farmfuncs.cpp This file contains the source for implementing the farm events</B> \n
*/
/**
\file
 by Frank Nikolaisen & Chris J. Topping \n
 Initial version of June 2003, but under continual change. \n
 All rights reserved. \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//
// farmfuncs.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
#define UNREFERENCED_PARAMETER(P) (P)

#include "ls.h"


using namespace std;

extern Landscape* g_landscape_p;
extern CfgFloat cfg_pest_product_1_amount;
extern CfgInt l_pest_productapplic_startdate;
extern CfgInt l_pest_productapplic_enddate;
extern CfgInt l_pest_productapplic_period;
extern CfgBool cfg_OptimiseBedriftsmodelCrops;
extern CfgInt cfg_productapplicstartyear;
extern CfgInt cfg_productapplicendyear;

#define DO_IT_PROB (l_farm_event_do_it_prob.value())
#define DO_IT_PROB_LONG (l_farm_event_do_it_prob_long.value())

static CfgInt l_farm_event_do_it_prob("FARM_EVENT_DO_IT_PROB", CFG_PRIVATE, 50);
static CfgInt l_farm_event_do_it_prob_long("FARM_EVENT_DO_IT_PROB_LONG", CFG_PRIVATE, 5);
static CfgFloat l_farm_cattle_veg_reduce("FARM_CATTLE_VEG_REDUCE", CFG_CUSTOM, 1.5);
static CfgFloat l_farm_cattle_veg_reduce2("FARM_CATTLE_VEG_REDUCE_LOW", CFG_CUSTOM, 1.00 );
static CfgFloat l_farm_pig_veg_reduce("FARM_PIG_VEG_REDUCE", CFG_CUSTOM, 0.98 );
static CfgFloat cfg_CustomInsecticideKillProp("CUSTOMINSECTIVIDEKILLPROP", CFG_CUSTOM, 0.5,0,1.0);

// Determine whether insecticide and herbicide actually
// influences the insect population. By default they *do*
// kill off some of the little critters.
static CfgBool l_farm_insecticide_kills("FARM_INSECTICIDE_KILLS", CFG_CUSTOM, true );
static CfgBool l_farm_herbicide_kills("FARM_PESTICIDE_KILLS", CFG_CUSTOM, true );

/** \brief Provided to allow configuration control of the first insecticide spray in winter wheat - this changes the day in the month */
CfgInt cfg_WW_InsecticideDay("PEST_WWINSECTONEDAY", CFG_CUSTOM, 1);
/** \brief Provided to allow configuration control of the first insecticide spray in winter wheat - this changes the month */
CfgInt cfg_WW_InsecticideMonth("PEST_WWINSECTONEMONTH", CFG_CUSTOM, 5);
/** \brief Provided to allow configuration control of the proportion of farmers doing first conventional tillage in winter wheat - this changes between 0 and 1 */
CfgFloat cfg_WW_conv_tillage_prop1("TILLAGE_WWCONVONEPROP", CFG_CUSTOM, 1.0);
/** \brief Provided to allow configuration control of the proportion of farmers doing second conventional tillage in winter wheat - this changes between 0 and 1 */
CfgFloat cfg_WW_conv_tillage_prop2("TILLAGE_WWCONVTWOPROP", CFG_CUSTOM, 1.0);
/** \brief Provided to allow configuration control of the proportion of farmers doing first non-inversion tillage in winter wheat - this changes between 0 and 1 */
CfgFloat cfg_WW_NINV_tillage_prop1("TILLAGE_WWNINVONEPROP", CFG_CUSTOM, 1.0);
/** \brief Provided to allow configuration control of the proportion of farmers doing second non-inversion tillage in winter wheat - this changes between 0 and 1 */
CfgFloat cfg_WW_NINV_tillage_prop2("TILLAGE_WWNINVTWOPROP", CFG_CUSTOM, 1.0);
/** \brief Provided to allow configuration control of the proportion of farmers doing first insecticide spray in winter wheat - this changes between 0 and 1 */
CfgFloat cfg_WW_isecticide_prop1("PEST_WWINSECTONEPROP", CFG_CUSTOM, 1.0);
/** \brief Provided to allow configuration control of the proportion of farmers doing second insecticide spray in winter wheat - this changes between 0 and 1 */
CfgFloat cfg_WW_isecticide_prop2("PEST_WWINSECTTWOPROP", CFG_CUSTOM, 1.0);
/** \brief Provided to allow configuration control of the proportion of farmers doing third insecticide spray in winter wheat - this changes between 0 and 1 */
CfgFloat cfg_WW_isecticide_prop3("PEST_WWINSECTTHREEPROP", CFG_CUSTOM, 1.0);


/** \brief Provided to allow configuration control of the first insecticide spray in spring barley crops - this changes the day in the month */
CfgInt cfg_SB_InsecticideDay("PEST_SBINSECTONEDAY", CFG_CUSTOM, 15);
/** \brief Provided to allow configuration control of the first insecticide spray in spring barley crops - this changes the month */
CfgInt cfg_SB_InsecticideMonth("PEST_SBINSECTONEMONTH", CFG_CUSTOM, 5);
/** \brief Provided to allow configuration control of the first insecticide spray in OSR - this changes the day in the month */
CfgInt cfg_OSR_InsecticideDay("PEST_OSRINSECTONEDAY", CFG_CUSTOM, 30);
/** \brief Provided to allow configuration control of the first insecticide spray in OSR - this changes the month */
CfgInt cfg_OSR_InsecticideMonth("PEST_OSRINSECTONEMONTH", CFG_CUSTOM, 4);

/** \brief Provided to allow configuration control of the first insecticide spray in cabbage crops - this changes the day in the month */
CfgInt cfg_CAB_InsecticideDay("PEST_CABINSECTONEDAY", CFG_CUSTOM, 15);
/** \brief Provided to allow configuration control of the first insecticide spray in cabbage crops - this changes the month */
CfgInt cfg_CAB_InsecticideMonth("PEST_CABINSECTONEMONTH", CFG_CUSTOM, 5);

/** \brief Provided to allow configuration control of the first insecticide spray in tulip crops - this changes the day in the month */
CfgInt cfg_TU_InsecticideDay("PEST_TUINSECTONEDAY", CFG_CUSTOM, 15);
/** \brief Provided to allow configuration control of the first insecticide spray in tulip crops - this changes the month */
CfgInt cfg_TU_InsecticideMonth("PEST_TUINSECTONEMONTH", CFG_CUSTOM, 5);

/** \brief Provided to allow configuration control of the first insecticide spray in potatoes crops - this changes the day in the month */
CfgInt cfg_POT_InsecticideDay("PEST_POTINSECTONEDAY", CFG_CUSTOM, 15);
/** \brief Provided to allow configuration control of the first insecticide spray in potatoes crops - this changes the month */
CfgInt cfg_POT_InsecticideMonth("PEST_POTINSECTONEMONTH", CFG_CUSTOM, 5);

/** \brief Provided to allow configuration control of the first insecticide spray in beet crops - this changes the day in the month */
CfgInt cfg_BE_InsecticideDay("PEST_BEINSECTONEDAY", CFG_CUSTOM, 15);
/** \brief Provided to allow configuration control of the first insecticide spray in beet crops - this changes the month */
CfgInt cfg_BE_InsecticideMonth("PEST_BEINSECTONEMONTH", CFG_CUSTOM, 5);


//declared in elements.cpp
extern CfgFloat l_el_o_cut_height;
extern CfgFloat l_el_o_cut_green;
extern CfgFloat l_el_o_cut_total;


/**
\brief
Carry out a ploughing event in the autumn on a_field
*/
bool Farm::AutumnPlough( LE *a_field, double /*a_user*/, int a_days )
{

	// LE is a pointer to the field element
	// a_user is a pointer to the farm
	// a_days is the end of the operation time - today
	// if a_days <0 then the time to do it is passed
	// the line below reads 'plough if last day possible OR if not raining and pass a probability test
	if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		// this bit sets up the events to occur when ploughing occurs
		// The trace below is for debugging checks
		a_field->Trace( autumn_plough );
		// Record the event for this field, so other objects can find out it has happened
		a_field->SetLastTreatment( autumn_plough );
		// Apply mortality to the insects present, in this case 90%. This only affects the general insect model, any ALMaSS model species need to take their specific action.
		a_field->InsectMortality( 0.1 );
		// Reduce the vegetation, in this case to zero
		a_field->ZeroVeg();
		// If the field has a field margin, then do all this to the field margin too. In events that don't occur on an unsprayed margin, e.g. insecticide, then is part is skipped.
		int pref=a_field->GetUnsprayedMarginPolyRef();
		if (pref!=-1){
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um=g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment( autumn_plough );
			um->InsectMortality( 0.1 );
			um->ZeroVeg();
		}
		return true;      // completed
	}
	return false;       // not completed
}

/**
\brief
Carry out a stubble ploughing event on a_field. This is similar to normal plough but shallow (normally 6-8cm, is special cases up to 12-15cm).
Done as a part of after-harvest treatments (instead of stubble cultivation) 
*/
bool Farm::StubblePlough(LE *a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(stubble_plough);
		a_field->SetLastTreatment(stubble_plough);
		a_field->InsectMortality(0.1); // The same global mortality as for normal plough
		a_field->ZeroVeg();
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(stubble_plough);
			um->InsectMortality(0.1);
			um->ZeroVeg();
		}
		return true;      // completed
	}
	return false;       // not completed
}

/**
\brief
Carry out a stubble cultivation event on a_field. This is non-inversion type of cultivation which can be done instead of autumn plough (on a depth up to 40 cm even, if necessary)
*/
bool Farm::StubbleCultivatorHeavy(LE *a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(stubble_cultivator_heavy);
		a_field->SetLastTreatment(stubble_cultivator_heavy);
		a_field->InsectMortality(0.1); // The same global mortality as for normal plough
		a_field->ZeroVeg(); // Zero veg as with stubble harrowing
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(stubble_cultivator_heavy);
			um->InsectMortality(0.1);
			um->ZeroVeg();
		}
		return true;      // completed
	}
	return false;       // not completed
}

/**
\brief
Carry out a heavy cultivation event on a_field. This is non-inversion type of cultivation which can be done after fertilizers application on spring for a spring crop
*/
bool Farm::HeavyCultivatorAggregate(LE *a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(heavy_cultivator_aggregate);
		a_field->SetLastTreatment(heavy_cultivator_aggregate);
		a_field->InsectMortality(0.1); // The same global mortality as for normal plough
		a_field->ZeroVeg(); // Zero veg as with stubble harrowing
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(heavy_cultivator_aggregate);
			um->InsectMortality(0.1);
			um->ZeroVeg();
		}
		return true;      // completed
	}
	return false;       // not completed
}

/**
\brief
Nothing to to today on a_field
*/
bool Farm::SleepAllDay( LE *a_field, double /*a_user*/, int a_days)
{
	if (0 >= a_days) {
		a_field->Trace(sleep_all_day);
		a_field->SetLastTreatment(sleep_all_day);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(sleep_all_day);
		}
		return true;
	}
	return false;
}

/**
\brief
Carry out a harrow event in the autumn on a_field
*/
bool Farm::AutumnHarrow( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( autumn_harrow );
    a_field->SetLastTreatment( autumn_harrow );
   // Apply 90% mortality to the insects
    a_field->InsectMortality( 0.1 );
   // Reduce the vegetation to zero
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( autumn_harrow );
      um->InsectMortality( 0.1 );
      um->ZeroVeg();
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a roll event in the autumn on a_field
*/
bool Farm::AutumnRoll( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( autumn_roll );
    a_field->SetLastTreatment( autumn_roll );
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( autumn_roll );
      um->ZeroVeg();
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out preseeding cultivation on a_field (tilling set including cultivator and string roller to compact soil)
*/
bool Farm::PreseedingCultivator(LE *a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(preseeding_cultivator);
		a_field->SetLastTreatment(preseeding_cultivator);
		// Apply 90% mortality to the insects as for harrowing
		a_field->InsectMortality(0.1);
		// Reduce the vegetation to zero as for harrowing
		a_field->ZeroVeg();
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(preseeding_cultivator);
			um->InsectMortality(0.1);
			um->ZeroVeg();
		}
		return true;
	}
	return false;
}

/**
\brief
Carry out preseeding cultivation together with sow on a_field (tilling and sowing set including cultivator and string roller to compact soil)
*/
bool Farm::PreseedingCultivatorSow(LE *a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(preseeding_cultivator_sow);
		a_field->SetLastTreatment(preseeding_cultivator_sow);
		a_field->SetGrowthPhase(sow); // as with AutumnSow treatment
		a_field->SetLastSownVeg(a_field->GetVegType()); // as with AutumnSow treatment
		// Reduce the vegetation to zero
		a_field->ZeroVeg();
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(preseeding_cultivator_sow);
			um->SetGrowthPhase(sow);
			um->ZeroVeg();
			um->SetLastSownVeg(um->GetVegType());
		}
		return true;
	}
	return false;
}

/**
\brief
Carry out a sowing event in the autumn on a_field
*/
bool Farm::AutumnSow( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( autumn_sow );
    a_field->SetLastTreatment( autumn_sow );
    a_field->SetGrowthPhase( sow );
	a_field->SetLastSownVeg(a_field->GetVegType());
	// Reduce the vegetation to zero
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( autumn_sow );
      um->SetGrowthPhase( sow );
      um->ZeroVeg();
	  um->SetLastSownVeg(um->GetVegType());
	}
    return true;
  }
  return false;
}

/**
\brief
Carry out a ploughing event in the winter on a_field
*/
bool Farm::WinterPlough( LE *a_field, double /*a_user*/, int a_days )
{

    if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( winter_plough );
    a_field->SetLastTreatment( winter_plough );
    a_field->InsectMortality( 0.1 );
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( winter_plough );
      um->InsectMortality( 0.1 );
      um->ZeroVeg();
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a deep ploughing event on a_field
*/
bool Farm::DeepPlough( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( deep_ploughing );
    a_field->SetLastTreatment( deep_ploughing );
   // Apply 90% mortality to the insects
    a_field->InsectMortality( 0.1 );
   // Reduce the vegetation to zero
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( deep_ploughing );
      um->InsectMortality( 0.1 );
      um->ZeroVeg();
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a ploughing event in the spring on a_field
*/
bool Farm::SpringPlough( LE *a_field, double /*a_user*/, int a_days )
{

	if ((a_days <= 0) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( spring_plough );
    a_field->SetLastTreatment( spring_plough );
   // Apply 90% mortality to the insects
    a_field->InsectMortality( 0.1 );
   // Reduce the vegetation to zero
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( spring_plough );
      um->InsectMortality( 0.1);
      um->ZeroVeg();
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a harrow event in the spring on a_field
*/
bool Farm::SpringHarrow( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( spring_harrow );
    a_field->SetLastTreatment( spring_harrow );
    // 30% insect mortality
       a_field->InsectMortality( 0.7 );
    // remove all vegetation
       a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( spring_harrow );
      um->InsectMortality( 0.7 );
      um->ZeroVeg();
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a shallow harrow event on a_field, e.g., after grass cutting event
*/
bool Farm::ShallowHarrow(LE *a_field, double /*a_user*/, int a_days)
{

	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(shallow_harrow);
		a_field->SetLastTreatment(shallow_harrow);
		// 30% insect mortality
		a_field->InsectMortality(0.7);
		// remove vegetation partly
		a_field->ReduceVeg(0.8);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(shallow_harrow);
			um->InsectMortality(0.7);
			um->ReduceVeg(0.8);
		}
		return true;
	}
	return false;
}

/**
\brief
Carry out a roll event in the spring on a_field
*/
bool Farm::SpringRoll( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( spring_roll );
    a_field->SetLastTreatment( spring_roll );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( spring_roll );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a sowing event in the spring on a_field
*/
bool Farm::SpringSow( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( spring_sow );
    a_field->SetLastTreatment( spring_sow );
    a_field->SetGrowthPhase( sow );
	a_field->SetLastSownVeg(a_field->GetVegType());
   // Reduce the  vegetation to zero - should not strictly be necessary, but prevents any false starts in the crop growth.
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
   if (pref!=-1){
     // Must have an unsprayed margin so need to pass the information on to it
     LE* um=g_landscape_p->SupplyLEPointer(pref);
     um->SetLastTreatment( spring_sow );
     um->SetGrowthPhase( sow );
     um->ZeroVeg();
	 um->SetLastSownVeg(um->GetVegType());
   }
   return true;
  }
  return false;
}

/**
\brief
Carry out a sowing event with start fertilizer in the spring on a_field
*/
bool Farm::SpringSowWithFerti(LE *a_field, double /*a_user*/, int a_days)
{

	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(spring_sow_with_ferti);
		a_field->SetLastTreatment(spring_sow_with_ferti);
		a_field->SetGrowthPhase(sow);
		a_field->SetLastSownVeg(a_field->GetVegType());
		// Reduce the  vegetation to zero - should not strictly be necessary, but prevents any false starts in the crop growth.
		a_field->ZeroVeg();
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(spring_sow_with_ferti);
			um->SetGrowthPhase(sow);
			um->ZeroVeg();
			um->SetLastSownVeg(um->GetVegType());
		}
		return true;
	}
	return false;
}

/**
\brief
Apply NPKS fertilizer, on a_field owned by an arable farmer
*/
bool Farm::FP_NPKS( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fp_npks );
    a_field->SetLastTreatment( fp_npks );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
if (pref!=-1){
  // Must have an unsprayed margin so need to pass the information on to it
  LE* um=g_landscape_p->SupplyLEPointer(pref);
  um->SetLastTreatment(sleep_all_day);
  um->SetGrowthPhase( harvest1 );
  um->InsectMortality( 0.4 );
  um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
}

    return true;
  }
  return false;
}


/**
\brief
Apply NPK fertilizer, on a_field owned by an arable farmer
*/
bool Farm::FP_NPK( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fp_npk );
    a_field->SetLastTreatment( fp_npk );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment(fp_npk);
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Apply PK fertilizer, on a_field owned by an arable farmer
*/
bool Farm::FP_PK( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fp_pk );
    a_field->SetLastTreatment( fp_pk );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment(fp_pk);
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Apply liquid ammonia fertilizer to a_field owned by an arable farmer
*/
bool Farm::FP_LiquidNH3( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fp_liquidNH3 );
    a_field->SetLastTreatment( fp_liquidNH3 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment(fp_liquidNH3);
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Apply slurry to a_field owned by an arable farmer
*/
bool Farm::FP_Slurry( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || ((g_weather->GetTemp()>0)&&
                                 !g_weather->Raining() && DoIt(DO_IT_PROB)))
{
    a_field->Trace( fp_slurry );
    a_field->SetLastTreatment( fp_slurry );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment(fp_slurry);
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Apply Manganse Sulphate to a_field owned by an arable farmer
*/
bool Farm::FP_ManganeseSulphate( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fp_manganesesulphate );
    a_field->SetLastTreatment( fp_manganesesulphate );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fp_manganesesulphate );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Apply Ammonium Sulphate to a_field owned by an arable farmer
*/
bool Farm::FP_AmmoniumSulphate(LE *a_field, double /*a_user*/, int a_days)
{

	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(fp_ammoniumsulphate);
		a_field->SetLastTreatment(fp_ammoniumsulphate);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(fp_ammoniumsulphate);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Spread manure on a_field owned by an arable farmer
*/
bool Farm::FP_Manure( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || ((g_weather->GetTemp()>0)&&
                        !g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fp_manure );
    a_field->SetLastTreatment( fp_manure );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fp_manure );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Spread green manure on a_field owned by an arable farmer
*/
bool Farm::FP_GreenManure( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fp_greenmanure );
    a_field->SetLastTreatment( fp_greenmanure );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fp_greenmanure );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Spread sewege on a_field owned by an arable farmer
*/
bool Farm::FP_Sludge( LE *a_field, double /*a_user*/, int a_days )
{
  //
  if ( (0 >= a_days) || ((g_weather->GetTemp()>0)&&
                                 !g_weather->Raining() && DoIt(DO_IT_PROB)))
{
    a_field->Trace( fp_sludge );
    a_field->SetLastTreatment( fp_sludge );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fp_sludge );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
RSM (ammonium nitrate solution) applied on a_field owned by an arable farmer
*/
bool Farm::FP_RSM(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(fp_rsm);
		a_field->SetLastTreatment(fp_rsm);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(fp_rsm);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Calcium applied on a_field owned by an arable farmer
*/
bool Farm::FP_Calcium(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(fp_calcium);
		a_field->SetLastTreatment(fp_calcium);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(fp_calcium);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Apply NPKS fertilizer, on a_field owned by a stock farmer
*/
bool Farm::FA_NPKS(LE *a_field, double /*a_user*/, int a_days)
{

	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(fa_npks);
		a_field->SetLastTreatment(fa_npks);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(sleep_all_day);
			um->SetGrowthPhase(harvest1);
			um->InsectMortality(0.4);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}

		return true;
	}
	return false;
}

/**
\brief
Apply NPK fertilizer to a_field owned by an stock farmer
*/
bool Farm::FA_NPK( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fa_npk );
    a_field->SetLastTreatment( fa_npk );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fa_npk );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Apply PK fertilizer to a_field owned by an stock farmer
*/
bool Farm::FA_PK( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fa_pk );
    a_field->SetLastTreatment( fa_pk );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fa_pk );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Spready slurry on a_field owned by an stock farmer
*/

bool Farm::FA_Slurry( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || ((g_weather->GetTemp()>0)&&
                        !g_weather->Raining() && DoIt(DO_IT_PROB)))
{
    a_field->Trace( fa_slurry );
    a_field->SetLastTreatment( fa_slurry );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fa_slurry );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Apply ammonium sulphate to a_field owned by an stock farmer
*/
bool Farm::FA_AmmoniumSulphate( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fa_ammoniumsulphate );
    a_field->SetLastTreatment( fa_ammoniumsulphate );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fa_ammoniumsulphate );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Apply manganese sulphate to a_field owned by an stock farmer
*/
bool Farm::FA_ManganeseSulphate(LE *a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(fa_manganesesulphate);
		a_field->SetLastTreatment(fa_manganesesulphate);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(fa_manganesesulphate);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}


/**
\brief
Spread manure on a_field owned by an stock farmer
*/
bool Farm::FA_Manure( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || ((g_weather->GetTemp()>0)&&
                                 !g_weather->Raining() && DoIt(DO_IT_PROB)))
{
    a_field->Trace( fa_manure );
    a_field->SetLastTreatment( fa_manure );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fa_manure );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Spread green manure on a_field owned by an stock farmer
*/
bool Farm::FA_GreenManure( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( fa_greenmanure );
    a_field->SetLastTreatment( fa_greenmanure );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fa_greenmanure );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Spread sewege sludge on a_field owned by an stock farmer
*/
bool Farm::FA_Sludge( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || ((g_weather->GetTemp()>0)&&
                                 !g_weather->Raining() && DoIt(DO_IT_PROB)))
{
    a_field->Trace( fa_sludge );
    a_field->SetLastTreatment( fa_sludge );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( fa_sludge );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
RSM (ammonium nitrate solution) applied on a_field owned by a stock farmer
*/
bool Farm::FA_RSM(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(fa_rsm);
		a_field->SetLastTreatment(fa_rsm);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(fa_rsm);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Calcium applied on a_field owned by a stock farmer
*/
bool Farm::FA_Calcium(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(fa_calcium);
		a_field->SetLastTreatment(fa_calcium);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(fa_calcium);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}


/**
\brief
Apply herbicide to a_field
*/
bool Farm::HerbicideTreat( LE *a_field, double /*a_user*/, int a_days )
{

  if (0 >= a_days) // Must do this
  {
	if (((a_field->GetSignal() & LE_SIG_NO_HERBICIDE)==0 )) 
	{
      a_field->Trace( herbicide_treat );
      a_field->SetLastTreatment( herbicide_treat );
		if ( l_farm_herbicide_kills.value()) 
		{
        a_field->ReduceWeedBiomass( 0.05 );
      }
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
      a_field->SetHerbicideDelay( EL_HERBICIDE_DELAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, herbicide);

    }
    return true;
  }
  else if ((g_weather->GetWind()<4.5) && (!g_weather->Raining()) && DoIt(DO_IT_PROB)) 
  {
	  if ( !(a_field->GetSignal() & LE_SIG_NO_HERBICIDE) ) 
	  {
      a_field->Trace( herbicide_treat );
      a_field->SetLastTreatment( herbicide_treat );
		  if ( l_farm_herbicide_kills.value()) 
		  {
        a_field->ReduceWeedBiomass( 0.05 );
      }
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
      a_field->SetHerbicideDelay( EL_HERBICIDE_DELAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, herbicide);
	  }
  return true;
  }
  return false;
}


bool OptimisingFarm::HerbicideTreat( LE *a_field, double /*a_user*/, int a_days )
{

  if (0 >= a_days) // Must do this
  {
	if (((a_field->GetSignal() & LE_SIG_NO_HERBICIDE)==0 )) 
	{
		a_field->Trace( herbicide_treat );
		a_field->SetLastTreatment( herbicide_treat );
		if ( l_farm_herbicide_kills.value()) {
			a_field->ReduceWeedBiomass( 0.05 );
		}
		a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
		a_field->SetHerbicideDelay( EL_HERBICIDE_DELAYTIME );
		g_landscape_p->CheckForPesticideRecord(a_field, herbicide);

		
		if(!cfg_OptimiseBedriftsmodelCrops.value()){
			Field * pf =  dynamic_cast<Field*>(a_field); 
			pf->Add_no_herb_app();
		}

     }
	 return true;
  }
  else if ((g_weather->GetWind()<4.5) && (!g_weather->Raining()) && DoIt(DO_IT_PROB)) 
  {
	if ( !(a_field->GetSignal() & LE_SIG_NO_HERBICIDE) ) 
	{
		a_field->Trace( herbicide_treat );
		a_field->SetLastTreatment( herbicide_treat );
		if ( l_farm_herbicide_kills.value()) {
			a_field->ReduceWeedBiomass( 0.05 );
		}
		a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
		a_field->SetHerbicideDelay( EL_HERBICIDE_DELAYTIME );
		g_landscape_p->CheckForPesticideRecord(a_field, herbicide);

		if(!cfg_OptimiseBedriftsmodelCrops.value()){
			Field * pf =  dynamic_cast<Field*>(a_field); 
			pf->Add_no_herb_app();
		}
	}
	return true;
  }
  return false;
}



/**
\brief
Apply growth regulator to a_field
*/
bool Farm::GrowthRegulator( LE *a_field, double /*a_user*/, int a_days )
{

  if (0 >= a_days)
  {
    if ( (!g_weather->Raining()) && (g_weather->GetWind()<4.5) &&
        (!(a_field->GetSignal() & LE_SIG_NO_GROWTH_REG) )) {
      a_field->Trace( growth_regulator );
      a_field->SetLastTreatment( growth_regulator );
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  else if ( (g_weather->GetWind()<4.5) &&
           (!g_weather->Raining()) && DoIt(DO_IT_PROB)) {
    if ( ! (a_field->GetSignal() & LE_SIG_NO_GROWTH_REG) ) {
      a_field->Trace( growth_regulator );
      a_field->SetLastTreatment( growth_regulator );
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Apply fungicide to a_field
*/
bool Farm::FungicideTreat( LE *a_field, double /*a_user*/, int a_days )
{

  if (0 >= a_days)
  {
		if ( ( ! a_field->GetSignal() & LE_SIG_NO_FUNGICIDE )) 
		{
      a_field->Trace( fungicide_treat );
      a_field->SetLastTreatment( fungicide_treat );
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, fungicide);
    }
    return true;
  }
  else if ( (g_weather->GetWind()<4.5) &&
            (!g_weather->Raining()) && DoIt(DO_IT_PROB)) {
    if ( ! (a_field->GetSignal() & LE_SIG_NO_FUNGICIDE) ) {
      a_field->Trace( fungicide_treat );
      a_field->SetLastTreatment( fungicide_treat );
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, fungicide);
	}
    return true;
  }
  return false;
}


bool OptimisingFarm::FungicideTreat( LE *a_field, double /*a_user*/, int a_days )
{

  if (0 >= a_days)
  {
	if ( ( ! a_field->GetSignal() & LE_SIG_NO_FUNGICIDE )) {
		a_field->Trace( fungicide_treat );
		a_field->SetLastTreatment( fungicide_treat );
		a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
		g_landscape_p->CheckForPesticideRecord(a_field, fungicide);

		if(!cfg_OptimiseBedriftsmodelCrops.value()){
			Field * pf =  dynamic_cast<Field*>(a_field); 
			pf->Add_no_fi_app();
		}
    }
    return true;
  }
  else if ( (g_weather->GetWind()<4.5) && (!g_weather->Raining()) && DoIt(DO_IT_PROB)) {
    if ( ! (a_field->GetSignal() & LE_SIG_NO_FUNGICIDE) ) {
		a_field->Trace( fungicide_treat );
		a_field->SetLastTreatment( fungicide_treat );
		a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
		g_landscape_p->CheckForPesticideRecord(a_field, fungicide);

		if(!cfg_OptimiseBedriftsmodelCrops.value()){
			Field * pf =  dynamic_cast<Field*>(a_field); 
			pf->Add_no_fi_app();
		}
	}	
    return true;
  }
  return false;
}



/**
\brief
Apply insecticide to a_field
*/
bool Farm::InsecticideTreat( LE *a_field, double /*a_user*/, int a_days )
{

  if (0 >= a_days) 
  {
	  if (( ! (a_field->GetSignal() & LE_SIG_NO_INSECTICIDE) )) 
	  {
		  // **CJT** Turn this code on to use the pesticide engine with insecticides
		  //      g_pest->DailyQueueAdd( a_field, l_pest_insecticide_amount.value());
		  //
      a_field->Trace( insecticide_treat );
      a_field->SetLastTreatment( insecticide_treat );
		  if ( l_farm_insecticide_kills.value()) 
		  {
       a_field->Insecticide( 0.36 );
      }
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, insecticide);
	  }
    return true;
  }
  else if ( (g_weather->GetWind()<4.5) &&
            (!g_weather->Raining()) && DoIt(DO_IT_PROB)) {
    if ( ! (a_field->GetSignal() & LE_SIG_NO_INSECTICIDE )) {
// **CJT** Turn this code on to use the pesticide engine with insecticides
//      g_pest->DailyQueueAdd( a_field, l_pest_insecticide_amount.value());
//
      a_field->Trace( insecticide_treat );
      a_field->SetLastTreatment( insecticide_treat );
      if ( l_farm_insecticide_kills.value()) {
       a_field->Insecticide( 0.36 );
      }
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, insecticide);
	}
    return true;
  }
  return false;
}


bool OptimisingFarm::InsecticideTreat( LE *a_field, double /*a_user*/, int a_days )
{

  if (0 >= a_days) 
  {
	if (( ! (a_field->GetSignal() & LE_SIG_NO_INSECTICIDE) )) 
	{
		// **CJT** Turn this code on to use the pesticide engine with insecticides
		//      g_pest->DailyQueueAdd( a_field, l_pest_insecticide_amount.value());
		//
		a_field->Trace( insecticide_treat );
		a_field->SetLastTreatment( insecticide_treat );
		if ( l_farm_insecticide_kills.value()) {
			a_field->Insecticide( 0.36 );
		}
		a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
		g_landscape_p->CheckForPesticideRecord(a_field, insecticide);

		if(!cfg_OptimiseBedriftsmodelCrops.value()){
			Field * pf =  dynamic_cast<Field*>(a_field); 
			pf->Add_no_fi_app();
		}

    }	
    return true;
  }
  else if ( (g_weather->GetWind()<4.5) && (!g_weather->Raining()) && DoIt(DO_IT_PROB)) 
  {
    if ( ! (a_field->GetSignal() & LE_SIG_NO_INSECTICIDE )) {
		// **CJT** Turn this code on to use the pesticide engine with insecticides
		//      g_pest->DailyQueueAdd( a_field, l_pest_insecticide_amount.value());
		//
		a_field->Trace( insecticide_treat );
		a_field->SetLastTreatment( insecticide_treat );
		if ( l_farm_insecticide_kills.value()) {
			a_field->Insecticide( 0.36 );
		}
		a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
		g_landscape_p->CheckForPesticideRecord(a_field, insecticide);

		if(!cfg_OptimiseBedriftsmodelCrops.value()){
			Field * pf =  dynamic_cast<Field*>(a_field); 
			pf->Add_no_fi_app();
		}
    }
	
    return true;
  }
  return false;
}


/**
\brief
Apply test pesticide to a_field
*/
bool Farm::ProductApplication(LE *a_field, double /*a_user*/, int a_days, double a_applicationrate, PlantProtectionProducts a_ppp)
{
	// NOTE Differs from normal pesticide in that it will be done on the last
	// day if not managed before
	if (0 >= a_days) {
		a_field->Trace(product_treat);
		a_field->SetLastTreatment(product_treat);
		if (l_farm_insecticide_kills.value())
		{
			a_field->Insecticide(cfg_CustomInsecticideKillProp.value());
		}
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		g_landscape_p->CheckForPesticideRecord(a_field, testpesticide);	
		g_pest->DailyQueueAdd(a_field, a_applicationrate, a_ppp);
		return true;
	}
	else {
		if ((!g_weather->Raining()) && (g_weather->GetWind() < 4.5)) {
			a_field->Trace(product_treat);
			a_field->SetLastTreatment(product_treat);
			if (l_farm_insecticide_kills.value()) {
				a_field->Insecticide(cfg_CustomInsecticideKillProp.value());
			}
			a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
			g_landscape_p->CheckForPesticideRecord(a_field, testpesticide);
			g_pest->DailyQueueAdd(a_field, a_applicationrate, a_ppp);
			return true;
		}
	}
	return false;
}

/**
\brief
Apply molluscidie to a_field
*/
bool Farm::Molluscicide( LE *a_field, double /*a_user*/, int a_days )
{

       if (0 >= a_days) {
    if ( (!g_weather->Raining()) && (g_weather->GetWind()<4.5) &&
        (! (a_field->GetSignal() & LE_SIG_NO_MOLLUSC ))) {
      a_field->Trace( molluscicide );
      a_field->SetLastTreatment( molluscicide );
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, insecticide);
	}
    return true;
  }
  else if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    if ( ! (a_field->GetSignal() & LE_SIG_NO_MOLLUSC) ) {
      a_field->Trace( molluscicide );
      a_field->SetLastTreatment( molluscicide );
      a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  g_landscape_p->CheckForPesticideRecord(a_field, insecticide);
	}
    return true;
  }
  return false;
}

/**
\brief
Carry out a harrowing between crop rows on a_field
*/
bool Farm::RowCultivation( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) && (g_weather->GetRainPeriod(g_date->Date(),3)<0.1) )
  {
    // Too much rain, just give up and claim we did it.
    return true;
  }

  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),3)<0.1) && DoIt(DO_IT_PROB))
  ) {
    a_field->Trace( row_cultivation );
    a_field->SetLastTreatment( row_cultivation );
    a_field->ReduceWeedBiomass( 0.5 );
    a_field->InsectMortality( 0.25 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( row_cultivation );
      um->InsectMortality( 0.25 );
      um->ReduceWeedBiomass( 0.5 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a mechanical weeding on a_field
*/
bool Farm::Strigling( LE *a_field, double /*a_user*/, int a_days )
{

  // Force strigling if it has not been done already!!!  This happens regardless of weather as of 26/10/2005
  if ( (0 >= a_days) )// && (g_weather->GetRainPeriod(g_date->Date(),3)>0.1) )
  {
    a_field->Trace( strigling );
    a_field->SetLastTreatment( strigling );
    a_field->ReduceWeedBiomass( 0.05 );
    a_field->InsectMortality( 0.7 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    a_field->SetHerbicideDelay( EL_STRIGLING_DELAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( strigling );
      um->ReduceWeedBiomass( 0.05 );
      um->InsectMortality( 0.7 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
      um->SetHerbicideDelay( EL_STRIGLING_DELAYTIME );
    return true;
  }
  }
  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),3)<0.1) && DoIt(DO_IT_PROB))
  ) {
    a_field->Trace( strigling );
    a_field->SetLastTreatment( strigling );
    a_field->ReduceWeedBiomass( 0.05 );
    a_field->InsectMortality( 0.7 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    a_field->SetHerbicideDelay( EL_STRIGLING_DELAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( strigling );
      um->ReduceWeedBiomass( 0.05 );
      um->InsectMortality( 0.7 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
      um->SetHerbicideDelay( EL_STRIGLING_DELAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a mechanical weeding followed by sowing on a_field
*/
bool Farm::StriglingSow( LE *a_field, double /*a_user*/, int a_days )
{

  //2 days good weather afterwards
  if ( (0 >= a_days) && (g_weather->GetRainPeriod(g_date->Date(),3)<0.1) )
  {
    return true;
  }

  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),3)<0.1) && DoIt(DO_IT_PROB))
  ) {
    a_field->Trace( strigling_sow );
    a_field->SetLastTreatment( strigling_sow );
    a_field->ReduceWeedBiomass( 0.05 );
    a_field->InsectMortality( 0.7 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    a_field->SetHerbicideDelay( EL_STRIGLING_DELAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( strigling_sow );
      um->ReduceWeedBiomass( 0.05 );
      um->InsectMortality( 0.7 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
      um->SetHerbicideDelay( EL_STRIGLING_DELAYTIME );
    }
    return true;
  }
  return false;
}


/**
\brief
Carry out a mechanical weeding on a_field followed by hilling up (probably on potatoes)
*/
bool Farm::StriglingHill(LE *a_field, double /*a_user*/, int a_days)
{

	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(strigling_hill);
		a_field->SetLastTreatment(strigling_hill);
		a_field->InsectMortality(0.75);
		a_field->ReduceWeedBiomass(0.25);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(strigling_hill);
			um->ReduceWeedBiomass(0.25);
			um->InsectMortality(0.75);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}


/**
\brief
Do hilling up on a_field, probably of potatoes
*/
bool Farm::HillingUp( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( hilling_up );
    a_field->SetLastTreatment( hilling_up );
    a_field->InsectMortality( 0.75 );
    a_field->ReduceWeedBiomass( 0.25 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( hilling_up );
      um->ReduceWeedBiomass( 0.25 );
      um->InsectMortality( 0.75 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Do bed forming up on a_field, probably of carrots
*/
bool Farm::BedForming(LE *a_field, double /*a_user*/, int a_days)
{

	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(bed_forming);
		a_field->SetLastTreatment(bed_forming);
		a_field->InsectMortality(0.75);
		a_field->ZeroVeg();
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(bed_forming);
			um->ZeroVeg();
			um->InsectMortality(0.75);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Carry out a watering on a_field
*/
bool Farm::Water( LE *a_field, double /*a_user*/, int a_days )
{


  /* Turn on this code to avoid watering on heavy soils
  int soiltype = a_field->GetSoilType();
  if ( soiltype < 1 || soiltype > 4 )
    return true;
*/
  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( water );
    a_field->SetLastTreatment( water );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( water );
    }
    return true;
  }
  return false;
}

/**
\brief
Cut the crop on a_field and leave it lying (probably rape)
*/
bool Farm::Swathing( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( swathing );
    a_field->SetLastTreatment( swathing );
    a_field->InsectMortality( 0.5 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( swathing );
      um->InsectMortality( 0.5 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out a harvest on a_field
*/
bool Farm::Harvest(LE *a_field, double /*a_user*/, int a_days)
{
	//5 days good weather before
	if ((0 >= a_days) || ((g_weather->GetRainPeriod(g_date->Date(), 5) < 0.1) && DoIt(DO_IT_PROB))) {
		a_field->Trace(harvest);
		a_field->SetLastTreatment(harvest);
		a_field->SetGrowthPhase(harvest1);
		// Here we have to do a little skip to avoid too low insect populations after harvest, but a correct veg biomass
		a_field->InsectMortality(0.4);
		double insects = a_field->GetInsectPop();
		a_field->ResetDigestability();
		a_field->RecalculateBugsNStuff();
		a_field->SetInsectPop(insects);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		// Are we a cereal crop? If so we need to have some spilled grain
		double someseed = 0.0;
		double somemaize = 0.0;
		if (dynamic_cast<VegElement*>(a_field)->IsMatureCereal()) {
			someseed = m_OurManager->GetSpilledGrain();
			a_field->SetStubble(true);
		}
		else if (dynamic_cast<VegElement*>(a_field)->IsMaize()) {
			// Maize in stubble
			a_field->SetStubble(true);
			somemaize = m_OurManager->GetSpilledMaize();
		}
		a_field->SetBirdSeed( someseed );
		a_field->SetBirdMaize( somemaize ); 
		if (pref != -1){
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(harvest);
			um->SetGrowthPhase(harvest1);
			um->InsectMortality(0.4);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
			um->ResetDigestability();
			um->RecalculateBugsNStuff();
			um->SetBirdSeed( someseed ); 
			um->SetBirdMaize( somemaize );
			if ((somemaize > 0) || (someseed > 0)) um->SetStubble(true);
		}
		return true;
	}
	return false;
}


/**
\brief
Carry out a harvest on a_field
*/
bool Farm::HarvestLong(LE *a_field, double /*a_user*/, int a_days)
{

	//5 days good weather before
	if ((0 >= a_days) ||
		((g_weather->GetRainPeriod(g_date->Date(), 5)<0.1) && DoIt(DO_IT_PROB_LONG))
		) {
		a_field->Trace(harvest);
		a_field->SetLastTreatment(harvest);
		a_field->SetGrowthPhase(harvest1);
		// Here we have to do a little skip to avoid too low insect populations after harvest, but a correct veg biomass
		a_field->InsectMortality(0.4);
		double insects = a_field->GetInsectPop();
		a_field->ResetDigestability();
		a_field->RecalculateBugsNStuff();
		a_field->SetInsectPop(insects);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		double someseed = 0.0;  //No bird seed or maize forage on sugar beet (which is currently (March 2015) the only crop using HarvestLong.
		a_field->SetBirdSeed( someseed );
		a_field->SetBirdMaize( someseed );
		if (pref != -1){
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(harvest);
			um->SetGrowthPhase(harvest1);
			um->InsectMortality(0.4);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
			um->ResetDigestability();
			um->RecalculateBugsNStuff();
			um->SetBirdSeed( someseed );
			um->SetBirdMaize( someseed );
		}
		return true;
	}
	return false;
}

bool OptimisingFarm::Harvest(LE *a_field, double /*a_user*/, int a_days)
{

  //5 days good weather before
  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),5)<0.1) && DoIt(DO_IT_PROB))) 
  {

	//05.03.13 need to get the info on biomass
	if(!cfg_OptimiseBedriftsmodelCrops.value()){
		Field * pf =  dynamic_cast<Field*>(a_field); //ok????? - maybe should be VegElement instead of field!
		if(pf->GetVegType()!=tov_Potatoes && pf->GetVegType()!=tov_PotatoesIndustry){ //do not save biomass for potatoes! 
			double biomass_at_harvest = a_field->GetVegBiomass();
			pf->Set_biomass_at_harvest(biomass_at_harvest, 0);	//sets the biomass of a current crop; the current crop is at the index 0
			pf->Set_harvested(); //mark this crop as harvested
		}
	}
	//05.03.13 end 

    a_field->Trace( harvest );
    a_field->SetLastTreatment( harvest );
    a_field->SetGrowthPhase( harvest1 );
    // Here we have to do a little skip to avoid too low insect populations after harvest, but a correct veg biomass
    a_field->InsectMortality( 0.4 );
    double insects=a_field->GetInsectPop();
    a_field->RecalculateBugsNStuff();
    a_field->SetInsectPop(insects);
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	a_field->ResetDigestability();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment(harvest);
      um->SetGrowthPhase( harvest1 );
      um->InsectMortality( 0.4 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  um->ResetDigestability();
	  um->RecalculateBugsNStuff();
	}
    return true;
  }
  return false;
}



/**
\brief
Start a grazing event on a_field today
*/
bool Farm::CattleOut( LE *a_field, double /*a_user*/, int a_days )
{
	/**
	* This is the main initiate grazing method and as such is called by all grazed field management plans at the moment cattle are put out.
	*/
	if ((0 >= a_days) || DoIt( DO_IT_PROB ))
	{
		a_field->ToggleCattleGrazing();
		a_field->Trace( cattle_out );
		a_field->SetLastTreatment( cattle_out );
		// Reduce the vegetation because of grazing
		//double h=a_field->GetVegHeight();
		//double reduc = 1-(l_farm_cattle_veg_reduce.value()*((h-15)/15));
		//a_field->ReduceVeg_Extended( reduc );
		a_field->GrazeVegetation( l_farm_cattle_veg_reduce.value(), false );
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1)
		{
			// Must have an unsprayed margin so need to pass the information on to it
			// This happens if all arable fields are given unsprayed margins - they have no effect on grass unless it is sprayed with pesticides
			LE* um = g_landscape_p->SupplyLEPointer( pref );
			um->ToggleCattleGrazing();
			um->SetLastTreatment( cattle_out );
			um->GrazeVegetation( l_farm_cattle_veg_reduce.value( ), false );
		}
		return true;
	}
	return false;
}

/**
\brief
Start a extensive grazing event on a_field today
*/
bool Farm::CattleOutLowGrazing( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days)||  DoIt(DO_IT_PROB)) {
          a_field->ToggleCattleGrazing();
    a_field->Trace( cattle_out_low );
    a_field->SetLastTreatment( cattle_out_low );
	// Reduce the vegetation because of grazing
	//double h=a_field->GetVegHeight();
	//double reduc = 1-(l_farm_cattle_veg_reduce.value()*((h-15)/15));
	//a_field->ReduceVeg_Extended( reduc );
	a_field->GrazeVegetation( l_farm_cattle_veg_reduce2.value( ), false );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
         // This happens if all arable fields are given unsprayed margins - they have no effect on grass unless it is sprayed with pesticides
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->ToggleCattleGrazing();
      um->SetLastTreatment( cattle_out_low );
	  um->GrazeVegetation( l_farm_cattle_veg_reduce2.value( ), false );
    }
    return true;
  }
  return false;
}

/**
\brief
Generate a 'cattle_out' event for every day the cattle are on a_field
*/
bool Farm::CattleIsOut(LE *a_field, double /*a_user*/, int a_days, int a_max)
{
	/**
	* This is the main grazing method and as such is called by all grazed field management plans each day the cattle are out.
	*/
	double biomass = a_field->GetVegBiomass();
	if (biomass > 100.0)
	{
		a_field->SetLastTreatment(cattle_out);
		a_field->Trace(cattle_out);
		// Reduce the vegetation because of grazing
		//double h=a_field->GetVegHeight();
		//double reduc = 1-(l_farm_cattle_veg_reduce.value()*((h-15)/15));
		//a_field->ReduceVeg_Extended( reduc );
		a_field->GrazeVegetation( l_farm_cattle_veg_reduce.value( ), false );
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1)
		{
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(cattle_out);
			um->GrazeVegetation( l_farm_cattle_veg_reduce.value( ), false );
		}
	}
	int d1 = g_date->DayInYear(10, 9);
	if (d1 > a_max)	d1 = a_max;
	if ((((g_date->DayInYear() > d1) && (10 >= a_days)) && DoIt(10)) || (0 >= a_days))
	{
		a_field->ToggleCattleGrazing();
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1){
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->ToggleCattleGrazing();
		}
		return true;
	}
	return false;
}

/**
\brief
Generate a 'cattle_out_low' event for every day the cattle are on a_field
*/
bool Farm::CattleIsOutLow(LE *a_field, double /*a_user*/, int a_days, int a_max)
{

	// Generate a 'cattle_in_out' event for every day the cattle is on the
	// field.
	double biomass = a_field->GetVegBiomass();
	if (biomass > 100.0)
	{
		a_field->SetLastTreatment(cattle_out_low);
		a_field->Trace(cattle_out_low);
		// Reduce the vegetation because of grazing
		//double h=a_field->GetVegHeight();
		//double reduc = 1-(l_farm_cattle_veg_reduce2.value()*((h-15)/15));
		//a_field->ReduceVeg_Extended( reduc );
		a_field->GrazeVegetation( l_farm_cattle_veg_reduce2.value( ), false );
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1)
		{
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(cattle_out_low);
			um->GrazeVegetation( l_farm_cattle_veg_reduce2.value( ), false );
		}
	}
	int d1 = g_date->DayInYear(10, 9);
	if (d1 > a_max) d1 = a_max;
	if ((((g_date->DayInYear() > d1) && (10 >= a_days)) && DoIt(10)) || (0 >= a_days))
	{
		a_field->ToggleCattleGrazing();
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1){
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->ToggleCattleGrazing();
		}
		return true;
	}
	return false;
}

/**
\brief
Generate a 'pigs_out' event for every day the cattle are on a_field
*/
bool Farm::PigsOut( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days)||  DoIt(DO_IT_PROB))
 {
    a_field->TogglePigGrazing();
    a_field->Trace( pigs_out );
    a_field->SetLastTreatment( pigs_out );
    // Reduce the vegetation because of grazing
    a_field->ReduceVeg_Extended( l_farm_pig_veg_reduce.value());
                                 // make this a function of grazing pressure
                                 //and field size - perhaps in a later life
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( pigs_out );
      um->ReduceVeg_Extended( l_farm_pig_veg_reduce.value() );
    }
    return true;
 }
  return false;
}

/**
\brief
Start a pig grazing event on a_field today - no exceptions
*/
bool Farm::PigsAreOutForced( LE *a_field, double /*a_user*/, int /*a_days*/)
{
  a_field->SetLastTreatment( pigs_out );
  a_field->Trace( pigs_out );
  // Reduce the vegetation because of grazing
  a_field->ReduceVeg_Extended( l_farm_pig_veg_reduce.value() );
  // make this a function of grazing pressure
  //and field size - perhaps in a later life
  int pref=a_field->GetUnsprayedMarginPolyRef();
  if (pref!=-1){
    // Must have an unsprayed margin so need to pass the information on to it
    LE* um=g_landscape_p->SupplyLEPointer(pref);
    um->SetLastTreatment( pigs_out );
    um->ReduceVeg_Extended( l_farm_pig_veg_reduce.value() );
  }
  return false;
}

/**
\brief
Start a pig grazing event on a_field today or soon
*/
bool Farm::PigsAreOut( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days)||  DoIt(50/a_days)) {
    a_field->TogglePigGrazing();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->TogglePigGrazing();
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out straw chopping on a_field
*/
bool Farm::StrawChopping( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),5)<0.1) && DoIt(DO_IT_PROB))
  ) {
    a_field->Trace( straw_chopping );
    a_field->SetLastTreatment( straw_chopping );
    a_field->InsectMortality( 0.4 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( straw_chopping );
      um->InsectMortality( 0.4 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out hay turning on a_field
*/
bool Farm::HayTurning( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),5)<0.1) && DoIt(DO_IT_PROB))
  ) {
    a_field->Trace( hay_turning );
    a_field->SetLastTreatment( hay_turning );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( hay_turning );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out hay bailing on a_field
*/
bool Farm::HayBailing( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),5)<0.1) && DoIt(DO_IT_PROB))
  ) {
    a_field->Trace( hay_bailing );
    a_field->SetLastTreatment( hay_bailing );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( hay_bailing );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out stubble harrowing on a_field
*/
bool Farm::StubbleHarrowing( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) ||
       ((g_weather->GetRainPeriod(g_date->Date(),3)<0.1) && DoIt(DO_IT_PROB))
  ) {
    a_field->Trace( stubble_harrowing );
    a_field->SetLastTreatment( stubble_harrowing );
    a_field->InsectMortality( 0.25 );
    a_field->ZeroVeg();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( stubble_harrowing );
      um->ZeroVeg();
      um->InsectMortality( 0.25 );
    }
    return true;
  }
  return false;
}

/**
\brief
Burn stubble on a_field
*/
bool Farm::BurnStrawStubble( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) && (g_weather->GetRainPeriod(g_date->Date(),3)>0.1))
  {
    return true;
  }
  if ( (0 >= a_days) || ((g_weather->GetRainPeriod(g_date->Date(),3)<0.1)
          && DoIt(DO_IT_PROB)))
  {
    a_field->Trace( burn_straw_stubble );
    a_field->SetLastTreatment( burn_straw_stubble );
    a_field->InsectMortality( 0.4 );
    a_field->ReduceVeg( 0.2 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( burn_straw_stubble );
      um->ReduceVeg( 0.2 );
      um->InsectMortality( 0.4 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out hay cutting on a_field
*/
bool Farm::CutToHay( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || ((g_weather->GetRainPeriod(g_date->Date(),5)<0.1)
          && DoIt(DO_IT_PROB)))
  {
    a_field->Trace( cut_to_hay );
    a_field->SetLastTreatment( cut_to_hay );
	a_field->SetGrowthPhase(harvest1);
	a_field->InsectMortality(0.4);
    a_field->ReduceVeg_Extended( 0.2 );
	a_field->SetVegHeight(10);
	//a_field->ResetDigestability();
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( cut_to_hay );
	  um->SetGrowthPhase(harvest1);
	  um->InsectMortality(0.4);
      um->ReduceVeg_Extended( 0.2 );
      um->SetVegHeight( 10);
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Carry out weed topping on a_field
*/
bool Farm::CutWeeds( LE *a_field, double /*a_user*/, int a_days )
{

  if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB)))
  {
    a_field->Trace( cut_weeds );
    a_field->SetLastTreatment( cut_weeds );
    a_field->ReduceVeg( 0.8 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( cut_weeds );
      um->ReduceVeg( 0.8 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
    }
    return true;
  }
  return false;
}

/**
\brief
Cut vegetation for silage on a_field
*/
bool Farm::CutToSilage( LE *a_field, double /*a_user*/, int a_days )
{

    if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( cut_to_silage );
    a_field->SetLastTreatment( cut_to_silage );
    a_field->ReduceVeg_Extended( 0.2 );
    a_field->InsectMortality( 0.4 );
    a_field->SetVegHeight( 10 );
    a_field->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	a_field->SetGrowthPhase(harvest1);
	//a_field->ResetDigestability();
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( cut_to_silage );
      um->ReduceVeg_Extended( 0.2 );
      um->InsectMortality( 0.4 );
      um->SetVegHeight( 10 );
      um->SetTramlinesDecay( EL_TRAMLINE_DECAYTIME );
	  um->ResetDigestability();
	  um->SetGrowthPhase(harvest1);
	}
    return true;
  }
  return false;
}

/**
\brief
Cut vegetation on orchard crop. //based on cut to silage - values from cutting function of orchard
*/
bool Farm::CutOrch( LE *a_field, double /* a_user */, int a_days )
{

    if ( (0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
    a_field->Trace( mow ); //? tried to do=mow...
    a_field->SetLastTreatment( mow );
    a_field->SetMownDecay( 12 ); // 12 days of not suitable
	//ReduceVeg_Extended( 0.2 );
    //a_field->InsectMortality( 0.4 );
	a_field->SetGrowthPhase( harvest1 );
	//m_DateCut = a_today; ?? what to substitute it with?

    a_field->SetVegParameters( l_el_o_cut_height.value(), l_el_o_cut_total.value(), l_el_o_cut_green.value(), 0 ); //is 0 ok for weed biomass
  
    int pref=a_field->GetUnsprayedMarginPolyRef();
    if (pref!=-1){
      // Must have an unsprayed margin so need to pass the information on to it
      LE* um=g_landscape_p->SupplyLEPointer(pref);
      um->SetLastTreatment( mow );
      um->ReduceVeg_Extended( 0.2 ); //are these values ok?
      um->InsectMortality( 0.4 );
      um->SetVegParameters( l_el_o_cut_height.value(), l_el_o_cut_total.value(), l_el_o_cut_green.value(), 0 );
    }
    return true;
  }
  return false;
}


/**
\brief
Special pesticide trial functionality
*/
// Pesticide Trial Farm Function Code
bool Farm::ProductApplication_DateLimited(LE *a_field, double /*a_user*/, int /*a_days*/, double a_applicationrate, PlantProtectionProducts a_ppp)
{
	/** This methods tests for date limitations */
	int year = g_landscape_p->SupplyYearNumber();
	if (year < cfg_productapplicstartyear.value()) return false;
	if (year > cfg_productapplicendyear.value()) return false;
	a_field->Trace(trial_insecticidetreat); // Debug function only
	a_field->SetLastTreatment(trial_insecticidetreat);
	a_field->InsectMortality(cfg_CustomInsecticideKillProp.value());  // Change this manually if it is really a herbicide
	a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
	a_field->SetSprayedToday(true);
	g_pest->DailyQueueAdd(a_field, a_applicationrate, a_ppp);
	return true;
}

/**
\brief
Biocide applied on a_field
*/
bool Farm::Biocide(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(biocide);
		a_field->SetLastTreatment(biocide);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(biocide);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Flower cutting applied on a_field
*/
bool Farm::FlowerCutting(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(flower_cutting);
		a_field->ReduceVeg_Extended(0.8);
		a_field->InsectMortality(0.9);
		a_field->SetVegHeight(35);
		a_field->SetLastTreatment(flower_cutting);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->ReduceVeg_Extended(0.8);
			um->InsectMortality(0.9);
			um->SetVegHeight(30);
			um->SetLastTreatment(flower_cutting);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Carry out a bulb harvest on a_field
*/
bool Farm::BulbHarvest(LE *a_field, double /*a_user*/, int a_days)
{
	//5 days good weather before
	if ((0 >= a_days) || ((g_weather->GetRainPeriod(g_date->Date(), 5) < 0.1) && DoIt(DO_IT_PROB))) {
		a_field->Trace(bulb_harvest);
		a_field->SetLastTreatment(bulb_harvest);
		a_field->SetGrowthPhase(harvest1);
		// Here we have to do a little skip to avoid too low insect populations after harvest, but a correct veg biomass
		a_field->InsectMortality(0.3);
		// remove all vegetation
		a_field->ZeroVeg();
		double insects = a_field->GetInsectPop();
		a_field->ResetDigestability();
		a_field->RecalculateBugsNStuff();
		a_field->SetInsectPop(insects);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(bulb_harvest);
			um->SetGrowthPhase(harvest1);
			um->InsectMortality(0.3);
			um->ZeroVeg();
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
			um->ResetDigestability();
			um->RecalculateBugsNStuff();
		}
		return true;
	}
	return false;
}

/**
\brief
Straw covering applied on a_field
*/
bool Farm::StrawCovering(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(straw_covering);
		a_field->SetLastTreatment(straw_covering);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(straw_covering);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}

/**
\brief
Straw covering applied on a_field
*/
bool Farm::StrawRemoval(LE * a_field, double /*a_user*/, int a_days)
{
	if ((0 >= a_days) || (!g_weather->Raining() && DoIt(DO_IT_PROB))) {
		a_field->Trace(straw_removal);
		a_field->SetLastTreatment(straw_removal);
		a_field->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		int pref = a_field->GetUnsprayedMarginPolyRef();
		if (pref != -1) {
			// Must have an unsprayed margin so need to pass the information on to it
			LE* um = g_landscape_p->SupplyLEPointer(pref);
			um->SetLastTreatment(straw_removal);
			um->SetTramlinesDecay(EL_TRAMLINE_DECAYTIME);
		}
		return true;
	}
	return false;
}
