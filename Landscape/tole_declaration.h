//
// tole_declaration.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

//---------------------------------------------------------------------------
#ifndef tole_declarationH
#define tole_declarationH
//----------------------------------------------------------------------------

// numbers in comments are the numbers expected in the raw data file direct
// from ArcView

typedef enum {
			tole_Hedges=0,  //130
			tole_RoadsideVerge,  //13
			tole_Railway,  //118
			tole_FieldBoundary,  //160
			tole_Marsh,  //95
			tole_Scrub,  //70
			tole_Field,  //20&30
			tole_PermPastureLowYield,  //26
			tole_PermPastureTussocky,  //27
			tole_PermanentSetaside,  //33
			tole_PermPasture,  //35
			tole_NaturalGrassDry,  //110
			tole_RiversidePlants,  //98
			tole_PitDisused,  //75
			tole_RiversideTrees,  //97
			tole_DeciduousForest,  //40
			tole_MixedForest,  //60
			tole_ConiferousForest,  //50
			tole_YoungForest,  //55
			tole_StoneWall,  //15
			tole_Fence, //225
			tole_Garden,  //11//tole20
			tole_Track,  //123
			tole_SmallRoad,  //122
			tole_LargeRoad,  //121
			tole_Building,  //5
			tole_ActivePit,  //115
			tole_Freshwater,  //90
			tole_River,  //96
			tole_Saltwater,  //80
			tole_Coast,  //100//tole30
			tole_HedgeBank,  //140
			tole_BeetleBank,  //141
			tole_Heath,  //94
			tole_Orchard,  //56
			tole_UnsprayedFieldMargin,  //31
			tole_OrchardBand,  //57
			tole_MownGrass,  //58
			tole_BareRock,  //69
			tole_AmenityGrass,  //12
			tole_Parkland,  //14
			tole_UrbanNoVeg,  //8
			tole_UrbanPark,  //17
			tole_BuiltUpWithParkland,  //16
			tole_SandDune,  //101
			tole_Copse,  //41
			tole_RoadsideSlope,  //201
			tole_MetalledPath,  //202
			tole_Carpark,  //203
			tole_Churchyard,  //204
			tole_NaturalGrassWet,  //205
			tole_Saltmarsh,  //206
			tole_Stream,  //207
			tole_HeritageSite,  //208
			tole_UnknownGrass,
			tole_Wasteland, // 209
			tole_IndividualTree,  //
			tole_WoodyEnergyCrop,// 216
			tole_PlantNursery, // 
			tole_Pylon,
			tole_WindTurbine,
			tole_WoodlandMargin,
			tole_Vildtager,
			tole_PermPastureTussockyWet, // 218
			tole_Pond, // 219
			tole_FishFarm, // 220
			tole_UrbanVeg, // Urban vegetated but not garden or park  9
			tole_RiverBed, //221
			tole_DrainageDitch, //222
			tole_Canal, //223
			tole_RefuseSite, // 224
			tole_WaterBufferZone, //226
			tole_Missing, // 2112 will cause this polygon to be removed and its pixels replaced.
			tole_Chameleon, // Special behaviour polygon - replaced when read in, but needs to be here for landscape manipulations
			tole_Foobar // 999   !! type unknown - should not happen
			  // Don't use this one when communicating with the landscape
              // model, it will trip an error in most cases!
              // --FN-- 24/11-2000.
             }
TTypesOfLandscapeElement;

// USE THE CODE BELOW TO TEST FOR MISSING CASES IN SWITCH STATEMENTS. PASTE BEFORE DEFAULT TO ID THOSE WHICH DO NOT GIVE AN
// ERROR AS ALREADY BEING PRESENT - REMEMBER TO ADD ANY NEW tole TYPES TO THIS LIST!

/*
case tole_Hedges:  //130
case tole_RoadsideVerge:  //13
case tole_Railway:  //118
case tole_FieldBoundary:  //160
case tole_Marsh:  //95
case tole_Scrub:  //70
case tole_Field:  //20&30
case tole_PermPastureLowYield:  //26
case tole_PermPastureTussocky:  //27
case tole_PermanentSetaside:  //33
case tole_PermPasture:  //35
case tole_NaturalGrassDry:  //110
case tole_RiversidePlants:  //98
case tole_PitDisused:  //75
case tole_RiversideTrees:  //97
case tole_DeciduousForest:  //40
case tole_MixedForest:  //60
case tole_ConiferousForest:  //50
case tole_YoungForest:  //55
case tole_StoneWall:  //15
case tole_Garden:  //11//tole20
case tole_Track:  //123
case tole_SmallRoad:  //122
case tole_LargeRoad:  //121
case tole_Building:  //5
case tole_ActivePit:  //115
case tole_Freshwater:  //90
case tole_River:  //96
case tole_Saltwater:  //80
case tole_Coast:  //100//tole30
case tole_HedgeBank:  //140
case tole_BeetleBank:  //141
case tole_Heath:  //94
case tole_Orchard:  //56
case tole_UnsprayedFieldMargin:  //31
case tole_OrchardBand:  //57
case tole_MownGrass:  //58
case tole_BareRock:  //69
case tole_AmenityGrass:  //12
case tole_Parkland:  //14
case tole_UrbanNoVeg:  //8
case tole_UrbanPark:  //17
case tole_BuiltUpWithParkland:  //16
case tole_SandDune:  //101
case tole_Copse:  //41
case tole_IndividualTree:  //42
case tole_RoadsideSlope:  //201
case tole_MetalledPath:  //202
case tole_Carpark:  //203
case tole_Churchyard:  //204
case tole_NaturalGrassWet:  //205
case tole_Saltmarsh:  //206
case tole_Stream:  //207
case tole_HeritageSite:  //208
case tole_WoodyEnergyCrop:// 59
case tole_PlantNursery: // 214
case tole_UnknownGrass:
case tole_Wasteland:
case tole_IndividualTree:
case tole_WoodyEnergyCrop:
case tole_PlantNursery:
case tole_Pylon:
case tole_WindTurbine:
case tole_WoodlandMargin:
case tole_Vildtager:
case tole_PermPastureTussockyWet: // 218
case tole_Pond: // 219
case tole_FishFarm
*/

#endif

