//
// configurator.h
//
/*
*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef CONFIGURATOR_H
#define CONFIGURATOR_H

// If defined will compile many methods as inlines for speed.
//#define CONF_INLINES

#include <stdio.h>
#ifdef __UNIX
#undef max
#endif
#include <string>
#include <map>
#include <vector>
//#include <algorithm.h>

// This one *has* to be a define!
// A global configurator variable will not do.
#define CFG_MAX_LINE_LENGTH 512

using namespace std;


typedef enum {
  CFG_NONE,
  CFG_INT,
  CFG_FLOAT,
  CFG_BOOL,
  CFG_STRING
} CfgType;

typedef enum {
  CFG_CUSTOM,
  CFG_PUBLIC,
  CFG_PRIVATE
} CfgSecureLevel;


/** \brief
* Base class for a configurator entry
*/
class CfgBase {
protected:
	string         m_key;
  CfgSecureLevel m_level;
  bool m_rangetest;

 public:
	 CfgBase(const char* a_key, CfgSecureLevel a_level);
	 virtual ~CfgBase(void);

  const string    getkey  ( void ) { return m_key; }
  virtual CfgType gettype ( void ) { return CFG_NONE; }
  CfgSecureLevel  getlevel( void ) { return m_level; }
};

/** \brief
* Integer configurator entry class
*/
class CfgInt : public CfgBase
{
protected:
	int m_int;
	int m_max;
	int m_min;

 public:
  CfgInt(const char* a_key, CfgSecureLevel a_level, int a_defval );
  CfgInt(const char* a_key, CfgSecureLevel a_level, int a_defval,  int a_min, int a_max);

  int value( void ) { return m_int; }
  virtual void set(int a_newval);
  virtual CfgType gettype( void ) { return CFG_INT; }
};

/** \brief
* Double configurator entry class
*/
class CfgFloat : public CfgBase
{
protected:
	double m_float;
	double m_min;
	double m_max;

public:
	CfgFloat(const char* a_key, CfgSecureLevel a_level, double a_defval);
	/** \brief Constructor with max min checking enabled*/
	CfgFloat(const char* a_key, CfgSecureLevel a_level, double a_defval, double a_min, double a_max);

	double           value(void) { return m_float; }
	virtual void set(double a_newval);
	virtual CfgType gettype(void) { return CFG_FLOAT; }
};


/** \brief
* Bool configurator entry class
*/
class CfgBool : public CfgBase
{
protected:
	bool            m_bool;

 public:
  CfgBool( const char* a_key, CfgSecureLevel a_level, bool a_defval );

  bool            value( void ) { return m_bool; }
  void            set( bool a_newval ) { m_bool = a_newval; }
  virtual CfgType gettype( void ) { return CFG_BOOL; }
};


/** \brief
* String configurator entry class
*/
class CfgStr : public CfgBase
{
protected:
	string          m_string;

 public:
  CfgStr( const char* a_key, CfgSecureLevel a_level, const char* a_defval );

  const char*     value( void ) { return m_string.c_str(); }
  void            set( char* a_newval ) { m_string = a_newval; }
  virtual CfgType gettype( void ) { return CFG_STRING; }
};


/** \brief
* A class to provide standard parameter entry facilities
*/
class Configurator
{
protected:
	map<string, unsigned int> CfgI;
  vector<CfgBase*>         CfgVals;

  // Private methods and fields for the configuration file parser.
  unsigned int m_lineno;
  void ParseCfgLine( char* a_line );
  void SetCfgInt   ( char* a_key, char* a_val );
  void SetCfgFloat ( char* a_key, char* a_val );
  void SetCfgBool  ( char* a_key, char* a_val );
  void SetCfgStr   ( char* a_key, char* a_val );
  bool LastDoubleQuote( char* a_rest_of_line );

  // Discreet security check. Returns true if we terminate line
  // parsing early.
  bool SetCfgGatekeeper( const char* a_method,
			 const char* a_key,
			 CfgSecureLevel a_level );

  // Helper methods.
  void  ShowIdType( unsigned int a_i );
  char* ExtractString( char* a_line );
  void  DumpSymbols( const char *a_dumpfile,
		     CfgSecureLevel a_level );

 public:
  // Write all configuration values with a security level at or below
  // a_level to a_dumpfile in alphabetical order. a_level must
  // be CFG_CUSTOM (user settable) or CFG_PUBLIC (user can see
  // predefined value and the very existence of this parameter).
  void DumpPublicSymbols( const char *a_dumpfile,
			  CfgSecureLevel a_level );

  // Dump *all* configuration values, including the private ones,
  // to a_dumpfile and then calls exit(), noting the event in the error
  // file!! For debugging purposes only. The call to exit() is there in
  // order to try and prevent any use of this call from making it into
  // production code.
  void DumpAllSymbolsAndExit( const char *a_dumpfile );

  // Reads and parses a_cfgfile for configuration values. Unknown
  // or CFG_PRIVATE keys are silently ignored thus preventing snooping
  // on possibly interesting key values. Returns true if reading and
  // parsing was error free.
  bool ReadSymbols( const char *a_cfgfile );

  // You should never use these ones directly, they are
  // called automagically by the CfgBase class con/destructor
  // when needed.
   Configurator( void );
  ~Configurator( void );

  // Please don't use this unless you know what you are doing.
  // Need to be public as it is used by the CfgBase class and friends.
  bool Register( CfgBase* a_cfgval, const char* a_key );
};

extern class Configurator *g_cfg;

#endif // CONFIGURATOR_H



