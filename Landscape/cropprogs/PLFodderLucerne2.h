/**
\file
\brief
<B>PLFodderLucerne2.h This file contains the headers for the FodderLucerne2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of January 2018 \n
All rights reserved. \n
\n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PLFodderLucerne2.h
//


#ifndef PLFODDERLUCERNE2_H
#define PLFODDERLUCERNE2_H

#define PLFODDERLUCERNE2_BASE 21200

/** Below is the list of things that a farmer can do if he is growing fodder lucerne in the first year, at least following this basic plan.
So all we have to do is figure out when to do the different things.
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon.
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_fl2_start = 1, // Compulsory, must always be 1 (one).
	pl_fl2_sleep_all_day = PLFODDERLUCERNE2_BASE,
	pl_fl2_spring_harrow,
	pl_fl2_ferti_p0,
	pl_fl2_ferti_s0,
	pl_fl2_herbicide1,
	pl_fl2_cut_to_silage1A,
	pl_fl2_harrow1A,
	pl_fl2_ferti_p1A,
	pl_fl2_ferti_s1A,
	pl_fl2_cut_to_silage2A,
	pl_fl2_harrow2A,
	pl_fl2_cut_to_silage3A,
	pl_fl2_cut_to_silage1B,
	pl_fl2_harrow1B,
	pl_fl2_ferti_p1B,
	pl_fl2_ferti_s1B,
	pl_fl2_cut_to_silage2B,
	pl_fl2_harrow2B,
	pl_fl2_cut_to_silage3B,
	pl_fl2_harrow3B,
	pl_fl2_cut_to_silage4B,
} PLFodderLucerne2ToDo;


/**
\brief
PLFodderLucerne2 class
\n
*/
/**
See PLFodderLucerne2.h::PLFodderLucerne2ToDo for a complete list of all possible events triggered codes by the fodder lucerne1 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLFodderLucerne2 : public Crop
{
public:
	virtual bool  Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev);
	PLFodderLucerne2()
	{
		// When we start it off, the first possible date for a farm operation is 31th March
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date = g_date->DayInYear(31, 3);
	}
};

#endif // PLFODDERLUCERNE2_H

