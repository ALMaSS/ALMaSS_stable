//
// dummy.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef DUMMY_H
#define DUMMY_H

// A particular crop probably doesn't need all of these, but it may need
// others instead in places.

// <cropname>_BASE is the first event number to be dumped into the
// debugging log from this crop. *Must* be unique among all crops.
// I suggest steps of 100 between crops.

#define DUMMY_BASE 100

typedef enum {
  start = 1, // Compulsory, must always be 1 (one).
    sleep_all_day = DUMMY_BASE,
    autumn_plough,
    autumn_harrow,
    autumn_roll,
    autumn_sow,
    winter_plough,
    grubning,      // Erm...? ;-)
    spring_plough,
    spring_harrow,
    spring_roll,
    spring_sow,
    fertilizer_plant1,
    fertilizer_plant2,
    fertilizer_plant3,
    fertilizer_plant4,
    fertilizer_plant5,
    fertilizer_animal1,
    fertilizer_animal2,
    fertilizer_animal3,
    fertilizer_animal4,
    staining,
    herbacide,
    growth_enhancer,
    fungicide,
    insecticide,
    snailicide,
    rowcol_cleaning, // My dictionary says this is hoeing too.
    grooming, // Well...
    flaming,
    hoeing,
    water,
    swathing,
    harvest,
    cattle_out,
    cattle_in,
    grass_to_hay,
    grass_to_ensilage,
    cut_to_straw,
    collect_straw,
    compress_straw,
    burning,
    stubble_hoeing
} DummyToDo;


class DummyCrop: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
};

#endif // DUMMY_H
