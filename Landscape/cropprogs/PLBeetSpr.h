/**
\file
\brief
<B>PLBeetSpr.h This file contains the headers for the BeetSpr class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLBeetSpr.h
//


#ifndef PLBEETSPR_H
#define PLBEETSPR_H

#define PLBEETSPR_BASE 21600
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_BES_SPRING_FERTI		a_field->m_user[1]
#define PL_BES_WATER_DATE		a_field->m_user[2]
#define PL_BES_HERBI_DATE			a_field->m_user[3]
#define PL_BES_HERBI1			a_field->m_user[4]
#define PL_BES_HERBI3			a_field->m_user[5]


/** Below is the list of things that a farmer can do if he is growing beet, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_bes_start = 1, // Compulsory, must always be 1 (one).
	pl_bes_sleep_all_day = PLBEETSPR_BASE,
	pl_bes_spring_plough,
	pl_bes_spring_harrow,
	pl_bes_ferti_p4,
	pl_bes_ferti_s4,
	pl_bes_ferti_p5,
	pl_bes_ferti_s5,
	pl_bes_heavy_cultivator,
	pl_bes_preseeding_cultivator,
	pl_bes_preseeding_cultivator_sow,
	pl_bes_spring_sow,
	pl_bes_harrow_before_emergence,
	pl_bes_thinning,
	pl_bes_watering1,
	pl_bes_watering2,
	pl_bes_watering3,
	pl_bes_herbicide1,
	pl_bes_herbicide2,
	pl_bes_herbicide3,
	pl_bes_herbicide4,
	pl_bes_fungicide1,
	pl_bes_fungicide2,
	pl_bes_insecticide,
	pl_bes_ferti_p6,
	pl_bes_ferti_s6,
	pl_bes_ferti_p7,
	pl_bes_ferti_s7,
	pl_bes_harvest,
	pl_bes_ferti_p8,
	pl_bes_ferti_s8,
} PLBeetSprToDo;


/**
\brief
PLBeetSpr class
\n
*/
/**
See PLBeetSpr.h::PLBeetSprToDo for a complete list of all possible events triggered codes by the beet management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLBeetSpr: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLBeetSpr()
   {
		m_first_date=g_date->DayInYear( 31,3 );
   }
};

#endif // PLBEETSPR_H

