/**
\file
\brief
<B>NLCarrotsSpring.h This file contains the headers for the CarrotsSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLCarrotsSpring.h
//


#ifndef NLCARROTSSPRING_H
#define NLCARROTSSPRING_H

#define NLCARROTSSPRING_BASE 26300
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_CAS_WINTER_PLOUGH	a_field->m_user[1]
#define NL_CAS_HERBI1	a_field->m_user[2]
#define NL_CAS_HERBI2	a_field->m_user[3]
#define NL_CAS_FUNGI1	a_field->m_user[4]
#define NL_CAS_FUNGI2	a_field->m_user[5]

/** Below is the list of things that a farmer can do if he is growing carrots, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_cas_start = 1, // Compulsory, must always be 1 (one).
	nl_cas_sleep_all_day = NLCARROTSSPRING_BASE,
	nl_cas_spring_plough_sandy,
	nl_cas_ferti_p1,
	nl_cas_ferti_s1,
	nl_cas_preseeding_cultivator,
	nl_cas_bed_forming,
	nl_cas_spring_sow,
	nl_cas_ferti_p2,
	nl_cas_ferti_s2,
	nl_cas_herbicide1,
	nl_cas_herbicide2,
	nl_cas_herbicide3,
	nl_cas_fungicide1,
	nl_cas_fungicide2,
	nl_cas_fungicide3,
	nl_cas_harvest,
} NLCarrotsSpringToDo;


/**
\brief
NLCarrotsSpring class
\n
*/
/**
See NLCarrotsSpring.h::NLCarrotsSpringToDo for a complete list of all possible events triggered codes by the carrots management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCarrotsSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCarrotsSpring()
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,3 );
   }
};

#endif // NLCARROTSSPRING_H

