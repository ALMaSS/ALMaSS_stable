/**
\file
\brief
<B>NLWinterWheat.h This file contains the headers for the WinterWheat class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLWinterWheat.h
//


#ifndef NLWINTERWHEAT_H
#define NLWINTERWHEAT_H

#define NLWINTERWHEAT_BASE 25400
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing winter wheat, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_ww_start = 1, // Compulsory, must always be 1 (one).
	nl_ww_sleep_all_day = NLWINTERWHEAT_BASE,
	nl_ww_stubble_cultivator,
	nl_ww_herbicide1,
	nl_ww_mole_plough,
	nl_ww_autumn_plough,
	nl_ww_preseeding_cultivator_sow,
	nl_ww_herbicide2, 
	nl_ww_ferti_p1, // slurry
	nl_ww_ferti_s1,
	nl_ww_ferti_p2, // PK
	nl_ww_ferti_s2,
	nl_ww_ferti_p3, // NI
	nl_ww_ferti_s3,
	nl_ww_ferti_p4, // NII
	nl_ww_ferti_s4,
	nl_ww_ferti_p5, // NIII
	nl_ww_ferti_s5,
	nl_ww_herbicide3, 
	nl_ww_fungicide1,
	nl_ww_fungicide2,
	nl_ww_fungicide3,
	nl_ww_insecticide1,
	nl_ww_insecticide2,
	nl_ww_growth_regulator1,
	nl_ww_growth_regulator2,
	nl_ww_desiccation,
	nl_ww_harvest,
	nl_ww_straw_chopping,
	nl_ww_hay_bailing,
} NLWinterWheat1ToDo;


/**
\brief
NLWinterWheat class
\n
*/
/**
See NLWinterWheat.h::NLWinterWheatToDo for a complete list of all possible events triggered codes by the winter wheat management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLWinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLWinterWheat()
   {
		// When we start it off, the first possible date for a farm operation is 15th September
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 10,10 );
   }
};

#endif // NLWINTERWHEAT_H

