//
// OFodderbeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OFodderbeet_h
#define OFodderbeet_h

#define OFBeet_BASE 6150

#define OFB_DID_HARROW   m_field->m_user[0]
#define OFB_DID_NPKS_ONE m_field->m_user[1]
#define OFB_DID_SLURRY   m_field->m_user[2]
#define OFB_SOW_DATE     m_field->m_user[3]

#define OFB_DID_ROW_TWO         m_field->m_user[0]
#define OFB_DID_NPKS_TWO        m_field->m_user[2]
#define OFB_DID_WATER_ONE       m_field->m_user[3]
#define OFB_TRULY_DID_WATER_ONE m_field->m_user[4]

typedef enum {
  ofb_start = 1, // Compulsory, start event must always be 1 (one).
  ofb_autumn_plough = OFBeet_BASE,
  ofb_fertmanure,
  ofb_spring_plough,
  ofb_start_threads_one,
  ofb_spring_harrow,
  ofb_fertnpks_one,
  ofb_fertslurry,
  ofb_spring_sow,
  ofb_spring_roll,
  ofb_row_cultivation_one,
  ofb_row_cultivation_two,
  ofb_fertnpks_two,
  ofb_water_one,
  ofb_water_two,
  ofb_harvest
} OFBToDo;



class OFodderbeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OFodderbeet()
  {
     m_first_date=g_date->DayInYear(11,10); // Was 1,10 - changed just for sake of getting year on year beet
  }
};

#endif // OFodderbeet_h
