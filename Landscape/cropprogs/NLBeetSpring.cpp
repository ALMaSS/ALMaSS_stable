/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLBeetSpring.cpp This file contains the source for the NLBeetSpring class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLBeetSpring.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLBeetSpring.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_beet_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_BE_InsecticideDay;
extern CfgInt   cfg_BE_InsecticideMonth;
extern CfgFloat cfg_pest_product_3_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional beet.
*/
bool NLBeetSpring::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case nl_bes_start:
	{
		// nl_bes_start just sets up all the starting conditions and reference dates that are needed to start a nl_be
		NL_BES_HERBI1 = false;
		NL_BES_FUNGI1 = false;


		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(10, 12)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(10, 12));

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - ms_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "NLBeetSpring::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "NLBeetSpring::Do(): ", "Crop start attempt after last possible start date");
					int prev = g_landscape_p->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousCrop(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), nl_bes_spring_sow, false, a_farm, a_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		if (g_date->Date() >= d1) d1 += 365;
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(d1, nl_bes_spring_plough_sandy, false, a_farm, a_field);
		}
		else
		{
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(d1, nl_bes_ferti_s1, false, a_farm, a_field);
			}
			else SimpleEvent_(d1, nl_bes_ferti_p1, false, a_farm, a_field);
		}
	}
	break;

	// This is the first real farm operation
	case nl_bes_spring_plough_sandy:
		if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_spring_plough_sandy, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 7, nl_bes_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 7, nl_bes_ferti_p1, false, a_farm, a_field);
		break;
	case nl_bes_ferti_p1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_p1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_bes_preseeding_cultivator, false, a_farm, a_field);
		break;
	case nl_bes_ferti_s1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_s1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_bes_preseeding_cultivator, false, a_farm, a_field);
		break;
	case nl_bes_preseeding_cultivator:
		if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(14, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_preseeding_cultivator, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_bes_spring_sow, false, a_farm, a_field);
		break;
	case nl_bes_spring_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_spring_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, nl_bes_herbicide1, false, a_farm, a_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), nl_bes_fungicide1, false, a_farm, a_field);	// Fungicide thread = MAIN THREAD
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), nl_bes_ferti_s2, false, a_farm, a_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), nl_bes_ferti_p2, false, a_farm, a_field);
		break;
	case nl_bes_ferti_p2:
		// Here comes fertilizers thread
		if (a_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_p2, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_p2, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_bes_ferti_s2:
		if (a_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_s2, false, a_farm, a_field);
			break;
		}
		else
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_ferti_s2, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_bes_herbicide1: // The first of the pesticide managements.
		// Here comes the herbicide thread
		// We assume that farmers do max 3 herbicide treatments
		if (a_field->GetGreenBiomass() <= 0)
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.75))
			{
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide1, true, a_farm, a_field);
					break;
				}
				NL_BES_HERBI1 = true;
			}
			SimpleEvent_(g_date->Date() + 14, nl_bes_herbicide2, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3), nl_bes_herbicide2, false, a_farm, a_field);
		break;
	case nl_bes_herbicide2:
		if (a_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide2, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide2, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 14, nl_bes_herbicide3, false, a_farm, a_field);
			break;
		}
		break;
	case nl_bes_herbicide3:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide3, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, nl_bes_herbicide4, false, a_farm, a_field);
		break;
	case nl_bes_herbicide4:
		if (a_ev->m_lock || (a_farm->DoIt_prob(0.20) && (NL_BES_HERBI1 == false)))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_herbicide4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_bes_fungicide1:
		// Here comes the fungicide thread
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_fungicide1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 21, nl_bes_fungicide2, false, a_farm, a_field);
		break;
	case nl_bes_fungicide2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.75))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_fungicide2, true, a_farm, a_field);
				break;
			}
			NL_BES_FUNGI1 = true;
		}
		SimpleEvent_(g_date->Date() + 21, nl_bes_fungicide3, false, a_farm, a_field);
		break;
	case nl_bes_fungicide3:
		if (a_ev->m_lock || (a_farm->DoIt_prob(0.667) && NL_BES_FUNGI1 == 1)) // 50% of all farmers
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_bes_fungicide3, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 9), nl_bes_harvest, false, a_farm, a_field);
		break;
	case nl_bes_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_bes_harvest, true, a_farm, a_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLBeetSpring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}