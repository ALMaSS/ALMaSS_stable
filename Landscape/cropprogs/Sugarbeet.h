//
// Sugarbeet.h
//
/* 
*******************************************************************************************************
Copyright (c) 2014, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef Sugarbeet_h
#define Sugarbeet_h

#define SBeet_BASE 6175

#define SB_DID_HARROW   m_field->m_user[0]
#define SB_DID_NPKS_ONE m_field->m_user[1]
#define SB_DID_SLURRY   m_field->m_user[2]
#define SB_SOW_DATE     m_field->m_user[3]

#define SB_DID_ROW_TWO         m_field->m_user[0]
#define SB_DID_INSECT_ONE      m_field->m_user[1]
#define SB_DID_NPKS_TWO        m_field->m_user[2]
#define SB_DID_WATER_ONE       m_field->m_user[3]
#define SB_TRULY_DID_WATER_ONE m_field->m_user[4]
#define SB_DECIDE_TO_HERB		m_field->m_user[5]
#define SB_DECIDE_TO_FI		 m_field->m_user[6]

typedef enum {
  sbe_start = 1, // Compulsory, start event must always be 1 (one).
  sbe_fertmanure = SBeet_BASE,
  sbe_spring_plough,
  sbe_start_threads_one,
  sbe_spring_harrow,
  sbe_fertnpks_one,
  sbe_fertslurry,
  sbe_spring_sow,
  sbe_spring_roll,
  sbe_herbicide_one,
  sbe_herbicide_two,
  sbe_herbicide_three,
  sbe_row_cultivation_one,
  sbe_row_cultivation_two,
  sbe_insecticide_one,
  sbe_fertnpks_two,
  sbe_water_one,
  sbe_insecticide_two,
  sbe_water_two,
  sbe_harvest
} SBEToDo;



class Sugarbeet: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  Sugarbeet()
  {
     m_first_date=g_date->DayInYear(10,3); // 
  }
};

#endif // Sugarbeet_h
