/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PLFodderLucerne1.cpp This file contains the source for the PLFodderLucerne1 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of January 2018 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLFodderLucerne1.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PLFodderLucerne1.h"



/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional fodder lucerne in the first year.
*/
bool PLFodderLucerne1::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case pl_fl1_start:
	{
		// pl_fl1_start just sets up all the starting conditions and reference dates that are needed to start a pl_sw
		PL_FL1_FERTI_P1 = false;
		PL_FL1_FERTI_S1 = false;
		PL_FL1_STUBBLE_PLOUGH = false;
		PL_FL1_WINTER_PLOUGH = false;
		PL_FL1_SPRING_FERTI = 0;
		PL_FL1_HERBICIDE1 = false;
		PL_FL1_HERBI_DATE = 0;

		// Set up the date management stuff

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(10, 8) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PLFodderLucerne1::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PLFodderLucerne1::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = g_landscape_p->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousCrop(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PLFodderLucerne1::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousCrop(a_field->GetRotIndex());
						int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				 //Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
				if (g_date->Date() >= d1) d1 += 365;
				SimpleEvent_(d1, pl_fl1_spring_harrow, false, a_farm, a_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.

		d1 = g_date->OldDays() + g_date->DayInYear(20, 7);
		SimpleEvent_(d1, pl_fl1_herbicide0, false, a_farm, a_field);


	}
	break;

	// This is the first real farm operation
	case pl_fl1_herbicide0:
		if (a_ev->m_lock || a_farm->DoIt(28))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(31, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_herbicide0, true, a_farm, a_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 14, pl_fl1_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 14, pl_fl1_ferti_p1, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_p1:
		// In total 64% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (32%) do it now
		if (a_ev->m_lock || a_farm->DoIt(32))
		{
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again (until 20/8 when we will suceed)
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_p1, true, a_farm, a_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				PL_FL1_FERTI_P1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, pl_fl1_stubble_plough, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_s1:
		// In total 64% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (32%) do it now
		if (a_ev->m_lock || a_farm->DoIt(32))
		{
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_s1, true, a_farm, a_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				PL_FL1_FERTI_S1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, pl_fl1_stubble_plough, false, a_farm, a_field);
		break;
	case pl_fl1_stubble_plough:
		// 60% will do stubble plough, but rest will get away with non-inversion cultivation
		if (a_ev->m_lock || a_farm->DoIt(60))
		{
			if (!a_farm->StubblePlough(a_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_stubble_plough, true, a_farm, a_field);
				break;
			}
			else
			{
				// 60% of farmers will do this, but the other 40% won't so we need to remember whether we are in one or the other group
				PL_FL1_STUBBLE_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->Date() + 1, pl_fl1_autumn_harrow1, false, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_stubble_harrow, false, a_farm, a_field);
		break;
	case pl_fl1_autumn_harrow1:
		if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->DayInYear(5, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl1_autumn_harrow1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 2, pl_fl1_autumn_harrow2, false, a_farm, a_field);
		break;
	case pl_fl1_autumn_harrow2:
		if (a_ev->m_lock || a_farm->DoIt(40))
		{
			if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->Date() + 7 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_autumn_harrow2, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_fl1_ferti_s2, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, pl_fl1_ferti_p2, false, a_farm, a_field);
		break;
	case pl_fl1_stubble_harrow:
		if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl1_stubble_harrow, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 9)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 9);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_fl1_ferti_s2, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, pl_fl1_ferti_p2, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_p2:
		// In total 64% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (32%) do it now (if haven't done before)
		if ((a_ev->m_lock || a_farm->DoIt(static_cast<int>((32.0/68.0)*100)))&&(PL_FL1_FERTI_P1==false))
		{
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_p2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_p3, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_s2:
		// In total 64% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (32%) do it now (if haven't done before)
		if ((a_ev->m_lock || a_farm->DoIt(static_cast<int>((32.0/68.0)*100))) && (PL_FL1_FERTI_S1==false))
		{
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_s2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_s3, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_p3:
		if (a_ev->m_lock || a_farm->DoIt(41))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_p3, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_winter_plough, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_s3:
		if (a_ev->m_lock || a_farm->DoIt(41))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_s3, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_winter_plough, false, a_farm, a_field);
		break;
	case pl_fl1_winter_plough:
		// 88% will do winter plough, but rest will get away with non-inversion cultivation
		if (a_ev->m_lock || a_farm->DoIt(88))
		{
			if (!a_farm->WinterPlough(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_winter_plough, true, a_farm, a_field);
				break;
			}
			else
			{
				// 88% of farmers will do this, but the other 12% won't so we need to remember whether we are in one or the other group
				PL_FL1_WINTER_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, pl_fl1_spring_harrow, false, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_winter_stubble_cultivator_heavy, false, a_farm, a_field);
		break;
	case pl_fl1_winter_stubble_cultivator_heavy:
		// the rest 12% who did not plough do heavy stubble cultivation
		if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl1_winter_stubble_cultivator_heavy, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, pl_fl1_spring_harrow, false, a_farm, a_field);
		break;
	case pl_fl1_spring_harrow:
		if ((a_ev->m_lock) || a_farm->DoIt(90))
		{
			if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_spring_harrow, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(5, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(5, 3);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_fl1_ferti_s4, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, pl_fl1_ferti_p4, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_p4:
		if (a_ev->m_lock || a_farm->DoIt(78))
		{
			if (!a_farm->FP_PK(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_p4, true, a_farm, a_field);
				break;
			}
			PL_FL1_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_heavy_cultivator, false, a_farm, a_field);
		break;
	case pl_fl1_ferti_s4:
		if (a_ev->m_lock || a_farm->DoIt(78))
		{
			if (!a_farm->FA_PK(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_ferti_s4, true, a_farm, a_field);
				break;
			}
			PL_FL1_SPRING_FERTI = 1;
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_heavy_cultivator, false, a_farm, a_field);
		break;
	case pl_fl1_heavy_cultivator:
		if (PL_FL1_SPRING_FERTI == 1)
		{
			if (!a_farm->HeavyCultivatorAggregate(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_heavy_cultivator, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, pl_fl1_herbicide1, false, a_farm, a_field);
		break;
	case pl_fl1_herbicide1:
		// Together 30% of farmers will do glyphosate spraying, but 15% before sow and the rest before emergence
		if (a_ev->m_lock || a_farm->DoIt(15))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_herbicide1, true, a_farm, a_field);
				break;
			}
			PL_FL1_HERBICIDE1 = true;
		}
		// Queue up the next event
		d1 = g_date->Date() + 3;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 4);
		}
		SimpleEvent_(d1, pl_fl1_preseeding_cultivator, false, a_farm, a_field);
		break;
	case pl_fl1_preseeding_cultivator:
		if (a_ev->m_lock || a_farm->DoIt(98))
		{
			if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_preseeding_cultivator, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl1_spring_sow, false, a_farm, a_field);
		break;
	case pl_fl1_spring_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl1_spring_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, pl_fl1_herbicide2, false, a_farm, a_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), pl_fl1_cut_to_silage1, false, a_farm, a_field); // Cutting thread
		break;
	case pl_fl1_herbicide2:
		// Here comes the herbicide thread
		// Check biomass
		if (a_field->GetGreenBiomass() <= 0)
		{
			// The rest 15% of farmers do glyphosate spraying before emergence
			if ((a_ev->m_lock || (a_farm->DoIt(static_cast<int>((15.0 / 85.0) * 100))) && (PL_FL1_HERBICIDE1 == false)))
			{
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_fl1_herbicide2, true, a_farm, a_field);
					break;
				}
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), pl_fl1_herbicide3, false, a_farm, a_field);
			break;
		}
		else {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), pl_fl1_herbicide3, false, a_farm, a_field);
		}
		break;
	case pl_fl1_herbicide3:
		if (a_ev->m_lock || a_farm->DoIt(49))
		{
			if (a_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_herbicide3, true, a_farm, a_field);
			}
			else
			{
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_fl1_herbicide3, true, a_farm, a_field);
					break;
				}
				PL_FL1_HERBI_DATE = g_date->Date();
			}
		}
		// End of thread
		break;
	case pl_fl1_cut_to_silage1:
		// Here comes cutting thread
		if (PL_FL1_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, pl_fl1_cut_to_silage1, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_cut_to_silage1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 7), pl_fl1_cut_to_silage2, false, a_farm, a_field);
		break;
	case pl_fl1_cut_to_silage2:
		if (PL_FL1_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, pl_fl1_cut_to_silage2, false, a_farm, a_field);
		}
		else
		{
			if (a_ev->m_lock || a_farm->DoIt(73))
			{
				if (!a_farm->CutToSilage(a_field, 0.0, g_date->DayInYear(10, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl1_cut_to_silage2, true, a_farm, a_field);
				break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "PLFodderLucerne1::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}