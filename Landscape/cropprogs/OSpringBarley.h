//
// OSpringBarley.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OSpringBarley_H
#define OSpringBarley_H

#define OSBarley_BASE 5600
#define OSB_ISAUTUMNPLOUGH    m_field->m_user[0]
#define OSB_SOW_DATE          m_field->m_user[1]
#define OSB_STRIGLING_DATE    m_field->m_user[2]

typedef enum {
  osb_start = 1, // Compulsory, start event must always be 1 (one).
  osb_ferti_p1 = OSBarley_BASE,
  osb_ferti_p2,
  osb_ferti_s1,
  osb_ferti_s2,
  osb_harvest,
  osb_spring_plough_p,
  osb_spring_plough_s,
  osb_autumn_plough,
  osb_spring_harrow,
  osb_spring_roll,
  osb_spring_sow1,
  osb_spring_sow2,
  osb_hay,
  osb_strigling1,
  osb_strigling2,
  osb_strigling3,
  osb_straw_chopping
// --FN--
} OSBToDo;



class OSpringBarley: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OSpringBarley()
  {
      m_first_date=g_date->DayInYear(1,11);
  }
};

#endif // OSpringBarley_H
