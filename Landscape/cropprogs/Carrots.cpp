/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#define _CRT_SECURE_NO_DEPRECATE
//#include "ALMaSS_Setup.h"
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/Carrots.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;

bool Carrots::Do( Farm * a_farm, LE * a_field, FarmEvent * a_ev ) {
  m_farm = a_farm;
  m_field = a_field;
  m_ev = a_ev;

  bool done = false;
  int today=0;
  int noDates=0;
  int d1=0;

  switch ( m_ev->m_todo ) {
    case ca_start:


      CA_SLURRY_DATE = 0;
	  CA_DECIDE_TO_HERB=1;
	  CA_DECIDE_TO_FI=1;

      // Set up the date management stuff
      m_last_date = g_date->DayInYear( 8,9 );
      // Start and stop dates for all events after harvest
      noDates = 1;
      m_field->SetMDates( 0, 0, g_date->DayInYear( 8,9 ) );
      // 0,0 determined by harvest date - used to see if at all possible
      m_field->SetMDates( 1, 0, g_date->DayInYear( 30, 11 ) );
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary


	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if ( m_ev->m_startday > g_date->DayInYear( 1, 7 ) ) {
        if ( m_field->GetMDates( 0, 0 ) >= m_ev->m_startday ) {
          g_msg->Warn( WARN_BUG, "Carrots::Do(): ""Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.

      if ( !m_ev->m_first_year ) {
        today = g_date->Date();
        // Are we before July 1st?
        d1 = g_date->OldDays() + g_date->DayInYear( 1, 7 );
        if ( today < d1 ) {
          // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "Carrots::Do(): ""Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
        } else {
          d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
          if ( today > d1 ) {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "Carrots::Do(): ""Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      } else {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25, 4 ), ca_spring_plough, false );
      break;
      }
	}//if

      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date +365; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
        d1 = g_date->Date();
      }
      // OK, let's go.

      if ( m_farm->IsStockFarmer() ) {
        SimpleEvent( d1, ca_fa_slurry, false );
      } else {
        SimpleEvent( d1, ca_fp_slurry, false );
      }
    break;

    case ca_fa_slurry:
      if ( m_ev->m_lock || m_farm->DoIt( 75 ) ) {
        if ( !m_farm->FA_Slurry( m_field, 0.0, g_date->DayInYear( 30, 4 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_fa_slurry, true );
        break;
        }
        CA_SLURRY_DATE = g_date->DayInYear();
      }
      {
        d1 = g_date->OldDays() + g_date->DayInYear( 5, 4 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_spring_plough, false );
      }
    break;

    case ca_fp_slurry:
      if ( m_ev->m_lock || m_farm->DoIt( 50 ) ) {
        if ( !m_farm->FP_Slurry( m_field, 0.0, g_date->DayInYear( 30, 3 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_fp_slurry, true );
        break;
        }
        CA_SLURRY_DATE = g_date->DayInYear();
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 5, 4 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_spring_plough, false );
      }
    break;

    case ca_spring_plough: {
        int forceday = g_date->DayInYear( 5, 5 );
        if ( CA_SLURRY_DATE && CA_SLURRY_DATE < forceday - 5 ) { //?AM
          forceday = CA_SLURRY_DATE + 5;
        }
        if ( !m_farm->SpringPlough( m_field, 0.0, forceday - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_spring_plough, false );
        break;
        }
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 10, 4 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_spring_harrow_one, false );
      }
    break;

    case ca_spring_harrow_one:
      if ( !m_farm->SpringHarrow( m_field, 0.0, g_date->DayInYear( 6, 5 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_spring_harrow_one, false );
      break;
      }
      if ( m_farm->DoIt( 75 ) ) {
        int d1 = g_date->OldDays() + g_date->DayInYear( 20, 4 );
        if ( g_date->Date() + 10 > d1 ) {
          d1 = g_date->Date() + 10;
        }
        SimpleEvent( d1, ca_spring_harrow_two, false );
      } else {
        int d1 = g_date->OldDays() + g_date->DayInYear( 20, 4 );
        if ( g_date->Date() + 10 > d1 ) {
          d1 = g_date->Date() + 10;
        }
        if (g_date->DayInYear() >150) {
          break;
        }
        SimpleEvent( d1, ca_herbi_one, false );
      }
    break;

    case ca_spring_harrow_two:
      if ( !m_farm->SpringHarrow( m_field, 0.0, g_date->DayInYear( 20, 5 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_spring_harrow_two, false );
      break;
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 1, 5 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_spring_sow, false );
      }
    break;

    case ca_herbi_one:
      if ( m_farm->DoIt( int(0 * cfg_herbi_app_prop.value() * m_farm->Prob_multiplier()) ) ) { //modified probability
        
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_herbicides(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_herb_app();
			if(m_farm->DoIt(50)) pf->Add_missed_herb_app(); //the 2nd missed application
			if(m_farm->DoIt(50)) pf->Add_missed_herb_app(); //the 3rd missed application
			if(m_farm->DoIt(50)) pf->Add_missed_herb_app(); //the 4th missed application
			CA_DECIDE_TO_HERB=0;
		} //end of the part for dec. making
		else{	
			if ( !m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 20, 5 ) - g_date->DayInYear() ) ) {
				SimpleEvent( g_date->Date() + 1, ca_herbi_one, true );
				break;
			}
		}
      }
      d1 = g_date->OldDays() + g_date->DayInYear( 1, 5 );
      if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
      }
      SimpleEvent( d1, ca_spring_sow, false );
    break;

    case ca_spring_sow:
      if ( !m_farm->SpringSow( m_field, 0.0, g_date->DayInYear( 20, 5 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_spring_sow, false );
      break;
      }
      // Rowcul one carries the main thread of execution.
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 12, 5 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_rowcul_one, false );
      }

      // This is a dead end, eventually.
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 10, 5 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_herbi_two, false );
      }
    break;

    case ca_herbi_two:
      if ( m_ev->m_lock || m_farm->DoIt( int( 0 * cfg_herbi_app_prop.value()*CA_DECIDE_TO_HERB * m_farm->Prob_multiplier()) ) ) { //modified probability
        if ( !m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 30, 5 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_herbi_two, true );
        break;
        }
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 20, 5 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_insect_one, false );
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 20, 5 );
        if ( g_date->Date() + 10 > d1 ) {
          d1 = g_date->Date() + 10;
        }
        SimpleEvent( d1, ca_herbi_three, false );
      }
    break;

    case ca_insect_one:
      if ( m_ev->m_lock || m_farm->DoIt(int(  0 * cfg_ins_app_prop1.value() * m_farm->Prob_multiplier()) ) ) { //modified probability
		
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
			if(m_farm->DoIt(0)) pf->Add_missed_fi_app(); //the 2nd missed application 
			if(m_farm->DoIt(0)) pf->Add_missed_fi_app(); //the 3rd missed application
			break; //here ok cause there's nothing else lined up
		} //end of the part for dec. making

        if ( !m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 15, 6 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_insect_one, true );
        break;
        }
      }
    break;

    case ca_herbi_three:
      if ( m_ev->m_lock || m_farm->DoIt( int( 0 * cfg_herbi_app_prop.value() *CA_DECIDE_TO_HERB * m_farm->Prob_multiplier())) ) { //modified probability
        if ( !m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 20, 6 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_herbi_three, false );
        break;
        }
      }
      if ( m_farm->IsStockFarmer() ) {
        int d1 = g_date->OldDays() + g_date->DayInYear( 1, 6 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_fa_npk_one, false );
      } else {
        int d1 = g_date->OldDays() + g_date->DayInYear( 1, 6 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_fp_npk_one, false );
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 1, 6 );
        if ( g_date->Date() + 10 > d1 ) {
          d1 = g_date->Date() + 10;
        }
        SimpleEvent( d1, ca_herbi_four, false );
      }
    break;

    case ca_herbi_four:
      if ( m_ev->m_lock || m_farm->DoIt(int(  0 * cfg_herbi_app_prop.value()*CA_DECIDE_TO_HERB * m_farm->Prob_multiplier()) ) ) { //modified probability
        if ( !m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 1, 7 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_herbi_four, true );
        break;
        }
      }
    break;

    case ca_fa_npk_one:
      if ( !m_farm->FA_NPK( m_field, 0.0, g_date->DayInYear( 2, 7 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_fa_npk_one, false );
      break;
      }
    break;

    case ca_fp_npk_one:
      if ( !m_farm->FP_NPK( m_field, 0.0, g_date->DayInYear( 2, 7 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_fp_npk_one, false );
      break;
      }
    break;

    case ca_rowcul_one:
      if ( !m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 5, 6 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_rowcul_one, false );
      break;
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 22, 5 );
        if ( g_date->Date() + 10 > d1 ) {
          d1 = g_date->Date() + 10;
        }
        SimpleEvent( d1, ca_rowcul_two, false );
      }
    break;

    case ca_rowcul_two:
      if ( !m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 20, 6 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_rowcul_two, false );
      break;
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 2, 6 );
        if ( g_date->Date() + 10 > d1 ) {
          d1 = g_date->Date() + 10;
        }
        SimpleEvent( d1, ca_rowcul_three, false );
      }
    break;

    case ca_rowcul_three:
      if ( !m_farm->RowCultivation( m_field, 0.0, g_date->DayInYear( 10, 7 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_rowcul_three, false );
      break;
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 1, 7 );
        if ( g_date->Date() + 3 > d1 ) {
          d1 = g_date->Date() + 3;
        }
        SimpleEvent( d1, ca_water_one, false );
      }
    break;

    case ca_water_one:
      if ( !m_farm->Water( m_field, 0.0, g_date->DayInYear( 20, 7 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_water_one, true );
      break;
      }
      {
        d1 = g_date->OldDays() + g_date->DayInYear( 20, 7 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_insect_two, false );
      }
      {
        d1 = g_date->OldDays() + g_date->DayInYear( 21, 7 );
        if ( g_date->Date() + 7 > d1 ) {
          d1 = g_date->Date() + 7;
        }
        SimpleEvent( d1, ca_water_two, false );
      }
    break;

    case ca_insect_two:
      if ( m_ev->m_lock || m_farm->DoIt( int( 0 * cfg_ins_app_prop1.value() *CA_DECIDE_TO_FI * m_farm->Prob_multiplier()) ) ) { //modified probability
        if ( !m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 5, 8 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_insect_two, true );
        break;
        }
      }
    break;

    case ca_water_two:
      if ( !m_farm->Water( m_field, 0.0, g_date->DayInYear( 10, 8 ) - g_date->DayInYear() ) ) {
        SimpleEvent( g_date->Date() + 1, ca_water_two, true );
      break;
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 11, 8 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_water_three, false );
      }

      if ( m_farm->IsStockFarmer() ) {
        int d1 = g_date->OldDays() + g_date->DayInYear( 25, 8 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_fa_npk_two, false );
      } else {
        d1 = g_date->OldDays() + g_date->DayInYear( 25, 8 );
        if ( g_date->Date() > d1 ) {
          d1 = g_date->Date();
        }
        SimpleEvent( d1, ca_fp_npk_two, false );
      }
    break;

    case ca_fa_npk_two:
      if ( m_ev->m_lock || m_farm->DoIt( 50 ) ) {
        if ( !m_farm->FA_NPK( m_field, 0.0, g_date->DayInYear( 20, 8 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_fa_npk_two, true );
        break;
        }
      }
    break;

    case ca_fp_npk_two:
      if ( m_ev->m_lock || m_farm->DoIt( 50 ) ) {
        if ( !m_farm->FP_NPK( m_field, 0.0, g_date->DayInYear( 20, 8 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_fp_npk_two, true );
        break;
        }
      }
    break;

    case ca_water_three:
      if ( m_ev->m_lock || m_farm->DoIt( 70 ) ) {
        if ( !m_farm->Water( m_field, 0.0, g_date->DayInYear( 6, 9 ) - g_date->DayInYear() ) ) {
          SimpleEvent( g_date->Date() + 1, ca_water_three, true );
        break;
        }
      }
      {
        d1 = g_date->OldDays() + g_date->DayInYear( 20, 8 );
        if ( g_date->Date() + 7 > d1 ) {
          d1 = g_date->Date() + 7;
        }

		if(d1 >  m_field->GetMDates( 1, 0 ) - 2){
			d1 =  m_field->GetMDates( 1, 0 ) - 2;
		}
        SimpleEvent( d1, ca_insect_three, false );
      }
    break;

    case ca_insect_three:
      if ( m_ev->m_lock || m_farm->DoIt( int( 0 * cfg_ins_app_prop1.value() * m_farm->Prob_multiplier()) ) ) { //modified probability
        
		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_fungins(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_fi_app();
		} //end of the part for dec. making
		else{
			 if ( !m_farm->InsecticideTreat( m_field, 0.0, g_date->DayInYear( 5, 9 ) - g_date->DayInYear() ) ) {
				 SimpleEvent( g_date->Date() + 1, ca_insect_three, true );
				break;
			}
		}
      }
      {
        int d1 = g_date->OldDays() + g_date->DayInYear( 1, 9 );
        if ( g_date->Date() + 7 > d1 ) {
          d1 = g_date->Date() + 7;
        }
		if(d1 >  m_field->GetMDates( 1, 0 ) - 1){
			d1 =  m_field->GetMDates( 1, 0 ) - 1;
		}
		ChooseNextCrop (1);
        SimpleEvent( d1, ca_harvest, false );
      }
    break;

    case ca_harvest:
		if (m_field->GetMConstants(0)==0) {
			if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "Carrots::Do(): failure in 'Harvest' execution", "" );
				exit( 1 );
			} 
		}
		else {  
			if ( !m_farm->Harvest( m_field, 0.0, m_field->GetMDates( 1, 0 ) - g_date->DayInYear() ) ) {  //is this the finishing date for harvest (1, 0 ) and it is 30.11 !!! and it wasnt changed because the startin date is 8.9 - ie before starting date for winter barley
				SimpleEvent( g_date->Date() + 1, ca_harvest, false );
				break;
			}
		}
		done = true;
		break;

    default:
      g_msg->Warn( WARN_BUG, "Carrots::Do(): ""Unknown event type! ", "" );
      exit( 1 );
  }

  return done;
}


