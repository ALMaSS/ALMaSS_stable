//
// OBarleyPeaCloverGrass.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OBarleyPeaCloverGrass_H
#define OBarleyPeaCloverGrass_H

#define OBPCG_BASE 4500

typedef enum {
  obpcg_start = 1, // Compulsory, start event must always be 1 (one).
  obpcg_ferti_zero = OBPCG_BASE,
  obpcg_water1,
  obpcg_water2,
  obpcg_harvest,
  obpcg_ferti_one,
  obpcg_spring_plough,
  obpcg_spring_harrow,
  obpcg_spring_roll,
  obpcg_spring_sow,
  obpcg_spring_sow1,
  obpcg_spring_sow2,
  obpcg_cut_to_silage,
  obpcg_cattle_out,
  obpcg_cattle_is_out
} OBPCGToDo;



class OBarleyPeaCloverGrass: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OBarleyPeaCloverGrass()
  {
      m_first_date=g_date->DayInYear(1,3);
  }
};

#endif // OBarleyPeaCloverGrass_H
