//
// OPotatoes.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OPotatoes.h"

extern CfgFloat cfg_strigling_prop;

bool OPotatoes::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case ope_start:
  {
    OPOT_SLURRY_DATE   = 0;
    OPOT_SOW_DATE      = 0;
    OPOT_HILLING_THREE = 0;
	m_field->SetVegPatchy(true); // Root crop so open until tall

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(1,11);
      // Start and stop dates for all events after harvest
      m_field->SetMDates(0,0,g_date->DayInYear(1,11));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(1,11));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OPotatoes::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // No late finishing problems fix possible
      }

      int today=g_date->Date();
      d1 = g_date->OldDays() + m_first_date;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if (today > d1)
      {
          // Yes too late - should not happen - raise an error
          g_msg->Warn( WARN_BUG, "OPotatoes::Do(): "
		 "Crop start attempt after last possible start date", "" );
          exit( 1 );
      }
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
    if ( m_farm->IsStockFarmer()) {
      SimpleEvent( d1, ope_fa_slurry, false );
    } else {
      SimpleEvent( d1, ope_fp_slurry, false );
    }
    break;
  }

  case ope_fa_slurry:
    d1 = g_date->DayInYear();
    if (!m_farm->FA_Slurry( m_field, 0.0,
         g_date->DayInYear( 30, 3 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_fa_slurry, false );
      break;
    }
    OPOT_SLURRY_DATE = g_date->DayInYear();
    SimpleEvent( g_date->Date() + 1, ope_fa_manure, false );
    break;

  case ope_fa_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 40 )) {
      if (!m_farm->FA_Manure( m_field, 0.0, OPOT_SLURRY_DATE -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ope_fa_manure, true );
	break;
      }
    }
    SimpleEvent( g_date->Date(), ope_spring_plough, false );
    break;

  case ope_fp_slurry:
    d1 = g_date->DayInYear();
    if (!m_farm->FP_Slurry( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_fp_slurry, false );
      break;
    }
    OPOT_SLURRY_DATE = g_date->DayInYear();
    SimpleEvent( g_date->Date() + 1, ope_fp_manure, false );
    break;

  case ope_fp_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
      if (!m_farm->FP_Manure( m_field, 0.0, OPOT_SLURRY_DATE -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ope_fp_manure, true );
	break;
      }
    }
    SimpleEvent( g_date->Date(), ope_spring_plough, false );
    break;

  case ope_spring_plough:
    d1 = g_date->DayInYear();
    if (!m_farm->SpringPlough( m_field, 0.0,
			       OPOT_SLURRY_DATE -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_spring_plough, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 10, 3 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, ope_spring_harrow, false );
    }
    break;

  case ope_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_spring_harrow, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      if ( g_date->Date() + 1> d1 ) {
	d1 = g_date->Date() + 1;
      }
      SimpleEvent( d1, ope_spring_sow, false );
    }
    break;

  case ope_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 1, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_spring_sow, false );
      break;
    }
    // Start hilling up sub thread.
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 18, 4 );
      if ( g_date->Date() + 18 > d1 ) {
	d1 = g_date->Date() + 18;
      }
      SimpleEvent( d1, ope_hilling_one, false );
    }
    if ( m_farm->DoIt( 10 )) {
      // Flaming
      d1 = g_date->OldDays() + g_date->DayInYear( 7, 4 );
      if ( g_date->Date() + 8 > d1 ) {
	d1 = g_date->Date() + 8;
      }
      SimpleEvent( d1, ope_flaming_one, false );
    } else {
      // Strigling
      d1 = g_date->OldDays() + g_date->DayInYear( 12, 4 );
      if ( g_date->Date() + 12 > d1 ) {
	d1 = g_date->Date() + 12;
      }
      SimpleEvent( d1, ope_strigling_one, false );
    }
    break;

  case ope_flaming_one:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->Swathing( m_field, 0.0,
			     g_date->DayInYear( 10, 5 ) -
			     g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ope_flaming_one, true );
	break;
      }
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 19, 4 );
      if ( g_date->Date() + 7 > d1 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, ope_strigling_two, false );
    }
    break;

  case ope_strigling_one:
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 20, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_strigling_one, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 19, 4 );
      if ( g_date->Date() + 7 > d1 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, ope_strigling_two, false );
    }
    break;

  case ope_strigling_two:
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 27, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_strigling_two, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 28, 4 );
      if ( g_date->Date() + 7 > d1 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, ope_strigling_three, false );
    }
    break;

  case ope_strigling_three:
    if ( !OPOT_HILLING_THREE &&
	 g_date->Date() >= OPOT_HILLING_THREE + 7 ) {
      SimpleEvent( g_date->Date() + 1, ope_strigling_three, true );
      break;
    }
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 75 ))) {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 12, 6 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ope_strigling_three, true );
	break;
      }
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 15, 6 );
      if ( g_date->Date() + 3 > d1 ) {
	d1 = g_date->Date() + 3;
      }
      SimpleEvent( d1, ope_water_one, false );
    }
    break;

  case ope_hilling_one:
    if (!m_farm->HillingUp( m_field, 0.0,
         g_date->DayInYear( 22, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_hilling_one, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 5, 5 );
      if ( g_date->Date() + 14 > d1 ) {
	d1 = g_date->Date() + 14;
      }
      SimpleEvent( d1, ope_hilling_two, false );
    }
    break;

  case ope_hilling_two:
    if (!m_farm->HillingUp( m_field, 0.0,
         g_date->DayInYear( 10, 6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_hilling_two, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 5, 5 );
      if ( g_date->Date() + 14 > d1 ) {
	d1 = g_date->Date() + 14;
      }
      SimpleEvent( d1, ope_hilling_three, false );
    }
    break;

  case ope_hilling_three:
    if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
      if (!m_farm->HillingUp( m_field, 0.0,
			      g_date->DayInYear( 10, 6 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ope_hilling_three, true );
	break;
      }
    }
    OPOT_HILLING_THREE = g_date->Date();
    // Main thread continues from strigling three.
    break;

  case ope_water_one:
    if ( m_ev->m_lock || m_farm->DoIt( 80 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 30, 6 ) -
			  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ope_water_one, true );
	break;
      }
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 26, 6 );
      if ( g_date->Date() + 10 > d1 ) {
	d1 = g_date->Date() + 10;
      }
      SimpleEvent( d1, ope_water_two, false );
    }
    break;

  case ope_water_two:
    if ( m_ev->m_lock || m_farm->DoIt( 70 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 25, 7 ) -
			  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ope_water_two, true );
	break;
      }
    }
     SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 8 ),
		 ope_flaming_two, false );
    break;

  case ope_flaming_two:
  {
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->Swathing( m_field, 0.0,
			     g_date->DayInYear( 15,10 ) -
			     g_date->DayInYear()))
      {
	SimpleEvent( g_date->Date() + 1, ope_flaming_two, true );
	break;
      }
    }
    {
      int d = g_date->OldDays() + g_date->DayInYear( 1, 9 );
      int rndval = random(7) + 14;
      if ( g_date->Date() + rndval > d ) {
	d = g_date->Date() + rndval;
    }
      SimpleEvent( d, ope_harvest, false );
    }
    break;
  }

  case ope_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         g_date->DayInYear( 1, 11 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ope_harvest, false );
      break;
    }
	m_field->SetVegPatchy(false);
	done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OPotatoesEat::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


