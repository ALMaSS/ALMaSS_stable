/**
\file
\brief
<B>NLMaize.h This file contains the headers for the Maize class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLMaize.h
//


#ifndef NLMAIZE_H
#define NLMAIZE_H

#define NLMAIZE_BASE 25200
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_M_START_FERTI	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing mazie, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_m_start = 1, // Compulsory, must always be 1 (one).
	nl_m_sleep_all_day = NLMAIZE_BASE,
	nl_m_stubble_harrow1,
	nl_m_stubble_harrow2,
	nl_m_winter_plough1,
	nl_m_winter_plough2,
	nl_m_ferti_p1,
	nl_m_ferti_s1,
	nl_m_spring_plough1,
	nl_m_spring_plough2,
	nl_m_preseeding_cultivator,
	nl_m_spring_sow_with_ferti,
	nl_m_spring_sow,
	nl_m_harrow,
	nl_m_ferti_p2,
	nl_m_ferti_s2,
	nl_m_herbicide1,
	nl_m_harvest,
	nl_m_straw_chopping,
} NLMaizeToDo;


/**
\brief
NLMaize class
\n
*/
/**
See NLMaize.h::NLMaizeToDo for a complete list of all possible events triggered codes by the mazie management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLMaize: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLMaize()
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );
   }
};

#endif // NLMAIZE_H

