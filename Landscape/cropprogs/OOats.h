//
// OOats.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OOats_h
#define OOats_h

#define OOats_BASE 6200

#define OOA_SOW_DATE          m_field->m_user[0]
#define OOA_DID_STRIGLING_ONE m_field->m_user[1]

typedef enum {
  ooa_start = 1, // Compulsory, start event must always be 1 (one).
  ooa_autumn_plough = OOats_BASE,
  ooa_fertmanure_stock,
  ooa_spring_plough,
  ooa_spring_harrow,
  ooa_fertslurry_stock,
  ooa_spring_sow_one,
  ooa_spring_sow_two,
  ooa_spring_roll,
  ooa_strigling_one,
  ooa_strigling_two,
  ooa_strigling_three,
  ooa_harvest,
  ooa_straw_chopping,
  ooa_hay_bailing
// --FN--
} OOAToDo;



class OOats: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OOats()
  {
      m_first_date=g_date->DayInYear(5,3);
  }
};

#endif // OOats_h
