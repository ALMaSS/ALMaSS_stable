//
// AgroChemIndustryCereal.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SYNGENTACEREAL_H
#define SYNGENTACEREAL_H

#define SYNGENTACEREAL_BASE 7300
#define SC_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define SC_AUTUMN_PLOUGH         m_field->m_user[1]

const int April1     =  91;
const int May1       = 121;
const int June1      = 152;
const int July1      = 182;

typedef enum {
  sc_start = 1, // Compulsory, must always be 1 (one).
  sc_sleep_all_day = SYNGENTACEREAL_BASE,
  sc_ferti_p1,
  sc_ferti_s1,
  sc_ferti_s2,
  sc_autumn_plough,
  sc_autumn_harrow,
  sc_stubble_harrow1,
  sc_autumn_sow,
  sc_autumn_roll,
  sc_ferti_p2,
  sc_herbicide1, //7310
  sc_spring_roll,
  sc_herbicide2,
  sc_GR,
  sc_fungicide,
  sc_fungicide2,
  sc_insecticide1,
  sc_insecticide2,
  sc_insecticide3,
  sc_strigling1,
  sc_strigling2, //7320
  sc_water1,
  sc_water2,
  sc_ferti_p3,
  sc_ferti_p4,
  sc_ferti_p5,
  sc_ferti_s3,
  sc_ferti_s4,
  sc_ferti_s5,
  sc_harvest,   //7329
  sc_straw_chopping,
  sc_hay_turning,
  sc_hay_baling,
  sc_stubble_harrow2,
  sc_grubning,
} SyngetaCerealToDo;


class AgroChemIndustryCereal: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   AgroChemIndustryCereal()
   {
      m_first_date=g_date->DayInYear(15,9);
   }
   int GetSprayDate();
};

#endif // SYNGENTACEREAL_H
