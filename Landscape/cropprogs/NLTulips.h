/**
\file
\brief
<B>NLTulips.h This file contains the headers for the Tulips class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLTulips.h
//


#ifndef NLTULIPS_H
#define NLTULIPS_H

#define NLTULIPS_BASE 25700
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_TU_AUTUMN_PLOUGH	a_field->m_user[1]
#define NL_TU_FERTI_DONE	a_field->m_user[2]
#define NL_TU_STRAW_REMOVED	a_field->m_user[3]
#define NL_TU_FUNGI_SPRAY_DATE	a_field->m_user[4]

/** Below is the list of things that a farmer can do if he is growing tulips, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_tu_start = 1, // Compulsory, must always be 1 (one).
	nl_tu_sleep_all_day = NLTULIPS_BASE,
	nl_tu_manure,
	nl_tu_autumn_plough,
	nl_tu_autumn_heavy_stubble_cultivator,
	nl_tu_bed_forming,
	nl_tu_fungicide0,
	nl_tu_planting,
	nl_tu_ferti_p1,
	nl_tu_ferti_s1,
	nl_tu_straw_covering,
	nl_tu_straw_removal,
	nl_tu_herbicide1,
	nl_tu_herbicide2,
	nl_tu_fungicide1,
	nl_tu_fungicide2,
	nl_tu_fungicide3,
	nl_tu_fungicide4,
	nl_tu_fungicide5,
	nl_tu_fungicide6,
	nl_tu_fungicide7,
	nl_tu_fungicide8,
	nl_tu_fungicide9,
	nl_tu_fungicide10,
	nl_tu_added_insecticide1,
	nl_tu_added_insecticide2,
	nl_tu_added_insecticide3,
	nl_tu_added_insecticide4,
	nl_tu_added_insecticide5,
	nl_tu_added_insecticide6,
	nl_tu_added_insecticide7,
	nl_tu_added_insecticide8,
	nl_tu_added_insecticide9,
	nl_tu_added_insecticide10,
	nl_tu_insecticide1,
	nl_tu_insecticide2,
	nl_tu_ferti_p2,
	nl_tu_ferti_s2,
	nl_tu_ferti_p3,
	nl_tu_ferti_s3,
	nl_tu_ferti_p4,
	nl_tu_ferti_s4,
	nl_tu_flower_cutting,
	nl_tu_irrigation,
	nl_tu_harvest,
} NLTulipsToDo;


/**
\brief
NLTulips class
\n
*/
/**
See NLTulips.h::NLTulipsToDo for a complete list of all possible events triggered codes by the tulips management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLTulips: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLTulips()
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10);
   }
};

#endif // NLTULIPS_H

