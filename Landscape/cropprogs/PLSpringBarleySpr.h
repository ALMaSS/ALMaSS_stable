/**
\file
\brief
<B>PLSpringBarleySpr.h This file contains the headers for the SpringBarleySpr class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLSpringBarleySpr.h
//


#ifndef PLSPRINGBARLEYSPR_H
#define PLSPRINGBARLEYSPR_H

#define PLSPRINGBARLEYSPR_BASE 21400
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_SBS_SPRING_FERTI a_field->m_user[6]
#define PL_SBS_DECIDE_TO_GR a_field->m_user[6]

/** Below is the list of things that a farmer can do if he is growing spring barley, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_sbs_start = 1, // Compulsory, must always be 1 (one).
	pl_sbs_sleep_all_day = PLSPRINGBARLEYSPR_BASE,
	pl_sbs_spring_plough,
	pl_sbs_spring_harrow,
	pl_sbs_ferti_p1,
	pl_sbs_ferti_s1,
	pl_sbs_heavy_cultivator,
	pl_sbs_preseeding_cultivator,
	pl_sbs_preseeding_cultivator_sow,	// 
	pl_sbs_spring_sow,	
	pl_sbs_herbicide1,
	pl_sbs_fungicide1,
	pl_sbs_fungicide2,
	pl_sbs_fungicide3,
	pl_sbs_insecticide1,
	pl_sbs_insecticide2,
	pl_sbs_ferti_p2,		
	pl_sbs_ferti_s2,	
	pl_sbs_ferti_p3,	// 
	pl_sbs_ferti_s3,
	pl_sbs_ferti_p4,
	pl_sbs_ferti_s4,
	pl_sbs_harvest,
	pl_sbs_straw_chopping,
	pl_sbs_hay_bailing,
	pl_sbs_ferti_p5,
	pl_sbs_ferti_s5,	
	pl_sbs_ferti_p6,	
	pl_sbs_ferti_s6,	// 
} PLSpringBarleySprToDo;


/**
\brief
PLSpringBarleySpr class
\n
*/
/**
See PLSpringBarleySpr.h::PLSpringBarleySprToDo for a complete list of all possible events triggered codes by the spring barley management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLSpringBarleySpr: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLSpringBarleySpr()
   {
		m_first_date=g_date->DayInYear( 1,3 );
   }
};

#endif // PLSPRINGBARLEYSPR_H

