//
// OWinterWheatUndersown.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWINTERWHEATUndersownExt_H
#define OWINTERWHEATUndersownExt_H

#define OWINTERWHEATUndersownExt_BASE 30400
#define OWWUEXT_PLOUGH_RUNS       m_field->m_user[0]
#define OWWUEXT_HARROW_RUNS       m_field->m_user[1]

typedef enum {
    owwuExt_start = 1, // Compulsory, must always be 1 (one).
    owwuExt_sleep_all_day = OWINTERWHEATUndersownExt_BASE,
    owwuExt_ferti_s1,
    owwuExt_ferti_s2,
    owwuExt_ferti_s3,
    owwuExt_ferti_p1,
    owwuExt_ferti_p2,
    owwuExt_autumn_plough,
    owwuExt_autumn_harrow,
    owwuExt_autumn_sow,
    owwuExt_strigling1,
    owwuExt_strigling2,
    owwuExt_strigling_sow,
    owwuExt_spring_sow,
    owwuExt_spring_roll1,
    owwuExt_spring_roll2,
    owwuExt_harvest,
    owwuExt_hay_turning,
    owwuExt_straw_chopping,
    owwuExt_hay_baling,
    owwuExt_stubble_harrow1,
    owwuExt_stubble_harrow2,
    owwuExt_deep_plough,
    owwuExt_catch_all
} OWinterWheatUndersownExtToDo;


class OWinterWheatUndersownExt: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   OWinterWheatUndersownExt()
   {
     m_first_date=g_date->DayInYear(28,8);
   }
};

#endif // OWINTERWHEATUndersown_H
