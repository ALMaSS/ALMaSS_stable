//
// OCloverGrassSilage1.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OCloverGrassSilage1H
#define OCloverGrassSilage1H

#define OCGS1_CUT_DATE     m_field->m_user[0]
#define OCGS1_SLURRY       m_field->m_user[1]
#define OCGS1_BASE 6000

typedef enum {
  ocgs1_start = 1, // Compulsory, start event must always be 1 (one).
  ocgs1_ferti_zero=OCGS1_BASE,
  ocgs1_ferti_one,
  ocgs1_ferti_two,
  ocgs1_ferti_three,
  ocgs1_cut_to_silage,
  ocgs1_cut_to_silage1,
  ocgs1_cut_to_silage2,
  ocgs1_cut_to_silage3,
  ocgs1_water_zero,
  ocgs1_water_one,
} OCloverGrassSilage1ToDo;



class OCloverGrassSilage1: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  OCloverGrassSilage1()
  {
      m_first_date=g_date->DayInYear(1,4);
  }
};

#endif
// OCloverGrassSilage1.H

