//
// OWinterWheat.h
//
/* 
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef OWINTERWHEAT_H
#define OWINTERWHEAT_H

#define OWINTERWHEAT_BASE 5040
#define OWW_PLOUGH_RUNS       m_field->m_user[0]
#define OWW_HARROW_RUNS       m_field->m_user[1]

typedef enum {
    oww_start = 1, // Compulsory, must always be 1 (one).
    oww_sleep_all_day = OWINTERWHEAT_BASE,
    oww_ferti_s1,
    oww_ferti_s2,
    oww_ferti_s3,
    oww_ferti_p1,
    oww_ferti_p2,
    oww_autumn_plough,
    oww_autumn_harrow,
    oww_autumn_sow,
    oww_strigling1,
    oww_strigling2,
    oww_strigling_sow,
    oww_spring_sow,
    oww_spring_roll1,
    oww_spring_roll2,
    oww_harvest,
    oww_hay_turning,
    oww_straw_chopping,
    oww_hay_baling,
    oww_stubble_harrow1,
    oww_stubble_harrow2,
    oww_deep_plough,
    oww_catch_all
} OWinterWheatToDo;


class OWinterWheat: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   OWinterWheat()
   {
     m_first_date=g_date->DayInYear(28,8);
   }
};

#endif // OWINTERWHEATUndersown_H
