//
// OOats.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OOats.h"

extern CfgFloat cfg_strigling_prop;

bool OOats::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case ooa_start:
  {
					// Set up the date management stuff
					// Could save the start day in case it is needed later
					// m_field->m_startday = m_ev->m_startday;
					m_last_date = g_date->DayInYear(25, 8);
					// Start and stop dates for all events after harvest
					int noDates = 2;
					m_field->SetMDates(0, 0, g_date->DayInYear(11, 8));
					// Determined by harvest date - used to see if at all possible
					m_field->SetMDates(1, 0, g_date->DayInYear(20, 8));
					m_field->SetMDates(0, 1, g_date->DayInYear(25, 8));
					m_field->SetMDates(1, 1, g_date->DayInYear(25, 8));
					// Check the next crop for early start, unless it is a spring crop
					// in which case we ASSUME that no checking is necessary!!!!
					// So DO NOT implement a crop that runs over the year boundary
					if (m_ev->m_startday > g_date->DayInYear(1, 7))
					{
						if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
						{
							g_msg->Warn(WARN_BUG, "OOats::Do(): "
								"Harvest too late for the next crop to start!!!", "");
							exit(1);
						}
						// Now fix any late finishing problems
						for (int i = 0; i < noDates; i++)
						{
							if (m_field->GetMDates(0, i) >= m_ev->m_startday)
								m_field->SetMDates(0, i, m_ev->m_startday - 1);
							if (m_field->GetMDates(1, i) >= m_ev->m_startday)
								m_field->SetMDates(1, i, m_ev->m_startday - 1);
						}
					}
					// Now no operations can be timed after the start of the next crop.

					int d1;
					if (!m_ev->m_first_year)
					{
						int today = g_date->Date();
						// Are we before July 1st?
						d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
						if (today < d1)
						{
							// Yes, too early. We assumme this is because the last crop was late
							g_msg->Warn(WARN_BUG, " OOats::Do(): "
								"Crop start attempt between 1st Jan & 1st July", "");
							exit(1);
						}
						else
						{
							d1 = g_date->OldDays() + m_first_date +365; // Add 365 for spring crop
							if (today > d1)
							{
								// Yes too late - should not happen - raise an error
								g_msg->Warn(WARN_BUG, " OOats::Do(): "
									"Crop start attempt after last possible start date", "");
								exit(1);
							}
						}
					}
					else
					{
						SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4),
							ooa_spring_harrow, false);
						break;
					}
					// End single block date checking code. Please see next line
					// comment as well.
					// Reinit d1 to first possible starting date.
					d1 = g_date->OldDays() + m_first_date + 365;
					if (g_date->Date() > d1)
					{
						d1 = g_date->Date();
					}
					// OK, let's go.
					OOA_SOW_DATE = 0;
					OOA_DID_STRIGLING_ONE = false;
					if (m_farm->IsStockFarmer())
					{
						SimpleEvent(d1, ooa_fertmanure_stock, false);
					}
					else
					{
						SimpleEvent(d1, ooa_spring_plough, false);
					}
					break;
  }

  case ooa_fertmanure_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 70 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 30,3 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ooa_fertmanure_stock, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(), ooa_spring_plough, false );
    break;

  case ooa_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
			       g_date->DayInYear( 30,3 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ooa_spring_plough, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
		 ooa_spring_harrow, false );
    break;

  case ooa_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
			       g_date->DayInYear( 20,4 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ooa_spring_harrow, true );
      break;
    }
    if ( m_farm->IsStockFarmer()) {
      SimpleEvent( g_date->Date(), ooa_fertslurry_stock, false );
    } else {
      SimpleEvent( g_date->Date(), ooa_spring_sow_one, false );
    }
    break;

  case ooa_fertslurry_stock:
    if (!m_farm->FA_Slurry( m_field, 0.0,
			    g_date->DayInYear( 20,4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ooa_fertslurry_stock, true );
      break;
    }
    SimpleEvent( g_date->Date(), ooa_spring_sow_one, false );
    break;

  case ooa_spring_sow_one:
    if (!m_farm->SpringSow( m_field, 0.0,
			    g_date->DayInYear( 20,4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ooa_spring_sow_one, true );
      break;
    }
    OOA_SOW_DATE = g_date->Date();
    if ( m_farm->IsStockFarmer()) {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 5,4 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 5,4 );
      }
      SimpleEvent( d1, ooa_spring_roll, false );
    } else {
      SimpleEvent( g_date->Date() + 1, ooa_spring_sow_two, false );
    }
    break;

  case ooa_spring_sow_two:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      if (!m_farm->SpringSow( m_field, 0.0,
			      g_date->DayInYear( 22,4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ooa_spring_sow_two, true );
	break;
      }
      OOA_SOW_DATE = g_date->Date();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ),
		 ooa_spring_roll, false );
    break;

  case ooa_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 80 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
			       g_date->DayInYear( 20,4 ) -
			       g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ooa_spring_roll, true );
        break;
      }
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 15,4 );
      if ( d1 < OOA_SOW_DATE + 10 ) {
	d1 = OOA_SOW_DATE + 10;
      }
      SimpleEvent( d1, ooa_strigling_one, false );
    }
    break;

  case ooa_strigling_one:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 80 )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 30,4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ooa_strigling_one, true );
	break;
      }
      OOA_DID_STRIGLING_ONE = true;
    }
    if ( ! OOA_DID_STRIGLING_ONE ) {
      // No strigling, so jump directly to harvest.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,8 ),
		   ooa_harvest, false );
      break;
    }
    {
      int d1 = g_date->Date() + 10;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25,4 ) ) {
	d1 = g_date->OldDays() + g_date->DayInYear( 25,4 );
      }
      SimpleEvent( d1, ooa_strigling_two, false );
    }
    break;

  case ooa_strigling_two:
    if (!m_farm->Strigling( m_field, 0.0,
			    g_date->DayInYear( 5,5 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ooa_strigling_two, true );
      break;
    }
    {
      int d1 = g_date->Date() + 10;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 5,5 ) ) {
	d1 = g_date->OldDays() + g_date->DayInYear( 5,5 );
      }
      SimpleEvent( d1, ooa_strigling_three, false );
    }
    break;

  case ooa_strigling_three:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 60 )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 20,5 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ooa_strigling_three, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,8 ),
		 ooa_harvest, false );
    break;

  case ooa_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  m_field->GetMDates(1,0) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ooa_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), ooa_straw_chopping, false );
    break;

  case ooa_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 80 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
				  m_field->GetMDates(0,1) -
				  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ooa_straw_chopping, true );
	break;
      }
      done = true;
      break;
    }
    SimpleEvent( g_date->Date(), ooa_hay_bailing, false );
    break;

  case ooa_hay_bailing:
    if (!m_farm->HayBailing( m_field, 0.0,
			     m_field->GetMDates(1,1) -
			     g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ooa_hay_bailing, true );
      break;
    }
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OOats::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


