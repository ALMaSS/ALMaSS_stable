//
// NorwegianOats.h
//
/* 
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef NorwegianPotatoes_H
#define NorwegianPotatoes_H

#define NPOTATOES_BASE 16900
#define NPOT_SLURRY_DATE m_field->m_user[0]
#define NPOT_HERBI_DATE  m_field->m_user[1]
#define NPOT_STRIG_DATE  m_field->m_user[2]
#define NPOT_HILL_DATE   m_field->m_user[3]
#define NPOT_DID_TREAT   m_field->m_user[4]
#define NPOT_DID_HILL    m_field->m_user[5]
#define NPOT_WATER_DATE  m_field->m_user[6]
#define NPOT_FUNGI_DATE  m_field->m_user[7]

typedef enum {
	npe_start = 1, // Compulsory, start event must always be 1 (one).
	npe_autumn_plough = NPOTATOES_BASE,
	npe_spring_plough,
	npe_spring_harrow,
	npe_fa_slurry,
	npe_spring_sow,
	npe_fa_npk,
	npe_fp_npk,
	npe_herbi_one,
	npe_herbi_two,
	npe_herbi_three,
	npe_strigling_one,
	npe_strigling_two,
	npe_strigling_three,
	npe_hilling,
	npe_insecticide,
	npe_water_one,
	npe_water_two,
	npe_water_three,
	npe_fungi_one,
	npe_fungi_two,
	npe_fungi_three,
	npe_fungi_four,
	npe_fungi_five,
	npe_growth_reg,
	npe_harvest
} NorwegianPotatoesToDo;



class NorwegianPotatoes : public Crop
{
public:
	bool  Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev);
	NorwegianPotatoes()
	{
		m_first_date = g_date->DayInYear(1, 11);
		m_ddegstoharvest = 1400; // this is hard coded for each crop that uses this 
	}
};

#endif // NorwegianPotatoes_H
