//
// WWheatPToxicControl.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef WWHEATPTOXICCONTROL_H
#define WWHEATPTOXICCONTROL_H

#define WWHEATPTOXICCONTROL_BASE 60100
#define WWTC_WAIT_FOR_PLOUGH       m_field->m_user[0]
#define WWTC_AUTUMN_PLOUGH         m_field->m_user[1]

typedef enum {
  wwptc_start = 1, // Compulsory, must always be 1 (one).
  wwptc_sleep_all_day = WWHEATPTOXICCONTROL_BASE,
  wwptc_ferti_p1,
  wwptc_ferti_s1,
  wwptc_ferti_s2,
  wwptc_autumn_plough,
  wwptc_autumn_harrow,
  wwptc_stubble_harrow1,
  wwptc_autumn_sow,
  wwptc_autumn_roll,
  wwptc_ferti_p2,
  wwptc_herbicide1,
  wwptc_spring_roll,
  wwptc_herbicide2,
  wwptc_GR,
  wwptc_fungicide,
  wwptc_fungicide2,
  wwptc_insecticide1,
  wwptc_insecticide2,
  wwptc_insecticide3,
  wwptc_strigling1,
  wwptc_strigling2,
  wwptc_water1,
  wwptc_water2,
  wwptc_ferti_p3,
  wwptc_ferti_p4,
  wwptc_ferti_p5,
  wwptc_ferti_s3,
  wwptc_ferti_s4,
  wwptc_ferti_s5,
  wwptc_harvest,
  wwptc_straw_chopping,
  wwptc_hay_turning,
  wwptc_hay_baling,
  wwptc_stubble_harrow2,
  wwptc_grubning,
} WinterWheatPToxicControlToDo;


class WWheatPToxicControl: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
};

#endif // WHEATPTOXICCONTROL_H
