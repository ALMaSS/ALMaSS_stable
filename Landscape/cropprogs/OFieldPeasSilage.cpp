//
// OFieldPeasSilage.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OFieldPeasSilage.h"

extern CfgFloat cfg_strigling_prop;

bool OFieldPeasSilage::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case ofps_start:
    {
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(25,8);
      // Start and stop dates for all events after harvest
      int noDates= 1;
      m_field->SetMDates(0,0,g_date->DayInYear(25,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(20,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OFieldPeasSilage::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      int d1;
      int today=g_date->Date();
      d1 = g_date->OldDays() + m_first_date +365; // Add 365 for spring crop
      if (today > d1)
      {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "OFieldPeasSilage::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays()+m_first_date;;
      if ( ! m_ev->m_first_year ) d1+=365; // Add 365 for spring crop (not 1st yr)
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      SimpleEvent( d1, ofps_spring_plough, false );
    }
    break;

  case ofps_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
         g_date->DayInYear( 30,3 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_spring_plough, true );
      break;
    }
    SimpleEvent( g_date->Date()+1, ofps_spring_harrow, false );
    break;

  case ofps_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 5,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_spring_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
    						 ofps_spring_sow, false );
    break;

  case ofps_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_spring_sow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                 ofps_spring_roll, false );
		// --FN--
    break;

  case ofps_spring_roll:
    if (!m_farm->SpringRoll( m_field, 0.0,
         g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_spring_roll, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,4 ),
                  ofps_strigling1, false );
    break;

  case ofps_strigling1:
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_strigling1, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
                  ofps_strigling2, false );
    break;

  case ofps_strigling2:
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 10,5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_strigling2, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 11,5 ),
                  ofps_strigling3, false );
    break;

  case ofps_strigling3:
    if (!m_farm->Strigling( m_field, 0.0,
         g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_strigling3, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16,5 ),
                  ofps_strigling4, false );
    break;

  case ofps_strigling4:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 26,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ofps_strigling4, true );
        break;
      }
    }
		// --FN--
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,6 ),
                ofps_water1, false );
    break;

  case ofps_water1:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 20,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ofps_water1, true );
        break;
      }
      SimpleEvent( g_date->Date()+5, ofps_water2, false );
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,0),
                  ofps_harvest, false );
    break;

  case ofps_water2:
    if (!m_farm->Water( m_field, 0.0,
         g_date->DayInYear( 30,6 ) - g_date->DayInYear())) {
    	// --FN--
      SimpleEvent( g_date->Date() + 1, ofps_water2, true );
      break;
    }
    break;

  case ofps_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         m_field->GetMDates(1,0) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ofps_harvest, true );
      break;
    }
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OFieldPeasSilage::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


