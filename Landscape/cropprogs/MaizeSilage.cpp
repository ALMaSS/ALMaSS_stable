//
// MaizeSilage.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/MaizeSilage.h"

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool MaizeSilage::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  int d1=0;

  bool done = false;
  switch ( m_ev->m_todo ) {
  case ms_start:
  {
      MAIZESILAGE_SOW_DATE       = 0;
      MAIZESILAGE_HERBI_ONE_DATE = 0;
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(30,9);
      // Start and stop dates for all events after harvest
      int noDates= 2;
      m_field->SetMDates(0,0,g_date->DayInYear(1,9));
      // 0,0 determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(10,9));
      m_field->SetMDates(0,1,g_date->DayInYear(15,9));
      m_field->SetMDates(1,1,g_date->DayInYear(30,9));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	  if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
			char veg_type[ 20 ];
			//char farm_no[20];
			//sprintf (farm_no, "%d", m_almass_no);
			sprintf( veg_type, "%d", m_ev->m_next_tov );
			g_msg->Warn( WARN_FILE, "MaizeSilage::Do(): Harvest too late for the next crop to start!!! The next crop is: ", veg_type);
			
			//g_msg->Warn( WARN_BUG, "MaizeSilage::Do(): Harvest too late for the next crop to start!!!", "" );
			exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.

      if ( ! m_ev->m_first_year )
      {
	int today=g_date->Date();
        // Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (today < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "MaizeSilage::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "MaizeSilage::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear(25,4),
                      ms_spring_plough, false );
         break;
      }
	  }//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      if ( m_farm->DoIt( 50 ))
      {
       SimpleEvent( d1, ms_fa_manure_a, false );
      }
      else
      {
       SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 3 ) + 365,
		   ms_fa_manure_b, false );
      }
      break;
  }

  case ms_fa_manure_a:
    if (!m_farm->FA_Manure( m_field, 0.0,
         g_date->DayInYear( 30, 11 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_fa_manure_a, false );
      break;
    }
    SimpleEvent( g_date->Date(), ms_autumn_plough, false );
    break;

  case ms_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
         g_date->DayInYear( 30, 11 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_autumn_plough, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 4 ) + 365,
                 ms_spring_harrow, false );
    break;

  case ms_fa_manure_b:
    if (!m_farm->FA_Manure( m_field, 0.0,
         g_date->DayInYear( 25, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_fa_manure_b, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      if ( g_date->Date()+1 > d1 ) {
	d1 = g_date->Date()+1;
      }
      SimpleEvent( d1, ms_fa_slurry_one, false );
    }
    break;

  case ms_fa_slurry_one:
    if (!m_farm->FA_Slurry( m_field, 0.0,
         g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_fa_slurry_one, false );
      break;
    }
    SimpleEvent( g_date->Date() + 1, ms_spring_plough, false );
    break;

  case ms_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
         g_date->DayInYear( 1, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_spring_plough, false );
      break;
    }
    SimpleEvent( g_date->Date(), ms_spring_harrow, false );
    break;

  case ms_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 10, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_spring_harrow, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 25, 4 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, ms_spring_sow, false );
    }
    break;

  case ms_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 15, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_spring_sow, false );
      break;
    }
    MAIZESILAGE_SOW_DATE = g_date->Date();
    SimpleEvent( g_date->Date(), ms_fa_npk, false );
    break;

  case ms_fa_npk:
    if (!m_farm->FA_NPK( m_field, 0.0,
         g_date->DayInYear( 20, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_fa_npk, false );
      break;
    }
    if ( m_farm->DoIt( 70 )) {
      {
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 5 );
	if ( MAIZESILAGE_SOW_DATE + 5 > d1 ) {
	  d1 = MAIZESILAGE_SOW_DATE + 5;
	}
	SimpleEvent( d1, ms_herbi_one, false );
      }
    } else {
      {
	d1 = g_date->OldDays() + g_date->DayInYear( 2, 5 );
	if ( MAIZESILAGE_SOW_DATE + 7 > d1 ) {
	  d1 = MAIZESILAGE_SOW_DATE + 7;
	}
	SimpleEvent( d1, ms_row_one, false );
      }
    }
    break;

  case ms_herbi_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (100*cfg_herbi_app_prop.value() * m_farm->Prob_multiplier()) )) { //modified probability

		//new - for decision making
		TTypesOfVegetation tov = m_field->GetVegType();
		if(!m_ev->m_lock && !m_farm->Spraying_herbicides(tov)){
			Field * pf =  dynamic_cast<Field*>(m_field); 
			pf->Add_missed_herb_app();
			if(m_farm->DoIt(59)) pf->Add_missed_herb_app(); //the 2nd missed application
		} //end of the part for dec. making
		else{
			if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 25, 5 ) - g_date->DayInYear())) {
			  SimpleEvent( g_date->Date() + 1, ms_herbi_one, false );
			  break;
			}
		}
	} 
    MAIZESILAGE_HERBI_ONE_DATE = g_date->Date();
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 1, 5 );
      if ( MAIZESILAGE_SOW_DATE + 5 > d1 ) {
	d1 = MAIZESILAGE_SOW_DATE + 5;
      }
      SimpleEvent( d1, ms_fa_slurry_two, false );
    }
    break;

  case ms_row_one:
    if (!m_farm->RowCultivation( m_field, 0.0,
         g_date->DayInYear( 25, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_row_one, false );
      break;
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 1, 5 );
      if ( MAIZESILAGE_SOW_DATE + 5 > d1 ) {
	d1 = MAIZESILAGE_SOW_DATE + 5;
      }
      SimpleEvent( d1, ms_fa_slurry_two, false );
    }
    break;

  case ms_fa_slurry_two:
    if (!m_farm->FA_Slurry( m_field, 0.0,
         g_date->DayInYear( 25, 5 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_fa_slurry_two, false );
      break;
    }
    if ( m_farm->DoIt( 86 )) {
      {
	d1 = g_date->OldDays() + g_date->DayInYear( 10, 5 );
	if ( MAIZESILAGE_HERBI_ONE_DATE + 10 > d1 ) {
	  d1 = MAIZESILAGE_HERBI_ONE_DATE + 10;
	}
	SimpleEvent( d1, ms_herbi_two, false );
      }
    } else {
      {
	d1 = g_date->OldDays() + g_date->DayInYear( 21, 5 );
	if ( g_date->Date() + 1 > d1 ) {
	  d1 = g_date->Date() + 1;
	}
	SimpleEvent( d1, ms_row_two, false );
      }
    }
    break;

  case ms_herbi_two:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (59*cfg_herbi_app_prop.value()  * m_farm->Prob_multiplier()))) { //modified probability
      if (!m_farm->HerbicideTreat( m_field, 0.0,
         g_date->DayInYear( 5, 6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ms_herbi_two, false );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 7 ),
		 ms_water_one, false );
    break;

  case ms_row_two:
    if (!m_farm->RowCultivation( m_field, 0.0,
         g_date->DayInYear( 20, 6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_row_two, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 7 ),
		 ms_water_one, false );
    break;

  case ms_water_one:
    if ( m_ev->m_lock || m_farm->DoIt( 30 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 15, 7 ) - g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, ms_water_one, false );
	break;
      }
    }
    {
      d1 = g_date->OldDays() + g_date->DayInYear( 16, 7 );
      if ( g_date->Date() + 7 > d1 ) {
	d1 = g_date->Date() + 7;
      }
      SimpleEvent( d1, ms_water_two, false );
    }
    break;

  case ms_water_two:
    if (!m_farm->Water( m_field, 0.0,
         g_date->DayInYear( 30, 7 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, ms_water_two, false );
      break;
    }
	ChooseNextCrop (2);
	//*that's it
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1, 9 ),
		 ms_harvest, false );
    break;

  case ms_harvest:
   if (m_field->GetMConstants(0)==0) {
		if (!m_farm->Harvest( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "MaizeSilage::Do(): failure in 'Harvest' execution", "" );
			exit( 1 );
		} 
	}
	else {  
		if (!m_farm->Harvest( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, ms_harvest, false );
			break;
		}
	}
    {
      d1 = g_date->OldDays() + m_field->GetMDates(0,1);
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      SimpleEvent( d1, ms_stubble, false );
    }
    break;

  case ms_stubble:
    if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
        if (m_field->GetMConstants(1)==0) {
			if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "MaizeSilage::Do(): failure in 'StubbleHarrowing' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, ms_stubble, false );
				break;
			}
		}
	}
    d1=g_date->DayInYear();
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "MaizeSilage::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


