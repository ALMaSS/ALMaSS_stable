/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PLBeetSpr.cpp This file contains the source for the PLBeetSpr class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PLBeetSpr.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PLBeetSpr.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_beet_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_BES_InsecticideDay;
extern CfgInt   cfg_BES_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional beet.
*/
bool PLBeetSpr::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case pl_bes_start:
	{
		// pl_bes_start just sets up all the starting conditions and reference dates that are needed to start a pl_sw
		PL_BES_SPRING_FERTI = 0;
		PL_BES_WATER_DATE = 0;
		PL_BES_HERBI_DATE = 0;
		PL_BES_HERBI1 = false;
		PL_BES_HERBI3 = false;

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 2;
		a_field->SetMDates(0, 0, g_date->DayInYear(30, 12)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(30, 12)); //  end day of calcium application
		a_field->SetMDates(0, 1, 0);
		a_field->SetMDates(1, 1, g_date->DayInYear(30, 12));

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = g_landscape_p->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousCrop(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousCrop(a_field->GetRotIndex());
						int almassnum = g_landscape_p->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), pl_bes_spring_plough, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.


		d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		if (g_date->Date() >= d1) d1 += 365;
		SimpleEvent_(d1, pl_bes_spring_plough, false, a_farm, a_field);
		break;
	}
	break;

	// This is the first real farm operation
	case pl_bes_spring_plough:
		if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(30, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_plough, true, a_farm, a_field);
			break;
		}

		SimpleEvent_(g_date->Date() + 1, pl_bes_spring_harrow, false, a_farm, a_field);
		break;
	case pl_bes_spring_harrow:
		if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_harrow, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(15, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_bes_ferti_s4, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, pl_bes_ferti_p4, false, a_farm, a_field);
		break;
	case pl_bes_ferti_p4:
		if (a_ev->m_lock || a_farm->DoIt(98))
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p4, true, a_farm, a_field);
				break;
			}
			PL_BES_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p5, false, a_farm, a_field);
		break;
	case pl_bes_ferti_s4:
		if (a_ev->m_lock || a_farm->DoIt(98))
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s4, true, a_farm, a_field);
				break;
			}
			PL_BES_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s5, false, a_farm, a_field);
		break;
	case pl_bes_ferti_p5:
		if (a_ev->m_lock || a_farm->DoIt(25))
		{
			if (!a_farm->FP_PK(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p5, true, a_farm, a_field);
				break;
			}
			PL_BES_SPRING_FERTI = true;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_heavy_cultivator, false, a_farm, a_field);
		break;
	case pl_bes_ferti_s5:
		if (a_ev->m_lock || a_farm->DoIt(25))
		{
			if (!a_farm->FA_PK(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s5, true, a_farm, a_field);
				break;
			}
			PL_BES_SPRING_FERTI = 1;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_heavy_cultivator, false, a_farm, a_field);
		break;
	case pl_bes_heavy_cultivator:
		if (PL_BES_SPRING_FERTI == 1)
		{
			if (!a_farm->HeavyCultivatorAggregate(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_heavy_cultivator, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, pl_bes_preseeding_cultivator, false, a_farm, a_field);
		break;
	case pl_bes_preseeding_cultivator:
		// 90% will do preseeding cultivation, the rest will do it together with sow
		if (a_ev->m_lock || a_farm->DoIt(90))
		{
			if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(19, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_preseeding_cultivator, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_sow, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_bes_preseeding_cultivator_sow, false, a_farm, a_field);
		break;
	case pl_bes_spring_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_spring_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, pl_bes_harrow_before_emergence, false, a_farm, a_field); // Harrowing before emergence, followed by thinning after emergence
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_watering1, false, a_farm, a_field);	// Watering thread
		SimpleEvent_(g_date->Date() + 5, pl_bes_herbicide1, false, a_farm, a_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pl_bes_fungicide1, false, a_farm, a_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_bes_insecticide, false, a_farm, a_field);	// Insecticide thread
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), pl_bes_ferti_s6, false, a_farm, a_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), pl_bes_ferti_p6, false, a_farm, a_field);
		break;
	case pl_bes_preseeding_cultivator_sow:
		// 10% will do preseeding cultivation with sow
		if (!a_farm->PreseedingCultivatorSow(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_preseeding_cultivator_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, pl_bes_harrow_before_emergence, false, a_farm, a_field); // Harrowing before emergence, followed by thinning after emergence
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_watering1, false, a_farm, a_field);	// Watering thread
		SimpleEvent_(g_date->Date() + 5, pl_bes_herbicide1, false, a_farm, a_field);	// Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pl_bes_fungicide1, false, a_farm, a_field);	// Fungicide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), pl_bes_insecticide, false, a_farm, a_field);	// Insecticide thread
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_ferti_s6, false, a_farm, a_field); // Fertilizers thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_bes_ferti_p6, false, a_farm, a_field);
		break;
	case pl_bes_harrow_before_emergence:
		if (a_field->GetGreenBiomass() <= 0)
		{
			if (a_ev->m_lock || a_farm->DoIt(3))
			{
				if (!a_farm->SpringHarrow(a_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_harrow_before_emergence, true, a_farm, a_field);
					break;
				}
			}
			SimpleEvent_(g_date->Date() + 5, pl_bes_thinning, false, a_farm, a_field);
			break;
		}
		else {
			SimpleEvent_(g_date->Date() + 1, pl_bes_thinning, false, a_farm, a_field);
		}
		break;
	case pl_bes_thinning:
		if (a_ev->m_lock || a_farm->DoIt(5))
		{
			if (a_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_thinning, true, a_farm, a_field);
			}
			else
			{
				if (!a_farm->RowCultivation(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_thinning, true, a_farm, a_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case pl_bes_watering1:
		if (a_ev->m_lock || a_farm->DoIt(3))
		{
			if (a_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_watering1, true, a_farm, a_field);
			}
			else
			{
				if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_watering1, true, a_farm, a_field);
					break;
				}
				PL_BES_WATER_DATE = g_date->Date();
			}
			d1 = g_date->Date() + 7;
			if (d1 < g_date->OldDays() + g_date->DayInYear(1, 6)) {
				d1 = g_date->OldDays() + g_date->DayInYear(1, 6);
			}
			SimpleEvent_(d1, pl_bes_watering2, false, a_farm, a_field);
			break;
		}
		break;

	case pl_bes_watering2:
		if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_watering2, true, a_farm, a_field);
			break;
		}
		PL_BES_WATER_DATE = g_date->Date();
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 7)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
		}
		SimpleEvent_(d1, pl_bes_watering3, false, a_farm, a_field);
		break;
	case pl_bes_watering3:
		if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(31, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_watering3, true, a_farm, a_field);
			break;
		}
		PL_BES_WATER_DATE = g_date->Date();
		// End of thread
		break;
	case pl_bes_ferti_p6:
		// Here comes fertilizers thread
		if (a_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p6, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p6, true, a_farm, a_field);
				break;
			}
			d1 = g_date->Date() + 1;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 5);
			}
			SimpleEvent_(d1, pl_bes_ferti_p7, false, a_farm, a_field);
			break;
		}
		break;
	case pl_bes_ferti_s6:
		if (a_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s6, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s6, true, a_farm, a_field);
				break;
			}
			d1 = g_date->Date() + 1;
			if (d1 < g_date->OldDays() + g_date->DayInYear(20, 5)) {
				d1 = g_date->OldDays() + g_date->DayInYear(20, 5);
			}
			SimpleEvent_(d1, pl_bes_ferti_s7, false, a_farm, a_field);
			break;
		}
		break;
	case pl_bes_ferti_p7:
		if (a_ev->m_lock || a_farm->DoIt(89))
		{
			if (!a_farm->FP_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p7, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case pl_bes_ferti_s7:
		if (a_ev->m_lock || a_farm->DoIt(89))
		{
			if (!a_farm->FA_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s7, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case pl_bes_herbicide1: // The first of the pesticide managements.
						   // Here comes the herbicide thread
						   // We assume that farmers do max 3 herbicide treatments
		if (a_field->GetGreenBiomass() <= 0)
		{
			if (a_ev->m_lock || a_farm->DoIt(43))
			{
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(25, 4) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide1, true, a_farm, a_field);
					break;
				}
				PL_BES_HERBI1 = true;
				PL_BES_HERBI_DATE = g_date->Date();
			}
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), pl_bes_herbicide2, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), pl_bes_herbicide2, false, a_farm, a_field);
		break;
	case pl_bes_herbicide2:
		if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(31, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide2, true, a_farm, a_field);
			break;
		}
		PL_BES_HERBI_DATE = g_date->Date();
		SimpleEvent_(g_date->Date() + 10, pl_bes_herbicide3, false, a_farm, a_field);
		break;
	case pl_bes_herbicide3:
		if (a_ev->m_lock || a_farm->DoIt(93))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide3, true, a_farm, a_field);
				break;
			}
			PL_BES_HERBI_DATE = g_date->Date();
			PL_BES_HERBI3 = true;
		}
		SimpleEvent_(g_date->Date() + 10, pl_bes_herbicide4, false, a_farm, a_field);
		break;
	case pl_bes_herbicide4:
		if (a_ev->m_lock || (a_farm->DoIt(81) && (PL_BES_HERBI1 == false || PL_BES_HERBI3 == false))) // which gives 46% of all farmers (and 81% of those who did not apply herbicides before beet emergence)
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_herbicide4, true, a_farm, a_field);
				break;
			}
			PL_BES_HERBI_DATE = g_date->Date();
		}
		// End of thread
		break;
	case pl_bes_fungicide1:
		// Here comes the fungicide thread
		if (a_ev->m_lock || a_farm->DoIt(83))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_fungicide1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, pl_bes_fungicide2, false, a_farm, a_field);
		break;
	case pl_bes_fungicide2:
		if (a_ev->m_lock || a_farm->DoIt(63))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_bes_fungicide2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 9), pl_bes_harvest, false, a_farm, a_field);
		break;
	case pl_bes_insecticide:
		// Here comes the insecticide thread

		if (PL_BES_HERBI_DATE >= g_date->Date() - 2) { // Should by at least 3 days after herbicide
			SimpleEvent_(g_date->Date() + 1, pl_bes_insecticide, false, a_farm, a_field);
		}
		else
		{
			if (a_ev->m_lock || a_farm->DoIt(58))
			{
				// here we check wheter we are using ERA pesticide or not
				if (!cfg_pest_beet_on.value() ||
					!g_landscape_p->SupplyShouldSpray()) // Not using pesticide spray
				{
					if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, pl_bes_insecticide, true, a_farm, a_field);
						break;
					}
				}
				else {
					a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
			}
		}
		// End of thread
		break;
	case pl_bes_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_bes_harvest, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s8, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p8, false, a_farm, a_field);
		break;

	case pl_bes_ferti_p8:
		if (a_ev->m_lock || a_farm->DoIt(28))
		{
			if (a_field->GetMConstants(3) == 0) {
				if (!a_farm->FP_Calcium(a_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): failure in 'FP_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->FP_Calcium(a_field, 0.0, a_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_p8, true, a_farm, a_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case pl_bes_ferti_s8:
		if (a_ev->m_lock || a_farm->DoIt(28))
		{
			if (a_field->GetMConstants(3) == 0) {
				if (!a_farm->FA_Calcium(a_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): failure in 'FA_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->FA_Calcium(a_field, 0.0, a_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, pl_bes_ferti_s8, true, a_farm, a_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "PLBeetSpr::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}