//
// SpringBarleySKManagement.h
//
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef SpringBarleySKManagement_H
#define SpringBarleySKManagement_H

#define SBARLEYSKM_BASE 4750
#define SBSKM_SLURRY_DONE       m_field->m_user[0]
#define SBSKM_MANURE_DONE       m_field->m_user[1]
#define SBSKM_SLURRY_EXEC       m_field->m_user[2]
#define SBSKM_MANURE_EXEC       m_field->m_user[3]
#define SBSKM_DID_AUTUMN_PLOUGH m_field->m_user[4]

#define SBSKM_HERBI_DATE        m_field->m_user[0]
#define SBSKM_GR_DATE           m_field->m_user[1]
#define SBSKM_FUNGI_DATE        m_field->m_user[2]
#define SBSKM_WATER_DATE        m_field->m_user[3]
#define SBSKM_INSECT_DATE       m_field->m_user[4]

typedef enum {
  sbskm_start = 1, // Compulsory, start event must always be 1 (one).
  sbskm_autumn_plough = SBARLEYSKM_BASE,
  sbskm_fertslurry_stock,
  sbskm_fertmanure_stock_one,
  sbskm_spring_plough,
  sbskm_spring_harrow,
  sbskm_fertmanure_plant,
  sbskm_fertlnh3_plant,
  sbskm_fertpk_plant,
  sbskm_fertmanure_stock_two,
  sbskm_fertnpk_stock,
  sbskm_spring_sow,
  sbskm_spring_roll,
  sbskm_herbicide_one,
  sbskm_herbicide_two,
  sbskm_GR,
  sbskm_fungicide_one,
  sbskm_insecticide,
  sbskm_fungicide_two,
  sbskm_water_one,
  sbskm_water_two,
  sbskm_harvest,
  sbskm_straw_chopping,
  sbskm_hay_baling,
  sbskm_stubble_harrow
} SBSKMToDo;



class SpringBarleySKManagement: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  SpringBarleySKManagement()
  {
      m_first_date=g_date->DayInYear(2,11);
  }
};

#endif // SpringBarleySKManagement_H

