//
// WinterBarley.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/WinterBarleyStrigling.h"
#include <math.h>

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgFloat cfg_strigling_prop;

bool WinterBarleyStrigling::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case wbs_start:
    {
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(10,8);
      // Start and stop dates for all events after harvest
      int noDates= 3;
      m_field->SetMDates(0,0,g_date->DayInYear(20,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(5,8));
      m_field->SetMDates(0,1,g_date->DayInYear(25,7));
      m_field->SetMDates(1,1,g_date->DayInYear(10,8));
      m_field->SetMDates(0,2,g_date->DayInYear(20,7));
      m_field->SetMDates(1,2,g_date->DayInYear(5,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      int d1;
      if ( ! m_ev->m_first_year )
      {
	int today=g_date->Date();
        // Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (today < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
        // Is the first year so must start in spring like nothing was unusual
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 )
                                                     ,wbs_harvest, false );
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      WBS_DID_SEVEN_ONE   = false;
      WBS_DID_SEVEN_TWO   = false;
      WBS_DID_SEVEN_THREE = false;
      WBS_DID_SEVEN_FOUR  = false;
      WBS_FUNGICIDE_DATE  = 0;
      WBS_HERBICIDE_DATE  = 0;

      if ( m_farm->IsStockFarmer()) {
	SimpleEvent( d1, wbs_fertmanure_stock, false );
	d1 += 14;
	SimpleEvent( d1, wbs_fertslurry_stock_one, false );
      } else {
	SimpleEvent( d1, wbs_fertsludge_plant_one, false );
      }
    }
    break;

  case wbs_fertmanure_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 5, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wbs_fertmanure_stock, true );
        break;
      }
    }
    break;

  case wbs_fertslurry_stock_one:
    if ( m_ev->m_lock || m_farm->DoIt( 95 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 10, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wbs_fertslurry_stock_one, true );
        break;
      }
    }
    SimpleEvent( g_date->Date() + 1, wbs_autumn_plough, false );
    break;

  case wbs_fertsludge_plant_one:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->FP_Sludge( m_field, 0.0,
			      g_date->DayInYear( 5, 9 ) -
			      g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wbs_fertsludge_plant_one, true );
        break;
      }
    }
    SimpleEvent( g_date->Date() + 1, wbs_autumn_plough, false );
    break;

  case wbs_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 5, 9 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wbs_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), wbs_autumn_harrow, false );
    break;

  case wbs_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 5, 9 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wbs_autumn_harrow, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1, 9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 9 );
      }
      SimpleEvent( d1, wbs_autumn_sow, false );
    }
    break;

  case wbs_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 15,9 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wbs_autumn_sow, true );
      break;
    }
    {
      int d1 = g_date->Date() + 10;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 15, 9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 15, 9 );
      }
      SimpleEvent( d1, wbs_strigling_one, false );
    }
    break;

case wbs_strigling_one:
  if (!m_farm->Strigling( m_field, 0.0,
                          g_date->DayInYear( 10,10 ) -
                          g_date->DayInYear())) {
    SimpleEvent( g_date->Date() + 1, wbs_strigling_one, true );
    break;
  }
  if ( m_farm->IsStockFarmer()) {
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
                 wbs_fertslurry_stock_two, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365,
                 wbs_fertnpk_stock_one, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ) + 365,
                 wbs_fungicide_one, false );
  } else {
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16,9 ),
                 wbs_fertmanganese_plant_one, false );
  }
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365,
                 wbs_strigling_two, false );
  break;

  case wbs_fertmanganese_plant_one:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
					 g_date->DayInYear( 30,10 ) -
					 g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_fertmanganese_plant_one, true );
	break;
      }
      // Did first, so do second manganese too.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ) + 365,
		   wbs_fertmanganese_plant_two, false );
    } else {
      // Event 'wbs_fertmanganese_plant_two' was not queued up, so we will
      // signal its completion already here...
      WBS_DID_SEVEN_ONE = true;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ) + 365,
		 wbs_fertnpk_plant_one, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ) + 365,
		 wbs_fungicide_one, false );
    break;

  case wbs_fertmanganese_plant_two:
    if (!m_farm->FP_ManganeseSulphate( m_field, 0.0,
				       g_date->DayInYear( 5,5 ) -
				       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wbs_fertmanganese_plant_two, true );
      break;
    }
    WBS_DID_SEVEN_ONE = true;
    if ( WBS_DID_SEVEN_TWO &&
	 WBS_DID_SEVEN_THREE &&
	 WBS_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wbs_switchboard, false );
    }
    break;

  case wbs_fertnpk_plant_one:
    if (!m_farm->FP_NPK( m_field, 0.0,
			 g_date->DayInYear( 1,5 ) -
			 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wbs_fertnpk_plant_one, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 2,5 ),
		   wbs_fertnpk_plant_two, false );
    break;

  case wbs_fertnpk_plant_two:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
      if (!m_farm->FP_NPK( m_field, 0.0,
			   g_date->DayInYear( 20,5 ) -
			   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_fertnpk_plant_two, true );
	break;
      }
    }
    WBS_DID_SEVEN_TWO = true;
    if ( WBS_DID_SEVEN_ONE &&
	 WBS_DID_SEVEN_THREE &&
	 WBS_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wbs_switchboard, false );
    }
    break;

  case wbs_fertslurry_stock_two:
    if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 15,5 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_fertslurry_stock_two, true );
	break;
      }
    }
    WBS_DID_SEVEN_ONE = true;
    if ( WBS_DID_SEVEN_TWO &&
	 WBS_DID_SEVEN_THREE &&
	 WBS_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wbs_switchboard, false );
    }
    break;

  case wbs_fertnpk_stock_one:
    if ( m_ev->m_lock || m_farm->DoIt( 90 )) {
      if (!m_farm->FA_NPK( m_field, 0.0,
			   g_date->DayInYear( 30,4 ) -
			   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_fertnpk_stock_one, true );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
		   wbs_fertnpk_stock_two, false );
    break;

  case wbs_fertnpk_stock_two:
    if ( m_ev->m_lock || m_farm->DoIt( 14 )) {
      if (!m_farm->FA_NPK( m_field, 0.0,
			   g_date->DayInYear( 15,5 ) -
			   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_fertnpk_stock_two, true );
	break;
      }
    }
    WBS_DID_SEVEN_TWO = true;
    if ( WBS_DID_SEVEN_ONE &&
	 WBS_DID_SEVEN_THREE &&
	 WBS_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wbs_switchboard, false );
    }
    break;

  case wbs_strigling_two:
    if ( m_ev->m_lock || m_farm->DoIt( (int) ((int) (60*cfg_herbi_app_prop.value() )) )) {
      if (!m_farm->HerbicideTreat( m_field, 0.0,
				   g_date->DayInYear( 25,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_strigling_two, true );
	break;
      }
      WBS_HERBICIDE_DATE = g_date->Date();
    }
    WBS_DID_SEVEN_THREE = true;
    if ( WBS_DID_SEVEN_ONE &&
	 WBS_DID_SEVEN_TWO &&
	 WBS_DID_SEVEN_FOUR ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wbs_switchboard, false );
    }
    break;

  case wbs_fungicide_one:
    if ( m_ev->m_lock || m_farm->DoIt( (int) (90*cfg_fungi_app_prop1.value() ))) {
      if (!m_farm->FungicideTreat( m_field, 0.0,
				   g_date->DayInYear( 15,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_fungicide_one, true );
	break;
      }
      WBS_FUNGICIDE_DATE = g_date->Date();
    }
    WBS_DID_SEVEN_FOUR = true;
    if ( WBS_DID_SEVEN_ONE &&
	 WBS_DID_SEVEN_TWO &&
	 WBS_DID_SEVEN_THREE ) {
      // We are the last surviving thread. Enter switchboard.
      SimpleEvent( g_date->Date(), wbs_switchboard, false );
    }
    break;

  case wbs_switchboard:
    // Welcome to the Winterbarley switchboard, where all threads
    // meet and have a jolly good time.

    // Reset all flags for the second round of parallel execution.
    // Flags double as date indicators when each action took place
    // (for checking proximity to watering).
    WBS_DID_EIGHT_ONE   = false;
    WBS_DID_EIGHT_TWO   = false;
    WBS_DID_EIGHT_THREE = false;
    WBS_DID_EIGHT_FOUR  = false;

    // Start all four threads.
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		   wbs_fungicide_two, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
		   wbs_growth_reg, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		   wbs_water, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
		   wbs_insecticide, false );
    break;

  case wbs_fungicide_two:
    if ( g_date->Date() < WBS_DID_EIGHT_THREE + 1 ||
	 g_date->Date() < WBS_FUNGICIDE_DATE + 7 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wbs_fungicide_two, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (27*cfg_fungi_app_prop1.value() ))) {
      if (!m_farm->FungicideTreat( m_field, 0.0,
				   g_date->DayInYear( 30,5 ) -
				   g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_fungicide_two, true );
	break;
      }
    }
    WBS_DID_EIGHT_ONE = g_date->Date();
    if ( WBS_DID_EIGHT_TWO &&
	 WBS_DID_EIGHT_THREE &&
	 WBS_DID_EIGHT_FOUR ) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
		   wbs_harvest, false );
    }
    break;

  case wbs_growth_reg:
    if ( g_date->Date() < WBS_DID_EIGHT_THREE + 1 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wbs_growth_reg, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (50*cfg_greg_app_prop.value() ))) {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
				    g_date->DayInYear( 31,5 ) -
				    g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_growth_reg, true );
	break;
      }
    }
    WBS_DID_EIGHT_TWO = g_date->Date();
    if ( WBS_DID_EIGHT_ONE &&
	 WBS_DID_EIGHT_THREE &&
	 WBS_DID_EIGHT_FOUR ) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
		   wbs_harvest, false );
    }
    break;

  case wbs_water:
    if ( g_date->Date() < WBS_HERBICIDE_DATE + 1 ||
	 g_date->Date() < WBS_DID_EIGHT_ONE  + 1 ||
	 g_date->Date() < WBS_DID_EIGHT_TWO  + 1 ||
	 g_date->Date() < WBS_DID_EIGHT_FOUR + 1 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wbs_water, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 5 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 15,6 ) -
			  g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_water, true );
	break;
      }
    }
    WBS_DID_EIGHT_THREE = g_date->Date();
    if ( WBS_DID_EIGHT_ONE &&
	 WBS_DID_EIGHT_TWO &&
	 WBS_DID_EIGHT_FOUR ) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
		   wbs_harvest, false );
    }
    break;

  case wbs_insecticide:
    if ( g_date->Date() < WBS_DID_EIGHT_THREE + 1 ) {
      // Try again tomorrow.
      SimpleEvent( g_date->Date() + 1, wbs_insecticide, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) (5*cfg_ins_app_prop1.value() ))) {
      if (!m_farm->InsecticideTreat( m_field, 0.0,
				     g_date->DayInYear( 1,6 ) -
				     g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_insecticide, true );
	break;
      }
    }
    WBS_DID_EIGHT_FOUR = g_date->Date();
    if ( WBS_DID_EIGHT_ONE &&
	 WBS_DID_EIGHT_TWO &&
	 WBS_DID_EIGHT_THREE ) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,7 ),
		   wbs_harvest, false );
    }
    break;

  case wbs_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  m_field->GetMDates(1,0) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wbs_harvest, true );
      break;
    }

    if ( m_farm->IsStockFarmer()) {
      if ( m_farm->DoIt( 15 )) {
	// Force straw chopping on the same day as harvest.
	m_farm->StrawChopping( m_field, 0.0, 0 );
	SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
		   wbs_stubble_harrowing, false );
	break;
      }
      // No chopping and harrowing, so try hay turning.
      SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
		   wbs_hay_turning, false );
      break;
    } else {
      // Plant farmer.
      if ( m_farm->DoIt( 75 )) {
	// Force straw chopping on the same day as harvest.
	m_farm->StrawChopping( m_field, 0.0, 0 );
	SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
		     wbs_stubble_harrowing, false );
	break;
      }
      SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
		   wbs_hay_turning, false );
    }
    break;

  case wbs_hay_turning:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->HayTurning( m_field, 0.0,
			       m_field->GetMDates(1,2) -
			       g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, wbs_hay_turning, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
		 wbs_hay_bailing, false );
    break;

  case wbs_hay_bailing:
    if (!m_farm->HayBailing( m_field, 0.0,
			     m_field->GetMDates(1,2) -
			     g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, wbs_hay_bailing, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + m_field->GetMDates(0,1)) {
	d1 = g_date->OldDays() + m_field->GetMDates(0,1);
      }
      SimpleEvent( d1, wbs_stubble_harrowing, false );
    }
    break;

  case wbs_stubble_harrowing:
    if ( m_ev->m_lock || m_farm->DoIt( 40 )) {
      if (!m_farm->StubbleHarrowing( m_field, 0.0,
				     m_field->GetMDates(1,2) -
				     g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, wbs_stubble_harrowing, true );
	break;
      }
    }
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "WinterBarley::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


