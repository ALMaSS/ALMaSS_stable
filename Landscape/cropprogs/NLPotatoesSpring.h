/**
\file
\brief
<B>NLPotatoesSpring.h This file contains the headers for the PotatoesSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLPotatoesSpring.h
//


#ifndef NLPOTATOESSPRING_H
#define NLPOTATOESSPRING_H

#define NLPOTATOESSPRING_BASE 26500
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_POTS_HERBI		a_field->m_user[1]
#define NL_POTS_FUNGI1		a_field->m_user[2]
#define NL_POTS_FUNGI2		a_field->m_user[3]
#define NL_POTS_FUNGI3		a_field->m_user[4]
#define NL_POTS_FUNGI4		a_field->m_user[5]
#define NL_POTS_FUNGI5		a_field->m_user[6]

/** Below is the list of things that a farmer can do if he is growing potatoes, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_pots_start = 1, // Compulsory, must always be 1 (one).
	nl_pots_sleep_all_day = NLPOTATOESSPRING_BASE,
	nl_pots_ferti_p2_clay,
	nl_pots_ferti_s2_clay,
	nl_pots_spring_plough_sandy,
	nl_pots_ferti_p2_sandy,
	nl_pots_ferti_s2_sandy,
	nl_pots_bed_forming,
	nl_pots_spring_planting,
	nl_pots_hilling1,
	nl_pots_ferti_p3_clay,
	nl_pots_ferti_s3_clay,
	nl_pots_ferti_p3_sandy,
	nl_pots_ferti_s3_sandy,
	nl_pots_ferti_p4,
	nl_pots_ferti_s4,
	nl_pots_herbicide1,
	nl_pots_herbicide2,
	nl_pots_fungicide1,
	nl_pots_fungicide2,
	nl_pots_fungicide3,
	nl_pots_fungicide4,
	nl_pots_fungicide5,
	nl_pots_fungicide6,
	nl_pots_fungicide7,
	nl_pots_fungicide8,
	nl_pots_fungicide9,
	nl_pots_fungicide10,
	nl_pots_fungicide11,
	nl_pots_fungicide12,
	nl_pots_fungicide13,
	nl_pots_fungicide14,
	nl_pots_fungicide15,
	nl_pots_insecticide,
	nl_pots_dessication1,
	nl_pots_dessication2,
	nl_pots_harvest,
} NLPotatoesSpringToDo;


/**
\brief
NLPotatoesSpring class
\n
*/
/**
See NLPotatoesSpring.h::NLPotatoesSpringToDo for a complete list of all possible events triggered codes by the potatoes management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLPotatoesSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLPotatoesSpring()
   {
		// When we start it off, the first possible date for a farm operation is 5th November
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 30,4 );
   }
};

#endif // NLPOTATOESSPRING_H

