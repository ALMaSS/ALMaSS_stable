/**
\file
\brief
<B>PLFodderLucerne1.h This file contains the headers for the FodderLucerne1 class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of January 2018 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// PLFodderLucerne1.h
//


#ifndef PLFODDERLUCERNE1_H
#define PLFODDERLUCERNE1_H

#define PLFODDERLUCERNE1_BASE 21100
/**
\brief A flag used to indicate autumn ploughing status
*/
#define PL_FL1_FERTI_P1	a_field->m_user[1]
#define PL_FL1_FERTI_S1	a_field->m_user[2]
#define PL_FL1_STUBBLE_PLOUGH	a_field->m_user[3]
#define PL_FL1_WINTER_PLOUGH	a_field->m_user[4]
#define PL_FL1_SPRING_FERTI a_field->m_user[5]
#define PL_FL1_HERBICIDE1	a_field->m_user[6]
#define PL_FL1_HERBI_DATE	a_field->m_user[7]

/** Below is the list of things that a farmer can do if he is growing fodder lucerne in the first year, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	pl_fl1_start = 1, // Compulsory, must always be 1 (one).
	pl_fl1_sleep_all_day = PLFODDERLUCERNE1_BASE,
	pl_fl1_herbicide0,
	pl_fl1_ferti_p1, 
	pl_fl1_ferti_s1,
	pl_fl1_stubble_plough,
	pl_fl1_autumn_harrow1,
	pl_fl1_autumn_harrow2,
	pl_fl1_stubble_harrow,
	pl_fl1_ferti_p2,
	pl_fl1_ferti_s2,
	pl_fl1_ferti_p3,
	pl_fl1_ferti_s3,	
	pl_fl1_winter_plough,
	pl_fl1_winter_stubble_cultivator_heavy,
	pl_fl1_spring_harrow,
	pl_fl1_ferti_p4,
	pl_fl1_ferti_s4,
	pl_fl1_heavy_cultivator,
	pl_fl1_herbicide1,
	pl_fl1_preseeding_cultivator,	
	pl_fl1_spring_sow,	
	pl_fl1_herbicide2,
	pl_fl1_herbicide3,
	pl_fl1_cut_to_silage1,
	pl_fl1_cut_to_silage2,
} PLFodderLucerne1ToDo;


/**
\brief
PLFodderLucerne1 class
\n
*/
/**
See PLFodderLucerne1.h::PLFodderLucerne1ToDo for a complete list of all possible events triggered codes by the fodder lucerne1 management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class PLFodderLucerne1: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   PLFodderLucerne1()
   {
		// When we start it off, the first possible date for a farm operation is 31th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 31,10 );
   }
};

#endif // PLFODDERLUCERNE1_H

