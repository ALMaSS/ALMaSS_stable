//
// NorwegianSpringBarley.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NorwegianSpringBarley.h"
#include "math.h"


bool NorwegianSpringBarley::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	int d1;

	bool done = false;

	switch (m_ev->m_todo)
	{
	case no_sb_start:
	{

		// Set up the date management stuff
		// Could save the start day in case it is needed later
		// m_field->m_startday = m_ev->m_startday;
		m_last_date = g_date->DayInYear(30, 8);
		// Start and stop dates for all events after harvest
		int noDates = 2;
		m_field->SetMDates(0, 0, g_date->DayInYear(20, 8));
		// Determined by harvest date - used to see if at all possible
		m_field->SetMDates(1, 0, g_date->DayInYear(10, 8));
		m_field->SetMDates(0, 1, g_date->DayInYear(10, 8));
		m_field->SetMDates(1, 1, g_date->DayInYear(30, 8));
		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary

		if (m_ev->m_startday > g_date->DayInYear(1, 7))
		{
			if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
			{
				g_msg->Warn(WARN_BUG, "NorwegianSpringBarley::Do(): "
					"Harvest too late for the next crop to start!!!", "");
				exit(1);
			}
			// Now fix any late finishing problems
			for (int i = 0; i < noDates; i++) {
				if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
					m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
				}
				if (m_field->GetMDates(1, i) >= m_ev->m_startday){
					m_field->SetMConstants(i, 0);
					m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
				}
			}
		}
		// Now no operations can be timed after the start of the next crop.

		if (!m_ev->m_first_year)
		{
			int today = g_date->Date();
			// Are we before July 1st?
			d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
			if (today < d1)
			{
				// Yes, too early. We assumme this is because the last crop was late
				g_msg->Warn(WARN_BUG, "NorwegianSpringBarley::Do(): " "Crop start attempt between 1st Jan & 1st July", "");
				exit(1);
			}
			else
			{
				d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
				if (today > d1)
				{
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "NorwegianSpringBarley::Do(): " "Crop start attempt after last possible start date", "");
					exit(1);
				}
			}
		}
		else
		{
			SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), no_sb_spring_plough, false);
			break;
		}


		// End single block date checking code. Please see next line
		// comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(2, 11);
		if (g_date->Date() > d1) {
			d1 = g_date->Date();
		}
		// OK, let's go.
		SimpleEvent(d1, no_sb_autumn_plough, false);
	}
	break;

	case no_sb_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt(70)) // was 70
		{
			if (!m_farm->AutumnPlough(m_field, 0.0,
				g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, no_sb_autumn_plough, true);
				break;
			}
			NO_SB_DID_AUTUMN_PLOUGH = true;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(20, 3) + 365, no_sb_spring_plough, false);
		break;

	case no_sb_spring_plough:
		if (!NO_SB_DID_AUTUMN_PLOUGH)
		{
			if (!m_farm->SpringPlough(m_field, 0.0,
				g_date->DayInYear(10, 4) - // was 10,4
				g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, no_sb_spring_plough, true);
				break;
			}
		}
	{
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 3);
		}
		SimpleEvent(d1, no_sb_spring_harrow, false);
	}
	break;

	case no_sb_spring_harrow:
		if (!m_farm->SpringHarrow(m_field, 0.0,
			g_date->DayInYear(10, 4) - g_date->DayInYear())) {  // WAS 10,4
			SimpleEvent(g_date->Date() + 1, no_sb_spring_harrow, true);
			break;
		}
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(25, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(25, 3);
		}
		SimpleEvent(d1, no_sb_spring_sow, false);
		break;

	case no_sb_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0,
			g_date->DayInYear(10, 4) - g_date->DayInYear())) {   // WAS 10,4
			SimpleEvent(g_date->Date() + 1, no_sb_spring_sow, true);
			break;
		}
	{
		d1 = g_date->Date();
		if (d1 < g_date->OldDays() + g_date->DayInYear(5, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(5, 4);
		}
		SimpleEvent(d1, no_sb_spring_roll, false);
	}
	break;

	case no_sb_spring_roll:
		if (m_ev->m_lock || m_farm->DoIt(0)) // was 30
		{
			if (!m_farm->SpringRoll(m_field, 0.0,
				g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, no_sb_spring_roll, true);
				break;
			}
		}
		ChooseNextCrop(2);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), no_sb_harvest, false);
		break;


	case no_sb_harvest:
		if (!m_farm->Harvest(m_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, no_sb_harvest, true);
			break;
		}
		SimpleEvent(g_date->Date(),no_sb_haybailing, false);
		break;

	case no_sb_haybailing:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->HayBailing(m_field, 0.0, -1)) { //raise an error
				g_msg->Warn(WARN_BUG, "NOSpringBarley::Do(): failure in 'HayBailing' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, no_sb_haybailing, true);
				break;
			}
		}
		// END MAIN THREAD
		done = true;
		break;

	default:
		g_msg->Warn(WARN_BUG, "NorwegianSpringBarley::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}


