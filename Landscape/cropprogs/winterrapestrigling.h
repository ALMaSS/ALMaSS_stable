//
// winterrapestrigling.h
//

#ifndef WINTERRAPESTRIGLING_H
#define WINTERRAPESTRIGLING_H

// <cropname>_BASE is the first event number to be dumped into the
// debugging log from this crop. *Must* be unique among all crops.
// I suggest steps of 100 between crops.

#define WINTERRAPESTRIGLING_BASE 30000
#define WR_DID_RC_CLEAN     m_field->m_user[0]
#define WR_DID_HERBI_ZERO   m_field->m_user[1]
#define WR_INSECT_DATE      m_field->m_user[2]
#define WR_FUNGI_DATE       m_field->m_user[3]
#define WR_SWARTH_DATE      m_field->m_user[4]

typedef enum {
  wrs_start = 1, // Compulsory, start event must always be 1 (one).
  wrs_ferti_zero = WINTERRAPESTRIGLING_BASE,
  wrs_autumn_plough,
  wrs_autumn_harrow,
  wrs_autumn_sow,
  wrs_strigling_one,
  wrs_strigling_two,
  wrs_strigling_three,
  wrs_strigling_threeb,
  wrs_ferti_p1,
  wrs_ferti_p2,
  wrs_ferti_s1,
  wrs_ferti_s2,
  wrs_fungi_one,
  wrs_insect_one,
  wrs_insect_one_b,
  wrs_insect_one_c,
  wrs_swarth,
  wrs_harvest,
  wrs_cuttostraw,
  wrs_compress,
  wrs_stub_harrow,
  wrs_grubbing
} WinterRapeStriglingToDo;



class WinterRapeStrigling: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   WinterRapeStrigling()
   {
      m_first_date=g_date->DayInYear(24,8);
   }
};

#endif // WINTERRAPE_H
