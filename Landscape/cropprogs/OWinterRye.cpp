//
// OWinterRye.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OWinterRye.h"

extern CfgFloat cfg_strigling_prop;

bool OWinterRye::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case owry_start:
    {
      // Start single block date checking code to be cut-'n-pasted...
      // Set up the date management stuff
      m_last_date=g_date->DayInYear(25,8);
      // Start and stop dates for all events after harvest
      int noDates= 2;
      m_field->SetMDates(0,0,g_date->DayInYear(6,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(15,8));
      m_field->SetMDates(0,1,g_date->DayInYear(20,8));
      m_field->SetMDates(1,1,g_date->DayInYear(25,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OWinterRye::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      int d1;
      if ( ! m_ev->m_first_year )
      {
	int today=g_date->Date();
        // Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (today < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, " OWinterRye::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, " OWinterRye::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear(1,8),
            owry_harvest, false );
         break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      if ( m_farm->IsStockFarmer()) {
	SimpleEvent( d1, owry_fertmanure_stock, false );
      } else {
	SimpleEvent( d1, owry_fertmanure_plant, false );
      }
    }
    break;

  case owry_fertmanure_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 50 )) {
      if (!m_farm->FA_Manure( m_field, 0.0,
			      g_date->DayInYear( 30, 9 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owry_fertmanure_stock, true );
	break;
      }
    }
    SimpleEvent( g_date->Date() +1, owry_fertslurry_stock, false );
    break;

  case owry_fertslurry_stock:
    if (!m_farm->FA_Slurry( m_field, 0.0,
			    g_date->DayInYear( 30, 9 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_fertslurry_stock, true );
      break;
    }
    SimpleEvent( g_date->Date(), owry_autumn_plough, false );
    break;

  case owry_fertmanure_plant:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->FP_Manure( m_field, 0.0,
			      g_date->DayInYear( 30, 9 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owry_fertmanure_plant, true );
	break;
      }
    }
    SimpleEvent( g_date->Date() + 1, owry_fertslurry_plant, false );
    break;

  case owry_fertslurry_plant:
    if (!m_farm->FP_Slurry( m_field, 0.0,
			    g_date->DayInYear( 30, 9 ) -
 			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_fertslurry_plant, true );
      break;
    }
    SimpleEvent( g_date->Date(), owry_autumn_plough, false );
    break;

  case owry_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 5, 10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), owry_autumn_harrow, false );
    break;

  case owry_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 10, 10 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_autumn_harrow, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1,9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 1,9 );
      }
      SimpleEvent( d1, owry_autumn_sow, false );
    }
    break;

  case owry_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
			    g_date->DayInYear( 15, 10 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_autumn_sow, true );
      break;
    }
    {
      int d1 = g_date->Date() + 10;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 11,9 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 11,9 );
      }
      SimpleEvent( d1, owry_strigling_one, false );
    }
    break;

  case owry_strigling_one:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 70 ))) {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 25, 10 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owry_strigling_one, true );
	break;
      }
    }
    {
      int d1 = g_date->Date() + 7;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 5,10 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 5,10 );
      }
      SimpleEvent( d1, owry_strigling_two, false );
    }
    break;

  case owry_strigling_two:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 15 ))) {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 2, 11 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owry_strigling_two, true );
	break;
      }
    }
    if ( m_farm->IsStockFarmer()) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ) + 365,
		   owry_strigling_three, false );
    } else {
      // Skip to spring sow.
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ) + 365,
		   owry_spring_sow, false );
    }
    break;

  case owry_spring_sow:
    if ( m_ev->m_lock || m_farm->DoIt( 60 )) {
      if (!m_farm->SpringSow( m_field, 0.0,
			      g_date->DayInYear( 15, 4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owry_spring_sow, true );
	break;
      }
      {
	// Did sow, so skip third strigling.
	int d1 = g_date->Date();
	if ( d1 < g_date->OldDays() + g_date->DayInYear( 10,4 )) {
	  d1 = g_date->OldDays() + g_date->DayInYear( 10,4 );
	}
	SimpleEvent( d1, owry_spring_roll, false );
	break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
		 owry_strigling_three, false );
    break;

  case owry_strigling_three:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 60 ))) {
      if (!m_farm->Strigling( m_field, 0.0,
			      g_date->DayInYear( 15, 4 ) -
			      g_date->DayInYear())) {
	SimpleEvent( g_date->Date() + 1, owry_strigling_three, true );
	break;
      }
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 10,4 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 10,4 );
      }
      SimpleEvent( d1, owry_spring_roll, false );
    }
    break;

  case owry_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->SpringRoll( m_field, 0.0,
			       g_date->DayInYear( 30,4 ) -
			       g_date->DayInYear())) {
        SimpleEvent( g_date->Date()+1, owry_spring_roll, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
		 owry_harvest, false );
    break;

  case owry_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
			  m_field->GetMDates(1,0) -
			  g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_harvest, true );
      break;
    }
    if ( m_farm->IsStockFarmer()) {
      SimpleEvent( g_date->Date(), owry_straw_chopping_stock, false );
    } else {
      SimpleEvent( g_date->Date(), owry_straw_chopping_plant, false );
    }
    break;

  case owry_straw_chopping_plant:
    if (!m_farm->StrawChopping( m_field, 0.0,
				m_field->GetMDates(1,0) -
				g_date->DayInYear())) {
      SimpleEvent( g_date->Date()+1, owry_straw_chopping_plant, true );
      break;
    }
    // End of program for plant farmers.
    done = true;
    break;

  case owry_straw_chopping_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 80 )) {
      if (!m_farm->StrawChopping( m_field, 0.0,
				  m_field->GetMDates(1,0) -
				  g_date->DayInYear())) {
        SimpleEvent( g_date->Date()+1, owry_straw_chopping_stock, true );
        break;
      }
      // Did chop, so stop here.
      done = true;
      break;
    }
    // No chopping, so do hay turning and bailing.
    SimpleEvent( g_date->Date(), owry_hay_turning, false );
    break;

  case owry_hay_turning:
    if (!m_farm->HayTurning( m_field, 0.0,
			     m_field->GetMDates(0,1) -
			     g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_hay_turning, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 5,8 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 5,8 );
      }
      SimpleEvent( d1, owry_hay_bailing, false );
    }
    break;

  case owry_hay_bailing:
    if (!m_farm->HayBailing( m_field, 0.0,
			     m_field->GetMDates(1,1) -
			     g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owry_hay_bailing, true );
        break;
      }
    // End of program.
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OWinterRye::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


