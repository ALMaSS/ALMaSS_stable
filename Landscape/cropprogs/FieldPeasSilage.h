//
// FieldPeasSilage.h
//copied from FieldPeas
/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef FieldPeasSilage_H
#define FieldPeasSilage_H

#define FPEASSI_BASE 5330

#define FPEADSI_INSECT_DATE m_field->m_user[0]
#define FPEADSI_FUNGI_DATE  m_field->m_user[1]
#define FPEADSI_WATER_DATE  m_field->m_user[2]
#define FPEASSI_DECIDE_TO_HERB  m_field->m_user[3]


typedef enum {
  fpsi_start = 1, // Compulsory, start event must always be 1 (one).
  fpsi_harvest= FPEASSI_BASE,
  fpsi_autumn_plough,
  fpsi_spring_plough,
  fpsi_spring_harrow,
  fpsi_fertmanure_plant,
  fpsi_fertmanure_stock,
  fpsi_spring_sow,
  fpsi_spring_roll,
  fpsi_herbicide_one,
  fpsi_herbicide_two,
  fpsi_insecticide,
  fpsi_fungicide,
  fpsi_water_one,
  fpsi_water_two,
  fpsi_growth_reg,
  fpsi_straw_chopping
} FieldPeasSilageToDo;



class FieldPeasSilage: public Crop
{
public:
  bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
  FieldPeasSilage()
  {
      m_first_date=g_date->DayInYear(1,10);
  }

};

#endif // FieldPeasSilage_H


