/**
\file
\brief
<B>NLCabbageSpring.h This file contains the headers for the CabbageSpring class</B> \n
*/
/**
\file 
 by Chris J. Topping \n
 modified by Elzbieta Ziolkowska \n
 Version of October 2017 \n
 All rights reserved. \n
 \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLCabbageSpring.h
//


#ifndef NLCABBAGESPRING_H
#define NLCABBAGESPRING_H

#define NLCABBAGESPRING_BASE 25700
/**
\brief A flag used to indicate autumn ploughing status
*/
#define NL_CABS_WINTER_PLOUGH	a_field->m_user[1]

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	nl_cabs_start = 1, // Compulsory, must always be 1 (one).
	nl_cabs_sleep_all_day = NLCABBAGESPRING_BASE,
	nl_cabs_spring_plough_sandy,
	nl_cabs_ferti_p1,
	nl_cabs_ferti_s1,
	nl_cabs_preseeding_cultivator,
	nl_cabs_spring_planting,
	nl_cabs_weeding1,
	nl_cabs_herbicide1,
	nl_cabs_weeding2,
	nl_cabs_fungicide1,
	nl_cabs_fungicide2,
	nl_cabs_fungicide3,
	nl_cabs_insecticide1,
	nl_cabs_insecticide2,
	nl_cabs_insecticide3,
	nl_cabs_ferti_p2,
	nl_cabs_ferti_s2,
	nl_cabs_ferti_p3,
	nl_cabs_ferti_s3,
	nl_cabs_ferti_p4,
	nl_cabs_ferti_s4,
	nl_cabs_watering,
	nl_cabs_harvest,
} NLCabbageSpringToDo;


/**
\brief
NLCabbageSpring class
\n
*/
/**
See NLCabbageSpring.h::NLCabbageSpringToDo for a complete list of all possible events triggered codes by the cabbage management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class NLCabbageSpring: public Crop
{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   NLCabbageSpring()
   {
		// When we start it off, the first possible date for a farm operation is 20th October
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 15,4 );
   }
};

#endif // NLCABBAGESPRING_H

