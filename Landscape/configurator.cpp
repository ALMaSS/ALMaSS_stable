/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>

#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "configurator.h"
#include "maperrormsg.h"

#if !(defined __UNIX) & !(defined __BORLANDC__)
#include <crtdbg.h>
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

using namespace std;

extern void FloatToDouble(double&, float);

class Configurator *g_cfg = NULL;

static CfgBool l_cfg_public_warn_on_set("CFG_PUBLIC_WARN_ON_SET",
					CFG_CUSTOM, true );
static CfgBool l_cfg_public_exit_on_set("CFG_PUBLIC_EXIT_ON_SET",
					CFG_CUSTOM, true );


static const char* CfgSecureStrings[] = {
  "CFG_CUSTOM",
  "CFG_PUBLIC",
  "CFG_PRIVATE"
};


static const char* CfgTypeStrings[] = {
  "*none*",
  "int",
  "double",
  "bool",
  "string"
};


CfgBase::CfgBase( const char* a_key, CfgSecureLevel a_level )
{
  if ( NULL == g_cfg ) {
    g_cfg = new Configurator;
  }

  m_key   = a_key;
  m_level = a_level;
  m_rangetest = false;
  g_cfg->Register( this, a_key );
}



CfgBase::~CfgBase( void )
{
  ;
}



CfgInt::CfgInt(const char* a_key, CfgSecureLevel a_level, int a_defval) :CfgBase(a_key, a_level)
{
	m_int = a_defval;
}

CfgInt::CfgInt(const char* a_key, CfgSecureLevel a_level, int a_defval, int a_min, int a_max) :CfgBase(a_key, a_level)
{
	/** \brief Constructor with max min checking enabled*/
	m_min = a_min;
	m_max = a_max;
	m_rangetest = true;
	set(a_defval);
}

void CfgInt::set(int a_newval) {
	if (m_rangetest) {
		if ((a_newval<m_min) || (a_newval>m_max))
		{
			g_msg->Warn(WARN_FILE, "CfgInt::set Value out of range: ", a_newval);
			g_msg->Warn(WARN_FILE, "CfgInt::set Max allowed: ", m_max);
			g_msg->Warn(WARN_FILE, "CfgInt::set Min allowed: ", m_min);
		}
	}
	m_int = a_newval;
}


CfgFloat::CfgFloat(const char* a_key, CfgSecureLevel a_level, double a_defval) :CfgBase(a_key, a_level)
{
	m_float = a_defval;
}

CfgFloat::CfgFloat(const char* a_key, CfgSecureLevel a_level, double a_defval, double a_min, double a_max) : CfgBase(a_key, a_level)
{
	m_min = a_min;
	m_max = a_max;
	m_rangetest = true;
	set(a_defval);
}

void CfgFloat::set(double a_newval) {
	if (m_rangetest) {
		if ((a_newval<m_min) || (a_newval>m_max))
		{
			g_msg->Warn("CfgFloat::set Value out of range: ", a_newval);
			g_msg->Warn("CfgFloat::set Max allowed: ", m_max);
			g_msg->Warn("CfgFloat::set Min allowed: ", m_min);
		}
	}
	m_float = a_newval;
}



CfgBool::CfgBool( const char*          a_key,
		  CfgSecureLevel a_level,
		  bool           a_defval )
  :CfgBase( a_key, a_level )
{
  m_bool = a_defval;
}



CfgStr::CfgStr( const char*          a_key,
		CfgSecureLevel a_level,
		const char*          a_defval )
  :CfgBase( a_key, a_level )
{
  m_string  = a_defval;
}



Configurator::Configurator( void )
{
	m_lineno = 0;
}



Configurator::~Configurator( void )
{
  ;
}



bool Configurator::Register( CfgBase* a_cfgval, const char* a_key )
{
  string l_key = a_key;

  if ( CfgI.find( l_key ) != CfgI.end() ) {
    // Couldn't register, already exists.
    return false;
  }

  unsigned int i   = (int) CfgVals.size();
  CfgI[ l_key ]    = i;
  CfgVals.resize( i+1 );
  CfgVals[ i ]     = a_cfgval;

  return true;
}



bool Configurator::ReadSymbols( const char *a_cfgfile ) {
	ifstream cf_file;
	char cfgline[ CFG_MAX_LINE_LENGTH ];
	cf_file.open(a_cfgfile,fstream::in);
	if ( !cf_file.is_open() ) {
		g_msg->Warn( WARN_FILE, "Configurator::ReadSymbols() Unable to open file for reading: ", a_cfgfile );
		exit(1);
	}
	while ( !cf_file.eof()) {
		for (unsigned i=0; i< CFG_MAX_LINE_LENGTH; i++) cfgline[i]=' '; // Done to get rid of the rubbish that otherwise messes up the parse
		cf_file.getline(cfgline,CFG_MAX_LINE_LENGTH);
		ParseCfgLine( cfgline );
		m_lineno++;
	}
	cf_file.close();
	return true;
}



// Caution, modifies original line, and returns a pointer to a
// substring of a_line.
char* Configurator::ExtractString( char* a_line )
{
  char lineno[ 20 ];

  // scan for the first double quote or end of line.
  while ( *a_line != '"' && *a_line != '\0' ) {
    a_line++;
  }

  // The first char in the string had better contain a '"':
  if ( *a_line != '"' ) {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::ExtractString()\n"
		 "  String not enclosed in double quotes at "
		 "config line ", lineno );
    exit(1);
  }

  char* endline = ++a_line;
  bool escaped = false, found = false;

  while ( *endline != '\0' ) {
    if ( *endline == '\\' ) {
      escaped = true;
      endline++;

      if ( *endline == '"' &&
	   LastDoubleQuote( endline )) {
	escaped = false;
      } else {
	continue;
      }
    }
    if ( *endline == '"' && !escaped ) {
      // Found end of string, terminate properly and break the loop.
      *endline++ = '\0';
      found = true;
      break;
    }
    escaped = false;
    endline++;
  }

  if ( !found ) {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::ExtractString() "
		 "No ending double quote after string at "
		 "config line ", lineno );
    exit(1);
  }


  // Check for comment if remainder of line isn't empty.
  if ( sscanf( endline, "%*s" ) == 1 ) {
    // Non-empty comment line.
    if ( sscanf( endline, "%*[#]" ) != 1 ) {
      // But not initiated by '#'.
      sprintf( lineno, "%d", m_lineno );
      g_msg->Warn( WARN_FILE, "Configurator::ExtractString() "
		   "Illegal comment at "
		   "config line ", lineno );
      exit(1);
    }
  }

  return a_line;
}


bool Configurator::LastDoubleQuote( char* a_rest_of_line )
{
  a_rest_of_line++;

  while ( *a_rest_of_line != '\0' && *a_rest_of_line != '#' ) {
    if ( *a_rest_of_line == '"' ) {
      return false;
    }
    a_rest_of_line++;
  }
  return true;
}



void Configurator::ParseCfgLine( char* a_line )
{
  char l_id  [ CFG_MAX_LINE_LENGTH ];
  char l_type[ CFG_MAX_LINE_LENGTH ];
  char l_sep [ CFG_MAX_LINE_LENGTH ];
  char l_val [ CFG_MAX_LINE_LENGTH ];
  char l_comm[ CFG_MAX_LINE_LENGTH ];
  char lineno[20];

  if ( sscanf( a_line, "%[#]", l_id ) == 1 ) {
    // Comment line.
    return;
  }

  if ( sscanf( a_line, "%s", l_id) == EOF ) {
    // Empty line consisting only of white spaces.
    return;
  }

  //int l_conv = sscanf( a_line, "%[A-Z_] (%[a-z]) %s", l_id, sizeof(l_id),l_type,sizeof(l_type), l_sep,sizeof(l_sep) );
int l_conv = sscanf( a_line, "%[A-Z_] (%[a-z]) %s", l_id, l_type, l_sep );

  if ( l_conv < 3 ) {
    // Syntax terror.
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::ParseCfgLine() "
		 "Syntax error at config line ",
		 lineno );
    exit(1);
  }

  if ( strcmp( l_sep, "=" ) != 0 ) {
    // Missing '=' assignment separator.
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::ParseCfgLine() "
		 "Missing '=' assignment operator at config line ",
		 lineno );
    exit(1);
  }

  if ( CfgI.find( l_id ) == CfgI.end() ) {
    // Key doesn't exists among the predefined, global configuration
    // variables. Ignore quietly.
    return;
  }

  if ( strlen( l_type ) == 6 &&
       strncmp( l_type, "string", 6 ) == 0 ) {
    // We are not yet ready to do the assignment.
    // If we really have a string enclosed in non-escaped
    // double quotes at the end of the line, then we need to
    // extract it first from our input.
    SetCfgStr( l_id, ExtractString( a_line ));
    return;
  }

  // Not a string, so extract data value and possible comment.
  l_conv = sscanf( a_line, "%*[A-Z_] (%*[a-z]) %*s %s %s",
		   l_val, l_comm );

  if ( l_conv == 2 && l_comm[0] != '#' ) {
    // Illegal comment at end of line.
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::ParseCfgLine() "
		 "Syntax error at end of config line ",
		 lineno );
    exit(1);
  }

  if ( strlen( l_type ) == 5 &&
       strncmp( l_type, "float", 5 ) == 0 ) {
    SetCfgFloat( l_id, l_val );
    return;
  }

  if ( strlen( l_type ) == 4 &&
       strncmp( l_type, "bool", 4 ) == 0 ) {
    SetCfgBool( l_id, l_val );
    return;
  }

  if ( strlen( l_type ) == 3 &&
       strncmp( l_type, "int", 3 ) == 0 ) {
    SetCfgInt( l_id, l_val );
    return;
  }

  sprintf( lineno, "%d", m_lineno );
  g_msg->Warn( WARN_FILE, "Configurator::ParseCfgLine() "
	       "Unknown type specifier at config line ",
	       lineno );
  exit(1);
}



void Configurator::ShowIdType( unsigned int a_i )
{
  g_msg->WarnAddInfo( WARN_FILE,
		      "Type for identifier ",
		      CfgVals[ a_i ]->getkey().c_str() );
  g_msg->WarnAddInfo( WARN_FILE, " is (",
		      CfgTypeStrings[ CfgVals[ a_i ]->gettype() ] );
  g_msg->WarnAddInfo( WARN_FILE, ")\n", "" );
}



bool Configurator::SetCfgGatekeeper( const char* a_method,
				     const char* /* a_key */,
				     CfgSecureLevel a_level )
{
  if ( a_level == CFG_PRIVATE ) {
    // Attempting to set private config variable. Ignore quietly.
    return true;
  }

  if ( a_level == CFG_PUBLIC &&
       l_cfg_public_warn_on_set.value()) {
    // Attempting to set public config variable. Warn and
    // possibly exit if this is configured.
    char lineno[20];
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, a_method, lineno );

    if ( l_cfg_public_exit_on_set.value()) {
      exit(1);
    }
    return true;
  }
  return false;
}



void Configurator::SetCfgInt( char* a_key, char* a_val )
{
  int l_val;
  char lineno[20];
  string l_key = a_key;

  if ( sscanf( a_val, "%d", &l_val ) != 1 ) {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::SetCfgInt() "
		 "Not an integer data value at config line",
		 lineno );
    exit(1);
  }

  // Check access security.
  unsigned int i         = CfgI[ l_key ];
  if ( SetCfgGatekeeper( "Configurator::SetCfgInt() "
			 "Attempting to set public config variable in line",
			 a_key,
			 CfgVals[ i ]->getlevel()
			 )) {
    return;
  }

  if ( CfgVals[ i ]->gettype() != CFG_INT ) {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::SetCfgInt() "
		 "Non-integer identifier specified at config line",
		 lineno );
    ShowIdType( i );
    exit(1);
  }

  dynamic_cast<CfgInt*>(CfgVals[ i ])->set( l_val );
}


void Configurator::SetCfgBool( char* a_key, char* a_val )
{
  char lineno[20];
  string l_key = a_key;
  bool   l_val = false;

  if ( strcmp ( a_val, "false" ) == 0 ) {
    ; // l_val defaults to false.
  } else if ( strcmp ( a_val, "true" ) == 0 ) {
    l_val = true;
  } else {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::SetCfgBool() "
		 "Not a boolean data value at config line",
		 lineno );
    exit(1);
  }

  // Check access security.
  unsigned int i         = CfgI[ l_key ];
  if ( SetCfgGatekeeper( "Configurator::SetCfgBool() "
			 "Attempting to set public config variable in line",
			 a_key,
			 CfgVals[ i ]->getlevel()
			 )) {
    return;
  }

  if ( CfgVals[ i ]->gettype() != CFG_BOOL ) {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::SetCfgBool() "
		 "Non-boolean identifier specified at config line",
		 lineno );
    ShowIdType( i );
    exit(1);
  }

  dynamic_cast<CfgBool*>(CfgVals[ i ])->set( l_val );
}



void Configurator::SetCfgFloat( char* a_key, char* a_val )
{
  double l_val;
  float f;
  char lineno[20];
  string l_key = a_key;

  if ( sscanf( a_val, "%f", &f) != 1 ) {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::SetCfgFloat() "
		 "Not a floating point data value at config line",
		 lineno );
    exit(1);
  }
  FloatToDouble(l_val,f);

  // Check access security.
  unsigned int i         = CfgI[ l_key ];
  if ( SetCfgGatekeeper( "Configurator::SetCfgFloat() "
			 "Attempting to set public config variable in line",
			 a_key,
			 CfgVals[ i ]->getlevel()
			 )) {
    return;
  }

  if ( CfgVals[ i ]->gettype() != CFG_FLOAT ) {
sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::SetCfgFloat() "
		 "Non-floating point identifier specified at config line",
		 lineno );
    ShowIdType( i );
    exit(1);
  }

  dynamic_cast<CfgFloat*>(CfgVals[ i ])->set( l_val );
}



void Configurator::SetCfgStr( char* a_key, char* a_val )
{
  char lineno[20];
  string l_key = a_key;

  // Check access security.
  unsigned int i         = CfgI[ l_key ];
  if ( SetCfgGatekeeper( "Configurator::SetCfgStr() "
			 "Attempting to set public config variable in line",
			 a_key,
			 CfgVals[ i ]->getlevel()
			 )) {
    return;
  }

  if ( CfgVals[ i ]->gettype() != CFG_STRING ) {
    sprintf( lineno, "%d", m_lineno );
    g_msg->Warn( WARN_FILE, "Configurator::SetCfgStr() "
		 "Non-string identifier specified at config line",
		 lineno );
    ShowIdType( i );
    exit(1);
  }

  dynamic_cast<CfgStr*>(CfgVals[ i ])->set( a_val );
}



void Configurator::DumpPublicSymbols( const char *a_dumpfile,
				      CfgSecureLevel a_level )
{
  if ( a_level > CFG_PUBLIC ) {
    a_level = CFG_PUBLIC;
  }
  DumpSymbols( a_dumpfile, a_level );
}



void Configurator::DumpAllSymbolsAndExit( const char *a_dumpfile )
{
  DumpSymbols( a_dumpfile, CFG_PRIVATE );
  exit(1);
}


void Configurator::DumpSymbols( const char *a_dumpfile,
				CfgSecureLevel a_level )
{
  FILE *l_dumpfile;
  char l_oprefix[ CFG_MAX_LINE_LENGTH ] = {""};
  char l_nprefix[ CFG_MAX_LINE_LENGTH ];
  const char* l_id;
  l_dumpfile=fopen( a_dumpfile, "w" );
  if (!l_dumpfile) {
    g_msg->Warn( WARN_FILE, "Configurator::DumpSymbols() "
		 "Unable to open file for writing:",
		 a_dumpfile );
    exit(1);
  }

  typedef map<string,unsigned int>::const_iterator MI;

  for ( MI ii = CfgI.begin(); ii != CfgI.end(); ii++ ) {
    unsigned int i = ii->second;

    // Skip 'secret' variables.
    if ( CfgVals[ i ]->getlevel() > a_level ) {
      continue;
    }

    // Weird hack to separate different groups of
    // configuration names.
    string rubbish=CfgVals[ i ]->getkey();
    l_id=rubbish.c_str();
    //l_id = CfgVals[ i ]->getkey().c_str();
    sscanf( l_id, "%[A-Z]", l_nprefix );
    if ( strcmp( l_oprefix, l_nprefix ) != 0 ) {
      fprintf( l_dumpfile, "\n" );
      strcpy( l_oprefix, l_nprefix );
    }

    fprintf( l_dumpfile, "%s (%s) = ",
	     l_id,
	     CfgTypeStrings[ CfgVals[ i ]->gettype() ]
	     );

    switch( CfgVals[ i ]->gettype() ) {
    case CFG_INT:
      {
	CfgInt* l_p = dynamic_cast<CfgInt*>(CfgVals[ i ]);
	fprintf( l_dumpfile, "%d", l_p->value() );
	break;
      }
    case CFG_FLOAT:
      {
	CfgFloat* l_p = dynamic_cast<CfgFloat*>(CfgVals[ i ]);
	fprintf( l_dumpfile, "%f", l_p->value() );
	break;
      }
    case CFG_BOOL:
      {
	CfgBool* l_p = dynamic_cast<CfgBool*>(CfgVals[ i ]);
	if ( l_p->value() ) {
	  fprintf( l_dumpfile, "true" );
	} else {
	  fprintf( l_dumpfile, "false" );
	}
	break;
      }
    case CFG_STRING:
      {
	CfgStr* l_p = dynamic_cast<CfgStr*>(CfgVals[ i ]);
	fprintf( l_dumpfile, "\"%s\"", l_p->value() );
	break;
      }
    default:
      {
	char l_errno[20];
	sprintf( l_errno, "%d", CfgVals[ i ]->gettype() );
	g_msg->Warn( WARN_FILE, "Configurator::DumpSymbols() "
		     "Unknown symbol type read:",
		     l_errno );
	exit(1);
      }
    }
    fprintf( l_dumpfile, "  # %s\n",
	     CfgSecureStrings[ CfgVals[ i ]->getlevel() ]
	     );
  }

}
