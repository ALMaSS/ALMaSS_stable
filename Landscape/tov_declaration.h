/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#ifndef tov_declarationH
  #define tov_declarationH
//---------------------------------------------------------------------
// Type Of Vegetation enum declaration

typedef enum {
	tov_Carrots = 0,
	tov_CloverGrassGrazed1, // 1
	tov_CloverGrassGrazed2, // 2
	tov_FieldPeas, // 3
	tov_FodderBeet, // 4
	tov_Maize, // 5
	tov_NaturalGrass, // 6
	tov_NoGrowth, // 7
	tov_None, // 8
	tov_Oats, // 9
	tov_OBarleyPeaCloverGrass, // 10

	tov_OCarrots, tov_OCloverGrassGrazed1, tov_OCloverGrassGrazed2, tov_OCloverGrassSilage1, tov_OFieldPeas,
	tov_OFirstYearDanger, tov_OGrazingPigs, tov_OOats, tov_OPermanentGrassGrazed, tov_OPotatoes, // 20

	tov_OSeedGrass1, tov_OSeedGrass2, tov_OSetaside, tov_OSpringBarley, tov_OSpringBarleyClover, tov_OSpringBarleyGrass,
	tov_OSpringBarleyPigs, tov_OTriticale, tov_OWinterBarley, tov_OWinterRape, // 30

	tov_OWinterRye, tov_OWinterWheatUndersown, tov_PermanentGrassGrazed, tov_PermanentGrassTussocky, tov_PermanentSetaside,
	tov_Potatoes, tov_PotatoesIndustry, tov_SeedGrass1, tov_SeedGrass2, tov_Setaside, // 40

	tov_SpringBarley, tov_SpringBarleyCloverGrass, tov_SpringBarleyGrass, tov_SpringBarleySeed, tov_SpringBarleySilage,
	tov_SpringRape, tov_SpringWheat, tov_Triticale, tov_WinterBarley, tov_WinterRape, // 50

	tov_WinterRye, tov_WinterWheat, tov_WWheatPControl, tov_WWheatPToxicControl, tov_WWheatPTreatment, tov_AgroChemIndustryCereal,
	tov_WinterWheatShort, tov_OSBarleySilage, tov_OFieldPeasSilage, tov_FieldPeasStrigling, // 60

	tov_MaizeStrigling, tov_SpringBarleyStrigling, tov_SpringBarleyCloverGrassStrigling,
	tov_WinterBarleyStrigling, tov_WinterRapeStrigling, tov_WinterRyeStrigling, tov_WinterWheatStrigling,
	tov_SpringBarleyPeaCloverGrassStrigling, tov_YoungForest, tov_SpringBarleyStriglingSingle, //70

	tov_SpringBarleyStriglingCulm, tov_WinterWheatStriglingSingle, tov_WinterWheatStriglingCulm, tov_MaizeSilage, tov_FodderGrass,
	tov_Lawn, tov_PermanentGrassLowYield, tov_SpringBarleyPTreatment, tov_OSpringBarleyExt, tov_OWinterWheatUndersownExt,  // 80

	tov_OWinterBarleyExt, tov_OMaizeSilage, tov_SpringBarleySKManagement, tov_FieldPeasSilage, tov_OrchardCrop, tov_OFodderBeet,
	tov_Wasteland, tov_PlantNursery, tov_SugarBeet, tov_SpringBarleySpr, tov_Heath, tov_BroadBeans, tov_OWinterWheat, //93

	tov_NorwegianOats, tov_NorwegianSpringBarley, tov_NorwegianPotatoes,

	tov_PLWinterWheat, //97
	tov_PLWinterRape,
	tov_PLWinterBarley,
	tov_PLWinterTriticale, //100
	tov_PLWinterRye,
	tov_PLSpringWheat,
	tov_PLSpringBarley,
	tov_PLMaize,
	tov_PLMaizeSilage,
	tov_PLPotatoes,
	tov_PLBeet,
	tov_PLFodderLucerne1,
	tov_PLFodderLucerne2,
	tov_PLCarrots,	//110
	tov_PLSpringBarleySpr,
	tov_PLWinterWheatLate,
	tov_PLBeetSpr,
	tov_PLBeans,

	tov_NLBeet,	//115
	tov_NLCarrots,
	tov_NLMaize,
	tov_NLPotatoes,
	tov_NLSpringBarley,
	tov_NLWinterWheat,	//120
	tov_NLCabbage,
	tov_NLTulips,
	tov_NLGrassGrazed1,
	tov_NLGrassGrazed2,
	tov_NLPermanentGrassGrazed,	//125
	tov_NLBeetSpring,
	tov_NLCarrotsSpring,
	tov_NLMaizeSpring,
	tov_NLPotatoesSpring,
	tov_NLSpringBarleySpring,	//130
	tov_NLCabbageSpring,
	tov_NLCatchPeaCrop,
	tov_NLGrassGrazed1Spring,
	tov_NLGrassGrazedLast,

	tov_WaterBufferZone,

	tov_DummyCropPestTesting,

    tov_Undefined // Must be in here and the last one as well!
}
TTypesOfVegetation;

#endif
