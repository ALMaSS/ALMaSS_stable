/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>Farm.h This file contains the headers for the Farm class</B> \n
*/
/**
\file
 by Frank Nikolaisen & Chris J. Topping \n
 Version of June 2003 \n
 \n
 Doxygen formatted comments in July 2008 \n
*/
//
// farm.h
//
/*

Copyright (c) 2003, National Environmental Research Institute, Denmark (NERI)

All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef FARM_H
#define FARM_H

// This really needs to be a #define.
#define PROG_START 1


class Population_Manager;
class FarmManager;
class BroadBeans;
class Carrots;
class CloverGrassGrazed1;
class CloverGrassGrazed2;
class FieldPeas;
class FieldPeasSilage;
class Fodderbeet;
class FodderGrass;
class Maize;
class MaizeSilage;
class OMaizeSilage;
class OBarleyPeaCloverGrass;
class OCarrots;
class OCloverGrassGrazed1;
class OCloverGrassGrazed2;
class OCloverGrassSilage1;
class OFieldPeas;
class OFieldPeasSilage;
class OFirstYearDanger;
class OGrazingPigs;
class OOats;
class Oats;
class OrchardCrop;
class OFodderbeet;
class OPermanentGrassGrazed;
class OPotatoes;
class OSBarleySilage;
class OSeedGrass1;
class OSeedGrass2;
class OSpringBarley;
class OSpringBarleyExt;
class OSpringBarleyPigs;
class OWinterBarley;
class OWinterBarleyExt;
class OWinterRape;
class OWinterRye;
class OWinterWheatUndersown;
class OWinterWheatUndersownExt;
class PermanentGrassGrazed;
class PermanentGrassLowYield;
class PermanentGrassTussocky;
class PermanentSetAside;
class Potatoes;
class PotatoesIndustry;
class SeedGrass1;
class SeedGrass2;
class SetAside;
class SpringBarley;
class SpringBarleySpr;
class SpringBarleyPTreatment;
class SpringBarleySKManagement;
class SpringBarleyCloverGrass;
class SpringBarleySeed;
class SpringBarleySilage;
class SpringRape;
class Sugarbeet;
class Triticale;
class OTriticale;
class WinterBarley;
class WinterRape;
class WinterRye;
class WinterWheat;
class WWheatPControl;
class WWheatPTreatment;
class WWheatPToxicControl;
class AgroChemIndustryCereal;
class WinterWheatStrigling;
class WinterWheatStriglingSingle;
class WinterWheatStriglingCulm;
class SpringBarleyCloverGrassStrigling;
class SpringBarleyStrigling;
class SpringBarleyStriglingCulm;
class SpringBarleyStriglingSingle;
class MaizeStrigling;
class WinterRapeStrigling;
class WinterRyeStrigling;
class WinterBarleyStrigling;
class FieldPeasStrigling;
class SpringBarleyPeaCloverGrassStrigling;
class YoungForestCrop;
class Hunter;
class OWinterWheat;
class NorwegianPotatoes;
class NorwegianOats;
class NorwegianSpringBarley;

class PLWinterWheat;
class PLWinterRape;
class PLWinterBarley;
class PLWinterTriticale;
class PLWinterRye;
class PLSpringWheat;
class PLSpringBarley;
class PLMaize;
class PLMaizeSilage;
class PLPotatoes;
class PLBeet;
class PLFodderLucerne1;
class PLFodderLucerne2;
class PLCarrots;
class PLSpringBarleySpr;
class PLWinterWheatLate;
class PLBeetSpr;
class PLBeans;

class NLBeet;
class NLCarrots;
class NLMaize;
class NLPotatoes;
class NLSpringBarley;
class NLWinterWheat;
class NLCabbage;
class NLTulips;
class NLGrassGrazed1;
class NLGrassGrazed1Spring;
class NLGrassGrazed2;
class NLGrassGrazedLast;
class NLPermanentGrassGrazed;
class NLBeetSpring;
class NLCarrotsSpring;
class NLMaizeSpring;
class NLPotatoesSpring;
class NLSpringBarleySpring;
class NLCabbageSpring;
class NLCatchPeaCrop;

class DummyCropPestTesting;


typedef vector<unsigned >polylist;
typedef vector < Hunter* > HunterList;

enum TTypeCropClassification {
	tocc_Winter = 0,
	tocc_Spring,
	tocc_Catch,
	tocc_Foobar
};


typedef enum
{
  tof_ConventionalCattle=0,
  tof_ConventionalPig,
  tof_ConventionalPlant,
  tof_OrganicCattle,
  tof_OrganicPig,
  tof_OrganicPlant,
  tof_PTrialControl,
  tof_PTrialTreatment,
  tof_PTrialToxicControl,
  tof_ConvMarginalJord,
  tof_AgroChemIndustryCerealFarm1, //10
  tof_AgroChemIndustryCerealFarm2,
  tof_AgroChemIndustryCerealFarm3,
  tof_NoPesticideBase,
  tof_NoPesticideNoP,
  tof_UserDefinedFarm1, //15
  tof_UserDefinedFarm2,
  tof_UserDefinedFarm3,
  tof_UserDefinedFarm4,
  tof_UserDefinedFarm5,
  tof_UserDefinedFarm6, //20
  tof_UserDefinedFarm7,
  tof_UserDefinedFarm8,
  tof_UserDefinedFarm9,
  tof_UserDefinedFarm10,
  tof_UserDefinedFarm11,
  tof_UserDefinedFarm12,
  tof_UserDefinedFarm13,
  tof_UserDefinedFarm14,
  tof_UserDefinedFarm15,
  tof_UserDefinedFarm16,
  tof_UserDefinedFarm17,
  tof_UserDefinedFarm18,
  tof_UserDefinedFarm19,
  tof_UserDefinedFarm20,
  tof_UserDefinedFarm21,
  tof_UserDefinedFarm22,
  tof_UserDefinedFarm23,
  tof_UserDefinedFarm24,
  tof_UserDefinedFarm25,
  tof_UserDefinedFarm26,
  tof_UserDefinedFarm27,
  tof_UserDefinedFarm28,
  tof_UserDefinedFarm29,
  tof_UserDefinedFarm30,
  tof_UserDefinedFarm31,
  tof_UserDefinedFarm32,
  tof_UserDefinedFarm33,
  tof_UserDefinedFarm34,
  tof_UserDefinedFarm35,
  tof_OptimisingFarm
}TTypesOfFarm;


//enums for the OptimisingFarms: toof, tos, tofs, toa, toc, top, tolp, tocv
typedef enum {
	toof_Other = 0,
	toof_Cattle, // 1
	toof_Plant, // 2
	toof_Pig, // 3
	toof_Foobar // Must be in here and the last one as well!
}TTypesOfOptFarms;

typedef enum {
	tos_Sand = 0,
	tos_Other, // 1
	tos_Clay, // 2
		tos_Foobar // Must be in here and the last one as well!
}TTypesOfSoils;

typedef enum {
	tofs_Business = 0,
	tofs_Private, // 1
		tofs_Foobar // Must be in here and the last one as well!
}TTypesOfFarmSize;

typedef enum {
	toa_Horse = 0,
	toa_DCow, // 1
	toa_Suckler,
	toa_DCalf,
	toa_MCalf,
	toa_MCattle,
	toa_Sheep,
	toa_Goat,
	toa_So,
	toa_Finisher,
	toa_Piglet,
	toa_Deer,
	toa_Mink,
	toa_EHen,
	toa_MHen,
	toa_Turkey,
	toa_Goose,
	toa_Duck,
	toa_MDuck,
	toa_Ostrich,
		toa_Foobar // Must be in here and the last one as well!
}TTypesOfAnimals;

typedef enum {
	toc_SBarley = 0,
	toc_Oats,  //1
	toc_OSCrops,
	toc_WBarley,
	toc_WWheat,
	toc_WRye, //5
	toc_Triticale,
	toc_SRape,
	toc_WRape,
	toc_OOilseed,
	toc_Peas, //10
	toc_OLSeed,
	toc_GrassSeed,
	toc_Potato,
	toc_PotatoFood,
	toc_SugarBeet, //15
	toc_GrassClover,
	toc_OLSeedCut,
	toc_SCerealSil,
	toc_PeasSil,
	toc_MaizeSil, //20
	toc_WCerealSil,
	toc_SCerealG,
	toc_PerGrassLow,
	toc_PerGrassNorm,
	toc_GrassEnv1, //25
	toc_GrassEnv2,
	toc_GrassRot,
	toc_Setaside,
	toc_Uncult,
	toc_OUncult, //30
	toc_FodderBeet,
	toc_OFodderBeet,
	toc_CloverGrz,
	toc_Veg,
	toc_Fruit,
	toc_FruitTree, //35
	toc_OSpecCrops,
	toc_ChrisTree,
	toc_EnergyFor,
	toc_OTriticale,
	toc_SpringRape,
	toc_Other,
		toc_Foobar // Must be in here and the last one as well!
}TTypesOfCrops;

//crop parameters that do not vary with farm type, soil type etc.
typedef enum {
	top_Subsidy = 0,
	top_PriceLM, //1
	top_PriceHerb,
	top_PriceFi,
	top_PriceG,
	top_PriceH,
	top_PriceW,
	top_AlfaHerb,
	top_BetaHerb,
	top_AlfaFi,
	top_BetaFi,
	top_AlfaG,
	top_BetaG,
	top_AlfaH,
	top_BetaH,
	top_AlfaW,
	top_BetaW,
		top_Foobar // Must be in here and the last one as well!
}TTypesOfParameters;

typedef enum {
	tolp_AUKey = 0,
	tolp_Nusable, //1
	tolp_FUuKey, //2
		tolp_Foobar // Must be in here and the last one as well!
}TTypesOfLivestockParameters;

typedef enum {
	tocv_AreaPercent = 0,
	tocv_AreaHa, //1
	tocv_N,
	tocv_Nt,
	tocv_BIHerb, //2
	tocv_BIFi,
	tocv_BI,
	tocv_Grooming,
	tocv_Hoeing,
	tocv_Weeding,
	tocv_TotalLoss,
	tocv_Response,
	tocv_Income,
	tocv_Costs,
	tocv_GM,
	tocv_Savings,
		tocv_Foobar // Must be in here and the last one as well!
}TTypesOfCropVariables;

/** \brief A list PPP names for tracking by the Pesticide class */
enum PlantProtectionProducts
{
	ppp_1 = 0,
	ppp_2,
	ppp_3,
	ppp_4,
	ppp_5,
	ppp_6,
	ppp_7,
	ppp_8,
	ppp_9,
	ppp_10,
	ppp_foobar
};

enum TTypeOfFarmerGoal {
	tofg_profit = 0,
	tofg_yield,
	tofg_environment,
	tofg_Foobar
};

struct pesticiderecord {
	double m_amounts[ppp_foobar];
	bool m_present;
};

/** \brief Used for storing permanent crop data for the farm rotation */
struct PermCropData {
	TTypesOfVegetation Tov;
	int Pct;
};

/** \brief Used for storing farmers field size vectors */
struct tpct {
	int index;
	int pct;
};

/**
\brief
A struct to hold the information required to trigger a farm event
*/
struct FarmEvent
{
  bool       m_lock;
  int        m_startday;
  bool       m_first_year;
  long       m_run;
  int        m_todo;
  TTypesOfVegetation m_event;
  TTypesOfVegetation m_next_tov;
  LE        *m_field;

  FarmEvent( TTypesOfVegetation a_event, LE *a_field, int a_todo, long a_run,
             bool a_lock, int a_start, bool a_first_year,
             TTypesOfVegetation a_nextcrop )
    {
      m_event = a_event;
	  m_field    = a_field;
      m_todo  = a_todo;
	  m_run      = a_run;
      m_lock  = a_lock;
	  m_startday = a_start;
      m_first_year = a_first_year;
      m_next_tov=a_nextcrop;
    }
};

/**
\brief
Used during saving farm/hunter information
*/
struct farminfo {
	int m_farmref;
	int m_farmtype;
	int m_farmsize;
	int m_farmarable;
	int m_nofields;
	int m_openfields;
	int m_areaopenfields;
	APoint m_farmcentroid;
	APoint m_farmvalid;
	int m_NoHunters;
};


/** \brief
* A data structure to hold hunter information for distribution
*/
class HunterInfo
{
public:
	/** \brief Unique reference number */
	int refID;
	/** \brief Hunter home x-coord */
	int homeX;
	/** \brief Hunter home y-coord */
	int homeY;
	/** \brief The hunter's is the farm reference number to where he hunts */
	vector<int> FarmHuntRef;
	/** \brief A list of farms that has been tested for duplicates*/
	vector<int> CheckedFarms;

	~HunterInfo() {
		FarmHuntRef.resize( 0 );
		CheckedFarms.resize( 0 );
	}
};

/**
\brief
The base class for all crops
*/
class Crop
{
protected:
  Farm      *m_farm;
  LE        *m_field;
  FarmEvent *m_ev;
  int        m_first_date;
  int        m_count;
  int        m_last_date;
  int		 m_ddegstoharvest;
  //* \brief Contains information on whether this is a winter crop, spring crop, or catch crop that straddles the year boundary (0,1,2)
  int		m_CropClassification;

  /**
  \brief
  Adds an event to this crop management
  */
  void SimpleEvent(long a_date, int a_todo, bool a_lock);

  /**
  \brief
  Adds an event to this crop management without relying on member variables
  */
  void SimpleEvent_(long a_date, int a_todo, bool a_lock, Farm* a_farm, LE* a_field);

public:
	virtual ~Crop() {}
	Crop();
	int GetFirstDate( void ) { return m_first_date; }
	virtual bool Do( Farm *a_farm,  LE *a_field, FarmEvent *a_ev );
	/** \brief Chooses the next crop to grow in a field. */
	void ChooseNextCrop (int a_no_dates);
	//* \brief Get method for m_CropClassification */
	int GetCropClassification() { return m_CropClassification; }
	//* \brief Get method for m_CropClassification */
	void SetCropClassification(int a_classification) { m_CropClassification = a_classification; }

};


/** \brief Struct for storing ALMaSS crop type (#TTypesOfVegetation) with a corresponding value (mainly crop area).*/

// v. similar to PermCropData, but has double instead of int. 
//Used originally for saving Bedriftsmodel (original farm optimization model) type crops in almass type. Also used to store data on biomass_factors
//so changed the name of the member - from Pct to Number

	struct AlmassCrop {
		double Number;
		TTypesOfVegetation Tov;
	};

/**
\brief
Class for storing data for optimisation.
*/
class DataForOptimisation {

public:
	DataForOptimisation();

	//Data at a general level
	void Set_livestockTypes(TTypesOfAnimals livestock_type){m_livestockTypes.push_back(livestock_type);};
	void Set_cropTypes(TTypesOfCrops crop_type){m_cropTypes.push_back(crop_type);};
	void Set_cropTypes_str (string crop_type){m_str_cropTypes.push_back(crop_type);};
	TTypesOfAnimals Get_livestockTypes(int i){return m_livestockTypes[i];};
	string Get_cropTypes_str (int i) {return m_str_cropTypes[i];};
	TTypesOfCrops Get_cropTypes(int i) {return m_cropTypes[i];};

	//Individual data for farms (vector size is no_farms x given_Foobar)
	void Set_livestockNumber(int a_number){m_livestockNumbers.push_back(a_number);}; 
	void Set_cropArea(double a_area){m_cropAreas.push_back(a_area);};
	int Get_livestockNumber(int index){return m_livestockNumbers[index];}; 
	int Get_livestockNumbersSize(){return (int)m_livestockNumbers.size();};
	double Get_cropArea(int index){return m_cropAreas[index];};
	int Get_cropAreasSize(){return (int)m_cropAreas.size();};

	//Data for farms
	/** \brief Used by FarmManager::CreateFarms. Finds a matching almass number and returns farm's type.*/
	TTypesOfOptFarms Get_farmType (int a_almass_no);
	void Set_winterMax (int a_value, int i){m_winterMax[i]=a_value;};
	int Get_winterMax (int i) {return m_winterMax[i];};

	//Data for livestock
	void Set_livestockParameter(double a_value, int i){m_livestockParameters[i]=a_value;};
	double Get_livestockParameter(int i){return m_livestockParameters[i];};

	//Data for crops
	void Set_cropParameter (double a_value, int i){m_cropParameters[i]=a_value;};
	double Get_cropParameter (int i) {return m_cropParameters[i];};

	void Set_alfa (double a_value, int i){m_alfa[i]=a_value;};
	double Get_alfa (int i) {return m_alfa[i];};
	void Set_beta1 (double a_value, int i){m_beta1[i]=a_value;};
	double Get_beta1 (int i) {return m_beta1[i];};
	void Set_beta2 (double a_value, int i){m_beta2[i]=a_value;};
	double Get_beta2 (int i) {return m_beta2[i];};
	void Set_Nnorm (double a_value, int i){m_Nnorm[i]=a_value;};
	double Get_Nnorm (int i) {return m_Nnorm[i];};


	void Set_fixed (string a_value, int i){
		if(a_value=="yes" || a_value=="YES" || a_value=="Yes" || a_value=="1"){m_fixed[i]=true;}
		else {m_fixed[i]=false;}
	};
	bool Get_fixed (int i) {return m_fixed[i];};
	void Set_fodder (string a_value, int i){
		if(a_value=="yes" || a_value=="YES" || a_value=="Yes" || a_value=="1"){m_fodder[i]=true;}
		else {m_fodder[i]=false;}
	};
	bool Get_fodder (int i) {return m_fodder[i];};
	void Set_FUKey (double a_value, int i){m_FUKey[i]=a_value;};
	double Get_FUKey (int i) {return m_FUKey[i];};

	void Set_sellingPrice (double a_value, int i){m_sellingPrice[i]=a_value;};
	double Get_sellingPrice (int i) {return m_sellingPrice[i];};
	void Set_sellingPrice_lastyr (double a_value, int i){m_sellingPrice_lastyr[i]=a_value;};
	double Get_sellingPrice_lastyr (int i) {return m_sellingPrice_lastyr[i];};

	void Set_rotationMax (double a_value, int i){m_rotationMax[i]=a_value;};
	double Get_rotationMax (int i) {return m_rotationMax[i];};
	void Set_rotationMin (double a_value, int i){m_rotationMin[i]=a_value;};
	double Get_rotationMin (int i) {return m_rotationMin[i];};


	//versions for almass crops
	void Set_biomass_factor (double a_value, int i){m_biomass_factors[i] = a_value;};
	double Get_biomass_factor (int i) { return m_biomass_factors[i];} 

	void Set_cropTypes_almass(TTypesOfVegetation crop_type){m_cropTypes_almass.push_back(crop_type);};
	TTypesOfVegetation Get_cropTypes_almass (int i) {return m_cropTypes_almass[i];};
	int Get_cropTypes_almass_size (){return (int)m_cropTypes_almass.size();};
	void Set_cropTypes_almass_str (string crop_type){m_str_cropTypes_almass.push_back(crop_type);};
	string Get_cropTypes_almass_str (int i) {return m_str_cropTypes_almass[i];};

	void Set_noCrops (int no) {m_noCrops = no;};
	int Get_noCrops (){return m_noCrops;};

	void Set_emaize_price (double a_value){m_energy_maize_price.push_back(a_value);};
	double Get_emaize_price (int i){return m_energy_maize_price[i];};

	/** \brief Class storing farm information: farm's number used in ALMaSS system, farm's soil type, farm's size (business or private) and farm's real ID number.*/
	class FarmData{
	public:
		int md_almass_no;
		TTypesOfOptFarms md_farmType;
		TTypesOfSoils md_soilType;
		TTypesOfFarmSize md_farmSize;
		int md_farmRealID;
		int md_soilSubType;
	};

	vector<FarmData*>m_farm_data;


protected:

	/** \brief Initializes vector with -1 values.*/ //called in the constructor
    void InitializeVector(vector<double>&vector);
	/** \brief Number of crops used in the simulation.*/
	int m_noCrops; //how many crops will be optimised - the number is taken from one of the input files; it has to be the same in all input files with crop data!

//I. Data at a general level - changed to config variables

//II. Data for farms

	/** \brief Crop types saved in string format (Bedriftsmodel i.e. original crop optimization model crop mode).*/
	vector<string>m_str_cropTypes; //used in output function
	/** \brief Crop types saved as enumarator types (Bedriftsmodel i.e. original crop optimization model crop mode).*/
	vector<TTypesOfCrops>m_cropTypes;
	//for almass crop types
	/** \brief Crop types saved in string format (ALMaSS crop mode).*/
	vector<string>m_str_cropTypes_almass;
	/** \brief Crop types saved as enumarator types (ALMaSS crop mode).*/
	vector<TTypesOfVegetation>m_cropTypes_almass;
	/** \brief Livestock types saved as enumarator types.*/
	vector<TTypesOfAnimals>m_livestockTypes;
	/** \brief Vector containing values of initial (i.e. in year the data is taken for) crop areas at farms included in the simulation.*/
	vector<double>m_cropAreas;
	/** \brief Vector containing numbers of all livestock types at farms included in the simulation.*/
	vector<int>m_livestockNumbers;

	/** \brief Maximal area taken by winter crops at a farm. [%]*/
	vector<int>m_winterMax;

//III. Data for livestock
	/** \brief Vector storing parameters of all livestock types.*/
	vector<double>m_livestockParameters; //full list of the parameters - enum declaration (TTypesOfLivestockParameters)

//IV. Data for crops
	//1. parameters that do not vary with farm type, soil type etc. (number of parameters = top_Foobar !)
		/** \brief Vector storing parameters of all crop types.*/
		vector<double>m_cropParameters; //full list of the parameters - enum declaration (enum TTypesOfParameters)
	//2. parameters that vary with a soil type
		/** \brief Crop parameter for response (growth) function (resp = alfa + beta1*N + beta2*N^2; defines relationship between yield and fertilizer). Soil type specific.*/
		vector<double>m_alfa;
		/** \brief Crop parameter for response (growth) function (resp = alfa + beta1*N + beta2*N^2; defines relationship between yield and fertilizer). Soil type specific.*/
		vector<double>m_beta1;
		/** \brief Crop parameter for response (growth) function (resp = alfa + beta1*N + beta2*N^2; defines relationship between yield and fertilizer). Soil type specific.*/
		vector<double>m_beta2;
		/** \brief Maximum amount of fertilizer (N) that can be applied for a given crop [kg N/ha]. Soil specific.*/
		vector<double>m_Nnorm;
		/** \brief Factor used to determine actual response (yield per ha) based on crop biomass at a harvest. Factor is crop specific and is equal to a ratio of the response 
		* for optimal fertilizer amount in Bedriftsmodel (original farm optimization model) to the biomass at a harvest for the corresponding ALMaSS crop grown with optimal fertilizer input.*/
		vector<double>m_biomass_factors;

	//3. parameters that vary with a farm type
		/** \brief Crop Boolean parameter - fixed/variable crop. Farm type specific.*/
		vector<bool>m_fixed;
		/** \brief Crop Boolean parameter - fodder/non-fodder crop. Farm type specific.*/
		vector<bool>m_fodder;
		/** \brief Crop parameter: Fodder unit key, i.e. a number of fodder units obtained from a given crop [FU/hkg]. Farm type specific.*/
		vector<double>m_FUKey;
	//4. parameters that vary with a farm and soil type
		/** \brief Selling price of a crop [DKK/hkg]. Farm type specific and soil type specific.*/
		vector<double>m_sellingPrice;
		/** \brief Selling price of a crop in a previous year [DKK/hkg]. Farm type specific and soil type specific.*/
		vector<double>m_sellingPrice_lastyr;
	//5. parameters that vary with a farm type, farm size and a soil type
		/** \brief Maximum acreage of a crop at a farm [%]. Farm type, soil type and farm size specific.*/
		vector<double>m_rotationMax;
		/** \brief Minimum acreage of a crop at a farm [%]. Farm type, soil type and farm size specific.*/
		vector<double>m_rotationMin;

	//6. Energy maize runs
		/** \brief Vector with energy maize prices for each year of simulation.*/
		vector<double>m_energy_maize_price;

};

/**
\brief
The base class for all farm types
*/
class Farm
{
public:
	// Management() will use the global date.
	virtual void Management( void );

	// InitiateManagement() must be called after all the fields have
	// been added to the farm.
	virtual void InitiateManagement( void );

	void     AddField( LE* a_newfield );
	void     RemoveField( LE* a_field );
    Farm( FarmManager* a_manager );
	virtual ~Farm( void );
	void SetFarmNumber( int a_farm_num ) { m_farm_num = a_farm_num; }
	int  GetFarmNumber( void ) { return m_farm_num; }
	//TTypesOfVegetation TranslateCropCodes(std::string& str); 19.03.13 - moved to farm manager
	virtual bool Spraying_herbicides(TTypesOfVegetation){return true; };
	virtual bool Spraying_fungins(TTypesOfVegetation){return true; };
	virtual double Prob_multiplier (){return 1;};
	void Assign_rotation(vector<TTypesOfVegetation>a_new_rotation);

	/** \brief Returns a list of fields with openness above a_openness */
	polylist* ListOpenFields( int a_openness );
	/** \brief Finds farm's centroids - x and y.*/ //14.01.13
	void Centroids();
	/** \brief Returns the number of the fields owned */
	int GetNoFields() { return (int) m_fields.size(); }
	/** \brief Returns the number of the fields above an openness of a_openness */
	int GetNoOpenFields(int a_openness);
	/** \brief Returns the area of the fields above an openness of a_openness */
	int GetAreaOpenFields(int a_openness);
	/** \brief Returns the valid coordinates of the first field owned by a farm */
	APoint GetValidCoords() { APoint pt;  pt.m_x = m_fields[0]->GetValidX(); pt.m_y = m_fields[0]->GetValidY(); return pt; }
	/** \brief Returns the maximum openness score of the fields */
	int GetMaxOpenness()
	{
		int op = 0;
		for (int i = 0; i < m_fields.size(); i++)
		{
			int openness = m_fields[i]->GetOpenness();
			if (openness > op) op = openness;
		}
		return op;
	}

  // And (*sigh*) the crop treatment functions... :-)
  virtual bool SleepAllDay( LE *a_field, double a_user, int a_days );
  virtual bool AutumnPlough( LE *a_field, double a_user, int a_days );
  virtual bool StubblePlough(LE *a_field, double a_user, int a_days);
  virtual bool StubbleCultivatorHeavy(LE *a_field, double a_user, int a_days);
  virtual bool AutumnHarrow( LE *a_field, double a_user, int a_days );
  virtual bool AutumnRoll( LE *a_field, double a_user, int a_days );
  virtual bool PreseedingCultivator(LE *a_field, double a_user, int a_days);
  virtual bool PreseedingCultivatorSow(LE *a_field, double a_user, int a_days);
  virtual bool AutumnSow( LE *a_field, double a_user, int a_days );
  virtual bool WinterPlough( LE *a_field, double a_user, int a_days );
  virtual bool DeepPlough( LE *a_field, double a_user, int a_days );
  virtual bool SpringPlough( LE *a_field, double a_user, int a_days );
  virtual bool SpringHarrow( LE *a_field, double a_user, int a_days );
  virtual bool SpringRoll( LE *a_field, double a_user, int a_days );
  virtual bool SpringSow( LE *a_field, double a_user, int a_days );
  virtual bool SpringSowWithFerti(LE *a_field, double a_user, int a_days);
  virtual bool HerbicideTreat( LE *a_field, double a_user, int a_days );
  virtual bool GrowthRegulator( LE *a_field, double a_user, int a_days );
  virtual bool FungicideTreat( LE *a_field, double a_user, int a_days );
  virtual bool InsecticideTreat( LE *a_field, double a_user, int a_days );
  virtual bool ProductApplication(LE *a_field, double a_user, int a_days, double a_applicationrate, PlantProtectionProducts a_ppp);
  virtual bool ProductApplication_DateLimited(LE * a_field, double, int, double a_applicationrate, PlantProtectionProducts a_ppp);
  virtual bool Molluscicide( LE *a_field, double a_user, int a_days );
  virtual bool RowCultivation( LE *a_field, double a_user, int a_days );
  virtual bool Strigling( LE *a_field, double a_user, int a_days );
  virtual bool StriglingSow( LE *a_field, double a_user, int a_days );
  virtual bool StriglingHill(LE *a_field, double a_user, int a_days);
  virtual bool HillingUp( LE *a_field, double a_user, int a_days );
  virtual bool Water( LE *a_field, double a_user, int a_days );
  virtual bool Swathing( LE *a_field, double a_user, int a_days );
  virtual bool Harvest(LE *a_field, double a_user, int a_days);
  virtual bool HarvestLong(LE *a_field, double a_user, int a_days);
  virtual bool CattleOut(LE *a_field, double a_user, int a_days);
  virtual bool CattleOutLowGrazing( LE *a_field, double a_user, int a_days );
  virtual bool CattleIsOut( LE *a_field, double a_user, int a_days, int a_max );
  virtual bool CattleIsOutLow( LE *a_field, double a_user, int a_days, int a_max );
  virtual bool PigsOut( LE *a_field, double a_user, int a_days );
  virtual bool PigsAreOut( LE *a_field, double a_user, int a_days );
  virtual bool PigsAreOutForced( LE *a_field, double a_user, int a_days );
  virtual bool CutToHay( LE *a_field, double a_user, int a_days );
  virtual bool CutWeeds( LE *a_field, double a_user, int a_days );
  virtual bool CutToSilage( LE *a_field, double a_user, int a_days );
  virtual bool CutOrch( LE *a_field, double a_user, int a_days ); //added 27.08.12
  virtual bool StrawChopping( LE *a_field, double a_user, int a_days );
  virtual bool HayTurning( LE *a_field, double a_user, int a_days );
  virtual bool HayBailing( LE *a_field, double a_user, int a_days );
  virtual bool BurnStrawStubble( LE *a_field, double a_user, int a_days );
  virtual bool StubbleHarrowing( LE *a_field, double a_user, int a_days );
  virtual bool FP_NPKS( LE *a_field, double a_user, int a_days );
  virtual bool FP_NPK( LE *a_field, double a_user, int a_days );
  virtual bool FP_PK( LE *a_field, double a_user, int a_days );
  virtual bool FP_LiquidNH3( LE *a_field, double a_user, int a_days );
  virtual bool FP_Slurry( LE *a_field, double a_user, int a_days );
  virtual bool FP_ManganeseSulphate( LE *a_field, double a_user, int a_days );
  virtual bool FP_AmmoniumSulphate(LE *a_field, double a_user, int a_days);
  virtual bool FP_Manure( LE *a_field, double a_user, int a_days );
  virtual bool FP_GreenManure( LE *a_field, double a_user, int a_days );
  virtual bool FP_Sludge( LE *a_field, double a_user, int a_days );
  virtual bool FP_RSM(LE *a_field, double a_user, int a_days);
  virtual bool FP_Calcium(LE *a_field, double a_user, int a_days);
  virtual bool FA_NPKS(LE *a_field, double a_user, int a_days);
  virtual bool FA_NPK( LE *a_field, double a_user, int a_days );
  virtual bool FA_PK( LE *a_field, double a_user, int a_days );
  virtual bool FA_Slurry( LE *a_field, double a_user, int a_days );
  virtual bool FA_ManganeseSulphate(LE *a_field, double a_user, int a_days);
  virtual bool FA_AmmoniumSulphate( LE *a_field, double a_user, int a_days );
  virtual bool FA_Manure( LE *a_field, double a_user, int a_days );
  virtual bool FA_GreenManure( LE *a_field, double a_user, int a_days );
  virtual bool FA_Sludge( LE *a_field, double a_user, int a_days );
  virtual bool FA_RSM(LE *a_field, double a_user, int a_days);
  virtual bool FA_Calcium(LE *a_field, double a_user, int a_days);
  virtual bool Biocide(LE *a_field, double a_user, int a_days);
  virtual bool BedForming(LE *a_field, double a_user, int a_days);
  virtual bool ShallowHarrow(LE *a_field, double a_user, int a_days);
  virtual bool HeavyCultivatorAggregate(LE *a_field, double a_user, int a_days);
  virtual bool FlowerCutting(LE *a_field, double a_user, int a_days);
  virtual bool BulbHarvest(LE *a_field, double a_user, int a_days);
  virtual bool StrawCovering(LE *a_field, double a_user, int a_days);
  virtual bool StrawRemoval(LE *a_field, double a_user, int a_days);
  
  // User functions for the treatment steps:
  void AddNewEvent( TTypesOfVegetation a_event, long a_date,
		    LE *a_field, int a_todo, long a_num,
		    bool a_lock, int a_start,
		    bool a_first_year, TTypesOfVegetation a_crop );
  bool DoIt(double a_probability);
  bool DoIt_prob(double a_probability);
  TTypesOfFarm  GetType( void ) { return m_farmtype; }
  int GetArea(void);
  int GetTotalArea(void);
  double GetAreaDouble(void);
  bool IsStockFarmer( void ) { return m_stockfarmer; }
  virtual void MakeStockFarmer( void ) { m_stockfarmer = true; }
  int GetIntensity( void ) { return m_intensity; }
  APoint GetCentroids() { APoint pt; pt.m_x = m_farm_centroidx; pt.m_y = m_farm_centroidy; return pt; }
  TTypesOfVegetation GetPreviousCrop(int a_index) {
	  int ind = a_index - 1;
	  if (ind < 0) ind = (int)m_rotation.size() - 1;
	  return m_rotation[ind];
  }
  TTypesOfVegetation GetCrop(int a_index) {
	  return m_rotation[a_index];
  }
  TTypesOfVegetation GetNextCrop(int a_index) {
	  int ind = a_index + 1;
	  if (ind >= (int)m_rotation.size()) ind = ind - (int)m_rotation.size();
	  return m_rotation[ind];
  }
  void AddHunter(Hunter* a_hunter) {
	  m_HuntersList.push_back( a_hunter );
  }
  void RemoveHunter( Hunter* a_hunter ) {
	  for (int h = 0; h < m_HuntersList.size(); h++) {
		  if (m_HuntersList[ h ] == a_hunter) {
			  m_HuntersList.erase( m_HuntersList.begin() + h );
		  }
	  }
  }

protected:
	/**\brief Pointer to the FarmManager.*/
	FarmManager* m_OurManager;
	LowPriority< FarmEvent* >  m_queue;
	vector< LE* >              m_fields;
	vector<TTypesOfVegetation> m_rotation;
	vector<PermCropData>		m_PermCrops;
	TTypesOfFarm  m_farmtype;
	/** \brief A list of hunters allocated to this farm */
	HunterList m_HuntersList;
	bool m_stockfarmer;
	int m_farm_num;
	int m_rotation_sync_index;
	int m_intensity;
	int GetFirstDate( TTypesOfVegetation a_tov );
	int GetNextCropStartDate( LE* a_field,TTypesOfVegetation &a_curr_veg );
	virtual int GetFirstCropIndex( TTypesOfLandscapeElement a_type );
	virtual int GetNextCropIndex( int a_rot_index );
	virtual void HandleEvents( void );
	bool LeSwitch( FarmEvent *ev );
	void CheckRotationManagementLoop( FarmEvent *ev );
	void ReadRotation(std::string fname);

	void AssignPermanentCrop(TTypesOfVegetation tov, int pct); //MOVED FROM THE  USER DEFINED FARM
	int InvIntPartition(vector<tpct>* items, int target);
	/** \brief Farm's centroid, value x. Equal to the average of the x centroid values of all farm's fields.*/
	int m_farm_centroidx;
	/** \brief Farm's centroid, value y. Equal to the average of the y centroid values of all farm's fields.*/
	int m_farm_centroidy;


  // The program function objects for the individual crops.
  // Remember to add these into the Farm class con- and destructor too.
  Carrots                 *m_carrots;
  BroadBeans              *m_broadbeans;
  FodderGrass		      *m_foddergrass;
  CloverGrassGrazed1      *m_CGG1;
  CloverGrassGrazed2      *m_CGG2;
  FieldPeas				  *m_fieldpeas;
  FieldPeasSilage		  *m_fieldpeassilage;
  Fodderbeet              *m_fodderbeet;
  Sugarbeet               *m_sugarbeet;
  OFodderbeet             *m_ofodderbeet;
  Maize                   *m_maize;
  MaizeSilage             *m_maizesilage;
  OMaizeSilage            *m_omaizesilage;
  OBarleyPeaCloverGrass   *m_OBarleyPCG;
  OCarrots                *m_ocarrots;
  OCloverGrassGrazed1     *m_OCGG1;
  OCloverGrassGrazed2     *m_OCGG2;
  OCloverGrassSilage1     *m_OCGS1;
  OFieldPeas		  *m_ofieldpeas;
  OFieldPeasSilage	  *m_ofieldpeassilage;
  OFirstYearDanger        *m_ofirstyeardanger;
  OGrazingPigs            *m_ograzingpigs;
  OrchardCrop			  *m_orchardcrop;
  Oats                    *m_oats;
  OOats                   *m_ooats;
  OPermanentGrassGrazed   *m_opermgrassgrazed;
  OPotatoes               *m_opotatoes;
  OSeedGrass1              *m_oseedgrass1;
  OSeedGrass2              *m_oseedgrass2;
  OSpringBarley           *m_ospringbarley;
  OSpringBarleyExt        *m_ospringbarleyext;
  OSpringBarleyPigs       *m_ospringbarleypigs;
  OSBarleySilage          *m_osbarleysilage;
  OTriticale               *m_otriticale;
  OWinterBarley           *m_owinterbarley;
  OWinterBarleyExt        *m_owinterbarleyext;
  OWinterRape             *m_owinterrape;
  OWinterRye              *m_owinterrye;
  OWinterWheatUndersown	  *m_owinterwheatundersown;
  OWinterWheat	          *m_owinterwheat;
  OWinterWheatUndersownExt *m_owinterwheatundersownext;
  PermanentGrassGrazed    *m_permgrassgrazed;
  PermanentGrassLowYield  *m_permgrasslowyield;
  PermanentGrassTussocky  *m_permgrasstussocky;
  PermanentSetAside       *m_permanentsetaside;
  Potatoes                *m_potatoes;
  PotatoesIndustry        *m_potatoesindustry;
  SeedGrass1              *m_seedgrass1;
  SeedGrass2              *m_seedgrass2;
  SetAside                *m_setaside;
  SpringBarley            *m_springbarley;
  SpringBarleySpr         *m_springbarleyspr;
  SpringBarleySKManagement *m_springbarleyskmanagement;
  SpringBarleyPTreatment  *m_springbarleyptreatment;
  SpringBarleyCloverGrass *m_sbarleyclovergrass;
  SpringBarleySeed        *m_springbarleyseed;
  SpringBarleySilage      *m_springbarleysilage;
  SpringRape			  *m_springrape;
  Triticale               *m_triticale;
  WinterBarley            *m_winterbarley;
  WinterRape              *m_winterrape;
  WinterRye               *m_winterrye;
  WinterWheat             *m_winterwheat;
  WWheatPControl          *m_wwheatpcontrol;
  WWheatPToxicControl     *m_wwheatptoxiccontrol;
  WWheatPTreatment        *m_wwheatptreatment;
  AgroChemIndustryCereal          *m_agrochemindustrycereal;
  WinterWheatStrigling    *m_winterwheatstrigling;
  WinterWheatStriglingCulm    *m_winterwheatstriglingculm;
  WinterWheatStriglingSingle    *m_winterwheatstriglingsingle;
  SpringBarleyCloverGrassStrigling *m_springbarleyclovergrassstrigling;
  SpringBarleyStrigling   *m_springbarleystrigling;
  SpringBarleyStriglingCulm   *m_springbarleystriglingculm;
  SpringBarleyStriglingSingle   *m_springbarleystriglingsingle;
  MaizeStrigling          *m_maizestrigling;
  WinterRapeStrigling     *m_winterrapestrigling;
  WinterRyeStrigling      *m_winterryestrigling;
  WinterBarleyStrigling   *m_winterbarleystrigling;
  FieldPeasStrigling      *m_fieldpeasstrigling;
  SpringBarleyPeaCloverGrassStrigling *m_springbarleypeaclovergrassstrigling;
  YoungForestCrop *m_youngforest;
  NorwegianPotatoes * m_norwegianpotatoes;
  NorwegianOats * m_norwegianoats;
  NorwegianSpringBarley* m_norwegianspringbarley;

  PLWinterWheat			*m_plwinterwheat;
  PLWinterRape			*m_plwinterrape;
  PLWinterBarley			*m_plwinterbarley;
  PLWinterTriticale			*m_plwintertriticale;
  PLWinterRye			*m_plwinterrye;
  PLSpringWheat			*m_plspringwheat;
  PLSpringBarley			*m_plspringbarley;
  PLMaize			*m_plmaize;
  PLMaizeSilage			*m_plmaizesilage;
  PLPotatoes			*m_plpotatoes;
  PLBeet				*m_plbeet;
  PLFodderLucerne1		*m_plfodderlucerne1;
  PLFodderLucerne2		*m_plfodderlucerne2;
  PLCarrots				*m_plcarrots;
  PLSpringBarleySpr			*m_plspringbarleyspr;
  PLWinterWheatLate			*m_plwinterwheatlate;
  PLBeetSpr				*m_plbeetspr;
  PLBeans				*m_plbeans;

  NLBeet				*m_nlbeet;
  NLCarrots				*m_nlcarrots;
  NLMaize				*m_nlmaize;
  NLPotatoes			*m_nlpotatoes;
  NLSpringBarley		*m_nlspringbarley;
  NLWinterWheat			*m_nlwinterwheat;
  NLCabbage				*m_nlcabbage;
  NLTulips				*m_nltulips;
  NLGrassGrazed1				*m_nlgrassgrazed1;
  NLGrassGrazed1Spring				*m_nlgrassgrazed1spring;
  NLGrassGrazed2				*m_nlgrassgrazed2;
  NLGrassGrazedLast				*m_nlgrassgrazedlast;
  NLPermanentGrassGrazed				*m_nlpermanentgrassgrazed;
  NLBeetSpring				*m_nlbeetspring;
  NLCarrotsSpring				*m_nlcarrotsspring;
  NLMaizeSpring				*m_nlmaizespring;
  NLPotatoesSpring			*m_nlpotatoesspring;
  NLSpringBarleySpring		*m_nlspringbarleyspring;
  NLCabbageSpring				*m_nlcabbagespring;
  NLCatchPeaCrop				*m_nlcatchpeacrop;

  DummyCropPestTesting	*m_dummycroppesttesting;
 };


/**
\brief Inbuilt farm type
*/
class ConventionalCattle : public Farm
{
public:
  ConventionalCattle( FarmManager* a_manager );
//protected:
//  virtual TTypesOfVegetation GetFirstCrop( void );
//  virtual TTypesOfVegetation GetNextCrop( TTypesOfVegetation a_veg );
};

/**
\brief Inbuilt farm type
*/
class ConventionalPig : public Farm
{
public:
	ConventionalPig(FarmManager* a_manager);
//protected:
//  virtual TTypesOfVegetation GetFirstCrop( void );
//  virtual TTypesOfVegetation GetNextCrop( TTypesOfVegetation a_veg );
};

/**
\brief Inbuilt farm type
*/
class ConventionalPlant : public Farm
{
public:
	ConventionalPlant(FarmManager* a_manager);
  virtual void MakeStockFarmer( void ) { m_stockfarmer = false; }
//protected:
//  virtual TTypesOfVegetation GetFirstCrop( void );
//  virtual TTypesOfVegetation GetNextCrop( TTypesOfVegetation a_veg );
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class OrganicPlant : public Farm
{
public:
	OrganicPlant(FarmManager* a_manager);
  virtual void MakeStockFarmer( void ) { m_stockfarmer = false; }
//protected:
//  virtual TTypesOfVegetation GetFirstCrop( void );
//  virtual TTypesOfVegetation GetNextCrop( TTypesOfVegetation a_veg );
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class OrganicPig : public Farm
{
public:
	OrganicPig(FarmManager* a_manager);
//protected:
//  virtual TTypesOfVegetation GetFirstCrop( void );
//  virtual TTypesOfVegetation GetNextCrop( TTypesOfVegetation a_veg );
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class OrganicCattle : public Farm
{
public:
	OrganicCattle(FarmManager* a_manager);
//protected:
//  virtual TTypesOfVegetation GetFirstCrop( void );
//  virtual TTypesOfVegetation GetNextCrop( TTypesOfVegetation a_veg );
};

/**
\brief Inbuilt special purpose farm type
*/
class PesticideTrialControl : public Farm
{
public:
	PesticideTrialControl(FarmManager* a_manager);
  virtual void MakeStockFarmer( void ) { m_stockfarmer = false; }
};

/**
\brief Inbuilt special purpose farm type
*/
class PesticideTrialToxicControl : public Farm
{
public:
	PesticideTrialToxicControl(FarmManager* a_manager);
  virtual void MakeStockFarmer( void ) { m_stockfarmer = false; }
};

/**
\brief Inbuilt special purpose farm type
*/
class PesticideTrialTreatment : public Farm
{
public:
	PesticideTrialTreatment(FarmManager* a_manager);
  virtual void MakeStockFarmer( void ) { m_stockfarmer = false; }
};



/**
\brief Inbuilt special purpose farm type
*/
class ConvMarginalJord : public Farm
{
public:
	ConvMarginalJord(FarmManager* a_manager);
};


/**
\brief Inbuilt special purpose farm type
*/
class AgroChemIndustryCerealFarm1 : public Farm
{
public:
	AgroChemIndustryCerealFarm1(FarmManager* a_manager);
};

/**
\brief Inbuilt special purpose farm type
*/
class AgroChemIndustryCerealFarm2 : public Farm
{
public:
	AgroChemIndustryCerealFarm2(FarmManager* a_manager);
};

/**
\brief Inbuilt special purpose farm type
*/
class AgroChemIndustryCerealFarm3 : public Farm
{
public:
	AgroChemIndustryCerealFarm3(FarmManager* a_manager);
};

/**
\brief Inbuilt special purpose farm type
*/
class NoPesticideBaseFarm : public Farm
{
public:
	NoPesticideBaseFarm(FarmManager* a_manager);
};

/**
\brief Inbuilt special purpose farm type
*/
class NoPesticideNoPFarm : public Farm
{
public:
	NoPesticideNoPFarm(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
/**
The rotation is controlled by a file with the format:
N\n
Crop\n
Crop\n
Crop...\n
..........where N is the number of entries and "Crop" is replaced by the crop name for each entry in the rotation. There are 16 Userdefined farm types available numbered 1 to 16, but 2->16 have been excluded from the documentation because they are identical to this class in all but name.\n
*/
class UserDefinedFarm : public Farm
{
public:
	UserDefinedFarm(const char* fname, FarmManager* a_manager);
protected:
	virtual void InitiateManagement( void );
	//void AssignPermanentCrop(TTypesOfVegetation tov, int pct); //MOVED TO THE FARM CLASS
	//int InvIntPartition(vector<tpct>* items, int target);
	//void TranslateBitsToFields(int bits, vector<LE*> &f_cpy, TTypesOfVegetation tov);
};

class UserDefinedFarm1 : public Farm
{
public:
	UserDefinedFarm1(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm2 : public Farm
{
public:
	UserDefinedFarm2(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm3 : public Farm
{
public:
	UserDefinedFarm3(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm4 : public Farm
{
public:
	UserDefinedFarm4(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm5 : public Farm
{
public:
	UserDefinedFarm5(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm6 : public Farm
{
public:
	UserDefinedFarm6(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm7 : public Farm
{
public:
	UserDefinedFarm7(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm8 : public Farm
{
public:
	UserDefinedFarm8(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm9 : public Farm
{
public:
	UserDefinedFarm9(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm10 : public Farm
{
public:
	UserDefinedFarm10(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm11 : public Farm
{
public:
	UserDefinedFarm11(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm12 : public Farm
{
public:
	UserDefinedFarm12(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm13 : public Farm
{
public:
	UserDefinedFarm13(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm14 : public Farm
{
public:
	UserDefinedFarm14(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm15 : public Farm
{
public:
	UserDefinedFarm15(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm16 : public Farm
{
public:
	UserDefinedFarm16(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm17 : public Farm
{
public:
	UserDefinedFarm17(FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm18 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm18(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm19 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm19(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm20 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm20(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm21 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm21(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm22 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm22(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm23 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm23(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm24 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm24(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm25 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm25(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm26 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm26(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm27 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm27(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm28 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm28(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm29 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm29(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm30 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm30(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm31 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm31(const char* fname, FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm32 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm32(const char* fname,FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm33 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm33(const char* fname,FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm34 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm34(const char* fname,FarmManager* a_manager);
};

/**
\brief A farm that can have its rotation defined by the user at runtime.
*/
class UserDefinedFarm35 : public UserDefinedFarm
{
public:
	/** Constructor, needs rotation but also requires a parameter for farm intensity. */
	UserDefinedFarm35(const char* fname,FarmManager* a_manager);
};



//--------------------------------------------------------------------------------------------
//--------------------------OptimisingFarm----------------------------------------------------
//--------------------------------------------------------------------------------------------

/**
\brief A class for storing livestock parameters and variables for optimising farms.
*/
class Livestock {
	public:
		/** \brief Constructor*/
		Livestock(TTypesOfAnimals a_animalType, int a_number);
		/** \brief Type of livestock.*/
		TTypesOfAnimals m_animalType;
		/** \brief Number of animals of a given type at a farm.*/
        int m_number;
        /** \brief Amount of usable animal fertilizer from a given type of livestock. [kg]*/
        double m_NanimUsable;
        /** \brief Amount of fodder needed for a given type of livestock. [fodder units]*/
        double m_FUdemand;
	};

/**
\brief A class for storing all parameters and results of crop optimisation.
*/
class CropOptimised {
	public:
		/** \brief Constructor*/
		CropOptimised(TTypesOfCrops a_cropType, double a_initialArea);
		CropOptimised(TTypesOfVegetation a_cropType, double a_initialArea); 
		CropOptimised(); //another constructor - for the fake crop

		/** \brief Initial area of a crop on a farm [ha].*/
		double m_initialArea;
		/** \brief Type/name of a crop (original farm optimization model crop types).*/
		TTypesOfCrops m_cropType;
		/** \brief Type/name of a crop (ALMaSS crops).*/
		TTypesOfVegetation m_cropType_almass;

	//parameters: here saved - to make it easier to read them (they are needed very often)
		/** \brief Maximum area in percent of farm's arable area for a given crop (depends on a farm size, farm type and soil type).*/
		double m_rotationMax;
		/** \brief Minimum area in percent of farm's arable area for a given crop (depends on a farm size, farm type and soil type).*/
		double m_rotationMin;

	//optimised variables
		/** \brief Total amount of fertilizer applied per ha of a crop [kg N/ha].*/
        double m_n;
        /** \brief Amount of purchased (and applied) fertilizer per ha of a crop [kg N/ha].*/
        double m_nt;
         /** \brief Optimal amount of fertilizer per ha of a crop supposing ferilizer price equals zero [kg N/ha]. Used in #OptimisingFarm::ActualProfit.*/
		double m_optimalN;

		/** \brief A value of selling price for non-fodder crops or a value of fodder units obtained from a hkg of a fodder crop [DKK/hkg].*/
		double m_benefit;
		/** \brief Response - yield of a crop per ha [hkg/ha].*/
        double m_resp;
        /** \brief Value of BI for herbicides [BI/ha].*/
        double m_BIHerb;
        /** \brief Value of BI for fung- and insecticides [BI/ha].*/
        double m_BIFi;
        /** \brief Summary value of BI for herbicides and fung- and insecticides [BI/ha].*/
        double m_BI;
        /** \brief Value of mechanical weed control for a crop - grooming [DKK/ha].*/
        double m_grooming;
        /** \brief Value of mechanical weed control for a crop - hoeing [DKK/ha].*/
        double m_hoeing;
        /** \brief Value of mechanical weed control for a crop - manual weeding [DKK/ha].*/ //!!!!! in bedriftsmodel data it says it is dkk per time...see more on this 10.07.13
        double m_weeding;
        /** \brief Value of the yield loss due to the limited use of herbicides [%].*/
        double m_lossHerb;
        /** \brief Value of the yield loss due to the limited use of fung- and insecticides [%].*/
        double m_lossFi;
        /** \brief Summary value of the yield loss due to the limited use of herbicides and fung- and insecticides [%].*/
        double m_totalLoss;
        /** \brief Costs of growing 1 ha of a crop. Include costs of labour and machine (constant), pesticides (herbicides and fung- and insecticides), mechanical weed control, fertilizer [DKK/ha].*/
        double m_costs_ha;
        /** \brief Value of income per ha of a crop. Includes value of sales and subsidy [DKK/ha].*/
        double m_income_ha;
        /** \brief Value of gross margin for a crop (#m_income_ha - #m_costs_ha) [DKK/ha].*/
        double m_GM;
        /** \brief Value of savings resulting from growing a fodder crop and not purchasing amount of fodder corresponding to the amount of fodder obtained from 1 ha of a given fodder crop [DKK/ha].*/
        double m_savings; 
        /** \brief Holds the value of #m_GM in case of non-fodder crops and a value of #m_savings in case of fodder crops [DKK/ha].*/
        double m_GM_Savings;
        /** \brief Area of a crop in percent of a farm's total area [%].*/
        double m_areaPercent;
        /** \brief Area of a crop that can be changed when checking for restrictions (=#m_areaPercent - #m_rotationMin) [%].*/
        double m_areaVariable;
        /** \brief Area of a crop in ha at a farm [ha].*/
        double m_area_ha;
};

/**
\brief
The Farm Manager class
*/
class FarmManager
{
public:
	/** \brief Farm manager constructor */
	FarmManager();
	/** \brief Farm manager destructor */
	~FarmManager();
	/** \brief Runs the daily farm management for each farm, but also calculates annual spilled grain and maize */
	void FarmManagement();
	void InitiateManagement( void );
	void AddField(int a_OwnerIndex, LE* a_newland, int a_Owner);
	void RemoveField(int a_OwnerIndex, LE* a_field);
	int ConnectFarm(int a_owner);
	TTypesOfVegetation TranslateCropCodes(std::string& str); //moved from farm
	void DumpFarmAreas();
	/** \brief dumps the farmrefs file to a standard named file */
	void DumpFarmrefs();
	/** \brief Calls #OptimisingFarm::Init for all optimising farms. */
	void InitFarms();
	/** \brief Calculates and saves total areas of all optimising farms and  specific farm types. */
	void Save_diff_farm_types_areas();
	/** \brief Returns the total farm area from the farm ref num */
	int GetFarmTotalSize(int a_farmref)
	{
		return (GetFarmPtr(a_farmref)->GetTotalArea());
	}
	/** \brief Returns the arable area from the farm ref num */
	int GetFarmArableSize(int a_farmref)
	{
		return (GetFarmPtr(a_farmref)->GetArea());
	}
	/** \brief Returns the farm type from the farm ref num */
	TTypesOfFarm GetFarmType(int a_farmref)
	{
		return (GetFarmPtr(a_farmref)->GetType());
	}
	/** \brief Returns the number of fields owned by a from the farm ref num */
	int GetFarmNoFields(int a_farmref)
	{
		return (GetFarmPtr(a_farmref)->GetNoFields());
	}
	/** \brief Returns the number of fields owned by a from the farm ref num */
	APoint GetFarmValidCoords(int a_farmref)
	{
		return (GetFarmPtr(a_farmref)->GetValidCoords());
	}
	
	/** \brief Returns the number of fields with openness more than a_openness */
	int GetFarmNoOpenFields(int a_farmref, int a_openness)
	{
		return (GetFarmPtr(a_farmref)->GetNoOpenFields(a_openness));
	}

	/** \brief Returns the area of fields with openness more than a_openness */
	int GetFarmAreaOpenFields(int a_farmref, int a_openness)
	{
		return (GetFarmPtr(a_farmref)->GetAreaOpenFields(a_openness));
	}

	/** \brief Returns the pointer to a farm with a specific number */
	Farm* GetFarmPtr( int a_owner ) {
		for (unsigned int i = 0; i < m_farms.size( ); i++) {
			if (a_owner == m_farms[ i ]->GetFarmNumber( )) {
				return m_farms[ i ];
			}
		}
		g_msg->Warn( "FarmManager::GetFarmPtr - missing farm ref", a_owner );
		exit( 92 );
	}

	/** \brief Returns the pointer to a farm with a specific index */
	Farm* GetFarmPtrIndex( int a_index) {
				return m_farms[ a_index ];
	}
	/** \brief Returns a random farm reference number */
	int GetRandomFarmRefnum() { return m_farms[random((int)m_farms.size())]->GetFarmNumber(); }
	/** \brief calculate all farm centroids */
	void CalcCentroids() { for (unsigned int i = 0; i < m_farms.size(); i++) m_farms[i]->Centroids(); }
	/** \brief Checks a list to see if a farm matches the illegal list of references */
	bool InIllegalList( int a_farm_ref, vector<int> * a_farmlist );
	/** \brief Add to a list if a farm is not already among the illegal list of references */
	void AddToIllegalList( int a_farm_ref, vector<int> * a_farmlist );
	/** \brief Finds the closest farm to this co-ordinate */
	int FindClosestFarm(HunterInfo a_hinfo, vector<int> *a_farmlist);
	/** \brief Finds the closest farm to this co-ordinate but uses a probability distribtution for acceptance */
	//int FindClosestFarmProb(HunterInfo a_hinfo, vector<int> *a_farmlist);
	/** \brief Finds the closest farm to this co-ordinate with openness more than a value*/
	int FindClosestFarmOpenness( HunterInfo a_hinfo, vector<int> * a_farmlist, int a_openness );
	/** \brief Finds the closest farm to this co-ordinate with openness more than a value but uses a probability distribtution for acceptance based on closeness */
	int FindClosestFarmOpennessProb( HunterInfo a_hinfo, vector<int> * a_farmlist, int a_openness );
	/** \brief Finds the closest farm to this co-ordinate with openness more than a value but uses a probability distribtution for acceptance based on closeness and another based on farm size */
	int FindClosestFarmOpennessProbSmallIsBest( HunterInfo a_hinfo, vector<int> * a_farmlist, int a_openness, vector<int> * a_farmsizelist );
	/** \brief Finds the closest farm to this co-ordinate with openness more than a value but uses a probability distribtution for acceptance based on closeness and another based on farm size */
	int FindClosestFarmOpennessProbNearRoostIsBest( HunterInfo a_hinfo, vector<int> * a_farmlist, int a_openness, vector<APoint> * a_farmsizelist );
	/** \brief Finds a farm openness more than a value not on the list */
	int FindFarmWithRandom(vector<int> * a_farmlist);
	/** \brief Finds a farm openness more than a value not on the list */
	int FindFarmWithOpenness(vector<int> * a_farmlist, int a_openness);
	/** \brief Finds a random farm with at least one field with openness above a_openness */
	int FindOpennessFarm(int a_openness);
	/** \brief Check if a farm has at least one field with openness above a_openness */
	bool CheckOpenness( int a_openness, int a_ref );
	/** \brief Gets the farm centroid as an APoint */
	APoint GetFarmCentroid(int a_farmref)
	{
		for (unsigned int i = 0; i < m_farms.size(); i++)
		{
			if (a_farmref == m_farms[i]->GetFarmNumber())
			{
				return m_farms[i]->GetCentroids();
			}
		}
		g_msg->Warn("FarmManager::GetFarmCentroid - missing farm ref", a_farmref);
		exit(92);
	}
	/** \brief Checks if we already have this ref */
	bool IsDuplicateRef(int a_ref, HunterInfo* a_hinfo);
	/** \brief Returns the average amount of spilled grain in KJ/m2 this year */
	double GetSpilledGrain();
	/** \brief Returns the average amount of spilled maize in KJ/m2 this year */
	double GetSpilledMaize();
	/** \brief Set m_SpilledGrain which is the flag for either 2013 (true) or 2014 (false) spilled grain distributions */
	void SetSpilledGrain( bool a_spilledgrain ) {
		m_SpilledGrain = a_spilledgrain;
	}
//for the decision making
	/** \brief Finds all OptimisingFarms' neighbours and saves them in the farm's #OptimisingFarm::m_neighbours vector. */ 
	void FindNeighbours();
	/** \brief Returnes day degrees for the period March 1st - November 1st. Used for determining yields of crops that are not harvested.*/ 
	double GetDD (void){return daydegrees;}; 
	void SetDD (double a_dd){daydegrees = a_dd;};
	double Get_cropTotals (int i) {return m_cropTotals[i];};
	void Add_to_cropTotals (int i, double value){m_cropTotals[i] += value;};
	void Set_cropTotals (int i, double value){m_cropTotals[i] = value;};
	int Get_cropTotals_size (){return (int)m_cropTotals.size();};
	/** \brief Calls #OptimisingFarm::ActualProfit for all optimising farms. */
	void ActualProfit();
	/** \brief Calls OptimisingFarm::ChooseDecisionMode for all optimising farms. */
	void ChooseDecisionMode_for_farms();
	/** \brief Calls OptimisingFarm::Save_last_years_crops for all optimising farms. */
	void Save_last_years_crops_for_farms();
	/** \brief For each OptimizingFarm it prints the number of times each of the decision modes was used within a single simulation. See also #OptimisingFarm::m_decision_mode_counters. */
	void PrintDecModes_counts();
	/** \brief At the end of a simulation it prints results on crop distribution, pesticide and fertilizer usage. */
	void PrintFinalResults();
	/** \brief Switches OptimisingFarms #Farm::m_rotation to the list of all possible rotational crops. */
	void Switch_rotation();
	TTypesOfVegetation Get_crops_summary_BIs_tov (int i){return m_crops_summary_BIs[i].Tov;};
	void Set_crops_summary_BIs_herb(int i, double BIherb){m_crops_summary_BIs[i].BIherb += BIherb;};
	void Set_crops_summary_BIs_fi(int i, double BIfi){m_crops_summary_BIs[i].BIfi += BIfi;};
	void Set_crops_summary_BIs (int i, double BI){m_crops_summary_BIs[i].BI += BI;};
	void Set_cropTotals_sum(int i, double crop_area){m_cropTotals_sum[i] += crop_area;};
	void Set_cropTotals_plant_sum(int i, double crop_area){m_cropTotals_plant_sum[i] += crop_area;};
	void Set_cropTotals_pig_sum(int i, double crop_area){m_cropTotals_pig_sum[i] += crop_area;};
	void Set_cropTotals_cattle_sum(int i, double crop_area){m_cropTotals_cattle_sum[i] += crop_area;};
	void Set_cropTotals_other_sum(int i, double crop_area){m_cropTotals_other_sum[i] += crop_area;};

	void Set_crops_fertilizer (int i, double fert){m_crops_fertilizer[i] += fert;};
	void Set_crops_fertilizer_trade (int i, double fert_trade){m_crops_fertilizer_trade[i] += fert_trade;};


	TTypesOfOptFarms TranslateFarmCodes(string &str);
	TTypesOfSoils TranslateSoilCodes(string &str);
	TTypesOfFarmSize TranslateFarmSizeCodes(string &str);
	TTypesOfAnimals TranslateAnimalsCodes (string &str);
	TTypesOfCrops TranslateCropsCodes (string &str);
	TTypesOfParameters TranslateParametersCodes (string &str);
	TTypesOfLivestockParameters TranslateLivestockParametersCodes (string &str);
	TTypesOfCropVariables TranslateCropVariablesCodes (string &str);
	/** \brief Pointer to the #DataForOptimisation.*/
	DataForOptimisation * pm_data;
	ofstream * pm_output_file;

	int Get_lookup_table(int index){return m_crop_lookup_table[index];};

	/** \brief Get a farm reference from the lookup table */
	int GetFarmNoLookup(int a_ref) { return m_farmmapping_lookup[a_ref * 2]; }
	/** \brief Get a farm type from the lookup table */
	int GetFarmTypeLookup(int a_ref) { return m_farmmapping_lookup[a_ref * 2 + 1]; }
	/** \brief Returns the flag for renumbering */
	bool GetIsRenumbered() { return m_renumbered; }
	/** \brief Returns the farm ref index for a farmref */
	int GetRenumberedFarmRef(int a_farmref)
	{
		for (int i = 0; i < (int)m_farms.size(); i++)
		{
			if (m_farmmapping_lookup[i * 2] == a_farmref)
			{
				return i;
			}
		}
		g_msg->Warn( "FarmManager::GetRenumberedFarmRef(int a_farmref)  Farm reference number not found in m_farmmapping_lookup ", a_farmref );
		exit( 9 );
	}
	int GetNoFarms( ) {
		return (int)m_farms.size();
	}


protected:
	vector<Farm*> m_farms;
	Population_Manager * m_population_manager; 
	void  CreateFarms( const char *a_farmfile );
	/** \brief Daydegress for period March 1st - November 1st. Used to determine yield of crops that are not harvested (and thus do not have values of biomass at harvest).*/
	double daydegrees;
	/** \brief Is it 2013 (true) or 2014 (false) as far as grain goes */
	bool m_SpilledGrain;
	
		/** \brief Used for a dynamic array of lookups converting farm references to internal renumbering */
	int* m_farmmapping_lookup;
	/** \brief A flag to show whether renumbering was already done */
	bool m_renumbered;


//for the optimisation

	/** \brief Struct used for storing data on pesticide usage throughout the whole simulation (one instance, #m_crops_summary_BIs). */
	struct PesticideData{
		TTypesOfVegetation Tov;
		double BIherb;
		double BIfi;
		double BI;
	};

	double totalOptFarmsArea;
	double totalOptFarmsArea_plant;
	double totalOptFarmsArea_pig;
	double totalOptFarmsArea_cattle;
	double totalOptFarmsArea_other;

	/** \brief Stores crops areas at the landscape level per year. */
	vector<double>m_cropTotals;
	/** \brief Specifies which crop order is allowed in a rotation (i.e. if a given crop can follow another crop). */
	vector<int>m_crop_lookup_table;
	/** \brief Stores the sums of crops areas within one simulation at the landscape level. */
	vector<double>m_cropTotals_sum;
	/** \brief Stores the sums of crops areas on plant farms within one simulation at the landscape level. */
	vector<double>m_cropTotals_plant_sum;
	/** \brief Stores the sums of crops areas on pig farms within one simulation at the landscape level. */
	vector<double>m_cropTotals_pig_sum;
	/** \brief Stores the sums of crops areas on cattle farms within one simulation at the landscape level. */
	vector<double>m_cropTotals_cattle_sum;
	/** \brief Stores the sums of crops areas on other farms within one simulation at the landscape level. */
	vector<double>m_cropTotals_other_sum;
	/** \brief Stores information on aggregate (all farms)pesticide usage for each crop.*/
	vector<PesticideData>m_crops_summary_BIs;
	/** \brief Stores information on aggregate (all farms) fertilizer usage for each crop. */
	vector<double>m_crops_fertilizer;
	/** \brief Stores information on aggregate (all farms) fertilizer trade (Nt) usage for each crop. */
	vector<double>m_crops_fertilizer_trade;
	/** \brief Stores information on crop areas calculated in different stages of model development: for comparison in sensitivity analysis.*/
	vector<double>m_crop_areas_stages;


	//data at a farm level
	/**\brief Reads farm level parameters and saves them in a vector #DataForOptimisation::m_farm_data.*/
	void ReadFarmsData();
	/**\brief Reads the data on livestock numbers and saves them in a vector #DataForOptimisation::m_livestockNumbers.*/
	void ReadLivestockNumbers();
	/**\brief Reads the data on farms' initial crop distributions and saves them in a vector #DataForOptimisation::m_cropAreas.*/
	void ReadInitialCropAreas();

	//data for farms
	/**\brief Reads the data on farms' parameters that vary with a farm type and saves them in a vector of a #DataForOptimisation class.*/
	void ReadFarmsData_perFarmType();

	//data for livestock
	/**\brief Reads the data on livestock parameters (do not vary with farm variables) and saves them in a vector #DataForOptimisation::m_livestockParameters.*/
	void ReadLivestockData();

	//data for crops
	/**\brief Reads the data on crops' parameters that do not vary with farm variables and saves them in a vector #DataForOptimisation::m_cropParameters.*/
	void ReadCropsData();
	/**\brief Reads the data on crops' parameters that vary with a soil type (alfa, beta1, beta2, Nnorm) and saves them in vectors of the #DataForOptimisation class.*/
	void ReadCropsData_perSoilType();
	/**\brief Reads the data on crops' parameters that vary with a farm type (fixed, fodder, FUKey) and saves them in vectors of the #DataForOptimisation class.*/
	void ReadCropsData_perFarmType();
	/**\brief Reads the data on crops' parameters that vary with a farm and soil type (sellingPrice) and saves them in a vector #DataForOptimisation::m_sellingPrice.*/
	void ReadCropsData_perFarm_and_Soil();
	/**\brief Reads the data on crops' parameters that vary with a farm type, soil type and farm size (rotationMax, rotationMin) and saves them in vectors of the #DataForOptimisation class.*/
	void ReadCropsData_perFarm_and_Soil_and_Size();

	//data for almass crops:
	/**\brief Reads the data on farms' initial crop distributions and saves them in a vector of a #DataForOptimisation class. ALMaSS crop mode.*/
	void ReadInitialCropAreas_almass();
	/**\brief Reads the data on crops' parameters that do not vary with farm variables and saves them in a vector #DataForOptimisation::m_cropParameters. ALMaSS crop mode.*/
	void ReadCropsData_almass();
	/**\brief Reads the data on crops' parameters that vary with a soil type (alfa, beta1, beta2, Nnorm) and saves them in vectors of the #DataForOptimisation class. ALMaSS crop mode.*/
	void ReadCropsData_perSoilType_almass();
	/**\brief Reads the data on crops' parameters that vary with a farm type (fixed, fodder, FUKey) and saves them in vectors of the #DataForOptimisation class. ALMaSS crop mode.*/
	void ReadCropsData_perFarmType_almass();
	/**\brief Reads the data on crops' parameters that vary with a farm and soil type (sellingPrice) and saves them in a vector #DataForOptimisation:m_sellingPrice. ALMaSS crop mode.*/
	void ReadCropsData_perFarm_and_Soil_almass();
	/**\brief Reads the data on crops' parameters that vary with a farm type, soil type and farm size (rotationMax, rotationMin) and saves them in vectors of the #DataForOptimisation class. ALMaSS crop mode.*/
	void ReadCropsData_perFarm_and_Soil_and_Size_almass();

	//for energy maize runs
	void ReadEnergyMaizePrice();

	//methods

	/**\brief Creates output files for all crop level variables and for landscape level crop distribution. Includes results of the initial farm otpimisation.*/
	void OptimisationOutput();
	/**\brief Makes an output file for one crop variable and prints its values for each crop and each #OptimisingFarm. Includes results of the initial farm otpimisation.*/
	void PrintOutput(TTypesOfCropVariables a_var, string a_fileName);
	/**\brief Calculates total crop areas (at the landscape level). Includes results of the initial farm otpimisation.*/
	void CropDistribution();
	/**\brief Creates an output file. Prints the crop variables in the first row of the file. The file is used then by farms (#OptimisingFarm) to print the farm level results of optimisation. Includes results of the initial farm otpimisation.*/
	void Create_Output_file_for_farms();
	/**\brief Reads the lookup table  with allowed/forbidden crop order from a text file and saves it into a vector #m_crop_lookup_table.*/
	void ReadLookupTable();
	/**\brief Creates an output file with a list of neighbours of each #OptimisingFarm.*/
	void PrintNeighbours();
	/**\brief Randomly assigns farmer types to farms (#OptimisingFarm) in proportions specified in an input file.*/
	void DistributeFarmerTypes();

};


/**
\brief A farm that carries out crop, pesticide and fertilizer planning using simplified optimisation or other decision startegies.
*/
class OptimisingFarm : public Farm
{
public:
	/**\brief The constructor. */
	OptimisingFarm( FarmManager* a_myfarmmanager, int a_No);
	virtual ~OptimisingFarm() { ; } 
	TTypesOfOptFarms Get_farmType (void) {return m_farmType;}
	TTypesOfSoils Get_soilType (void) {return m_soilType;}
	TTypesOfFarmSize Get_farmSize (void) {return m_farmSize;}
	int Get_farmRealID (void) {return m_farmRealID;}
	int Get_soilSubType (void) {return m_soilSubType;}
	int Get_almass_no (void) {return m_almass_no;}
	CropOptimised* Get_crop (int i) {return m_crops[i];}
	int Get_cropsSize(void) {return (int)m_crops.size();}
	void Set_Livestock (Livestock* p_lvs){m_livestock.push_back(p_lvs);};
	void Set_Crop(CropOptimised* p_crop){m_crops.push_back(p_crop);};

	//-------------------------------------------------------------------------------------------------------------
	//for the modified dec. making - using CONSUMAT approach

	void Set_Neighbour(OptimisingFarm* farm){m_neighbours.push_back(farm);};
	int Get_NeighboursSize(void) {return (int)m_neighbours.size();};
	OptimisingFarm * Get_Neighbour (int i){return m_neighbours[i];};
	vector<AlmassCrop> Get_rotational_crops(){return m_rotational_crops;};
	vector<AlmassCrop> Get_rotational_crops_visible(){return m_rotational_crops_visible;}; 
	double Get_actual_profit() {return m_actual_profit;};
	double Get_actual_aggregated_yield() {return m_actual_aggregated_yield;};
	int GetFarmCentroidX() { return m_farm_centroidx; }
    int GetFarmCentroidY() { return m_farm_centroidy; }
	void Set_main_goal (TTypeOfFarmerGoal a_goal){m_main_goal = a_goal;};
	TTypeOfFarmerGoal Get_main_goal() {return m_main_goal;};
	void Set_animals_no (int a_number){animals_no = a_number;};
	int Get_decision_mode_counters (int index){return m_decision_mode_counters[index];};
	/** \brief #OptimisingFarm's virtual version of #Farm::Harvest(). Saves information on biomass of a crop at harvest.*/
	bool Harvest( LE *a_field, double a_user, int a_days );
	/** \brief Finds a crop to be grown on a given field next year.*/
	void Match_crop_to_field(LE* a_field);
	/** \brief Picks randomly a farmer to imitate/compare with. It chooses among neighbouring farmers with similar farms.*/
	OptimisingFarm * Find_neighbour_to_imitate();

	/** \brief Function that determines actual crop yields and profit in a given year.*/ 
	void ActualProfit();
	/** \brief It saves the #OptimisingFarm::m_rotational_crops in a vector #m_rotational_crops_visible which is accessible for other farmers if they decide to copy it in the following year.*/
	void Save_last_years_crops();
	/** \brief Function determines which decision mode to use. The choice depends on the values of need satisfaction and uncertainty.*/
	void ChooseDecisionMode();
	/**\brief Returns true if a farmer decided to treat a given crop with herbicides. */
	virtual bool Spraying_herbicides(TTypesOfVegetation a_tov_type);
	/**\brief Returns true if a farmer decided to treat a given crop with fung- and insecticides. */
	virtual bool Spraying_fungins(TTypesOfVegetation a_tov_type);
	/**\brief Used when determining whether there should be a spraying event (i.e. pesticides application) or not. For yield maximizer it increases the chance of spraying event to account for his 'just in case' spraying. */
	virtual double Prob_multiplier ();

	//-------------------------------------------------------------------------------------------------------------

	/** \brief Function carrying out the initial calculations at a farm level (including the initial optimisation).*/
	void Init(ofstream * ap_output_file);

protected:
	/**\brief Kicks off the farm's management.*/
	virtual void InitiateManagement( void );
	/** \brief Assigns to each farm its farm type, farm size, farm's real ID number, and soil type. It creates livestock and crops.*/
	void Initialize(FarmManager * a_pfm);
	virtual void HandleEvents( void );
	/**\brief Carries out fungicide application. Saves  information on each application for a given crop.*/
	virtual bool FungicideTreat( LE *a_field, double /*a_user*/, int a_days );
	/**\brief Carries out insecticide application. Saves  information on each application for a given crop.*/
	virtual bool InsecticideTreat( LE *a_field, double /*a_user*/, int a_days );
	/**\brief Carries out herbicide application. Saves  information on each application for a given crop.*/
	virtual bool HerbicideTreat( LE *a_field, double /*a_user*/, int a_days );

	//----------------------------------------------------------------------------------------------------
	//'farmer' variables - for the modified decision making 

	/** \brief Farmer's main goal (determined by a farmer's type) .*/
	TTypeOfFarmerGoal m_main_goal;
	//TTypeOfFarmerGoal m_pest_goal;
	//TTypeOfFarmerGoal m_fert_goal;

	/** \brief Vector of pointers to the farms considered neighbours (fulfilling the neighbourship condition) of a given farm.*/
	vector<OptimisingFarm*>m_neighbours;
	/** \brief Farmer's actual satisfaction level.*/
	double m_need_satisfaction_level;
	/** \brief Farmer's certainty level.*/
	double m_certainty_level;
	/** \brief Vector with counters for each decision mode. 0 - imitation, 1 - social comparison, 2 - repeat, 3 - deliberation.*/
	vector<int>m_decision_mode_counters;

	//the actual and expected values
	/** \brief An actual profit realised at a farm in a given year.*/
	double m_actual_profit;
	/** \brief An expected farm's profit for a given year.*/
	double m_exp_profit;
	/** \brief An actual income at a farm in a given year.*/
	double m_actual_income;
	/** \brief An expected farm's income at a farm in a given year.*/
	double m_exp_income;
	/** \brief Actual costs at a farm in a given year.*/
	double m_actual_costs;
	/** \brief Expected costs at a farm in a given year.*/
	double m_exp_costs;
	/** \brief Actual aggregated yield at a farm in a given year.*/
	/**Sum of yield ratios (actual to expected yield) from all grown crops.*/
	double m_actual_aggregated_yield;
	/** \brief Expected aggregated yield at a farm in a given year.*/
	/**Equal to a number of fields with crops with positive #CropOptimised::m_resp values.*/
	double m_exp_aggregated_yield;

	/** \brief Vector of profits from previous years.*/	
	vector<double>m_previous_profits;
	/** \brief Vector of incomes from previous years.*/
	vector<double>m_previous_incomes;
	/** \brief Vector of costs from previous years.*/
	vector<double>m_previous_costs;
	/** \brief Vector of aggregated yields from previous years.*/
	vector<double>m_previous_aggregated_yields;
	/** \brief Vector of satisfaction levels in five previous years.*/
	vector<double>m_previous_satisfaction_levels;
	/** \brief The neighbouring farmer whose crops might be copied in imitation and social comparison decision modes.*/
	OptimisingFarm * m_previously_imitated_neighbour;

	/** \brief Vector for storing numbers of animals at a farm in previous years (3).*/
	vector<double>m_animals_numbers;
	/** \brief If set to true, a farm must use deliberation as a decision strategy.*/
	bool force_deliberation;
	/** \brief Holds the number of animals in a farm at a particular day in a year (depends on a species).*/
	int animals_no;

	//-----------------------------------------------------------------------------------------------------


	//structs
	/** \brief Struct used for sorting crops.*/
    struct CropSort {
        double key;
        CropOptimised *crop;
    };

	/** \brief Struct redefining operator < - used for sorting crops.*/
	struct reverseSort {
	   bool operator()(CropSort a, CropSort b) { return a.key > b.key; }
	};

	/** \brief Struct used only in Bedriftsmodel crop type mode for creating #m_rotation vector. Bool member
	used for marking the element of a vector as already assigned a #TTypesOfVegetation crop type.*/
	struct MakeRotation {
		bool taken;
		TTypesOfVegetation Tov;
	};


	//lists	- vectors
	/** \brief Vector of pointers to animals belonging to a farm*/
	vector<Livestock*>m_livestock;
	/** \brief Vector of pointers to all crops*/
	vector<CropOptimised*>m_crops;
	/** \brief Vector of structs containing pointers to crops which are not fixed.*/
    vector<CropSort>m_variableCrops;
    /** \brief Vector of structs containing pointers to crops which are not fixed and:
        in case of the winter rotation restriction - exclude winter rotation crops,
        in case of the cattle rotation restriction - exclude the three crops
        that form the condition of the restriction and winter wheat.*/
    vector<CropSort>m_variableCrops2;
    /** \brief Vector of pointers to variable crops that are grown on area larger than areaMin (after determineAreas function was called).*/
    vector<CropOptimised*>m_grownVariableCrops;
    /** \brief Vector of pointers to fixed crops.*/
    vector<CropOptimised*>m_fixedCrops;
    /** \brief Vector of structs containing pointers to (winter) rotation crops.*/
    vector<CropSort>m_rotationCrops;
    /** \brief Vector of structs containing pointers to winter crops.*/
    vector<CropSort>m_winterCrops;
	/** \brief Vector of structs with almass type crops with positive areas in % (result of optimisation).*/
	vector<AlmassCrop>m_crops_almass;
	/** \brief Vector of structs with almass type crops with positive areas in % (result of optimisation): includes only rotational crops.*/
	vector<AlmassCrop>m_rotational_crops;
	/** \brief A copy of #m_rotational_crops used when matching crops to fields.*/
	vector<AlmassCrop>m_rotational_crops_copy;
	/** \brief Stores a copy of #m_rotational_crops from a previous year and is accessible to farmers who want to copy this farm's crops (in imitation or social comparison decision mode).*/
	vector<AlmassCrop>m_rotational_crops_visible;

	//farm parameters (not-farm specific parameters are stored in the DataForOptimisation class)
    /** \brief %Farm's type (cattle, pig, plant, other).*/
	TTypesOfOptFarms m_farmType;
    /** \brief %Farm's soil type (sandy, clay, other).*/
    TTypesOfSoils m_soilType;
    /** \brief Scale of the farm - business (size above 10 ha) or private (size below 10 ha).*/
    TTypesOfFarmSize m_farmSize;
    /** \brief %Farm's real ID number. */
    int m_farmRealID;
	/** \brief %Farm's soil subtype. Defined only for cattle farms on sandy soil (0-bad, 1-good, 2-undefined).*/
	int m_soilSubType;
	/** \brief %Farm's almass number. */
	int m_almass_no;

	//variables
    /** \brief Total area of a farm. A sum of initial crop areas (if in bedriftsmodel, i.e. original farm optimization model, crops mode) or a sum of farm's fields area (if in ALMaSS crops mode). [ha]*/
    double m_totalArea;
	/** \brief Total area of a farm as in bedriftsmodel, i.e. original farm optimization model. [ha]*/
	double m_totalArea_original;
	/** \brief Factor used to scale areas of fixed crops and livestock numbers. Used to adjust these values to the farm's area used in ALMaSS crops mode.*/
	double m_area_scaling_factor;
    /** \brief Total animal fertilizer at a farm. A sum of #Livestock::m_NanimUsable (from all types of livestock). [kg]*/
    double m_totalNanim;
    /** \brief Amount of animal fertilizer available at a farm per hectar. [kg/ha]*/
    double m_Nanim;
    /** \brief %Farm's total demand for fodder. [fodder units]*/
    double m_totalFUdemandBefore;
	 /** \brief %Farm's total demand for fodder (it is covered by growing fodder crops and/or purchasing fodder and thus, at the end of a year it should not be positive). [fodder units]*/
    double m_totalFUdemand;
    /** \brief Fodder from trade (has to be purchased). [fodder units]*/
    double m_totalFUt;
	/** \brief Fodder grown, i.e. obtained from growing fodder crops. [fodder units]*/
    double m_totalFUgrown;
    /** \brief Variable holding a value of area already reserved for certain crops at a farm. [0-100%]*/
    double m_assigned;
    /** \brief Total amount of fertilizer used at a farm. [kg]*/
    double m_totalN;
    /** \brief Total amount of fertilizer purchased at a farm. [kg]*/
    double m_totalNt;
    /** \brief Total amount of herbicides which is planned to be applied at a farm. Expressed as a Treatment frequency index (behandling indeks, BI in Danish).*/
    double m_totalBIHerb;
    /** \brief Total amount of fung- and insecticides which is planned to be applied at a farm. Expressed as a Treatment frequency index (behandling indeks, BI in Danish). */
    double m_totalBIFi;
    /** \brief Total amount of pesticides (sum of #m_totalBIHerb and #m_totalBIFi) which is planned to be applied at a farm. Expressed as a Treatment frequency index (behandling indeks, BI in Danish). */
    double m_totalBI;
    /** \brief Total grooming planned at a farm.*/
    double m_totalGrooming;
    /** \brief Total hoeing planned at a farm.*/
    double m_totalHoeing;
    /** \brief Total manual weeding planned at a farm.*/
    double m_totalWeeding;
    /** \brief Planned total costs of growing crops at a farm. [DKK]*/
    double m_totalCosts;
    /** \brief Planned total income from growing crops at a farm. [DKK]*/
    double m_totalIncome;
    /** \brief Planned total profit (= income - costs) at a farm. In case of animal farms costs of purchased fodder is subtracted from the profit. [DKK]*/
    double m_totalProfit;
	/** \brief Area assigned to rotational crops. [ha]*/
	double m_area_rot;


	// methods

	/** \brief Creates lists of crops. */
	virtual void  createCropsLists(int a_foobar);
    /** \brief Creates a list of pointers to all variable crops included in the optimisation and a list of pointers to fixed crops.*/
    void createVariableCrops(int a_foobar);
	/** \brief Calls functions determining farm level values before the initial optimisation.*/
	void FarmLevelCalculation();
	/** \brief Carries out the whole farm optimisation.*/
	void OptimiseFarm(int a_foobar);

	/** \brief Modifies areas of SeedGrass1 and SeedGrass2, CloverGrassGrazed1 and CloverGrassGrazed2 to be even. Used only in ALMaSS crops mode (in Bedriftsmodel (original farm optimization model) crops mode this is taken care of in #Translate_crops_to_almass()).*/
	void Check_SG_and_CGG();

    //METHODS FOR FARM VARIABLES
    /** \brief Determines #m_totalArea of a farm.*/
    void findTotalArea();
    /** \brief Determines total animal fertilizer (#m_totalNanim) available at a farm.*/
    void findTotalNanim();
    /** \brief Determines amount of animal fertilizer per ha (#m_Nanim) at a farm.*/
    void findNanim();
    /** \brief Determines farm's total demand for fodder (#m_totalFUdemand).*/
    virtual void findFodderDemand();
	/** \brief Prevents small cattle farms from growing cash crops and maize silage.*/
	virtual void preventCashCrops(){};


    //OPTIMISATION
    /** \brief Carries out crop optimisation at a farm.*/
    void optimizeCrops(int a_foobar);

    //OPTIMISATION - SIMPLIFIED METHOD
    /** \brief Determines the optimal amounts of: total fertilizer (CropOptimised::m_n) and purchased fertilizer (CropOptimised::m_nt) for a given crop at a farm.*/
    void findFertilizer(CropOptimised * a_crop, int a_foobar, double benefit);
    /** \brief Determines the response (CropOptimised::m_resp) of a crop at a farm.*/
    void findResponse(CropOptimised * a_crop, int a_foobar);
    /** \brief Determines the optimal Treatment frequency indices (behandling index, BI in Danish) (CropOptimised::m_BIHerb, CropOptimised::m_BIFi, CropOptimised::m_BI) for a given crop at a farm.*/
    void findBIs(CropOptimised * a_crop, double benefit);
	/** \brief Sets values of Treatment frequency indices (BI) for crops with fixed amount of pesticides (CropOptimised::m_BIHerb for FodderBeet and both CropOptimised::m_BIHerb and CropOptimised::m_BIFi for PotatoesIndustry and Potatoes).*/
    void fixBI();
	/**\brief Determines the optimal mechanical weed control means (CropOptimised::m_grooming, CropOptimised::m_hoeing, CropOptimised::m_weeding) for a given crop at a farm.*/
    void findMWeedControl(CropOptimised * a_crop);
    /** \brief Determines the yield losses (CropOptimised::m_lossHerb, CropOptimised::m_lossFi, CropOptimised::m_totalLoss) for a given crop at a farm.*/
    void findYieldLoss(CropOptimised * a_crop);
    /** \brief Determines the gross margin (CropOptimised::m_GM) for a given crop at a farm.*/
    void findGrossMargin(CropOptimised * a_crop, int a_foobar, double benefit);

	//AREA ASSIGNMENT
    /** \brief Adds areas of fixed crops to the variable #m_assigned. For each fixed crop it saves its area under variable #CropOptimised::m_areaPercent.*/
    void assignFixed();
	/** \brief Adds minimum required areas of variable crops to the variable m_assigned.*/
    void sumMinAreas();
    /** \brief Determines areas of variable crops.*/
    virtual void determineAreas(int a_foobar);
    /** \brief Determines areas of crops in ha.*/
    void determineAreas_ha(vector<CropOptimised*>crops);

	//RESTRICTIONS - CROPS AREA
    /** \brief Checks if the restrictions are fulfilled and corrects crops' areas if necessary.*/
    virtual void checkRestrictions();
	/** \brief Checks if the restriction on a winter rotation is fulfilled.*/
	virtual void checkWinterRotation1(){}; 
    /** \brief Checks if the restriction on a max. share of winter crops is fulfilled.*/
	virtual void checkWinterCrops(){};
    /** \brief Increases area of winter rotation crops to their max. allowed area.*/
    void setRotationCropsAtMax();

	//OTHER FUNCTIONS
	/** \brief Reads in crop parameters that do NOT vary with any farm level parameters.*/
	double crop_parameter(int index, string par_name);
	/** \brief Returns a pointer to a crop whose name is specified as the argument (bedriftsmodel, i.e. original farm optimization model, crops mode).*/
	CropOptimised * findCropByName (string crop_name);
	/** \brief Returns a pointer to almass crop whose name is specified as the argument (ALMaSS crops mode).*/
	CropOptimised * findCropByName_almass (string crop_name);
	/** \brief Returns a pointer to almass crop whose tov type is specified as the argument.*/
	CropOptimised * findCropByName_almass (TTypesOfVegetation a_tov_type);
    /** \brief Function for determining total values per farm after initial optimisation.*/
    double total(TTypesOfCropVariables variable_name);
    /** \brief Sorts structs of type CropSort.*/
    void sortCrops(vector<CropSort> &cropsToSort, string sortingKey);
    /** \brief Swaps randomly elements of the list holding same values of the key (according to which the list was previosuly sorted).*/
    void randomizeCropList(vector<CropSort> &listToRandomize, string key);
	/** \brief Increases area of crops by a specified number.*/
    virtual void increaseCrops(vector<CropSort>cropsToIncrease, double &howMuchToIncrease);
    /** \brief Decreases area of a crops by a specified number.*/
    virtual void decreaseCrops(vector<CropSort>cropsToDecrease, double &howMuchToDecrease);
	/** \brief Prints farm-level variables to a text file (one file for all farms).*/
	void Print_FarmVariables (ofstream * ap_output_file);
	/** \brief Creates m_rotation. Not used in ALMaSS crop mode.*/
	void Make_rotations();
	/** \brief Checks if the sum of crops' areaPercent is 100%.*/
	void Check_if_area_100();
	/** \brief Translates crops from Bedriftsmodel (original farm optimization model) to Almass crops. Used in Bedriftsmodel crop mode.*/
	void Translate_crops_to_almass();
	/** \brief Creates a vector storing crops with positive area. Used in ALMaSS crop mode.*/
	void Make_almass_crops_vector();
	/** \brief Creates a vector #m_rotational_crops using the results of optimisation.*/
	void Make_rotational_crops();
	/** \brief Prints the content of a farm's m_rotation. Not used in ALMaSS crop mode.*/
	void Print_rotations(ofstream * ap_output_file);

};

/**
\brief Subclass of the OptimisingFarm. Either pig or cattle farm.
*/
class  AnimalFarm: public OptimisingFarm{
public:
	 AnimalFarm(FarmManager * a_myfarmmanager, int a_No);
	 virtual ~AnimalFarm(){};
protected:

	/** \brief Crop used to fill up area of a farm up to 100% in case its non-fodder crops do not sum to 100% in #determineAreas. Later this crop is removed in the function #determineFodderAreas.*/
     CropOptimised *m_fakeCrop;
	/** \brief Set to true if the fake crop is present after determineAreas function.*/
	 bool m_fakeCropTest;
	/** \brief List of pointers to variable (non-fixed) fodder crops.*/
    vector<CropSort>m_fodderCrops;

	 //variables

	/** \brief Fodder that has to be produced by a farm to fulfill the min. fodder production restriction. [fodder units]*/
    double m_totalFodderToProduce;
    /** \brief Fodder that has to be produced to fulfill the min. fodder production restriction at the end of a year (should be zero). Accounts for fodder from fixed crops and from min. areas of variable crops. [fodder units]*/
    double m_fodderToProduce;
    /** \brief Fodder that has to be produced to fulfill the min. fodder production restriction at the beginning of a year. Accounts for fodder from fixed crops and from min. areas of variable crops. Should be zero at the end of a year. [fodder units]*/
    double m_fodderToProduceBefore;
	/** \brief Sets to true if cash crops are allowed. True by default.*/
	bool cash_crops_allowed;

	//methods
	virtual void createCropsLists(int a_foobar);
	/** \brief Determines areas of variable crops.*/
    virtual void determineAreas(int a_foobar);

	//FODDER CROPS
	/** \brief Creates a list of pointers to all fodder crops.*/
    void createFodderCrops(int a_foobar);
    /** \brief Determines the savings resulting from growing a fodder crop relative to purchasing fodder.*/
    void findFodderCropSavings();
    /** \brief Determines how much fodder is produced from fixed crops and min. areas of variable crops.*/
    void correctFodderDemand(int a_foobar);
    /** \brief Determines areas of fodder crops and corrects areas of non-fodder crops.*/
    void determineFodderAreas(int a_foobar);
	/** \brief Covers the min. required fodder production for animal farms.*/
    void determineMinFodder(int a_foobar);

	//RESTRICTIONS - CROPS AREA
	/** \brief Animal farm version of the #OptimisingFarm::checkWinterRotation1() function (accounts for fodder changes).*/
    virtual void checkWinterRotation1(); 
	/** \brief Animal farm version of the #checkWinterCrops() function (accounts for fodder changes).*/
    virtual void checkWinterCrops();

	//OTHER
	/** \brief Increases area of cropsToIncrease by howMuchToIncrease.*/
    virtual void increaseCrops(vector<CropSort>cropsToIncrease, double &howMuchToIncrease);
    /** \brief Decreases area of cropsToDecrease by howMuchToDecrease.*/
    virtual void decreaseCrops(vector<CropSort>cropsToDecrease, double &howMuchToDecrease);

};

/**
\brief Subclass of the AnimalFarm.
*/
class OptimisingPigFarm: public AnimalFarm{
public:
	OptimisingPigFarm(FarmManager* a_myfarmmanager, int a_No);
	virtual ~OptimisingPigFarm(){};

protected:
	/** \brief Checks if the restrictions are fulfilled and corrects crops' areas if necessary.*/
    virtual void checkRestrictions();
	/** \brief Checks if the restriction on winter rape and winter barley attachment is fulfilled and corrects crops' areas if necessary.*/
    void check_WRape_WBarley();
};

/**
\brief Subclass of the AnimalFarm.
*/
class OptimisingCattleFarm: public AnimalFarm{
public:
	OptimisingCattleFarm(FarmManager* a_myfarmmanager, int a_No);
	virtual ~OptimisingCattleFarm(){};

protected:
	/** \brief Vector of crops used in #checkCattleRotation.*/
	vector<CropSort>m_cattleCrops;
	/** \brief Vector of crops used in #checkCattleRotation_almass.*/
	vector<CropSort>m_cattleCrops_almass;
	/** \brief Checks if the restrictions are fulfilled and corrects crops' areas if necessary.*/
    virtual void checkRestrictions();
	/** \brief Checks if the cattle restriction is fulfilled - in the Bedriftsmodel (original farm optimization model) crop mode.*/
    void checkCattleRotation();
	/** \brief Checks if the cattle restriction is fulfilled - in the ALMaSS crops mode.*/
	void checkCattleRotation_almass();
    /** \brief Decreases area of cattle crops to their min. required area.*/
    void setCattleCropsAtMin();
	/** \brief Prevents small cattle farms from growing cash crops and maize silage.*/
	virtual void preventCashCrops();

};


/**
\brief Subclass of the OptimisingFarm. Either plant or other farm.
*/
class  NonAnimalFarm: public OptimisingFarm{
public:
	NonAnimalFarm(FarmManager * a_myfarmmanager, int a_No);
	virtual ~NonAnimalFarm(){};
protected:

	//methods
	//METHODS FOR FARM VARIABLES
	virtual void findFodderDemand();

	//RESTRICTIONS
	/** \brief Checks if the restriction on a winter rotation is fulfilled.*/
    virtual void checkWinterRotation1(); 
	/** \brief Checks if the restriction on a max. share of winter crops is fulfilled.*/
    virtual void checkWinterCrops();
};

/**
\brief Subclass of the NonAnimalFarm.
*/
class OptimisingPlantFarm: public NonAnimalFarm{
public:
	OptimisingPlantFarm(FarmManager* a_myfarmmanager, int a_No);
	virtual ~OptimisingPlantFarm(){};
};

/**
\brief Subclass of the NonAnimalFarm.
*/
class OptimisingOtherFarm: public NonAnimalFarm{
public:
	OptimisingOtherFarm(FarmManager* a_myfarmmanager, int a_No);
	virtual ~OptimisingOtherFarm(){};
};





#endif // FARM_H
