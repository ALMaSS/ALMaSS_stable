/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#define _CRT_SECURE_NO_DEPRECATE



#define __WEED_CURVE 99 //99  // 99 is the weed curve

// The default no better information values (Mean of the four crop values)
#define EL_BUG_PERCENT_A 0.0
#define EL_BUG_PERCENT_B 0.2975
#define EL_BUG_PERCENT_C 0.095916647275
#define EL_BUG_PERCENT_D 0

// SBarley
#define EL_BUG_PERCENT_SB_A 0
#define EL_BUG_PERCENT_SB_B 0.380763296
#define EL_BUG_PERCENT_SB_C 0
#define EL_BUG_PERCENT_D 0

// WWheat
#define EL_BUG_PERCENT_WW_A 0.0
#define EL_BUG_PERCENT_WW_B 0.1283
#define EL_BUG_PERCENT_WW_C 0.0
#define EL_BUG_PERCENT_D 0

// WRye
#define EL_BUG_PERCENT_WRy_A 0.0
#define EL_BUG_PERCENT_WRy_B 0.395651915
#define EL_BUG_PERCENT_WRy_C 0.0
#define EL_BUG_PERCENT_D 0

//WRape
#define EL_BUG_PERCENT_WR_A 0.0
#define EL_BUG_PERCENT_WR_B 0.028271643
#define EL_BUG_PERCENT_WR_C 0.0
#define EL_BUG_PERCENT_D 0

//Cropped/Grazed Grass
#define EL_BUG_PERCENT_G_A 4.123817127
#define EL_BUG_PERCENT_G_B 0.151015629
#define EL_BUG_PERCENT_G_C -0.228228353
#define EL_BUG_PERCENT_D 0

//Edges
#define EL_BUG_PERCENT_Edges_A 10.72459109
#define EL_BUG_PERCENT_Edges_B 0.4
#define EL_BUG_PERCENT_Edges_C 2.529631141
#define EL_BUG_PERCENT_D 0

#include <math.h>
#include "../Landscape/ls.h"
#include "../BatchALMaSS/BoostRandomGenerators.h"

using namespace std;

extern boost::variate_generator<base_generator_type&, boost::uniform_real<> > g_rand_uni;
extern class PollenNectarDevelopmentData * g_nectarpollen;

extern void FloatToDouble(double &, float);
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_period;




/** \brief Flag to determine whether nectar and pollen models are used - should be set to true for pollinator models! */
CfgBool cfg_pollen_nectar_on("ELE_POLLENNECTAR_ON", CFG_CUSTOM, false);
/** \brief Flag to determine whether to calculate pond pesticide concentration */
CfgBool cfg_calc_pond_pesticide("POND_PEST_CALC_ON", CFG_CUSTOM, false);
/** \brief The multiplication factor assumed to account for ingress of pesticide from run-off and soil water to a pond*/
CfgFloat cfg_pondpesticiderunoff("POND_PEST_RUNOFFFACTOR", CFG_CUSTOM, 10.0);
/** \brief Controls whether random pond quality is used */
CfgBool cfg_randompondquality("POND_RANDOMQUALITY", CFG_CUSTOM, false);
/** \brief The number of days a goose count can be used */
CfgInt cfg_goosecountperiod("GOOSE_GOOSECOUNTPERIOD",CFG_CUSTOM,1);
/** \brief Scales the growth of vegetation - max value */
CfgFloat cfg_PermanentVegGrowthMaxScaler("VEG_GROWTHSCALERMAX", CFG_CUSTOM, 1.0);
/** \brief Scales the growth of vegetation - min value */
CfgFloat cfg_PermanentVegGrowthMinScaler("VEG_GROWTHSCALERMIN", CFG_CUSTOM, 1.0);
const double c_SolarConversion[ 2 ] [ 81 ] = {
   {
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.28,
     0.56,0.84,1.12,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,1.4,
     1.4,1.4,1.26,1.12,0.98,0.84,0.7,0.56,0.42,0.28,0.14,0,0,0,0,0,0,0,0,0,0,0,
     0,0,0,0,0
   },
   {
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     0,0.242857,0.485714,0.728571,0.971429,1.214286,1.457143,1.7,1.7,1.7,1.7,
     1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,1.7,
     1.53,1.36,1.19,1.02,0.85,0.68,0.51,0.34,0.17,0,0,0,0,0,0
   }
  };

extern CfgFloat l_pest_insecticide_amount;
static CfgFloat l_pest_productOrchard_amount( "PEST_PRODUCTORCHARD_AMOUNT", CFG_CUSTOM, 0.0 );
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_startdate2;
extern CfgInt cfg_pest_productapplic_startdate3;
extern CfgInt cfg_pest_productapplic_period;
extern CfgFloat cfg_goose_GrainDecayRateWinter;
extern CfgFloat cfg_goose_GrainDecayRateSpring;
extern CfgFloat cfg_goose_grass_to_winter_cereal_scaler;


static CfgFloat cfg_beetlebankinsectscaler("ELE_BBINSECTSCALER",CFG_CUSTOM,1.0); // 1.0 means beetlebank is the same as hedgebank
static double g_biomass_scale[ tov_Undefined ];
static double g_weed_percent[ tov_Undefined ];
static double g_bug_percent_a[ tov_Undefined ];
static double g_bug_percent_b[ tov_Undefined ];
static double g_bug_percent_c[ tov_Undefined ];
static double g_bug_percent_d[ tov_Undefined ];
static CfgInt cfg_OrchardSprayDay( "TOX_ORCHARDSPRAYDAY", CFG_CUSTOM, 150 );
static CfgInt cfg_OrchardSprayDay2( "TOX_ORCHARDSPRAYDAYTWO", CFG_CUSTOM, 200000 );
CfgInt cfg_OrchardNoCutsDay( "TOX_ORCHARDNOCUTS", CFG_CUSTOM, -1 );
static CfgInt cfg_MownGrassNoCutsDay( "ELE_MOWNGRASSNOCUTS", CFG_CUSTOM, -1 );
static CfgInt cfg_UMPatchyChance( "UMPATCHYCHANCE", CFG_CUSTOM, 0 );
/** \brief The chance that a beetlebank being created is patchy or not */
static CfgFloat cfg_BBPatchyChance( "BEETLEBANKBPATCHYCHANCE", CFG_CUSTOM, 0.5 );
/** \brief The chance that a beetlebank being created is patchy or not */
static CfgFloat cfg_MGPatchyChance( "MOWNGRASSPATCHYCHANCE", CFG_CUSTOM, 0.5 );
/** \brief The chance that a setaside being created is patchy or not */
static CfgFloat cfg_SetAsidePatchyChance("SETASIDEPATCHYCHANCE", CFG_CUSTOM, 1.0);
static CfgFloat cfg_ele_weedscaling( "ELE_WEEDSCALING", CFG_CUSTOM, 1.0 );
/** \brief A constant relating the proportion of food units per m2. The value is calibrated to estimates of newt density. */
CfgFloat cfg_PondLarvalFoodBiomassConst("POND_LARVALFOODBIOMASSCONST", CFG_CUSTOM, 215.0);
/** \brief The instanteous rate of growth for larval food (r from logistic equation) */
CfgFloat cfg_PondLarvalFoodR("POND_LARVALFOODFOODR", CFG_CUSTOM, 0.15);
// Docs in elements.h
CfgInt g_el_tramline_decaytime_days( "ELEM_TRAMLINE_DECAYTIME_DAYS", CFG_PRIVATE, 21 );
CfgInt g_el_herbicide_delaytime_days( "ELEM_HERBICIDE_DELAYTIME_DAYS", CFG_PRIVATE, 35 );
CfgInt g_el_strigling_delaytime_days( "ELEM_STRIGLING_DELAYTIME_DAYS", CFG_PRIVATE, 28 );

// Daydegree sum set on an element by ReduceVeg().
#define EL_GROWTH_DAYDEG_MAGIC l_el_growth_daydeg_magic.value()
static CfgInt l_el_growth_daydeg_magic( "ELEM_GROWTH_DAYDEG_MAGIC", CFG_PRIVATE, 100 );

// Date after which ReduceVeg() automatically sets the growth phase
// to harvest1. Cannot become a global configuration variable as it
// is calculated at runtime.
#define EL_GROWTH_DATE_MAGIC   (g_date->DayInYear(1,9))

// If the fraction used in the call to ReduceVeg() is *below* this
// value, then a phase transition to harvest1 is considered if
// there has been no previous 'forced' phase transition before this
// year.
#define EL_GROWTH_PHASE_SHIFT_LEVEL (l_el_growth_phase_shift_level.value())
static CfgFloat l_el_growth_phase_shift_level( "ELEM_GROWTH_PHASE_SHIFT_LEVEL", CFG_PRIVATE, 0.5 );


// Types of landscape elements. Default is 'unknown'.
// The conversion arrays are at the top in 'elements.cpp'!
// Change when adding or deleting element types.
// Outdated, not used anywhere in the landscape. *FN*
//#define EL_MAX_ELEM_TYPES 18

// Constant of proportionality between leaf area total and plant
// biomass.
#define EL_PLANT_BIOMASS  (l_el_plant_biomass_proport.value())  // Scaled to dry matter on Spring Barley for 2001 & 2002
// All other values are scaled relative to this as of 29/03/05
static CfgFloat l_el_plant_biomass_proport( "ELEM_PLANT_BIOMASS_PROPORT", CFG_PRIVATE, 41.45 );

// Default starting LAI Total. (NB LAIGreen=LAITotal/4)
#define EL_VEG_START_LAIT (l_el_veg_start_lait.value())
static CfgFloat l_el_veg_start_lait( "ELEM_VEG_START_LAIT", CFG_PRIVATE, 1.08 );

// Constant * biomass to get height
#define EL_VEG_HEIGHTSCALE  (l_el_veg_heightscale.value())
static CfgInt l_el_veg_heightscale( "ELEM_VEG_HEIGHTSCALE", CFG_PRIVATE, 16 );
//May+21
#define RV_CUT_MAY (l_el_rv_cut_may.value())
static CfgInt l_el_rv_cut_may( "ELEM_RV_CUT_MAY", CFG_PRIVATE, 142 );

#define RV_CUT_JUN (l_el_rv_cut_jun.value())
static CfgInt l_el_rv_cut_jun( "ELEM_RV_CUT_JUN", CFG_PRIVATE, 28 );

#define RV_CUT_JUL (l_el_rv_cut_jul.value())
static CfgInt l_el_rv_cut_jul( "ELEM_RV_CUT_JUL", CFG_PRIVATE, 35 );

#define RV_CUT_AUG (l_el_rv_cut_aug.value())
static CfgInt l_el_rv_cut_aug( "ELEM_RV_CUT_AUG", CFG_PRIVATE, 42 );

#define RV_CUT_SEP (l_el_rv_cut_sep.value())
static CfgInt l_el_rv_cut_sep( "ELEM_RV_CUT_SEP", CFG_PRIVATE, 49 );

#define RV_CUT_OCT (l_el_rv_cut_oct.value())
static CfgInt l_el_rv_cut_oct( "ELEM_RV_CUT_OCT", CFG_PRIVATE, 49 );

#define RV_MAY_1ST (l_el_rv_may_1st.value())
static CfgInt l_el_rv_may_1st( "ELEM_RV_MAY_1ST", CFG_PRIVATE, 121 );

#define RV_CUT_HEIGHT (l_el_rv_cut_height.value())
static CfgFloat l_el_rv_cut_height( "ELEM_RV_CUT_HEIGHT", CFG_PRIVATE, 10.0 );
#define RV_CUT_GREEN (l_el_rv_cut_green.value())
static CfgFloat l_el_rv_cut_green( "ELEM_RV_CUT_GREEN", CFG_PRIVATE, 1.5 );
#define RV_CUT_TOTAL (l_el_rv_cut_total.value())
static CfgFloat l_el_rv_cut_total( "ELEM_RV_CUT_TOTAL", CFG_PRIVATE, 2.0 );

CfgFloat l_el_o_cut_height( "ELEM_RV_CUT_HEIGHT", CFG_PRIVATE, 10.0 );
CfgFloat l_el_o_cut_green( "ELEM_RV_CUT_GREEN", CFG_PRIVATE, 1.5 );
CfgFloat l_el_o_cut_total( "ELEM_RV_CUT_TOTAL", CFG_PRIVATE, 2.0 );

// Default fraction between crop and weed biomasses.
#define EL_WEED_PERCENT (l_el_weed_percent.value())
static CfgFloat l_el_weed_percent( "ELEM_WEED_PERCENT", CFG_PRIVATE, 0.1 );

// Weed biomass regrowth slope after herbacide application.
#define EL_WEED_SLOPE (l_el_weed_slope.value())
static CfgFloat l_el_weed_slope( "ELEM_WEED_SLOPE", CFG_PRIVATE, 0.15 );

// Bug biomass regrowth slope after insecticide application.
#define EL_BUG_SLOPE (l_el_bug_slope.value())
static CfgFloat l_el_bug_slope( "ELEM_BUG_SLOPE", CFG_PRIVATE, 0.2 );

// Fraction of the weed biomass below which we are in pesticide
// regrowth phase, above we are in proportionality mode.
// CANNOT be 1.00!
#define EL_WEED_GLUE (l_el_weed_glue.value())
static CfgFloat l_el_weed_glue( "ELEM_WEED_GLUE", CFG_PRIVATE, 0.99 );

// Same as for weed, but bugs this time.
#define EL_BUG_GLUE (l_el_bug_glue.value())
static CfgFloat l_el_bug_glue( "ELEM_BUG_GLUE", CFG_PRIVATE, 0.50 );

/** \brief Used for birds that feed on grain on cereal fields 3% spill is expected
*
* Yield	%	kg/Ha spill	kJ/kg	kj/m
* 0.85	0.01	8.5	13680	11.628
* 0.85	0.02	17	13680	23.256
* 0.85	0.03	25.5	13680	34.884
* 0.85	0.04	34	13680	46.512
* 0.85	0.05	42.5	13680	58.14
* 0.85	0.06	51	13680	69.768
*/


// This is inversed prior to use. A multiplication is very much less
// expensive compared to a division.
//
// The original array supplied is:
// {1.11,1.06,1.01,0.99,0.96,0.92,0.92,0.93,0.97,0.99,1.02,1.06}
double LE::m_monthly_traffic[ 12 ] =
  {0.9009, 0.9434, 0.9901, 1.0101, 1.0417, 1.0870,
 1.0870, 1.0753, 1.0753, 1.0101, 0.9804,  0.9434};

double LE::m_largeroad_load[ 24 ] =
  {15,9,4,5,14,54,332,381,252,206,204,215,
 231,256,335,470,384,270,191,130,91,100,99,60};

double LE::m_smallroad_load[ 24 ] =
  {4,3,1,1,4,15,94,108,71,58,58,61,
 65,73,95,133,109,76,54,37,26,28,28,17};

class LE_TypeClass * g_letype;

LE::LE(void) {

	/**
	* The major job of this constructor is simply to provide default values for all members
	*/
	m_signal_mask = 0;
	m_lasttreat.resize(1);
	m_lasttreat[0] = sleep_all_day;
	m_lastindex = 0;
	m_running = 0;
	m_poison = false;
	m_owner_file = -1;
	m_owner_index = -1;
	m_high = false;
	m_cattle_grazing = 0;
	m_default_grazing_level = 0; // this means any grazed elements must set this in their constructor.
	m_pig_grazing = false;
	m_yddegs = 0.0;
	m_vegddegs = -1.0;
	m_olddays = 0;
	m_days_since_insecticide_spray = 0;
	m_tramlinesdecay = 0;
	m_mowndecay = 0;
	m_herbicidedelay = 0;
	m_border = NULL;
	m_unsprayedmarginpolyref = -1;
	m_valid_x = -1;
	m_valid_y = -1;
	m_is_in_map = false;
	m_squares_in_map = 0;
	m_management_loop_detect_date = 0;
	m_management_loop_detect_count = 0;
	m_repeat_start = false;
	m_skylarkscrapes = false;
	m_type = tole_Foobar;
	SetALMaSSEleType(-1);
	m_ddegs = 0.0;
	m_maxx = -1; // a very small number
	m_maxy = -1;
	m_minx = 9999999; // a very big number
	m_miny = 9999999;
	m_countrydesignation = -1; // default not set
	m_soiltype = -1;
	m_area = 0;
	m_centroidx = -1;
	m_centroidy = -1;
	m_vege_danger_store = -1;
	m_PesticideGridCell = -1;
	m_subtype = -1;
	m_owner = NULL;
	m_rot_index = -1;
	m_poly = -1;
	m_map_index = -1;
	m_almass_le_type = -1;
	m_farmfunc_tried_to_do = -1;
	SetStubble(false);
	m_birdseedforage = -1;
	m_birdmaizeforage = -1;
	m_openness = -1;
	m_vegage = -1;
	m_OsmiaNestProb = 0;
	m_maxOsmiaNests = 0;
	m_currentOsmiaNests = 0;
	for (int i = 0; i<10; i++) SetMConstants(i, 1);
	for (int i = 0; i < 366; i++)
	{
		m_gooseNos[i] = 0;
		m_gooseNosTimed[i] = 0;
		for (int l = 0; l < gs_foobar; l++)
		{
			m_gooseSpNos[i][l] = 0;
			m_gooseSpNosTimed[i][l] = 0;
		}
	}
	for (int l = 0; l < gs_foobar; l++)
	{
		m_goosegrazingforage[l] = 0;
	}
	for (int i = 0; i < 25; i++)
	{
		MDates[0][i] = -1;
		MDates[1][i] = -1;
	}
	SetLastSownVeg(tov_Undefined);
#ifdef FMDEBUG
	m_pindex = 0;
	for ( int i = 0; i < 256; i++ ) {
		m_pdates[ i ] = 0;
		m_ptrace[ i ] = 0;
	}
#endif
}

void LE::DoCopy(const LE* a_LE) {

	/**
	* The major job of this method is simply to copy values for all members from one LE to another
	*/
	m_signal_mask = a_LE->m_signal_mask;
	m_lasttreat = a_LE->m_lasttreat;
	m_lastindex = a_LE->m_lastindex;
	m_running = a_LE->m_running;
	m_poison = a_LE->m_poison;
	m_owner_file = a_LE->m_owner_file;
	m_owner_index = a_LE->m_owner_index;
	m_high = a_LE->m_high;
	m_cattle_grazing = a_LE->m_cattle_grazing;
	m_default_grazing_level = a_LE->m_default_grazing_level; // this means any grazed elements must set this in their constructor.
	m_pig_grazing = a_LE->m_pig_grazing;
	m_yddegs = a_LE->m_yddegs;
	m_olddays = a_LE->m_olddays;
	m_vegddegs = a_LE->m_vegddegs;
	m_days_since_insecticide_spray = a_LE->m_days_since_insecticide_spray;
	m_tramlinesdecay = a_LE->m_tramlinesdecay;
	m_mowndecay = a_LE->m_mowndecay;
	m_herbicidedelay = a_LE->m_herbicidedelay;
	m_border = a_LE->m_border;
	m_unsprayedmarginpolyref = a_LE->m_unsprayedmarginpolyref;
	m_valid_x = a_LE->m_valid_x;
	m_valid_y = a_LE->m_valid_y;
	m_is_in_map = a_LE->m_is_in_map;
	m_squares_in_map = a_LE->m_squares_in_map;
	m_management_loop_detect_date = a_LE->m_management_loop_detect_date;
	m_management_loop_detect_count = a_LE->m_management_loop_detect_count;
	m_repeat_start = a_LE->m_repeat_start;
	m_skylarkscrapes = a_LE->m_skylarkscrapes;
	m_type = a_LE->m_type;
	m_birdseedforage = a_LE->m_birdseedforage;
	m_birdmaizeforage = a_LE->m_birdmaizeforage; 
	m_ddegs = a_LE->m_ddegs;
	m_maxx = a_LE->m_maxx; 
	m_maxy = a_LE->m_maxy;
	m_minx = a_LE->m_minx; 
	m_miny = a_LE->m_miny;
	m_countrydesignation = a_LE->m_countrydesignation; 
	m_soiltype = a_LE->m_soiltype;
	m_area = a_LE->m_area;
	m_centroidx = a_LE->m_centroidx;
	m_centroidy = a_LE->m_centroidy;
	m_vege_danger_store = a_LE->m_vege_danger_store;
	m_PesticideGridCell = a_LE->m_PesticideGridCell;
	m_subtype = a_LE->m_subtype;
	m_owner = a_LE->m_owner;
	m_rot_index = a_LE->m_rot_index;
	m_poly = a_LE->m_poly;
	m_map_index = a_LE->m_map_index;
	m_almass_le_type = a_LE->m_almass_le_type;
	m_farmfunc_tried_to_do = a_LE->m_farmfunc_tried_to_do;
	m_openness = a_LE->m_openness;
	m_vegage = a_LE->m_vegage;
	for (int i = 0; i < 366; i++)
	{
		m_gooseNos[i] = a_LE->m_gooseNos[i];
		m_gooseNosTimed[i] = a_LE->m_gooseNosTimed[i];
		for (int l = 0; l < gs_foobar; l++)
		{
			m_gooseSpNos[i][l] = a_LE->m_gooseSpNos[i][l];
			m_gooseSpNosTimed[i][l] = a_LE->m_gooseSpNosTimed[i][l];
		}
	}
	for (int l = 0; l < gs_foobar; l++)
	{
		m_goosegrazingforage[l] = a_LE->m_goosegrazingforage[l];
	}
	for (int i = 0; i < 25; i++)
	{
		MDates[0][i] = a_LE->MDates[0][i];
		MDates[1][i] = a_LE->MDates[1][i];
	}
	for (int i = 0; i<10; i++) SetMConstants(i, a_LE->MConsts[i]);
}


LE::~LE( void ) {
}


#ifdef FMDEBUG
void LE::Trace( int a_value ) {
  m_farmfunc_tried_to_do = a_value;
#ifdef __RECORDFARMEVENTS
  g_landscape_p->RecordEvent(a_value, g_date->DayInYear(), g_date->GetYearNumber());
#endif
  m_pdates[ m_pindex ] = g_date->DayInYear();
  m_ptrace[ m_pindex++ ] = a_value;
  m_pindex &= 0xff; // Circular buffer if need be.
}

void LE::ResetTrace( void ) {
  m_pindex = 0;
  for ( int i = 0; i < 256; i++ ) {
    m_pdates[ i ] = 0;
    m_ptrace[ i ] = 0;
  }
}

#else
// Compiles into nothing if FMDEBUG is #undef.
void LE::Trace( int a_value ) {
  m_farmfunc_tried_to_do = a_value;
}

void LE::ResetTrace( void ) {
}

#endif

void LE::SetCopyTreatment( int a_treatment ) {
  SetLastTreatment( a_treatment );
}

void LE::SetLastTreatment( int a_treatment ) {
  unsigned sz = (int) m_lasttreat.size();
  if ( m_lastindex == sz )
    m_lasttreat.resize( m_lastindex + 1 );

  m_lasttreat[ m_lastindex++ ] = a_treatment;

  // Count this treatment in the grand scope of things.
  g_landscape_p->IncTreatCounter( a_treatment );
  // If we have a field margin then we need to tell it about this
  // but not if it is an insecticide spray etc..
  /* if (m_unsprayedmarginpolyref!=-1) { switch (a_treatment) { case  herbicide_treat: case  growth_regulator:
  case  fungicide_treat: case  insecticide_treat: case trial_insecticidetreat: case syninsecticide_treat: case  molluscicide:
  break; // Do not add sprayings default: LE* le=g_landscape_p->SupplyLEPointer(m_unsprayedmarginpolyref);
  le->SetCopyTreatment(a_treatment); // Now we also need to do something with the treatment

  break; }

  } */
}

int LE::GetLastTreatment( int * a_index ) {
  if ( * a_index == ( int )m_lastindex )
    return sleep_all_day;
  int i = ( * a_index ) ++;
  int treat = m_lasttreat[ i ];
  return treat;
}

void LE::Tick( void ) {
  m_lastindex = 0;
  m_lasttreat[ 0 ] = sleep_all_day;
  if ( m_tramlinesdecay > 0 )
    m_tramlinesdecay--;
  if ( m_mowndecay > 0 )
    m_mowndecay--;
  if ( m_herbicidedelay > 0 ) m_herbicidedelay--;
}



void LE::DoDevelopment( void ) {
}

APoint LE::GetCentroid()
{
    APoint p;
    p.m_x=m_centroidx;
    p.m_y=m_centroidy;
    return p;
}

int LE::GetGooseNos( ) {
	/**
	* This simply looks X days behind at the moment and sums the total number of geese seen.The length of the backward count can be altered by
	* changing the config variable value cfg_goosecountperiod (default 1, only care about yesterday).
	*/
	int geese = 0;
	for (unsigned i = 1; i <= (unsigned)cfg_goosecountperiod.value( ); i++) {
		unsigned ind = ((unsigned)g_date->DayInYear( ) - i) % 365;
		geese += m_gooseNos[ ind ];
	}
	return geese;
}

int LE::GetQuarryNos() {
	/**
	* This simply looks X days behind at the moment and sums the total number of legal quarry species seen.The length of the backward count can be altered by
	* changing the config variable value cfg_goosecountperiod (default 1, only care about yesterday).
	*/
	int geese = 0;
	for (unsigned i = 1; i <= (unsigned)cfg_goosecountperiod.value(); i++) {
		unsigned ind = ((unsigned)g_date->DayInYear() - i) % 365;
		geese += m_gooseSpNos[ind][gs_Pinkfoot];
		geese += m_gooseSpNos[ind][gs_Greylag];
	}
	return geese;
}

int LE::GetGooseNosToday() {
	/**
	* This simply sums the total number of geese seen today.
	*/
	int geese = 0;
	for (unsigned i = 0; i < (unsigned)gs_foobar; i++) {
		geese += m_gooseSpNos[g_date->DayInYear()][i];
	}
	return geese;
}

int LE::GetGooseNosTodayTimed() {
	/**
	* This simply sums the total number of geese seen today at our predefined timepoint.
	*/
	int geese = 0;
	for (unsigned i = 0; i < (unsigned)gs_foobar; i++) {
		geese += m_gooseSpNosTimed[g_date->DayInYear()][i];
	}
	return geese;
}

/** \brief Returns the number of geese of a specific species on a field today.*/
int LE::GetGooseSpNosToday(GooseSpecies a_goose) {
	return m_gooseSpNos[g_date->DayInYear()][a_goose];
}

/** \brief Returns the number of geese of a specific species on a field today.*/
int LE::GetGooseSpNosTodayTimed(GooseSpecies a_goose) {
	return m_gooseSpNosTimed[g_date->DayInYear()][a_goose];
}
/** \brief Returns the distance to closest roost from the field.*/
int LE::GetGooseRoostDist(GooseSpecies a_goose) {
	return int(m_dist_to_closest_roost[a_goose]);
}

void LE::SetPollenNectarData(int a_almasstype) {
	SetPollenNectarCurves(g_nectarpollen->GetPollenNectarCurvePtr(a_almasstype).m_pollencurveptr, g_nectarpollen->GetPollenNectarCurvePtr(a_almasstype).m_nectarcurveptr);
	m_pollenquality.m_quality = m_pollencurve->GetData(365);
	m_nectarquality.m_quality = m_nectarcurve->GetData(365);
	m_totalPollen = 0.0;
	m_totalNectar = 0.0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

VegElement::VegElement(void) : LE() {
	SetVegPatchy(false);
	m_growth_scaler = 1.0; // default
	m_veg_biomass = 0.0;
	m_weed_biomass = 0.0;
	m_veg_height = 0.0;
	m_veg_cover = 0.0;
	m_insect_pop = 1.0;
	m_vege_type = tov_NoGrowth;
	m_curve_num = g_crops->VegTypeToCurveNum(m_vege_type);
	m_weed_curve_num = 99; // 99 used as zero growth curve
	m_yddegs = 0.0;
	m_vegddegs = -1.0;
	m_ddegs = 0.0;
	m_LAgreen = 0.0;
	m_LAtotal = 0.0;
	m_digestability = 1.0;
	for (int i = 0; i < 32; i++) m_oldnewgrowth[i] = 0.5;
	m_newoldgrowthindex = 0;
	m_newgrowthsum = 8.0;
	m_forced_phase_shift = false;
	m_force_growth = false;
	SetGrowthPhase(janfirst);
	m_total_biomass = 0.0;
	m_total_biomass_old = 0.0;



	g_biomass_scale[tov_Carrots] = 0.7857;
	g_biomass_scale[tov_CloverGrassGrazed1] = 1.2;
	g_biomass_scale[tov_CloverGrassGrazed2] = 1.2;
	g_biomass_scale[tov_FodderGrass] = 1.2;
	g_biomass_scale[ tov_BroadBeans ] = 0.857;
	g_biomass_scale[ tov_FieldPeas ] = 0.857;
	g_biomass_scale[ tov_FieldPeasSilage ] = 0.857;
	g_biomass_scale[tov_FodderBeet] = 0.857;
	g_biomass_scale[tov_SugarBeet] = 0.857;
	g_biomass_scale[tov_OFodderBeet] = 0.857;
	g_biomass_scale[tov_Maize] = 1.00;
	g_biomass_scale[tov_MaizeSilage] = 1.00;
	g_biomass_scale[tov_OMaizeSilage] = 1.00;
	g_biomass_scale[tov_NaturalGrass] = 0.567; //0.567 is scaled for actual yield
	g_biomass_scale[tov_Heath] = 0.567; //0.567 is scaled for actual yield
	g_biomass_scale[tov_NoGrowth] = 0.0;
	g_biomass_scale[tov_None] = 0.0;
	g_biomass_scale[tov_OrchardCrop] = 0.7857;
	g_biomass_scale[tov_Oats] = 0.857;
	g_biomass_scale[tov_OBarleyPeaCloverGrass] = 0.857;
	g_biomass_scale[tov_OSBarleySilage] = 0.857 * 0.8;
	g_biomass_scale[tov_OCarrots] = 0.7857 * 0.8;
	g_biomass_scale[tov_OCloverGrassGrazed1] = 1.1;
	g_biomass_scale[tov_OCloverGrassGrazed2] = 1.1;
	g_biomass_scale[tov_OCloverGrassSilage1] = 1.1;
	g_biomass_scale[tov_OFieldPeas] = 0.857 * 0.8;
	g_biomass_scale[tov_OFieldPeasSilage] = 0.857 * 0.8;
	g_biomass_scale[tov_OFirstYearDanger] = 0.0;
	g_biomass_scale[tov_OGrazingPigs] = 0.7857 * 0.8;
	g_biomass_scale[tov_OOats] = 0.857 * 0.8;
	g_biomass_scale[tov_OPermanentGrassGrazed] = 0.7857;
	g_biomass_scale[tov_OPotatoes] = 0.857 * 0.8;
	g_biomass_scale[tov_OSeedGrass1] = 0.7857;
	g_biomass_scale[tov_OSeedGrass2] = 0.7857;
	g_biomass_scale[tov_OSetaside] = 0.7857;
	g_biomass_scale[tov_OSpringBarley] = 0.857 * 0.8;
	g_biomass_scale[tov_OSpringBarleyExt] = 0.857 * 0.8 * 0.8;
	g_biomass_scale[tov_OSpringBarleyClover] = 0.857 * 0.8;
	g_biomass_scale[tov_OSpringBarleyGrass] = 0.857 * 0.8;
	g_biomass_scale[tov_OSpringBarleyPigs] = 0.857 * 0.8;
	g_biomass_scale[tov_OTriticale] = 1.00 * 0.8;
	g_biomass_scale[tov_OWinterBarley] = 0.857 * 0.8;
	g_biomass_scale[tov_OWinterBarleyExt] = 0.857 * 0.8 * 0.8;
	g_biomass_scale[tov_OWinterRape] = 1.071 * 0.8;
	g_biomass_scale[tov_OWinterRye] = 0.857 * 0.8;
	g_biomass_scale[ tov_OWinterWheat ] = 1.00 * 0.8;
	g_biomass_scale[ tov_OWinterWheatUndersown ] = 1.00 * 0.8;
	g_biomass_scale[ tov_OWinterWheatUndersownExt ] = 1.00 * 0.8 * 0.8;
	g_biomass_scale[tov_PermanentGrassGrazed] = 1.1;
	g_biomass_scale[tov_PermanentGrassLowYield] = 1.0;
	g_biomass_scale[tov_PermanentGrassTussocky] = 0.7857;
	g_biomass_scale[tov_PermanentSetaside] = 0.7857;
	g_biomass_scale[tov_Potatoes] = 0.857;
	g_biomass_scale[tov_PotatoesIndustry] = 0.857;
	g_biomass_scale[tov_SeedGrass1] = 0.7857;
	g_biomass_scale[tov_SeedGrass2] = 0.7857;
	g_biomass_scale[tov_OSeedGrass1] = 0.7857;
	g_biomass_scale[tov_OSeedGrass2] = 0.7857;
	g_biomass_scale[tov_Setaside] = 0.7857;
	g_biomass_scale[tov_SpringBarley] = 0.857;
	g_biomass_scale[tov_SpringBarleySpr] = 0.857;
	g_biomass_scale[tov_SpringBarleyPTreatment] = 0.857;
	g_biomass_scale[tov_SpringBarleySKManagement] = 0.857;
	g_biomass_scale[tov_SpringBarleyCloverGrass] = 0.857;
	g_biomass_scale[tov_SpringBarleyGrass] = 0.857;
	g_biomass_scale[tov_SpringBarleySeed] = 0.857;
	g_biomass_scale[tov_SpringBarleySilage] = 0.857;
	g_biomass_scale[tov_SpringRape] = 1.071;
	g_biomass_scale[tov_SpringWheat] = 1.00;
	g_biomass_scale[tov_Triticale] = 1.00;
	g_biomass_scale[tov_WinterBarley] = 0.857;
	g_biomass_scale[tov_WinterRape] = 1.071;
	g_biomass_scale[tov_WinterRye] = 0.857;
	g_biomass_scale[tov_WinterWheat] = 1.00;  // This gives approx 18 tonnes biomass for WW
	g_biomass_scale[tov_WinterWheatShort] = 1.00;
	g_biomass_scale[tov_AgroChemIndustryCereal] = 1.00;
	g_biomass_scale[tov_WWheatPControl] = 1.00;
	g_biomass_scale[tov_WWheatPToxicControl] = 1.00;
	g_biomass_scale[tov_WWheatPTreatment] = 1.00;
	g_biomass_scale[tov_WinterWheatStrigling] = 1.00;
	g_biomass_scale[tov_WinterWheatStriglingCulm] = 1.00;
	g_biomass_scale[tov_WinterWheatStriglingSingle] = 1.00;
	g_biomass_scale[tov_SpringBarleyCloverGrassStrigling] = 0.857;
	g_biomass_scale[tov_SpringBarleyStrigling] = 0.857;
	g_biomass_scale[tov_SpringBarleyStriglingSingle] = 0.857;
	g_biomass_scale[tov_SpringBarleyStriglingCulm] = 0.857;
	g_biomass_scale[tov_MaizeStrigling] = 0.857;
	g_biomass_scale[tov_WinterRapeStrigling] = 1.071;
	g_biomass_scale[tov_WinterRyeStrigling] = 0.857;
	g_biomass_scale[tov_WinterBarleyStrigling] = 0.857;
	g_biomass_scale[tov_FieldPeasStrigling] = 0.857;
	g_biomass_scale[tov_SpringBarleyPeaCloverGrassStrigling] = 0.857;
	g_biomass_scale[tov_YoungForest] = 0.7857 * 0.67;
	g_biomass_scale[tov_Wasteland] = 0.7857 * 0.67;
	g_biomass_scale[tov_WaterBufferZone] = 0.567;

	g_biomass_scale[tov_PLWinterWheat] = 1.00;
	g_biomass_scale[tov_PLWinterRape] = 1.071;
	g_biomass_scale[tov_PLWinterBarley] = 0.857;
	g_biomass_scale[tov_PLWinterTriticale] = 1.00;
	g_biomass_scale[tov_PLWinterRye] = 0.857;
	g_biomass_scale[tov_PLSpringWheat] = 1.00;
	g_biomass_scale[tov_PLSpringBarley] = 0.857;
	g_biomass_scale[tov_PLMaize] = 1.00;
	g_biomass_scale[tov_PLMaizeSilage] = 1.00;
	g_biomass_scale[tov_PLPotatoes] = 0.857;
	g_biomass_scale[tov_PLBeet] = 0.857;
	g_biomass_scale[tov_PLFodderLucerne1] = 1.2;
	g_biomass_scale[tov_PLFodderLucerne2] = 1.2;
	g_biomass_scale[tov_PLCarrots] = 0.7857;
	g_biomass_scale[tov_PLSpringBarleySpr] = 0.857;
	g_biomass_scale[tov_PLWinterWheatLate] = 1.00;
	g_biomass_scale[tov_PLBeetSpr] = 0.857;
	g_biomass_scale[tov_PLBeans] = 0.857;

	g_biomass_scale[tov_NLWinterWheat] = 1.00;
	g_biomass_scale[tov_NLSpringBarley] = 0.857;
	g_biomass_scale[tov_NLMaize] = 1.00;
	g_biomass_scale[tov_NLPotatoes] = 0.857;
	g_biomass_scale[tov_NLBeet] = 0.857;
	g_biomass_scale[tov_NLCarrots] = 0.7857;
	g_biomass_scale[tov_NLCabbage] = 0.7857;
	g_biomass_scale[tov_NLTulips] = 0.7857;
	g_biomass_scale[tov_NLGrassGrazed1] = 1.2;
	g_biomass_scale[tov_NLGrassGrazed1Spring] = 1.2;
	g_biomass_scale[tov_NLGrassGrazed2] = 1.2;
	g_biomass_scale[tov_NLGrassGrazedLast] = 1.2;
	g_biomass_scale[tov_NLPermanentGrassGrazed] = 1.2;
	g_biomass_scale[tov_NLSpringBarleySpring] = 0.857;
	g_biomass_scale[tov_NLMaizeSpring] = 1.00;
	g_biomass_scale[tov_NLPotatoesSpring] = 0.857;
	g_biomass_scale[tov_NLBeetSpring] = 0.857;
	g_biomass_scale[tov_NLCarrotsSpring] = 0.7857;
	g_biomass_scale[tov_NLCabbageSpring] = 0.7857;
	g_biomass_scale[tov_NLCatchPeaCrop] = 0.857;


	g_biomass_scale[tov_DummyCropPestTesting] = 1.00;	// just for testing of pesticide spraying distribution

	if (l_el_read_bug_percentage_file.value()) {
		//ReadBugPercentageFile();
		return;
	}

	g_weed_percent[tov_Carrots] = 0.1;
	g_weed_percent[ tov_BroadBeans ] = 0.1;
	g_weed_percent[ tov_Maize ] = 0.05;
	g_weed_percent[tov_MaizeSilage] = 0.05;
	g_weed_percent[tov_OMaizeSilage] = 0.05;
	g_weed_percent[tov_OCarrots] = 0.1;
	g_weed_percent[tov_Potatoes] = 0.1;
	g_weed_percent[tov_OPotatoes] = 0.1;
	g_weed_percent[tov_FodderGrass] = 0.1;
	g_weed_percent[tov_CloverGrassGrazed1] = 0.1;
	g_weed_percent[tov_CloverGrassGrazed2] = 0.1;
	g_weed_percent[tov_OCloverGrassGrazed1] = 0.1;
	g_weed_percent[tov_OCloverGrassGrazed2] = 0.1;
	g_weed_percent[tov_SpringBarley] = 0.1;
	g_weed_percent[tov_SpringBarleySpr] = 0.1;
	g_weed_percent[tov_SpringBarleyPTreatment] = 0.1;
	g_weed_percent[tov_SpringBarleySKManagement] = 0.1;
	g_weed_percent[tov_WinterWheat] = 0.1;
	g_weed_percent[tov_WinterWheatShort] = 0.1;
	g_weed_percent[tov_SpringBarleySilage] = 0.1;
	g_weed_percent[tov_SpringBarleySeed] = 0.1;
	g_weed_percent[tov_OGrazingPigs] = 0.1;
	g_weed_percent[tov_OCloverGrassSilage1] = 0.1;
	g_weed_percent[tov_SpringBarleyCloverGrass] = 0.1;
	g_weed_percent[tov_OSpringBarleyPigs] = 0.1;
	g_weed_percent[tov_OBarleyPeaCloverGrass] = 0.1;
	g_weed_percent[tov_OSpringBarley] = 0.1;
	g_weed_percent[tov_OSpringBarleyExt] = 0.1;
	g_weed_percent[tov_OSBarleySilage] = 0.1;
	g_weed_percent[ tov_OWinterWheat ] = 0.1;
	g_weed_percent[ tov_OWinterWheatUndersown ] = 0.1;
	g_weed_percent[ tov_OWinterWheatUndersownExt ] = 0.1;
	g_weed_percent[tov_WinterRape] = 0.05;
	g_weed_percent[tov_OWinterRape] = 0.05;
	g_weed_percent[tov_OWinterRye] = 0.1;
	g_weed_percent[tov_OWinterBarley] = 0.1;
	g_weed_percent[tov_OWinterBarleyExt] = 0.1;
	g_weed_percent[tov_WinterBarley] = 0.1;
	g_weed_percent[tov_WinterRye] = 0.1;
	g_weed_percent[ tov_OFieldPeas ] = 0.1;
	g_weed_percent[tov_OFieldPeas] = 0.1;
	g_weed_percent[ tov_OFieldPeasSilage ] = 0.1;
	g_weed_percent[tov_OOats] = 0.1;
	g_weed_percent[tov_Oats] = 0.1;
	g_weed_percent[tov_Heath] = 0.1;
	g_weed_percent[tov_OrchardCrop] = 0.1;
	g_weed_percent[ tov_FieldPeas ] = 0.1;
	g_weed_percent[ tov_FieldPeasSilage ] = 0.1;
	g_weed_percent[tov_SeedGrass1] = 0.1;
	g_weed_percent[tov_SeedGrass2] = 0.1;
	g_weed_percent[tov_OSeedGrass1] = 0.15;
	g_weed_percent[tov_OSeedGrass2] = 0.15;
	g_weed_percent[tov_OPermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_Setaside] = 0.1;
	g_weed_percent[tov_OSetaside] = 0.1;
	g_weed_percent[tov_PermanentSetaside] = 0.1;
	g_weed_percent[tov_PermanentGrassLowYield] = 0.1;
	g_weed_percent[tov_PermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_PermanentGrassTussocky] = 0.1;
	g_weed_percent[tov_FodderBeet] = 0.1;
	g_weed_percent[tov_SugarBeet] = 0.1;
	g_weed_percent[tov_OFodderBeet] = 0.1;
	g_weed_percent[tov_NaturalGrass] = 0.1;
	g_weed_percent[tov_None] = 0.1;
	g_weed_percent[tov_NoGrowth] = 0.1;
	g_weed_percent[tov_OFirstYearDanger] = 0.1;
	g_weed_percent[tov_Triticale] = 0.1;
	g_weed_percent[tov_OTriticale] = 0.1;
	g_weed_percent[tov_WWheatPControl] = 0.1;
	g_weed_percent[tov_WWheatPToxicControl] = 0.1;
	g_weed_percent[tov_WWheatPTreatment] = 0.1;
	g_weed_percent[tov_AgroChemIndustryCereal] = 0.1;
	g_weed_percent[tov_WinterWheatStrigling] = 0.1;
	g_weed_percent[tov_WinterWheatStriglingSingle] = 0.1;
	g_weed_percent[tov_WinterWheatStriglingCulm] = 0.1;
	g_weed_percent[tov_SpringBarleyCloverGrassStrigling] = 0.1;
	g_weed_percent[tov_SpringBarleyStrigling] = 0.1;
	g_weed_percent[tov_SpringBarleyStriglingSingle] = 0.1;
	g_weed_percent[tov_SpringBarleyStriglingCulm] = 0.1;
	g_weed_percent[tov_MaizeStrigling] = 0.1;
	g_weed_percent[tov_WinterRapeStrigling] = 0.1;
	g_weed_percent[tov_WinterRyeStrigling] = 0.1;
	g_weed_percent[tov_WinterBarleyStrigling] = 0.1;
	g_weed_percent[tov_FieldPeasStrigling] = 0.1;
	g_weed_percent[tov_SpringBarleyPeaCloverGrassStrigling] = 0.1;
	g_weed_percent[tov_YoungForest] = 0.1;
	g_weed_percent[tov_Wasteland] = 0.1;
	g_weed_percent[tov_WaterBufferZone] = 0.1;

	g_weed_percent[tov_PLWinterWheat] = 0.1;
	g_weed_percent[tov_PLWinterRape] = 0.05;
	g_weed_percent[tov_PLWinterBarley] = 0.1;
	g_weed_percent[tov_PLWinterTriticale] = 0.1;
	g_weed_percent[tov_PLWinterRye] = 0.1;
	g_weed_percent[tov_PLSpringWheat] = 0.1;
	g_weed_percent[tov_PLSpringBarley] = 0.1;
	g_weed_percent[tov_PLMaize] = 0.1;
	g_weed_percent[tov_PLMaizeSilage] = 0.1;
	g_weed_percent[tov_PLPotatoes] = 0.1;
	g_weed_percent[tov_PLBeet] = 0.1;
	g_weed_percent[tov_PLFodderLucerne1] = 0.1;
	g_weed_percent[tov_PLFodderLucerne2] = 0.1;
	g_weed_percent[tov_PLCarrots] = 0.1;
	g_weed_percent[tov_PLSpringBarleySpr] = 0.1;
	g_weed_percent[tov_PLWinterWheatLate] = 0.1;
	g_weed_percent[tov_PLBeetSpr] = 0.1;
	g_weed_percent[tov_PLBeans] = 0.1;

	g_weed_percent[tov_NLWinterWheat] = 0.1;
	g_weed_percent[tov_NLSpringBarley] = 0.1;
	g_weed_percent[tov_NLMaize] = 0.1;
	g_weed_percent[tov_NLPotatoes] = 0.1;
	g_weed_percent[tov_NLBeet] = 0.1;
	g_weed_percent[tov_NLCarrots] = 0.1;
	g_weed_percent[tov_NLCabbage] = 0.1;
	g_weed_percent[tov_NLTulips] = 0.1;
	g_weed_percent[tov_NLGrassGrazed1] = 0.1;
	g_weed_percent[tov_NLGrassGrazed1Spring] = 0.1;
	g_weed_percent[tov_NLGrassGrazed2] = 0.1;
	g_weed_percent[tov_NLGrassGrazedLast] = 0.1;
	g_weed_percent[tov_NLPermanentGrassGrazed] = 0.1;
	g_weed_percent[tov_NLSpringBarleySpring] = 0.1;
	g_weed_percent[tov_NLMaizeSpring] = 0.1;
	g_weed_percent[tov_NLPotatoesSpring] = 0.1;
	g_weed_percent[tov_NLBeetSpring] = 0.1;
	g_weed_percent[tov_NLCarrotsSpring] = 0.1;
	g_weed_percent[tov_NLCabbageSpring] = 0.1;
	g_weed_percent[tov_NLCatchPeaCrop] = 0.1;

	g_bug_percent_a[tov_Carrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_MaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Maize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Potatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FodderGrass] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleySpr] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_WinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatShort] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_SpringBarleySilage] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleySeed] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OGrazingPigs] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSpringBarleyExt] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_OSBarleySilage] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[ tov_OWinterWheat ] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_SpringRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_OWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_OWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_OWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OWinterBarleyExt] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_OFieldPeas] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OFieldPeasSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OrchardCrop] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OOats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Oats] = EL_BUG_PERCENT_A;
	g_bug_percent_a[ tov_BroadBeans ] = EL_BUG_PERCENT_A;
	g_bug_percent_a[ tov_FieldPeas ] = EL_BUG_PERCENT_A;
	g_bug_percent_a[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OSeedGrass1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OSeedGrass2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SeedGrass1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_SeedGrass2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_Heath] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_b[tov_Heath] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_c[tov_Heath] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_d[tov_Heath] = EL_BUG_PERCENT_D;
	g_bug_percent_a[tov_Setaside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_OSetaside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_PermanentSetaside] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_FodderBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_SugarBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OFodderBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NaturalGrass] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_None] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NoGrowth] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OFirstYearDanger] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_Triticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_OTriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WWheatPControl] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WWheatPToxicControl] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WWheatPTreatment] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatStrigling] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_SpringBarleyCloverGrassStrigling] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_MaizeStrigling] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_WinterRapeStrigling] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_WinterRyeStrigling] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_FieldPeasStrigling] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_YoungForest] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_Wasteland] = EL_BUG_PERCENT_Edges_A;
	g_bug_percent_a[tov_WaterBufferZone] = EL_BUG_PERCENT_Edges_A;

	g_bug_percent_a[tov_PLWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_PLWinterRape] = EL_BUG_PERCENT_WR_A;
	g_bug_percent_a[tov_PLWinterTriticale] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_PLWinterBarley] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLWinterRye] = EL_BUG_PERCENT_WRy_A;
	g_bug_percent_a[tov_PLSpringWheat] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLMaizeSilage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLFodderLucerne1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PLFodderLucerne2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_PLCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_PLWinterWheatLate] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_PLBeetSpr] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_PLBeans] = EL_BUG_PERCENT_A;

	g_bug_percent_a[tov_NLWinterWheat] = EL_BUG_PERCENT_WW_A;
	g_bug_percent_a[tov_NLSpringBarley] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_NLMaize] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLPotatoes] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLBeet] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCarrots] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCabbage] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLTulips] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLGrassGrazed1] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazed2] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_G_A;
	g_bug_percent_a[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_SB_A;
	g_bug_percent_a[tov_NLMaizeSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLPotatoesSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLBeetSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCarrotsSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCabbageSpring] = EL_BUG_PERCENT_A;
	g_bug_percent_a[tov_NLCatchPeaCrop] = EL_BUG_PERCENT_A;


	g_bug_percent_b[tov_Carrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_MaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Maize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Potatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FodderGrass] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_SpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleySpr] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_WinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatShort] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_SpringBarleySilage] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleySeed] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OGrazingPigs] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSBarleySilage] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_OSpringBarleyExt] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[ tov_OWinterWheat ] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_SpringRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_OWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_OWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_OWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OWinterBarleyExt] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_OFieldPeas] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OFieldPeasSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OrchardCrop] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OOats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Oats] = EL_BUG_PERCENT_B;
	g_bug_percent_b[ tov_BroadBeans ] = EL_BUG_PERCENT_B;
	g_bug_percent_b[ tov_FieldPeas ] = EL_BUG_PERCENT_B;
	g_bug_percent_b[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_SeedGrass1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_SeedGrass2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OSeedGrass1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OSeedGrass2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_Setaside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_OSetaside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_PermanentSetaside] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_FodderBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_SugarBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OFodderBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NaturalGrass] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_None] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NoGrowth] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OFirstYearDanger] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_Triticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_OTriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WWheatPControl] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WWheatPToxicControl] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WWheatPTreatment] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatStrigling] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_SpringBarleyCloverGrassStrigling] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_MaizeStrigling] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_WinterRapeStrigling] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_WinterRyeStrigling] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_FieldPeasStrigling] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_YoungForest] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_b[tov_Wasteland] = EL_BUG_PERCENT_Edges_B;
	g_bug_percent_a[tov_WaterBufferZone] = EL_BUG_PERCENT_Edges_B;

	g_bug_percent_b[tov_PLWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_PLWinterRape] = EL_BUG_PERCENT_WR_B;
	g_bug_percent_b[tov_PLWinterTriticale] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_PLWinterBarley] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLWinterRye] = EL_BUG_PERCENT_WRy_B;
	g_bug_percent_b[tov_PLSpringWheat] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLMaizeSilage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLFodderLucerne1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PLFodderLucerne2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_PLCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_PLWinterWheatLate] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_PLBeetSpr] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_PLBeans] = EL_BUG_PERCENT_B;

	g_bug_percent_b[tov_NLWinterWheat] = EL_BUG_PERCENT_WW_B;
	g_bug_percent_b[tov_NLSpringBarley] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_NLMaize] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLPotatoes] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLBeet] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCarrots] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCabbage] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLTulips] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLGrassGrazed1] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazed2] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_G_B;
	g_bug_percent_b[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_SB_B;
	g_bug_percent_b[tov_NLMaizeSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLPotatoesSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLBeetSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCarrotsSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCabbageSpring] = EL_BUG_PERCENT_B;
	g_bug_percent_b[tov_NLCatchPeaCrop] = EL_BUG_PERCENT_B;

	g_bug_percent_c[tov_Carrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_MaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Maize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Potatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FodderGrass] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_SpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleySpr] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_WinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatShort] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_SpringBarleySilage] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleySeed] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OGrazingPigs] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSBarleySilage] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_OSpringBarleyExt] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[ tov_OWinterWheat ] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_SpringRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_OWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_OWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_OWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OWinterBarleyExt] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_OFieldPeas] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OFieldPeasSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OrchardCrop] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OOats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Oats] = EL_BUG_PERCENT_C;
	g_bug_percent_c[ tov_FieldPeas ] = EL_BUG_PERCENT_C;
	g_bug_percent_c[ tov_BroadBeans ] = EL_BUG_PERCENT_C;
	g_bug_percent_c[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_SeedGrass1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_SeedGrass2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OSeedGrass1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OSeedGrass2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_Setaside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_OSetaside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_PermanentSetaside] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_FodderBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_SugarBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OFodderBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NaturalGrass] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_None] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NoGrowth] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OFirstYearDanger] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_Triticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_OTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WWheatPControl] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WWheatPToxicControl] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WWheatPTreatment] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatStrigling] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_SpringBarleyCloverGrassStrigling] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_MaizeStrigling] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_WinterRapeStrigling] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_WinterRyeStrigling] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_FieldPeasStrigling] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_YoungForest] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_c[tov_Wasteland] = EL_BUG_PERCENT_Edges_C;
	g_bug_percent_a[tov_WaterBufferZone] = EL_BUG_PERCENT_Edges_C;

	g_bug_percent_c[tov_PLWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_PLWinterRape] = EL_BUG_PERCENT_WR_C;
	g_bug_percent_c[tov_PLWinterTriticale] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_PLWinterBarley] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLWinterRye] = EL_BUG_PERCENT_WRy_C;
	g_bug_percent_c[tov_PLSpringWheat] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLMaizeSilage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLFodderLucerne1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PLFodderLucerne2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_PLCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_PLWinterWheatLate] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_PLBeetSpr] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_PLBeans] = EL_BUG_PERCENT_C;

	g_bug_percent_c[tov_NLWinterWheat] = EL_BUG_PERCENT_WW_C;
	g_bug_percent_c[tov_NLSpringBarley] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_NLMaize] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLPotatoes] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLBeet] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCarrots] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCabbage] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLTulips] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLGrassGrazed1] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazed2] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_G_C;
	g_bug_percent_c[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_SB_C;
	g_bug_percent_c[tov_NLMaizeSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLPotatoesSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLBeetSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCarrotsSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCabbageSpring] = EL_BUG_PERCENT_C;
	g_bug_percent_c[tov_NLCatchPeaCrop] = EL_BUG_PERCENT_C;

	g_bug_percent_d[tov_Carrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Maize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_MaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Potatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FodderGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_CloverGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_CloverGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCloverGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCloverGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySpr] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyPTreatment] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySKManagement] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatShort] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleySeed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OGrazingPigs] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OCloverGrassSilage1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyCloverGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSpringBarleyPigs] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OBarleyPeaCloverGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSBarleySilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSpringBarleyExt] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_OWinterWheat ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_OWinterWheatUndersown ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_OWinterWheatUndersownExt ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OWinterBarleyExt] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFieldPeas] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFieldPeasSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OrchardCrop] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OOats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Oats] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_BroadBeans ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_FieldPeas ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[ tov_FieldPeasSilage ] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SeedGrass1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SeedGrass2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSeedGrass1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSeedGrass2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OPermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Setaside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OSetaside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentSetaside] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentGrassLowYield] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PermanentGrassTussocky] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FodderBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SugarBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFodderBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NaturalGrass] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_None] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NoGrowth] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OFirstYearDanger] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Triticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_OTriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WWheatPControl] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WWheatPToxicControl] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WWheatPTreatment] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_AgroChemIndustryCereal] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatStriglingCulm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterWheatStriglingSingle] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyCloverGrassStrigling] = -EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyStriglingSingle] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyStriglingCulm] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_MaizeStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRapeStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterRyeStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_WinterBarleyStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_FieldPeasStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_SpringBarleyPeaCloverGrassStrigling] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_YoungForest] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_Wasteland] = EL_BUG_PERCENT_D;
	g_bug_percent_a[tov_WaterBufferZone] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_PLWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterRape] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterTriticale] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterRye] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLSpringWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLMaizeSilage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLFodderLucerne1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLFodderLucerne2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLSpringBarleySpr] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLWinterWheatLate] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLBeetSpr] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_PLBeans] = EL_BUG_PERCENT_D;

	g_bug_percent_d[tov_NLWinterWheat] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLSpringBarley] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLMaize] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLPotatoes] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLBeet] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCarrots] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCabbage] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLTulips] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazed1] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazed1Spring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazed2] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLGrassGrazedLast] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLPermanentGrassGrazed] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLSpringBarleySpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLMaizeSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLPotatoesSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLBeetSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCarrotsSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCabbageSpring] = EL_BUG_PERCENT_D;
	g_bug_percent_d[tov_NLCatchPeaCrop] = EL_BUG_PERCENT_D;


	//05.03.13 - modifications for farmer decision making, AM
	Clean_CropDataStorage(0);
	Clean_CropDataStorage(1);
	m_crop_index = 0;
}

void VegElement::ReadBugPercentageFile( void ) {
    FILE* lm_ifile=fopen(l_el_bug_percentage_file.value(), "r" );
  if ( !lm_ifile ) {
    g_msg->Warn( WARN_FILE, "PlantGrowthData::ReadBugPercentageFile(): Unable to open file", l_el_bug_percentage_file.value() );
    exit( 1 );
  }
  for ( int i = 0; i < tov_Undefined; i++ ) {
    int vegnum;
    // **cjt** modified 31/01/2004
    float weedpercent, bugpercent_a, bugpercent_b, bugpercent_c, bugpercent_d;
    if ( 2 != fscanf( lm_ifile, "%d %f %f %f %f %f", & vegnum, & weedpercent, & bugpercent_a, & bugpercent_b,
         & bugpercent_c, & bugpercent_d ) ) {
           char vegnums[ 20 ];
           sprintf( vegnums, "%d", tov_Undefined );
           g_msg->Warn( WARN_FILE,
                "VegElement::ReadBugPercentageFile(): Unable to read"
                " sufficient number of int/double pairs from bug percentage file."" Lines expected:", vegnums );
           exit( 1 );
    }
    FloatToDouble( g_weed_percent[ vegnum ], weedpercent);
	FloatToDouble( g_bug_percent_a[ vegnum ] , bugpercent_a);
    FloatToDouble( g_bug_percent_b[ vegnum ] , bugpercent_b);
    FloatToDouble( g_bug_percent_c[ vegnum ] , bugpercent_c);
    FloatToDouble( g_bug_percent_d[ vegnum ] , bugpercent_d);
  }
  fclose( lm_ifile );
}

bool VegElement::IsCereal(void)
{
	// NB Maize here is classed as cereal
	switch (m_lastsownvegtype)
	{
	case tov_SpringBarley:
	case tov_SpringBarleySpr:
	case tov_WinterBarley:
	case tov_SpringWheat:
	case tov_WinterWheat:
	case tov_WinterRye:
	case tov_Oats:
	case tov_Triticale:
	case tov_SpringBarleySeed:
	case tov_SpringBarleyStrigling:
	case tov_SpringBarleyStriglingSingle:
	case tov_SpringBarleyStriglingCulm:
	case tov_WinterWheatStrigling:
	case tov_WinterWheatStriglingSingle:
	case tov_WinterWheatStriglingCulm:
	case tov_OWinterBarley:
	case tov_OWinterBarleyExt:
	case tov_OWinterRye:
	case tov_SpringBarleyGrass:
	case tov_SpringBarleyCloverGrass:
	case tov_OBarleyPeaCloverGrass:
	case tov_SpringBarleyPeaCloverGrassStrigling:
	case tov_OSpringBarley:
	case tov_OSpringBarleyPigs:
	case tov_OWinterWheatUndersown:
	case tov_OWinterWheat:
	case tov_OOats:
	case tov_OTriticale:
	case tov_WWheatPControl:
	case tov_WWheatPToxicControl:
	case tov_WWheatPTreatment:
	case tov_AgroChemIndustryCereal:
	case tov_SpringBarleyPTreatment:
	case tov_SpringBarleySKManagement:
	case tov_OSpringBarleyExt:
	case tov_OSpringBarleyGrass:
	case tov_OSBarleySilage:
	case tov_OSpringBarleyClover:
	case tov_PLWinterWheat:
	case tov_PLWinterBarley:
	case tov_PLWinterRye:
	case tov_PLWinterTriticale:
	case tov_PLSpringWheat:
	case tov_PLSpringBarley:
	case tov_PLSpringBarleySpr:
	case tov_PLWinterWheatLate:
	case tov_PLMaize:
	case tov_PLMaizeSilage:
	case tov_Maize:
	case tov_MaizeSilage:
	case tov_MaizeStrigling:
	case tov_NLSpringBarley:
	case tov_NLWinterWheat:
	case tov_NLMaize:
	case tov_NLSpringBarleySpring:
	case tov_NLMaizeSpring:
		return true;
	default: // No matching code so is should not be cereal
		return false;
	}
}

bool VegElement::IsMatureCereal(void)
{
	switch (m_vege_type)
	{
	case tov_SpringBarley:
	case tov_SpringBarleySpr:
	case tov_WinterBarley:
	case tov_SpringWheat:
	case tov_WinterWheat:
	case tov_WinterRye:
	case tov_Oats:
	case tov_Triticale:
	case tov_SpringBarleySeed:
	case tov_SpringBarleyStrigling:
	case tov_SpringBarleyStriglingSingle:
	case tov_SpringBarleyStriglingCulm:
	case tov_WinterWheatStrigling:
	case tov_WinterWheatStriglingSingle:
	case tov_WinterWheatStriglingCulm:
	case tov_OWinterBarley:
	case tov_OWinterBarleyExt:
	case tov_OWinterRye:
	case tov_SpringBarleyGrass:
	case tov_SpringBarleyCloverGrass:
	case tov_SpringBarleyPeaCloverGrassStrigling:
	case tov_OSpringBarley:
	case tov_OSpringBarleyPigs:
	case tov_OWinterWheatUndersown:
	case tov_OWinterWheat:
	case tov_OOats:
	case tov_OTriticale:
	case tov_WWheatPControl:
	case tov_WWheatPToxicControl:
	case tov_WWheatPTreatment:
	case tov_AgroChemIndustryCereal:
	case tov_SpringBarleyPTreatment:
	case tov_SpringBarleySKManagement:
	case tov_OSpringBarleyExt:
	case tov_OSpringBarleyGrass:
	case tov_OSpringBarleyClover:
	case tov_PLWinterWheat:
	case tov_PLWinterBarley:
	case tov_PLWinterRye:
	case tov_PLWinterTriticale:
	case tov_PLSpringWheat:
	case tov_PLSpringBarley:
	case tov_PLSpringBarleySpr:
	case tov_PLWinterWheatLate:
	case tov_NLSpringBarley:
	case tov_NLWinterWheat:
	case tov_NLSpringBarleySpring:
		return true;
	default: // No matching code so is should not be mature cereal
		return false;
	}
}

bool VegElement::IsMaize(void)
{
	/** Currently (18/03/2015) only used for goose foraging, so silage maize does not produce grain */
	switch (m_vege_type)
	{
		case tov_Maize:
		case tov_OMaizeSilage:
		case tov_MaizeSilage:
		case tov_PLMaize:
		case tov_PLMaizeSilage:
		case tov_NLMaize:
		case tov_NLMaizeSpring:
		  return true;
		default: // No matching code so is should not be maize
		  return false;
	  }
}


inline bool VegElement::IsGrass()
{
	switch (m_vege_type)
	{
	case tov_NaturalGrass:
	case tov_PermanentGrassGrazed:
	case tov_PermanentGrassLowYield:
	case tov_PermanentGrassTussocky:
	case tov_PermanentSetaside:
	case tov_Setaside:
	case tov_SeedGrass1:
	case tov_SeedGrass2:
	case tov_OSeedGrass1:
	case tov_OSeedGrass2:
	case tov_CloverGrassGrazed1:
	case tov_CloverGrassGrazed2:
	case tov_OCloverGrassGrazed1:
	case tov_OCloverGrassGrazed2:
	case tov_OrchardCrop:
	case tov_YoungForest:
	case tov_FodderGrass:
	case tov_Heath:
	case tov_PLFodderLucerne1:
	case tov_PLFodderLucerne2:
	case tov_NLGrassGrazed1:
	case tov_NLGrassGrazed1Spring:
	case tov_NLGrassGrazed2:
	case tov_NLPermanentGrassGrazed:
	case tov_WaterBufferZone:
		return true;
	default: return false;
	}
}

inline bool VegElement::IsGooseGrass() {
	switch (m_vege_type) {
		case tov_NaturalGrass:
		case tov_PermanentGrassGrazed:
		case tov_PermanentGrassLowYield:
		case tov_PermanentGrassTussocky:
			return true;
		default: break;
	}
	switch (m_lastsownvegtype) {
		case tov_SeedGrass1:
		case tov_SeedGrass2:
		case tov_OSeedGrass1:
		case tov_OSeedGrass2:
		case tov_CloverGrassGrazed1:
		case tov_CloverGrassGrazed2:
		case tov_OCloverGrassGrazed1:
		case tov_OCloverGrassGrazed2:
		case tov_FodderGrass:
		case tov_PLFodderLucerne1:
		case tov_PLFodderLucerne2:
		case tov_NLGrassGrazed1:
		case tov_NLGrassGrazed1Spring:
		case tov_NLGrassGrazed2:
		case tov_NLPermanentGrassGrazed:
			return true;
		default:
			return false;
	}

}



void VegElement::RecalculateBugsNStuff(void) {
	/** This is the heart of the dynamics of vegetation elements. It calculates vegetation cover and uses this to determine vegetation biomass.
	It also calculates spilled grain and goose forage, as well a calculating insect biomass, vegetation density and dead biomass*/
	double newgrowth = 0;
	m_veg_cover = 1.0 - (exp(m_LAtotal * -0.6)); // Beer's Law to give cover
	double usefull_veg_cover = 1.0 - (exp(m_LAgreen * -0.4)); // This is used to calc growth rate
	// Need gloabal radiation today
	double glrad = g_landscape_p->SupplyGlobalRadiation();
	// This is different for maize (a C4 plant)
	int ptype;
	if ((m_vege_type == tov_Maize) || (m_vege_type == tov_OMaizeSilage) || (m_vege_type == tov_MaizeSilage) || (m_vege_type == tov_MaizeStrigling)) ptype = 1; else ptype = 0;
	int index = (int)floor(0.5 + g_landscape_p->SupplyTemp()) + 30; // There are 30 negative temps
	double radconv = c_SolarConversion[ptype][index];
	if (m_LAtotal >= m_oldLAtotal) {
		// we are in positive growth so grow depending on our equation
		newgrowth = usefull_veg_cover * glrad * radconv * g_biomass_scale[m_vege_type];
		if (m_owner_index != -1) { // This only works because only crops and similar structures have owners
			int fintensity = g_landscape_p->SupplyFarmIntensity(m_poly);
			if (fintensity == 1) {
				// 1 means extensive, so reduce vegetation biomass by 20%
				// NB this cannot be used with extensive crop types otherwise you get an additional 20% reduction
				// This way of doing things provides a quick and dirty general effect.
				m_veg_biomass += newgrowth * 0.8;
			}
			else m_veg_biomass += newgrowth;
		}
		else m_veg_biomass += newgrowth;
	}
	else {
		// Negative growth - so shrink proportional to the loss in LAI Total
		if (m_oldLAtotal > 0) {
			m_veg_biomass *= (m_LAtotal / m_oldLAtotal);
		}
	}
	/** Here we also want to know how much biomass we have on the field in total. So we multiply the current biomass by area */
	m_total_biomass = m_veg_biomass * m_area;
	m_total_biomass_old = m_total_biomass;
	// NB The m_weed_biomass is calculated directly from the curve in Curves.pre
	// rather than going through the rigmorole of converting leaf-area index

	// Another thing to do is to calculate mean vegetation digestability
	// This is a 32-day running average of the amount of new growth per day divided by veg biomass
	// With a minimum value of 0.5
	++m_newoldgrowthindex &= 31;
	if (m_veg_biomass > 0) {
		switch (m_vege_type) {
		case tov_NoGrowth:
		case tov_None:
		case tov_OFirstYearDanger:
			m_digestability = 0.0;
			break;
		case tov_OPotatoes:
		case tov_Maize:
		case tov_MaizeSilage:
		case tov_OMaizeSilage:
		case tov_MaizeStrigling:
		case tov_Potatoes:
		case tov_PotatoesIndustry:
		case tov_PLMaize:
		case tov_PLMaizeSilage:
		case tov_PLPotatoes:
		case tov_NLMaize:
		case tov_NLPotatoes:
		case tov_NLMaizeSpring:
		case tov_NLPotatoesSpring:
			m_digestability = 0.5;
			break;
		default:
			//m_oldnewgrowth[m_newoldgrowthindex]=(newgrowth/m_veg_biomass);
			m_oldnewgrowth[m_newoldgrowthindex] = (newgrowth);
			m_newgrowthsum = 0.0;
			for (int i = 0; i < 32; i++) {
				m_newgrowthsum += m_oldnewgrowth[i];
			}
			m_digestability = m_newgrowthsum / m_veg_biomass;
			m_digestability += 0.5;
			if (m_digestability > 0.8) m_digestability = 0.8;
		}
	}
	else {
		m_oldnewgrowth[m_newoldgrowthindex] = 0.0;
		m_digestability = 0.0;
	}
	// The insect calculation part
	// Bugmass = a + b(biomass) + c(height)
	double temp_bugmass = //g_bug_percent_d[ m_vege_type ] // This was used as a scaler - now not used
		g_bug_percent_a[m_vege_type] + ((m_veg_biomass + m_weed_biomass) * g_bug_percent_b[m_vege_type])
		+ (m_veg_height * g_bug_percent_c[m_vege_type]);
	// Set a minimum value (regressions will otherwise sometimes give a -ve value
	if (temp_bugmass < 0.05) temp_bugmass = 0.05;
	// Now need to check for deviations caused by management
	// First spot the deviation - this is easy because the only deviation that does
	// not affect the vegetation too is insecticide spraying
	if (m_days_since_insecticide_spray > 0) {
		// Need to change insects still, so grow towards the target, but only when 21 days from zero effect
		if (m_days_since_insecticide_spray < 21) m_insect_pop += (temp_bugmass - m_insect_pop) / m_days_since_insecticide_spray;
		m_days_since_insecticide_spray--;
	}
	else {
		m_insect_pop = temp_bugmass;
	}
	m_veg_density = (int)(floor(0.5 + (m_veg_biomass / (1 + m_veg_height))));
	if (m_veg_density > 100) m_veg_density = 100; // to stop array bounds problems
	if (m_LAtotal == 0.0) m_green_biomass = 0.0;
	else m_green_biomass = m_veg_biomass * (m_LAgreen / (m_LAtotal));
	m_dead_biomass = m_veg_biomass - m_green_biomass;
	/** Once bugs, cover etc are calculated the pollen and nectar level calculations are done */
	PollenNectarPhenologyCalculation();
	/** Finally calculate the goose forage resource availability */
	CalcGooseForageResources();
}

void VegElement::PollenNectarPhenologyCalculation()
{
	/**
	Here we need to first recalculate the daily m2 value in case some has been foraged,
	then supply the day number to each curve and get the slope per m2 for both pollen and nectar, then add this to the m2.
	If the total value is negative set it to zero, then recalculate the polygon total forage.
	*/
	int today = g_date->DayInYear();
	//need to store the correct pollen curve for fast look-up - this can be a pointer directly to the correct curve - avoiding the problem of tov vs tole
	m_pollenquality.m_quantity = m_pollencurve->GetData(today);
	m_NectarM2 = m_pollencurve->GetData(today);
	m_totalPollen = double(m_area) * m_PollenM2;
	m_totalNectar = double(m_area) * m_NectarM2;
}

void VegElement::CalcGooseForageResources()
{
	// For geese that eat spilled grain and maize we need to remove some of this daily (loss to other things than geese)
	// Get the Julian day
	int day = g_date->DayInYear();
	double rate;
	if ((day > March) && (day < July)) rate = cfg_goose_GrainDecayRateSpring.value();
	else rate = cfg_goose_GrainDecayRateWinter.value();
		m_birdseedforage *= rate;
	if (m_birdseedforage < 0.01) m_birdseedforage = 0.0;
	m_birdmaizeforage *= rate;
	if (m_birdmaizeforage < 0.01) m_birdmaizeforage = 0.0;
	// We also need to calculate non-grain forage for geese
	if (IsCereal()) {
		//if (m_green_biomass > 0.5)  //Testing if this could be a suitable fix for the cereals
		//{
			for (unsigned i = 0; i < gs_foobar; i++) {
				/** The 1.0325 is a quick fix to account for higher energy intake on winter cereal - Based on Therkildsen & Madsen 2000 Energetics of feeding...*/
				m_goosegrazingforage[ i ] = g_landscape_p->SupplyGooseGrazingForageH( m_veg_height, (GooseSpecies)i ) * cfg_goose_grass_to_winter_cereal_scaler.value();
			}
		//}
		//else for (unsigned i = 0; i < gs_foobar; i++) {
		//	m_goosegrazingforage[i] = 0.0;
		//}
		//m_goosegrazingforage[gs_foobar] = 1; // Is cereal
	}
	/** or potentially it is a grazable grass */
	else {
		if (IsGooseGrass()) {
			for (unsigned i = 0; i < gs_foobar; i++) {
				//m_goosegrazingforage[ i ] = 0.0;
				m_goosegrazingforage[i] = g_landscape_p->SupplyGooseGrazingForageH(m_veg_height, (GooseSpecies)i);
			}
		}
		else for (unsigned i = 0; i < gs_foobar; i++)	m_goosegrazingforage[i] = 0.0;
	}
}

void VegElement::RandomVegStartValues( double * a_LAtotal, double * a_LAgreen, double * a_veg_height, double * a_weed_biomass ) {
  * a_LAtotal = EL_VEG_START_LAIT * ( ( ( ( double )( random( 21 ) - 10 ) ) / 100.0 ) + 1.0 ); // +/- 10%
  * a_LAgreen = * a_LAtotal / 4.0;
  * a_veg_height = * a_LAgreen * EL_VEG_HEIGHTSCALE;
  * a_weed_biomass = * a_LAgreen * 0.1; // 10% weeds by biomass
}


void VegElement::SetGrowthPhase(int a_phase) {

	if (a_phase == sow) {
		m_vegddegs = 0.0;
	}
	else if (a_phase == harvest) m_vegddegs = -1;
	if (a_phase == janfirst) {
		m_forced_phase_shift = false;
		/**
		* If it is the first growth phase of the year then we might cause some unnecessary hops if e.g. our biomass is 0 and we suddenly jump up to
		* 20 cm To stop this happening we check here and if our settings are lower than the targets we do nothing.
		*/
		if (g_crops->StartValid(m_curve_num, a_phase)) {
			double temp_veg_height = g_crops->GetStartValue(m_curve_num, a_phase, 2);
			if (temp_veg_height < m_veg_height) { // Otherwise we are better off with the numbers we have to start with
				// Now with added variability
				m_LAgreen = g_crops->GetStartValue(m_curve_num, a_phase, 0);
				m_LAtotal = g_crops->GetStartValue(m_curve_num, a_phase, 1);
				m_veg_height = g_crops->GetStartValue(m_curve_num, a_phase, 2);
			}
		}

	}
	else if (g_crops->StartValid(m_curve_num, a_phase)) {
		m_LAgreen = g_crops->GetStartValue(m_curve_num, a_phase, 0);
		m_LAtotal = g_crops->GetStartValue(m_curve_num, a_phase, 1);
		m_veg_height = g_crops->GetStartValue(m_curve_num, a_phase, 2);
	}
	else if (!m_force_growth) {
		// If we are in forced growth mode (which is very likely),
		// then do not choose a new set of starting values, as we have
		// already calculated our way to a reasonable set of values.
		//RandomVegStartValues( & m_LAtotal, & m_LAgreen, & m_veg_height, & m_weed_biomass ); // **CJT** Testing removal 17/02/2015
	}
	m_veg_phase = a_phase;
	m_yddegs = 0.0;
	m_ddegs = g_weather->GetDDDegs(g_date->Date());
	m_force_growth = false;

	if (m_veg_phase == janfirst) {
		// For some growth curves there is no growth in the first
		// two months of the year. This will more likely than
		// not cause a discontinuous jump in the growth curves
		// come March first. ForceGrowthSpringTest() tries
		// to avoid that by checking for positive growth values
		// for the January growth phase. If none are found, then
		// it initializes a forced growth/transition to the March
		// 1st starting values.
		ForceGrowthSpringTest(); // Removal of this causes continuous increase in vegetation growth year on year for any curve that does not have a hard reset (e.g. harvest).
	}
}


void VegElement::ForceGrowthTest( void ) {
  // Called whenever the farmer does something 'destructive' to a
  // field, that reduced the vegetaion.
  if ( g_date->DayInYear() >= g_date->DayInYear( 1, 11 )
       || ( g_date->DayInYear() < g_date->DayInYear( 1, 3 ) && m_force_growth ) ) {
         ForceGrowthInitialize();
  }
}



void VegElement::ForceGrowthSpringTest(void) {
	// Check if there are any positive growth differentials in the curve
	// for the first two months of the year. Do nothing if there is.
	// If we have any positive growth then no need to force either
	if (g_crops->GetLAgreenDiff(90000.0, 0.0, m_curve_num, janfirst) > 0.001
		|| g_crops->GetLAtotalDiff(90000.0, 0.0, m_curve_num, janfirst) > 0.001
		|| g_crops->GetHeightDiff(90000.0, 0.0, m_curve_num, janfirst) > 0.001) {
		return;
	}
	// No growth, force it.
	ForceGrowthInitialize();
}



void VegElement::ForceGrowthInitialize( void ) {
  double LAgreen_target;
  double Weed_target;
  double LAtotal_target;
  double veg_height_target;
  int next_phase, daysleft;

  // Figure out what our target phase is.
  if ( g_date->DayInYear() < g_date->DayInYear( 3, 1 ) ) {
    daysleft = g_date->DayInYear( 1, 3 ) - g_date->DayInYear();
    next_phase = marchfirst;
  } else if ( g_date->DayInYear() >= g_date->DayInYear( 1, 11 ) ) {
    daysleft = 366 - g_date->DayInYear(); // Adjusted from 365 to prevent occaisional negative values
    next_phase = janfirst;
  } else {
    return;
  }
  if ( daysleft <= 0 )
       // Uh! Oh! This really shouldn't happen.
         return;

  if ( !g_crops->StartValid( m_curve_num, next_phase ) ) {
    // If no valid starting values for next phase, then
    // preinitialize the random starting values! Ie. make the
    // choice here and then do not choose another set come
    // next phase transition, but use the values we already
    // got at that point in time.
    RandomVegStartValues( & LAtotal_target, & LAgreen_target, & veg_height_target, & Weed_target );
  }
  else {
	  //add +/- 20% variation
	  double vari = (g_rand_uni() * 0.4) + 0.8;
	  Weed_target = g_crops->GetStartValue(m_weed_curve_num, next_phase, 0) * vari;
	  LAgreen_target = g_crops->GetStartValue(m_curve_num, next_phase, 0) * vari;
	  LAtotal_target = g_crops->GetStartValue(m_curve_num, next_phase, 1) * vari;
	  veg_height_target = g_crops->GetStartValue(m_curve_num, next_phase, 2) * vari;
  }

  m_force_growth = true;
  m_force_Weed = ( Weed_target - m_weed_biomass ) / ( double )daysleft;
  m_force_LAgreen = ( LAgreen_target - m_LAgreen ) / ( double )daysleft;
  m_force_LAtotal = ( LAtotal_target - m_LAtotal ) / ( double )daysleft;
  m_force_veg_height = ( veg_height_target - m_veg_height ) / ( double )daysleft;
}


void VegElement::ForceGrowthDevelopment( void ) {
  //if ( m_herbicidedelay == 0 ) m_weed_biomass += m_force_Weed; // ***CJT*** 12th Sept 2008 - rather than force growth, weeds might be allowed to grow on their own
  m_LAgreen += m_force_LAgreen;
  m_LAtotal += m_force_LAtotal;
  m_veg_height += m_force_veg_height;

  if (m_LAgreen < 0)  m_LAgreen = 0;
  if (m_LAtotal < 0)   m_LAtotal = 0;
  if (m_veg_height < 0)  m_veg_height = 0;
}



void VegElement::ZeroVeg( void ) {
  m_LAgreen = 0.0;
  m_LAtotal = 0.0;
  m_veg_height = 0.0;
  m_veg_cover = 0.0;
  m_veg_biomass = 0.0;
  m_weed_biomass = 0.0;
  m_birdseedforage = 0.0;
  m_birdmaizeforage = 0.0;
  SetStubble(false);
  ForceGrowthTest();
  RecalculateBugsNStuff();
}


void VegElement::DoDevelopment(void) {
	if (!m_force_growth) {
		//** First does the day degree calculations */
		m_yddegs = m_ddegs;
		m_ddegs = g_weather->GetDDDegs(g_date->Date());
		if (m_vegddegs != -1.0) m_vegddegs += m_ddegs; // Sum up the vegetation day degrees since sowing
		m_ddegs += m_yddegs; // and sum up the phase ddegs

		double dLAG = g_crops->GetLAgreenDiffScaled(m_ddegs, m_yddegs, m_curve_num, m_veg_phase, m_growth_scaler);
		double dLAT = g_crops->GetLAtotalDiffScaled(m_ddegs, m_yddegs, m_curve_num, m_veg_phase, m_growth_scaler);
		double dHgt = g_crops->GetHeightDiffScaled(m_ddegs, m_yddegs, m_curve_num, m_veg_phase, m_growth_scaler);

		m_LAgreen += dLAG;
		if (m_LAgreen < 0.0)
			m_LAgreen = 0.0;
		m_LAtotal += dLAT;
		if (m_LAtotal < 0.0)
			m_LAtotal = 0.0;
		int fintensity = 0;
#ifdef __EXTSHRINKSHEIGHT
		if (this->m_owner_index != -1) { // This only works because only crops and similar structures have owners
			fintensity = g_landscape_p->SupplyFarmIntensity(m_poly);
			if (fintensity == 1) {
				// 1 means extensive, so reduce vegetation height change by 10%
				dHgt *= 0.9;
			}
		}
#endif
		m_veg_height += dHgt;
		if (m_veg_height < 0.0)    m_veg_height = 0.0;
		/** Nest grows weeds proportionally to day degrees and using the weed curve if no herbicide effect before calling RecalculateBugsNStuff to caculate insect biomass, cover, digestability etc..*/
		if (m_herbicidedelay == 0) {
			double dWee = g_crops->GetLAtotalDiff(m_ddegs, m_yddegs, m_weed_curve_num, m_veg_phase);
			m_weed_biomass += dWee * cfg_ele_weedscaling.value()* (1 + fintensity);
		}
		if (m_weed_biomass < 0.0) m_weed_biomass = 0.0;
	}
	else {
		ForceGrowthDevelopment();
	}
	RecalculateBugsNStuff();
	/** Here we need to set today's goose numbers to zero in case they are not written by the goose population manager (the normal situation) */
	ResetGeese();
}
void VegElement::ResetGeese( void ) {
	m_gooseNos[ g_date->DayInYear() ] = 0;
	for (unsigned i = 0; i < gs_foobar; i++) {
		m_gooseSpNos[ g_date->DayInYear() ][ (GooseSpecies)i ] = 0;
		m_gooseSpNosTimed[ g_date->DayInYear() ][ (GooseSpecies)i ] = 0;
	}
}

void VegElement::GrazeVegetationTotal( double a_grams )
{
	GrazeVegetation( a_grams/m_area, true );
}

void VegElement::GrazeVegetation( double a_reduc, bool a_force )
{
	/**
	* Used to calculate the change in vegetation height and biomass as a result of grazing.
	* Input parameter is the change in wet biomass/m2. The problem is to convert this into changes in LAtotal, LAgreen and height.
	* We have an area, biomass, total biomass, height and density. If biomass is missing we need to change height and biomass before continuing and
	* and do something with LA_total and LA_Green.
	* Some assumptions:
	* 1 - The grazing takes all LA equally
	* 2 - That biomass is evenly distributed
	* 3 - That LA is proportional to biomass in some way, so LA is also evenly distributed
	* 4 - That we can use the current grazing pressure to alter a_reduc
	*/
	if (!a_force) a_reduc *= m_default_grazing_level;
	if (a_reduc >= m_veg_biomass) return;
	double propreduc = 1.0 - (a_reduc / m_veg_biomass);
	m_veg_height *= propreduc;
	m_weed_biomass *= propreduc;
	m_veg_biomass -= a_reduc;
	// Need to do something with the LA too - 
	m_LAgreen *= propreduc;
	m_LAtotal *= propreduc;
	m_oldLAtotal = m_LAtotal; // this stops double reduction of biomass later in RecalculateBugsNStuff();
}

void VegElement::ReduceVeg(double a_reduc) {
	m_LAgreen *= a_reduc;
	m_LAtotal *= a_reduc;
	m_veg_height *= a_reduc;
	m_veg_biomass *= a_reduc;
	m_weed_biomass *= a_reduc;

	ForceGrowthTest();
	m_oldLAtotal = m_LAtotal; // this stops double reduction of biomass later in RecalculateBugsNStuff();
}

void VegElement::ReduceVeg_Extended(double a_reduc) {
  m_LAgreen *= a_reduc;
  m_LAtotal *= a_reduc;
  m_veg_height *= a_reduc;
  m_veg_biomass *= a_reduc;
  m_weed_biomass *= a_reduc;

  if ( a_reduc < EL_GROWTH_PHASE_SHIFT_LEVEL ) {
    m_yddegs = 0.0;
    m_ddegs = EL_GROWTH_DAYDEG_MAGIC;
  }

  if ( g_date->DayInYear() >= EL_GROWTH_DATE_MAGIC && a_reduc < EL_GROWTH_PHASE_SHIFT_LEVEL && !m_forced_phase_shift ) {
    SetGrowthPhase( harvest1 );
    m_forced_phase_shift = true;
  }

  ForceGrowthTest();
  m_oldLAtotal = m_LAtotal; // this stops double reduction of biomass later in RecalculateBugsNStuff();
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


Field::Field( void ) : VegElement() {
  m_type = tole_Field;
  m_default_grazing_level = 3;
}



void VegElement::Clean_CropDataStorage(int index){

	/**Sets the values of a struct in an array #m_CropDataStorage (at a given index) to default values.*/ //05.03.13 AM

	m_CropDataStorage[index].taken = false;
	m_CropDataStorage[index].tov_type = tov_Undefined;
	m_CropDataStorage[index].biomass_at_harvest = -1;
	m_CropDataStorage[index].harvested = false;
	m_CropDataStorage[index].area = 0;
	m_CropDataStorage[index].no_herb_app = 0;
	m_CropDataStorage[index].missed_herb_app = 0;
	m_CropDataStorage[index].no_fi_app = 0;
	m_CropDataStorage[index].missed_fi_app = 0;
}

void  VegElement::SetVegType(TTypesOfVegetation a_vege_type, TTypesOfVegetation a_weed_type)
{
	m_vege_type = a_vege_type;
	m_curve_num = g_crops->VegTypeToCurveNum(a_vege_type);
	// -1 is used as a signal not to change the weed type
	// this is because it may be specific to that field
	if (a_weed_type != tov_Undefined) m_weed_curve_num = a_weed_type;
	SetPollenNectarCurves(g_nectarpollen->tovGetPollenNectarCurvePtr(a_vege_type).m_pollencurveptr,g_nectarpollen->tovGetPollenNectarCurvePtr(a_vege_type).m_nectarcurveptr);
	if (m_unsprayedmarginpolyref != -1) {
		// Must have an unsprayed margin so need to pass the information on to it
		LE* um = g_landscape_p->SupplyLEPointer(m_unsprayedmarginpolyref);
		dynamic_cast<VegElement*>(um)->SetPollenNectarCurves(m_pollencurve, m_nectarcurve);
	}
}


void Field::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  SetSprayedToday(false); // Reset the overspray flag in case it is set
  // Now if we have an unsprayed field margin, we need to transfer the crop
  // data to it.
  if ( GetUnsprayedMarginPolyRef() != -1 ) {
    if (( m_type == tole_PermPasture ) || ( m_type == tole_PermPastureTussocky ) || ( m_type == tole_PermPastureLowYield ))
      g_landscape_p->SupplyLEPointer( GetUnsprayedMarginPolyRef() )->SetCropDataAll( m_veg_height,
           ( m_veg_biomass - m_weed_biomass ) * 0.9, m_LAtotal, m_LAgreen, m_vege_type, m_weed_biomass, m_veg_cover,
           m_cattle_grazing, m_insect_pop, m_veg_patchy, m_veg_density );
	else g_landscape_p->SupplyLEPointer( GetUnsprayedMarginPolyRef() )->SetCropData( m_veg_height, m_LAtotal, m_LAgreen,
           m_vege_type, m_veg_cover, m_cattle_grazing );
  }
}


TTypesOfVegetation Field::GetPreviousCrop(int a_index) {
	return m_owner->GetPreviousCrop(a_index); 
}


void VegElement::SetCropData( double a_veg_height, double a_LAtotal, double a_LAgreen, TTypesOfVegetation a_veg,
     double a_cover, int a_grazed ) {
       m_veg_height = a_veg_height;
       m_LAtotal = a_LAtotal;
       m_LAgreen = a_LAgreen;
       m_vege_type = a_veg;
       m_veg_cover = a_cover;
       m_cattle_grazing = a_grazed;
}

void VegElement::SetCropDataAll( double a_veg_height, double a_biomass, double a_LAtotal, double a_LAgreen,
     TTypesOfVegetation a_veg, double a_wb, double a_cover, int a_grazed, double a_ins, bool a_patchy, double a_dens ) {
       m_veg_height = a_veg_height;
       m_veg_biomass = a_biomass;
       m_LAtotal = a_LAtotal;
       m_LAgreen = a_LAgreen;
       m_vege_type = a_veg;
       m_weed_biomass = a_wb;
       m_veg_cover = a_cover;
       m_cattle_grazing = a_grazed;
       m_insect_pop = a_ins;
       m_veg_density = (int) a_dens;
       m_veg_patchy = a_patchy;
}

void VegElement::InsectMortality( double a_fraction ) {
  m_insect_pop *= a_fraction;
}

PermPasture::PermPasture( void ) : VegElement() {
  // Vegetation type set by the farm manager.
  m_type = tole_PermPasture;
  m_veg_patchy = false;
  m_default_grazing_level = 3;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

PermPastureLowYield::PermPastureLowYield( void ) : VegElement() {
  // Vegetation type set by the farm manager.
  m_type = tole_PermPastureLowYield;
  m_veg_patchy = false;
  m_default_grazing_level = 2;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}


PermPastureTussocky::PermPastureTussocky( void ) : VegElement() {
  // Vegetation type set by the farm manager.
  m_type = tole_PermPastureTussocky;
  m_veg_patchy = true;
  m_digestability+=0.2;
  if (m_digestability>0.8) m_digestability=0.8;
  m_default_grazing_level = 1;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}


PermanentSetaside::PermanentSetaside( void ) : VegElement() {
  m_vege_type = tov_PermanentSetaside;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_PermanentSetaside;
  if (g_rand_uni() < cfg_SetAsidePatchyChance.value()) m_veg_patchy = true; else m_veg_patchy = false;
  m_digestability+=0.2;
  if (m_digestability>0.8) m_digestability=0.8;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value()- cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs

}


Hedges::Hedges( void ) : VegElement() {
  LE::SetHigh( true );
  //default to tall - later will depend on height recorded in polygon data
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_Hedges;
  SetSubType(0);
}


HedgeBank::HedgeBank( void ) : VegElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_HedgeBank;
  SetSubType(0);
}


BeetleBank::BeetleBank( void ) : VegElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_BeetleBank;
  if (g_rand_uni() < cfg_BBPatchyChance.value()) m_veg_patchy = true; else m_veg_patchy = false;
}


RoadsideVerge::RoadsideVerge( void ) : VegElement() {
  m_type = tole_RoadsideVerge;
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum(m_vege_type);
  m_veg_patchy = true;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

void RoadsideVerge::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  // Add cutting functionality when ready.
  long today = g_date->DayInYear();

  if ( g_date->JanFirst() ) {
    // beginning of year so restart the cutting
    m_DateCut = 0;
  }

  if ( today > RV_MAY_1ST ) // No cutting before May 1st
  {
    long SinceCut = today - m_DateCut; // how many days since last cut
    int month = g_date->GetMonth();
    switch ( month ) {
      case 5:
        if ( random( 14 ) + SinceCut > RV_CUT_MAY ) Cutting( today );
      break;
      case 6:
        if ( random( 14 ) + SinceCut > RV_CUT_JUN ) Cutting( today );
      break;
      case 7:
        if ( random( 14 ) + SinceCut > RV_CUT_JUL ) Cutting( today );
      break;
      case 8:
        if ( random( 14 ) + SinceCut > RV_CUT_AUG ) Cutting( today );
      break;
      case 9:
        if ( random( 14 ) + SinceCut > RV_CUT_SEP ) Cutting( today );
      break;
      case 10:
        if ( random( 14 ) + SinceCut > RV_CUT_OCT ) Cutting( today );
      break;
      default:
      break;
    }
  }
}

void RoadsideVerge::Cutting( int a_today )
{
  SetLastTreatment( mow );
  m_DateCut = a_today;
  m_veg_height = RV_CUT_HEIGHT;
  m_LAgreen = RV_CUT_GREEN;
  m_LAtotal = RV_CUT_TOTAL;
  RecalculateBugsNStuff();
}

RoadsideSlope::RoadsideSlope( void ) : RoadsideVerge() {
  m_type = tole_RoadsideSlope;
  m_vege_type = tov_NaturalGrass;
  m_veg_patchy = true;
}

void RoadsideSlope::DoDevelopment( void )
{
	RoadsideVerge::DoDevelopment();

}


WaterBufferZone::WaterBufferZone(void) : VegElement() {
	m_type = tole_WaterBufferZone;
	m_vege_type = tov_WaterBufferZone;
	m_curve_num = g_crops->VegTypeToCurveNum(m_vege_type);
	m_veg_patchy = true;
	m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

void WaterBufferZone::DoDevelopment(void) {
	VegElement::DoDevelopment();
	// Add reseting veg functionality on January 1st
	long today = g_date->DayInYear();

	if (g_date->JanFirst()) {
		// beginning of year so restart the cutting
		ResetingVeg(today);
	}
}

void WaterBufferZone::ResetingVeg(int a_today)
{
	ZeroVeg();
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

GreenElement::GreenElement( void ) : VegElement() {
  m_veg_patchy = false;
}


void GreenElement::DoDevelopment( void ) {
  VegElement::DoDevelopment();
}


Scrub::Scrub( void ) : GreenElement() {
  LE::SetHigh( true );
  m_type = tole_Scrub;
}


Marsh::Marsh( void ) : GreenElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_Marsh;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

Saltmarsh::Saltmarsh( void ) : Marsh() {
	m_type = tole_Saltmarsh;
	m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}


Heath::Heath( void ) : GreenElement() {
  m_vege_type = tov_Heath;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_Heath;
  m_veg_patchy = true;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

Orchard::Orchard( void ) : GreenElement() {
  LE::SetHigh( true );
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_MownGrass;
}

MownGrass::MownGrass( void ) : GreenElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( tov_Lawn );
  m_type = tole_MownGrass;
  if (g_rand_uni() < cfg_MGPatchyChance.value()) m_veg_patchy = true; else m_veg_patchy = false;
}

OrchardBand::OrchardBand( void ) : GreenElement() {
  LE::SetHigh( true );
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_OrchardBand;
  m_LastSprayed = 99999;
}

void Orchard::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  long today = g_date->DayInYear();
  // Spraying
  //int sprayday = cfg_OrchardSprayDay.value();
  int sprayday2 = cfg_pest_productapplic_startdate2.value();
  int sprayday3 = cfg_pest_productapplic_startdate3.value();
  int sprayday=cfg_pest_productapplic_startdate.value();
  //sprayday+=random(cfg_pest_productapplic_period.value());
  if ( ( today == sprayday ) || ( today == sprayday2 ) ||  ( today == sprayday3 ) )
    if ( g_landscape_p->SupplyShouldSpray() ) {
      //g_pest->DailyQueueAdd( this, l_pest_insecticide_amount.value() );
	  g_pest->DailyQueueAdd( this, l_pest_productOrchard_amount.value(), ppp_1);
    }
  // Cutting functionality
  if ( g_date->JanFirst() ) {
    // beginning of year so restart the cutting
    m_DateCut = 0;
  }

  switch ( cfg_OrchardNoCutsDay.value() ) {
    case 99:
      if ( today == ( sprayday - 7 ) ) Cutting( today );
    break;
    case 4:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) || ( today == 196 ) )
        Cutting( today );
    break;
    case 3:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) ) Cutting( today );
    break;
    case 2:
      if ( ( today == 259 ) || ( today == 122 ) ) Cutting( today );
    break;
    case 1:
      if ( ( today == 259 ) ) Cutting( today );
    break;
    default: // No cut
    break;
  }
}

void Orchard::Cutting( int a_today ) {
  SetLastTreatment( mow );
  SetMownDecay( 12 ); // 12 days of not suitable
  SetGrowthPhase( harvest1 );
  m_DateCut = a_today;
  m_veg_height = l_el_o_cut_height.value();
  m_LAgreen = l_el_o_cut_green.value();
  m_LAtotal = l_el_o_cut_total.value();
}

void MownGrass::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  long today = g_date->DayInYear();
  // Cutting functionality
  if ( g_date->JanFirst() ) {
    // beginning of year so restart the cutting
    m_DateCut = 0;
  }
  switch ( cfg_MownGrassNoCutsDay.value() ) {
    case 99:
		// Use to define special cutting behaviour e.g. cutting every 14 days after 1st May
		if (  ( today >= ( March + 15 ) ) && ( today % 42 == 0 )) {
			if (today < October) Cutting( today );
		}
    break;
    case 5:
      if ( ( today == 151 ) ) // 1st June
        Cutting( today );
    break;
    case 4:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) || ( today == 196 ) )
        Cutting( today );
    break;
    case 3:
      if ( ( today == 259 ) || ( today == 122 ) || ( today == 92 ) ) Cutting( today );
    break;
    case 2:
      if ( ( today == 259 ) || ( today == 122 ) ) Cutting( today );
    break;
    case 1:
      if ( ( today == 259 ) ) Cutting( today );
    break;
    default: // No cut
    break;
  }
}

void MownGrass::Cutting( int a_today ) {
  SetLastTreatment( mow );
  SetMownDecay( 21 ); // 21 days of not suitable
  SetGrowthPhase( harvest1 );
  m_DateCut = a_today;
  m_veg_height = l_el_o_cut_height.value();
  m_LAgreen = l_el_o_cut_green.value();
  m_LAtotal = l_el_o_cut_total.value();
}


void OrchardBand::DoDevelopment( void ) {
	VegElement::DoDevelopment();
	long today = g_date->DayInYear();
	if (m_LastSprayed<today) {
		m_herbicidedelay = today-m_LastSprayed;
		if (m_herbicidedelay > 5) m_herbicidedelay = 5;
			else if (m_herbicidedelay < 0) m_herbicidedelay = 0;
		this->ReduceVeg(0.9);
		if ((today == 0) || (today-m_LastSprayed > 90)) m_LastSprayed = 999999;
	}
  // Spraying
	int sprayday2 = cfg_pest_productapplic_startdate2.value();
	int sprayday3 = cfg_pest_productapplic_startdate3.value();
	int sprayday=cfg_pest_productapplic_startdate.value();
  //sprayday+=random(cfg_pest_productapplic_period.value());
	if ( ( today == sprayday ) || ( today == sprayday2 ) || ( today == sprayday3 ) ) {
		if ( g_landscape_p->SupplyShouldSpray() ) {
			g_pest->DailyQueueAdd( this, l_pest_productOrchard_amount.value(), ppp_1);
		}
		m_LastSprayed = today; // Regardless of whether we spray our test compound, we want to have the herbicide effect

	}
}

RiversidePlants::RiversidePlants( void ) : GreenElement() {
  m_type = tole_RiversidePlants;
}


NaturalGrassDry::NaturalGrassDry( void ) : GreenElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_veg_patchy=true;
  m_type = tole_NaturalGrassDry;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

NaturalGrassWet::NaturalGrassWet( void ) : GreenElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_veg_patchy=true;
  m_type = tole_NaturalGrassWet;
  m_growth_scaler = (g_rand_uni() * (cfg_PermanentVegGrowthMaxScaler.value() - cfg_PermanentVegGrowthMinScaler.value())) + cfg_PermanentVegGrowthMinScaler.value(); // Scales growth stochastically in a range given by the configs
}

Wasteland::Wasteland(void) : GreenElement() {
	m_vege_type = tov_Wasteland;
	m_curve_num = g_crops->VegTypeToCurveNum(m_vege_type);
	m_veg_patchy = true;
	m_type = tole_Wasteland;
}

void NaturalGrassDry::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  // The assumption is that natural grass has a range of species, which means
  // there should be good food all year - but still it should vary with season
  // So we add a constant to the digestability of 0.2
  m_digestability+=0.2;
  if (m_digestability>0.8) m_digestability=0.8;
}

void NaturalGrassWet::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  // The assumption is that natural grass has a range of species, which means
  // there should be good food all year - but still it should vary with season
  // So we add a constant to the digestability of 0.2
  m_digestability+=0.2;
  if (m_digestability>0.8) m_digestability=0.8;
}

void Wasteland::DoDevelopment( void ) {
	// The assumption is that natural grass has a range of species, which means
	// there should be good food all year - but still it should vary with season
	// So we add a constant to the digestability of 0.2
	m_digestability += 0.2;
	VegElement::DoDevelopment();
	if (m_digestability>0.8) m_digestability = 0.8;
}

FieldBoundary::FieldBoundary( void ) : GreenElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_FieldBoundary;
}

UnsprayedFieldMargin::UnsprayedFieldMargin( void ) : GreenElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_UnsprayedFieldMargin;
  if (random(100) < cfg_UMPatchyChance.value()) m_veg_patchy = true; else
    m_veg_patchy = false;
}

void UnsprayedFieldMargin::DoDevelopment( void ) {
  m_yddegs = m_ddegs;
  m_ddegs = m_yddegs + g_weather->GetDDDegs( g_date->Date() );
  double dWee = g_crops->GetLAtotalDiff( m_ddegs, m_yddegs, m_weed_curve_num, m_veg_phase );
  m_weed_biomass += dWee;
  if ( m_weed_biomass < 0.0 ) m_weed_biomass = 0.0;
  double temp_biomass = m_LAtotal * EL_PLANT_BIOMASS * 0.9 * g_biomass_scale[ m_vege_type ];
  // Calculate vegdensity here so that excess weeds do not make the density greater
  // They are in the patches which are not calculated in the 0.1 reduc.
  m_veg_density = ( int )floor( 0.5 + ( temp_biomass / ( 1 + m_veg_height ) ) );
  if ( m_veg_density > 100 ) m_veg_density = 100; // to stop array bounds problems
  double temp_bugmass = g_bug_percent_d[ m_vege_type ] * g_bug_percent_a[ m_vege_type ]
       + ( ( temp_biomass + m_weed_biomass ) * g_bug_percent_b[ m_vege_type ] )
       + ( m_veg_height * g_bug_percent_c[ m_vege_type ] );
  // Set a minimum value (regressions will otherwise give a -ve value
  if ( temp_bugmass < 0.05 ) temp_bugmass = 0.05;
  m_veg_biomass = m_weed_biomass + ( temp_biomass * 0.9 );
  m_insect_pop = temp_bugmass;
  m_veg_density = ( int )floor( 0.5 + ( m_veg_biomass / ( 1 + m_veg_height ) ) );
  if ( m_veg_density > 100 ) m_veg_density = 100; // to stop array bounds problems
}


RiversideTrees::RiversideTrees( void ) : GreenElement() {
  LE::SetHigh( true );
  m_type = tole_RiversideTrees;
}


Railway::Railway( void ) : GreenElement() {
  m_type = tole_Railway;
}

Vildtager::Vildtager(void) : GreenElement()
{
	m_type = tole_Vildtager;
	m_vege_type = tov_NaturalGrass;
}


PlantNursery::PlantNursery(void) : GreenElement() {
	LE::SetHigh(true);
	m_type = tole_PlantNursery;
	m_vege_type = tov_PlantNursery;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


ForestElement::ForestElement( void ) : VegElement() {
  LE::SetHigh( true ); // default to tall
}


Copse::Copse( void ) : ForestElement() {
  m_type = tole_Copse;
}


DeciduousForest::DeciduousForest( void ) : ForestElement() {
  m_type = tole_DeciduousForest;
}


ConiferousForest::ConiferousForest( void ) : ForestElement() {
  m_type = tole_ConiferousForest;
}


MixedForest::MixedForest( void ) : ForestElement() {
  m_type = tole_MixedForest;
}

YoungForest::YoungForest( void ) : ForestElement() {
  m_vege_type = tov_NaturalGrass;
  m_curve_num = g_crops->VegTypeToCurveNum( m_vege_type );
  m_type = tole_YoungForest;
  m_veg_patchy = true;
  LE::SetHigh( false ); // default to tall
}

WoodlandMargin::WoodlandMargin(void) : ForestElement() {
	m_type = tole_WoodlandMargin;
}

IndividualTree::IndividualTree(void) : ForestElement() {
	m_type = tole_IndividualTree;
}

WoodyEnergyCrop::WoodyEnergyCrop(void) : ForestElement() {
	m_type = tole_WoodyEnergyCrop;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

NonVegElement::NonVegElement( void ) : LE() {
  ;
}


ChameleonLE::ChameleonLE(void) : NonVegElement() {
	m_type = tole_Chameleon;
}

Garden::Garden(void) : NonVegElement() {
	LE::SetHigh(true);
	m_type = tole_Garden;
}


Building::Building( void ) : NonVegElement() {
  LE::SetHigh( true );
  m_type = tole_Building;
  m_countrydesignation = 0; // default = 0 = town
}


StoneWall::StoneWall( void ) : NonVegElement() {
  LE::SetHigh( true );
  m_type = tole_StoneWall;
}

Fence::Fence(void) : NonVegElement() {
	LE::SetHigh(false);
	m_type = tole_Fence;
}


PitDisused::PitDisused( void ) : NonVegElement() {
  LE::SetHigh( true );
  m_type = tole_PitDisused;
}


Saltwater::Saltwater( void ) : NonVegElement() {
  m_type = tole_Saltwater;
}


Freshwater::Freshwater(void) : NonVegElement() {
	m_type = tole_Freshwater;
}


FishFarm::FishFarm(void) : NonVegElement() {
	m_type = tole_FishFarm;
}


Pond::Pond( void ) : Freshwater() {
	m_type = tole_Pond;
	m_LarvalFood = 0.01;
	m_MaleNewtPresent = false;
	if (cfg_randompondquality.value()) m_pondquality = g_rand_uni(); else m_pondquality = 1.0;
}

void Pond::DoDevelopment()
{
	LE::DoDevelopment();
	CalcPondPesticide();
	CalcLarvalFood();
	m_MaleNewtPresent = false;
}

void Pond::CalcPondPesticide()
{
	/**
	* The pesticide is calculated based on the mean concentration per m2 which is then multiplied by a factor representing run-off from the surroundings
	* or other movements of pesticides through soil into the pond. This method assumes a uniform depth of water, which is then ignored (so can be seen
	* as being part of the run-off factor).
	* The pesticide concentration is calculated each day.
	* These calculations are heavy on CPU time because of the need to search the landscape map for pond 1m2 and sum pesticide. So this has to be set
	* to run by flagging using cfg_calc_pond_pesticide
	*/
	m_pondpesticide = 0.0;
	if (!cfg_calc_pond_pesticide.value())
	{
		return;
	}
	/** 
	* To create the sum of pesticide the map is searched from min x,y to max x,y coords, and pond cells are summed for their pesticide content.
	* Then the mean found.
	*/
	for (int x = m_minx; x <= m_maxx; x++)
	for (int y = m_miny; y <= m_maxy; y++)
	{
		if (g_landscape_p->SupplyPolyRef(x, y) == m_poly) m_pondpesticide += g_landscape_p->SupplyPesticide(x, y, ppp_1);
	}
	m_pondpesticide /= m_area;
	/** 
	* We assume a mean pond depth of 1 m, so pesticide per l = m_pondpesticide/1000 to get per litre then multiplied buy the run-off factor
	*/
	m_pondpesticide *= cfg_pondpesticiderunoff.value()/1000.0;
}

void Pond::CalcLarvalFood()
{
/**
* The larval food is calculated assuming a logistic equation in the form of Nt+1 = Nt+(N*r * (1-N/K))
* t = one day, N is a scaler which is multiplied by a constant and the area of the pond to get the total larval food, K & r are carrying capacity and instantaneous reproductive rate respectively.
* K can change with season and this is currently hard coded, but could be an input variable later. The values are held in LarvalFoodMonthlyK\n
* The steps in the calculation are:\n
* - Enforce a assumed pond size for newts as maximum 400 m2
* - Ensure we never get zero larval food, so there is always something to grow the curve from.
* - Back calculate the current scaler value. This is needed because between time steps, food may be eaten by larvae. This is done based on the area and a fixed constant held in cfg_PondLarvalFoodBiomassConst
* - Calculate the new scaler based on the logistic equation as described above
* - Re-calculate the new total food biomass based on the area and a fixed constant held in cfg_PondLarvalFoodBiomassConst
*
*/
	double area = m_area;
	if (m_area > 400) area = 400;
	const double LarvalFoodMonthlyK[12] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
	// Back calculate the scaler
	m_LarvalFoodScaler = m_LarvalFood / (cfg_PondLarvalFoodBiomassConst.value() * area * m_pondquality);
	// Calculate the new scaler
	m_LarvalFoodScaler = m_LarvalFoodScaler + (m_LarvalFoodScaler*cfg_PondLarvalFoodR.value() * (1 - (m_LarvalFoodScaler / LarvalFoodMonthlyK[g_date->GetMonth() - 1])));
	// Provide some contest competition by preventing all the food from being used up, leaving minimum 1%
	if (m_LarvalFoodScaler < 0.01) m_LarvalFoodScaler = 0.01;
	// Calculate the new food biomass
	m_LarvalFood = m_LarvalFoodScaler * (cfg_PondLarvalFoodBiomassConst.value() * area * m_pondquality);
}

bool Pond::SubtractLarvalFood(double a_food)
{
	/**
	* If the total amount of food is low then there is a probability test to determine if food can be found. If failed the return code false means no food was removed.
	* If passed then this removes the amount of food passed in a_food and return true.
	*/
	m_LarvalFood -= a_food;
	if (m_LarvalFood < 0) return false;
	return true;
}

RiverBed::RiverBed(void) : NonVegElement() {
	m_type = tole_RiverBed;
}

River::River(void) : NonVegElement() {
	m_type = tole_River;
}

Canal::Canal(void) : NonVegElement() {
	m_type = tole_Canal;
}

DrainageDitch::DrainageDitch(void) : NonVegElement() {
	m_type = tole_DrainageDitch;
}

RefuseSite::RefuseSite(void) : NonVegElement() {
	m_type = tole_RefuseSite;
}


Coast::Coast( void ) : NonVegElement() {
  m_type = tole_Coast;
}


BareRock::BareRock( void ) : NonVegElement() {
  m_type = tole_BareRock;
}


SandDune::SandDune( void ) : NonVegElement() {
  m_type = tole_SandDune;
}


AmenityGrass::AmenityGrass( void ) : NonVegElement() {
  m_type = tole_AmenityGrass;
}


Parkland::Parkland( void ) : NonVegElement() {
  m_type = tole_Parkland;
}


UrbanNoVeg::UrbanNoVeg( void ) : NonVegElement() {
  LE::SetHigh( true );
  m_type = tole_UrbanNoVeg;
}

UrbanVeg::UrbanVeg(void) : NonVegElement() {
	LE::SetHigh(true);
	m_type = tole_UrbanVeg;
}


UrbanPark::UrbanPark( void ) : NonVegElement() {
  m_type = tole_UrbanPark;
}


BuiltUpWithParkland::BuiltUpWithParkland( void ) : NonVegElement() {
  LE::SetHigh( true );
  m_type = tole_BuiltUpWithParkland;
}


ActivePit::ActivePit( void ) : NonVegElement() {
  LE::SetHigh( true );
  m_type = tole_ActivePit;
}


LargeRoad::LargeRoad( void ) : NonVegElement() {
  m_type = tole_LargeRoad;
}


double LargeRoad::GetTrafficLoad( void ) {
  return m_largeroad_load[ g_date->GetHour() ] * m_monthly_traffic[ g_date->GetMonth() - 1 ];
}


SmallRoad::SmallRoad( void ) : NonVegElement() {
  m_type = tole_SmallRoad;
}


double SmallRoad::GetTrafficLoad( void ) {
  return m_smallroad_load[ g_date->GetHour() ] * m_monthly_traffic[ g_date->GetMonth() - 1 ];
}


Track::Track( void ) : NonVegElement() {
  m_type = tole_Track;
}

Stream::Stream( void ) : NonVegElement() {
  m_type = tole_Stream;
}

Churchyard::Churchyard( void ) : NonVegElement() {
  m_type = tole_Churchyard;
}

Carpark::Carpark( void ) : NonVegElement() {
  m_type = tole_Carpark;
}

HeritageSite::HeritageSite( void ) : NonVegElement() {
  m_type = tole_HeritageSite;
}

MetalledPath::MetalledPath( void ) : NonVegElement() {
  m_type = tole_MetalledPath;
}

Pylon::Pylon(void) : NonVegElement() {
	LE::SetHigh(true);
	m_type = tole_Pylon;
}


WindTurbine::WindTurbine(void) : NonVegElement() {
	LE::SetHigh(true);
	m_type = tole_Pylon;
}





//--------------------------------------------------------------------
// created 25/08/00
TTypesOfLandscapeElement LE_TypeClass::TranslateEleTypes( int EleReference ) {
  static char error_num[ 20 ];

  // This returns the vegetation type (or crop type) as applicable
  switch ( EleReference ) {
  case	5:	return	tole_Building;
  case	8:	return	tole_UrbanNoVeg;
  case	9:	return	tole_UrbanVeg;
  case	11:	return	tole_Garden;
  case	12:	return	tole_AmenityGrass;
  case	13:	return	tole_RoadsideVerge;
  case	14:	return	tole_Parkland;
  case	15:	return	tole_StoneWall;
  case	16:	return	tole_BuiltUpWithParkland;
  case	17:	return	tole_UrbanPark;
  case	20:	return	tole_Field;
  case	27:	return	tole_PermPastureTussocky;
  case	26:	return	tole_PermPastureLowYield;
  case	31:	return	tole_UnsprayedFieldMargin;
  case	33:	return	tole_PermanentSetaside;
  case	35:	return	tole_PermPasture;
  case	40:	return	tole_DeciduousForest;
  case	41:	return	tole_Copse;
  case	50:	return	tole_ConiferousForest;
  case	55:	return	tole_YoungForest;
  case	56:	return	tole_Orchard;
  case	69:	return	tole_BareRock;
  case	57:	return	tole_OrchardBand;
  case	58:	return	tole_MownGrass;
  case	60:	return	tole_MixedForest;
  case	70:	return	tole_Scrub;
  case	75:	return	tole_PitDisused;
  case	80:	return	tole_Saltwater;
  case	90:	return	tole_Freshwater;
  case	94:	return	tole_Heath;
  case	95:	return	tole_Marsh;
  case	96:	return	tole_River;
  case	97:	return	tole_RiversideTrees;
  case	98:	return	tole_RiversidePlants;
  case	100:	return	tole_Coast;
  case	101:	return	tole_SandDune;
  case	110:	return	tole_NaturalGrassDry;
  case	115:	return	tole_ActivePit;
  case	118:	return	tole_Railway;
  case	121:	return	tole_LargeRoad;
  case	122:	return	tole_SmallRoad;
  case	123:	return	tole_Track;
  case	130:	return	tole_Hedges;
  case	140:	return	tole_HedgeBank;
  case	141:	return	tole_BeetleBank;
  case	150:	return	tole_Chameleon;
  case	160:	return	tole_FieldBoundary;
  case	201:	return	tole_RoadsideSlope;
  case	202:	return	tole_MetalledPath;
  case	203:	return	tole_Carpark;
  case	204:	return	tole_Churchyard;
  case	205:	return	tole_NaturalGrassWet;
  case	206:	return	tole_Saltmarsh;
  case	207:	return	tole_Stream;
  case	208:	return	tole_HeritageSite;
  case	209:	return	tole_Wasteland;
  case	210:	return	tole_UnknownGrass;
  case	211:	return	tole_WindTurbine;
  case	212:	return	tole_Pylon;
  case	213:	return	tole_IndividualTree;
  case	214:	return	tole_PlantNursery;
  case	215:	return	tole_Vildtager;
  case	216:	return	tole_WoodyEnergyCrop;
  case	217:	return	tole_WoodlandMargin;
  case	218:	return	tole_PermPastureTussockyWet;
  case	219:	return	tole_Pond;
  case	220:	return	tole_FishFarm;
  case	221:	return	tole_RiverBed;
  case	222:	return	tole_DrainageDitch;
  case	223:	return	tole_Canal;
  case	224:	return	tole_RefuseSite;
  case	225:	return	tole_Fence;
  case	226:	return	tole_WaterBufferZone;
  case	2112:	return	tole_Missing;

      //  case 999: return tole_Foobar;
      // !! type unknown - should not happen
    default:
      sprintf( error_num, "%d", EleReference );
      g_msg->Warn( WARN_FILE, "LE_TypeClass::TranslateEleTypes(): ""Unknown landscape element type:", error_num );
      exit( 1 );
  }
}



//----------------------------------------------------------------------
// created 24/08/00
TTypesOfVegetation LE_TypeClass::TranslateVegTypes( int VegReference ) {
  char error_num[ 20 ];

  // This returns the vegetation type (or crop type) as applicable
  switch ( VegReference ) {
    case 1:
      return tov_SpringBarley;
    case 2:
      return tov_WinterBarley;
    case 3:
      return tov_SpringWheat;
    case 4:
      return tov_WinterWheat;
    case 5:
      return tov_WinterRye;
    case 6:
      return tov_Oats;
    case 7:
      return tov_Triticale;
    case 8:
      return tov_Maize;
    case 13:
      return tov_SpringBarleySeed;
    case 14: return tov_SpringBarleyStrigling;
    case 15: return tov_SpringBarleyStriglingSingle;
    case 16: return tov_SpringBarleyStriglingCulm;
    case 17: return tov_WinterWheatStrigling;
    case 18: return tov_WinterWheatStriglingSingle;
    case 19: return tov_WinterWheatStriglingCulm;
    case 21:
      return tov_SpringRape;
    case 22:
      return tov_WinterRape;
    case 30:
      return tov_FieldPeas;
	case 31:
		return tov_FieldPeasSilage; //ok?
	case 32:
		return tov_BroadBeans;
	case 50:
      return tov_Setaside;
    case 54:
      return tov_PermanentSetaside;
    case 55:
      return tov_YoungForest;
	case 60:
		return tov_FodderBeet;
	case 61:
		return tov_SugarBeet;
	case 65:
      return tov_CloverGrassGrazed1;
    case 92:
      return tov_PotatoesIndustry;
    case 93:
      return tov_Potatoes;
    case 94:
      return tov_SeedGrass1;
    case 102:
      return tov_OWinterBarley;
    case 611:
      return tov_OWinterBarleyExt;
    case 103:
      return tov_OSBarleySilage;
    case 105:
      return tov_OWinterRye;
    case 106:
      return tov_OFieldPeasSilage;
    case 107:
      return tov_SpringBarleyGrass;
	case 108:
		return tov_SpringBarleyCloverGrass;
	case 109:
		return tov_SpringBarleySpr;
	case 113:
      return tov_OBarleyPeaCloverGrass;
    case 114:
      return tov_SpringBarleyPeaCloverGrassStrigling;
    case 115:
      return tov_SpringBarleySilage;
    case 122:
      return tov_OWinterRape;
    case 140:
      return tov_PermanentGrassGrazed;
    case 141:
      return tov_PermanentGrassLowYield;
    case 142:
      return tov_PermanentGrassTussocky;
    case 165:
      return tov_CloverGrassGrazed2;
    case 194:
      return tov_SeedGrass2;
    case 201:
      return tov_OSpringBarley;
	case 204:
		return tov_OWinterWheatUndersown;
	case 205:
		return tov_OWinterWheat;
	case 206:
      return tov_OOats;
    case 207:
      return tov_OTriticale;
	case 230:
		return tov_OFieldPeas;
	case 26:
		return tov_OFodderBeet;
	case 265:
      return tov_OCloverGrassGrazed1;
    case 270:
      return tov_OCarrots;
    case 271:
      return tov_Carrots;
	case 272:
		return tov_Wasteland;
    case 273:
      return tov_OGrazingPigs;
    case 293:
      return tov_OPotatoes;
    case 294:
      return tov_OSeedGrass1;
	case 306:
		return tov_OSpringBarleyPigs;
	case 307:
      return tov_OSpringBarleyGrass;
    case 308:
      return tov_OSpringBarleyClover;
    case 340:
      return tov_OPermanentGrassGrazed;
    case 365:
      return tov_OCloverGrassGrazed2;
	case 366:
		return tov_OCloverGrassSilage1;
    case 394:
      return tov_OSeedGrass2;
    case 400:
      return tov_NaturalGrass;
    case 401:
      return tov_None;
    case 601:
      return tov_WWheatPControl;
    case 602:
      return tov_WWheatPToxicControl;
    case 603:
      return tov_WWheatPTreatment;
    case 604:
      return tov_AgroChemIndustryCereal;
    case 605:
      return tov_WinterWheatShort;
	case 606:
	  return tov_MaizeSilage;
	case 607:
		return tov_FodderGrass;
	case 608:
		return tov_SpringBarleyPTreatment;
    case 609:
      return tov_OSpringBarleyExt;
	case 610:
	  return tov_OMaizeSilage;
	case 612:
		return tov_SpringBarleySKManagement;
	case 613:
		return tov_Heath;
	case 700:
	  return tov_OrchardCrop;
	case 701:
		return tov_WaterBufferZone;
	case 801:
		return tov_PLWinterWheat;
	case 802:
		return tov_PLWinterRape;
	case 803:
		return tov_PLWinterBarley;
	case 804:
		return tov_PLWinterTriticale;
	case 805:
		return tov_PLWinterRye;
	case 806:
		return tov_PLSpringWheat;
	case 807:
		return tov_PLSpringBarley;
	case 808:
		return tov_PLMaize;
	case 809:
		return tov_PLMaizeSilage;
	case 810:
		return tov_PLPotatoes;
	case 811:
		return tov_PLBeet;
	case 812:
		return tov_PLFodderLucerne1;
	case 813:
		return tov_PLFodderLucerne2;
	case 814:
		return tov_PLCarrots;
	case 815:
		return tov_PLSpringBarleySpr;
	case 816:
		return tov_PLWinterWheatLate;
	case 817:
		return tov_PLBeetSpr;
	case 818:
		return tov_PLBeans;

	case 850:
		return tov_NLBeet;
	case 851:
		return tov_NLCarrots;
	case 852:
		return tov_NLMaize;
	case 853:
		return tov_NLPotatoes;
	case 854:
		return tov_NLSpringBarley;
	case 855:
		return tov_NLWinterWheat;
	case 856:
		return tov_NLCabbage;
	case 857:
		return tov_NLTulips;
	case 858:
		return tov_NLGrassGrazed1;
	case 859:
		return tov_NLGrassGrazed2;
	case 860:
		return tov_NLPermanentGrassGrazed;
	case 861:
		return tov_NLCatchPeaCrop;
	case 862:
		return tov_NLBeetSpring;
	case 863:
		return tov_NLCarrotsSpring;
	case 864:
		return tov_NLMaizeSpring;
	case 865:
		return tov_NLPotatoesSpring;
	case 866:
		return tov_NLSpringBarleySpring;
	case 867:
		return tov_NLCabbageSpring;
	case 868:
		return tov_NLGrassGrazed1Spring;
	case 869:
		return tov_NLGrassGrazedLast;

	case 888:
		return tov_DummyCropPestTesting;

    case 999:
      return tov_Undefined;
    default: // No matching code so we need an error message of some kind
      sprintf( error_num, "%d", VegReference );
      g_msg->Warn( WARN_FILE, "LE_TypeClass::TranslateVegTypes(): ""Unknown vegetation type:", error_num );
      exit( 1 );
  }
}

//-----------------------------------------------------------------------
// created 25/08/00
int LE_TypeClass::BackTranslateVegTypes( TTypesOfVegetation VegReference ) {
  char error_num[ 20 ];

  // This returns the vegetation type (or crop type) as applicable
  switch ( VegReference ) {
    case tov_SpringBarley:
      return 1;
    case tov_WinterBarley:
      return 2;
    case tov_SpringWheat:
      return 3;
    case tov_WinterWheat:
      return 4;
    case tov_WinterRye:
      return 5;
    case tov_Oats:
      return 6;
    case tov_Triticale:
      return 7;
    case tov_Maize:
      return 8;
    case tov_SpringBarleySeed:
      return 13;
    case tov_SpringBarleyStrigling:
      return 14;
    case tov_SpringBarleyStriglingSingle:
      return 15;
    case tov_SpringBarleyStriglingCulm:
      return 16;
    case tov_WinterWheatStrigling:
		return 17;
    case tov_WinterWheatStriglingSingle:
		return 18;
    case tov_WinterWheatStriglingCulm:
		return 19;
    case tov_SpringRape:
      return 21;
    case tov_WinterRape:
      return 22;
    case tov_FieldPeas:
      return 30;
	case tov_FieldPeasSilage:
		return 31;
	case tov_BroadBeans:
		return 32;
    case tov_Setaside:
      return 50;
    case tov_PermanentSetaside:
      return 54;
    case tov_YoungForest:
      return 55;
	case tov_FodderBeet:
		return 60;
	case tov_SugarBeet:
		return 61;
	case tov_CloverGrassGrazed1:
      return 65;
    case tov_PotatoesIndustry:
      return 92;
    case tov_Potatoes:
      return 93;
    case tov_SeedGrass1:
      return 94;
    case tov_OWinterBarley:
      return 102;
    case tov_OWinterBarleyExt:
      return 611;
    case tov_OWinterRye:
      return 105;
    case tov_SpringBarleyGrass:
      return 107;
    case tov_SpringBarleyCloverGrass:
      return 108;
	case tov_SpringBarleySpr:
		return 109;
	case tov_OSBarleySilage:
		return 103;
	case tov_OBarleyPeaCloverGrass:
      return 113;
    case tov_SpringBarleyPeaCloverGrassStrigling:
      return 114;
    case tov_SpringBarleySilage:
      return 115;
    case tov_OWinterRape:
      return 122;
    case tov_PermanentGrassGrazed:
      return 140;
    case tov_PermanentGrassLowYield:
      return 141;
    case tov_PermanentGrassTussocky:
      return 142;
    case tov_CloverGrassGrazed2:
      return 165;
    case tov_SeedGrass2:
      return 194;
    case tov_OSpringBarley:
      return 201;
	case tov_OWinterWheatUndersown:
		return 204;
	case tov_OWinterWheat:
		return 205;
	case tov_OOats:
      return 206;
    case tov_OTriticale:
      return 207;
    case tov_OFieldPeas:
      return 230;
    case tov_OFieldPeasSilage:
      return 106;
	case tov_OFodderBeet:
		return 260;
	case tov_OCloverGrassGrazed1:
      return 265;
    case tov_OCarrots:
      return 270;
    case tov_Carrots:
      return 271;
    case tov_OPotatoes:
      return 293;
    case tov_OSeedGrass1:
      return 294;
	case tov_OSpringBarleyPigs:
	  return 306;
	case tov_OSpringBarleyGrass:
      return 307;
	case tov_OSpringBarleyClover:
      return 308;
    case tov_OPermanentGrassGrazed:
      return 340;
    case tov_OCloverGrassGrazed2:
      return 365;
	case tov_OCloverGrassSilage1:
		return 366;
    case tov_OSeedGrass2:
      return 394;
    case tov_NaturalGrass:
      return 400;
    case tov_None:
      return 401;
    case tov_NoGrowth:
      return 402;
    case tov_WWheatPControl:
      return 601;
    case tov_WWheatPToxicControl:
      return 602;
    case tov_WWheatPTreatment:
      return 603;
    case tov_AgroChemIndustryCereal:
      return 604;
    case tov_WinterWheatShort:
      return 605;
    case tov_MaizeSilage:
      return 606;
	case tov_FodderGrass:
		return 607;
    case tov_SpringBarleyPTreatment:
      return 608;
    case tov_OSpringBarleyExt:
      return 609;
    case tov_OMaizeSilage:
      return 610;
	case tov_SpringBarleySKManagement:
		return 612;
	case tov_Heath:
		return 613;
	case tov_OrchardCrop:
		return 700;
	case tov_WaterBufferZone:
		return 701;
	case tov_PLWinterWheat:
		return 801;
	case tov_PLWinterRape:
		return 802;
	case tov_PLWinterBarley:
		return 803;
	case tov_PLWinterTriticale:
		return 804;
	case tov_PLWinterRye:
		return 805;
	case tov_PLSpringWheat:
		return 806;
	case tov_PLSpringBarley:
		return 807;
	case tov_PLMaize:
		return 808;
	case tov_PLMaizeSilage:
		return 809;
	case tov_PLPotatoes:
		return 810;
	case tov_PLBeet:
		return 811;
	case tov_PLFodderLucerne1:
		return 812;
	case tov_PLFodderLucerne2:
		return 813;
	case tov_PLCarrots:
		return 814;
	case tov_PLSpringBarleySpr:
		return 815;
	case tov_PLWinterWheatLate:
		return 816;
	case tov_PLBeetSpr:
		return 817;
	case tov_PLBeans:
		return 818;

	case tov_NLBeet:
		return 850;
	case tov_NLCarrots:
		return 851;
	case tov_NLMaize:
		return 852;
	case tov_NLPotatoes:
		return 853;
	case tov_NLSpringBarley:
		return 854;
	case tov_NLWinterWheat:
		return 855;
	case tov_NLCabbage:
		return 856;
	case tov_NLTulips:
		return 857;
	case tov_NLGrassGrazed1:
		return 858;
	case tov_NLGrassGrazed2:
		return 859;
	case tov_NLPermanentGrassGrazed:
		return 860;
	case tov_NLCatchPeaCrop:
		return 861;
	case tov_NLBeetSpring:
		return 862;
	case tov_NLCarrotsSpring:
		return 863;
	case tov_NLMaizeSpring:
		return 864;
	case tov_NLPotatoesSpring:
		return 865;
	case tov_NLSpringBarleySpring:
		return 866;
	case tov_NLCabbageSpring:
		return 867;
	case tov_NLGrassGrazed1Spring:
		return 868;
	case tov_NLGrassGrazedLast:
		return 869;

	case tov_OGrazingPigs:
		return 271;
	case tov_Wasteland:
		return 272;
	case tov_DummyCropPestTesting:
		return 888;

    case tov_Undefined:
      return 999;
    default: // No matching code so we need an error message of some kind
      sprintf( error_num, "%d", VegReference );
      g_msg->Warn( WARN_FILE, "LE_TypeClass::BackTranslateVegTypes(): ""Unknown vegetation type:", error_num );
      exit( 1 );
  }
}

//-----------------------------------------------------------------------
// created 25/08/00
int LE_TypeClass::BackTranslateEleTypes( TTypesOfLandscapeElement EleReference ) {
  static char error_num[ 20 ];

  // This returns the vegetation type (or crop type) as applicable
  switch ( EleReference )
  {
    case tole_Building:				return 5;
    case tole_UrbanNoVeg:			return 8;
	case tole_UrbanVeg:		return 9;
    case tole_Garden:				return 11;
    case tole_AmenityGrass:			return 12;
    case tole_RoadsideVerge:		return 13;
    case tole_Parkland:				return 14;
    case tole_StoneWall:			return 15;
    case tole_BuiltUpWithParkland:	return 16;
    case tole_UrbanPark:			return 17;
    case tole_Field:			    return 20;
    case tole_PermPastureTussocky:  return 27;
    case tole_PermPastureLowYield:  return 26;
    case tole_UnsprayedFieldMargin: return 31;
    case tole_PermanentSetaside:    return 33;
    case tole_PermPasture:		    return 35;
    case tole_DeciduousForest:      return 40;
	case tole_Copse:				return 41;
	case tole_ConiferousForest:     return 50;
    case tole_YoungForest:		    return 55;
    case tole_Orchard:			    return 56;
    case tole_BareRock:			    return 69;
	case tole_OrchardBand:			return 57;
	case tole_MownGrass:			return 58;
	case tole_MixedForest:		    return 60;
	case tole_Scrub:				return 70;
	case tole_PitDisused:		    return 75;
    case tole_Saltwater:		    return 80;
    case tole_Freshwater:		    return 90;
    case tole_Heath:			    return 94;
    case tole_Marsh:				return 95;
    case tole_River:				return 96;
    case tole_RiversideTrees:	    return 97;
    case tole_RiversidePlants:      return 98;
    case tole_Coast:			    return 100;
	case tole_SandDune:				return 101;
	case tole_NaturalGrassDry:      return 110;
    case tole_ActivePit:		    return 115;
    case tole_Railway:				return 118;
    case tole_LargeRoad:			return 121;
    case tole_SmallRoad:			return 122;
    case tole_Track:				return 123;
    case tole_Hedges:				return 130;
    case tole_HedgeBank:			return 140;
    case tole_BeetleBank:		    return 141;
	case tole_Chameleon:			return 150;
	case tole_FieldBoundary:	    return 160;
 	case tole_RoadsideSlope:		return 201;
	case tole_MetalledPath:			return 202;
	case tole_Carpark:				return 203;
	case tole_Churchyard:			return 204;
	case tole_NaturalGrassWet:		return 205;
	case tole_Saltmarsh:			return 206;
	case tole_Stream:				return 207;
	case tole_HeritageSite:			return 208;
	case tole_Wasteland:			return 209; 
	case tole_UnknownGrass:		return 210;
	case tole_WindTurbine:			return 211;
	case tole_Pylon:				return 212;
	case tole_IndividualTree:		return 213;
	case tole_PlantNursery:			return 214;
	case tole_Vildtager:			return 215;
	case tole_WoodyEnergyCrop:		return 216;
	case tole_WoodlandMargin:		return 217;
	case tole_PermPastureTussockyWet:		return 218;
	case tole_Pond:                 return 219;
	case tole_FishFarm:                 return 220;
	case tole_RiverBed:		return 221;
	case tole_DrainageDitch:		return 222;
	case tole_Canal:	return 223;
	case tole_RefuseSite:	return 224;
	case tole_Fence:				return 225;
	case tole_WaterBufferZone:		return 226;

	case tole_Missing:		return 2112;

	//case tole_Foobar: return 999;
	// !! type unknown - should not happen
	default:
      sprintf( error_num, "%d", EleReference );
      g_msg->Warn( WARN_FILE, "LE_TypeClass::BackTranslateEleTypes(): ""Unknown landscape element type:", error_num );
      exit( 1 );
  }
}

//------------------------------------------------------------------------

void HedgeBank::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  m_insect_pop = m_insect_pop * 3.0;
}

void BeetleBank::DoDevelopment( void ) {
  VegElement::DoDevelopment();
  m_insect_pop = m_insect_pop * cfg_beetlebankinsectscaler.value()*3.0;
}


